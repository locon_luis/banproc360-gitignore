ConfigurationsModuleConfig = {
    "BusinessControllerConfig": {
        "CommandHandler": [],
        "BusinessControllerClass": "ConfigurationsModule/BusinessControllers/BusinessController"
    },
    "PresentationControllerConfig": {
        "Default": {
            "PresentationExtensions": [
                "Navigation_Presentation_Extn"
            ],
            "PresentationControllerClass": "ConfigurationsModule/PresentationControllers/PresentationController"
        }
    },
    "Forms": {
        "desktop": {
            "frmBusinessConfigurations": {
                "ControllerExtensions": [
                  	"AdminConsoleCommonUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "TabUtil_FormExtn"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "ConfigurationsModule/frmBusinessConfigurationsController",
                "FormName": "ConfigurationsModule/frmBusinessConfigurations",
                "friendlyName": "frmBusinessConfigurations"
            },
            "frmConfigurationBundles": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "TabUtil_FormExtn"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "ConfigurationsModule/frmConfigurationBundlesController",
                "FormName": "ConfigurationsModule/frmConfigurationBundles",
                "friendlyName": "frmConfigurationBundles"
            }
        }
    },
    "ModuleName": "ConfigurationsModule"
};