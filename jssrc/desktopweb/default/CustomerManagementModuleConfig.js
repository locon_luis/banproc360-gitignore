CustomerManagementModuleConfig = {
    "BusinessControllerConfig": {
        "BusinessControllerClass": "CustomerManagementModule/BusinessControllers/BusinessController",
        "CommandHandler": []
    },
    "Forms": {
        "desktop": {
            "frmCustomerManagement": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "downloadCSV_Extn",
                    "CustomerManagement_Search_Extn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "CurrencyUtils_FormExtn",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmCustomerManagementController",
                "FormName": "CustomerManagementModule/frmCustomerManagement",
                "friendlyName": "frmCustomerManagement"
            },
            "frmAssistedOnboarding": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "AdminConsoleDateTimeUtilities",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmAssistedOnboardingController",
                "FormName": "CustomerManagementModule/frmAssistedOnboarding",
                "friendlyName": "frmAssistedOnboarding"
            },
            "frmLoansDashboard": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "AdminConsoleDateTimeUtilities",
                    "TabUtil_FormExtn",
                    "Sorting_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmLoansDashboardController",
                "FormName": "CustomerManagementModule/frmLoansDashboard",
                "friendlyName": "frmLoansDashboard"
            },
            "frmUpgradeUser": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "downloadCSV_Extn",
                    "CustomerManagement_Search_Extn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "CurrencyUtils_FormExtn",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmUpgradeUserController",
                "FormName": "CustomerManagementModule/frmUpgradeUser",
                "friendlyName": "frmUpgradeUser"
            },
            "frmTrackApplication": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "AdminConsoleDateTimeUtilities",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmTrackApplicationController",
                "FormName": "CustomerManagementModule/frmTrackApplication",
                "friendlyName": "frmTrackApplication"
            },
            "frmNewCustomer": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "downloadCSV_Extn",
                    "CustomerManagement_Search_Extn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "CurrencyUtils_FormExtn",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmNewCustomerController",
                "FormName": "CustomerManagementModule/frmNewCustomer",
                "friendlyName": "frmNewCustomer"
            },
            "frmCustomerProfileContacts": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "AdminConsoleDateTimeUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "downloadCSV_Extn",
                    "CustomerManagement_Search_Extn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "CurrencyUtils_FormExtn",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmCustomerProfileContactsController",
                "FormName": "CustomerManagementModule/frmCustomerProfileContacts",
                "friendlyName": "frmCustomerProfileContacts"
            },
            "frmCustomerProfileAccounts": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "AdminConsoleDateTimeUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "downloadCSV_Extn",
                    "CustomerManagement_Search_Extn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "CurrencyUtils_FormExtn",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmCustomerProfileAccountsController",
                "FormName": "CustomerManagementModule/frmCustomerProfileAccounts",
                "friendlyName": "frmCustomerProfileAccounts"
            },
            "frmCustomerProfileHelpCenter": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "AdminConsoleDateTimeUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "downloadCSV_Extn",
                    "CustomerManagement_Search_Extn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "CurrencyUtils_FormExtn",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmCustomerProfileHelpCenterController",
                "FormName": "CustomerManagementModule/frmCustomerProfileHelpCenter",
                "friendlyName": "frmCustomerProfileHelpCenter"
            },
            "frmCustomerProfileRoles": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "AdminConsoleDateTimeUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "downloadCSV_Extn",
                    "CustomerManagement_Search_Extn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "CurrencyUtils_FormExtn",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmCustomerProfileRolesController",
                "FormName": "CustomerManagementModule/frmCustomerProfileRoles",
                "friendlyName": "frmCustomerProfileRoles"
            },
            "frmCustomerProfileActivityHistory": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "AdminConsoleDateTimeUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "downloadCSV_Extn",
                    "CustomerManagement_Search_Extn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "CurrencyUtils_FormExtn",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmCustomerProfileActivityHistoryController",
                "FormName": "CustomerManagementModule/frmCustomerProfileActivityHistory",
                "friendlyName": "frmCustomerProfileActivityHistory"
            },
            "frmCustomerProfileAlerts": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "AdminConsoleDateTimeUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "downloadCSV_Extn",
                    "CustomerManagement_Search_Extn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "CurrencyUtils_FormExtn",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmCustomerProfileAlertsController",
                "FormName": "CustomerManagementModule/frmCustomerProfileAlerts",
                "friendlyName": "frmCustomerProfileAlerts"
            },
            "frmCustomerProfileDeviceInfo": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "AdminConsoleDateTimeUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "downloadCSV_Extn",
                    "CustomerManagement_Search_Extn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "CurrencyUtils_FormExtn",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmCustomerProfileDeviceInfoController",
                "FormName": "CustomerManagementModule/frmCustomerProfileDeviceInfo",
                "friendlyName": "frmCustomerProfileDeviceInfo"
            },
            "frmCustomerProfileEntitlements": {
                "ControllerExtensions": [
                    "AdminConsoleCommonUtilities",
                    "AdminConsoleDateTimeUtilities",
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "downloadCSV_Extn",
                    "CustomerManagement_Search_Extn",
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn",
                    "DateTimeUtils_FormExtn",
                    "CurrencyUtils_FormExtn",
                    "TabUtil_FormExtn",
                    "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmCustomerProfileEntitlementsController",
                "FormName": "CustomerManagementModule/frmCustomerProfileEntitlements",
                "friendlyName": "frmCustomerProfileEntitlements"
            },
            "frmDepositsDashboard": {
              "ControllerExtensions": ["AdminConsoleCommonUtilities",
                                       "AdminConsoleDateTimeUtilities",
                                       "Pagination_FormExtn",
                                       "Sorting_FormExtn",
                                       "downloadCSV_Extn","Navigation_Form_Extn",
                                       "DateTimeUtils_FormExtn",
                                       "CurrencyUtils_FormExtn",
                                       "TabUtil_FormExtn",
                                       "Navigation_Form_Extn",
                                       "WidgetPermission_Checker_FormExtns"
                                      ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CustomerManagementModule/frmDepositsDashboardController",
                "FormName": "CustomerManagementModule/frmDepositsDashboard",
                "friendlyName": "frmDepositsDashboard"
            }
        }
    },
    "ModuleName": "CustomerManagementModule",
    "PresentationControllerConfig": {
        "Default": {
            "PresentationControllerClass": "CustomerManagementModule/PresentationControllers/PresentationController",
            "PresentationExtensions": [
                "CustomerManagementModule/PresentationControllers/PresentationController_Extn",
                "Navigation_Presentation_Extn"
            ]
        }
    }
};