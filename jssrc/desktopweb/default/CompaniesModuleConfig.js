CompaniesModuleConfig = {
    "BusinessControllerConfig": {
        "CommandHandler": [],
        "BusinessControllerClass": "CompaniesModule/BusinessControllers/BusinessController"
    },
    "PresentationControllerConfig": {
        "Default": {
            "PresentationExtensions": [
                "Navigation_Presentation_Extn",
                "WidgetPermission_Checker_FormExtns",
                "Sorting_FormExtn"
            ],
            "PresentationControllerClass": "CompaniesModule/PresentationControllers/PresentationController"
        }
    },
    "Forms": {
        "desktop": {
            "frmCompanies": {
                "ControllerExtensions": [
                  "Navigation_Form_Extn","TabUtil_FormExtn","AdminConsoleCommonUtilities",
                  "DateTimeUtils_FormExtn",
                  "CurrencyUtils_FormExtn",
                  "Sorting_FormExtn",
                  "CustomerManagement_Search_Extn",
                  "AdminConsoleCommonUtilities",
                  "downloadCSV_Extn",
                  "ErrorInterceptor",
                  "googleApiKeys"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "Controller": "CompaniesModule/frmCompaniesController",
                "FormName": "CompaniesModule/frmCompanies",
                "friendlyName": "frmCompanies"
            }
        }
    },
    "ModuleName": "CompaniesModule"
};