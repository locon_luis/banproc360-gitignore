AlertsManagementModuleConfig = {
    "BusinessControllerConfig": {
        "BusinessControllerClass": "AlertsManagementModule/BusinessControllers/BusinessController",
        "CommandHandler": []
    },
    "Forms": {
        "desktop": {
            "frmAlertsManagement": {
                "Controller": "AlertsManagementModule/frmAlertsManagementController",
                "ControllerExtensions": [
                    "Pagination_FormExtn",
                    "Sorting_FormExtn",
                    "Navigation_Form_Extn",
                    "WidgetPermission_Checker_FormExtns",
                  	"TabUtil_FormExtn",
                    "ErrorInterceptor",
                    "AdminConsoleCommonUtilities"
    
                ],
                "FormController": "kony.mvc.MDAFormController",
                "FormName": "AlertsManagementModule/frmAlertsManagement",
                "friendlyName": "frmAlertsManagement"
            }
        }
    },
    "ModuleName": "AlertsManagementModule",
    "PresentationControllerConfig": {
        "Default": {
            "PresentationControllerClass": "AlertsManagementModule/PresentationControllers/PresentationController",
            "PresentationExtensions": [
                "Navigation_Presentation_Extn"
            ]
        }
    }
};