PasswordAgeAndLockoutModuleConfig = {
    "BusinessControllerConfig": {
        "BusinessControllerClass": "PasswordAgeAndLockoutModule/BusinessControllers/BusinessController",
        "CommandHandler": []
    },
    "Forms": {
        "desktop": {
            "frmPolicies": {
                "Controller": "PasswordAgeAndLockoutModule/frmPasswordAgeAndLockoutSettingsController",
                "ControllerExtensions": [
                    "WidgetPermission_Checker_FormExtns",
                    "Navigation_Form_Extn"
                ],
                "FormController": "kony.mvc.MDAFormController",
                "FormName": "PasswordAgeAndLockoutModule/frmPasswordAgeAndLockoutSettings",
                "friendlyName": "frmPasswordAgeAndLockoutSettings"
            }
        }
    },
    "ModuleName": "PasswordAgeAndLockoutModule",
    "PresentationControllerConfig": {
        "Default": {
            "PresentationControllerClass": "PasswordAgeAndLockoutModule/PresentationControllers/PresentationController",
            "PresentationExtensions": [
                "Navigation_Presentation_Extn"
            ]
        }
    }
};