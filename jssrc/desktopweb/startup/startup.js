kony.globals["appid"] = "Customer360";
kony.globals["build"] = "debug";
kony.globals["defaultLocale"] = "en_US";
kony.globals["locales"] = ["de_DE", "en_GB", "en_US", "es_ES", "fr_FR"];
kony.globals["i18nArray"] = [];
kony.globals["localization"] = "true";
kony.globals["i18nVersion"] = "1932495111";
//startup.js
var appConfig = {
    appId: "Customer360",
    appName: "Customer360",
    appVersion: "1.0.0",
    isturlbase: "https://konydev.banpro.com.ni/services",
    isDebug: true,
    isMFApp: true,
    appKey: "c69aa6247550cedea95dfa0941dd9558",
    appSecret: "9b58ba278128598144839d8acf207fb4",
    serviceUrl: "null/appconfig",
    svcDoc: {
        "identity_meta": {},
        "app_version": "1.0",
        "baseId": "ce4bb5fe-af04-4207-b156-f00f4a3cad37",
        "app_default_version": "1.0",
        "login": [{
            "mandatory_fields": [],
            "provider_type": "custom",
            "alias": "KonyBankingIdentityAD",
            "type": "basic",
            "prov": "KonyBankingIdentityAD",
            "url": "https://konydev.banpro.com.ni/authService/100000002"
        }, {
            "provider_type": "userstore",
            "alias": "democall",
            "type": "basic",
            "prov": "democall",
            "url": "https://konydev.banpro.com.ni/authService/100000002"
        }, {
            "provider_type": "konyads",
            "alias": "AdminConsoleIdentityAD",
            "type": "basic",
            "prov": "AdminConsoleIdentityAD",
            "url": "https://konydev.banpro.com.ni/authService/100000002"
        }, {
            "mandatory_fields": [],
            "provider_type": "custom",
            "alias": "KonyBankingAdminConsoleAPIIdentityService",
            "type": "basic",
            "prov": "KonyBankingAdminConsoleAPIIdentityService",
            "url": "https://konydev.banpro.com.ni/authService/100000002"
        }, {
            "mandatory_fields": [],
            "provider_type": "custom",
            "alias": "KonyBankingAdminConsoleIdentityService",
            "type": "basic",
            "prov": "KonyBankingAdminConsoleIdentityService",
            "url": "https://konydev.banpro.com.ni/authService/100000002"
        }],
        "services_meta": {
            "DecisionManagement": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/DecisionManagement",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/DecisionManagement"
            },
            "LoansLocalServices": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/LoansLocalServices",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/LoansLocalServices"
            },
            "MFAObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/MFAObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/MFAObjService"
            },
            "CardManagementObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/CardManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/CardManagementObjService"
            },
            "SystemManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/SystemManagement"
            },
            "ApplicantManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/ApplicantManagement"
            },
            "CustomerAndCustomerGroup": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/CustomerAndCustomerGroup"
            },
            "CustomerServiceManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/CustomerServiceManagement"
            },
            "BankProductManagment": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/BankProductManagment",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/BankProductManagment"
            },
            "ConsumerLendingDB": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/ConsumerLendingDB"
            },
            "CampaignManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/CampaignManagement"
            },
            "LocationDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/LocationDetails"
            },
            "BrowserManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/BrowserManagement"
            },
            "DpJavaService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/DpJavaService"
            },
            "InternalusersObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/InternalusersObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/InternalusersObjService"
            },
            "CommonGetDataService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/CommonGetDataService"
            },
            "RolesAndPermissionsObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/RolesAndPermissionsObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/RolesAndPermissionsObjService"
            },
            "BusinessBankingObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/BusinessBankingObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/BusinessBankingObjService"
            },
            "DBPServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/DBPServices"
            },
            "ReportService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/ReportService"
            },
            "DBPAuthService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/DBPAuthService"
            },
            "LoanSubmissions": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/LoanSubmissions"
            },
            "CustomerGroupsAndEntitlObjSvc": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/CustomerGroupsAndEntitlObjSvc",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/CustomerGroupsAndEntitlObjSvc"
            },
            "AuditLogsObjSvc": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/AuditLogsObjSvc",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/AuditLogsObjSvc"
            },
            "CardManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/CardManagement"
            },
            "BrowserManagementObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/BrowserManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/BrowserManagementObjService"
            },
            "AuthKMSService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/AuthKMSService"
            },
            "StaticContentManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/StaticContentManagement"
            },
            "ServerManagementObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/ServerManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/ServerManagementObjService"
            },
            "ValidateModule": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/ValidateModule"
            },
            "CRUDLayer": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/CRUDLayer"
            },
            "CustServiceObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/CustServiceObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/CustServiceObjService"
            },
            "TermsAndConditionsCustom": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/TermsAndConditionsCustom"
            },
            "CustomerManagementObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/CustomerManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/CustomerManagementObjService"
            },
            "AccountRequests": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/AccountRequests"
            },
            "C360_DependencyLibraries": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/C360_DependencyLibraries"
            },
            "MasterDataObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/MasterDataObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/MasterDataObjService"
            },
            "BusinessConfigObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/BusinessConfigObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/BusinessConfigObjService"
            },
            "AddressValidation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/AddressValidation"
            },
            "LeadManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/LeadManagement"
            },
            "DMSUserCreationObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/DMSUserCreationObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/DMSUserCreationObjService"
            },
            "LocationManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/LocationManagement"
            },
            "SecurityImagesManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/SecurityImagesManagement"
            },
            "CampaignManagementObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/CampaignManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/CampaignManagementObjService"
            },
            "EMailService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/EMailService"
            },
            "CustomIdentity": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/CustomIdentity"
            },
            "CustomerEntitlements": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/CustomerEntitlements"
            },
            "UserManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/UserManagement"
            },
            "EmailObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/EmailObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/EmailObjService"
            },
            "C360ADIdentityOrchService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/C360ADIdentityOrchService"
            },
            "LeadAndApplicant": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/LeadAndApplicant",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/LeadAndApplicant"
            },
            "TermsAndConditionsOrchCustom": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/TermsAndConditionsOrchCustom"
            },
            "RolesAndPermissionManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/RolesAndPermissionManagement"
            },
            "APICustomIdentity": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/APICustomIdentity"
            },
            "DpJsonServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/DpJsonServices"
            },
            "OLBServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/OLBServices"
            },
            "MFAConfigAndScenarios": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/MFAConfigAndScenarios"
            },
            "ConfigurationService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/ConfigurationService"
            },
            "AlertManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/AlertManagement"
            },
            "TestSVCUsingCRUDLayer": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/TestSVCUsingCRUDLayer"
            },
            "SecurityOpsObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/SecurityOpsObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/SecurityOpsObjService"
            },
            "IdentityManagementOrchService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/IdentityManagementOrchService"
            },
            "UserAuthentication": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/UserAuthentication"
            },
            "ConfigurationObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/ConfigurationObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/ConfigurationObjService"
            },
            "EmailKMSJavaService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/EmailKMSJavaService"
            },
            "ManageLimitsAndFees": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/ManageLimitsAndFees",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/ManageLimitsAndFees"
            },
            "TransactionAndAuditLogs": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/TransactionAndAuditLogs"
            },
            "IdentityManagementObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/IdentityManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/IdentityManagementObjService"
            },
            "IdentityManagementService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/IdentityManagementService"
            },
            "DashboardObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/DashboardObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/DashboardObjService"
            },
            "DecisionRuleManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/DecisionRuleManagement"
            },
            "AlertAndAlertTypes": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/AlertAndAlertTypes",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/AlertAndAlertTypes"
            },
            "BusinessBanking": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/BusinessBanking"
            },
            "AccountRequestsObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/AccountRequestsObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/AccountRequestsObjService"
            },
            "C360Identity": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/C360Identity"
            },
            "CustomerManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/CustomerManagement"
            },
            "CustomerRequest": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/CustomerRequest"
            },
            "LocationObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/LocationObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/LocationObjService"
            },
            "DMSUserCreation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/DMSUserCreation"
            },
            "StaticContentObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/StaticContentObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/StaticContentObjService"
            },
            "CustomerGroupsAndEntitlements": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/CustomerGroupsAndEntitlements"
            },
            "LimitsAndFeesIntegration": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/LimitsAndFeesIntegration"
            },
            "EmailOrchService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/EmailOrchService"
            },
            "ValidationServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/ValidationServices"
            },
            "LocationDummy": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/LocationDummy"
            },
            "TermsAndConditions": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/TermsAndConditions"
            },
            "dbpLoansLocalServicesJava": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/dbpLoansLocalServicesJava"
            },
            "SecurityQuestionsManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/SecurityQuestionsManagement"
            },
            "AuditLogs": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/AuditLogs"
            },
            "EligibilityCriteria": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/EligibilityCriteria"
            },
            "LocationsUsingCSVService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/LocationsUsingCSVService"
            },
            "C360IdentityOrchService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/C360IdentityOrchService"
            },
            "ReportsObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/ReportsObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/ReportsObjService"
            },
            "LogServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/LogServices"
            },
            "TermsAndConditionsObjService": {
                "offline": false,
                "metadata_url": "https://konydev.banpro.com.ni/services/metadata/v1/TermsAndConditionsObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/data/v1/TermsAndConditionsObjService"
            },
            "tempService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/tempService"
            },
            "ProductManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://konydev.banpro.com.ni/services/ProductManagement"
            }
        },
        "selflink": "https://konydev.banpro.com.ni:443/authService/100000002/appconfig",
        "integsvc": {
            "C360ADIdentityOrchService": "https://konydev.banpro.com.ni/services/C360ADIdentityOrchService",
            "TermsAndConditionsOrchCustom": "https://konydev.banpro.com.ni/services/TermsAndConditionsOrchCustom",
            "RolesAndPermissionManagement": "https://konydev.banpro.com.ni/services/RolesAndPermissionManagement",
            "APICustomIdentity": "https://konydev.banpro.com.ni/services/APICustomIdentity",
            "SystemManagement": "https://konydev.banpro.com.ni/services/SystemManagement",
            "ApplicantManagement": "https://konydev.banpro.com.ni/services/ApplicantManagement",
            "CustomerAndCustomerGroup": "https://konydev.banpro.com.ni/services/CustomerAndCustomerGroup",
            "CustomerServiceManagement": "https://konydev.banpro.com.ni/services/CustomerServiceManagement",
            "DpJsonServices": "https://konydev.banpro.com.ni/services/DpJsonServices",
            "OLBServices": "https://konydev.banpro.com.ni/services/OLBServices",
            "MFAConfigAndScenarios": "https://konydev.banpro.com.ni/services/MFAConfigAndScenarios",
            "ConfigurationService": "https://konydev.banpro.com.ni/services/ConfigurationService",
            "ConsumerLendingDB": "https://konydev.banpro.com.ni/services/ConsumerLendingDB",
            "CampaignManagement": "https://konydev.banpro.com.ni/services/CampaignManagement",
            "AlertManagement": "https://konydev.banpro.com.ni/services/AlertManagement",
            "TestSVCUsingCRUDLayer": "https://konydev.banpro.com.ni/services/TestSVCUsingCRUDLayer",
            "LocationDetails": "https://konydev.banpro.com.ni/services/LocationDetails",
            "BrowserManagement": "https://konydev.banpro.com.ni/services/BrowserManagement",
            "DpJavaService": "https://konydev.banpro.com.ni/services/DpJavaService",
            "IdentityManagementOrchService": "https://konydev.banpro.com.ni/services/IdentityManagementOrchService",
            "CommonGetDataService": "https://konydev.banpro.com.ni/services/CommonGetDataService",
            "UserAuthentication": "https://konydev.banpro.com.ni/services/UserAuthentication",
            "EmailKMSJavaService": "https://konydev.banpro.com.ni/services/EmailKMSJavaService",
            "TransactionAndAuditLogs": "https://konydev.banpro.com.ni/services/TransactionAndAuditLogs",
            "DBPServices": "https://konydev.banpro.com.ni/services/DBPServices",
            "_internal_logout": "https://konydev.banpro.com.ni/services/IST",
            "ReportService": "https://konydev.banpro.com.ni/services/ReportService",
            "DBPAuthService": "https://konydev.banpro.com.ni/services/DBPAuthService",
            "LoanSubmissions": "https://konydev.banpro.com.ni/services/LoanSubmissions",
            "IdentityManagementService": "https://konydev.banpro.com.ni/services/IdentityManagementService",
            "CardManagement": "https://konydev.banpro.com.ni/services/CardManagement",
            "AuthKMSService": "https://konydev.banpro.com.ni/services/AuthKMSService",
            "StaticContentManagement": "https://konydev.banpro.com.ni/services/StaticContentManagement",
            "DecisionRuleManagement": "https://konydev.banpro.com.ni/services/DecisionRuleManagement",
            "BusinessBanking": "https://konydev.banpro.com.ni/services/BusinessBanking",
            "C360Identity": "https://konydev.banpro.com.ni/services/C360Identity",
            "CustomerManagement": "https://konydev.banpro.com.ni/services/CustomerManagement",
            "ValidateModule": "https://konydev.banpro.com.ni/services/ValidateModule",
            "CustomerRequest": "https://konydev.banpro.com.ni/services/CustomerRequest",
            "CRUDLayer": "https://konydev.banpro.com.ni/services/CRUDLayer",
            "DMSUserCreation": "https://konydev.banpro.com.ni/services/DMSUserCreation",
            "CustomerGroupsAndEntitlements": "https://konydev.banpro.com.ni/services/CustomerGroupsAndEntitlements",
            "TermsAndConditionsCustom": "https://konydev.banpro.com.ni/services/TermsAndConditionsCustom",
            "LimitsAndFeesIntegration": "https://konydev.banpro.com.ni/services/LimitsAndFeesIntegration",
            "EmailOrchService": "https://konydev.banpro.com.ni/services/EmailOrchService",
            "AccountRequests": "https://konydev.banpro.com.ni/services/AccountRequests",
            "ValidationServices": "https://konydev.banpro.com.ni/services/ValidationServices",
            "LocationDummy": "https://konydev.banpro.com.ni/services/LocationDummy",
            "C360_DependencyLibraries": "https://konydev.banpro.com.ni/services/C360_DependencyLibraries",
            "TermsAndConditions": "https://konydev.banpro.com.ni/services/TermsAndConditions",
            "AddressValidation": "https://konydev.banpro.com.ni/services/AddressValidation",
            "dbpLoansLocalServicesJava": "https://konydev.banpro.com.ni/services/dbpLoansLocalServicesJava",
            "SecurityQuestionsManagement": "https://konydev.banpro.com.ni/services/SecurityQuestionsManagement",
            "AuditLogs": "https://konydev.banpro.com.ni/services/AuditLogs",
            "EligibilityCriteria": "https://konydev.banpro.com.ni/services/EligibilityCriteria",
            "LeadManagement": "https://konydev.banpro.com.ni/services/LeadManagement",
            "LocationsUsingCSVService": "https://konydev.banpro.com.ni/services/LocationsUsingCSVService",
            "LocationManagement": "https://konydev.banpro.com.ni/services/LocationManagement",
            "C360IdentityOrchService": "https://konydev.banpro.com.ni/services/C360IdentityOrchService",
            "SecurityImagesManagement": "https://konydev.banpro.com.ni/services/SecurityImagesManagement",
            "EMailService": "https://konydev.banpro.com.ni/services/EMailService",
            "CustomIdentity": "https://konydev.banpro.com.ni/services/CustomIdentity",
            "CustomerEntitlements": "https://konydev.banpro.com.ni/services/CustomerEntitlements",
            "LogServices": "https://konydev.banpro.com.ni/services/LogServices",
            "UserManagement": "https://konydev.banpro.com.ni/services/UserManagement",
            "tempService": "https://konydev.banpro.com.ni/services/tempService",
            "ProductManagement": "https://konydev.banpro.com.ni/services/ProductManagement"
        },
        "service_doc_etag": "000001721566C460",
        "appId": "6d00b464-78db-46bb-9f6e-9c2922e1e3da",
        "identity_features": {
            "reporting_params_header_allowed": true
        },
        "name": "KonyBankingAdminConsole",
        "reportingsvc": {
            "session": "https://konydev.banpro.com.ni/services/IST",
            "custom": "https://konydev.banpro.com.ni/services/CMS"
        },
        "Webapp": {
            "url": "https://konydev.banpro.com.ni/apps/Customer360"
        }
    },
    runtimeAppVersion: "1.0",
    eventTypes: ["FormEntry", "Error", "Crash"],
};
sessionID = "";

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        isMVC: true,
        APILevel: 7300
    })
};

function themeCallBack() {
    initializeGlobalVariables();
    requirejs.config({
        baseUrl: kony.appinit.getStaticContentPath() + 'desktopweb/appjs'
    });
    require(['kvmodules'], function() {
        applicationController = require("applicationController");
        kony.application.setApplicationInitializationEvents({
            init: applicationController.appInit,
            preappinit: applicationController.AS_AppEvents_hf8e5e479c1348dc9f9ed8e64ecde98e,
            deeplink: applicationController.AS_AppEvents_d9f275df03bb4f2c9c87f0aca8dac598,
            postappinit: applicationController.postAppInitCallBack,
            showstartupform: function() {
                new kony.mvc.Navigation("frmLogin").navigate();
            }
        });
    });
};

function onSuccess(oldlocalname, newlocalename, info) {
    loadResources();
};

function onFailure(errorcode, errormsg, info) {
    loadResources();
};

function loadResources() {
    kony.theme.packagedthemes(["default"]);
    globalhttpheaders = {};
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    spaAPM && spaAPM.startTracking();
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}

function initializeApp() {
    kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
    //This is the entry point for the application.When Locale comes,Local API call will be the entry point.
    kony.i18n.setDefaultLocaleAsync("en_US", onSuccess, onFailure, null);
};
									function getSPARequireModulesList(){ return ['kvmodules']; }
								