define("flxServicesAndFaqSelected", function() {
    return function(controller) {
        var flxServicesAndFaqSelected = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "id": "flxServicesAndFaqSelected",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknflxffffffop100"
        });
        flxServicesAndFaqSelected.setDefaultUnit(kony.flex.DP);
        var flxServicesAndFaq = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "id": "flxServicesAndFaq",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknflxffffffop100"
        });
        flxServicesAndFaq.setDefaultUnit(kony.flex.DP);
        var flxServicesAndFaqLeft = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxServicesAndFaqLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 2
        }, {}, {});
        flxServicesAndFaqLeft.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ff9169ffccb844c0a2386a7fef252b37,
            "skin": "slFbox",
            "top": "20dp",
            "width": "30px",
            "zIndex": 2
        }, {}, {});
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2({
            "centerX": "50%",
            "height": "12px",
            "id": "imgCheckBox",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkbox.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckbox.add(imgCheckBox);
        var flxDropdown = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_d2776e0c9b6c49ea8479d57f23bb90b1,
            "right": "10px",
            "skin": "slFbox",
            "top": "20dp",
            "width": "30px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var ImgArrow = new kony.ui.Image2({
            "height": "12px",
            "id": "ImgArrow",
            "isVisible": false,
            "left": "0dp",
            "skin": "slImage",
            "src": "img_desc_arrow.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonArrow = new kony.ui.Label({
            "height": "12px",
            "id": "fonticonArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconDescDownArrow12px",
            "text": "",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDropdown.add(ImgArrow, fonticonArrow);
        var lblServiceName = new kony.ui.Label({
            "bottom": "20dp",
            "id": "lblServiceName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Jompay",
            "top": "20dp",
            "width": "250px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxServicesAndFaqLeft.add(flxCheckbox, flxDropdown, lblServiceName);
        var lblDescription = new kony.ui.Label({
            "bottom": "20dp",
            "id": "lblDescription",
            "isVisible": true,
            "left": "29%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Mobile App Mobile AppMobile AppMobile AppMobile AppMobile AppMobile AppMobile AppMobile AppMobile AppMobile AppMobile AppMobile AppMobile AppMobile AppMobile AppMobile AppMobile App",
            "top": "20dp",
            "width": "34%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCategory = new kony.ui.Label({
            "bottom": "20dp",
            "id": "lblCategory",
            "isVisible": true,
            "left": "63.50%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Mobile App",
            "top": "20dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20dp",
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "70dp",
            "skin": "slFbox",
            "top": "20dp",
            "width": "120px",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblServiceStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblServiceStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "60dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgServiceStatus = new kony.ui.Image2({
            "centerY": "50%",
            "height": "12dp",
            "id": "imgServiceStatus",
            "isVisible": false,
            "left": "0dp",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "width": "12dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonActive = new kony.ui.Label({
            "centerY": "50%",
            "id": "fonticonActive",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblServiceStatus, imgServiceStatus, fonticonActive);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_cfb381a8f2044aa0824a0e32f2de5b89,
            "right": "35px",
            "skin": "slFbox",
            "top": "15dp",
            "width": "25px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var imgOptions = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "imgOptions",
            "isVisible": false,
            "left": 14,
            "skin": "slImage",
            "src": "dots3x.png",
            "width": "6dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "lblIconOptions",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(imgOptions, lblIconOptions);
        flxServicesAndFaq.add(flxServicesAndFaqLeft, lblDescription, lblCategory, flxStatus, flxOptions);
        var flxServiceDescriptionContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxServiceDescriptionContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "2dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxServiceDescriptionContent.setDefaultUnit(kony.flex.DP);
        var flxLeft = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLeft",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "65px",
            "zIndex": 20
        }, {}, {});
        flxLeft.setDefaultUnit(kony.flex.DP);
        var lbleft = new kony.ui.Label({
            "height": "0dp",
            "id": "lbleft",
            "isVisible": true,
            "left": "0dp",
            "skin": "slLabel",
            "top": "7dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeft.add(lbleft);
        var flxFaqDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFaqDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "55dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "91%",
            "zIndex": 20
        }, {}, {});
        flxFaqDetails.setDefaultUnit(kony.flex.DP);
        var lblDescriptionServices = new kony.ui.Label({
            "height": "15px",
            "id": "lblDescriptionServices",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "text": "Description",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblServiceDescription = new kony.ui.Label({
            "id": "lblServiceDescription",
            "isVisible": true,
            "left": "0px",
            "right": "35px",
            "skin": "sknlblLato5d6c7f12px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin scelerisque eleifend libero, a rhoncus sapien mattis ac. Donec dictum finibus sagittis. Vestibulum accumsan odio efficituDonec felis dolor, molestie. Curabitur est eros, volutpat in elit rutrum, tristique placerat nunc.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin  scelerisque eleifend libero, a rhoncus sapien mattis ac. Donec dictum finibus sagittis. Vestibulum<br> accumsan odio efficitur est molestie viverra.",
            "top": "40px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFaqDetails.add(lblDescriptionServices, lblServiceDescription);
        flxServiceDescriptionContent.add(flxLeft, flxFaqDetails);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "width": "96%",
            "zIndex": 20
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxServicesAndFaqSelected.add(flxServicesAndFaq, flxServiceDescriptionContent, lblSeparator);
        return flxServicesAndFaqSelected;
    }
})