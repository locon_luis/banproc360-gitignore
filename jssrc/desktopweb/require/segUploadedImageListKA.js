define("segUploadedImageListKA", function() {
    return function(controller) {
        var segUploadedImageListKA = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "segUploadedImageListKA",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        segUploadedImageListKA.setDefaultUnit(kony.flex.DP);
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "right": "90px",
            "skin": "slFbox0af9c2241871c44",
            "top": "20dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblImageStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblImageStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconStatus = new kony.ui.Label({
            "centerY": "56%",
            "height": "15dp",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon13pxGreen",
            "text": "",
            "top": "10dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblImageStatus, lblIconStatus);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknSeparatorFileList0f464e41c0a2e4a",
            "text": ".",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "27dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_i98b59c7d77540748c4d450772683aab,
            "onTouchStart": controller.AS_FlexContainer_a5aa33867d214d7681ca134bbcec27d3,
            "right": "60px",
            "skin": "slFbox0af9c2241871c44",
            "top": "10dp",
            "width": "27px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblOptions = new kony.ui.Label({
            "centerX": "45%",
            "centerY": "48%",
            "height": "15dp",
            "id": "lblOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "14dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblOptions);
        var imgUpload = new kony.ui.Image2({
            "centerY": "50%",
            "height": "46px",
            "id": "imgUpload",
            "isVisible": true,
            "left": "35dp",
            "skin": "slImage0c7f9310195944c",
            "src": "imgplaceholder.png",
            "top": "0dp",
            "width": "60px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        segUploadedImageListKA.add(flxStatus, lblSeperator, flxOptions, imgUpload);
        return segUploadedImageListKA;
    }
})