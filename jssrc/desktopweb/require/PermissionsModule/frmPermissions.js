define("PermissionsModule/frmPermissions", function() {
    return function(controller) {
        function addWidgetsfrmPermissions() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0.00%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.Permissions\")"
                    },
                    "lblUserName": {
                        "text": "Lauren"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "63dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "48dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false,
                        "left": "35dp"
                    },
                    "flxSearchContainer": {
                        "top": "5dp"
                    },
                    "flxSubHeader": {
                        "centerY": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "subHeader": {
                        "centerY": "50%",
                        "height": "48dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxScrollMainContent = new kony.ui.FlexContainer({
                "bottom": 0,
                "clipBounds": false,
                "height": "100%",
                "id": "flxScrollMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1px",
                "isModalContainer": false,
                "right": "-1px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxScrollMainContent.setDefaultUnit(kony.flex.DP);
            var flxPermissions = new kony.ui.FlexContainer({
                "clipBounds": false,
                "height": "100%",
                "id": "flxPermissions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0dp",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxPermissions.setDefaultUnit(kony.flex.DP);
            var flxPermissionsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "98%",
                "id": "flxPermissionsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxPermissionsContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderPermissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxHeaderPermissions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "width": "93%"
            }, {}, {});
            flxHeaderPermissions.setDefaultUnit(kony.flex.DP);
            var flxPermName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxPermName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_fbe01558a0a64e0ca4625eeff7f71077,
                "skin": "slFbox",
                "top": 0,
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxPermName.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": "37px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortName",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxPermName.add(lblName, fontIconSortName);
            var flxPermDesc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxPermDesc",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "24%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxPermDesc.setDefaultUnit(kony.flex.DP);
            var lblHeaderDesc = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderDesc",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPermDesc.add(lblHeaderDesc);
            var flxRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRoles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "65%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_bed9873cfdeb4311ba234401c30421f2,
                "skin": "slFbox",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxRoles.setDefaultUnit(kony.flex.DP);
            var lblRoleName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRoleName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.ROLES\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortRoles = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortRoles",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxRoles.add(lblRoleName, fontIconSortRoles);
            var flxUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxUsers",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "75%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxUsers.setDefaultUnit(kony.flex.DP);
            var lblUsers = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsers",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.USERS\")",
                "top": 0,
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortUser = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortUser",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxUsers.add(lblUsers, fontIconSortUser);
            var flxHeaderStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeaderStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "86%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_c62ddb6717304b7199d5a79964e58736,
                "skin": "slFbox",
                "top": "0dp",
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxHeaderStatus.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": 0,
                "width": "46px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconFilterStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilterStatus",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxHeaderStatus.add(lblStatus, fontIconFilterStatus);
            var lblHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblHeaderSeperator",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderPermissions.add(flxPermName, flxPermDesc, flxRoles, flxUsers, flxHeaderStatus, lblHeaderSeperator);
            var flxSegmentPermmissions = new kony.ui.FlexContainer({
                "clipBounds": false,
                "height": "85.50%",
                "id": "flxSegmentPermmissions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegmentPermmissions.setDefaultUnit(kony.flex.DP);
            var segPermissions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "fontIconStatusImg": "",
                    "lblDescription": "",
                    "lblIconOptions": "Label",
                    "lblNoOfRoles": "",
                    "lblNoOfUsers": "",
                    "lblPermissionName": "",
                    "lblPermissionStatus": "",
                    "lblSeperator": ""
                }],
                "groupCells": false,
                "height": "250dp",
                "id": "segPermissions",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_a0439cb31b4145b1bceae41287f27d42,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxPermissions",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxOptions": "flxOptions",
                    "flxPermissions": "flxPermissions",
                    "flxStatus": "flxStatus",
                    "fontIconStatusImg": "fontIconStatusImg",
                    "lblDescription": "lblDescription",
                    "lblIconOptions": "lblIconOptions",
                    "lblNoOfRoles": "lblNoOfRoles",
                    "lblNoOfUsers": "lblNoOfUsers",
                    "lblPermissionName": "lblPermissionName",
                    "lblPermissionStatus": "lblPermissionStatus",
                    "lblSeperator": "lblSeperator"
                },
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegmentPermmissions.add(segPermissions);
            var flxPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": false,
                "height": "30px",
                "id": "flxPagination",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox0d9c3974835234d",
                "top": "15dp",
                "width": "222px",
                "zIndex": 1
            }, {}, {});
            flxPagination.setDefaultUnit(kony.flex.DP);
            var lbxPagination = new kony.ui.ListBox({
                "height": "30dp",
                "id": "lbxPagination",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Page 1 of 20"],
                    ["lb2", "Page 2 of 20"],
                    ["lb3", "Page 3 of 20"],
                    ["lb4", "Page 4 of 20"]
                ],
                "onSelection": controller.AS_ListBox_a5aff4bd4d3446d283199e7c0539af03,
                "selectedKey": "lb1",
                "selectedKeyValue": ["lb1", "Page 1 of 20"],
                "skin": "sknlbxNobgNoBorderPagination",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlstbxcursor",
                "multiSelect": false
            });
            var flxPaginationSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPaginationSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "2dp",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxPaginationSeperator.setDefaultUnit(kony.flex.DP);
            flxPaginationSeperator.add();
            var flxPrevious = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxPrevious",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_f0a418b4f60c4b3aa94ca15c2d5cbcb7,
                "skin": "sknflxRadius40px",
                "width": "35px",
                "zIndex": 1
            }, {}, {});
            flxPrevious.setDefaultUnit(kony.flex.DP);
            var fontIconPrevious = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconPrevious",
                "isVisible": true,
                "left": "0px",
                "skin": "sknFontIconPrevNextPage",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconPrevious\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPrevious.add(fontIconPrevious);
            var flxNext = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxNext",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "3dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0b655f1f9284f149eb1e30bfdc44ad5,
                "skin": "sknflxRadius40px",
                "width": "35px",
                "zIndex": 1
            }, {}, {});
            flxNext.setDefaultUnit(kony.flex.DP);
            var fontIconNext = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconNext",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconPrevNextPage",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconNext\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNext.add(fontIconNext);
            var imgPreviousArrow = new kony.ui.Image2({
                "height": "30px",
                "id": "imgPreviousArrow",
                "isVisible": false,
                "left": "15px",
                "onTouchStart": controller.AS_Image_j56563ebfafd4b3d80502fec0c98cba7,
                "skin": "slImage",
                "src": "rightarrow_circel2x.png",
                "top": "0px",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgNextArrow = new kony.ui.Image2({
                "height": "30dp",
                "id": "imgNextArrow",
                "isVisible": false,
                "left": "10dp",
                "onTouchStart": controller.AS_Image_ed3c712efdb8401b8975d8b184830e46,
                "skin": "slImage",
                "src": "left_cricle.png",
                "top": "0dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPagination.add(lbxPagination, flxPaginationSeperator, flxPrevious, flxNext, imgPreviousArrow, imgNextArrow);
            var flxNoResultsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNoResultsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoResultsFound.setDefaultUnit(kony.flex.DP);
            var rtxSearchMesg = new kony.ui.RichText({
                "bottom": "40px",
                "centerX": "50%",
                "id": "rtxSearchMesg",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "90px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultsFound.add(rtxSearchMesg);
            flxPermissionsContainer.add(flxHeaderPermissions, flxSegmentPermmissions, flxPagination, flxNoResultsFound);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "7%",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100px",
                "width": "200px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var lblDescription = new kony.ui.Label({
                "id": "lblDescription",
                "isVisible": true,
                "left": "16px",
                "skin": "sknlblLatoBold9ca9ba14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ASSIGN\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRoles = new kony.ui.Label({
                "id": "lblRoles",
                "isVisible": false,
                "left": "15dp",
                "skin": "sknlbllato00a3d412px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Users\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPremissions = new kony.ui.Label({
                "bottom": 7,
                "id": "lblPremissions",
                "isVisible": false,
                "left": "15dp",
                "skin": "sknlbllato00a3d412px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.CopylblPremissions0f8ff66e806ad46\")",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnRoles = new kony.ui.Button({
                "id": "btnRoles",
                "isVisible": true,
                "left": "16dp",
                "skin": "contextuallinks",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.btnRoles\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPermissions = new kony.ui.Button({
                "bottom": "15dp",
                "id": "btnPermissions",
                "isVisible": true,
                "left": "16dp",
                "skin": "contextuallinks",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.Permissions\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeperator.setDefaultUnit(kony.flex.DP);
            flxSeperator.add();
            var flxOption1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0cf0cd7fc6d5840",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption1.setDefaultUnit(kony.flex.DP);
            var fontIconOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato11ABEB13px"
            });
            flxOption1.add(fontIconOption1, lblOption1);
            var flxOption2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0e5763148d82442",
                "top": "7dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption2.setDefaultUnit(kony.flex.DP);
            var fontIconOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconOption2",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato11ABEB13px"
            });
            flxOption2.add(fontIconOption2, lblOption2);
            var flxOption3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption3",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a6ed527bebbb44",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption3.setDefaultUnit(kony.flex.DP);
            var fontIconOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconOption3",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconOption3\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption3",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Suspend\")",
                "top": "4dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato11ABEB13px"
            });
            flxOption3.add(fontIconOption3, lblOption3);
            var flxOption4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption4",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0h2db4b5201f34d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption4.setDefaultUnit(kony.flex.DP);
            var fontIconOption4 = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconOption4",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDeactive\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption4 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption4",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato11ABEB13px"
            });
            flxOption4.add(fontIconOption4, lblOption4);
            flxSelectOptions.add(lblDescription, lblRoles, lblPremissions, btnRoles, btnPermissions, flxSeperator, flxOption1, flxOption2, flxOption3, flxOption4);
            var flxPermissionStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPermissionStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "877px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "130dp",
                "zIndex": 1
            }, {}, {});
            flxPermissionStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPermissionStatusFilter.add(statusFilterMenu);
            flxPermissions.add(flxPermissionsContainer, flxSelectOptions, flxPermissionStatusFilter);
            var flxViews = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViews",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViews.setDefaultUnit(kony.flex.DP);
            var flxPermissionBreadCrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxPermissionBreadCrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPermissionBreadCrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "35px",
                        "right": "35px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 1
                    },
                    "btnBackToMain": {
                        "left": "0px",
                        "text": "ALERTS"
                    },
                    "btnPreviousPage": {
                        "isVisible": true
                    },
                    "imgBreadcrumbsRight2": {
                        "isVisible": false
                    },
                    "lblCurrentScreen": {
                        "centerY": "viz.val_cleared",
                        "isVisible": true,
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPermissionBreadCrumb.add(breadcrumbs);
            var flxAddMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 35,
                "clipBounds": true,
                "height": "480px",
                "id": "flxAddMainContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 35,
                "skin": "sknflxf5f6f8Op100BorderE1E5Ed",
                "top": "60px",
                "zIndex": 1
            }, {}, {});
            flxAddMainContainer.setDefaultUnit(kony.flex.DP);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0%",
                "width": "215px",
                "zIndex": 1
            }, {}, {});
            flxOptions.setDefaultUnit(kony.flex.DP);
            var flxOptionDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxOptionDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOptionDetails.setDefaultUnit(kony.flex.DP);
            var btnOptionDetails = new kony.ui.Button({
                "id": "btnOptionDetails",
                "isVisible": true,
                "left": "30dp",
                "skin": "Btn000000font14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.PERMISSIONDETAILS\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconRightArrow1 = new kony.ui.Label({
                "id": "fontIconRightArrow1",
                "isVisible": true,
                "right": "25px",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "30px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxoptionSeperator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxoptionSeperator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxe1e5edop100",
                "width": "72.09%",
                "zIndex": 1
            }, {}, {});
            flxoptionSeperator1.setDefaultUnit(kony.flex.DP);
            flxoptionSeperator1.add();
            flxOptionDetails.add(btnOptionDetails, fontIconRightArrow1, flxoptionSeperator1);
            var flxAddPermissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxAddPermissions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddPermissions.setDefaultUnit(kony.flex.DP);
            var btnAddPermissions = new kony.ui.Button({
                "id": "btnAddPermissions",
                "isVisible": true,
                "left": "30dp",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.ADDPERMISSIONS\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddOptional1 = new kony.ui.Label({
                "id": "lblAddOptional1",
                "isVisible": true,
                "left": "30px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
                "top": "50px",
                "width": "93px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddSeperator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddSeperator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxe1e5edop100",
                "width": "72.09%",
                "zIndex": 1
            }, {}, {});
            flxAddSeperator2.setDefaultUnit(kony.flex.DP);
            flxAddSeperator2.add();
            var fontIconRightArrow2 = new kony.ui.Label({
                "id": "fontIconRightArrow2",
                "isVisible": true,
                "right": "25px",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "30px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddPermissions.add(btnAddPermissions, lblAddOptional1, flxAddSeperator2, fontIconRightArrow2);
            var flxAddRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxAddRoles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddRoles.setDefaultUnit(kony.flex.DP);
            var btnAddRoles = new kony.ui.Button({
                "id": "btnAddRoles",
                "isVisible": true,
                "left": "30dp",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.ASSIGNROLES\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddOptional3 = new kony.ui.Label({
                "id": "lblAddOptional3",
                "isVisible": true,
                "left": "30px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
                "top": "50px",
                "width": "93px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconRightArrow4 = new kony.ui.Label({
                "id": "fontIconRightArrow4",
                "isVisible": true,
                "right": "25px",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "30px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddSeperator4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddSeperator4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxe1e5edop100",
                "width": "72.09%",
                "zIndex": 1
            }, {}, {});
            flxAddSeperator4.setDefaultUnit(kony.flex.DP);
            flxAddSeperator4.add();
            flxAddRoles.add(btnAddRoles, lblAddOptional3, fontIconRightArrow4, flxAddSeperator4);
            var flxAddUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxAddUsers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddUsers.setDefaultUnit(kony.flex.DP);
            var btnAddUsers = new kony.ui.Button({
                "id": "btnAddUsers",
                "isVisible": true,
                "left": "30dp",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.ASSIGNUSERS\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddOptional2 = new kony.ui.Label({
                "id": "lblAddOptional2",
                "isVisible": true,
                "left": "30px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
                "top": "50px",
                "width": "93px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconRightArrow3 = new kony.ui.Label({
                "id": "fontIconRightArrow3",
                "isVisible": true,
                "right": "25px",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "30px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddSeperator3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddSeperator3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxe1e5edop100",
                "width": "72.09%",
                "zIndex": 1
            }, {}, {});
            flxAddSeperator3.setDefaultUnit(kony.flex.DP);
            flxAddSeperator3.add();
            flxAddUsers.add(btnAddUsers, lblAddOptional2, fontIconRightArrow3, flxAddSeperator3);
            flxOptions.add(flxOptionDetails, flxAddPermissions, flxAddRoles, flxAddUsers);
            var flxAddOptionsSection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddOptionsSection",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "215px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAddOptionsSection.setDefaultUnit(kony.flex.DP);
            var lblAddOptionsHeading = new kony.ui.Label({
                "id": "lblAddOptionsHeading",
                "isVisible": false,
                "left": "30px",
                "skin": "sknlblLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.ADD_USERS\")",
                "top": 30,
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddOptionsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5px",
                "clipBounds": true,
                "id": "flxAddOptionsContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "right": "30px",
                "skin": "slFbox",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxAddOptionsContainer.setDefaultUnit(kony.flex.DP);
            var flxAvailableOptionsSection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 80,
                "clipBounds": true,
                "id": "flxAvailableOptionsSection",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0c11dce22bfe447",
                "top": "0px",
                "width": "59.39%",
                "zIndex": 1
            }, {}, {});
            flxAvailableOptionsSection.setDefaultUnit(kony.flex.DP);
            var lblAvailableOptionsHeading = new kony.ui.Label({
                "id": "lblAvailableOptionsHeading",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.AvailableUsers\")",
                "top": 0,
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55px",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": 0,
                "skin": "sknflxf5f6f8Op100",
                "top": "30px",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "focusSkin": "slFbox0ebc847fa67a243Search",
                "height": "35px",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": 10,
                "skin": "sknflxd5d9ddop100",
                "zIndex": 1
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearch",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconSearchimg\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "100%",
                "id": "tbxSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "35dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "width": "83%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxSearchCrossImg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "35px",
                "id": "flxSearchCrossImg",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxSearchCrossImg.setDefaultUnit(kony.flex.DP);
            var fontIconCross = new kony.ui.Label({
                "centerY": "49%",
                "id": "fontIconCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchCrossImg.add(fontIconCross);
            flxSearchContainer.add(fontIconSearch, tbxSearchBox, flxSearchCrossImg);
            flxSearch.add(flxSearchContainer);
            var flxAddOptionsSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "id": "flxAddOptionsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffOp100BorderE1E5EdNoRadius",
                "top": "85dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddOptionsSegment.setDefaultUnit(kony.flex.DP);
            var segAddOptions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "data": [{
                    "btnAdd": "Add",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "Add",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "Add",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "Add",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "Add",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "Add",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "Add",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "Add",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "Add",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segAddOptions",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknsegffffffOp100Bordere1e5ed",
                "rowTemplate": "flxAddUsers",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnAdd": "btnAdd",
                    "flxAddUsers": "flxAddUsers",
                    "flxAddUsersWrapper": "flxAddUsersWrapper",
                    "flxxUsernameWrapper": "flxxUsernameWrapper",
                    "lblFullName": "lblFullName",
                    "lblUserIdValue": "lblUserIdValue",
                    "lblUsername": "lblUsername"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxAvailableOptionsMessage = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxAvailableOptionsMessage",
                "isVisible": false,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddOptionsSegment.add(segAddOptions, rtxAvailableOptionsMessage);
            var btnSelectAll = new kony.ui.Button({
                "id": "btnSelectAll",
                "isVisible": true,
                "right": 0,
                "skin": "sknBtnLato11ABEB11Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Add_All\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAvailableOptionsSection.add(lblAvailableOptionsHeading, flxSearch, flxAddOptionsSegment, btnSelectAll);
            var flxSelectedOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 75,
                "clipBounds": true,
                "id": "flxSelectedOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0c11dce22bfe447",
                "top": "0px",
                "width": "39%",
                "zIndex": 1
            }, {}, {});
            flxSelectedOptions.setDefaultUnit(kony.flex.DP);
            var lblSelectedOption = new kony.ui.Label({
                "id": "lblSelectedOption",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SelectedUsers\")",
                "top": 0,
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSegSelectedOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxSegSelectedOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegSelectedOptions.setDefaultUnit(kony.flex.DP);
            var segSelectedOptions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "fontIconClose": "Label",
                    "lblOption": "John Doe"
                }, {
                    "fontIconClose": "Label",
                    "lblOption": "John Doe"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segSelectedOptions",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "sknsegf9f9f9Op100",
                "rowTemplate": "flxOptionAdded",
                "sectionHeaderSkin": "sliPhoneSegmentHeader0h3448bdd9d8941",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "64646400",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": true,
                "top": "0px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAddOptionWrapper": "flxAddOptionWrapper",
                    "flxClose": "flxClose",
                    "flxOptionAdded": "flxOptionAdded",
                    "fontIconClose": "fontIconClose",
                    "lblOption": "lblOption"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxSelectedOptionsMessage = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxSelectedOptionsMessage",
                "isVisible": false,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Click_Add_to_select_a_user\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegSelectedOptions.add(segSelectedOptions, rtxSelectedOptionsMessage);
            var btnRemoveAll = new kony.ui.Button({
                "id": "btnRemoveAll",
                "isVisible": true,
                "right": 2,
                "skin": "sknBtnLato11ABEB11Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectedOptions.add(lblSelectedOption, flxSegSelectedOptions, btnRemoveAll);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "80px",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var flxAddOptionBtnseparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAddOptionBtnseparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddOptionBtnseparator.setDefaultUnit(kony.flex.DP);
            flxAddOptionBtnseparator.add();
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnCancel",
                "isVisible": true,
                "left": 0,
                "right": "29.50%",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var flxRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "width": "220px",
                "zIndex": 1
            }, {}, {});
            flxRightButtons.setDefaultUnit(kony.flex.DP);
            var btnNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px",
                "height": "40dp",
                "id": "btnNext",
                "isVisible": true,
                "left": 0,
                "skin": "bt6fb3cf",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "Btn0eb251a7a12014cHover"
            });
            var btnSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40dp",
                "id": "btnSave",
                "isVisible": true,
                "right": "0px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxRightButtons.add(btnNext, btnSave);
            flxButtons.add(flxAddOptionBtnseparator, btnCancel, flxRightButtons);
            flxAddOptionsContainer.add(flxAvailableOptionsSection, flxSelectedOptions, flxButtons);
            var flxAddRoleDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 50,
                "clipBounds": true,
                "id": "flxAddRoleDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "right": 30,
                "skin": "slFbox",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxAddRoleDetails.setDefaultUnit(kony.flex.DP);
            var flxName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75px",
                "id": "flxName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "41.74%",
                "zIndex": 1
            }, {}, {});
            flxName.setDefaultUnit(kony.flex.DP);
            var lblRoleNameKey = new kony.ui.Label({
                "id": "lblRoleNameKey",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.NameoftheRole\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxRoleNameValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxRoleNameValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblRoleNameSize = new kony.ui.Label({
                "id": "lblRoleNameSize",
                "isVisible": true,
                "right": "0px",
                "skin": "sknllbl485c75Lato13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblRoleNameSize\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxName.add(lblRoleNameKey, tbxRoleNameValue, lblRoleNameSize);
            var flxValidity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75px",
                "id": "flxValidity",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0dp",
                "width": "55.87%",
                "zIndex": 1
            }, {}, {});
            flxValidity.setDefaultUnit(kony.flex.DP);
            var lblValidFrom = new kony.ui.Label({
                "id": "lblValidFrom",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblValidity\")",
                "top": 0,
                "width": "48%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var calValidStartDate = new kony.ui.Calendar({
                "calendarIcon": "calbtn.png",
                "dateFormat": "MM/dd/yyyy",
                "height": "40dp",
                "id": "calValidStartDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknCal35475f14Px",
                "top": "30dp",
                "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
                "width": "48%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "noOfMonths": 1
            });
            var calValidEndDate = new kony.ui.Calendar({
                "calendarIcon": "calbtn.png",
                "dateFormat": "MM/dd/yyyy",
                "height": "40dp",
                "id": "calValidEndDate",
                "isVisible": true,
                "right": 1,
                "skin": "sknCal35475f14Px",
                "top": "30dp",
                "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
                "width": "48%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "noOfMonths": 1
            });
            var lblValidTo = new kony.ui.Label({
                "id": "lblValidTo",
                "isVisible": true,
                "right": 1,
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.CopylblValidity0d9c3f33b4acc44\")",
                "top": 0,
                "width": "48%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxValidity.add(lblValidFrom, calValidStartDate, calValidEndDate, lblValidTo);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblRoleDescription = new kony.ui.Label({
                "id": "lblRoleDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Description\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtRoleDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "200dp",
                "id": "txtRoleDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "numberOfVisibleLines": 3,
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxDescription.add(lblRoleDescription, txtRoleDescription);
            var flxRadioButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRadioButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "340dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRadioButtons.setDefaultUnit(kony.flex.DP);
            var RadioButtonGroup0f134922d89c646333 = new kony.ui.RadioButtonGroup({
                "height": "40dp",
                "id": "RadioButtonGroup0f134922d89c646333",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["rbg1", "Activate"],
                    ["rbg2", "Deactivate"]
                ],
                "selectedKey": "rbg2",
                "selectedKeyValue": ["rbg2", "Deactivate"],
                "skin": "RadioButtonGroup0g76fd84e461646",
                "top": "0dp",
                "width": "260dp",
                "zIndex": 1
            }, {
                "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioButtons.add(RadioButtonGroup0f134922d89c646333);
            var flxAddRoleButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAddRoleButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddRoleButtons.setDefaultUnit(kony.flex.DP);
            var btnAddRoleCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnAddRoleCancel",
                "isVisible": true,
                "left": 0,
                "right": "29.50%",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var flxAddRoleRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddRoleRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "width": "220px",
                "zIndex": 1
            }, {}, {});
            flxAddRoleRightButtons.setDefaultUnit(kony.flex.DP);
            var btnAddRoleNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px",
                "height": "40dp",
                "id": "btnAddRoleNext",
                "isVisible": true,
                "left": 0,
                "skin": "bt6fb3cf",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "Btn0eb251a7a12014cHover"
            });
            var btnAddRoleSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40dp",
                "id": "btnAddRoleSave",
                "isVisible": true,
                "right": "0px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxAddRoleRightButtons.add(btnAddRoleNext, btnAddRoleSave);
            flxAddRoleButtons.add(btnAddRoleCancel, flxAddRoleRightButtons);
            flxAddRoleDetails.add(flxName, flxValidity, flxDescription, flxRadioButtons, flxAddRoleButtons);
            var flxAddPermissionDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddPermissionDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddPermissionDetails.setDefaultUnit(kony.flex.DP);
            var flxScrollAddPermissionDetails = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "75%",
                "horizontalScrollIndicator": true,
                "id": "flxScrollAddPermissionDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "20dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxScrollAddPermissionDetails.setDefaultUnit(kony.flex.DP);
            var flxPermissionName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxPermissionName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "41.74%",
                "zIndex": 1
            }, {}, {});
            flxPermissionName.setDefaultUnit(kony.flex.DP);
            var lblPermissionNameKey = new kony.ui.Label({
                "id": "lblPermissionNameKey",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NameofthePermission\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxPermissionNameValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxPermissionNameValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var imgMandatoryPermissionName = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryPermissionName",
                "isVisible": false,
                "left": "134px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPermissionNameSize = new kony.ui.Label({
                "id": "lblPermissionNameSize",
                "isVisible": true,
                "right": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupNameCount\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoPermissionNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoPermissionNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoPermissionNameError.setDefaultUnit(kony.flex.DP);
            var lblNoPermissionErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoPermissionErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoPermissionError = new kony.ui.Label({
                "id": "lblNoPermissionError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblNoPermissionError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoPermissionNameError.add(lblNoPermissionErrorIcon, lblNoPermissionError);
            flxPermissionName.add(lblPermissionNameKey, tbxPermissionNameValue, imgMandatoryPermissionName, lblPermissionNameSize, flxNoPermissionNameError);
            var flxPermissionDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPermissionDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "right": "30dp",
                "skin": "slFbox",
                "top": "100dp",
                "zIndex": 1
            }, {}, {});
            flxPermissionDescription.setDefaultUnit(kony.flex.DP);
            var lblPermissiondescription = new kony.ui.Label({
                "id": "lblPermissiondescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Description\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPermissiondescriptionSize = new kony.ui.Label({
                "id": "lblPermissiondescriptionSize",
                "isVisible": true,
                "right": "0px",
                "skin": "sknllbl485c75Lato13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.lblMessageSize\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtPermissionDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "height": "200dp",
                "id": "txtPermissionDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 300,
                "numberOfVisibleLines": 3,
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblOptional = new kony.ui.Label({
                "id": "lblOptional",
                "isVisible": false,
                "left": "75px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                "top": "0dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoPermissionDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoPermissionDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "230dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoPermissionDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoPermissionDescErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoPermissionDescErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoPermissionDescriptionError = new kony.ui.Label({
                "id": "lblNoPermissionDescriptionError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblNoPermissionDescriptionError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoPermissionDescriptionError.add(lblNoPermissionDescErrorIcon, lblNoPermissionDescriptionError);
            flxPermissionDescription.add(lblPermissiondescription, lblPermissiondescriptionSize, txtPermissionDescription, lblOptional, flxNoPermissionDescriptionError);
            var flxPermissionStatusSwitch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPermissionStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "380dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPermissionStatusSwitch.setDefaultUnit(kony.flex.DP);
            var lblSwitchActivate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSwitchActivate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Activate\")",
                "top": 0,
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPermissionStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxPermissionStatus",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "45px",
                "zIndex": 1
            }, {}, {});
            flxPermissionStatus.setDefaultUnit(kony.flex.DP);
            flxPermissionStatus.add();
            var switchPermissionStatus = new kony.ui.Switch({
                "centerY": "50%",
                "height": "25dp",
                "id": "switchPermissionStatus",
                "isVisible": true,
                "left": "2px",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPermissionStatusSwitch.add(lblSwitchActivate, flxPermissionStatus, switchPermissionStatus);
            flxScrollAddPermissionDetails.add(flxPermissionName, flxPermissionDescription, flxPermissionStatusSwitch);
            var flxAddPermissionButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxAddPermissionButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddPermissionButtons.setDefaultUnit(kony.flex.DP);
            var flxFooterSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxFooterSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "right": "30dp",
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxFooterSeparator.setDefaultUnit(kony.flex.DP);
            flxFooterSeparator.add();
            var btnAddPermissionCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnAddPermissionCancel",
                "isVisible": true,
                "left": "30dp",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var flxAddPermissionRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddPermissionRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30dp",
                "skin": "slFbox",
                "width": "220px",
                "zIndex": 1
            }, {}, {});
            flxAddPermissionRightButtons.setDefaultUnit(kony.flex.DP);
            var btnAddPermissionNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px",
                "height": "40dp",
                "id": "btnAddPermissionNext",
                "isVisible": true,
                "left": 0,
                "skin": "bt6fb3cf",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "Btn0eb251a7a12014cHover"
            });
            var btnAddPermissionSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40dp",
                "id": "btnAddPermissionSave",
                "isVisible": true,
                "right": "0px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxAddPermissionRightButtons.add(btnAddPermissionNext, btnAddPermissionSave);
            flxAddPermissionButtons.add(flxFooterSeparator, btnAddPermissionCancel, flxAddPermissionRightButtons);
            flxAddPermissionDetails.add(flxScrollAddPermissionDetails, flxAddPermissionButtons);
            flxAddOptionsSection.add(lblAddOptionsHeading, flxAddOptionsContainer, flxAddRoleDetails, flxAddPermissionDetails);
            flxAddMainContainer.add(flxOptions, flxAddOptionsSection);
            var flxViewPermissions = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "480px",
                "horizontalScrollIndicator": true,
                "id": "flxViewPermissions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "right": 35,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "54dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxViewPermissions.setDefaultUnit(kony.flex.DP);
            var flxViewContainer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewContainer1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewContainer1.setDefaultUnit(kony.flex.DP);
            var flxViewKeyValue1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewKeyValue1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "240px",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue1.setDefaultUnit(kony.flex.DP);
            var fontIconViewKey1 = new kony.ui.Label({
                "id": "fontIconViewKey1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewName\")",
                "top": "1px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewKey1 = new kony.ui.Label({
                "id": "lblViewKey1",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": "2dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue1 = new kony.ui.Label({
                "id": "lblViewValue1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewNameValue\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewKeyValue1.add(fontIconViewKey1, lblViewKey1, lblViewValue1);
            var flxViewKeyValue2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewKeyValue2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30.77%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue2.setDefaultUnit(kony.flex.DP);
            var fontIconviewKey2 = new kony.ui.Label({
                "id": "fontIconviewKey2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewStatus\")",
                "top": "1px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewKey2 = new kony.ui.Label({
                "id": "lblViewKey2",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": "2dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue2 = new kony.ui.Label({
                "id": "lblViewValue2",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconViewValue2 = new kony.ui.Label({
                "id": "fontIconViewValue2",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "29dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewKeyValue2.add(fontIconviewKey2, lblViewKey2, lblViewValue2, fontIconViewValue2);
            var flxViewKeyValue3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewKeyValue3",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "61.54%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue3.setDefaultUnit(kony.flex.DP);
            var fontIconViewValue3 = new kony.ui.Label({
                "id": "fontIconViewValue3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconValidTill\")",
                "top": "1px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewKey3 = new kony.ui.Label({
                "id": "lblViewKey3",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.VALIDTILL\")",
                "top": "2dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue3 = new kony.ui.Label({
                "id": "lblViewValue3",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewValidTillValue\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewKeyValue3.add(fontIconViewValue3, lblViewKey3, lblViewValue3);
            var flxViewDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxViewDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxViewDescription.setDefaultUnit(kony.flex.DP);
            var lblViewDescription = new kony.ui.Label({
                "id": "lblViewDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconViewDescription = new kony.ui.Label({
                "id": "fontIconViewDescription",
                "isVisible": true,
                "left": "8px",
                "skin": "sknfontIconDescRightArrow14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblBreadcrumbsRight\")",
                "top": "1px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewDescription.add(lblViewDescription, fontIconViewDescription);
            var rtxViewDescription = new kony.ui.RichText({
                "bottom": "30px",
                "id": "rtxViewDescription",
                "isVisible": false,
                "left": "35dp",
                "right": 35,
                "skin": "sknrtxLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.rtxViewDescription\")",
                "top": "105dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewEditButton = new kony.ui.Button({
                "bottom": "0px",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "flxViewEditButton",
                "isVisible": true,
                "right": "35px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "15px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            flxViewContainer1.add(flxViewKeyValue1, flxViewKeyValue2, flxViewKeyValue3, flxViewDescription, rtxViewDescription, flxViewEditButton);
            var flxViewContainer2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 50,
                "clipBounds": true,
                "id": "flxViewContainer2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewContainer2.setDefaultUnit(kony.flex.DP);
            var flxViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewTabs.setDefaultUnit(kony.flex.DP);
            var flxViewTab1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50px",
                "zIndex": 1
            }, {}, {});
            flxViewTab1.setDefaultUnit(kony.flex.DP);
            var lblTabName1 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblTabName1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.ROLES\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [5, 0, 5, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            var flxTabUnderline1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "3dp",
                "id": "flxTabUnderline1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-10dp",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "top": "45dp",
                "width": "50px",
                "zIndex": 1
            }, {}, {});
            flxTabUnderline1.setDefaultUnit(kony.flex.DP);
            flxTabUnderline1.add();
            flxViewTab1.add(lblTabName1, flxTabUnderline1);
            var flxViewTab2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "110dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50px",
                "zIndex": 1
            }, {}, {});
            flxViewTab2.setDefaultUnit(kony.flex.DP);
            var lblTabName2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblTabName2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.USERS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [5, 0, 5, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            var flxTabUnderline2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "3dp",
                "id": "flxTabUnderline2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-10dp",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "top": "45dp",
                "width": "50px",
                "zIndex": 1
            }, {}, {});
            flxTabUnderline2.setDefaultUnit(kony.flex.DP);
            flxTabUnderline2.add();
            flxViewTab2.add(lblTabName2, flxTabUnderline2);
            flxViewTabs.add(flxViewTab1, flxViewTab2);
            var flxViewSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxD5D9DD",
                "top": "66dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSeperator.setDefaultUnit(kony.flex.DP);
            flxViewSeperator.add();
            var flxViewSegmentAndHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewSegmentAndHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": 35,
                "skin": "slFbox",
                "top": "67dp",
                "zIndex": 1
            }, {}, {});
            flxViewSegmentAndHeaders.setDefaultUnit(kony.flex.DP);
            var flxPermissionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxPermissionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxPermissionsHeader.setDefaultUnit(kony.flex.DP);
            var flxViewPermissionName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewPermissionName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewPermissionName.setDefaultUnit(kony.flex.DP);
            var lblViewPermissionName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewPermissionName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": "37px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconViewPermsnNameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconViewPermsnNameSort",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewPermissionName.add(lblViewPermissionName, fontIconViewPermsnNameSort);
            var flxViewPermissionDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewPermissionDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "26.27%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewPermissionDescription.setDefaultUnit(kony.flex.DP);
            var lblViewPermissionDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewPermissionDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "top": 0,
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewPermissionDescription.add(lblViewPermissionDescription);
            var flxViewPermissionSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewPermissionSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewPermissionSeperator.setDefaultUnit(kony.flex.DP);
            flxViewPermissionSeperator.add();
            flxPermissionsHeader.add(flxViewPermissionName, flxViewPermissionDescription, flxViewPermissionSeperator);
            var flxUsersHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxUsersHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxUsersHeader.setDefaultUnit(kony.flex.DP);
            var flxViewUsersFullName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewUsersFullName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewUsersFullName.setDefaultUnit(kony.flex.DP);
            var lblViewUsersFullName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewUsersFullName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.FULLNAME\")",
                "top": 0,
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconViewUsersNameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconViewUsersNameSort",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewUsersFullName.add(lblViewUsersFullName, fontIconViewUsersNameSort);
            var flxViewUsersUsername = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewUsersUsername",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "18.93%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewUsersUsername.setDefaultUnit(kony.flex.DP);
            var lblViewUsersUsername = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewUsersUsername",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.USERNAME\")",
                "top": 0,
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconViewUsersUsrNameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconViewUsersUsrNameSort",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewUsersUsername.add(lblViewUsersUsername, fontIconViewUsersUsrNameSort);
            var flxViewUsersEmailId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewUsersEmailId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "36.69%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewUsersEmailId.setDefaultUnit(kony.flex.DP);
            var lblEmail = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmail",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.EMAILID\")",
                "top": 0,
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconViewUsersEmailSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconViewUsersEmailSort",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewUsersEmailId.add(lblEmail, fontIconViewUsersEmailSort);
            var flxViewUsersUpdatedBy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewUsersUpdatedBy",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "60.35%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewUsersUpdatedBy.setDefaultUnit(kony.flex.DP);
            var lblRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.UPDATEDBY\")",
                "top": 0,
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconViewUpdateBySort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconViewUpdateBySort",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewUsersUpdatedBy.add(lblRole, fontIconViewUpdateBySort);
            var flxViewUsersUpdatedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewUsersUpdatedOn",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "77.28%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewUsersUpdatedOn.setDefaultUnit(kony.flex.DP);
            var lblPermissions = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPermissions",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.UPDATEDON\")",
                "top": 0,
                "width": "82px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconViewUpdatedOnSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconViewUpdatedOnSort",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewUsersUpdatedOn.add(lblPermissions, fontIconViewUpdatedOnSort);
            var flxViewUsersSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewUsersSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxD5D9DD",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewUsersSeperator.setDefaultUnit(kony.flex.DP);
            flxViewUsersSeperator.add();
            flxUsersHeader.add(flxViewUsersFullName, flxViewUsersUsername, flxViewUsersEmailId, flxViewUsersUpdatedBy, flxViewUsersUpdatedOn, flxViewUsersSeperator);
            var flxViewSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxViewSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "61dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSegment.setDefaultUnit(kony.flex.DP);
            var segViewSegment = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblPermissionName": " A/c Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblPermissionName": "FD Admin",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblPermissionName": "Backend Admin",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblPermissionName": "ATM Admin",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblPermissionName": "Loans Admin",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblPermissionName": " Branch Admin",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblPermissionName": "View Permissions",
                    "lblSeperator": "."
                }],
                "groupCells": false,
                "id": "segViewSegment",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxViewPermissions",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxViewPermissions": "flxViewPermissions",
                    "lblDescription": "lblDescription",
                    "lblPermissionName": "lblPermissionName",
                    "lblSeperator": "lblSeperator"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewSegment.add(segViewSegment);
            var rtxAvailabletxt = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "50%",
                "id": "rtxAvailabletxt",
                "isVisible": false,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.rtxAvailabletxt\")",
                "top": "50px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewSegmentAndHeaders.add(flxPermissionsHeader, flxUsersHeader, flxViewSegment, rtxAvailabletxt);
            var flxPagination1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": true,
                "height": "30px",
                "id": "flxPagination1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox0d9c3974835234d",
                "top": "90px",
                "width": "222px",
                "zIndex": 1
            }, {}, {});
            flxPagination1.setDefaultUnit(kony.flex.DP);
            var lbxPaginationKA = new kony.ui.ListBox({
                "height": "30dp",
                "id": "lbxPaginationKA",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Page 1 of 20"],
                    ["lb2", "Page 2 of 20"],
                    ["lb3", "Page 3 of 20"],
                    ["lb4", "Page 4 of 20"]
                ],
                "selectedKey": "lb1",
                "selectedKeyValue": ["lb1", "Page 1 of 20"],
                "skin": "sknlbxNobgNoBorderPagination",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxPaginationSeperatorKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPaginationSeperatorKA",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "2dp",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxPaginationSeperatorKA.setDefaultUnit(kony.flex.DP);
            flxPaginationSeperatorKA.add();
            var flxPrevious2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxPrevious2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius100px",
                "top": "0dp",
                "width": "30px",
                "zIndex": 1
            }, {}, {});
            flxPrevious2.setDefaultUnit(kony.flex.DP);
            var imgPrevious2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgPrevious2",
                "isVisible": true,
                "skin": "slImage",
                "src": "rightarrow_circel2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPrevious2.add(imgPrevious2);
            var flxNext2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxNext2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius100px",
                "top": "0dp",
                "width": "30px",
                "zIndex": 1
            }, {}, {});
            flxNext2.setDefaultUnit(kony.flex.DP);
            var imgNext2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgNext2",
                "isVisible": true,
                "skin": "slImage",
                "src": "left_cricle.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNext2.add(imgNext2);
            var imgPreviousArrow1 = new kony.ui.Image2({
                "height": "30px",
                "id": "imgPreviousArrow1",
                "isVisible": true,
                "left": "15px",
                "skin": "slImage",
                "src": "rightarrow_circel2x.png",
                "top": "0px",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgNextArrow1 = new kony.ui.Image2({
                "height": "30dp",
                "id": "imgNextArrow1",
                "isVisible": true,
                "left": "10dp",
                "skin": "slImage",
                "src": "left_cricle.png",
                "top": "0dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPagination1.add(lbxPaginationKA, flxPaginationSeperatorKA, flxPrevious2, flxNext2, imgPreviousArrow1, imgNextArrow1);
            flxViewContainer2.add(flxViewTabs, flxViewSeperator, flxViewSegmentAndHeaders, flxPagination1);
            flxViewPermissions.add(flxViewContainer1, flxViewContainer2);
            flxViews.add(flxPermissionBreadCrumb, flxAddMainContainer, flxViewPermissions);
            flxScrollMainContent.add(flxPermissions, flxViews);
            var flxScrollParent = new kony.ui.FlexContainer({
                "bottom": 0,
                "clipBounds": true,
                "height": "220dp",
                "id": "flxScrollParent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxScrollParent.setDefaultUnit(kony.flex.DP);
            flxScrollParent.add();
            flxRightPanel.add(flxMainHeader, flxMainSubHeader, flxScrollMainContent, flxScrollParent);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var flxToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "45px",
                "id": "flxToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknflxSuccessToast1F844D",
                "top": "0%",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxToastContainer.setDefaultUnit(kony.flex.DP);
            var imgLeft = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgLeft",
                "isVisible": true,
                "left": "15px",
                "skin": "slImage",
                "src": "arrow2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbltoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbltoastMessage",
                "isVisible": true,
                "left": "45px",
                "right": "45px",
                "skin": "lblfffffflatoregular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SuccessfullyDeactivated\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgRight = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRight",
                "isVisible": true,
                "right": "15px",
                "skin": "slImage",
                "src": "close_small2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToastContainer.add(imgLeft, lbltoastMessage, imgRight);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(flxToastContainer, toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "23%",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "180px",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "30%",
                "id": "imgLoading",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxMain.add(flxLeftPannel, flxRightPanel, flxHeaderDropdown, flxToastMessage, flxLoading);
            var flxDeactivatePermission = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeactivatePermission",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxDeactivatePermission.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp1({
                "clipBounds": true,
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "180px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DeactivatePermission\")"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "20px",
                        "right": "viz.val_cleared",
                        "text": "Are you sure to deactivate the Permission \"View\"?<br><br>\nThe Permission has been assigned to few roles. Deactivating this may impact the users who are assigned to these roles to perform certain actions.",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeactivatePermission.add(popUp);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxMain, flxDeactivatePermission, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmPermissions,
            "enabledForIdleTimeout": true,
            "id": "frmPermissions",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_ae86ce8d24c342a89e68d036c12fba87(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_jb75f86eee8d4ae7a1dc665136ca7a8d,
            "retainScrollPosition": false
        }]
    }
});