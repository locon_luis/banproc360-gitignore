define(['Promisify', 'ErrorInterceptor', 'ErrorIsNetworkDown'], function(Promisify, ErrorInterceptor, isNetworkDown) {
    function CustomerManagement_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.searchCount = 0;
        this.companies = [];
        this.AsyncLoadingModel = {
            isAsyncLoading: false,
            currentCTR: 0,
            expectedCTR: 0
        };
        this.customerCreateModel = {
            usernameRulesAndPolicy: null
        };
        this.searchInputs = {
            name: null,
            firstname: null,
            lastname: null,
            email: null,
            id: null,
            phone: null,
            username: null,
            applicationId: null,
            SSN: null,
            driverLicense: null
        };
        this.searchModel = {
            customers: null,
            target: null
        };
        this.CustomerBasicInfo = {
            customer: null,
            configuration: null,
            target: null
        };
        this.ApplicantInfo = {
            applicant: null
        };
        this.CustomerContactInfo = {
            Addresses: null,
            Emails: null,
            ContactNumbers: null,
            PreferredTimeAndMethod: null,
            target: null,
            subTarget: null
        };
        this.toastModel = {
            message: null,
            status: null,
            operation: null,
            context: null
        };
        this.searchTarget = null;
        this.StatusGroup = null;
        this.AllGroups = null;
        this.CustomerNotes = null;
        this.CustomerAccounts = null;
        this.AccountTrasactions = null;
        this.CustomerSessions = null;
        this.CustomerSessionActivities = null;
        this.CustomerGroups = {
            AssignedGroups: [],
            AllGroups: [],
            target: null
        };
        this.CustomerEntitlements = {
            AssignedEntitlements: [],
            AllEntitlements: [],
            CustomerIndirectEntitlements: [],
            target: null
        };
        this.CustomerIndirectEntitlements = null;
        this.AllEntitlements = null;
        this.AddressModel = {
            countries: [],
            states: [],
            cities: []
        };
        this.prefData = {
            AlertCategoryId: null,
            categories: null,
            catPref: null,
            channelPref: null,
            typePref: null
        };
        this.ActivityHistory = {
            modulesMap: null,
            activityMap: null
        };
        this.CustomerRequestAndNotificationCount = null;
        this.LockedOnInfo = null;
        this.CustomerRequests = null;
        this.CustomerNotifications = null;
        this.alertHistory = null;
        this.CoreBankingUpdate = null;
        this.customerLockStatus = null;
        this.OnlineBankingLogin = null;
        this.SEARCH_CONFIG_LOANS = "LOANS";
        this.SEARCH_CONFIG_BANKING = "BANKING";
        this.SEARCH_CONFIG_BASIC = "BASIC";
        this.LoansMasterData = null;
        this.sourceFormDetail = {
            name: null,
            data: null
        };
    }
    inheritsFrom(CustomerManagement_PresentationController, kony.mvc.Presentation.BasePresenter);
    CustomerManagement_PresentationController.prototype.initializePresentationController = function() {
        var self = this;
        ErrorInterceptor.wrap(this, 'businessController').match(function(on) {
            return [
                on(isNetworkDown).do(function() {
                    self.presentUserInterface('frmCustomerManagement', {
                        NetworkDownMessage: {}
                    });
                })
            ];
        });
    };
    /**
     * @name showCustomerManagement
     * @member CustomerManagementModule.presentationController
     * 
     */
    CustomerManagement_PresentationController.prototype.showCustomerManagement = function() {
        this.navigateTo('CustomerManagementModule', 'displayCustomerMangement');
    };
    /**
     * @name showCompaniesCustomerEdit
     * @member CustomerManagementModule.presentationController
     * 
     */
    CustomerManagement_PresentationController.prototype.showCompaniesCustomerEdit = function(context) {
        this.navigateTo('CompaniesModule', 'navigateToEditCustomerScreen', [context]);
    };
    /**
     * @name displayCustomerMangement
     * @member CustomerManagementModule.presentationController
     * 
     */
    CustomerManagement_PresentationController.prototype.displayCustomerMangement = function() {
        this.presentUserInterface('frmCustomerManagement', {
            "showCustomerSearch": true,
            "shouldReset": true
        });
        this.AsyncLoadingModel.isAsyncLoading = true;
        this.AsyncLoadingModel.currentCTR = 0;
        this.AsyncLoadingModel.expectedCTR = 3;
        this.getCitiesStatesAndCountries();
        this.getAllCompanies();
        this.getAllGroups();
        this.sourceFormDetail = {
            name: null,
            data: null
        };
    };
    CustomerManagement_PresentationController.prototype.displayCustomerMangementSearchForm = function() {
        this.presentUserInterface('frmCustomerManagement', {
            "showCustomerSearch": true
        });
    };
    CustomerManagement_PresentationController.prototype.navigateToProfileForm = function() {
        this.navigateTo('CustomerManagementModule', 'showCustomerManagementPersonal');
    };
    CustomerManagement_PresentationController.prototype.showCustomerManagementPersonal = function() {
        var self = this;
        self.presentUserInterface('frmCustomerProfileContacts', {
            "CustomerBasicInfo": self.CustomerBasicInfo
        });
    };
    CustomerManagement_PresentationController.prototype.turnOnCustomerManagementPersonal = function() {
        this.presentUserInterface('frmCustomerProfileContacts', {
            "HeaderRefresh": true
        });
    };
    CustomerManagement_PresentationController.prototype.turnOnCustomerManagementSearch = function() {
        this.presentUserInterface('frmCustomerManagement', {
            "LoadingScreen": {
                focus: false
            }
        });
    };
    CustomerManagement_PresentationController.prototype.diplayCustomerProfileFromOtherModule = function(Customer_id, formDetail) {
        this.presentUserInterface('frmCustomerManagement', {
            "showCustomerSearch": true,
            "shouldReset": true
        });
        this.AsyncLoadingModel.isAsyncLoading = true;
        this.AsyncLoadingModel.currentCTR = 0;
        this.AsyncLoadingModel.expectedCTR = 4;
        this.sourceFormDetail.name = formDetail;
        this.getCitiesStatesAndCountries();
        this.getAllCompanies();
        this.getAllGroups();
        this.getCustomerBasicInfo({
            "Customer_id": Customer_id
        }, "InfoScreen", null);
    };
    CustomerManagement_PresentationController.prototype.sourceFormNavigatedFrom = function() {
        return this.sourceFormDetail;
    };
    CustomerManagement_PresentationController.prototype.navigateToContactsTab = function(Customer_id) {
        this.CustomerBasicInfo.target = "InfoScreen";
        if (this.CustomerBasicInfo.customer) {
            this.presentUserInterface('frmCustomerProfileContacts', {
                "CustomerBasicInfo": this.CustomerBasicInfo
            });
        } else {
            this.getCustomerBasicInfo({
                "Customer_id": Customer_id
            }, "InfoScreen");
        }
    };
    CustomerManagement_PresentationController.prototype.navigateToAccountsTab = function() {
        this.CustomerBasicInfo.target = "InfoScreen";
        this.presentUserInterface('frmCustomerProfileAccounts', {
            "LoadingScreen": {
                focus: true
            }
        });
        this.presentUserInterface('frmCustomerProfileAccounts', {
            "CustomerBasicInfo": this.CustomerBasicInfo
        });
        this.getCustomerAccounts({
            "CustomerUsername": this.getCurrentCustomerDetails().Username
        });
    };
    CustomerManagement_PresentationController.prototype.navigateToHelpCenterTab = function() {
        this.CustomerBasicInfo.target = "InfoScreen";
        this.presentUserInterface('frmCustomerProfileHelpCenter', {
            "LoadingScreen": {
                focus: true
            }
        });
        this.presentUserInterface('frmCustomerProfileHelpCenter', {
            "CustomerBasicInfo": this.CustomerBasicInfo
        });
        this.getCustomerRequests({
            "username": this.getCurrentCustomerDetails().Username
        });
    };
    CustomerManagement_PresentationController.prototype.navigateToRolesTab = function() {
        this.CustomerBasicInfo.target = "InfoScreen";
        this.presentUserInterface('frmCustomerProfileRoles', {
            "LoadingScreen": {
                focus: true
            }
        });
        this.presentUserInterface('frmCustomerProfileRoles', {
            "CustomerBasicInfo": this.CustomerBasicInfo
        });
        this.getCustomerGroups({
            "customerID": this.getCurrentCustomerDetails().Customer_id
        }, "InfoScreen");
    };
    CustomerManagement_PresentationController.prototype.navigateToActivityHistoryTab = function() {
        this.CustomerBasicInfo.target = "InfoScreen";
        this.presentUserInterface('frmCustomerProfileActivityHistory', {
            "LoadingScreen": {
                focus: true
            }
        });
        this.presentUserInterface('frmCustomerProfileActivityHistory', {
            "CustomerBasicInfo": this.CustomerBasicInfo
        });
        this.getLastNCustomerSessions({
            "customerUsername": this.getCurrentCustomerDetails().Username
        });
    };
    CustomerManagement_PresentationController.prototype.navigateToAlertHistoryTab = function() {
        this.CustomerBasicInfo.target = "InfoScreen";
        var customerId = this.getCurrentCustomerDetails().Customer_id;
        this.presentUserInterface('frmCustomerProfileAlerts', {
            "LoadingScreen": {
                focus: true
            }
        });
        this.presentUserInterface('frmCustomerProfileAlerts', {
            "CustomerBasicInfo": this.CustomerBasicInfo
        });
        this.getCustomerAlertHistory({
            "CustomerId": customerId
        });
        this.getAlertCategories(customerId);
    };
    CustomerManagement_PresentationController.prototype.navigateToDeviceInfoTab = function() {
        this.CustomerBasicInfo.target = "InfoScreen";
        this.presentUserInterface('frmCustomerProfileDeviceInfo', {
            "LoadingScreen": {
                focus: true
            }
        });
        this.presentUserInterface('frmCustomerProfileDeviceInfo', {
            "CustomerBasicInfo": this.CustomerBasicInfo
        });
        this.getCustomerDevices({
            "$filter": "Customer_id eq " + this.getCurrentCustomerDetails().Customer_id
        });
    };
    CustomerManagement_PresentationController.prototype.navigateToEntitlementsTab = function() {
        this.CustomerBasicInfo.target = "InfoScreen";
        this.presentUserInterface('frmCustomerProfileEntitlements', {
            "LoadingScreen": {
                focus: true
            }
        });
        this.presentUserInterface('frmCustomerProfileEntitlements', {
            "CustomerBasicInfo": this.CustomerBasicInfo
        });
        this.getCustomerIndirectEntitlements({
            "customerID": this.getCurrentCustomerDetails().Customer_id
        }, "InfoScreen");
    };
    /**
     * @name searchCustomers
     * @member CustomerManagementModule.presentationController
     * @param {_searchType : string, _id : null, _name : string, _username : null, _phone : null, _email : null, _group : null, _requestID : null, _SSN : null, _pageOffset : string, _pageSize : number, _sortVariable : string, _sortDirection : string} data
     * @param string target
     */
    CustomerManagement_PresentationController.prototype.searchCustomers = function(data) {
        var self = this;
        this.searchModel.customers = [];
        self.presentUserInterface('frmCustomerManagement', {
            "LoadingScreen": {
                focus: true
            }
        });

        function searchCompletionCallback(response) {
            self.searchCount++;
            self.searchModel.customers = response.records;
            self.presentUserInterface('frmCustomerManagement', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmCustomerManagement', {
                "searchModel": self.searchModel
            });
            if (response.records.length == 1) {
                self.CustomerBasicInfo.customer = response.customerbasicinfo_view;
                self.CustomerBasicInfo.configuration = response.Configuration;
                if (response.requestCount && response.notificationCount && (response.requestCount != 0 || response.notificationCount != 0)) {
                    self.CustomerRequestAndNotificationCount = {
                        "requestCount": response.requestCount,
                        "notificationCount": response.notificationCount
                    };
                } else {
                    self.CustomerRequestAndNotificationCount = null;
                }
                self.CustomerBasicInfo.target = "InfoScreen";
                self.presentUserInterface('frmCustomerProfileContacts', {
                    "LoadingScreen": {
                        focus: false
                    }
                });
                self.presentUserInterface('frmCustomerProfileContacts', {
                    "CustomerBasicInfo": self.CustomerBasicInfo
                });
            }
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerManagement', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmCustomerManagement', {
                "toastModel": self.toastModel
            });
        }
        data["_searchType"] = "CUSTOMER_SEARCH";
        self.businessController.searchCustomers(data, searchCompletionCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.isValidForApplicantSearch = function(data) {
        return data["_phone"] || data["_id"] || data["_email"] || data["_SSN"] || data["_IDValue"];
    };
    CustomerManagement_PresentationController.prototype.getUsernameRulesAndPolicy = function() {
        var self = this;

        function successCallback(response) {
            var usernameRulesAndPolicy = {
                usernamerules: response.usernamerules,
                usernamepolicy: response.usernamepolicy
            };
            self.customerCreateModel.usernameRulesAndPolicy = usernameRulesAndPolicy;
            self.presentUserInterface('frmAssistedOnboarding', self.customerCreateModel);
            self.hideLoadingScreen();
        }

        function failureCallback(error) {
            self.showToastMessage(ErrorInterceptor.errorMessage(error), kony.i18n.getLocalizedString("i18n.frmGroupsController.error"));
            self.hideLoadingScreen();
        }
        self.businessController.getUsernameRulesAndPolicy({}, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.searchCustomersAssistiveOnBoarding = function(data, target) {
        var self = this;
        self.presentUserInterface('frmAssistedOnboarding', {
            "UserLoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            if (response.records.length === 0) {
                self.presentUserInterface('frmAssistedOnboarding', {
                    "UserLoadingScreen": {
                        focus: false
                    },
                    "userNameIsAvailable": true
                });
            } else {
                self.presentUserInterface('frmAssistedOnboarding', {
                    "UserLoadingScreen": {
                        focus: false
                    },
                    "userNameIsAvailable": false
                });
            }
        }

        function failureCallback(error) {
            self.presentUserInterface('frmAssistedOnboarding', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmAssistedOnboarding', {
                "toastModel": self.toastModel
            });
        }
        self.businessController.searchCustomers(data, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.customerSSNValidation = function(data, target) {
        var self = this;
        self.presentUserInterface('frmAssistedOnboarding', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            if (response.records.length === 0) {
                self.presentUserInterface('frmAssistedOnboarding', {
                    "LoadingScreen": {
                        focus: false
                    },
                    "isSSN": false
                });
            } else {
                self.presentUserInterface('frmAssistedOnboarding', {
                    "LoadingScreen": {
                        focus: false
                    },
                    "isSSN": true,
                    "data": response.records[0]
                });
            }
        }

        function failureCallback(error) {
            self.presentUserInterface('frmAssistedOnboarding', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmAssistedOnboarding', {
                "toastModel": self.toastModel
            });
        }
        self.businessController.searchCustomers(data, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.getCurrentCustomerBasicInfo = function() {
        return this.CustomerBasicInfo;
    };
    CustomerManagement_PresentationController.prototype.getCurrentCustomerType = function() {
        return this.CustomerBasicInfo.customer.CustomerType_id;
    };
    CustomerManagement_PresentationController.prototype.getCurrentCustomerDetails = function() {
        return this.CustomerBasicInfo.customer;
    };
    CustomerManagement_PresentationController.prototype.getCurrentApplicantDetails = function() {
        return this.ApplicantInfo.applicant;
    };
    CustomerManagement_PresentationController.prototype.getCurrentCustomerRequestAndNotificationCount = function() {
        return this.CustomerRequestAndNotificationCount;
    };
    CustomerManagement_PresentationController.prototype.getCurrentCustomerLockedOnInfo = function() {
        return this.LockedOnInfo;
    };
    CustomerManagement_PresentationController.prototype.setCurrentCustomerLockedOnInfo = function(LockedOnInfo) {
        this.LockedOnInfo = LockedOnInfo;
    };
    CustomerManagement_PresentationController.prototype.getCurrentCustomerEmailInformation = function() {
        var emails = this.CustomerContactInfo.Emails;
        var emailsList = [];
        var primaryEmail = null;
        if (emails) {
            for (var i = 0; i < emails.length; i++) {
                if (emails[i].isPrimary === "true") {
                    primaryEmail = emails[i].Value;
                }
                emailsList.push(emails[i].Value);
            }
        }
        return {
            "emails": emailsList,
            "primaryEmail": primaryEmail
        };
    };
    CustomerManagement_PresentationController.prototype.dismissLoadingIndicator = function() {
        var self = this;
        if (self.AsyncLoadingModel.isAsyncLoading) {
            self.AsyncLoadingModel.currentCTR++;
            if (self.AsyncLoadingModel.currentCTR === self.AsyncLoadingModel.expectedCTR) {
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "LoadingScreen": {
                        focus: false
                    }
                });
                self.AsyncLoadingModel.isAsyncLoading = false;
                self.AsyncLoadingModel.currentCTR = 0;
                self.AsyncLoadingModel.expectedCTR = 0;
            }
        } else {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
        }
    };
    CustomerManagement_PresentationController.prototype.alertMessageRequestOnClick = function() {
        this.presentUserInterface('frmCustomerProfileHelpCenter', {
            "LoadingScreen": {
                focus: false
            }
        });
        this.presentUserInterface('frmCustomerProfileHelpCenter', {
            "CustomerBasicInfo": this.CustomerBasicInfo
        });
        this.presentUserInterface('frmCustomerProfileHelpCenter', {
            "alertMessageRequestOnclick": true
        });
    };
    CustomerManagement_PresentationController.prototype.alertMessageNotificationOnClick = function() {
        this.presentUserInterface('frmCustomerProfileHelpCenter', {
            "LoadingScreen": {
                focus: false
            }
        });
        this.presentUserInterface('frmCustomerProfileHelpCenter', {
            "CustomerBasicInfo": this.CustomerBasicInfo
        });
        this.presentUserInterface('frmCustomerProfileHelpCenter', {
            "alertMessageNotoficationOnclick": true
        });
    };
    /**
     * @name getCustomerBasicInfo
     * @member CustomerManagementModule.presentationController
     * @param {Customer_username : string} data
     * @param string target
     * @param {flxCustMangSearch : string, flxCustMangSearchWrapper : string, flxFirstColumn : string, flxlastColoumn : string, lblContactNo : string, lblEmailId : string, lblHiddenCustomerId : string, lblName : string, lblSeperator : string, lblDOB : string, lblUserId : string, lblUserName : string, lblSSN : string, template : string} 
     */
    CustomerManagement_PresentationController.prototype.getCustomerBasicInfo = function(data, target, previousFormDetails) {
        var self = this;
        if (target === "InfoScreenProfile") {
            self.presentUserInterface('frmCustomerProfileContacts', {
                "LoadingScreen": {
                    focus: true
                }
            });
        } else {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: true
                }
            });
        }
        if (previousFormDetails && previousFormDetails.name) {
            self.sourceFormDetail.name = previousFormDetails.name;
        }
        if (previousFormDetails && previousFormDetails.data) {
            self.sourceFormDetail.data = previousFormDetails.data;
        }
        //to display toast message after lead creation and navigated to customer
        if (previousFormDetails && previousFormDetails.toast) {
            if (previousFormDetails.toast.status === "success") {
                self.presentUserInterface('frmCustomerProfileContacts', {
                    "toastModel": {
                        "status": "SUCCESS",
                        "message": previousFormDetails.toast.message
                    }
                });
            } else {
                self.presentUserInterface('frmCustomerProfileContacts', {
                    "toastModel": {
                        "status": "FAILURE",
                        "message": previousFormDetails.toast.message
                    }
                });
            }
        }

        function successCallback(response) {
            if (target === "InfoScreenProfile") {
                self.presentUserInterface('frmCustomerProfileContacts', {
                    "LoadingScreen": {
                        focus: false
                    }
                });
            } else {
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "LoadingScreen": {
                        focus: false
                    }
                });
            }
            self.CustomerBasicInfo.customer = response.customerbasicinfo_view;
            self.CustomerBasicInfo.configuration = response.Configuration;
            if (response.requestCount && response.notificationCount && (response.requestCount != 0 || response.notificationCount != 0)) {
                self.CustomerRequestAndNotificationCount = {
                    "requestCount": response.requestCount,
                    "notificationCount": response.notificationCount
                };
            } else {
                self.CustomerRequestAndNotificationCount = null;
            }
            self.CustomerBasicInfo.target = target;
            if (target === "InfoScreenPostEdit") {
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "LoadingScreen": {
                        focus: true
                    }
                });
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "CustomerBasicInfo": self.CustomerBasicInfo
                });
            } else {
                self.presentUserInterface('frmCustomerProfileContacts', {
                    "LoadingScreen": {
                        focus: false
                    }
                });
                self.presentUserInterface('frmCustomerProfileContacts', {
                    "CustomerBasicInfo": self.CustomerBasicInfo
                });
            }
        }

        function failureCallback(error) {
            kony.print("Failed to get customer basic info");
            if (target === "EditScreen") {
                self.presentUserInterface('frmCustomerProfileContacts', {
                    "LoadingScreen": {
                        focus: false
                    }
                });
                self.toastModel.message = ErrorInterceptor.errorMessage(error);
                self.toastModel.status = "FAILURE";
                self.toastModel.operation = "";
                self.presentUserInterface('frmCustomerProfileContacts', {
                    "toastModel": self.toastModel
                });
            } else {
                self.presentUserInterface('frmCustomerManagement', {
                    "LoadingScreen": {
                        focus: false
                    }
                });
                self.toastModel.message = ErrorInterceptor.errorMessage(error);
                self.toastModel.status = "FAILURE";
                self.toastModel.operation = "";
                self.presentUserInterface('frmCustomerManagement', {
                    "toastModel": self.toastModel
                });
            }
        }
        self.businessController.getBasicInfo(data, successCallback, failureCallback);
    };
    /*
     * function to call command handler to get all companies
     * @param: customerID
     */
    CustomerManagement_PresentationController.prototype.getAllCompanies = function() {
        var self = this;
        var payload = {
            "$orderby": "Name"
        };
        self.presentUserInterface(kony.application.getCurrentForm().id, {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.dismissLoadingIndicator();
            self.companies = response.organisation;
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "Companies": self.companies
            });
        }

        function failureCallback(error) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print(error.dbpErrMsg);
        }
        self.businessController.getAllCompanies(payload, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.getCurrentCustomerContactInfo = function() {
        return this.CustomerContactInfo;
    };
    /**
     * @name getCustomerContactInfo
     * @member CustomerManagementModule.presentationController
     * @param {customerID : string} data
     * @param string target
     * @param undefined subTarget
     */
    CustomerManagement_PresentationController.prototype.getCustomerContactInfo = function(data, target, subTarget) {
        var self = this;
        var payload = {
            "Customer_id": data.customerID
        };
        self.presentUserInterface(kony.application.getCurrentForm().id, {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.CustomerContactInfo.Addresses = response.Addresses;
            self.CustomerContactInfo.Emails = response.EmailIds;
            self.CustomerContactInfo.ContactNumbers = response.ContactNumbers;
            self.CustomerContactInfo.PreferredTimeAndMethod = response["PreferredTime&Method"];
            self.CustomerContactInfo.target = target;
            self.CustomerContactInfo.subTarget = subTarget;
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "CustomerContactInfo": self.CustomerContactInfo
            });
        }

        function failureCallback(error) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("Failed to get customer contact info" + dbpErrMsg);
        }
        self.businessController.getContactInfo(payload, successCallback, failureCallback);
    };
    /**
     * @name getCustomerNotes
     * @member CustomerManagementModule.presentationController
     * @param {customerID : string} data
     */
    CustomerManagement_PresentationController.prototype.getCustomerNotes = function(data) {
        var self = this;
        var payload = {
            "$filter": "Customer_id eq " + data.customerID,
            "$orderby": "createdts"
        };
        self.presentUserInterface(kony.application.getCurrentForm().id, {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.CustomerNotes = response.customernotes_view;
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "CustomerNotes": self.CustomerNotes
            });
        }

        function failureCallback(error) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("Failed to get customer notes");
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "GetCustomerNotes";
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "toastModel": self.toastModel
            });
        }
        self.businessController.getCustomerNotes(payload, successCallback, failureCallback);
    };
    /**
     * @name updateDBPUserStatus
     * @member CustomerManagementModule.presentationController
     * @param {customerID : string} data
     */
    CustomerManagement_PresentationController.prototype.updateDBPUserStatus = function(data) {
        var self = this;
        var payload = {
            "customerUsername": data.customerUsername,
            "status": data.status
        };
        self.presentUserInterface(kony.application.getCurrentForm().id, {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.updateStatusData(response.status);
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "UpdateDBPUserStatus": {
                    "status": response.status
                }
            });
        }

        function failureCallback(error) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("Failed to update user status");
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "toastModel": self.toastModel
            });
        }
        self.businessController.updateDBPUserStatus(payload, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.updateStatusData = function(status) {
            this.CustomerBasicInfo.customer.OLBCustomerFlags.Status = status;
        }
        /*
         * function to call command handler to get customer accounts specific alerts
         * @param: customerID or accountnumber
         */
    CustomerManagement_PresentationController.prototype.getAccountSpecificAlerts = function(data) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileAlerts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.AccountSpecificAlerts = response.accountSpecificAlerts;
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "AccountSpecificAlerts": self.AccountSpecificAlerts
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        self.businessController.getAccountSpecificAlerts(data, successCallback, failureCallback);
    };
    /**
     * @name getCustomerAccounts
     * @member CustomerManagementModule.presentationController
     * @param {CustomerUsername : string} data
     */
    CustomerManagement_PresentationController.prototype.getCustomerAccounts = function(data) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileAccounts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.CustomerAccounts = response.Accounts;
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "CustomerAccounts": self.CustomerAccounts
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "CustomerAccounts": []
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "Getaccounts";
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "toastModel": self.toastModel
            });
        }
        self.businessController.getCustomerAccounts(data, successCallback, failureCallback);
    };
    /**
     * @name getCustomerAccountInfo
     * @member CustomerManagementModule.presentationController
     * @param string accountID
     */
    CustomerManagement_PresentationController.prototype.getCustomerAccountInfo = function(accountID) {
        return this.CustomerAccounts.filter(function(x) {
            return x.accountID === accountID;
        });
    };
    /**
     * @name getAccountTransactions
     * @member CustomerManagementModule.presentationController
     * @param {AccountNumber : string, StartDate : string, EndDate : string} data
     */
    CustomerManagement_PresentationController.prototype.getAccountTransactions = function(data) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileAccounts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.AccountTrasactions = response.Transactions;
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "AccountTrasactions": self.AccountTrasactions
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "toastModel": self.toastModel
            });
        }
        self.businessController.getCustomerTransactions(data, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.getCurrentCustomerGroups = function() {
        return this.CustomerGroups;
    };
    /**
     * @name getCustomerGroups
     * @member CustomerManagementModule.presentationController
     * @param {customerID : string} data
     * @param string target
     */
    CustomerManagement_PresentationController.prototype.getCustomerGroups = function(data, target) {
        var self = this;
        var payload = {
            "$filter": "Customer_id eq " + data.customerID
        };
        self.presentUserInterface('frmCustomerProfileRoles', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileRoles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.CustomerGroups.AssignedGroups = response.customergroupinfo_view;
            self.CustomerGroups.AllGroups = self.AllGroups;
            self.CustomerGroups.target = target;
            self.presentUserInterface('frmCustomerProfileRoles', {
                "CustomerGroups": self.CustomerGroups
            });
        }

        function failureCallback(response) {
            self.presentUserInterface('frmCustomerProfileRoles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("Failed to get customer groups");
        }
        self.businessController.getCustomerGroups(payload, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.getCurrentCustomerEntitlements = function() {
        return this.CustomerEntitlements;
    };
    /*
     * function to call command handler to get customer entitlements
     * @param: customerID
     */
    CustomerManagement_PresentationController.prototype.getAllEntitlements = function() {
        var self = this;

        function successCallback(response) {
            self.AllEntitlements = response.service;
        }

        function failureCallback(response) {
            kony.print("Failed to get all entitlements");
        }
        if (!self.AllEntitlements) {
            self.businessController.getAllEntitlements({}, successCallback, failureCallback);
        }
    };
    /**
     * @name getCustomerEntitlements
     * @member CustomerManagementModule.presentationController
     * @param {customerID : string} data
     * @param string target
     */
    CustomerManagement_PresentationController.prototype.getCustomerEntitlements = function(data, target) {
        var self = this;
        var payload = {
            "$filter": "Customer_id eq " + data.customerID
        };
        self.presentUserInterface('frmCustomerProfileEntitlements', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileEntitlements', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.CustomerEntitlements.AssignedEntitlements = response.customerpermissions_view;
            self.CustomerEntitlements.AllEntitlements = self.AllEntitlements;
            self.CustomerEntitlements.CustomerIndirectEntitlements = self.CustomerIndirectEntitlements;
            self.CustomerEntitlements.target = target;
            self.presentUserInterface('frmCustomerProfileEntitlements', {
                "CustomerEntitlements": self.CustomerEntitlements
            });
        }

        function failureCallback(response) {
            self.presentUserInterface('frmCustomerProfileEntitlements', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("Failed to get customer entitlements");
        }
        self.businessController.getCustomerEntitlements(payload, successCallback, failureCallback);
    };
    /**
     * @name getCustomerIndirectEntitlements
     * @member CustomerManagementModule.presentationController
     * @param {customerID : string} data
     * @param string target
     */
    CustomerManagement_PresentationController.prototype.getCustomerIndirectEntitlements = function(data, target) {
        var self = this;
        var payload = {
            "$filter": "Customer_id eq " + data.customerID
        };
        self.presentUserInterface('frmCustomerProfileEntitlements', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileEntitlements', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.CustomerIndirectEntitlements = response.customer_indirect_permissions_view;
            self.getCustomerEntitlements({
                "customerID": data.customerID
            }, target);
        }

        function failureCallback(response) {
            self.presentUserInterface('frmCustomerProfileEntitlements', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("Failed to get customer entitlements");
            self.getCustomerEntitlements({
                "customerID": data.customerID
            }, target);
        }
        self.businessController.getCustomerIndirectEntitlements(payload, successCallback, failureCallback);
    };
    /**
     * @name getAllGroups
     * @member CustomerManagementModule.presentationController
     * 
     */
    CustomerManagement_PresentationController.prototype.getAllGroups = function() {
        var self = this;
        self.presentUserInterface(kony.application.getCurrentForm().id, {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.AllGroups = response.membergroup;
            self.dismissLoadingIndicator();
        }

        function failureCallback(response) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("Failed to get all groups");
        }
        self.businessController.getAllGroups({}, successCallback, failureCallback);
    };
    /*
     * function to call command handler to get status group
     * 
     */
    CustomerManagement_PresentationController.prototype.getStatusGroup = function(data) {
        var self = this;
        if (self.StatusGroup && self.StatusGroup.length > 0) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "StatusGroup": self.StatusGroup
            });
        } else {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: true
                }
            });

            function successCallback(response) {
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "LoadingScreen": {
                        focus: false
                    }
                });
                self.StatusGroup = response.status;
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "StatusGroup": self.StatusGroup
                });
            }

            function failureCallback(response) {
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "LoadingScreen": {
                        focus: false
                    }
                });
                kony.print("Failed to get status group");
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "StatusGroup": []
                });
            }
            self.businessController.getStatusGroup({}, successCallback, failureCallback);
        }
    };
    /**
     * @name createNote
     * @member CustomerManagementModule.presentationController
     * @param {Customer_id : string, Note : string, Internal_username : string} data
     * @param string target
     */
    CustomerManagement_PresentationController.prototype.createNote = function(data, target) {
        var self = this;
        self.presentUserInterface(kony.application.getCurrentForm().id, {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(status) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = "Customer note created successfully.";
            self.toastModel.status = "SUCCESS";
            self.toastModel.operation = "CreateNote";
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "toastModel": self.toastModel
            });
            self.getCustomerNotes({
                "customerID": data.Customer_id
            });
        }

        function failureCallback(error) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "CreateNote";
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "toastModel": self.toastModel
            });
            self.getCustomerNotes({
                "customerID": data.Customer_id
            });
        }
        self.businessController.createNote(data, successCallback, failureCallback);
    };
    /*
     * function to call command handler to edit customer basic info
     * @param: Edit data
     */
    CustomerManagement_PresentationController.prototype.editCustomerBasicInfo = function(data) {
        var self = this;
        self.presentUserInterface(kony.application.getCurrentForm().id, {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(status) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = "Customer profile edit successful. ";
            self.toastModel.status = "SUCCESS";
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "toastModel": self.toastModel
            });
            self.getCustomerBasicInfo({
                "Customer_id": data.Customer_id
            }, "InfoScreenPostEdit");
        }

        function failureCallback(error) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "toastModel": self.toastModel
            });
            self.getCustomerBasicInfo({
                "Customer_id": data.Customer_id
            }, "InfoScreenPostEdit");
        }
        self.businessController.editCustomerBasicInfo(data, successCallback, failureCallback);
    };
    /*
     * function to call command handler to edit customer groups
     * @param: Edit data
     */
    CustomerManagement_PresentationController.prototype.editCustomerGroups = function(data) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileRoles', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(status) {
            self.presentUserInterface('frmCustomerProfileRoles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.editCustomerRoleSuccess");
            self.toastModel.status = "SUCCESS";
            self.toastModel.operation = "";
            self.presentUserInterface('frmCustomerProfileRoles', {
                "toastModel": self.toastModel
            });
            self.getCustomerGroups({
                "customerID": data.Customer_id
            }, "InfoScreen");
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileRoles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmCustomerProfileRoles', {
                "toastModel": self.toastModel
            });
            self.getCustomerGroups({
                "customerID": data.Customer_id
            }, "InfoScreen");
        }
        self.businessController.editCustomerGroups(data, successCallback, failureCallback);
    };
    /*
     * function to call command handler to edit customer entitlements
     * @param: Edit data
     */
    CustomerManagement_PresentationController.prototype.editCustomerEntitlements = function(data) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileEntitlements', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(status) {
            self.presentUserInterface('frmCustomerProfileEntitlements', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.editCustomerPermissionSuccess");
            self.toastModel.status = "SUCCESS";
            self.presentUserInterface('frmCustomerProfileEntitlements', {
                "toastModel": self.toastModel
            });
            self.getCustomerEntitlements({
                "customerID": data.Customer_id
            }, "InfoScreen");
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileEntitlements', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.presentUserInterface('frmCustomerProfileEntitlements', {
                "toastModel": self.toastModel
            });
            self.getCustomerEntitlements({
                "customerID": data.Customer_id
            }, "InfoScreen");
        }
        self.businessController.editCustomerEntitlements(data, successCallback, failureCallback);
    };
    /**
     * @name editCustomerContactInfo
     * @member CustomerManagementModule.presentationController
     * @param {ModifiedByName : string, Customer_id : string, PhoneNumbers : [{value : object, isPrimary : object, Extension : object, id : object}]} data
     */
    CustomerManagement_PresentationController.prototype.editCustomerContactInfo = function(data) {
        var self = this;
        self.presentUserInterface(kony.application.getCurrentForm().id, {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(status) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = "Edit customer contact info successful.";
            self.toastModel.status = "SUCCESS";
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "toastModel": self.toastModel
            });
            self.getCustomerContactInfo({
                "customerID": data.Customer_id
            }, "InfoScreen");
        }

        function failureCallback(error) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "toastModel": self.toastModel
            });
            self.getCustomerContactInfo({
                "customerID": data.Customer_id
            }, "InfoScreen");
        }
        self.businessController.editCustomerContactInfo(data, successCallback, failureCallback);
    };
    /*
     * function to call command handler to enroll a customer
     * @param: enroll data
     */
    CustomerManagement_PresentationController.prototype.enrollACustomer = function(data) {
        var self = this;
        self.presentUserInterface(kony.application.getCurrentForm().id, {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(status) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.EnrollmentLinkSent");
            self.toastModel.status = "SUCCESS";
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "toastModel": self.toastModel
            });
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "enrollACustomer": true
            });
        }

        function failureCallback(error) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "toastModel": self.toastModel
            });
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "enrollACustomer": true
            });
        }
        self.businessController.enrollCustomer(data, successCallback, failureCallback);
    };
    /**
     * @name getCitiesStatesAndCountries
     * @member CustomerManagementModule.presentationController
     * 
     */
    CustomerManagement_PresentationController.prototype.getCitiesStatesAndCountries = function() {
        var self = this;
        if (self.AddressModel.countries.length === 0 || self.AddressModel.states.length === 0 || self.AddressModel.cities.length === 0) {
            var getCountryList = Promisify(self.businessController, 'getCountryList');
            var getRegionList = Promisify(self.businessController, 'getRegionList');
            var getCityList = Promisify(self.businessController, 'getCityList');
            Promise.all([
                getCountryList({}),
                getRegionList({}),
                getCityList({})
            ]).then(function(responses) {
                self.AddressModel.countries = responses[0];
                self.AddressModel.states = responses[1];
                self.AddressModel.cities = responses[2];
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "AddressModel": self.AddressModel
                });
                self.dismissLoadingIndicator();
            }).catch(function failureCallback(error) {
                kony.print("Failed to get country, region & city list");
                self.toastModel.message = "Failed to get country, region & city lists and " + ErrorInterceptor.errorMessage(error);
                self.toastModel.status = "FAILURE";
                self.toastModel.operation = "";
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "toastModel": self.toastModel
                });
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "LoadingScreen": {
                        focus: false
                    }
                });
            });
        } else {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "AddressModel": self.AddressModel
            });
            self.dismissLoadingIndicator();
        }
    };
    CustomerManagement_PresentationController.prototype.getSpecifiedCitiesAndStates = function(addressSelection, addressId, target) {
        var self = this;
        if (addressSelection === "country") {
            var states = [];
            states.push(["lbl1", "Select a State"]);
            for (var i = 0; i < Object.keys(self.AddressModel.states).length; ++i) {
                if (self.AddressModel.states[i].Country_id === addressId) {
                    states.push([self.AddressModel.states[i].id, self.AddressModel.states[i].Name]);
                }
            }
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "targetAddressModel": {
                    "states": states,
                    "target": target
                }
            });
        } else if (addressSelection === "state") {
            var cities = [];
            cities.push(["lbl1", "Select a City"]);
            for (var j = 0; j < Object.keys(self.AddressModel.cities).length; ++j) {
                if (self.AddressModel.cities[j].Region_id === addressId) {
                    cities.push([self.AddressModel.cities[j].id, self.AddressModel.cities[j].Name]);
                }
            }
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "targetAddressModel": {
                    "cities": cities,
                    "target": target
                }
            });
        }
    };
    /**
     * @name getCustomerRequests
     * @member CustomerManagementModule.presentationController
     * @param {customerID : string} data
     */
    CustomerManagement_PresentationController.prototype.getCustomerRequests = function(data) {
        var self = this;
        var payload = {
            "username": data.username
        };
        self.presentUserInterface('frmCustomerProfileHelpCenter', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.CustomerRequests = response.customerrequests_view;
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "CustomerRequests": self.CustomerRequests
            });
        }

        function failureCallback(response) {
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("Failed to get customer requests");
        }
        self.businessController.getCustomerRequests(payload, successCallback, failureCallback);
    };
    /**
     * @name getCustomerNotifications
     * @member CustomerManagementModule.presentationController
     * @param {customerID : string} data
     */
    CustomerManagement_PresentationController.prototype.getCustomerNotifications = function(data) {
        var self = this;
        var payload = {
            "$filter": "customer_Id eq " + data.customerID
        };
        self.presentUserInterface('frmCustomerProfileAlerts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.CustomerNotifications = response.customernotifications_view;
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "CustomerNotifications": self.CustomerNotifications
            });
        }

        function failureCallback(response) {
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("Failed to get customer notifications");
        }
        self.businessController.getCustomerNotifications(payload, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.getAlertCategories = function(customerId) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileAlerts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onSuccess(response) {
            var context = {
                "AlertCategoryId": response.records[0].alertcategory_id,
                "CustomerId": customerId
            };
            self.prefData.categories = response;
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.getAlertCategoryPref(context);
        }

        function onError() {
            //TODO : show error on screen
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        this.businessController.getAlertCategories({}, onSuccess, onError);
    };
    CustomerManagement_PresentationController.prototype.getAlertCategoryPref = function(context) {
        var self = this;
        self.prefData.AlertCategoryId = context.AlertCategoryId;
        self.presentUserInterface('frmCustomerProfileAlerts', {
            "LoadingScreen": {
                focus: true
            }
        });
        var promiseFetchAlertCategoryPref = Promisify(self.businessController, 'getCustomerAlertCategoryPreference');
        var promiseFetchAlertCategoryChannelPref = Promisify(self.businessController, 'getCustomerAlertCategoryChannelPreference');
        var promiseFetchAlertTypePref = Promisify(self.businessController, 'getCustomerAlertTypePreference');
        Promise.all([
            promiseFetchAlertCategoryPref(context),
            promiseFetchAlertCategoryChannelPref(context),
            promiseFetchAlertTypePref(context),
        ]).then(function(responses) {
            self.prefData.catPref = responses[0];
            self.prefData.channelPref = responses[1];
            self.prefData.typePref = responses[2];
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "prefData": self.prefData
            });
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "prefData": prefData
            });
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }).catch(function(error) {
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            console.log("ERROR" + error);
        });
    };
    CustomerManagement_PresentationController.prototype.setAlertPreferences = function(alertsData) {
        var self = this;
        var params = {
            AlertCategoryId: alertsData.AlertCategoryId,
            AccountId: null,
            CustomerId: alertsData.CustomerId,
            isSubscribed: alertsData.isSubscribed,
            channelPreference: alertsData.channelPreference,
            typePreference: alertsData.typePreference
        };
        self.presentUserInterface('frmCustomerProfileAlerts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            var context = {
                "AlertCategoryId": alertsData.AlertCategoryId,
                "CustomerId": alertsData.CustomerId
            };
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = "Preferences updated successfully";
            self.toastModel.status = "SUCCESS";
            self.toastModel.operation = "setAlertPreferences";
            self.toastModel.context = alertsData;
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "toastModel": self.toastModel
            });
            self.getAlertCategoryPref(context);
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "resetPassword";
            self.toastModel.context = data;
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "toastModel": self.toastModel
            });
        }
        self.businessController.setAlertPreferences(params, successCallback, failureCallback);
    };
    /**
     * @name getCustomerNotifications
     * @member CustomerManagementModule.presentationController
     * @param {customerID : string} data
     */
    CustomerManagement_PresentationController.prototype.getCustomerAlertHistory = function(data) {
        var self = this;
        var payload = {
            "CustomerId": data.CustomerId
        };
        self.presentUserInterface('frmCustomerProfileAlerts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.alertHistory = response.alertHistory;
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "alertHistory": self.alertHistory
            });
        }

        function failureCallback(response) {
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("Failed to get customer notifications");
        }
        self.businessController.getCustomerAlertHistory(payload, successCallback, failureCallback);
    };
    /**
     * @name updateCustomerLockstatus
     * @member CustomerManagementModule.presentationController
     * @param {customerUsername : string} data
     */
    CustomerManagement_PresentationController.prototype.updateCustomerLockstatus = function(data, onSuccess) {
        var self = this;
        var payload = {
            "customerUsername": data.customerUsername
        };
        self.presentUserInterface('frmCustomerProfileContacts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            if (onSuccess) {
                onSuccess(response);
            }
            self.presentUserInterface('frmCustomerProfileContacts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.CoreBankingUpdate = response.CoreBankingUpdate;
            self.presentUserInterface('frmCustomerProfileContacts', {
                "CoreBankingUpdate": self.CoreBankingUpdate
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileContacts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmCustomerProfileContacts', {
                "toastModel": self.toastModel
            });
            kony.print("Failed to update customer lock status");
        }
        self.businessController.updateCustomerLockstatus(payload, successCallback, failureCallback);
    };
    /**
     * @name authorizeCSRAssist
     * @member CustomerManagementModule.presentationController
     * @param {customerid : string, customerUsername : string} data
     */
    CustomerManagement_PresentationController.prototype.authorizeCSRAssist = function(data) {
        var self = this;

        function authCallback(response) {
            if (response.opstatus === 0 && !response.dbpErrCode) {
                self.OnlineBankingLogin = {
                    "BankingURL": response.BankingURL,
                    "currentAction": data.currentAction
                };
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "OnlineBankingLogin": self.OnlineBankingLogin
                });
            } else if (response.dbpErrCode === 20932) {
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "OnlineBankingLogin": "USER_LOCKED"
                });
            } else {
                self.presentUserInterface(kony.application.getCurrentForm().id, {
                    "OnlineBankingLogin": "FAILED"
                });
            }
        }
        if (data.currentAction === "OLB") {
            self.businessController.CSRAssistAuthorization(data, authCallback, authCallback);
        } else if (data.currentAction === "SIMULATE_PERSONAL_LOAN") {
            self.businessController.CSRAssistAuthorizationLearnPersonalLoan(data, authCallback, authCallback);
        } else if (data.currentAction === "SIMULATE_VEHICLE_LOAN") {
            self.businessController.CSRAssistAuthorizationLearnVehicleLoan(data, authCallback, authCallback);
        } else if (data.currentAction === "SIMULATE_CREDIT_LOAN") {
            self.businessController.CSRAssistAuthorizationLearnCreditLoan(data, authCallback, authCallback);
        } else if (data.currentAction === "APPLY_CREDIT_LOAN") {
            self.businessController.CSRAssistAuthorizationApplyCreditLoan(data, authCallback, authCallback);
        } else if (data.currentAction === "APPLY_PERSONAL_LOAN") {
            self.businessController.CSRAssistAuthorizationApplyPersonalLoan(data, authCallback, authCallback);
        } else if (data.currentAction === "APPLY_VEHICLE_LOAN") {
            self.businessController.CSRAssistAuthorizationApplyVehicleLoan(data, authCallback, authCallback);
        } else if (data.currentAction === "RESUME_LOAN") {
            self.businessController.CSRAssistAuthorizationResumeLoan(data, authCallback, authCallback);
        } else if (data.currentAction === "CREATE_APPLICANT") {
            self.businessController.CSRAssistAuthorizationCreateApplicant(data, authCallback, authCallback);
        }
    };
    /**
     * @name CSRAssistLogCloseEvent
     * @member CustomerManagementModule.presentationController
     * @param reset data
     */
    CustomerManagement_PresentationController.prototype.CSRAssistLogCloseEvent = function(data) {
        var self = this;
        var payload = {
            "customerid": data.customerid,
            "customerUsername": data.customerUsername
        };

        function callback() {}
        self.businessController.CSRAssistLogCloseEvent(payload, callback, callback);
    };
    /**
     * @name sendResetPasswordLink
     * @member CustomerManagementModule.presentationController
     * @param reset data
     */
    CustomerManagement_PresentationController.prototype.sendResetPasswordLink = function(data) {
        var self = this;
        self.presentUserInterface(kony.application.getCurrentForm().id, {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(status) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.PasswordResetLinkSent");
            self.toastModel.status = "SUCCESS";
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "toastModel": self.toastModel
            });
        }

        function failureCallback(error) {
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.presentUserInterface(kony.application.getCurrentForm().id, {
                "toastModel": self.toastModel
            });
        }
        self.businessController.sendResetPasswordLink(data, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.getCurrentCustomerRequests = function() {
        return this.CustomerRequests;
    };
    CustomerManagement_PresentationController.prototype.getCurrentCustomerRequest = function(requestId) {
        for (var i = 0; i < this.CustomerRequests.length; i++) {
            if (this.CustomerRequests[i].id === requestId) {
                return this.CustomerRequests[i];
            }
        }
        return null;
    };
    /**
     * @name showMessageModule
     * @member CustomerManagementModule.presentationController
     * @param {requestID : string} requestIdParam
     * @param string status
     * @param string requestId
     * @param string subject
     */
    CustomerManagement_PresentationController.prototype.showMessageModule = function(requestIdParam, customPayload) {
        this.navigateTo('CSRModule', 'fetchAllCategories', [null, {
            "requestIdParam": requestIdParam,
            "customPayload": customPayload
        }]);
    };
    /**
     * @name navigateToCustomerLogs
     * @member CustomerManagementModule.presentationController
     * @param {CustomerManagementRequest : {Username : string, CustomerSegmentDetails : {flxCustMangSearch : object, flxCustMangSearchWrapper : object, flxFirstColumn : object, flxlastColoumn : object, lblContactNo : object, lblEmailId : object, lblHiddenCustomerId : object, lblName : object, lblSeperator : object, lblDOB : object, lblUserId : object, lblUserName : object, lblSSN : object, template : object}}} param
     */
    CustomerManagement_PresentationController.prototype.navigateToCustomerLogs = function(param) {
        this.navigateTo('LogsModule', 'showLogs', [param]);
    };
    /**
     * @name getLastNCustomerSessions
     * @member CustomerManagementModule.presentationController
     * @param {customerUsername : string} data
     */
    CustomerManagement_PresentationController.prototype.getLastNCustomerSessions = function(data) {
        var self = this;
        var payload = {
            "username": data.customerUsername,
            "sessionCount": "4"
        };
        self.presentUserInterface('frmCustomerProfileActivityHistory', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileActivityHistory', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.CustomerSessions = response.sessions;
            var onGetModulesOnSuccess = function(response) {
                var modulesMap = {};
                response.eventtype.forEach(function(ele) {
                    modulesMap[ele.id] = ele.Name;
                });
                self.ActivityHistory.modulesMap = modulesMap;
            }
            var onGetModulesOnError = function(error) {
                self.presentUserInterface('frmCustomerProfileActivityHistory', {
                    "LoadingScreen": {
                        focus: false
                    }
                });
                self.toastModel.message = ErrorInterceptor.errorMessage(error);
                self.toastModel.status = "FAILURE";
                self.toastModel.operation = "";
                self.presentUserInterface('frmCustomerProfileActivityHistory', {
                    "toastModel": self.toastModel
                });
            }
            var logsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LogsModule");
            if (!self.ActivityHistory.modulesMap) {
                logsModule.businessController.getModules({}, onGetModulesOnSuccess, onGetModulesOnError);
            }
            var onGetActivitiesOnSuccess = function(response) {
                var activityMap = {};
                response.eventsubtype.forEach(function(ele) {
                    activityMap[ele.id] = ele.Name;
                });
                self.ActivityHistory.activityMap = activityMap;
                self.presentUserInterface('frmCustomerProfileActivityHistory', {
                    "CustomerSessions": self.CustomerSessions
                });
            }
            var onGetActivitiesOnError = function(error) {
                self.presentUserInterface('frmCustomerProfileActivityHistory', {
                    "LoadingScreen": {
                        focus: false
                    }
                });
                self.toastModel.message = ErrorInterceptor.errorMessage(error);
                self.toastModel.status = "FAILURE";
                self.toastModel.operation = "";
                self.presentUserInterface('frmCustomerProfileActivityHistory', {
                    "toastModel": self.toastModel
                });
            }
            if (!self.ActivityHistory.activityMap) {
                logsModule.businessController.getActivityType({}, onGetActivitiesOnSuccess, onGetActivitiesOnError);
            } else {
                self.presentUserInterface('frmCustomerProfileActivityHistory', {
                    "CustomerSessions": self.CustomerSessions
                });
            }
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileActivityHistory', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmCustomerProfileActivityHistory', {
                "toastModel": self.toastModel
            });
            kony.print("Failed to get the customer activity sessions");
        }
        self.businessController.getLastNCustomerSessions(payload, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.getModuleNameFromModuleID = function(moduleID) {
        if (this.ActivityHistory.modulesMap[moduleID]) {
            return this.ActivityHistory.modulesMap[moduleID];
        } else {
            return moduleID;
        }
    };
    CustomerManagement_PresentationController.prototype.getActivityNameFromActivityID = function(activityID) {
        if (this.ActivityHistory.activityMap[activityID]) {
            return this.ActivityHistory.activityMap[activityID];
        } else {
            return activityID;
        }
    };
    /**
     * @name getAllActivitiesInACustomerSession
     * @member CustomerManagementModule.presentationController
     * @param {sessionId : string} data
     */
    CustomerManagement_PresentationController.prototype.getAllActivitiesInACustomerSession = function(data) {
        var self = this;
        var payload = {
            "sessionId": data.sessionId
        }
        self.presentUserInterface('frmCustomerProfileActivityHistory', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileActivityHistory', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.CustomerSessionActivities = response.activities;
            self.presentUserInterface('frmCustomerProfileActivityHistory', {
                "CustomerSessionActivities": self.CustomerSessionActivities
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileActivityHistory', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmCustomerProfileActivityHistory', {
                "toastModel": self.toastModel
            });
            kony.print("Failed to get the customer activities");
        }
        self.businessController.getAllActivitiesInACustomerSession(payload, successCallback, failureCallback);
    };
    /**
     * @name getCustomerDevices
     * @member CustomerManagementModule.presentationController
     * @param {$filter : string} data
     */
    CustomerManagement_PresentationController.prototype.getCustomerDevices = function(data) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileDeviceInfo', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileDeviceInfo', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.CustomerDevices = response.customer_device_information_view;
            self.presentUserInterface('frmCustomerProfileDeviceInfo', {
                "CustomerDevices": self.CustomerDevices
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileDeviceInfo', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmCustomerProfileDeviceInfo', {
                "toastModel": self.toastModel
            });
            kony.print("Failed to get the customer activities");
        }
        self.businessController.getCustomerDevices(data, successCallback, failureCallback);
    };
    /**
     * @name customerUpdateDeviceInformation
     * @member CustomerManagementModule.presentationController
     * @param {Device_id : string, Customer_id : string, Status_id : string} data
     */
    CustomerManagement_PresentationController.prototype.customerUpdateDeviceInformation = function(data) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileDeviceInfo', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(status) {
            self.presentUserInterface('frmCustomerProfileDeviceInfo', {
                "LoadingScreen": {
                    focus: false
                }
            });
            if (data.Status_id === "SID_DEVICE_INACTIVE") {
                self.toastModel.message = "Device deactivated successfully";
            } else {
                self.toastModel.message = "Device deregistered successfully";
            }
            self.toastModel.status = "SUCCESS";
            self.toastModel.operation = "";
            self.presentUserInterface('frmCustomerProfileDeviceInfo', {
                "toastModel": self.toastModel
            });
            self.getCustomerDevices({
                "$filter": "Customer_id eq " + data.Customer_id
            });
        }

        function failureCallback(status) {
            self.presentUserInterface('frmCustomerProfileDeviceInfo', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(status);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmCustomerProfileDeviceInfo', {
                "toastModel": self.toastModel
            });
            kony.print("Failed to update the record ");
        }
        self.businessController.customerUpdateDeviceInformation(data, successCallback, failureCallback);
    };
    /*
     * function to call command handler to update e-Statement status (whether Paper or e-Statement)
     */
    CustomerManagement_PresentationController.prototype.updateEstatementStatus = function(eStatementJSON) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileAccounts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "eStatementUpdate": response
            });
        }

        function failureCallback(response) {
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "eStatementUpdateStatus": "failure"
            });
        }
        self.businessController.updateEstatementStatus(eStatementJSON, successCallback, failureCallback);
    };
    /**
     * @name getAlertPrefrences
     * @member CustomerManagementModule.presentationController
     * @param {userName : string} userName
     */
    CustomerManagement_PresentationController.prototype.getAlertPrefrences = function(userName) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileAlerts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            var alertPreferences = {
                alertTypes: []
            };
            var c = 0;
            for (var i = 0; i < response.alertTypes.length; ++i) {
                if (response.alertTypes[i].alertType !== "Account") {
                    alertPreferences.alertTypes[c++] = response.alertTypes[i];
                }
            }
            self.AlertPrefrences = alertPreferences;
            self.presentUserInterface('frmCustomerProfileAlerts', {
                "AlertPrefrences": self.AlertPrefrences
            });
        }

        function failureCallback(response) {}
        self.businessController.getAlertPrefrences(userName, successCallback, failureCallback);
    };
    /**
     * @name getCardsInformation
     * @member CustomerManagementModule.presentationController
     * @param {customerUsername : string} data
     */
    CustomerManagement_PresentationController.prototype.getCardsInformation = function(data) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileAccounts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "cardsInfomartion": response
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "toastModel": self.toastModel
            });
            kony.print("Failed to get the customer activities");
        }
        self.businessController.getCardsInformation(data, successCallback, failureCallback);
    };
    /**
     * @name getCustomerRequestAndNotificationCount
     * @member CustomerManagementModule.presentationController
     * @param {customerUsername : string} data
     */
    CustomerManagement_PresentationController.prototype.getCustomerRequestAndNotificationCount = function(data) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileContacts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileContacts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            if (response.requestCount != 0 || response.notificationCount != 0) {
                self.CustomerRequestAndNotificationCount = {
                    "requestCount": response.requestCount,
                    "notificationCount": response.notificationCount
                };
            }
            self.presentUserInterface('frmCustomerProfileContacts', {
                "CustomerRequestAndNotificationCount": self.CustomerRequestAndNotificationCount
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileContacts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmCustomerProfileContacts', {
                "toastModel": self.toastModel
            });
            kony.print("Failed to get the customer request and notification count");
        }
        self.businessController.getCustomerRequestAndNotificationCount(data, successCallback, failureCallback);
    };
    /**
     * @name getTravelNotifications
     * @member CustomerManagementModule.presentationController
     * @param {Username : string} reqParam
     */
    CustomerManagement_PresentationController.prototype.getTravelNotifications = function(reqParam) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileHelpCenter', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "LoadingScreen": {
                    focus: false
                }
            });
            var notificationResponse = null;
            if (response.TravelRequests) {
                notificationResponse = response;
                for (var i = 0; i < notificationResponse.TravelRequests.length; i++) {
                    notificationResponse.TravelRequests[i].notificationType = "Travel Notification";
                }
            } else {
                notificationResponse = {
                    TravelRequests: []
                };
            }
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "TravelNotifications": notificationResponse
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel = {
                message: ErrorInterceptor.errorMessage(error),
                status: "FAILURE",
                operation: ""
            };
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "toastModel": self.toastModel
            });
            kony.print("Failed to get the customer travel notifications");
        }
        self.businessController.getTravelNotifications(reqParam, successCallback, failureCallback);
    };
    /**
     * @name getCardRequests
     * @member CustomerManagementModule.presentationController
     * @param {Username : string} reqParam
     */
    CustomerManagement_PresentationController.prototype.getCardRequests = function(reqParam) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileHelpCenter', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "CardRequests": response
            });
        }

        function failureCallback(response) {
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel = {
                message: ErrorInterceptor.errorMessage(response),
                status: "FAILURE",
                operation: ""
            };
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "toastModel": self.toastModel
            });
            kony.print("Failed to get the customer card requests");
        }
        self.businessController.getCardRequests(reqParam, successCallback, failureCallback);
    };
    /**
     * @name updateCardStatus
     * @member CustomerManagementModule.presentationController
     * @param {customerUsername : string, cardNumber : string, cardAction : string, actionReason : string} reqParam
     */
    CustomerManagement_PresentationController.prototype.updateCardStatus = function(reqParam) {
        var self = this;
        self.presentUserInterface('frmCustomerProfileAccounts', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "UpdateCardRequests": response
            });
        }

        function failureCallback(response) {
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel = {
                message: ErrorInterceptor.errorMessage(error),
                status: "FAILURE",
                operation: ""
            };
            self.presentUserInterface('frmCustomerProfileAccounts', {
                "toastModel": self.toastModel
            });
            kony.print("Failed to get the customer card requests");
        }
        self.businessController.updateCardsInformation(reqParam, successCallback, failureCallback);
    };
    /**
     * @name cancelNotification
     * @member CustomerManagementModule.presentationController
     * @param {notificationId : string, username : string} reqParam
     */
    CustomerManagement_PresentationController.prototype.cancelNotification = function(reqParam) {
        var self = this;
        var requestParam = {
            "request_id": reqParam.notificationId,
            "Username": reqParam.username
        };
        self.presentUserInterface('frmCustomerProfileHelpCenter', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(status) {
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel = {
                message: "Travel Notification successfully cancelled",
                status: "SUCCESS",
                operation: ""
            };
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "toastModel": self.toastModel
            });
            self.getTravelNotifications({
                "Username": reqParam.username
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel = {
                message: ErrorInterceptor.errorMessage(error),
                status: "FAILURE",
                operation: ""
            };
            self.presentUserInterface('frmCustomerProfileHelpCenter', {
                "toastModel": self.toastModel
            });
            kony.print("Failed to cancel notification");
        }
        self.businessController.cancelTravelNotification(requestParam, successCallback, failureCallback);
    };
    /**
     * @name showCustomerManagementSearchResults
     * @member CustomerManagementModule.presentationController
     * 
     */
    CustomerManagement_PresentationController.prototype.showCustomerManagementSearchResults = function() {
        var self = this;
        self.presentUserInterface('frmCustomerManagement', {
            "showCustomerSearch": true
        });
        self.presentUserInterface('frmCustomerManagement', {
            "showCustomerSearchResults": self.searchModel
        });
    };
    CustomerManagement_PresentationController.prototype.getSearchInputs = function() {
        var self = this;
        self.presentUserInterface('frmCustomerManagement', {
            "searchInputs": self.searchInputs
        });
    };
    CustomerManagement_PresentationController.prototype.showAssistedOnboarding = function(eligibityID) {
        var self = this;
        self.presentUserInterface('frmAssistedOnboarding', {
            "firstTime": true,
            id: eligibityID
        });
        self.presentUserInterface('frmAssistedOnboarding', {
            "LoadingScreen": {
                focus: true
            }
        });
        self.businessController.getIdTypes({}, function onSuccess(response) {
            self.presentUserInterface('frmAssistedOnboarding', {
                idInfos: response.idtype,
            });
            self.presentUserInterface('frmAssistedOnboarding', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }, function onError(error) {
            self.toastModel = {
                message: ErrorInterceptor.errorMessage(error),
                status: "FAILURE",
                operation: ""
            };
            self.presentUserInterface('frmAssistedOnboarding', {
                "toastModel": self.toastModel
            });
            self.presentUserInterface('frmAssistedOnboarding', {
                "LoadingScreen": {
                    focus: false
                }
            });
        });
    };
    CustomerManagement_PresentationController.prototype.getAddressSuggestion = function(text, onSucess, OnError) {
        var context = {
            "input": text,
        };
        this.businessController.getAddressSuggestion(context, onSucess, OnError);
    };
    CustomerManagement_PresentationController.prototype.getPlaceDetails = function(PlaceId, onSucess, OnError) {
        var context = {
            "placeid": PlaceId,
        };
        this.businessController.getPlaceDetails(context, onSucess, OnError);
    };
    CustomerManagement_PresentationController.prototype.USPSValidations = function(context) {
        var self = this;
        self.presentUserInterface('frmAssistedOnboarding', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmAssistedOnboarding', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmAssistedOnboarding', {
                "USPSRecommendations": response
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmAssistedOnboarding', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel = {
                message: ErrorInterceptor.errorMessage(error),
                status: "FAILURE",
                operation: ""
            };
            self.presentUserInterface('frmAssistedOnboarding', {
                "toastModel": self.toastModel
            });
        }
        self.businessController.USPSValidation(context, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.fetchLocationPrefillData = function(callback) {
        var self = this;
        self.globalDetails = {
            countries: null,
            regions: null,
            cities: null,
        };
        var promiseFetchCountryList = Promisify(this.businessController, 'fetchCountryList');
        var promiseFetchRegionList = Promisify(this.businessController, 'fetchRegionList');
        var promiseFetchCityList = Promisify(this.businessController, 'fetchCityList');
        Promise.all([
            promiseFetchCountryList({}),
            promiseFetchRegionList({}),
            promiseFetchCityList({}),
        ]).then(function(responses) {
            self.globalDetails.countries = responses[0];
            self.globalDetails.regions = responses[1];
            self.globalDetails.cities = responses[2];
            if (typeof callback === 'function') callback(self.globalDetails);
        }).catch(function(res) {
            callback("error");
            kony.print("unable to fetch preloaded data", res);
        });
    };
    CustomerManagement_PresentationController.prototype.getTermsAndConds = function() {
        var self = this;
        self.presentUserInterface('frmAssistedOnboarding', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmAssistedOnboarding', {
                "termsAndConds": response.onboardingtermsandconditions
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmAssistedOnboarding', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel = {
                message: ErrorInterceptor.errorMessage(error),
                status: "FAILURE",
                operation: ""
            };
            self.presentUserInterface('frmAssistedOnboarding', {
                "toastModel": self.toastModel
            });
        }
        this.businessController.fetchTermsAndConds({}, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.createOnboardingApplicant = function(context) {
        var self = this;
        self.presentUserInterface('frmAssistedOnboarding', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmAssistedOnboarding', {
                "LoadingScreen": {
                    focus: false
                }
            });
            if (response.applicantStatus === "ACCEPTED") {
                self.presentUserInterface('frmAssistedOnboarding', {
                    "createApplicant": {
                        status: "SUCCESS",
                        applicantInfo: response.applicantInformation
                    }
                });
            } else self.presentUserInterface('frmAssistedOnboarding', {
                "createApplicant": {
                    status: "FAILURE",
                    applicantInfo: response.applicantInformation
                }
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmAssistedOnboarding', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmAssistedOnboarding', {
                "toastModel": self.toastModel
            });
            self.presentUserInterface('frmAssistedOnboarding', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        this.businessController.createOnboardingApplicant(context, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.showEligibilityCriteria = function() {
        var self = this;
        self.presentUserInterface('frmNewCustomer', {
            "LoadingScreen": {
                focus: true
            }
        });
        this.businessController.getEligibilityCriteria({}, function onSuccess(response) {
            self.presentUserInterface('frmNewCustomer', {
                eligibilityCriteria: response.eligibilitycriteria
            });
            self.presentUserInterface('frmNewCustomer', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }, function onError(error) {
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmNewCustomer', {
                "toastModel": self.toastModel
            });
            self.presentUserInterface('frmNewCustomer', {
                "LoadingScreen": {
                    focus: false
                }
            });
        });
    };
    /**
     * @name showLoansForm
     * 
     */
    CustomerManagement_PresentationController.prototype.showLoansForm = function(context) {
        var scopeObj = this;

        function successCallback(response) {
            scopeObj.LoansMasterData = response.LoanTypeAPRs;
            scopeObj.retrieveAllLoansDetails();
            var resonseObject = {
                "LoansMasterData": scopeObj.LoansMasterData,
                "LoadingScreen": {
                    focus: true
                }
            };
            if (context && context.isApplicantFlow === true) {
                resonseObject.isApplicantFlow = true;
                resonseObject.response = context.response;
            }
            scopeObj.presentUserInterface("frmLoansDashboard", resonseObject);
        }

        function failureCallback(response) {
            scopeObj.retrieveAllLoansDetails();
        }
        scopeObj.businessController.getLoanTypeApr(successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.retrieveAllLoansDetails = function() {
        var scopeObj = this;
        var applicationListResponse = {
            PendingList: null,
            SubmittedList: null
        };

        function getSubmittedApplicationsListSuccess(response) {
            applicationListResponse.SubmittedList = (response.hasOwnProperty("errorCode") && response.errorCode === "3402") ? {
                "records": []
            } : response;
            scopeObj.presentUserInterface("frmLoansDashboard", {
                "action": "getAllApplicationData",
                "response": applicationListResponse,
                "LoadingScreen": {
                    focus: false
                }
            });
        }

        function getSubmittedApplicationsListError(response) {
            scopeObj.presentUserInterface("frmLoansDashboard", {
                "action": "getAllApplicationData",
                "response": applicationListResponse,
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        var customerId = kony.store.getItem("Customer_id");
        var idtobesent = {
            "Customer_id": customerId,
            "Status_id": "PENDING"
        };

        function getPendingApplicationsListSuccess(response) {
            applicationListResponse.PendingList = (response.hasOwnProperty("errorCode") && response.errorCode === "3402") ? {
                "records": []
            } : response;
            var idtobesent = {
                "Customer_id": customerId,
                "Status_id": "SUBMITTED"
            };
            scopeObj.businessController.getPendingApplicationsList(idtobesent, getSubmittedApplicationsListSuccess, getSubmittedApplicationsListError);
        }

        function getPendingApplicationsListError(response) {
            var idtobesent = {
                "Customer_id": customerId,
                "Status_id": "SUBMITTED"
            };
            scopeObj.businessController.getPendingApplicationsList(idtobesent, getSubmittedApplicationsListSuccess, getSubmittedApplicationsListError);
        }
        scopeObj.businessController.getPendingApplicationsList(idtobesent, getPendingApplicationsListSuccess, getPendingApplicationsListError);
    };
    CustomerManagement_PresentationController.prototype.retrieveLoansList = function(Status_id) {
        var scopeObj = this;
        var customerId = kony.store.getItem("Customer_id");

        function getPendingApplicationsListSuccess(response) {
            if (response.hasOwnProperty("errorCode") && response.errorCode === "3402") {
                response.records = [];
            }
            scopeObj.presentUserInterface("frmLoansDashboard", {
                "action": "getPendingApplicationsList" + Status_id,
                "response": response,
                "LoadingScreen": {
                    focus: false
                }
            });
        }

        function getPendingApplicationsListError(response) {
            kony.print("failed retrieving pending applications list");
            scopeObj.presentUserInterface("frmLoansDashboard", {
                "action": "getPendingApplicationsList" + Status_id,
                "response": response,
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        var idtobesent = {
            "Customer_id": customerId,
            "Status_id": Status_id
        };
        scopeObj.businessController.getPendingApplicationsList(idtobesent, getPendingApplicationsListSuccess, getPendingApplicationsListError);
    };
    CustomerManagement_PresentationController.prototype.fetchLeads = function() {
        var scopeObj = this;

        function getLeadsListSuccess(response) {
            if (response.hasOwnProperty("errorCode") && response.errorCode === "3402") {
                response.records = [];
            }
            scopeObj.presentUserInterface("frmLoansDashboard", {
                "action": "getLeadsList",
                "response": response,
                "LoadingScreen": {
                    focus: false
                }
            });
        }

        function getLeadsListError(response) {
            scopeObj.presentUserInterface("frmLoansDashboard", {
                "action": "ErrorOccured",
                "response": response,
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        var customerId = kony.store.getItem("Customer_id");
        var idtobesent = {
            "statusIds": "SID_NEW,SID_INPROGRESS",
            "productId": "",
            "leadType": "",
            "assignedTo": "",
            "phoneNumber": "",
            "emailAddress": "",
            "customerId": customerId,
            "pageNumber": "1",
            "recordsPerPage": "50",
            "modifiedStartDate": "",
            "modifiedEndDate": "",
        };
        scopeObj.businessController.fetchLeads(idtobesent, getLeadsListSuccess, getLeadsListError);
    };
    CustomerManagement_PresentationController.prototype.showUpgrdageUserScreen = function(param1) {
        var self = this;
        this.navigateTo('CustomerManagementModule', 'showUpgrdageUserScreenDetails', [param1]);
    };
    CustomerManagement_PresentationController.prototype.showUpgrdageUserScreenDetails = function(username) {
        var self = this;
        self.presentUserInterface('frmUpgradeUser', {
            "value": username
        });
    };
    CustomerManagement_PresentationController.prototype.getAddressSuggestion = function(text, onSucess, OnError) {
        var self = this;
        var context = {
            "input": text,
        };
        this.businessController.getAddressSuggestion(context, onSucess, OnError);
    };
    CustomerManagement_PresentationController.prototype.upgradeUser = function(params) {
        var self = this;
        self.presentUserInterface('frmUpgradeUser', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onSucess(response) {
            if (response.errMsg) {
                self.toastModel.message = response.errMsg;
                self.toastModel.status = "FAILURE";
                self.toastModel.operation = "";
                self.presentUserInterface('frmUpgradeUser', {
                    "toastModel": self.toastModel
                });
                self.presentUserInterface('frmUpgradeUser', {
                    "LoadingScreen": {
                        focus: false
                    }
                });
            } else {
                self.presentUserInterface('frmUpgradeUser', {
                    "LoadingScreen": {
                        focus: false
                    }
                });
                self.getCustomerBasicInfo({
                    "Customer_id": self.getCurrentCustomerDetails().Customer_id
                }, "InfoScreenProfile");
            }
        }

        function OnError(error) {
            self.toastModel.message = error.dbpErrMsg;
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmUpgradeUser', {
                "toastModel": self.toastModel
            });
            self.presentUserInterface('frmUpgradeUser', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        this.businessController.upgradeUser(params, onSucess, OnError);
    };
    CustomerManagement_PresentationController.prototype.fetchLocationPrefillData = function(callback) {
        var self = this;
        self.globalDetails = {
            countries: null,
            regions: null,
            cities: null,
        };
        var promiseFetchCountryList = Promisify(this.businessController, 'fetchCountryList');
        var promiseFetchRegionList = Promisify(this.businessController, 'fetchRegionList');
        var promiseFetchCityList = Promisify(this.businessController, 'fetchCityList');
        Promise.all([
            promiseFetchCountryList({}),
            promiseFetchRegionList({}),
            promiseFetchCityList({}),
        ]).then(function(responses) {
            self.globalDetails.countries = responses[0];
            self.globalDetails.regions = responses[1];
            self.globalDetails.cities = responses[2];
            if (typeof callback === 'function') callback(self.globalDetails);
        }).catch(function(res) {
            callback("error");
            kony.print("unable to fetch preloaded data", res);
        });
    };
    CustomerManagement_PresentationController.prototype.trackApplication = function(context) {
        var self = this;

        function successCallback(response) {
            var responseObject = {
                "action": context.isEdit ? "getTrackApplicationEditMode" : "getTrackApplicationDetails",
                "response": response,
                "loanType": context.loanType,
                "LoadingScreen": {
                    focus: context.isEdit ? true : false
                }
            };
            if (context.hasOwnProperty("status")) {
                responseObject.toastModel = {
                    "status": context.status,
                    "message": kony.i18n.getLocalizedString("i18n.frmLoansDashboard.SavedSuccessfully")
                };
            }
            self.presentUserInterface("frmTrackApplication", responseObject);
        }

        function failureCallback(response) {
            self.presentUserInterface("frmLoansDashboard", {
                "action": "ErrorOccured",
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        self.businessController.trackApplication(context, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.showNewCustomerScreen = function() {
        var self = this;
        this.navigateTo('CustomerManagementModule', 'newCustomerScreenDisplay', []);
    };
    CustomerManagement_PresentationController.prototype.newCustomerScreenDisplay = function() {
        var self = this;
        self.presentUserInterface('frmNewCustomer', {
            "value": "display"
        });
    };
    CustomerManagement_PresentationController.prototype.updateSubmittedLoanApplication = function(context, loantype) {
        var self = this;

        function successCallback(response) {
            self.trackApplication({
                "QueryResponseID": context.id,
                "loanType": loantype,
                "status": "SUCCESS"
            });
        }

        function failureCallback(response) {
            self.presentUserInterface("frmTrackApplication", {
                "action": "ErrorOccured",
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        if (context.QueryDefinition_id === "PERSONAL_APPLICATION") {
            self.businessController.updatePersonalLoan(context, successCallback, failureCallback);
        } else if (context.QueryDefinition_id === "VEHICLE_APPLICATION") {
            self.businessController.updateVehicleLoan(context, successCallback, failureCallback);
        } else {
            self.businessController.updateCreditCardApp(context, successCallback, failureCallback);
        }
    };
    CustomerManagement_PresentationController.prototype.navigateToCompanyDetailsScreen = function(context, currForm) {
        this.navigateTo('CompaniesModule', 'getCompanyAllDetails', [context]);
    };
    CustomerManagement_PresentationController.prototype.navigateToLeadDetailsScreen = function(context) {
        this.navigateTo('LeadManagementModule', 'fetchLeadDetails', [context]);
    };
    CustomerManagement_PresentationController.prototype.navigateToLeadScreen = function() {
        this.navigateTo('LeadManagementModule', 'fetchLeads', []);
    };
    CustomerManagement_PresentationController.prototype.getPersonalLoanPurpose = function(loanType) {
        var self = this;
        self.presentUserInterface('frmTrackApplication', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmTrackApplication', {
                "LoadingScreen": {
                    focus: false
                },
                "editMasterData": response.records
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmTrackApplication', {
                "LoadingScreen": {
                    focus: false
                },
                "toastModel": {
                    status: "FAILURE",
                    message: ErrorInterceptor.errorMessage(error)
                }
            });
        }
        self.businessController.getPersonalLoanPurpose({
            "QueryDefinitionID": loanType
        }, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.ValidateAddress = function(context) {
        var self = this;
        self.presentUserInterface('frmTrackApplication', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmTrackApplication', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmTrackApplication', {
                "USPSRecommendations": response
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmTrackApplication', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel = {
                message: ErrorInterceptor.errorMessage(error),
                status: "FAILURE",
                operation: ""
            };
            self.presentUserInterface('frmTrackApplication', {
                "toastModel": self.toastModel
            });
        }
        self.businessController.USPSValidation(context, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.SSNValidation = function(context) {
        var self = this;
        self.presentUserInterface('frmTrackApplication', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            if (response.records.length === 0) {
                self.presentUserInterface('frmTrackApplication', {
                    "LoadingScreen": {
                        focus: false
                    },
                    "isSSN": false,
                    "action": "SSNValidation"
                });
            } else {
                self.presentUserInterface('frmTrackApplication', {
                    "LoadingScreen": {
                        focus: false
                    },
                    "isSSN": true,
                    "data": response.records[0],
                    "action": "SSNValidation"
                });
            }
        }

        function failureCallback(error) {
            self.presentUserInterface('frmTrackApplication', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.toastModel.operation = "";
            self.presentUserInterface('frmTrackApplication', {
                "toastModel": self.toastModel
            });
        }
        self.businessController.SSNValidation(context, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.NumberValidation = function(context) {
        var self = this;
        self.presentUserInterface('frmTrackApplication', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmTrackApplication', {
                "LoadingScreen": {
                    focus: false
                },
                "NumberValidation": response
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmTrackApplication', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.toastModel = {
                message: ErrorInterceptor.errorMessage(error),
                status: "FAILURE",
                operation: ""
            };
            self.presentUserInterface('frmTrackApplication', {
                "toastModel": self.toastModel
            });
        }
        self.businessController.NumberValidation(context, successCallback, failureCallback);
    };
    /*
     * @name: showDepositsForm
     * @member CustomerManagementModule.presentationController
     */
    CustomerManagement_PresentationController.prototype.showDepositsForm = function(context) {
        var self = this;
        self.presentUserInterface('frmDepositsDashboard', {});
        self.fetchProductTypesForDeposits();
    };
    /*
     * @name: fetchLeadsForDeposits
     * @member CustomerManagementModule.presentationController
     */
    CustomerManagement_PresentationController.prototype.fetchLeadsForDeposits = function(context) {
        var self = this;
        self.presentUserInterface('frmDepositsDashboard', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmDepositsDashboard', {
                "LoadingScreen": {
                    focus: false
                },
                "depositsLeads": response
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmDepositsDashboard', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        self.businessController.fetchLeadsDeposits(context, successCallback, failureCallback);
    };
    /*
     * @name: fetchProductTypesForDeposits
     * @member CustomerManagementModule.presentationController
     */
    CustomerManagement_PresentationController.prototype.fetchProductTypesForDeposits = function(context) {
        var self = this;
        self.presentUserInterface('frmDepositsDashboard', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmDepositsDashboard', {
                "LoadingScreen": {
                    focus: false
                },
                "depositsProducts": response.productTypes
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmDepositsDashboard', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        self.businessController.fetchProductsDeposits(context, successCallback, failureCallback);
    };
    /*
     * @name: fetchSubmittedApplicationsDeposits
     * @member CustomerManagementModule.presentationController
     */
    CustomerManagement_PresentationController.prototype.fetchSubmittedApplicationsDeposits = function(context) {
        var self = this;
        self.presentUserInterface('frmDepositsDashboard', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmDepositsDashboard', {
                "LoadingScreen": {
                    focus: false
                },
                "depositsSubmitApplications": response.Accounts
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmDepositsDashboard', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        self.businessController.getCustomerAccounts(context, successCallback, failureCallback);
    };
    /*
     * @name: navigateToLeadCreateScreen
     * @member CustomerManagementModule.presentationController
     * @param: create payload for lead, action-create/edit,navObj - {formName:"",breadcrumbBack:""}
     */
    CustomerManagement_PresentationController.prototype.navigateToLeadCreateScreen = function(param, action, navObj) {
        var self = this;
        self.navigateTo('LeadManagementModule', 'createLeadFromCustomer', [param, action, navObj]);
    };
    CustomerManagement_PresentationController.prototype.updateLeadStatus = function(payload, context) {
        var self = this;
        self.presentUserInterface('frmDepositsDashboard', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            if (context === "deposits") {
                self.presentUserInterface('frmDepositsDashboard', {
                    "LoadingScreen": {
                        focus: false
                    },
                    "toast": {
                        message: "Update lead success",
                        status: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SUCCESS")
                    },
                    "updateLead": true
                });
                var param = {
                    "statusIds": "",
                    "productId": "",
                    "leadType": "",
                    "customerId": self.getCurrentCustomerDetails().Customer_id,
                    "assignedTo": "",
                    "phoneNumber": "",
                    "emailAddress": "",
                    "pageNumber": 1,
                    "recordsPerPage": 20,
                    "modifiedStartDate": "",
                    "modifiedEndDate": "",
                    "sortCriteria": "",
                    "sortOrder": ""
                };
                self.fetchLeadsForDeposits(param);
            } else if (context === "loans") {}
        }

        function failureCallback(error) {
            if (context === "deposits") {
                self.presentUserInterface('frmDepositsDashboard', {
                    "LoadingScreen": {
                        focus: false
                    },
                    "toast": {
                        message: "Update lead failed",
                        status: "ERROR"
                    }
                });
            } else if (context === "loans") {}
        }
        self.businessController.updateLeadStatus(payload, successCallback, failureCallback);
    };
    CustomerManagement_PresentationController.prototype.getTermsAndConditions = function() {
        var self = this;
        self.presentUserInterface('frmAssistedOnboarding', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onSuccess(response) {
            self.presentUserInterface('frmAssistedOnboarding', {
                "LoadingScreen": {
                    focus: false
                },
                "termsAndConds": response.termsAndConditionsContent
            });
        }

        function onError(error) {
            self.toastModel.message = ErrorInterceptor.errorMessage(error);
            self.toastModel.status = "FAILURE";
            self.presentUserInterface('frmAssistedOnboarding', {
                "LoadingScreen": {
                    focus: false
                },
                "toastModel": self.toastModel
            });
        }
        self.businessController.getTermsAndConditions({
            "termsAndConditionsCode": "C360_CustomerOnboarding"
        }, onSuccess, onError);
    };
    return CustomerManagement_PresentationController;
});