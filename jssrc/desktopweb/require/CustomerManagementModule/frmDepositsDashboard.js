define("CustomerManagementModule/frmDepositsDashboard", function() {
    return function(controller) {
        function addWidgetsfrmDepositsDashboard() {
            this.setDefaultUnit(kony.flex.DP);
            var flxDepositsDashboardMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDepositsDashboardMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDepositsDashboardMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "konydbxlogobignverted.png"
                    },
                    "imgBottomLogo": {
                        "src": "konydbxinverted.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxDepositsContainer = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxDepositsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "CopysknFlxScrlf0d7845baf952542",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxDepositsContainer.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false,
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "top": "58px"
                    },
                    "imgLogout": {
                        "isVisible": true,
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "text": "Customer Management"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var btnNotes = new kony.ui.Button({
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "30px",
                "id": "btnNotes",
                "isVisible": true,
                "right": "35px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnNotes\")",
                "top": "58dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxMainHeader.add(mainHeader, btnNotes);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SearchCustomers\")",
                        "left": "35px"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "text": "BRYAN NASH",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            var CSRAssistToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "CSRAssistToolTip",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "50px",
                "skin": "slFbox",
                "top": "180dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": true,
                        "left": "viz.val_cleared",
                        "right": "50px",
                        "top": "180dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMainScrollContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxMainScrollContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": 0,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "140dp",
                "verticalScrollIndicator": true
            }, {}, {});
            flxMainScrollContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderAndMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderAndMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxHeaderAndMainContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderAndCardsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderAndCardsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBordere4e6ecCustomRadius5px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeaderAndCardsContainer.setDefaultUnit(kony.flex.DP);
            var flxGeneralInfoHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGeneralInfoHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknFlxFFFFFF100O",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxGeneralInfoHeader.setDefaultUnit(kony.flex.DP);
            var generalInfoHeader = new com.adminConsole.customerMang.generalInfoHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "generalInfoHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "overrides": {
                    "flxActionButtons": {
                        "reverseLayoutDirection": false
                    },
                    "flxCSRAssist": {
                        "isVisible": false
                    },
                    "flxHeader": {
                        "width": "100%"
                    },
                    "flxRiskStatus": {
                        "isVisible": true,
                        "zIndex": 10
                    },
                    "flxUnlock": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": 150
                    },
                    "generalInfoHeader": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "left": "35px",
                        "right": "35px",
                        "top": "0px",
                        "width": "viz.val_cleared"
                    },
                    "lblSeparator": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxGeneralInfoHeader.add(generalInfoHeader);
            var flxAlertMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertMessage",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAlertMessage.setDefaultUnit(kony.flex.DP);
            var alertMessage = new com.adminConsole.customerMang.alertMessage({
                "clipBounds": true,
                "height": "70px",
                "id": "alertMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "top": "0",
                "width": "93%",
                "zIndex": 1,
                "overrides": {
                    "alertMessage": {
                        "bottom": "viz.val_cleared",
                        "isVisible": true,
                        "left": "35px",
                        "right": "viz.val_cleared",
                        "top": "0",
                        "width": "93%",
                        "zIndex": 1
                    },
                    "lblData1": {
                        "centerY": "50%",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.requestsAndNotificationsCountPrefix\")",
                        "left": "5px",
                        "top": "15px"
                    },
                    "lblData2": {
                        "centerY": "50%",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.requestsAndNotificationsCountSufix\")",
                        "isVisible": true,
                        "left": "5px",
                        "top": "15px"
                    },
                    "lblData3": {
                        "centerY": "50%",
                        "left": "1px",
                        "top": "15px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertMessage.add(alertMessage);
            var flxMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxProductsTabsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxProductsTabsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxFFFFFF1000",
                "top": "0dp",
                "zIndex": 50
            }, {}, {});
            flxProductsTabsWrapper.setDefaultUnit(kony.flex.DP);
            var dashboardCommonTab = new com.adminConsole.commonTabsDashboard.dashboardCommonTab({
                "clipBounds": true,
                "height": "100%",
                "id": "dashboardCommonTab",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxProductsTabsWrapper.add(dashboardCommonTab);
            var flxDepositsCardsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDepositsCardsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxDepositsCardsContainer.setDefaultUnit(kony.flex.DP);
            var flx360degreeView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flx360degreeView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 100
            }, {}, {});
            flx360degreeView.setDefaultUnit(kony.flex.DP);
            var flxSeparatorHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSeparatorHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 500
            }, {}, {});
            flxSeparatorHeader.setDefaultUnit(kony.flex.DP);
            var lblSeparatorHeader = new kony.ui.Label({
                "id": "lblSeparatorHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSeparatorHeader.add(lblSeparatorHeader);
            var flxDepositTypesContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDepositTypesContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxDepositTypesContainer.setDefaultUnit(kony.flex.DP);
            var flxDepositTypes = new kony.ui.FlexContainer({
                "bottom": "20dp",
                "clipBounds": true,
                "height": "138dp",
                "id": "flxDepositTypes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "93%",
                "zIndex": 100
            }, {}, {});
            flxDepositTypes.setDefaultUnit(kony.flex.DP);
            flxDepositTypes.add();
            var depositsInfo = new com.adminConsole.loans.applications.loantype.loaninfo({
                "bottom": "10dp",
                "clipBounds": true,
                "height": "100dp",
                "id": "depositsInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBordere1e5eedCustomRadius5px",
                "top": "10dp",
                "width": "32.50%",
                "overrides": {
                    "btnApplyLoan": {
                        "isVisible": false,
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "flxApplyLoan": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "20dp",
                        "left": "10dp",
                        "top": "viz.val_cleared"
                    },
                    "flxLearnMore": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerY": "viz.val_cleared",
                        "left": "73dp",
                        "top": "5dp"
                    },
                    "flxLoanAprDetails": {
                        "isVisible": true,
                        "left": "10dp",
                        "width": "99%"
                    },
                    "flxLoanTypeIcon": {
                        "height": "49dp",
                        "left": "20dp",
                        "width": "49dp"
                    },
                    "lblLoanApr": {
                        "isVisible": false,
                        "left": "73dp"
                    },
                    "lblNewLoanType": {
                        "left": "83dp"
                    },
                    "lblNewLoanTypeId": {
                        "top": "30dp"
                    },
                    "loaninfo": {
                        "centerX": "viz.val_cleared",
                        "height": "100dp",
                        "isVisible": false,
                        "left": "1%",
                        "width": "32.50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDepositTypesContainer.add(flxDepositTypes, depositsInfo);
            flx360degreeView.add(flxSeparatorHeader, flxDepositTypesContainer);
            flxDepositsCardsContainer.add(flx360degreeView);
            var flxRegistrationWrapperKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRegistrationWrapperKA",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "81%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "16%",
                "zIndex": 51
            }, {}, {});
            flxRegistrationWrapperKA.setDefaultUnit(kony.flex.DP);
            var flxRegistrationLinkKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRegistrationLinkKA",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 52
            }, {}, {});
            flxRegistrationLinkKA.setDefaultUnit(kony.flex.DP);
            var lblRegistrationLinkKA = new kony.ui.Label({
                "id": "lblRegistrationLinkKA",
                "isVisible": true,
                "left": "5%",
                "onTouchEnd": controller.AS_Label_b5befc3b01724c89a3ada56766fe60f6,
                "skin": "sknLbl006CCA13pxKA",
                "text": "Send Registration Email",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblHover006CCA13pxKA"
            });
            flxRegistrationLinkKA.add(lblRegistrationLinkKA);
            var flxRegistrationLinkOptionsKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRegistrationLinkOptionsKA",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBordere1e5eedCustomRadius5px",
                "top": "45dp",
                "width": "95%",
                "zIndex": 11
            }, {}, {});
            flxRegistrationLinkOptionsKA.setDefaultUnit(kony.flex.DP);
            var lblSendLinkKA = new kony.ui.Label({
                "id": "lblSendLinkKA",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato5d6c7f12px",
                "text": "SEND LINK VIA",
                "top": "7dp",
                "width": "80%",
                "zIndex": 11
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailoptionKA = new kony.ui.Label({
                "id": "lblEmailoptionKA",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLblFont485C75100O",
                "text": "Email",
                "top": "10dp",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl11abeb100O"
            });
            var lblSMSOptionKA = new kony.ui.Label({
                "id": "lblSMSOptionKA",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLblFont485C75100O",
                "text": "SMS",
                "top": "5dp",
                "width": "80%",
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl11abeb100O"
            });
            var lblEmailAndSMSOptionKA = new kony.ui.Label({
                "bottom": "5dp",
                "id": "lblEmailAndSMSOptionKA",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLblFont485C75100O",
                "text": "Both email and SMS",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl11abeb100O"
            });
            flxRegistrationLinkOptionsKA.add(lblSendLinkKA, lblEmailoptionKA, lblSMSOptionKA, lblEmailAndSMSOptionKA);
            flxRegistrationWrapperKA.add(flxRegistrationLinkKA, flxRegistrationLinkOptionsKA);
            flxMainContainer.add(flxProductsTabsWrapper, flxDepositsCardsContainer, flxRegistrationWrapperKA);
            flxHeaderAndCardsContainer.add(flxGeneralInfoHeader, flxAlertMessage, flxMainContainer);
            var flxApplicationsandLeadsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxApplicationsandLeadsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBordere4e6ecCustomRadius5px",
                "top": "12dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxApplicationsandLeadsContainer.setDefaultUnit(kony.flex.DP);
            var flxApplicationAndLeadsTabsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxApplicationAndLeadsTabsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF1000",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxApplicationAndLeadsTabsContainer.setDefaultUnit(kony.flex.DP);
            var flxTabButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "59px",
                "id": "flxTabButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "preShow": function(eventobject) {
                    controller.AS_FlexContainer_b1a3158c443a4c42acb8d6a705fbc11f(eventobject);
                },
                "right": "20dp",
                "skin": "slFbox",
                "top": "0px"
            }, {}, {});
            flxTabButtons.setDefaultUnit(kony.flex.DP);
            var btnTabName1 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName1",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplicationStatic\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName2 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName2",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.LeadsStatic\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName3 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName3",
                "isVisible": false,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.HELP_CENTER\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            flxTabButtons.add(btnTabName1, btnTabName2, btnTabName3);
            var lblTabsSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblTabsSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblSeparator",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxApplicationAndLeadsTabsContainer.add(flxTabButtons, lblTabsSeperator);
            var flxPendingSubmittedTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxPendingSubmittedTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBge7eaeeOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPendingSubmittedTabs.setDefaultUnit(kony.flex.DP);
            var btnPendingTab = new kony.ui.Button({
                "height": "37dp",
                "id": "btnPendingTab",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.StartedApplication\")",
                "top": "13dp",
                "width": "85dp",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSubmittedTab = new kony.ui.Button({
                "height": "37dp",
                "id": "btnSubmittedTab",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SubmittedApplication\")",
                "top": "13dp",
                "width": "100dp",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPendingSubmittedTabs.add(btnPendingTab, btnSubmittedTab);
            var flxCreateLeadConatiner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxCreateLeadConatiner",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCreateLeadConatiner.setDefaultUnit(kony.flex.DP);
            var btnCreateLead = new kony.ui.Button({
                "height": "30dp",
                "id": "btnCreateLead",
                "isVisible": true,
                "right": "32dp",
                "skin": "sknCreateCustomer",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CreateLead\")",
                "top": "14dp",
                "width": "96dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCreateLeadConatiner.add(btnCreateLead);
            var flxListOfApplications = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxListOfApplications",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxListOfApplications.setDefaultUnit(kony.flex.DP);
            var flxListOfApplicationsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxListOfApplicationsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF1000",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxListOfApplicationsHeader.setDefaultUnit(kony.flex.DP);
            var flxApplicationIDContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxApplicationIDContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxApplicationIDContainer.setDefaultUnit(kony.flex.DP);
            var lblApplicationIDHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblApplicationIDHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplicationId\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconApplicationIdSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconApplicationIdSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxApplicationIDContainer.add(lblApplicationIDHeader, lblIconApplicationIdSort);
            var flxProductTypeContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxProductTypeContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxProductTypeContainer.setDefaultUnit(kony.flex.DP);
            var lblProductTypeHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProductTypeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PRODUCT_TYPE\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconProductSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconProductSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxProductTypeContainer.add(lblProductTypeHeader, lblIconProductSort);
            var flxCreateOnContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCreateOnContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "55%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "14dp",
                "width": "14%",
                "zIndex": 1
            }, {}, {});
            flxCreateOnContainer.setDefaultUnit(kony.flex.DP);
            var lblCreateOnHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCreateOnHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CreatedOn\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblIconCreateOnSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconCreateOnSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCreateOnContainer.add(lblCreateOnHeader, lblIconCreateOnSort);
            var flxModifyOnContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxModifyOnContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "14dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxModifyOnContainer.setDefaultUnit(kony.flex.DP);
            var lblModifyOnHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblModifyOnHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ModifiedOn\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblIconModifySort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconModifySort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxModifyOnContainer.add(lblModifyOnHeader, lblIconModifySort);
            var lblSeparatorApplicationHeader = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeparatorApplicationHeader",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLblSeparator696C73",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxListOfApplicationsHeader.add(flxApplicationIDContainer, flxProductTypeContainer, flxCreateOnContainer, flxModifyOnContainer, lblSeparatorApplicationHeader);
            var flxListOfApplicationsSubmittedHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxListOfApplicationsSubmittedHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF1000",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxListOfApplicationsSubmittedHeader.setDefaultUnit(kony.flex.DP);
            var flxApplicationIDSubmitted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxApplicationIDSubmitted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxApplicationIDSubmitted.setDefaultUnit(kony.flex.DP);
            var lblApplicationHeaderSubmitted = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblApplicationHeaderSubmitted",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplicationId\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblIconApplIdSubmitSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconApplIdSubmitSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxApplicationIDSubmitted.add(lblApplicationHeaderSubmitted, lblIconApplIdSubmitSort);
            var flxProductTypeSubmitted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxProductTypeSubmitted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "22%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxProductTypeSubmitted.setDefaultUnit(kony.flex.DP);
            var lblProductTypeHeaderSubmitted = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProductTypeHeaderSubmitted",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PRODUCT_TYPE\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblIconApplProductSubmitSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconApplProductSubmitSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxProductTypeSubmitted.add(lblProductTypeHeaderSubmitted, lblIconApplProductSubmitSort);
            var flxCreatedOnSubmitted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCreatedOnSubmitted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "43%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "13%",
                "zIndex": 1
            }, {}, {});
            flxCreatedOnSubmitted.setDefaultUnit(kony.flex.DP);
            var lblCreatedOnHeaderSubmitted = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCreatedOnHeaderSubmitted",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CREATED_ON\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblIconApplCreatedSubmitSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconApplCreatedSubmitSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCreatedOnSubmitted.add(lblCreatedOnHeaderSubmitted, lblIconApplCreatedSubmitSort);
            var flxSubmitOnSubmitted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSubmitOnSubmitted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "61%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "14%",
                "zIndex": 1
            }, {}, {});
            flxSubmitOnSubmitted.setDefaultUnit(kony.flex.DP);
            var lblSubmitOnHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubmitOnHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SubmittedOn\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblIconSubmittedOnSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconSubmittedOnSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxSubmitOnSubmitted.add(lblSubmitOnHeader, lblIconSubmittedOnSort);
            var flxStatusSubmitted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxStatusSubmitted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxStatusSubmitted.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Status\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblIconApplStatusSubmitSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconApplStatusSubmitSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxStatusSubmitted.add(lblStatus, lblIconApplStatusSubmitSort);
            var lblSeparatorApplicationHeaderSubmitted = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeparatorApplicationHeaderSubmitted",
                "isVisible": true,
                "left": "22dp",
                "right": "22dp",
                "skin": "sknLblSeparator696C73",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxListOfApplicationsSubmittedHeader.add(flxApplicationIDSubmitted, flxProductTypeSubmitted, flxCreatedOnSubmitted, flxSubmitOnSubmitted, flxStatusSubmitted, lblSeparatorApplicationHeaderSubmitted);
            var segmentListOfApplications = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblDepositApplCreatedStarted": "14-06-2019",
                    "lblDepositApplIdStarted": "95236853443",
                    "lblDepositApplModifiedStarted": "17-06-2019",
                    "lblDepositApplProductStarted": "12 Month Term CD",
                    "lblStartedSeperator": "Label"
                }],
                "groupCells": false,
                "id": "segmentListOfApplications",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxDepositApplicationsStarted",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "60dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxDepositApplicationsStarted": "flxDepositApplicationsStarted",
                    "flxStartedApplications": "flxStartedApplications",
                    "lblDepositApplCreatedStarted": "lblDepositApplCreatedStarted",
                    "lblDepositApplIdStarted": "lblDepositApplIdStarted",
                    "lblDepositApplModifiedStarted": "lblDepositApplModifiedStarted",
                    "lblDepositApplProductStarted": "lblDepositApplProductStarted",
                    "lblStartedSeperator": "lblStartedSeperator"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segmentListOfApplicationsSubmitted = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblDepositApplCreatedStarted": "",
                    "lblDepositApplIdSubmit": "",
                    "lblDepositApplProductSubmit": "",
                    "lblDepositApplStatusSubmit": "",
                    "lblDepositApplSubmitOn": "",
                    "lblStartedSeperator": ""
                }],
                "groupCells": false,
                "id": "segmentListOfApplicationsSubmitted",
                "isVisible": false,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxDepositApplicationsSubmitted",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "60dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxDepositApplicationsSubmitted": "flxDepositApplicationsSubmitted",
                    "flxSubmittedApplications": "flxSubmittedApplications",
                    "lblDepositApplCreatedStarted": "lblDepositApplCreatedStarted",
                    "lblDepositApplIdSubmit": "lblDepositApplIdSubmit",
                    "lblDepositApplProductSubmit": "lblDepositApplProductSubmit",
                    "lblDepositApplStatusSubmit": "lblDepositApplStatusSubmit",
                    "lblDepositApplSubmitOn": "lblDepositApplSubmitOn",
                    "lblStartedSeperator": "lblStartedSeperator"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoApplicationsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120dp",
                "id": "flxNoApplicationsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxNoApplicationsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoapplicationsFound = new kony.ui.RichText({
                "centerX": "50%",
                "id": "rtxNoapplicationsFound",
                "isVisible": true,
                "linkSkin": "defRichTextLink",
                "skin": "sknRtxLatoReg84939e13px",
                "text": "No applications available.",
                "top": "50dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoApplicationsFound.add(rtxNoapplicationsFound);
            var flxSelectOptionsApplications = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptionsApplications",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "33px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100px",
                "width": "200px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptionsApplications.setDefaultUnit(kony.flex.DP);
            var applicationsContextualMenu = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "applicationsContextualMenu",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnLink1": {
                        "isVisible": false
                    },
                    "btnLink2": {
                        "isVisible": false
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOption4": {
                        "isVisible": false
                    },
                    "flxOptionsSeperator": {
                        "isVisible": false
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false
                    },
                    "lblOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.edit\")"
                    },
                    "lblOption2": {
                        "text": "Track"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSelectOptionsApplications.add(applicationsContextualMenu);
            flxListOfApplications.add(flxListOfApplicationsHeader, flxListOfApplicationsSubmittedHeader, segmentListOfApplications, segmentListOfApplicationsSubmitted, flxNoApplicationsFound, flxSelectOptionsApplications);
            var flxListOfLeads = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxListOfLeads",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxListOfLeads.setDefaultUnit(kony.flex.DP);
            var flxDepositCreateLead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxDepositCreateLead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF1000",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDepositCreateLead.setDefaultUnit(kony.flex.DP);
            var btnDepositCreateLead = new kony.ui.Button({
                "height": "30dp",
                "id": "btnDepositCreateLead",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknBtn205584LatoBoldffffff13px",
                "text": "Create Lead",
                "top": "15dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDepositCreateLead.add(btnDepositCreateLead);
            var flxListOfLeadsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxListOfLeadsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF1000",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxListOfLeadsHeader.setDefaultUnit(kony.flex.DP);
            var flxListOfLeadsHeaderInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxListOfLeadsHeaderInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxListOfLeadsHeaderInner.setDefaultUnit(kony.flex.DP);
            var flxProductContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxProductContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "28dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxProductContainer.setDefaultUnit(kony.flex.DP);
            var lblProductHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProductHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Product\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblProductHeaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProductHeaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxProductContainer.add(lblProductHeader, lblProductHeaderImg);
            var flxStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "22%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxStatusContainer.setDefaultUnit(kony.flex.DP);
            var lblStatusHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatusHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Status\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStatusHeaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatusHeaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxStatusContainer.add(lblStatusHeader, lblStatusHeaderImg);
            var flxLeadsCreatedOnContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxLeadsCreatedOnContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "36%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxLeadsCreatedOnContainer.setDefaultUnit(kony.flex.DP);
            var lblLeadsCreatedOnHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLeadsCreatedOnHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CreatedOn\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblLeadsCreatedOnHeaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLeadsCreatedOnHeaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxLeadsCreatedOnContainer.add(lblLeadsCreatedOnHeader, lblLeadsCreatedOnHeaderImg);
            var flxCreatedByContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCreatedByContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxCreatedByContainer.setDefaultUnit(kony.flex.DP);
            var lblCreatedByHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCreatedByHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CreatedBy\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCreatedByHeaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCreatedByHeaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCreatedByContainer.add(lblCreatedByHeader, lblCreatedByHeaderImg);
            var flxAssignToContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxAssignToContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "72%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxAssignToContainer.setDefaultUnit(kony.flex.DP);
            var lblAssignToHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAssignToHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.AssignTo\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblAssignToHeaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAssignToHeaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxAssignToContainer.add(lblAssignToHeader, lblAssignToHeaderImg);
            flxListOfLeadsHeaderInner.add(flxProductContainer, flxStatusContainer, flxLeadsCreatedOnContainer, flxCreatedByContainer, flxAssignToContainer);
            var lblSeparatorLeadsHeader = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeparatorLeadsHeader",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLblSeparator696C73",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxListOfLeadsHeader.add(flxListOfLeadsHeaderInner, lblSeparatorLeadsHeader);
            var flxDepositsLeadScroll = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "350dp",
                "horizontalScrollIndicator": true,
                "id": "flxDepositsLeadScroll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "120dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxDepositsLeadScroll.setDefaultUnit(kony.flex.DP);
            var segmentListOfLeads = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAssignTo": "Label",
                    "lblCreatedBy": "Label",
                    "lblCreatedOn": "Label",
                    "lblIconOptionsMenu": "",
                    "lblProduct": "Label",
                    "lblSeparatorListOfLeads": "",
                    "lblStatus": "Label"
                }],
                "groupCells": false,
                "id": "segmentListOfLeads",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxDepositsLeads",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxDepositsLeads": "flxDepositsLeads",
                    "flxLeadsOptionsMenu": "flxLeadsOptionsMenu",
                    "flxListOfLeadsInner": "flxListOfLeadsInner",
                    "lblAssignTo": "lblAssignTo",
                    "lblCreatedBy": "lblCreatedBy",
                    "lblCreatedOn": "lblCreatedOn",
                    "lblIconOptionsMenu": "lblIconOptionsMenu",
                    "lblProduct": "lblProduct",
                    "lblSeparatorListOfLeads": "lblSeparatorListOfLeads",
                    "lblStatus": "lblStatus"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelectOptionsLeads = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptionsLeads",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "33px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100px",
                "width": "200px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptionsLeads.setDefaultUnit(kony.flex.DP);
            var leadContextualMenu = new com.adminConsole.common.leadContextualMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "leadContextualMenu",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0px",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSelectOptionsLeads.add(leadContextualMenu);
            flxDepositsLeadScroll.add(segmentListOfLeads, flxSelectOptionsLeads);
            var flxNoLeadsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120dp",
                "id": "flxNoLeadsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxNoLeadsFound.setDefaultUnit(kony.flex.DP);
            var rtxDepositsNoLeads = new kony.ui.RichText({
                "centerX": "50%",
                "id": "rtxDepositsNoLeads",
                "isVisible": true,
                "linkSkin": "defRichTextLink",
                "skin": "sknRtxLatoReg84939e13px",
                "text": "No applications available.",
                "top": "24dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoLeadsFound.add(rtxDepositsNoLeads);
            flxListOfLeads.add(flxDepositCreateLead, flxListOfLeadsHeader, flxDepositsLeadScroll, flxNoLeadsFound);
            flxApplicationsandLeadsContainer.add(flxApplicationAndLeadsTabsContainer, flxPendingSubmittedTabs, flxCreateLeadConatiner, flxListOfApplications, flxListOfLeads);
            var flxBottomPadding = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "48dp",
                "id": "flxBottomPadding",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBottomPadding.setDefaultUnit(kony.flex.DP);
            flxBottomPadding.add();
            flxHeaderAndMainContainer.add(flxHeaderAndCardsContainer, flxApplicationsandLeadsContainer, flxBottomPadding);
            flxMainScrollContainer.add(flxHeaderAndMainContainer);
            var flxSelectOptions = new com.adminConsole.customerMang.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "55px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "190dp",
                "width": "200px",
                "overrides": {
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementPersonal.upgradeToMicroBusiness\")"
                    },
                    "selectOptions": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "55px",
                        "top": "190dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDepositsContainer.add(flxMainHeader, flxBreadCrumbs, CSRAssistToolTip, flxMainScrollContainer, flxSelectOptions);
            var flxNote = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNote",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "30%",
                "zIndex": 100
            }, {}, {});
            flxNote.setDefaultUnit(kony.flex.DP);
            var Notes = new com.adminConsole.customerMang.Notes({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "Notes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0d4d94de464db42CM",
                "width": "100%",
                "overrides": {
                    "flxNotes": {
                        "top": "viz.val_cleared"
                    },
                    "imgCloseNotes": {
                        "src": "close_blue.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNote.add(Notes);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            flxRightPanel.add(flxDepositsContainer, flxNote, flxHeaderDropdown);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoadingBlur",
                "top": "0px",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxPopUpConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpConfirmation = new com.adminConsole.common.popUp({
                "clipBounds": true,
                "height": "100%",
                "id": "popUpConfirmation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "right": "200px",
                        "text": "NO, LEAVE IT AS IT IS",
                        "width": "180px"
                    },
                    "btnPopUpDelete": {
                        "text": "YES, OFFLINE",
                        "width": "150px"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Are_you_sure_to\")",
                        "maxWidth": "540px",
                        "minWidth": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpConfirmation.add(popUpConfirmation);
            flxDepositsDashboardMain.add(flxLeftPannel, flxRightPanel, flxToastMessage, flxLoading, flxPopUpConfirmation);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "imgPopUpClose": {
                        "src": "close_blue.png"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            var CSRAssist = new com.adminConsole.customerMang.CSRAssist({
                "clipBounds": true,
                "height": "100%",
                "id": "CSRAssist",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 50,
                "overrides": {
                    "CSRAssist": {
                        "isVisible": false
                    },
                    "imgCSRLoading": {
                        "src": "loadingscreenimage.gif"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.add(flxDepositsDashboardMain, flxEditCancelConfirmation, CSRAssist);
        };
        return [{
            "addWidgets": addWidgetsfrmDepositsDashboard,
            "enabledForIdleTimeout": true,
            "id": "frmDepositsDashboard",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_cc775b57e57e412a9bfcebabc788acf8(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});