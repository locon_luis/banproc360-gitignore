define("CustomerManagementModule/userfrmCustomerProfileAccountsController", {
    AccountTrasactions: [],
    successLabel: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Successful"),
    sortBy: null,
    cardNumber: null,
    willUpdateUI: function(context) {
        if (context) {
            this.updateLeftMenu(context);
            if (context.LoadingScreen) {
                if (context.LoadingScreen.focus) {
                    kony.adminConsole.utils.showProgressBar(this.view);
                } else {
                    kony.adminConsole.utils.hideProgressBar(this.view);
                }
            } else if (context.toastModel) {
                if (context.toastModel.status === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SUCCESS")) {
                    this.view.toastMessage.showToastMessage(context.toastModel.message, this);
                } else {
                    this.view.toastMessage.showErrorToastMessage(context.toastModel.message, this);
                }
            } else if (context.CustomerBasicInfo) {
                this.view.flxGeneralInfoWrapper.setBasicInformation(context.CustomerBasicInfo, this);
            } else if (context.UpdateDBPUserStatus) {
                this.view.flxGeneralInfoWrapper.setLockStatus(context.UpdateDBPUserStatus.status.toUpperCase(), this);
            } else if (context.enrollACustomer) {
                this.view.flxGeneralInfoWrapper.setEnrollmentAccessandStatus();
            } else if (context.StatusGroup) {
                this.view.flxGeneralInfoWrapper.processAndFillStatusForEdit(context.StatusGroup, this);
            } else if (context.CustomerNotes) {
                this.view.Notes.displayNotes(this, context.CustomerNotes);
            } else if (context.OnlineBankingLogin) {
                this.view.CSRAssist.onlineBankingLogin(context.OnlineBankingLogin, this);
            } else if (context.CustomerAccounts) {
                this.resetAllSortImagesAccounts();
                this.setSkinForProductTabs(this.view.btnProductsAccounts);
                this.setDataForProductsSegment(context.CustomerAccounts);
            } else if (context.AccountTrasactions) {
                this.resetAllSortImagesTransactions();
                var acctNo = this.view.segProductListing.selecteditems[0].lblAccountNumber;
                this.setDataForProductDetailsScreen(this.presenter.getCustomerAccountInfo(acctNo));
                this.AccountTrasactions = context.AccountTrasactions;
                this.setDataForProductTransactionSegment(this.successLabel);
            } else if (context.cardsInfomartion) {
                this.setDataToCardMangSegment(context.cardsInfomartion);
            } else if (context.UpdateCardRequests) {
                this.presenter.getCardsInformation({
                    "customerUsername": this.presenter.getCurrentCustomerDetails().Username
                });
            } else if (context.eStatementUpdate) {
                if (context.eStatementUpdate.eStatementUpdateStatus === "success") {
                    if (this.view.imgRbPaper.src === this.AdminConsoleCommonUtils.radioSelected) {
                        this.view.toastMessage.showToastMessage("Paper statement successfully enabled.", this);
                    } else if (this.view.imgRbEstatement.src === this.AdminConsoleCommonUtils.radioSelected) {
                        this.view.toastMessage.showToastMessage("e-statement successfully enabled.", this);
                    }
                    this.showEstateColoumns(context.eStatementUpdate);
                } else {
                    if (this.view.imgRbPaper.src === this.AdminConsoleCommonUtils.radioSelected) {
                        this.view.imgRbPaper.src = this.AdminConsoleCommonUtils.radioNotSelected;
                        this.view.imgRbEstatement.src = this.AdminConsoleCommonUtils.radioSelected;
                        this.view.flxSubscriptionsEmail.setVisibility(true);
                        this.view.toastMessage.showErrorToastMessage("Paper statement could not be enabled.", this);
                    } else if (this.view.imgRbEstatement.src === this.AdminConsoleCommonUtils.radioSelected) {
                        this.view.imgRbEstatement.src = this.AdminConsoleCommonUtils.radioNotSelected;
                        this.view.imgRbPaper.src = this.AdminConsoleCommonUtils.radioSelected;
                        this.view.flxSubscriptionsEmail.setVisibility(false);
                        this.view.toastMessage.showErrorToastMessage("e-statement could not be enabled.", this);
                    }
                }
            }
        }
    },
    showEstateColoumns: function(eStateInfo) {
        if (eStateInfo.LastUpdated && eStateInfo.LastUpdated !== "") this.view.lblData1.text = this.getLocaleDate(eStateInfo.LastUpdated);
        else this.view.lblData1.text = "N/A";
        if (eStateInfo.UpdatedBy && eStateInfo.UpdatedBy !== "") this.view.lblData2.text = eStateInfo.UpdatedBy;
        else this.view.lblData2.text = "N/A";
        this.view.flxUpdatedOn.setVisibility(true);
        this.view.flxUpdatedBy.setVisibility(true);
        this.view.forceLayout();
    },
    CustomerProfileAccountsPreshow: function() {
        this.view.tabs.setSkinForInfoTabs(this.view.tabs.btnTabName2);
        this.view.Notes.setDefaultNotesData(this);
        var screenHeight = kony.os.deviceInfo().screenHeight;
        this.view.flxMainContent.height = screenHeight - 135 + "px";
        this.view.flxGeneralInfoWrapper.changeSelectedTabColour(this.view.flxGeneralInfoWrapper.dashboardCommonTab.btnProfile);
        this.view.flxGeneralInfoWrapper.generalInfoHeader.setDefaultHeaderData(this);
        this.view.flxGeneralInfoWrapper.setFlowActionsForGeneralInformationComponent(this);
        this.view.lblCardType.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CardType");
        this.AdminConsoleCommonUtils.setVisibility(this.view.flxTransactionHistorySegmentHeader, true);
        this.AdminConsoleCommonUtils.setVisibility(this.view.flxSeperator4, true);
        this.AdminConsoleCommonUtils.setVisibility(this.view.rtxMsgTransctions, false);
        this.view.flxProductHeaderAndDetails.bottom = "20px";
        this.view.flxProductHeaderAndDetails.top = "35px";
        this.view.flxProductHeaderAndDetails.left = "35px";
        this.view.flxUpdatedOn.setVisibility(false);
        this.view.flxUpdatedBy.setVisibility(false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.flxProductDetailsHeader, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.productHeader.lblSeperator, false);
        this.view.flxSubcriptionContainer.info = {
            "clicked": false
        };
        this.setFlowActions();
    },
    setFlowActions: function() {
        var scopeObj = this;
        this.view.btnTabName1.onClick = function() {
            scopeObj.setSkinForHistoryTabs(scopeObj.view.btnTabName1);
            kony.adminConsole.utils.showProgressBar(scopeObj.view);
            scopeObj.setDataForProductTransactionSegment(scopeObj.successLabel);
            kony.adminConsole.utils.hideProgressBar(scopeObj.view);
        };
        this.view.btnTabName2.onClick = function() {
            scopeObj.setSkinForHistoryTabs(scopeObj.view.btnTabName2);
            kony.adminConsole.utils.showProgressBar(scopeObj.view);
            scopeObj.setDataForProductTransactionSegment(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Pending"));
            kony.adminConsole.utils.hideProgressBar(scopeObj.view);
        };
        this.view.btnTabName3.onClick = function() {
            scopeObj.setSkinForHistoryTabs(scopeObj.view.btnTabName3);
            kony.adminConsole.utils.showProgressBar(scopeObj.view);
            scopeObj.setDataForProductTransactionSegment(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Scheduled"));
            kony.adminConsole.utils.hideProgressBar(scopeObj.view);
        };
        this.view.transactionHistorySearch.lbxPageNumbers.onSelection = function() {
            var acctNo = scopeObj.view.segProductListing.selecteditems[0].lblAccountNumber;
            var endDate, startDate;
            scopeObj.view.calStartDate.setVisibility(false);
            scopeObj.view.transactionHistorySearch.flxStartDate.setVisibility(false);
            scopeObj.view.calEndDate.setVisibility(false);
            scopeObj.view.transactionHistorySearch.flxEndDate.setVisibility(false);
            if (scopeObj.view.transactionHistorySearch.lbxPageNumbers.selectedKey === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.7days")) {
                if (scopeObj.view.transactionHistorySearch.lbxPageNumbers.info.previousSelection !== kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.7days")) {
                    endDate = scopeObj.getTransactionDateForServiceCall(new Date());
                    startDate = scopeObj.getTransactionDateCustom(7, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DAYS"));
                    scopeObj.presenter.getAccountTransactions({
                        "AccountNumber": acctNo,
                        "StartDate": startDate,
                        "EndDate": endDate
                    });
                }
                scopeObj.view.transactionHistorySearch.lbxPageNumbers.info = {
                    "previousSelection": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.7days")
                };
            } else if (scopeObj.view.transactionHistorySearch.lbxPageNumbers.selectedKey === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.30days")) {
                if (scopeObj.view.transactionHistorySearch.lbxPageNumbers.info.previousSelection !== kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.30days")) {
                    endDate = scopeObj.getTransactionDateForServiceCall(new Date());
                    startDate = scopeObj.getTransactionDateCustom(30, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DAYS"));
                    scopeObj.presenter.getAccountTransactions({
                        "AccountNumber": acctNo,
                        "StartDate": startDate,
                        "EndDate": endDate
                    });
                }
                scopeObj.view.transactionHistorySearch.lbxPageNumbers.info = {
                    "previousSelection": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.30days")
                };
            } else if (scopeObj.view.transactionHistorySearch.lbxPageNumbers.selectedKey === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.3months")) {
                if (scopeObj.view.transactionHistorySearch.lbxPageNumbers.info.previousSelection !== kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.3months")) {
                    endDate = scopeObj.getTransactionDateForServiceCall(new Date());
                    startDate = scopeObj.getTransactionDateCustom(3, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.MONTHS"));
                    scopeObj.presenter.getAccountTransactions({
                        "AccountNumber": acctNo,
                        "StartDate": startDate,
                        "EndDate": endDate
                    });
                }
                scopeObj.view.transactionHistorySearch.lbxPageNumbers.info = {
                    "previousSelection": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.3months")
                };
            } else if (scopeObj.view.transactionHistorySearch.lbxPageNumbers.selectedKey === "custom") {
                scopeObj.view.calStartDate.setVisibility(true);
                scopeObj.view.transactionHistorySearch.flxStartDate.setVisibility(true);
                scopeObj.view.calEndDate.setVisibility(true);
                scopeObj.view.transactionHistorySearch.flxEndDate.setVisibility(true);
                scopeObj.view.calStartDate.value = "";
                scopeObj.view.calEndDate.value = "";
                scopeObj.view.transactionHistorySearch.lbxPageNumbers.info = {
                    "previousSelection": "custom"
                };
            }
        };
        this.view.customCalCreatedDate.event = function() {
            scopeObj.view.transactionHistorySearch.flxStartDate.skin = "sknFlxCalendar";
            scopeObj.view.transactionHistorySearch.flxEndDate.skin = "sknFlxCalendar";
            if (scopeObj.view.calStartDate.value !== "" && scopeObj.view.calEndDate.value !== "") {
                var endDate = scopeObj.getTransactionDateCustom(scopeObj.view.calEndDate.value, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOM"));
                var startDate = scopeObj.getTransactionDateCustom(scopeObj.view.calStartDate.value, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOM"));
                if (scopeObj.compareStartAndEndDate(scopeObj.view.calStartDate.value, scopeObj.view.calEndDate.value)) {
                    var acctNo = scopeObj.view.segProductListing.selecteditems[0].lblAccountNumber;
                    scopeObj.presenter.getAccountTransactions({
                        "AccountNumber": acctNo,
                        "StartDate": startDate,
                        "EndDate": endDate
                    });
                } else {
                    scopeObj.view.transactionHistorySearch.flxStartDate.skin = "sknFlxCalendarError";
                }
            }
        };
        this.view.calEndDate.event = function() {
            scopeObj.view.transactionHistorySearch.flxStartDate.skin = "sknFlxCalendar";
            scopeObj.view.transactionHistorySearch.flxEndDate.skin = "sknFlxCalendar";
            if (scopeObj.view.calStartDate.value !== "" && scopeObj.view.calEndDate.value !== "") {
                var endDate = scopeObj.getTransactionDateCustom(scopeObj.view.calEndDate.value, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOM"));
                var startDate = scopeObj.getTransactionDateCustom(scopeObj.view.calStartDate.value, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOM"));
                if (scopeObj.compareStartAndEndDate(scopeObj.view.calStartDate.value, scopeObj.view.calEndDate.value)) {
                    var acctNo = scopeObj.view.segProductListing.selecteditems[0].lblAccountNumber;
                    scopeObj.presenter.getAccountTransactions({
                        "AccountNumber": acctNo,
                        "StartDate": startDate,
                        "EndDate": endDate
                    });
                } else {
                    scopeObj.view.transactionHistorySearch.flxEndDate.skin = "sknFlxCalendarError";
                }
            }
        };
        this.view.segProductListing.onRowClick = function() {
            scopeObj.setSkinForAccountTabs(scopeObj.view.btnTransactionHistory);
            scopeObj.setSkinForHistoryTabs(scopeObj.view.btnTabName1);
            scopeObj.view.btnTransactionHistory.width = "160px";
            scopeObj.view.btnProductDetails.left = "205px";
            scopeObj.view.flxProductHeader.bottom = "0px";
            var acctNo = scopeObj.view.segProductListing.selecteditems[0].lblAccountNumber;
            var endDate = scopeObj.getTransactionDateForServiceCall(new Date());
            var startDate = scopeObj.getTransactionDateCustom(7, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DAYS"));
            scopeObj.presenter.getAccountTransactions({
                "AccountNumber": acctNo,
                "StartDate": startDate,
                "EndDate": endDate
            });
            scopeObj.view.forceLayout();
        };
        //search on transaction history
        this.view.transactionHistorySearch.tbxSearchBox.onBeginEditing = function() {
            scopeObj.view.transactionHistorySearch.flxSearchContainer.skin = "sknflx0cc44f028949b4cradius30px";
        };
        this.view.transactionHistorySearch.tbxSearchBox.onEndEditing = function() {
            scopeObj.view.transactionHistorySearch.flxSearchContainer.skin = "sknflxBgffffffBorderc1c9ceRadius30px";
        };
        this.view.transactionHistorySearch.tbxSearchBox.onKeyUp = function() {
            scopeObj.view.transactionHistorySearch.flxClearSearchImage.setVisibility(true);
            var searchParameters = [{
                "searchKey": "lblRefNo",
                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
            }, {
                "searchKey": "lblDateAndTime",
                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
            }, {
                "searchKey": "lblTransctionDescription",
                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
            }, {
                "searchKey": "lblType",
                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
            }, {
                "searchKey": "lblAmount",
                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
            }];
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                "flxRecurrIcon": "flxRecurrIcon",
                "lblLimitsIcon": "lblLimitsIcon",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "template": "flxCustMangTransctionHistory"
            };
            var listOfWidgetsToHide = [scopeObj.view.flxTransactionHistorySegmentHeader, scopeObj.view.flxSeperator4];
            scopeObj.search(scopeObj.view.segTransactionHistory, searchParameters, scopeObj.view.rtxMsgTransctions, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.TOEND"), scopeObj.recordsSize, toAdd, listOfWidgetsToHide);
        };
        this.view.transactionHistorySearch.flxClearSearchImage.onClick = function() {
            scopeObj.view.transactionHistorySearch.tbxSearchBox.text = "";
            scopeObj.view.transactionHistorySearch.flxClearSearchImage.setVisibility(false);
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                "flxRecurrIcon": "flxRecurrIcon",
                "lblLimitsIcon": "lblLimitsIcon",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "template": "flxCustMangTransctionHistory"
            };
            var listOfWidgetsToHide = [scopeObj.view.flxTransactionHistorySegmentHeader, scopeObj.view.flxSeperator4];
            scopeObj.clearSearch(scopeObj.view.segTransactionHistory, scopeObj.view.rtxMsgTransctions, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.TOEND"), scopeObj.recordsSize, toAdd, listOfWidgetsToHide);
        };
        this.view.btnApply.onClick = function() {
            scopeObj.advSearchUsers();
        };
        this.view.ProductCardRow2.btnLink2.onClick = function() {
            scopeObj.navigateToCustomerInfoPage(scopeObj.primaryUsername);
        };
        this.view.segProductCardListing.onRowClick = function() {
            scopeObj.view.flxProductHeader.isVisible = true;
            scopeObj.view.flxProductsAccounts.setVisibility(false);
            scopeObj.view.flxProductsCards.setVisibility(false);
            scopeObj.view.flxProductInfoTabs.setVisibility(false);
            scopeObj.view.flxProductHeaderAndDetails.setVisibility(false);
            scopeObj.view.flxTransactionHistoryWrapper.setVisibility(false);
            scopeObj.view.flxProductsTabs.setVisibility(false);
            scopeObj.view.flxProductDetails.setVisibility(true);
            scopeObj.view.flxProductCardDetails.setVisibility(true);
            scopeObj.view.flxBackToProductListing.setVisibility(true);
            scopeObj.view.productHeader.lblSeperator.setVisibility(true);
            //Setting Header
            scopeObj.view.backToProductListing.btnBack.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Back_To_Cards");
            var index = scopeObj.view.segProductCardListing.selectedRowIndex[1];
            var rowData = scopeObj.view.segProductCardListing.data[index];
            scopeObj.primaryUsername = rowData.cardHolder_username;
            scopeObj.view.productHeader.lblProductCardName.text = rowData.product_cardProductName !== undefined ? rowData.product_cardProductName : "N/A";
            scopeObj.view.productHeader.blAccountNumber.text = rowData.product_maskedAccountNumber !== undefined ? rowData.product_maskedAccountNumber : "N/A";
            scopeObj.view.productHeader.lblCardStatus.text = rowData.product_cardStatus !== undefined ? rowData.product_cardStatus : "N/A";
            scopeObj.view.productHeader.fontIconCardStatus.isVisible = false;
            scopeObj.setCardOrAccountNumber(rowData.product_maskedAccountNumber);
            scopeObj.view.productHeader.lblCardStatus.skin = "sknFontIconOptionMenu";
            // Setting data      
            if (rowData.product_cardType === "Credit") {
                scopeObj.setCreditCardHeader();
                scopeObj.view.ProductCardRow1.lblData1.text = rowData.product_cardType !== undefined ? rowData.product_cardType : "N/A";
                scopeObj.view.ProductCardRow1.lblData2.text = rowData.product_accountName !== undefined ? rowData.product_accountName : "N/A";
                scopeObj.view.ProductCardRow1.lblData3.text = rowData.product_maskedCardNumber !== undefined ? rowData.product_maskedCardNumber : "N/A";
                scopeObj.view.ProductCardRow2.lblData1.text = rowData.product_serviceProvider !== undefined ? rowData.product_serviceProvider : "N/A";
                scopeObj.view.ProductCardRow2.lblData2.text = rowData.product_cardHolderName !== undefined ? rowData.product_cardHolderName : "N/A";
                scopeObj.view.ProductCardRow2.lblData3.text = rowData.product_interestRate !== undefined ? rowData.product_interestRate : "N/A";
                scopeObj.view.ProductCardRow3.lblData1.text = rowData.product_issuedOn !== undefined ? scopeObj.getLocaleDate(rowData.product_issuedOn.substr(0, 10)) : "N/A";
                scopeObj.view.ProductCardRow3.lblData2.text = rowData.product_expiryDate !== undefined ? scopeObj.getLocaleDate(rowData.product_expiryDate.substr(0, 10)) : "N/A";
                scopeObj.view.ProductCardRow3.lblData3.text = rowData.product_currentBalance !== undefined ? rowData.product_currentBalance : "N/A";
                scopeObj.view.ProductCardRow4.lblData1.text = rowData.product_withdrawlLimit !== undefined ? rowData.product_withdrawlLimit : "N/A";
                scopeObj.view.ProductCardRow4.lblData2.text = rowData.product_currentDueAmount !== undefined ? rowData.product_currentDueAmount : "N/A";
                scopeObj.view.ProductCardRow4.lblData3.text = rowData.product_rewardPointBalance !== undefined ? rowData.product_rewardPointBalance : "N/A";
                scopeObj.view.ProductCardRow5.lblData1.text = rowData.product_availableCredit !== undefined ? rowData.product_availableCredit : "N/A";
                scopeObj.view.ProductCardRow5.lblData2.text = rowData.product_currentDueDate !== undefined ? rowData.product_currentDueDate : "N/A";
                scopeObj.view.ProductCardRow5.lblData3.text = rowData.product_minimumDueAmount !== undefined ? rowData.product_minimumDueAmount : "N/A";
                scopeObj.view.ProductCardRow6.lblData1.text = rowData.product_lastStatementBalance !== undefined ? rowData.product_lastStatementBalance : "N/A";
                scopeObj.view.ProductCardRow6.lblData3.text = rowData.product_lastPaymentDate !== undefined ? rowData.product_lastPaymentDate.substr(0, 10) : "N/A";
                scopeObj.view.ProductCardRow6.lblData3.text = rowData.product_lastStatementPayment !== undefined ? rowData.product_lastStatementPayment : "N/A";
                scopeObj.view.ProductCardRow7.lblData1.text = rowData.billingAddress !== undefined ? rowData.billingAddress : "N/A";
            }
            if (rowData.product_cardType === "Debit") {
                scopeObj.setDebitCardHeader();
                scopeObj.view.ProductCardRow1.lblData1.text = rowData.product_cardType !== undefined ? rowData.product_cardType : "N/A";
                scopeObj.view.ProductCardRow1.lblData2.text = rowData.product_accountName !== undefined ? rowData.product_accountName : "N/A";
                scopeObj.view.ProductCardRow1.lblData3.text = rowData.product_maskedCardNumber !== undefined ? rowData.product_maskedCardNumber : "N/A";
                scopeObj.view.ProductCardRow2.lblData1.text = rowData.product_serviceProvider !== undefined ? rowData.product_serviceProvider : "N/A";
                scopeObj.view.ProductCardRow2.lblData2.text = rowData.product_cardHolderName !== undefined ? rowData.product_cardHolderName : "N/A";
                scopeObj.view.ProductCardRow2.lblData3.text = rowData.product_secondaryCardHolder !== undefined ? rowData.product_secondaryCardHolder : "N/A";
                scopeObj.view.ProductCardRow3.lblData1.text = rowData.product_issuedOn !== undefined ? scopeObj.getLocaleDate(rowData.product_issuedOn.substr(0, 10)) : "N/A";
                scopeObj.view.ProductCardRow3.lblData2.text = rowData.product_expiryDate !== undefined ? scopeObj.getLocaleDate(rowData.product_expiryDate.substr(0, 10)) : "N/A";
                scopeObj.view.ProductCardRow3.lblData3.text = rowData.product_currentBalance !== undefined ? rowData.product_rewardPointBalance : "N/A";
            }
        };
        this.view.backToProductListing.btnBack.onClick = function() {
            if (scopeObj.view.backToProductListing.btnBack.text === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Back_To_Cards")) {
                scopeObj.showProductCardScreen();
            } else {
                scopeObj.showProductInfoScreen();
            }
        };
        this.view.btnProductDetails.onClick = function() {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.setSkinForAccountTabs(scopeObj.view.btnProductDetails);
            scopeObj.showProductDetailsScreen();
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        this.view.btnTransactionHistory.onClick = function() {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.setSkinForAccountTabs(scopeObj.view.btnTransactionHistory);
            scopeObj.showProductTransactionHistory();
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        //sorting on accounts
        this.view.flxProductType.onClick = function() {
            scopeObj.resetAllSortImagesAccounts();
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segProductListing, "lblProductType", scopeObj.view.fonticonSortProductType, scopeObj.view.flxOtherInfoWrapper, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ALL"), null, scopeObj);
        };
        this.view.flxProductName.onClick = function() {
            scopeObj.resetAllSortImagesAccounts();
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segProductListing, "lblProductName", scopeObj.view.fonticonSortProductName, scopeObj.view.flxOtherInfoWrapper, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ALL"), null, scopeObj);
        };
        this.view.flxAccountNumber.onClick = function() {
            scopeObj.resetAllSortImagesAccounts();
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segProductListing, "lblAccountNumber", scopeObj.view.fonticonSortAccountNumber, scopeObj.view.flxOtherInfoWrapper, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ALL"), null, scopeObj);
        };
        this.view.flxAccountOwner.onClick = function() {
            scopeObj.resetAllSortImagesAccounts();
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segProductListing, "lblAccountOwner", scopeObj.view.fonticonSortAccountOwner, scopeObj.view.flxOtherInfoWrapper, "ALL", null, scopeObj);
        };
        this.view.flxAmount.onClick = function() {
            scopeObj.resetAllSortImagesAccounts();
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segProductListing, "lblStatus", scopeObj.view.fonticonSortAmount, scopeObj.view.flxOtherInfoWrapper, "ALL", null, scopeObj);
        };
        //sorting on transactions
        this.view.flxTranasctionRefNo.onClick = function() {
            scopeObj.resetAllSortImagesTransactions();
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                "flxRecurrIcon": "flxRecurrIcon",
                "lblLimitsIcon": "lblLimitsIcon",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "template": "flxCustMangTransctionHistory"
            };
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segTransactionHistory, "lblRefNo", scopeObj.view.fonticonSortTranasctionRefNo, "TOEND", scopeObj.recordsSize, toAdd, scopeObj);
        };
        this.view.flxTransactionDateAndTime.onClick = function() {
            scopeObj.resetAllSortImagesTransactions();
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                "flxRecurrIcon": "flxRecurrIcon",
                "lblLimitsIcon": "lblLimitsIcon",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "template": "flxCustMangTransctionHistory"
            };
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segTransactionHistory, "lblDateAndTime", scopeObj.view.fonticonSortTransactionDateAndTime, "TOEND", scopeObj.recordsSize, toAdd, scopeObj);
        };
        this.view.flxTransactionType.onClick = function() {
            scopeObj.resetAllSortImagesTransactions();
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                "flxRecurrIcon": "flxRecurrIcon",
                "lblLimitsIcon": "lblLimitsIcon",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "template": "flxCustMangTransctionHistory"
            };
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segTransactionHistory, "lblType", scopeObj.view.fonticonSortTransactionType, "TOEND", scopeObj.recordsSize, toAdd, scopeObj);
        };
        this.view.flxTransactionAmountOriginal.onClick = function() {
            scopeObj.resetAllSortImagesTransactions();
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                "flxRecurrIcon": "flxRecurrIcon",
                "lblLimitsIcon": "lblLimitsIcon",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "template": "flxCustMangTransctionHistory"
            };
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segTransactionHistory, "lblAmount", scopeObj.view.fonticonSortTransactionAmountOriginal, "TOEND", scopeObj.recordsSize, toAdd, scopeObj);
        };
        this.view.flxTransactionAmountConverted.onClick = function() {
            scopeObj.resetAllSortImagesTransactions();
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                "flxRecurrIcon": "flxRecurrIcon",
                "lblLimitsIcon": "lblLimitsIcon",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "template": "flxCustMangTransctionHistory"
            };
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segTransactionHistory, "lblAmount", scopeObj.view.fonticonSortTransactionAmountConverted, "TOEND", scopeObj.recordsSize, toAdd, scopeObj);
        };
        this.view.btnProductsAccounts.onClick = function() {
            scopeObj.resetAllSortImagesAccounts();
            var username = scopeObj.presenter.getCurrentCustomerDetails().Username;
            scopeObj.presenter.getCustomerAccounts({
                "CustomerUsername": username
            });
        };
        this.view.btnProductsCards.onClick = function() {
            scopeObj.setSkinForProductTabs(scopeObj.view.btnProductsCards);
            scopeObj.view.flxProductsAccounts.setVisibility(false);
            scopeObj.view.flxProductsCards.setVisibility(true);
            scopeObj.presenter.getCardsInformation({
                "customerUsername": scopeObj.presenter.getCurrentCustomerDetails().Username
            });
        };
        //Cards
        this.view.cardContextualMenu.btnLink1.onClick = function() {
            scopeObj.notificationTabOnClick();
        };
        this.view.cardContextualMenu.btnLink2.onClick = function() {
            scopeObj.requestTabOnClick();
        };
        this.view.cardContextualMenu.flxOption1.onClick = function() {
            scopeObj.view.flxPopUpConfirmation.setVisibility(true);
            scopeObj.view.popUpConfirmation.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Mark_as_lost_stolen");
            scopeObj.view.popUpConfirmation.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.MARK_AS_LOST_MSG1") + "" + kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.MARK_AS_LOST_MSG2");
            //"Are you sure you want to mark the selected card as Lost / Stolen?<BR><Br>This action cannot be reversed and the user will be required to request for a new card. Are you sure you want to mark this card as lost or stolen?"
            scopeObj.view.popUpConfirmation.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.YES_MARK_AS_LOST"); //"YES, MARK AS LOST";
            //
            scopeObj.view.popUpConfirmation.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Redirect_CancelMsg");
            scopeObj.view.popUpConfirmation.btnPopUpDelete.onClick = function() {
                scopeObj.view.flxPopUpConfirmation.setVisibility(false);
                scopeObj.presenter.updateCardStatus({
                    "customerUsername": scopeObj.presenter.getCurrentCustomerDetails().Username, //"konyolbuser",
                    "cardNumber": scopeObj.cardNumber, //Mandatory
                    "cardAction": "LOST_STOLEN_CARD", //LOCK_CARD||UNLOCK_CARD||||ACTIVATE_CARD||DEACTIVATE_CARD
                    "actionReason": "" //Optional
                });
            };
        };
        this.view.cardContextualMenu.flxOption2.onClick = function() {
            if (scopeObj.view.cardContextualMenu.lblOption2.text === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Lock")) {
                scopeObj.view.popUpConfirmation.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Lock_Card"); //"Lock Card";
                scopeObj.view.popUpConfirmation.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Lock_Card_MSG1") + "" + kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Lock_Card_MSG2");
                //  "Are you sure you want to lock the selected card? <BR><BR>The user will not be able to use this card till unlocked. Are you sure you want to temporarily lock this card?";
                scopeObj.view.popUpConfirmation.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.YES_LOCK"); //"YES, LOCK";//
                scopeObj.view.popUpConfirmation.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Redirect_CancelMsg");
                scopeObj.view.flxPopUpConfirmation.setVisibility(true);
                scopeObj.view.popUpConfirmation.btnPopUpDelete.onClick = function() {
                    scopeObj.view.flxPopUpConfirmation.setVisibility(false);
                    scopeObj.presenter.updateCardStatus({
                        "customerUsername": scopeObj.presenter.getCurrentCustomerDetails().Username, //"konyolbuser",
                        "cardNumber": scopeObj.cardNumber, //Mandatory
                        "cardAction": "LOCK_CARD", //LOCK_CARD||UNLOCK_CARD||||ACTIVATE_CARD||DEACTIVATE_CARD
                        "actionReason": "" //Optional
                    });
                };
            } else if (scopeObj.view.cardContextualMenu.lblOption2.text === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Unlock")) {
                scopeObj.view.popUpConfirmation.lblPopUpMainMessage.text = "Unlock Card";
                scopeObj.view.popUpConfirmation.rtxPopUpDisclaimer.text = "Are you sure you want to unlock the selected card?";
                scopeObj.view.popUpConfirmation.btnPopUpDelete.text = "YES, UNLOCK";
                scopeObj.view.popUpConfirmation.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS");
                scopeObj.view.flxPopUpConfirmation.setVisibility(true);
                scopeObj.view.popUpConfirmation.btnPopUpDelete.onClick = function() {
                    scopeObj.view.flxPopUpConfirmation.setVisibility(false);
                    scopeObj.presenter.updateCardStatus({
                        "customerUsername": scopeObj.presenter.getCurrentCustomerDetails().Username,
                        "cardNumber": scopeObj.cardNumber, //Mandatory
                        "cardAction": "ACTIVATE_CARD", //LOCK_CARD||UNLOCK_CARD||||ACTIVATE_CARD||DEACTIVATE_CARD
                        "actionReason": "" //Optional
                    });
                }
            };
        };
        this.view.transactionHistorySearch.flxDownload.onClick = function() {
            scopeObj.downloadTransactionsAsCSV();
        };
        this.view.flxCardType.onClick = function() {
            scopeObj.resetAllSortImagesCards();
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segProductCardListing, "lblCardType", scopeObj.view.fontIconSortCardType, "NONE", "ALL", null, scopeObj);
        };
        this.view.flxCardName.onClick = function() {
            scopeObj.resetAllSortImagesCards();
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segProductCardListing, "lblCardName", scopeObj.view.fontIconSortCardName, "NONE", "ALL", null, scopeObj);
        };
        this.view.flxCardNumber.onClick = function() {
            scopeObj.resetAllSortImagesCards();
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segProductCardListing, "lblCardNumber", scopeObj.view.fontIconSortCardNumber, "NONE", "ALL", null, scopeObj);
        };
        this.view.flxCardHolder.onClick = function() {
            scopeObj.resetAllSortImagesCards();
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segProductCardListing, "lblCardHolder", scopeObj.view.fontIconSortCardHolder, "NONE", "ALL", null, scopeObj);
        };
        this.view.flxStatus.onClick = function() {
            scopeObj.resetAllSortImagesCards();
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segProductCardListing, "lblServiceStatus", scopeObj.view.fontIconSortStatus, "NONE", "ALL", null, scopeObj);
        };
        this.view.flxImgRbPaper.onClick = function() {
            if (scopeObj.view.imgRbPaper.src === scopeObj.AdminConsoleCommonUtils.radioNotSelected) {
                var confirmAction = function() {
                    scopeObj.view.flxSubscriptionsEmail.setVisibility(false);
                    scopeObj.view.imgRbEstatement.src = scopeObj.AdminConsoleCommonUtils.radioNotSelected;
                    scopeObj.view.imgRbPaper.src = scopeObj.AdminConsoleCommonUtils.radioSelected;
                    scopeObj.updateEstatementStatus(false);
                };
                var cancelAction = function() {
                    scopeObj.view.imgRbEstatement.src = scopeObj.AdminConsoleCommonUtils.radioSelected;
                    scopeObj.view.imgRbPaper.src = scopeObj.AdminConsoleCommonUtils.radioNotSelected;
                };
                scopeObj.AdminConsoleCommonUtils.openConfirm({
                    header: "Enable paper statement",
                    message: "Are you sure to enable paper statement?<br><br>Once paper statement service is enabled, the customer will stop receiving e-Statements for the selected accounts.",
                    confirmAction: confirmAction,
                    cancelMsg: kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS"),
                    cancelAction: cancelAction,
                    confirmMsg: 'YES, ENABLE'
                }, scopeObj);
            }
        };
        this.view.flxImgRbEstatement.onClick = function() {
            if (scopeObj.view.imgRbEstatement.src === scopeObj.AdminConsoleCommonUtils.radioNotSelected) {
                var email = [];
                for (var i = 0; i < scopeObj.presenter.getCurrentCustomerEmailInformation().emails.length; ++i) {
                    if (scopeObj.presenter.getCurrentCustomerEmailInformation().emails[i] === scopeObj.presenter.getCurrentCustomerEmailInformation().primaryEmail) {
                        var selectedEmail = scopeObj.presenter.getCurrentCustomerEmailInformation().primaryEmail + " (Primary email)";;
                        email.push([selectedEmail, selectedEmail]);
                    } else {
                        email.push([scopeObj.presenter.getCurrentCustomerEmailInformation().emails[i], scopeObj.presenter.getCurrentCustomerEmailInformation().emails[i]]);
                    }
                }
                scopeObj.view.lbxEstatementEmail.masterData = email;
                scopeObj.view.lbxEstatementEmail.selectedKey = scopeObj.presenter.getCurrentCustomerEmailInformation().primaryEmail + " (Primary email)";
                scopeObj.view.flxEnableEstatement.setVisibility(true);
                scopeObj.view.forceLayout();
            }
        };
        this.view.flxSubcriptionContainer.onTouchStart = function() {
            var email = [];
            for (var i = 0; i < scopeObj.presenter.getCurrentCustomerEmailInformation().emails.length; ++i) {
                if (scopeObj.presenter.getCurrentCustomerEmailInformation().emails[i] === scopeObj.presenter.getCurrentCustomerEmailInformation().primaryEmail) {
                    var selectedEmail = scopeObj.presenter.getCurrentCustomerEmailInformation().primaryEmail + " (Primary email)";;
                    email.push([selectedEmail, selectedEmail]);
                } else {
                    email.push([scopeObj.presenter.getCurrentCustomerEmailInformation().emails[i], scopeObj.presenter.getCurrentCustomerEmailInformation().emails[i]]);
                }
            }
            scopeObj.view.lbxEstatementEmail.masterData = email;
            if (scopeObj.presenter.getCurrentCustomerEmailInformation().primaryEmail === scopeObj.view.lblSubscriptionsEmail.text) {
                scopeObj.view.lbxEstatementEmail.selectedKey = scopeObj.view.lblSubscriptionsEmail.text + " (Primary email)";
            } else {
                scopeObj.view.lbxEstatementEmail.selectedKey = scopeObj.view.lblSubscriptionsEmail.text;
            }
            scopeObj.view.lbxEstatementEmail.setVisibility(true);
            scopeObj.view.flxEnableEstatement.setVisibility(true);
            scopeObj.view.flxSubcriptionContainer.info.clicked = true;
        };
        // e-Statement
        this.view.flxEstatementClose.onClick = function() {
            scopeObj.view.flxEnableEstatement.setVisibility(false);
            if (scopeObj.view.flxSubcriptionContainer.info.clicked === true) {
                scopeObj.view.flxSubcriptionContainer.info.clicked = false;
            }
        };
        this.view.btnEstatementLeave.onClick = function() {
            scopeObj.view.flxEnableEstatement.setVisibility(false);
            if (scopeObj.view.flxSubcriptionContainer.info.clicked === true) {
                scopeObj.view.flxSubcriptionContainer.info.clicked = false;
            } else {
                scopeObj.view.imgRbPaper.src = scopeObj.AdminConsoleCommonUtils.radioSelected;
                scopeObj.view.imgRbEstatement.src = scopeObj.AdminConsoleCommonUtils.radioNotSelected;
            }
        };
        this.view.btnEstatementEnable.onClick = function() {
            scopeObj.view.flxSubcriptionContainer.info.clicked = false;
            scopeObj.view.flxEnableEstatement.setVisibility(false);
            var eStatementEmail = scopeObj.view.lbxEstatementEmail.selectedKeyValue[1];
            if (eStatementEmail.indexOf(" (Primary email)") != -1) {
                eStatementEmail = eStatementEmail.substring(0, eStatementEmail.indexOf(" (Primary email)"));
                scopeObj.view.lblSubscriptionsEmail.text = eStatementEmail;
            } else {
                scopeObj.view.lblSubscriptionsEmail.text = scopeObj.view.lbxEstatementEmail.selectedKeyValue[1];
            }
            scopeObj.view.flxSubscriptionsEmail.setVisibility(true);
            scopeObj.view.imgRbPaper.src = scopeObj.AdminConsoleCommonUtils.radioNotSelected;
            scopeObj.view.imgRbEstatement.src = scopeObj.AdminConsoleCommonUtils.radioSelected;
            scopeObj.view.forceLayout();
            scopeObj.updateEstatementStatus(true);
        };
    },
    setDataForProductDetailsScreen: function(CustomerAccounts) {
        var scopeObj = this;
        var customerDetails = CustomerAccounts[0];
        var status = customerDetails.statusDesc || kony.i18n.getLocalizedString("i18n.frmCustomers.NA");
        //set data
        this.view.backToProductListing.btnBack.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ACCOUNTS_BTN_BACK");
        this.view.productHeader.lblProductCardName.text = customerDetails.accountName;
        this.view.productHeader.lblCardStatus.text = status;
        var StatusImg;
        //TODO: all images for status
        if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.secureimage.Active").toUpperCase()) {
            StatusImg = "sknFontIconActivate";
        } else if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomers.Suspended").toUpperCase()) {
            StatusImg = "sknFontIconSuspend";
        } else if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomers.Closed").toUpperCase()) {
            StatusImg = "sknfontIconInactive";
        } else if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomers.NA").toUpperCase()) {
            StatusImg = "sknfontIconInactive";
        }
        this.view.productHeader.fontIconCardStatus.skin = StatusImg;
        if (customerDetails.IBAN) {
            this.view.productHeader.lblIBAN.isVisible = true;
            this.view.productHeader.lblIBANLabel.isVisible = true;
            this.view.productHeader.lblIBAN.text = customerDetails.IBAN;
            this.view.productHeader.lblIBAN.width = (8.75 * (customerDetails.IBAN).length) + "px";
            this.view.productHeader.lblIBAN.right = "0px";
            this.view.productHeader.lblIBANLabel.right = ((8.75 * (customerDetails.IBAN).length)) + "px";
        } else {
            this.view.productHeader.lblIBAN.isVisible = false;
            this.view.productHeader.lblIBANLabel.isVisible = false;
        }
        if (customerDetails.accountID) {
            this.view.productHeader.blAccountNumber.isVisible = true;
            this.view.productHeader.lblAccountNumberLabel.isVisible = true;
            var cardOrAccountNumber = customerDetails.accountID;
            this.view.productHeader.blAccountNumber.text = cardOrAccountNumber;
            this.view.productHeader.blAccountNumber.width = (8.75 * cardOrAccountNumber.length) + "px";
            if (customerDetails.IBAN) {
                var accountNumberRight = (8.75 * (customerDetails.IBAN).length) + 50;
                this.view.productHeader.blAccountNumber.right = accountNumberRight + "px";
                this.view.productHeader.lblAccountNumberLabel.right = (accountNumberRight + (8.75 * cardOrAccountNumber.length)) + "px";
            } else {
                this.view.productHeader.blAccountNumber.right = "0px";
                this.view.productHeader.lblAccountNumberLabel.right = ((8.75 * cardOrAccountNumber.length)) + "px";
            }
        } else {
            this.view.productHeader.blAccountNumber.isVisible = false;
            this.view.productHeader.lblAccountNumberLabel.isVisible = false;
        }
        //row1
        this.view.ProductRow1.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CREATED_ON");
        this.view.ProductRow1.lblData1.text = customerDetails.openingDate ? scopeObj.getLocaleDate(customerDetails.openingDate) : "N/A";
        this.view.ProductRow1.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CURRENT_BALANCE");
        this.view.ProductRow1.lblSignData2.left = "-7px";
        this.view.ProductRow1.lblSignData2.text = customerDetails.currentBalance.substring(0, 1) === "-" ? "-" : "";
        this.view.ProductRow1.lblIconData2.text = this.defaultCurrencyCode(customerDetails.currencyCode) ? this.defaultCurrencyCode(customerDetails.currencyCode) : this.defaultCurrencyCode("USD");
        this.view.ProductRow1.lblData2.text = this.formatCurrencyByDeletingSign(this.getCurrencyFormat(customerDetails.currentBalance));
        this.view.ProductRow1.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DIVIDEND_RATE");
        this.view.ProductRow1.lblData3.text = customerDetails.dividendRate ? customerDetails.dividendRate : "N/A";
        //row 2
        this.view.ProductRow2.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.OWNERSHIP");
        this.view.ProductRow2.lblData1.text = customerDetails.jointHolders ? kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.JOINT") : kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SINGLE");
        this.view.ProductRow2.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.AVAILABLE_BALANCE");
        this.view.ProductRow2.lblSignData2.left = "-7px";
        this.view.ProductRow2.lblSignData2.text = customerDetails.availableBalance.substring(0, 1) === "-" ? "-" : "";
        this.view.ProductRow2.lblIconData2.text = this.defaultCurrencyCode(customerDetails.currencyCode) ? this.defaultCurrencyCode(customerDetails.currencyCode) : this.defaultCurrencyCode("USD");
        this.view.ProductRow2.lblData2.text = this.formatCurrencyByDeletingSign(this.getCurrencyFormat(customerDetails.availableBalance));
        this.view.ProductRow2.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DIVIDEND_PAID_(YTD)");
        this.view.ProductRow2.lblData3.text = customerDetails.dividendPaidYTD ? customerDetails.dividendPaidYTD : "N/A";
        //row 3
        this.view.ProductRow3.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.JOINT_HOLDER_NAME");
        //Parse account holder object
        var accountHolder = customerDetails.accountHolder;
        if (accountHolder === null || accountHolder === undefined) {
            accountHolder = "N/A"
        } else if (typeof accountHolder === 'string') {
            try {
                accountHolder = JSON.parse(accountHolder);
            } catch (e) {
                accountHolder = "N/A";
                kony.print("Account Holder is not a valid json object " + e);
            }
        }
        var currentUsername = this.presenter.getCurrentCustomerDetails().Username;
        //Set data for account holder label
        this.view.ProductRow3.btnData11.setVisibility(false);
        this.view.ProductRow3.lblData11.setVisibility(false);
        if (accountHolder !== "N/A") {
            if (currentUsername !== accountHolder.username) {
                this.view.ProductRow3.lblData11.setVisibility(false);
                this.view.ProductRow3.btnData11.setVisibility(true);
                this.view.ProductRow3.btnData11.text = accountHolder.fullname + " (Primary)";
                this.view.ProductRow3.btnData11.onClick = function() {
                    scopeObj.navigateToCustomerInfoPage(accountHolder.username);
                };
            } else {
                this.view.ProductRow3.lblData11.setVisibility(true);
                this.view.ProductRow3.btnData11.setVisibility(false);
                this.view.ProductRow3.lblData11.text = this.view.flxGeneralInfoWrapper.generalInfoHeader.lblCustomerName.info.FirstName + " " + this.view.flxGeneralInfoWrapper.generalInfoHeader.lblCustomerName.info.LastName + " (Primary)";
            }
        } else {
            this.view.ProductRow3.lblData11.setVisibility(true);
            this.view.ProductRow3.lblData11.text = accountHolder;
        }
        var jointHolders = customerDetails.jointHolders;
        if (jointHolders === null || jointHolders === undefined) {
            jointHolders = "N/A";
        } else if (typeof jointHolders === 'string') {
            try {
                jointHolders = JSON.parse(jointHolders);
            } catch (e) {
                jointHolders = "N/A";
                kony.print("Joint Holders is not a valid json Array " + e);
            }
        }
        //Set data for joint holders label
        if (jointHolders !== "N/A") {
            this.view.ProductRow3.lblData21.setVisibility(false);
            this.view.ProductRow3.btnData21.setVisibility(false);
            this.view.ProductRow3.lblComma1.setVisibility(false);
            this.view.ProductRow3.btnData22.setVisibility(false);
            this.view.ProductRow3.lblComma2.setVisibility(false);
            this.view.ProductRow3.btnData23.setVisibility(false);
            var islblData21Set = false,
                isbtnData21Set = false,
                isbtnData22Set = false,
                isbtnData23Set = false;
            jointHolders = jointHolders.filter(function(item) {
                return item.username !== accountHolder.username
            });
            jointHolders.forEach(function(jointHolder) {
                var self = scopeObj;
                if (currentUsername === jointHolder.username) {
                    scopeObj.view.ProductRow3.lblData21.setVisibility(true);
                    scopeObj.view.ProductRow3.btnData21.setVisibility(false);
                    scopeObj.view.ProductRow3.lblData21.text = scopeObj.view.flxGeneralInfoWrapper.generalInfoHeader.lblCustomerName.info.FirstName + " " + scopeObj.view.flxGeneralInfoWrapper.generalInfoHeader.lblCustomerName.info.LastName;
                    islblData21Set = true;
                    isbtnData21Set = false;
                } else {
                    if (!isbtnData22Set) {
                        scopeObj.view.ProductRow3.btnData22.setVisibility(true);
                        scopeObj.view.ProductRow3.btnData22.text = jointHolder.fullname;
                        isbtnData22Set = true;
                        scopeObj.view.ProductRow3.btnData22.onClick = function() {
                            self.navigateToCustomerInfoPage(jointHolder.username);
                        };
                    } else if (!isbtnData23Set) {
                        scopeObj.view.ProductRow3.btnData23.setVisibility(true);
                        scopeObj.view.ProductRow3.btnData23.text = jointHolder.fullname.substring(0, 7) + "...";
                        scopeObj.view.ProductRow3.btnData23.toolTip = jointHolder.fullname;
                        isbtnData23Set = true;
                        scopeObj.view.ProductRow3.btnData23.onClick = function() {
                            self.navigateToCustomerInfoPage(jointHolder.username);
                        };
                    } else if (!isbtnData21Set && !islblData21Set) {
                        scopeObj.view.ProductRow3.btnData21.setVisibility(true);
                        scopeObj.view.ProductRow3.btnData21.text = jointHolder.fullname;
                        isbtnData21Set = true;
                        scopeObj.view.ProductRow3.btnData21.onClick = function() {
                            self.navigateToCustomerInfoPage(jointHolder.username);
                        };
                    }
                }
            });
            if (isbtnData22Set && (islblData21Set || isbtnData21Set)) {
                this.view.ProductRow3.lblComma1.setVisibility(true);
            }
            if (isbtnData23Set && isbtnData22Set) {
                this.view.ProductRow3.lblComma2.setVisibility(true);
            }
        } else {
            this.view.ProductRow3.btnData21.setVisibility(false);
            this.view.ProductRow3.lblComma1.setVisibility(false);
            this.view.ProductRow3.btnData22.setVisibility(false);
            this.view.ProductRow3.lblComma2.setVisibility(false);
            this.view.ProductRow3.btnData23.setVisibility(false);
            this.view.ProductRow3.lblData21.setVisibility(false);
        }
        this.view.ProductRow3.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ROUTING_NUMBER");
        this.view.ProductRow3.lblData2.text = customerDetails.routingNumber ? customerDetails.routingNumber : "N/A";
        this.view.ProductRow3.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LAST_DIVIDEND_PAID");
        this.view.ProductRow3.lblData3.text = customerDetails.lastDividendPaidAmount ? customerDetails.lastDividendPaidAmount : "N/A";
        //row 4
        this.view.ProductRow4.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LAST_UPDATE_ON");
        this.view.ProductRow4.lblData1.text = customerDetails.lastPaymentDate ? this.getFormattedDataTimeFromDBDateTime(customerDetails.lastPaymentDate) : "N/A";
        this.view.ProductRow4.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SWIFT_CODE");
        this.view.ProductRow4.lblData2.text = customerDetails.swiftCode === undefined ? "N/A" : customerDetails.swiftCode;
        this.view.ProductRow4.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LAST_DIVIDEND_PAID_ON");
        this.view.ProductRow4.lblData3.text = scopeObj.getLocaleDate(customerDetails.dividendLastPaidDate);
        //row 5
        this.view.ProductRow5.setVisibility(false);
        var eStatementStatus = this.view.segProductListing.selecteditems[0].eStatementStatus;
        if (eStatementStatus === "true") {
            this.view.imgRbEstatement.src = this.AdminConsoleCommonUtils.radioSelected;
            this.view.imgRbPaper.src = this.AdminConsoleCommonUtils.radioNotSelected;
            this.view.lblSubscriptionsEmail.text = this.view.segProductListing.selecteditems[0].eStatementEmail;
            var segRowinfo = this.view.segProductListing.selecteditems[0].info;
            this.view.lblData1.text = segRowinfo && segRowinfo.lastUpdatedOn ? this.getFormattedDataTimeFromDBDateTime(segRowinfo.lastUpdatedOn) : "N/A";
            this.view.lblData2.text = segRowinfo && segRowinfo.updatedBy ? segRowinfo.updatedBy : "N/A";
            this.view.flxUpdatedBy.setVisibility(true);
            this.view.flxUpdatedOn.setVisibility(true);
            this.view.flxSubscriptionsEmail.setVisibility(true);
            this.view.forceLayout();
        } else {
            this.view.imgRbEstatement.src = this.AdminConsoleCommonUtils.radioNotSelected;
            this.view.flxUpdatedBy.setVisibility(false);
            this.view.flxUpdatedOn.setVisibility(false);
            this.view.imgRbPaper.src = this.AdminConsoleCommonUtils.radioSelected;
            this.view.flxSubscriptionsEmail.setVisibility(false);
        }
        //Check for access control
        if (this.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "false") {
            this.view.ProductRow1.lblIconData2.setVisibility(false);
            this.view.ProductRow2.lblIconData2.setVisibility(false);
        } else {
            this.view.ProductRow1.lblIconData2.setVisibility(true);
            this.view.ProductRow2.lblIconData2.setVisibility(true);
        }
    },
    resetAllSortImagesTransactions: function() {
        if (this.view.fonticonSortTranasctionRefNo.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortTranasctionRefNo.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortTranasctionRefNo.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fonticonSortTransactionDateAndTime.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortTransactionDateAndTime.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortTransactionDateAndTime.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fonticonSortTransactionType.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortTransactionType.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortTransactionType.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fonticonSortTransactionAmountOriginal.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortTransactionAmountOriginal.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortTransactionAmountOriginal.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fonticonSortTransactionAmountConverted.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortTransactionAmountConverted.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortTransactionAmountConverted.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
    },
    resetAllSortImagesAccounts: function() {
        if (this.view.fonticonSortProductType.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortProductType.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortProductType.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fonticonSortProductName.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortProductName.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortProductName.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fonticonSortAccountNumber.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortAccountNumber.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortAccountNumber.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fonticonSortAmount.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortAmount.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortAmount.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fonticonSortAccountOwner.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortAccountOwner.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortAccountOwner.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
    },
    setSkinForProductTabs: function(btnWidget) {
        this.view.btnProductsCards.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
        this.view.btnProductsAccounts.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
        btnWidget.skin = "sknbtnBgffffffLato485c75Radius3Px12Px";
    },
    setDataForProductsSegment: function(CustomerAccounts) {
        var scopeObj = this;
        var dataMap = {
            "flxCustMangProduct": "flxCustMangProduct",
            "flxCustMangProductInfo": "flxCustMangProductInfo",
            "flxProductType": "flxProductType",
            "flxStatus": "flxStatus",
            "fontIconStatus": "fontIconStatus",
            "lblAccountNumber": "lblAccountNumber",
            "lblProductName": "lblProductName",
            "lblProductType": "lblProductType",
            "flxAccountOwner": "flxAccountOwner",
            "btnAccountOwner": "btnAccountOwner",
            "lblAccountOwner": "lblAccountOwner",
            "lblSeperator": "lblSeperator",
            "lblStatus": "lblStatus",
            "eStatementStatus": "eStatementStatus",
            "eStatementEmail": "eStatementEmail"
        };
        var data = [];
        var toAdd;
        if (CustomerAccounts.length > 0) {
            for (var i = 0; i < CustomerAccounts.length; i++) {
                var status = CustomerAccounts[i].statusDesc ? (CustomerAccounts[i].statusDesc.charAt(0).toUpperCase() + CustomerAccounts[i].statusDesc.slice(1).toLowerCase()) : kony.i18n.getLocalizedString("i18n.frmCustomers.NA");
                var StatusImg, statusSkin;
                if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.secureimage.Active").toUpperCase()) {
                    StatusImg = "sknFontIconActivate";
                    statusSkin = "sknlblLato5bc06cBold14px";
                } else if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomers.Suspended").toUpperCase()) {
                    StatusImg = "sknFontIconSuspend";
                    statusSkin = "sknlblLatoeab55d12px";
                } else if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomers.Closed").toUpperCase()) {
                    StatusImg = "sknfontIconInactive";
                    statusSkin = "sknlblCustMngLocked";
                } else if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomers.NA").toUpperCase()) {
                    StatusImg = "sknfontIconInactive";
                    statusSkin = "sknlblCustMngLocked"
                }
                var accountHolder = CustomerAccounts[i].accountHolder;
                if (accountHolder === null || accountHolder === undefined) {
                    accountHolder = "N/A";
                } else if (typeof accountHolder === 'string') {
                    try {
                        accountHolder = JSON.parse(accountHolder);
                    } catch (e) {
                        accountHolder = "N/A";
                        kony.print("Account Holder is not a valid json object " + e);
                    }
                }
                //Set data for account holder label
                var currentUsername = this.presenter.getCurrentCustomerDetails().Username,
                    displayUsername, isbtnAccountOwner = false,
                    islblAccountOwner = false;
                if (accountHolder !== "N/A") {
                    if (currentUsername !== accountHolder.username) {
                        displayUsername = accountHolder.fullname;
                        isbtnAccountOwner = true;
                        islblAccountOwner = false;
                    } else {
                        displayUsername = this.view.flxGeneralInfoWrapper.generalInfoHeader.lblCustomerName.info.FirstName + " " + this.view.flxGeneralInfoWrapper.generalInfoHeader.lblCustomerName.info.LastName
                        isbtnAccountOwner = false;
                        islblAccountOwner = true;
                    }
                } else {
                    displayUsername = "N/A";
                    isbtnAccountOwner = false;
                    islblAccountOwner = true;
                }
                toAdd = {
                    "flxCustMangProduct": "flxCustMangProduct",
                    "flxCustMangProductInfo": "flxCustMangProductInfo",
                    "flxProductType": "flxProductType",
                    "flxStatus": "flxStatus",
                    "fontIconStatus": {
                        "skin": StatusImg
                    },
                    "lblAccountNumber": CustomerAccounts[i].accountID ? CustomerAccounts[i].accountID : "N/A",
                    "lblProductName": CustomerAccounts[i].accountName ? CustomerAccounts[i].accountName : "N/A",
                    "lblProductType": CustomerAccounts[i].accountType ? CustomerAccounts[i].accountType : "N/A",
                    "lblSeperator": ".",
                    "btnAccountOwner": {
                        "isVisible": isbtnAccountOwner,
                        "text": displayUsername,
                        "info": accountHolder,
                        "onClick": function(widget) {
                            scopeObj.navigateToCustomerInfoPage(widget.info.username);
                        }
                    },
                    "lblAccountOwner": {
                        "isVisible": islblAccountOwner,
                        "text": displayUsername
                    },
                    "lblStatus": {
                        "text": status,
                        "skin": statusSkin
                    },
                    "eStatementStatus": CustomerAccounts[i].eStatementEnable,
                    "eStatementEmail": CustomerAccounts[i].eStatementEmail,
                    "template": "flxCustMangProductInfo",
                    "info": {
                        "lastUpdatedOn": CustomerAccounts[i].LastUpdated,
                        "updatedBy": CustomerAccounts[i].UpdatedBy
                    }
                };
                data.push(toAdd);
            }
            this.view.segProductListing.widgetDataMap = dataMap;
            this.view.segProductListing.setData(data);
            this.view.rtxMsgProducts.setVisibility(false);
            this.view.flxProductListingHeader.setVisibility(true);
            this.view.lblHeaderSeperator.setVisibility(true);
            this.view.segProductListing.setVisibility(true);
            this.view.segProductListing.info = {
                "data": data,
                "searchAndSortData": data
            };
        } else {
            var customerType = this.presenter.getCurrentCustomerDetails().CustomerType_id;
            if (customerType === "TYPE_ID_SMALL_BUSINESS" || customerType === "TYPE_ID_MICRO_BUSINESS") {
                this.view.rtxMsgProducts.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagement.No_accounts_business_user");
            } else {
                this.view.rtxMsgProducts.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.NO_RECORDS_FOUND");
            }
            this.view.rtxMsgProducts.setVisibility(true);
            this.view.segProductListing.setVisibility(false);
            this.view.flxProductListingHeader.setVisibility(false);
            this.view.lblHeaderSeperator.setVisibility(false);
        }
        this.showProductInfoScreen();
    },
    navigateToCustomerInfoPage: function(customerUsername) {
        var scopeObj = this;
        var confirmAction = function() {
            scopeObj.view.flxGeneralInfoWrapper.row1.btnLink2.info = {
                "emailSentCtr": 0
            };
            scopeObj.view.flxGeneralInfoWrapper.alertMessage.setVisibility(false);
            if (!scopeObj.view.flxGeneralInfoWrapper.flxGmInfoDetailWrapper.isVisible) {
                scopeObj.view.flxGeneralInfoWrapper.toggleGeneralInfoTab();
            }
            scopeObj.presenter.getCustomerBasicInfo({
                "Customer_username": customerUsername
            }, "InfoScreen");
        };
        var cancelAction = function() {};
        this.AdminConsoleCommonUtils.openConfirm({
            header: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Redirect_Header") + ' "' + (customerUsername) + '"',
            message: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Redirect_Body1") + '"' + (customerUsername) + '"' + kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Redirect_Body2"),
            confirmAction: confirmAction,
            cancelMsg: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Redirect_CancelMsg"),
            cancelAction: cancelAction,
            confirmMsg: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Redirect_ConfirmMsg"),
        }, this);
    },
    setDataForProductTransactionSegment: function(target) {
        var dataMap = {
            "flxCustMangRequestHeader": "flxCustMangRequestHeader",
            "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
            "flxRecurrIcon": "flxRecurrIcon",
            "lblLimitsIcon": "lblLimitsIcon",
            "flxFirstColoum": "flxFirstColoum",
            "lblAmountOriginalSign": "lblAmountOriginalSign",
            "lblAmountOriginalSymbol": "lblAmountOriginalSymbol",
            "lblAmountOriginal": "lblAmountOriginal",
            "lblAmountConvertedSign": "lblAmountConvertedSign",
            "lblAmountConvertedSymbol": "lblAmountConvertedSymbol",
            "lblAmountConverted": "lblAmountConverted",
            "lblDateAndTime": "lblDateAndTime",
            "lblRefNo": "lblRefNo",
            "lblSeperator": "lblSeperator",
            "lblTransctionDescription": "lblTransctionDescription",
            "lblType": "lblType"
        };
        var toAdd;
        var data = [];
        if (target === this.successLabel || target === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Pending")) {
            for (var i = 0; i < this.AccountTrasactions.length; i++) {
                var newDate = this.getFormattedDataTimeFromDBDateTime(this.AccountTrasactions[i].transactionDate.replace("T", " ").replace("Z", ""));
                if (this.AccountTrasactions[i].statusDescription && this.AccountTrasactions[i].statusDescription.toUpperCase() === target.toUpperCase()) {
                    toAdd = {
                        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                        "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                        "flxRecurrIcon": "flxRecurrIcon",
                        "lblLimitsIcon": {
                            "text": "",
                            "skin": "sknFontIconLimits13Px",
                            "isVisible": parseInt(this.AccountTrasactions[i].numberOfRecurrences) > 0 ? true : false
                        },
                        "flxFirstColoum": "flxFirstColoum",
                        "lblAmountOriginalSign": this.AccountTrasactions[i].amount && this.AccountTrasactions[i].amount.substring(0, 1) === "-" ? "-" : "",
                        "lblAmountOriginalSymbol": this.AccountTrasactions[i].amount ? this.defaultCurrencyCode(this.AccountTrasactions[i].baseCurrency) : "",
                        "lblAmountOriginal": this.AccountTrasactions[i].amount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].amount)) : "-",
                        "lblAmountConvertedSign": this.AccountTrasactions[i].convertedAmount && this.AccountTrasactions[i].convertedAmount.substring(0, 1) === "-" ? "-" : "",
                        "lblAmountConvertedSymbol": this.AccountTrasactions[i].convertedAmount ? this.defaultCurrencyCode(this.AccountTrasactions[i].transactionCurrency) : "",
                        "lblAmountConverted": this.AccountTrasactions[i].convertedAmount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].convertedAmount)) : "-",
                        "lblDateAndTime": this.getLocaleDateAndTime(newDate),
                        "lblRefNo": this.AccountTrasactions[i].transactionId,
                        "lblSeperator": ".",
                        "lblTransctionDescription": this.AccountTrasactions[i].description,
                        "lblType": this.AccountTrasactions[i].transactiontype,
                        "template": "flxCustMangTransctionHistory"
                    };
                    data.push(toAdd);
                } else if (target === this.successLabel) {
                    toAdd = {
                        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                        "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                        "flxRecurrIcon": "flxRecurrIcon",
                        "lblLimitsIcon": {
                            "text": "",
                            "skin": "sknFontIconLimits13Px",
                            "isVisible": parseInt(this.AccountTrasactions[i].numberOfRecurrences) > 0 ? true : false
                        },
                        "flxFirstColoum": "flxFirstColoum",
                        "lblAmountOriginalSign": this.AccountTrasactions[i].amount && this.AccountTrasactions[i].amount.substring(0, 1) === "-" ? "-" : "",
                        "lblAmountOriginalSymbol": this.AccountTrasactions[i].amount ? this.defaultCurrencyCode(this.AccountTrasactions[i].baseCurrency) : "",
                        "lblAmountOriginal": this.AccountTrasactions[i].amount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].amount)) : "-",
                        "lblAmountConvertedSign": this.AccountTrasactions[i].convertedAmount && this.AccountTrasactions[i].convertedAmount.substring(0, 1) === "-" ? "-" : "",
                        "lblAmountConvertedSymbol": this.AccountTrasactions[i].convertedAmount ? this.defaultCurrencyCode(this.AccountTrasactions[i].transactionCurrency) : "",
                        "lblAmountConverted": this.AccountTrasactions[i].convertedAmount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].convertedAmount)) : "-",
                        "lblDateAndTime": this.getLocaleDateAndTime(newDate),
                        "lblRefNo": this.AccountTrasactions[i].transactionId,
                        "lblSeperator": ".",
                        "lblTransctionDescription": this.AccountTrasactions[i].description,
                        "lblType": this.AccountTrasactions[i].fromAccountType,
                        "template": "flxCustMangTransctionHistory"
                    };
                    data.push(toAdd);
                }
            }
        } else if (target === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Scheduled")) {
            for (var i = 0; i < this.AccountTrasactions.length; i++) {
                var newDate = this.getFormattedDataTimeFromDBDateTime(this.AccountTrasactions[i].transactionDate.replace("T", " ").replace("Z", ""));
                if (this.AccountTrasactions[i].isScheduled === "true") {
                    toAdd = {
                        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                        "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                        "flxRecurrIcon": "flxRecurrIcon",
                        "lblLimitsIcon": {
                            "text": "",
                            "skin": "sknFontIconLimits13Px",
                            "isVisible": parseInt(this.AccountTrasactions[i].numberOfRecurrences) > 0 ? true : false
                        },
                        "flxFirstColoum": "flxFirstColoum",
                        "lblAmountOriginalSign": this.AccountTrasactions[i].amount && this.AccountTrasactions[i].amount.substring(0, 1) === "-" ? "-" : "",
                        "lblAmountOriginalSymbol": this.AccountTrasactions[i].amount ? this.defaultCurrencyCode(this.AccountTrasactions[i].baseCurrency) : "",
                        "lblAmountOriginal": this.AccountTrasactions[i].amount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].amount)) : "-",
                        "lblAmountConvertedSign": this.AccountTrasactions[i].convertedAmount && this.AccountTrasactions[i].convertedAmount.substring(0, 1) === "-" ? "-" : "",
                        "lblAmountConvertedSymbol": this.AccountTrasactions[i].convertedAmount ? this.defaultCurrencyCode(this.AccountTrasactions[i].transactionCurrency) : "",
                        "lblAmountConverted": this.AccountTrasactions[i].convertedAmount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].convertedAmount)) : "-",
                        "lblDateAndTime": this.getLocaleDateAndTime(newDate),
                        "lblRefNo": this.AccountTrasactions[i].transactionId,
                        "lblSeperator": ".",
                        "lblTransctionDescription": this.AccountTrasactions[i].description,
                        "lblType": this.AccountTrasactions[i].transactiontype,
                        "template": "flxCustMangTransctionHistory"
                    };
                    data.push(toAdd);
                }
            }
        }
        this.view.transactionHistorySearch.tbxSearchBox.text = "";
        if (data.length > 0) {
            toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                "flxRecurrIcon": "flxRecurrIcon",
                "lblLimitsIcon": "lblLimitsIcon",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmountOriginalSign": "",
                "lblAmountOriginalSymbol": "",
                "lblAmountOriginal": "",
                "lblAmountConvertedSign": "",
                "lblAmountConvertedSymbol": "",
                "lblAmountConverted": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "template": "flxCustMangTransctionHistory"
            };
            this.view.segTransactionHistory.widgetDataMap = dataMap;
            this.view.segTransactionHistory.setData(data);
            this.view.segTransactionHistory.info = {
                "data": data,
                "searchAndSortData": data
            };
            this.view.segTransactionHistory.setVisibility(true);
            this.view.flxTransactionHistorySegmentHeader.setVisibility(true);
            this.view.flxSeperator4.setVisibility(true);
            this.view.rtxMsgTransctions.setVisibility(false);
            this.view.transactionHistorySearch.flxDownload.setVisibility(true);
        } else {
            this.view.segTransactionHistory.info = {
                "data": [],
                "searchAndSortData": []
            };
            this.view.transactionHistorySearch.flxDownload.setVisibility(false);
            this.view.segTransactionHistory.setVisibility(false);
            this.view.flxTransactionHistorySegmentHeader.setVisibility(false);
            this.view.flxSeperator4.setVisibility(false);
            this.view.rtxMsgTransctions.setVisibility(true);
        }
        this.showProductTransactionHistory();
    },
    setSkinForHistoryTabs: function(btnWidget) {
        var widgetArray = [this.view.btnTabName1, this.view.btnTabName2, this.view.btnTabName3];
        this.tabUtilButtonFunction(widgetArray, btnWidget);
    },
    setSkinForAccountTabs: function(btnWidget) {
        this.view.btnTransactionHistory.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
        this.view.btnProductDetails.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
        btnWidget.skin = "sknbtnBgffffffLato485c75Radius3Px12Px";
    },
    advSearchUsers: function() {
        var scopeObj = this;
        var acctNo = scopeObj.view.segProductListing.selecteditems[0].lblAccountNumber;
        var StartDate;
        var EndDate;
        var rangeType = scopeObj.view.customCalCreatedDate.value;
        if (rangeType !== "") {
            StartDate = rangeType.substring(0, rangeType.indexOf(" - "));
            var mm = StartDate.substr(0, 2);
            var dd = StartDate.substr(3, 2);
            var yyyy = StartDate.substr(6, 4);
            StartDate = yyyy + "-" + mm + "-" + dd + " 00:00:00";
            EndDate = rangeType.substring(rangeType.indexOf(" - ") + 3);
            var mm1 = EndDate.substr(0, 2);
            var dd1 = EndDate.substr(3, 2);
            var yyyy1 = EndDate.substr(6, 4);
            EndDate = yyyy1 + "-" + mm1 + "-" + dd1 + " 00:00:00";
        }
        kony.adminConsole.utils.showProgressBar(scopeObj.view);
        scopeObj.presenter.getAccountTransactions({
            "AccountNumber": acctNo,
            "StartDate": StartDate,
            "EndDate": EndDate
        });
    },
    getTransactionDateCustom: function(Num, target) {
        var date = this.getCustomDate(Num, target);
        return this.getTransactionDateForServiceCall(date);
    },
    setDataToCardMangSegment: function(totalRecords) {
        var dataToAdd = totalRecords.records;
        var scopeObj = this;
        var dataMap = {
            "flxCardType": "flxCardType",
            "flxCustomerCards": "flxCustomerCards",
            "flxCustomerCardsList": "flxCustomerCardsList",
            "flxOptions": "flxOptions",
            "flxStatus": "flxStatus",
            "ImgCardType": "ImgCardType",
            "lblFontIconServiceStatus": "lblFontIconServiceStatus",
            "imgServiceStatus": "imgServiceStatus",
            "lblCardHolder": "lblCardHolder",
            "lblCardName": "lblCardName",
            "lblCardNumber": "lblCardNumber",
            "lblCardType": "lblCardType",
            "lblIconOptions": "lblIconOptions",
            "lblRequestAndNotification": "lblRequestAndNotification",
            "lblSeparator": "lblSeparator",
            "lblServiceStatus": "lblServiceStatus",
            "product_accountName": "product_accountName"
        };
        if (dataToAdd.length > 0) {
            var toSegment = function(dataToAdd) {
                return {
                    // "flxCardType":"flxCardType",
                    // "flxCustomerCards":"flxCustomerCards",
                    //"flxCustomerCardsList":"flxCustomerCardsList",
                    "flxOptions": {
                        "isVisible": true, //self.currentStatus==="SID_ARCHIVED" || self.currentStatus==="SID_RESOLVED"?false:
                        "onClick": function() {
                            //            this.view.flxCardsContextualMenu
                            scopeObj.toggleContextualMenuCards(70, dataToAdd.cardStatus, dataToAdd.maskedCardNumber);
                        }
                    },
                    //"flxStatus":"flxStatus",
                    "ImgCardType": {
                        //"src": dataToAdd.serviceProvider==="Visa"?"visa.png":"mastercard.png",
                        //"base64":totalRecords.issuerImages[dataToAdd.serviceProvider].Image.substr(22)
                        "base64": totalRecords.issuerImages[dataToAdd.serviceProvider] === undefined ? "N/A" : totalRecords.issuerImages[dataToAdd.serviceProvider].Image.substr(22)
                    },
                    "lblFontIconServiceStatus": {
                        "isVisible": false,
                        "text": "\ue921",
                        "skin": dataToAdd.cardStatus === "Active" ? "sknIcon13pxGreen" : dataToAdd.cardStatus === "Locked" ? "sknIconInProgress" : dataToAdd.cardStatus === "Reported Lost" ? "sknIcon13pxBlue" : "sknFontIconOptionMenu"
                    },
                    //"imgServiceStatus":"imgServiceStatus",
                    "lblCardHolder": dataToAdd.cardHolderName,
                    "lblCardName": dataToAdd.cardProductName,
                    "lblCardNumber": dataToAdd.maskedCardNumber,
                    "lblCardType": dataToAdd.cardType,
                    "lblIconOptions": {
                        "text": "\ue91f",
                        "skin": "sknFontIconOptionMenu"
                    },
                    "lblRequestAndNotification": dataToAdd.requestCount + " Request(s) & " + dataToAdd.notificationCount + " Notification(s)",
                    "lblSeparator": "-",
                    "lblServiceStatus": {
                        "text": dataToAdd.cardStatus,
                        "left": "0px"
                    },
                    "template": "flxCustomerCardsList",
                    "product_Action": dataToAdd.Action,
                    "product_accountName": dataToAdd.accountName,
                    "product_currentDueAmount": dataToAdd.currentDueAmount,
                    "product_currentDueDate": dataToAdd.currentDueDate,
                    "product_maskedCardNumber": dataToAdd.maskedCardNumber,
                    "product_issuedOn": dataToAdd.issuedOn,
                    "product_expiryDate": dataToAdd.expiryDate,
                    "product_withdrawlLimit": dataToAdd.withdrawlLimit,
                    "product_lastPaymentDate": dataToAdd.lastPaymentDate,
                    "product_cardStatus": dataToAdd.cardStatus,
                    "product_interestRate": dataToAdd.interestRate,
                    "product_minimumDueAmount": dataToAdd.minimumDueAmount,
                    "product_requestCount": dataToAdd.requestCount,
                    "product_secondaryCardHolder": dataToAdd.secondaryCardHolder,
                    "product_cardHolderName": dataToAdd.cardHolderName,
                    "product_notificationCount": dataToAdd.notificationCount,
                    "product_currentBalance": dataToAdd.currentBalance,
                    "product_rewardPointBalance": dataToAdd.rewardPointBalance,
                    "product_lastStatementPayment": dataToAdd.lastStatementPayment,
                    "product_cardType": dataToAdd.cardType,
                    "product_accountNumber": dataToAdd.accountNumber,
                    "product_userId": dataToAdd.userId,
                    "product_maskedAccountNumber": dataToAdd.maskedAccountNumber,
                    "product_lastStatementBalance": dataToAdd.lastStatementBalance,
                    "product_cardProductName": dataToAdd.cardProductName,
                    "product_cardId": dataToAdd.cardId,
                    "product_serviceProvider": dataToAdd.serviceProvider,
                    "product_cardNumber": dataToAdd.cardNumber,
                    "billingAddress": dataToAdd.billingAddress,
                    "product_availableCredit": dataToAdd.availableCredit,
                    "cardHolder_username": dataToAdd.username
                };
            };
            var data = dataToAdd.map(toSegment);
            this.view.segProductCardListing.info = {
                "data": data,
                "searchAndSortData": data
            };
            this.view.flxProductCardsHeader.isVisible = true;
            this.view.lblHeaderSeperator1.isVisible = true;
            this.view.segProductCardListing.isVisible = true;
            this.view.rtxMsgProductCard.isVisible = false;
            this.view.segProductCardListing.widgetDataMap = dataMap;
            this.view.segProductCardListing.setData(data);
            this.view.forceLayout();
        } else {
            this.view.flxProductCardsHeader.isVisible = false;
            this.view.lblHeaderSeperator1.isVisible = false;
            this.view.segProductCardListing.isVisible = false;
            this.view.rtxMsgProductCard.isVisible = true;
            this.view.forceLayout();
        }
    },
    showProductInfoScreen: function() {
        this.view.flxProductInfoWrapper.setVisibility(true);
        this.view.flxProductListing.setVisibility(true);
        this.view.flxProductDetails.setVisibility(false);
        this.view.flxProductsAccounts.setVisibility(true);
        this.view.flxProductCardDetails.setVisibility(false);
        this.view.flxProductsCards.setVisibility(false);
        this.view.calStartDate.setVisibility(false);
        this.view.transactionHistorySearch.flxStartDate.setVisibility(false);
        this.view.calEndDate.setVisibility(false);
        this.view.transactionHistorySearch.flxEndDate.setVisibility(false);
        this.view.calStartDate.value = "";
        this.view.calEndDate.value = "";
        this.view.calStartDate.resetData = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Start_Date");
        this.view.calEndDate.resetData = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.End_Date");
        this.view.customCalCreatedDate.resetData = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Last_7_days");
        this.view.transactionHistorySearch.flxStartDate.skin = "sknFlxCalendar";
        this.view.transactionHistorySearch.flxEndDate.skin = "sknFlxCalendar";
        this.view.transactionHistorySearch.lbxPageNumbers.selectedKey = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.7days");
        this.view.transactionHistorySearch.lbxPageNumbers.info = {
            "previousSelection": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.7days")
        };
        this.view.flxProductsTabs.setVisibility(true);
        this.view.forceLayout();
        this.view.flxMainContent.scrollToWidget(this.view.flxOtherInfoWrapper);
    },
    showProductCardScreen: function() {
        this.view.btnProductsAccounts.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
        this.view.btnProductsCards.skin = "sknbtnBgffffffLato485c75Radius3Px12Px";
        this.view.flxProductsAccounts.setVisibility(false);
        this.view.flxProductsCards.setVisibility(true);
        this.view.flxProductCardDetails.setVisibility(false);
        this.view.flxBackToProductListing.setVisibility(false);
        this.view.flxProductHeader.setVisibility(false);
        this.view.flxProductsTabs.setVisibility(true);
        this.view.forceLayout();
    },
    showProductDetailsScreen: function() {
        this.view.flxProductHeaderAndDetails.setVisibility(true);
        this.view.flxTransactionHistoryWrapper.setVisibility(false);
        this.view.flxProductInfoWrapper.setVisibility(true);
        this.view.flxProductsAccounts.setVisibility(true);
        this.view.flxProductCardDetails.setVisibility(false);
        this.view.flxProductsCards.setVisibility(false);
        this.view.flxProductListing.setVisibility(false);
        this.view.flxProductDetails.setVisibility(true);
        this.view.flxProductInfoTabs.setVisibility(true);
        this.view.flxBackToProductListing.setVisibility(true);
        this.view.flxProductHeader.setVisibility(true);
        this.view.forceLayout();
        this.view.flxMainContent.scrollToWidget(this.view.flxOtherInfoWrapper);
    },
    showProductTransactionHistory: function() {
        this.AdminConsoleCommonUtils.storeScrollHeight(this.view.flxMainContent);
        this.view.flxProductHeaderAndDetails.setVisibility(false);
        this.view.flxTransactionHistoryWrapper.setVisibility(true);
        this.view.flxProductInfoWrapper.setVisibility(true);
        this.view.flxProductListing.setVisibility(false);
        this.view.flxProductDetails.setVisibility(true);
        this.view.flxProductInfoTabs.setVisibility(true);
        this.view.flxBackToProductListing.setVisibility(true);
        this.view.flxProductHeader.setVisibility(true);
        this.view.forceLayout();
        //Determine scroll width
        var flxScroll = document.getElementById("frmCustomerProfileAccounts_flxScrollTransctionsSegment");
        var scrollWidth = flxScroll.offsetWidth - flxScroll.clientWidth;
        this.view.flxTransactionHistorySegmentHeader.right = 35 + scrollWidth + "px";
        this.view.forceLayout();
        this.AdminConsoleCommonUtils.scrollToDefaultHeight(this.view.flxMainContent);
    },
    setCardOrAccountNumber: function(cardOrAccountNumber) {
        this.view.productHeader.blAccountNumber.text = cardOrAccountNumber;
        this.view.productHeader.blAccountNumber.width = (8.75 * cardOrAccountNumber.length) + "px";
        this.view.productHeader.blAccountNumber.right = "0px";
        this.view.productHeader.lblAccountNumberLabel.right = ((8.75 * cardOrAccountNumber.length)) + "px";
        this.view.productHeader.lblIBAN.isVisible = false;
        this.view.productHeader.lblIBANLabel.isVisible = false;
        this.view.productHeader.blAccountNumber.isVisible = true;
        this.view.productHeader.lblAccountNumberLabel.isVisible = true;
    },
    onHoverEventCallbackCards: function(widget, context) {
        if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
            this.view.flxCardsContextualMenu.isVisible = false;
        }
    },
    setCardsContextualMenu: function(cardStatus) {
        this.view.cardContextualMenu.lblHeader.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.View");
        this.view.cardContextualMenu.btnLink1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Notifictions");
        this.view.cardContextualMenu.btnLink2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Requests");
        this.view.cardContextualMenu.lblIconOption1.text = "\ue950";
        this.view.cardContextualMenu.lblIconOption1.skin = "sknFontIconOptionMenu";
        this.view.cardContextualMenu.lblIconOption2.text = "\ue932";
        this.view.cardContextualMenu.lblIconOption2.skin = "sknFontIconOptionMenu";
        this.view.cardContextualMenu.flxOption1.isVisible = true;
        this.view.cardContextualMenu.flxOption2.isVisible = true;
        if (cardStatus === "Active") {
            this.view.cardContextualMenu.lblOption1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Mark_as_lost_stolen");
            this.view.cardContextualMenu.lblOption2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Lock");
        } else if (cardStatus === "Locked") {
            this.view.cardContextualMenu.lblOption1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Mark_as_lost_stolen");
            this.view.cardContextualMenu.lblOption2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Unlock");
        } else {
            this.view.cardContextualMenu.flxOption1.isVisible = false;
            this.view.cardContextualMenu.flxOption2.isVisible = false;
        }
    },
    toggleContextualMenuCards: function(rowHeight, cardStatus, cardNumber) {
        this.view.flxCardsContextualMenu.onHover = this.onHoverEventCallbackCards;
        if (this.view.flxCardsContextualMenu.isVisible === false) {
            this.setCardsContextualMenu(cardStatus);
            this.cardNumber = cardNumber;
            var index = this.view.segProductCardListing.selectedIndex;
            this.rowIndex = index[1];
            var height = (this.rowIndex * 51) + 95;
            this.view.flxCardsContextualMenu.top = height + "px";
            this.view.flxCardsContextualMenu.setVisibility(true);
        } else {
            this.view.flxCardsContextualMenu.setVisibility(false);
        }
        this.view.forceLayout();
        this.view.flxMainContent.scrollToWidget(this.view.flxOtherInfoWrapper);
    },
    setDebitCardHeader: function() {
        //Row 1
        this.view.ProductCardRow1.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.PRODUCT_TYPE");
        this.view.ProductCardRow1.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CARD_TYPE");
        this.view.ProductCardRow1.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.PRIMARY_NUMBER");
        //Row 2
        this.view.ProductCardRow2.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CARD_NETWORK");
        this.view.ProductCardRow2.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CARD_HOLDER");
        this.view.ProductCardRow2.btnLink2.isVisible = false;
        this.view.ProductCardRow2.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ASSOCIATED_ACCOUNT");
        //Row 3
        this.view.ProductCardRow3.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ISSUED_ON");
        this.view.ProductCardRow3.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.EXPIRES_ON");
        this.view.ProductCardRow3.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.REWARD_POINTS_BALANCE");
        this.view.ProductCardRow4.isVisible = false;
        this.view.ProductCardRow5.isVisible = false;
        this.view.ProductCardRow6.isVisible = false;
        this.view.ProductCardRow7.isVisible = false;
    },
    setCreditCardHeader: function() {
        //Row 1
        this.view.ProductCardRow1.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.PRODUCT_TYPE");
        this.view.ProductCardRow1.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CARD_TYPE");
        this.view.ProductCardRow1.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.PRIMARY_NUMBER");
        //Row 2
        this.view.ProductCardRow2.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CARD_NETWORK");
        this.view.ProductCardRow2.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CARD_HOLDER");
        if (this.presenter.getCurrentCustomerDetails().Username === this.primaryUsername) {
            this.view.ProductCardRow2.btnLink2.isVisible = false;
        } else {
            this.view.ProductCardRow2.btnLink2.isVisible = true;
        }
        this.view.ProductCardRow2.btnLink2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.View_Primary_Card_Holder");
        this.view.ProductCardRow2.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.INTEREST_RATE");
        //Row 3
        this.view.ProductCardRow3.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ISSUED_ON");
        this.view.ProductCardRow3.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.EXPIRES_ON");
        this.view.ProductCardRow3.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CURRENT_BALANCE");
        this.view.ProductCardRow4.isVisible = true;
        this.view.ProductCardRow5.isVisible = true;
        this.view.ProductCardRow6.isVisible = true;
        this.view.ProductCardRow7.isVisible = true;
        //Row 4
        this.view.ProductCardRow4.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CREDIT_LIMIT");
        this.view.ProductCardRow4.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CURRENT_DUE_AMOUNT");
        this.view.ProductCardRow4.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.REWARD_POINTS_BALANCE");
        //Row 5
        this.view.ProductCardRow5.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.AVAILABLE_CREDIT");
        this.view.ProductCardRow5.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CURRENT_DUE_DATE");
        this.view.ProductCardRow5.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.MINIMUM_DUE_BALANCE");
        //Row 6
        this.view.ProductCardRow6.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LAST_STATEMENT_BALANCE");
        this.view.ProductCardRow6.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LAST_PAYMENT_DATE");
        this.view.ProductCardRow6.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LAST_STATEMENT_PAYMENT");
        //Row 7
        this.view.ProductCardRow7.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.BILLING_ADDRESS");
    },
    resetAllSortImagesCards: function() {
        if (this.view.fontIconSortCardType.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fontIconSortCardType.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fontIconSortCardType.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fontIconSortCardName.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fontIconSortCardName.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fontIconSortCardName.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fontIconSortCardNumber.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fontIconSortCardNumber.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fontIconSortCardNumber.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fontIconSortCardHolder.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fontIconSortCardHolder.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fontIconSortCardHolder.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fontIconSortReq.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fontIconSortReq.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fontIconSortReq.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fontIconSortStatus.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fontIconSortStatus.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fontIconSortStatus.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
    },
    updateEstatementStatus: function(eStatementStatus) {
        kony.print("Inside updateEstatementStatus() of frmCustomerManagementController");
        var scopeObj = this;
        var eStatementJSON = {
            "customerID": scopeObj.presenter.getCurrentCustomerDetails().Customer_id,
            "accountID": scopeObj.view.segProductListing.selecteditems[0].lblAccountNumber,
            "eStatementStatus": eStatementStatus
        };
        if (eStatementStatus === true) {
            var eStatementEmail = scopeObj.view.lbxEstatementEmail.selectedKeyValue[1];
            if (eStatementEmail.indexOf(" (Primary email)") != -1) {
                eStatementEmail = eStatementEmail.substring(0, eStatementEmail.indexOf(" (Primary email)"));
                eStatementJSON.eStatementEmail = eStatementEmail;
            } else {
                eStatementJSON.eStatementEmail = scopeObj.view.lbxEstatementEmail.selectedKeyValue[1];
            }
        }
        scopeObj.presenter.updateEstatementStatus(eStatementJSON);
    },
    downloadTransactionsAsCSV: function() {
        var segData = this.view.segTransactionHistory.data;
        var list = segData.map(function(record) {
            return {
                "RefNo": record.lblRefNo,
                "Type": record.lblType,
                "Description": record.lblTransctionDescription,
                "DateAndTime": record.lblDateAndTime,
                "Amount": record.lblAmountOriginal
            };
        });
        this.commonDownloadCSV(list, "Transactions.csv");
    },
});
define("CustomerManagementModule/frmCustomerProfileAccountsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmCustomerProfileAccounts **/
    AS_Form_f1554e5b428b4f0cabbc8fe2682adabc: function AS_Form_f1554e5b428b4f0cabbc8fe2682adabc(eventobject) {
        var self = this;
        this.CustomerProfileAccountsPreshow();
    },
    /** onDeviceBack defined for frmCustomerProfileAccounts **/
    AS_Form_d800fc7df1d5421f8ca0fb7590629e08: function AS_Form_d800fc7df1d5421f8ca0fb7590629e08(eventobject) {
        var self = this;
    }
});
define("CustomerManagementModule/frmCustomerProfileAccountsController", ["CustomerManagementModule/userfrmCustomerProfileAccountsController", "CustomerManagementModule/frmCustomerProfileAccountsControllerActions"], function() {
    var controller = require("CustomerManagementModule/userfrmCustomerProfileAccountsController");
    var controllerActions = ["CustomerManagementModule/frmCustomerProfileAccountsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
