define("CustomerManagementModule/frmCustomerProfileAccounts", function() {
    return function(controller) {
        function addWidgetsfrmCustomerProfileAccounts() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false,
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "top": "58px"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.customer_Management\")"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var btnNotes = new kony.ui.Button({
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "30px",
                "id": "btnNotes",
                "isVisible": true,
                "right": "35px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnNotes\")",
                "top": "58dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxMainHeader.add(mainHeader, btnNotes);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "left": "35px",
                        "text": "SEARCH"
                    },
                    "lblCurrentScreen": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            var flxModifySearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxModifySearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxModifySearch.setDefaultUnit(kony.flex.DP);
            var modifySearch = new com.adminConsole.search.modifySearch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "modifySearch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxModifySearch.add(modifySearch);
            var flxMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "784px",
                "horizontalScrollIndicator": true,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "126px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxGeneralInformationWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGeneralInformationWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "15px",
                "zIndex": 1
            }, {}, {});
            flxGeneralInformationWrapper.setDefaultUnit(kony.flex.DP);
            var flxGeneralInfoWrapper = new com.adminConsole.CustomerManagement.ProfileGeneralInfo({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGeneralInfoWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "maxHeight": "550px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "EditGeneralInfo.imgFlag1": {
                        "src": "checkbox.png"
                    },
                    "EditGeneralInfo.imgFlag2": {
                        "src": "checkbox.png"
                    },
                    "EditGeneralInfo.imgFlag3": {
                        "src": "checkbox.png"
                    },
                    "imgArrow": {
                        "src": "img_down_arrow.png"
                    },
                    "imgGeneralInfoEditButton": {
                        "src": "img_edit_btn.png"
                    },
                    "row1.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row1.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row1.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxOtherInfoWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxOtherInfoWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoWrapper.setDefaultUnit(kony.flex.DP);
            var flxOtherInfoTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxOtherInfoTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknNormalDefault",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoTabs.setDefaultUnit(kony.flex.DP);
            var tabs = new com.adminConsole.customerMang.tabs({
                "clipBounds": true,
                "height": "60px",
                "id": "tabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox0hfd18814fd664dCM",
                "overrides": {
                    "tabs": {
                        "right": "0px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOtherInfoTabs.add(tabs);
            var flxTabsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxTabsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTabsSeperator.setDefaultUnit(kony.flex.DP);
            flxTabsSeperator.add();
            var flxOtherInfoDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOtherInfoDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "61px",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoDetails.setDefaultUnit(kony.flex.DP);
            var flxProductInfoWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductInfoWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductInfoWrapper.setDefaultUnit(kony.flex.DP);
            var flxProductListing = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductListing",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductListing.setDefaultUnit(kony.flex.DP);
            var flxProductsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxProductsTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductsTabs.setDefaultUnit(kony.flex.DP);
            var flxProductsTabsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxProductsTabsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxedefefOp100NoBorderRadius3Px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxProductsTabsWrapper.setDefaultUnit(kony.flex.DP);
            flxProductsTabsWrapper.add();
            var btnProductsAccounts = new kony.ui.Button({
                "height": "37px",
                "id": "btnProductsAccounts",
                "isVisible": true,
                "left": "35px",
                "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACCOUNTS\")",
                "top": "13px",
                "width": "90dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnProductsCards = new kony.ui.Button({
                "height": "37px",
                "id": "btnProductsCards",
                "isVisible": true,
                "left": "135px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CARDS\")",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProductsTabs.add(flxProductsTabsWrapper, btnProductsAccounts, btnProductsCards);
            var flxProductsAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductsAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductsAccounts.setDefaultUnit(kony.flex.DP);
            var flxProductListingHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxProductListingHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100",
                "top": "0"
            }, {}, {});
            flxProductListingHeader.setDefaultUnit(kony.flex.DP);
            var flxProductType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxProductType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "23%",
                "zIndex": 1
            }, {}, {});
            flxProductType.setDefaultUnit(kony.flex.DP);
            var lblProductType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProductType",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblProductType\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortProductType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortProductType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortProductType.setDefaultUnit(kony.flex.DP);
            var imgSortProductType = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortProductType",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortProductType = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortProductType",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortProductType.add(imgSortProductType, fonticonSortProductType);
            flxProductType.add(lblProductType, flxSortProductType);
            var flxProductName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxProductName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24.50%",
                "zIndex": 1
            }, {}, {});
            flxProductName.setDefaultUnit(kony.flex.DP);
            var lblProductNameHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProductNameHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblProductNameHeader\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortProductName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortProductName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortProductName.setDefaultUnit(kony.flex.DP);
            var imgSortProductName = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortProductName",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortProductName = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortProductName",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortProductName.add(imgSortProductName, fonticonSortProductName);
            flxProductName.add(lblProductNameHeader, flxSortProductName);
            var flxAccountNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountNumber",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "21%",
                "zIndex": 1
            }, {}, {});
            flxAccountNumber.setDefaultUnit(kony.flex.DP);
            var lblAccountNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountNumber",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACCOUNT_NUMBER\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAccountNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAccountNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAccountNumber.setDefaultUnit(kony.flex.DP);
            var imgSortAccountNumber = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortAccountNumber",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortAccountNumber = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortAccountNumber",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortAccountNumber.add(imgSortAccountNumber, fonticonSortAccountNumber);
            flxAccountNumber.add(lblAccountNumber, flxSortAccountNumber);
            var flxAccountOwner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountOwner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "19.50%",
                "zIndex": 1
            }, {}, {});
            flxAccountOwner.setDefaultUnit(kony.flex.DP);
            var lblAccountOwner = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountOwner",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Primary_owner\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAccountOwner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAccountOwner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAccountOwner.setDefaultUnit(kony.flex.DP);
            var fonticonSortAccountOwner = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortAccountOwner",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortAccountOwner.add(fonticonSortAccountOwner);
            flxAccountOwner.add(lblAccountOwner, flxSortAccountOwner);
            var flxAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAmount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxAmount.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAmount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAmount.setDefaultUnit(kony.flex.DP);
            var imgSortAmount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortAmount",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortAmount = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortAmount",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortAmount.add(imgSortAmount, fonticonSortAmount);
            flxAmount.add(lblStatus, flxSortAmount);
            flxProductListingHeader.add(flxProductType, flxProductName, flxAccountNumber, flxAccountOwner, flxAmount);
            var lblHeaderSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblHeaderSeperator",
                "isVisible": true,
                "left": "35dp",
                "right": "35px",
                "skin": "sknLblTableHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": 60,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segProductListing = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "25px",
                "data": [{
                    "btnAccountOwner": "",
                    "flblUnlink": "",
                    "fontIconStatus": "",
                    "lblAccountNumber": "",
                    "lblAccountOwner": "",
                    "lblProductName": "",
                    "lblProductType": "",
                    "lblSeperator": "",
                    "lblStatus": ""
                }],
                "groupCells": false,
                "id": "segProductListing",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustMangProductInfo",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "61px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnAccountOwner": "btnAccountOwner",
                    "flxAccountOwner": "flxAccountOwner",
                    "flxCustMangProduct": "flxCustMangProduct",
                    "flxCustMangProductInfo": "flxCustMangProductInfo",
                    "flxProductType": "flxProductType",
                    "flxStatus": "flxStatus",
                    "fontIconStatus": "fontIconStatus",
                    "lblAccountNumber": "lblAccountNumber",
                    "lblAccountOwner": "lblAccountOwner",
                    "lblProductName": "lblProductName",
                    "lblProductType": "lblProductType",
                    "lblSeperator": "lblSeperator",
                    "lblStatus": "lblStatus"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxMsgProducts = new kony.ui.RichText({
                "bottom": "200px",
                "centerX": "50%",
                "id": "rtxMsgProducts",
                "isVisible": false,
                "skin": "sknrtxLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.rtxMsgActivityHistory\")",
                "top": "200px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProductsAccounts.add(flxProductListingHeader, lblHeaderSeperator, segProductListing, rtxMsgProducts);
            var flxProductsCards = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductsCards",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductsCards.setDefaultUnit(kony.flex.DP);
            var flxProductCardsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxProductCardsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxProductCardsHeader.setDefaultUnit(kony.flex.DP);
            var flxCardType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxCardType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_aec1b5c1f8a3427bbf1faa4eed8d887b,
                "skin": "slFbox",
                "width": "11%",
                "zIndex": 2
            }, {}, {});
            flxCardType.setDefaultUnit(kony.flex.DP);
            var lblCardType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCardType",
                "isVisible": true,
                "left": "1px",
                "skin": "sknlblLato5d6c7f12px",
                "text": "Credit Card",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortCardType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortCardType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortCardType.setDefaultUnit(kony.flex.DP);
            var fontIconSortCardType = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fontIconSortCardType",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortCardType.add(fontIconSortCardType);
            flxCardType.add(lblCardType, flxSortCardType);
            var flxCardName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCardName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "13%"
            }, {}, {});
            flxCardName.setDefaultUnit(kony.flex.DP);
            var lblCardName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCardName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "text": "Card Name",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortCardName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortCardName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortCardName.setDefaultUnit(kony.flex.DP);
            var fontIconSortCardName = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fontIconSortCardName",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortCardName.add(fontIconSortCardName);
            flxCardName.add(lblCardName, flxSortCardName);
            var flxCardNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCardNumber",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "13%"
            }, {}, {});
            flxCardNumber.setDefaultUnit(kony.flex.DP);
            var lblCardNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCardNumber",
                "isVisible": true,
                "left": "1px",
                "skin": "sknlblLato5d6c7f12px",
                "text": "Card Number",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortCardNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortCardNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortCardNumber.setDefaultUnit(kony.flex.DP);
            var fontIconSortCardNumber = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fontIconSortCardNumber",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortCardNumber.add(fontIconSortCardNumber);
            flxCardNumber.add(lblCardNumber, flxSortCardNumber);
            var flxCardHolder = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCardHolder",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%"
            }, {}, {});
            flxCardHolder.setDefaultUnit(kony.flex.DP);
            var lblCardHolder = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCardHolder",
                "isVisible": true,
                "left": "1px",
                "skin": "sknlblLato5d6c7f12px",
                "text": "Card Holder",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortCardHolder = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortCardHolder",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortCardHolder.setDefaultUnit(kony.flex.DP);
            var fontIconSortCardHolder = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fontIconSortCardHolder",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortCardHolder.add(fontIconSortCardHolder);
            flxCardHolder.add(lblCardHolder, flxSortCardHolder);
            var flxCardReqAndNotification = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCardReqAndNotification",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17%"
            }, {}, {});
            flxCardReqAndNotification.setDefaultUnit(kony.flex.DP);
            var lblRequestAndNotification = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRequestAndNotification",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "text": "Request/Notifications",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortReqAndNoti = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortReqAndNoti",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortReqAndNoti.setDefaultUnit(kony.flex.DP);
            var fontIconSortReq = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fontIconSortReq",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortReqAndNoti.add(fontIconSortReq);
            flxCardReqAndNotification.add(lblRequestAndNotification, flxSortReqAndNoti);
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblServiceStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblServiceStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "text": "Status",
                "width": "45px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortStatus.setDefaultUnit(kony.flex.DP);
            var fontIconSortStatus = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fontIconSortStatus",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortStatus.add(fontIconSortStatus);
            flxStatus.add(lblServiceStatus, flxSortStatus);
            flxProductCardsHeader.add(flxCardType, flxCardName, flxCardNumber, flxCardHolder, flxCardReqAndNotification, flxStatus);
            var lblHeaderSeperator1 = new kony.ui.Label({
                "height": "1px",
                "id": "lblHeaderSeperator1",
                "isVisible": true,
                "left": "35dp",
                "right": "35px",
                "skin": "sknLblTableHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": 60,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segProductCardListing = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "25px",
                "data": [{
                    "ImgCardType": "",
                    "lblCardHolder": "",
                    "lblCardName": "",
                    "lblCardNumber": "",
                    "lblCardType": "",
                    "lblFontIconServiceStatus": "",
                    "lblIconOptions": "",
                    "lblRequestAndNotification": "",
                    "lblSeparator": "",
                    "lblServiceStatus": ""
                }],
                "groupCells": false,
                "id": "segProductCardListing",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustomerCardsList",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "61px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "ImgCardType": "ImgCardType",
                    "flxCardType": "flxCardType",
                    "flxCustomerCards": "flxCustomerCards",
                    "flxCustomerCardsList": "flxCustomerCardsList",
                    "flxOptions": "flxOptions",
                    "flxStatus": "flxStatus",
                    "lblCardHolder": "lblCardHolder",
                    "lblCardName": "lblCardName",
                    "lblCardNumber": "lblCardNumber",
                    "lblCardType": "lblCardType",
                    "lblFontIconServiceStatus": "lblFontIconServiceStatus",
                    "lblIconOptions": "lblIconOptions",
                    "lblRequestAndNotification": "lblRequestAndNotification",
                    "lblSeparator": "lblSeparator",
                    "lblServiceStatus": "lblServiceStatus"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxMsgProductCard = new kony.ui.RichText({
                "bottom": "200px",
                "centerX": "50%",
                "id": "rtxMsgProductCard",
                "isVisible": false,
                "skin": "sknrtxLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.rtxMsgActivityHistory\")",
                "top": "200px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCardsContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCardsContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "70px",
                "width": "190px"
            }, {}, {});
            flxCardsContextualMenu.setDefaultUnit(kony.flex.DP);
            var cardContextualMenu = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "cardContextualMenu",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnLink1": {
                        "text": "Request",
                        "top": "15dp"
                    },
                    "btnLink2": {
                        "text": "Notifications"
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOption4": {
                        "isVisible": false
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "lblHeader": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                        "isVisible": true,
                        "top": 15
                    },
                    "lblOption3": {
                        "isVisible": true
                    },
                    "lblOption4": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCardsContextualMenu.add(cardContextualMenu);
            flxProductsCards.add(flxProductCardsHeader, lblHeaderSeperator1, segProductCardListing, rtxMsgProductCard, flxCardsContextualMenu);
            flxProductListing.add(flxProductsTabs, flxProductsAccounts, flxProductsCards);
            var flxProductDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductDetails",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductDetails.setDefaultUnit(kony.flex.DP);
            var flxBackToProductListing = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxBackToProductListing",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBackToProductListing.setDefaultUnit(kony.flex.DP);
            var backToProductListing = new com.adminConsole.customerMang.backToPageHeader({
                "clipBounds": true,
                "height": "20px",
                "id": "backToProductListing",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBackToProductListing.add(backToProductListing);
            var flxProductHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 20,
                "clipBounds": true,
                "id": "flxProductHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductHeader.setDefaultUnit(kony.flex.DP);
            var productHeader = new com.adminConsole.common.productHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "productHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "lblSeperator": {
                        "bottom": "2px"
                    },
                    "productHeader": {
                        "height": "60px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxProductHeader.add(productHeader);
            var flxProductInfoTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45px",
                "id": "flxProductInfoTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductInfoTabs.setDefaultUnit(kony.flex.DP);
            var flxTabsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45px",
                "id": "flxTabsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxedefefOp100NoBorderRadius3Px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxTabsWrapper.setDefaultUnit(kony.flex.DP);
            flxTabsWrapper.add();
            var btnTransactionHistory = new kony.ui.Button({
                "height": "40px",
                "id": "btnTransactionHistory",
                "isVisible": true,
                "left": "40px",
                "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnTransactionHistory\")",
                "top": "5px",
                "width": "140dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnProductDetails = new kony.ui.Button({
                "height": "40px",
                "id": "btnProductDetails",
                "isVisible": true,
                "left": "185px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnProductDetails\")",
                "top": "5px",
                "width": "140dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProductInfoTabs.add(flxTabsWrapper, btnTransactionHistory, btnProductDetails);
            var flxProductHeaderAndDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductHeaderAndDetails",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_c56ba97ff3f0490483af8b7728751669,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductHeaderAndDetails.setDefaultUnit(kony.flex.DP);
            var flxProductDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "height": "30px",
                "id": "flxProductDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductDetailsHeader.setDefaultUnit(kony.flex.DP);
            var lblProductName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProductName",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLatoRegular485c7518Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblProductName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgProductStatus = new kony.ui.Image2({
                "centerY": "50%",
                "height": "12dp",
                "id": "imgProductStatus",
                "isVisible": false,
                "left": "30dp",
                "skin": "slImage",
                "src": "active_circle2x.png",
                "width": "12dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconStatus",
                "isVisible": true,
                "left": "30px",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblProductStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProductStatus",
                "isVisible": true,
                "left": "5px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProductDetailsHeader.add(lblProductName, imgProductStatus, fontIconStatus, lblProductStatus);
            var ProductRow1 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details": {
                        "height": "60px"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProductRow2 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details": {
                        "height": "60px"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxProductRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductRow3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductRow3.setDefaultUnit(kony.flex.DP);
            var ProductRow3 = new com.adminConsole.customerMang.productDetails({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "ProductRow3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnData22": {
                        "left": 0
                    },
                    "btnData23": {
                        "left": 0
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "lblComma1": {
                        "right": "5px"
                    },
                    "lblComma2": {
                        "right": "5px"
                    },
                    "productDetails": {
                        "height": "80px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxProductRow3.add(ProductRow3);
            var ProductRow4 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductRow4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details": {
                        "height": "60px"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProductRow5 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductRow5",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details": {
                        "height": "60px"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxSeprator.setDefaultUnit(kony.flex.DP);
            flxSeprator.add();
            var flxSubscriptionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSubscriptionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSubscriptionsHeader.setDefaultUnit(kony.flex.DP);
            var lblSubscriptions = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "lblSubscriptions",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLatoRegular485c7518Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Subscriptions\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubscriptionsHeader.add(lblSubscriptions);
            var flxSubscriptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxSubscriptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSubscriptions.setDefaultUnit(kony.flex.DP);
            var lblAccountStatement = new kony.ui.Label({
                "height": "15dp",
                "id": "lblAccountStatement",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.AccountStatement\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSubscriptionsRadio = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSubscriptionsRadio",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxSubscriptionsRadio.setDefaultUnit(kony.flex.DP);
            var flxImgRbPaper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15dp",
                "id": "flxImgRbPaper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {}, {});
            flxImgRbPaper.setDefaultUnit(kony.flex.DP);
            var imgRbPaper = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgRbPaper",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "radio_selected.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImgRbPaper.add(imgRbPaper);
            var lblPaper = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPaper",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Paper",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImgRbEstatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15dp",
                "id": "flxImgRbEstatement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {}, {});
            flxImgRbEstatement.setDefaultUnit(kony.flex.DP);
            var imgRbEstatement = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgRbEstatement",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "radio_notselected.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImgRbEstatement.add(imgRbEstatement);
            var lblEstatement = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEstatement",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "e-Statement",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubscriptionsRadio.add(flxImgRbPaper, lblPaper, flxImgRbEstatement, lblEstatement);
            var flxSubscriptionsEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSubscriptionsEmail",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "45dp",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            flxSubscriptionsEmail.setDefaultUnit(kony.flex.DP);
            var lblSubscriptionsEmail = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSubscriptionsEmail",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Label",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSubcriptionContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubcriptionContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "20dp"
            }, {}, {});
            flxSubcriptionContainer.setDefaultUnit(kony.flex.DP);
            var lblSubscriptionEmall = new kony.ui.Label({
                "id": "lblSubscriptionEmall",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "3dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubcriptionContainer.add(lblSubscriptionEmall);
            flxSubscriptionsEmail.add(lblSubscriptionsEmail, flxSubcriptionContainer);
            var flxUpdatedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxUpdatedOn",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxUpdatedOn.setDefaultUnit(kony.flex.DP);
            var lblHeading1 = new kony.ui.Label({
                "id": "lblHeading1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.UPDATEDON\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDataAndImage1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxDataAndImage1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDataAndImage1.setDefaultUnit(kony.flex.DP);
            var lblIconData1 = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblIconData1",
                "isVisible": false,
                "left": "0dp",
                "right": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "0dp",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblData1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblData1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDataAndImage1.add(lblIconData1, lblData1);
            flxUpdatedOn.add(lblHeading1, flxDataAndImage1);
            var flxUpdatedBy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxUpdatedBy",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "72%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxUpdatedBy.setDefaultUnit(kony.flex.DP);
            var lblHeading2 = new kony.ui.Label({
                "id": "lblHeading2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.UPDATEDBY\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDataAndImage2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxDataAndImage2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDataAndImage2.setDefaultUnit(kony.flex.DP);
            var lblIconData2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblIconData2",
                "isVisible": false,
                "left": "0dp",
                "right": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "0dp",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblData2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblData2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDataAndImage2.add(lblIconData2, lblData2);
            flxUpdatedBy.add(lblHeading2, flxDataAndImage2);
            flxSubscriptions.add(lblAccountStatement, flxSubscriptionsRadio, flxSubscriptionsEmail, flxUpdatedOn, flxUpdatedBy);
            flxProductHeaderAndDetails.add(flxProductDetailsHeader, ProductRow1, ProductRow2, flxProductRow3, ProductRow4, ProductRow5, flxSeprator, flxSubscriptionsHeader, flxSubscriptions);
            var flxProductCardDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductCardDetails",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_j4bdee7b67144392bc98fc1ae0b7a6c1,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductCardDetails.setDefaultUnit(kony.flex.DP);
            var ProductCardRow1 = new com.adminConsole.view.details1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductCardRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "60px",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "lblHeading1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblProductType\")"
                    },
                    "lblHeading2": {
                        "text": "CARD TYPE"
                    },
                    "lblHeading3": {
                        "text": "PRIMARY NUMBER"
                    },
                    "lblIconData2": {
                        "isVisible": false
                    },
                    "lblIconData3": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProductCardRow2 = new com.adminConsole.view.detailsWithLink({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductCardRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnLink1": {
                        "isVisible": false
                    },
                    "btnLink2": {
                        "text": "(View Primary Card Holder)"
                    },
                    "btnLink3": {
                        "isVisible": false
                    },
                    "detailsWithLink": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "60px",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "lblHeading1": {
                        "text": "CARD NETWORK"
                    },
                    "lblHeading2": {
                        "text": "CARD HOLDER"
                    },
                    "lblHeading3": {
                        "text": "INTEREST RATE"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProductCardRow3 = new com.adminConsole.view.details1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductCardRow3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "60px",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "lblHeading1": {
                        "text": "ISSUED ON"
                    },
                    "lblHeading2": {
                        "text": "EXPIRES ON"
                    },
                    "lblHeading3": {
                        "text": "CURRENT BALANCE"
                    },
                    "lblIconData2": {
                        "isVisible": false
                    },
                    "lblIconData3": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProductCardRow4 = new com.adminConsole.view.details1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductCardRow4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "60px",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "lblHeading1": {
                        "text": "CREDIT LIMIT"
                    },
                    "lblHeading2": {
                        "text": "CURRENT DUE AMOUNT"
                    },
                    "lblHeading3": {
                        "text": "REWARD POINTS BALANCE"
                    },
                    "lblIconData2": {
                        "isVisible": false
                    },
                    "lblIconData3": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProductCardRow5 = new com.adminConsole.view.details1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductCardRow5",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "60px",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "lblHeading1": {
                        "text": "AVAILABLE CREDIT"
                    },
                    "lblHeading2": {
                        "text": "CURRENT DUE DATE"
                    },
                    "lblHeading3": {
                        "text": "MINIMUM DUE BALANCE"
                    },
                    "lblIconData2": {
                        "isVisible": false
                    },
                    "lblIconData3": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProductCardRow6 = new com.adminConsole.view.details1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductCardRow6",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "60px",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "lblHeading1": {
                        "text": "LAST STATEMENT BALANCE"
                    },
                    "lblHeading2": {
                        "text": "LAST PAYMENT DATE"
                    },
                    "lblHeading3": {
                        "text": "LAST STATEMENT PAYMENT"
                    },
                    "lblIconData2": {
                        "isVisible": false
                    },
                    "lblIconData3": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProductCardRow7 = new com.adminConsole.view.details1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductCardRow7",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "60px",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "flxColumn2": {
                        "isVisible": false
                    },
                    "flxColumn3": {
                        "isVisible": false
                    },
                    "lblHeading1": {
                        "text": "BILLING ADDRESS"
                    },
                    "lblIconData2": {
                        "isVisible": false
                    },
                    "lblIconData3": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxProductCardDetails.add(ProductCardRow1, ProductCardRow2, ProductCardRow3, ProductCardRow4, ProductCardRow5, ProductCardRow6, ProductCardRow7);
            var flxTransactionHistoryWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTransactionHistoryWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactionHistoryWrapper.setDefaultUnit(kony.flex.DP);
            var flxTransactionHistory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTransactionHistory",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactionHistory.setDefaultUnit(kony.flex.DP);
            var flxTransactionHistoryTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "61px",
                "id": "flxTransactionHistoryTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "55px",
                "isModalContainer": false,
                "right": "55px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxTransactionHistoryTabs.setDefaultUnit(kony.flex.DP);
            var btnTabName1 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName1",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RECENT\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName2 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName2",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PENDING\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName3 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName3",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SCHEDULED\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            flxTransactionHistoryTabs.add(btnTabName1, btnTabName2, btnTabName3);
            var flxTransctionHistoryTabsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxTransctionHistoryTabsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxd6dbe7",
                "top": "60px",
                "zIndex": 1
            }, {}, {});
            flxTransctionHistoryTabsSeperator.setDefaultUnit(kony.flex.DP);
            flxTransctionHistoryTabsSeperator.add();
            var flxTransactionHistorySearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "61px",
                "id": "flxTransactionHistorySearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "61dp",
                "zIndex": 1
            }, {}, {});
            flxTransactionHistorySearch.setDefaultUnit(kony.flex.DP);
            var flxStart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStart",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 225,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 10
            }, {}, {});
            flxStart.setDefaultUnit(kony.flex.DP);
            var calStartDate = new kony.ui.CustomWidget({
                "id": "calStartDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "18px",
                "top": "18px",
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "opens": "right",
                "rangeType": null,
                "resetData": "Start Date",
                "type": "single",
                "uniqueId": "calStartDate",
                "value": ""
            });
            flxStart.add(calStartDate);
            var flxEndDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEndDate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "380px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 10
            }, {}, {});
            flxEndDate.setDefaultUnit(kony.flex.DP);
            var calEndDate = new kony.ui.CustomWidget({
                "id": "calEndDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "18px",
                "top": "18px",
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "opens": "right",
                "rangeType": null,
                "resetData": "End Date",
                "type": "single",
                "uniqueId": "calEndDate",
                "value": ""
            });
            flxEndDate.add(calEndDate);
            var flxCreatedRangePicker = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "33px",
                "id": "flxCreatedRangePicker",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "100px",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "width": "200px",
                "zIndex": 10
            }, {}, {});
            flxCreatedRangePicker.setDefaultUnit(kony.flex.DP);
            var customCalCreatedDate = new kony.ui.CustomWidget({
                "id": "customCalCreatedDate",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "2px",
                "top": "5dp",
                "width": "167px",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "list",
                "value": "Last 7 Days"
            });
            var flxCloseCal1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxCloseCal1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal1.setDefaultUnit(kony.flex.DP);
            var lblCloseCal1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCloseCal1",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal1.add(lblCloseCal1);
            flxCreatedRangePicker.add(customCalCreatedDate, flxCloseCal1);
            var btnApply = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "btnApply",
                "isVisible": true,
                "left": "350px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
                "width": "55px",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            var transactionHistorySearch = new com.adminConsole.history.search({
                "clipBounds": true,
                "height": "60px",
                "id": "transactionHistorySearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgClearSearch": {
                        "src": "close_blue.png"
                    },
                    "imgDownload": {
                        "src": "download_2x.png"
                    },
                    "imgSearchIcon": {
                        "src": "search_1x.png"
                    },
                    "lbxPageNumbers": {
                        "isVisible": false
                    },
                    "search": {
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            transactionHistorySearch.tbxSearchBox.onTouchStart = controller.AS_TextField_gf9a2570adc34581b930bf1d5169b593;
            var flxSeperator3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperator3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "60px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeperator3.setDefaultUnit(kony.flex.DP);
            flxSeperator3.add();
            flxTransactionHistorySearch.add(flxStart, flxEndDate, flxCreatedRangePicker, btnApply, transactionHistorySearch, flxSeperator3);
            var flxTransactionHistorySegmentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxTransactionHistorySegmentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100",
                "top": "122px"
            }, {}, {});
            flxTransactionHistorySegmentHeader.setDefaultUnit(kony.flex.DP);
            var flxTranasctionRefNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTranasctionRefNo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "43px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxTranasctionRefNo.setDefaultUnit(kony.flex.DP);
            var lblTranasctionRefNo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTranasctionRefNo",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblTranasctionRefNo\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTranasctionRefNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTranasctionRefNo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTranasctionRefNo.setDefaultUnit(kony.flex.DP);
            var imgSortTranasctionRefNo = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortTranasctionRefNo",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortTranasctionRefNo = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortTranasctionRefNo",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortTranasctionRefNo.add(imgSortTranasctionRefNo, fonticonSortTranasctionRefNo);
            flxTranasctionRefNo.add(lblTranasctionRefNo, flxSortTranasctionRefNo);
            var flxTransactionDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "15%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxTransactionDateAndTime.setDefaultUnit(kony.flex.DP);
            var lblTransactionDateAndTime = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionDateAndTime",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblDateAndTime\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionDateAndTime.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionDateAndTime = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortTransactionDateAndTime",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortTransactionDateAndTime = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortTransactionDateAndTime",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortTransactionDateAndTime.add(imgSortTransactionDateAndTime, fonticonSortTransactionDateAndTime);
            flxTransactionDateAndTime.add(lblTransactionDateAndTime, flxSortTransactionDateAndTime);
            var flxTransactionDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "27%",
                "zIndex": 1
            }, {}, {});
            flxTransactionDescription.setDefaultUnit(kony.flex.DP);
            var lblTransactionDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblTransactionDescription\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTransactionDescription.add(lblTransactionDescription);
            var flxTransactionType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "57%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "13%",
                "zIndex": 1
            }, {}, {});
            flxTransactionType.setDefaultUnit(kony.flex.DP);
            var lblTransactionType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TYPE\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionType.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionType = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortTransactionType",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortTransactionType = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortTransactionType",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortTransactionType.add(imgSortTransactionType, fonticonSortTransactionType);
            flxTransactionType.add(lblTransactionType, flxSortTransactionType);
            var flxTransactionAmountOriginal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionAmountOriginal",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxTransactionAmountOriginal.setDefaultUnit(kony.flex.DP);
            var lblTransactionAmountOriginal = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionAmountOriginal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblTransactionAmountOriginal\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionAmountOriginal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionAmountOriginal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "19px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionAmountOriginal.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionAmountOriginal = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortTransactionAmountOriginal",
                "isVisible": false,
                "right": "0px",
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortTransactionAmountOriginal = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortTransactionAmountOriginal",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortTransactionAmountOriginal.add(imgSortTransactionAmountOriginal, fonticonSortTransactionAmountOriginal);
            flxTransactionAmountOriginal.add(lblTransactionAmountOriginal, flxSortTransactionAmountOriginal);
            var flxTransactionAmountConverted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionAmountConverted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "85%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxTransactionAmountConverted.setDefaultUnit(kony.flex.DP);
            var lblTransactionAmountConverted = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionAmountConverted",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblTransactionAmountConverted\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionAmountConverted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionAmountConverted",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "19px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionAmountConverted.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionAmountConverted = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortTransactionAmountConverted",
                "isVisible": false,
                "right": "0px",
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortTransactionAmountConverted = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortTransactionAmountConverted",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortTransactionAmountConverted.add(imgSortTransactionAmountConverted, fonticonSortTransactionAmountConverted);
            flxTransactionAmountConverted.add(lblTransactionAmountConverted, flxSortTransactionAmountConverted);
            flxTransactionHistorySegmentHeader.add(flxTranasctionRefNo, flxTransactionDateAndTime, flxTransactionDescription, flxTransactionType, flxTransactionAmountOriginal, flxTransactionAmountConverted);
            var flxSeperator4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperator4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxd6dbe7",
                "top": "182px",
                "zIndex": 1
            }, {}, {});
            flxSeperator4.setDefaultUnit(kony.flex.DP);
            flxSeperator4.add();
            var rtxMsgTransctions = new kony.ui.RichText({
                "bottom": "125px",
                "centerX": "50%",
                "id": "rtxMsgTransctions",
                "isVisible": false,
                "skin": "sknrtxLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.rtxMsgActivityHistory\")",
                "top": "300px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxScrollTransctionsSegment = new kony.ui.FlexContainer({
                "bottom": "25px",
                "clipBounds": true,
                "height": "250px",
                "id": "flxScrollTransctionsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "maxHeight": "250px",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "183dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScrollTransctionsSegment.setDefaultUnit(kony.flex.DP);
            var segTransactionHistory = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblAmountConverted": "",
                    "lblAmountConvertedSign": "",
                    "lblAmountConvertedSymbol": "",
                    "lblAmountOriginal": "",
                    "lblAmountOriginalSign": "",
                    "lblAmountOriginalSymbol": "",
                    "lblDateAndTime": "",
                    "lblLimitsIcon": "",
                    "lblRefNo": "",
                    "lblSeperator": "",
                    "lblTransctionDescription": "",
                    "lblType": ""
                }],
                "groupCells": false,
                "height": "250px",
                "id": "segTransactionHistory",
                "isVisible": true,
                "left": "0dp",
                "maxHeight": "250px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustMangTransctionHistory",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAmountConverted": "flxAmountConverted",
                    "flxAmountOriginal": "flxAmountOriginal",
                    "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                    "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                    "flxFirstColoum": "flxFirstColoum",
                    "flxRecurrIcon": "flxRecurrIcon",
                    "lblAmountConverted": "lblAmountConverted",
                    "lblAmountConvertedSign": "lblAmountConvertedSign",
                    "lblAmountConvertedSymbol": "lblAmountConvertedSymbol",
                    "lblAmountOriginal": "lblAmountOriginal",
                    "lblAmountOriginalSign": "lblAmountOriginalSign",
                    "lblAmountOriginalSymbol": "lblAmountOriginalSymbol",
                    "lblDateAndTime": "lblDateAndTime",
                    "lblLimitsIcon": "lblLimitsIcon",
                    "lblRefNo": "lblRefNo",
                    "lblSeperator": "lblSeperator",
                    "lblTransctionDescription": "lblTransctionDescription",
                    "lblType": "lblType"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxScrollTransctionsSegment.add(segTransactionHistory);
            flxTransactionHistory.add(flxTransactionHistoryTabs, flxTransctionHistoryTabsSeperator, flxTransactionHistorySearch, flxTransactionHistorySegmentHeader, flxSeperator4, rtxMsgTransctions, flxScrollTransctionsSegment);
            flxTransactionHistoryWrapper.add(flxTransactionHistory);
            flxProductDetails.add(flxBackToProductListing, flxProductHeader, flxProductInfoTabs, flxProductHeaderAndDetails, flxProductCardDetails, flxTransactionHistoryWrapper);
            flxProductInfoWrapper.add(flxProductListing, flxProductDetails);
            flxOtherInfoDetails.add(flxProductInfoWrapper);
            flxOtherInfoWrapper.add(flxOtherInfoTabs, flxTabsSeperator, flxOtherInfoDetails);
            flxGeneralInformationWrapper.add(flxGeneralInfoWrapper, flxOtherInfoWrapper);
            var flxEntitlementsContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "35px",
                "id": "flxEntitlementsContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "36px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "93px",
                "width": "200px",
                "zIndex": 2
            }, {}, {});
            flxEntitlementsContextualMenu.setDefaultUnit(kony.flex.DP);
            var entitlementsContextualMenu = new com.adminConsole.common.contextualMenu({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "entitlementsContextualMenu",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "contextualMenu": {
                        "bottom": "viz.val_cleared",
                        "height": "35px",
                        "maxHeight": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "imgOption1": {
                        "src": "configure2x.png"
                    },
                    "imgOption2": {
                        "src": "delete_2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "lblOption1": {
                        "text": "Configure"
                    },
                    "lblOption2": {
                        "text": "Remove"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEntitlementsContextualMenu.add(entitlementsContextualMenu);
            var ResetPasswordToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "ResetPasswordToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "150px",
                "skin": "slFbox",
                "top": "425dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "150px",
                        "top": "425dp",
                        "width": "230px"
                    },
                    "flxToolTipMessage": {
                        "height": "50px",
                        "width": "100%"
                    },
                    "lblNoConcentToolTip": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DisabledResetPasswordLink\")",
                        "width": "93%"
                    },
                    "lblarrow": {
                        "centerX": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var CSRAssistToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "CSRAssistToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "80px",
                "skin": "slFbox",
                "top": "58dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "80px",
                        "top": "58dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var EnrolToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "EnrolToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "130px",
                "skin": "slFbox",
                "top": "242dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "130px",
                        "top": "242dp",
                        "width": "230px"
                    },
                    "flxToolTipMessage": {
                        "height": "50px"
                    },
                    "lblNoConcentToolTip": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DisabledEnrollLink\")"
                    },
                    "lblarrow": {
                        "centerX": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSelectOptions = new com.adminConsole.customerMang.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "55px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "80dp",
                "width": "200px",
                "overrides": {
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementPersonal.upgradeToMicroBusiness\")"
                    },
                    "selectOptions": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "55px",
                        "top": "80dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainContent.add(flxGeneralInformationWrapper, flxEntitlementsContextualMenu, ResetPasswordToolTip, CSRAssistToolTip, EnrolToolTip, flxSelectOptions);
            var flxNote = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNote",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "30%",
                "zIndex": 14
            }, {}, {});
            flxNote.setDefaultUnit(kony.flex.DP);
            var Notes = new com.adminConsole.customerMang.Notes({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "Notes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0d4d94de464db42CM",
                "width": "100%",
                "overrides": {
                    "flxNotes": {
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNote.add(Notes);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0px",
                "width": "100%",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxBreadCrumbs, flxModifySearch, flxMainContent, flxNote, flxLoading);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "toastMessage": {
                        "zIndex": 20
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxPopUpConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpConfirmation = new com.adminConsole.common.popUp({
                "clipBounds": true,
                "height": "100%",
                "id": "popUpConfirmation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "right": "200px",
                        "text": "NO, LEAVE IT AS IT IS",
                        "width": "180px"
                    },
                    "btnPopUpDelete": {
                        "text": "YES, OFFLINE",
                        "width": "150px"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Are_you_sure_to\")",
                        "maxWidth": "540px",
                        "minWidth": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpConfirmation.add(popUpConfirmation);
            var flxPopUpError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpError.setDefaultUnit(kony.flex.DP);
            var popUpError = new com.adminConsole.common.popUp({
                "clipBounds": true,
                "height": "100%",
                "id": "popUpError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "isVisible": true,
                        "right": "20px",
                        "text": "CLOSE",
                        "width": "100px"
                    },
                    "btnPopUpDelete": {
                        "isVisible": false,
                        "text": "YES, OFFLINE",
                        "width": "150px"
                    },
                    "flxPopUp": {
                        "centerY": "viz.val_cleared",
                        "top": "220px"
                    },
                    "flxPopUpButtons": {
                        "height": "80px",
                        "width": "100%",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure to set the Branch to Offline mode?<br><br>\n\nIf you set it offline, the Branch does not show operational to the customer."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpError.add(popUpError);
            var flxPopupSelectEnrolEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopupSelectEnrolEmail",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopupSelectEnrolEmail.setDefaultUnit(kony.flex.DP);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "250px",
                "id": "flxPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abebop100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupBody.setDefaultUnit(kony.flex.DP);
            var flxSelectEmailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectEmailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxSelectEmailHeader.setDefaultUnit(kony.flex.DP);
            var lblPopupHeader = new kony.ui.Label({
                "id": "lblPopupHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.SendEnrollHeader\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var imgPopUpClose = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgPopUpClose",
                "isVisible": true,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(imgPopUpClose);
            flxSelectEmailHeader.add(lblPopupHeader, flxPopUpClose);
            var lblPopUpMainMessage = new kony.ui.Label({
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold485c7516px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.Selectenroll\")",
                "top": "20px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstboxEmails = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "35px",
                "id": "lstboxEmails",
                "isVisible": true,
                "left": "20dp",
                "masterData": [
                    ["Select", "kony.i18n.getLocalizedString(\"i18n.frmCustomers.selectEmail\")"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "20px",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxPopupBody.add(flxSelectEmailHeader, lblPopUpMainMessage, lstboxEmails);
            var flxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnPopUpCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnPopUpDelete = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40px",
                "id": "btnPopUpDelete",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.Send\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxPopUpButtons.add(btnPopUpCancel, btnPopUpDelete);
            flxPopUp.add(flxPopUpTopColor, flxPopupBody, flxPopUpButtons);
            flxPopupSelectEnrolEmail.add(flxPopUp);
            var flxEnableEstatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEnableEstatement",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxEnableEstatement.setDefaultUnit(kony.flex.DP);
            var flxEstatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxEstatement",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxEstatement.setDefaultUnit(kony.flex.DP);
            var flxEstatementTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxEstatementTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxebb54cOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementTopColor.setDefaultUnit(kony.flex.DP);
            flxEstatementTopColor.add();
            var flxEstatementInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEstatementInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementInner.setDefaultUnit(kony.flex.DP);
            var flxEstatementClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEstatementClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxEstatementClose.setDefaultUnit(kony.flex.DP);
            var lblPopUpClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPopUpClose",
                "isVisible": true,
                "left": "4dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEstatementClose.add(lblPopUpClose, fontIconImgCLose);
            var lblEnableEstatement = new kony.ui.Label({
                "height": "20px",
                "id": "lblEnableEstatement",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.EnableEstatement\")",
                "top": "40px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEstatementHeader = new kony.ui.Label({
                "height": "20px",
                "id": "lblEstatementHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "text": "Following email ID will be used to send e-Statements",
                "top": "80px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxEstatementEmail = new kony.ui.ListBox({
                "bottom": "60px",
                "height": "30px",
                "id": "lbxEstatementEmail",
                "isVisible": true,
                "left": "20px",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "20px",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "120px",
                "width": "400px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var lblEstatementMessage = new kony.ui.Label({
                "bottom": "10px",
                "height": "40px",
                "id": "lblEstatementMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "text": "Once eStatements service is activated, the customer will stop receiving his/her account statements on paper for the selected accounts",
                "top": "170px",
                "width": "560px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEstatementInner.add(flxEstatementClose, lblEnableEstatement, lblEstatementHeader, lbxEstatementEmail, lblEstatementMessage);
            var flxEstatementButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEstatementButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementButtons.setDefaultUnit(kony.flex.DP);
            var btnEstatementLeave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnEstatementLeave",
                "isVisible": true,
                "right": "150px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnEstatementEnable = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40px",
                "id": "btnEstatementEnable",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesEnable\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxEstatementButtons.add(btnEstatementLeave, btnEstatementEnable);
            flxEstatement.add(flxEstatementTopColor, flxEstatementInner, flxEstatementButtons);
            flxEnableEstatement.add(flxEstatement);
            var flxPreviewPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPreviewPopup.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "470dp",
                "id": "flxAlertPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "120px",
                "width": "800px",
                "zIndex": 10
            }, {}, {});
            flxAlertPopUp.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAlertPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx24ace8",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxAlertPopUpTopColor.add();
            var flxPreviewPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPreviewPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPreviewPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAlertPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "slFbox",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAlertPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblAlertPopupClose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblAlertPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPopUpClose.add(lblAlertPopupClose);
            var lblAlertPopUpHeader = new kony.ui.Label({
                "id": "lblAlertPopUpHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Preview Mode",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewPopupHeader.add(flxAlertPopUpClose, lblAlertPopUpHeader);
            var flxAlertProductsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAlertProductsTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertProductsTabs.setDefaultUnit(kony.flex.DP);
            var flxAlertProductsTabsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAlertProductsTabsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxedefefOpNoBorder",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAlertProductsTabsWrapper.setDefaultUnit(kony.flex.DP);
            var btnSMS = new kony.ui.Button({
                "height": "37px",
                "id": "btnSMS",
                "isVisible": true,
                "left": "20px",
                "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
                "text": "SMS",
                "top": "13px",
                "width": "60dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPushNoti = new kony.ui.Button({
                "height": "37px",
                "id": "btnPushNoti",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "PUSH",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEmail = new kony.ui.Button({
                "height": "37px",
                "id": "btnEmail",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "EMAIL",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNotifCenter = new kony.ui.Button({
                "height": "37px",
                "id": "btnNotifCenter",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "NOTIFICATION CENTER",
                "top": "13px",
                "width": "200dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertProductsTabsWrapper.add(btnSMS, btnPushNoti, btnEmail, btnNotifCenter);
            flxAlertProductsTabs.add(flxAlertProductsTabsWrapper);
            var flxTemplatePreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68px",
                "id": "flxTemplatePreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewHeader.setDefaultUnit(kony.flex.DP);
            var flxPreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPreviewHeader.setDefaultUnit(kony.flex.DP);
            var lblPreviewSubHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader1",
                "isVisible": true,
                "left": 35,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLatoBold35475f14px",
                "text": "Summer discount",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxVerticalSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": 20,
                "id": "flxVerticalSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxVerticalSeprator.setDefaultUnit(kony.flex.DP);
            flxVerticalSeprator.add();
            var lblPreviewSubHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader2",
                "isVisible": true,
                "left": 15,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLatoBold35475f14px",
                "text": "12/12/2017",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewHeader.add(lblPreviewSubHeader1, flxVerticalSeprator, lblPreviewSubHeader2);
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxTemplatePreviewHeader.add(flxPreviewHeader, flxHeaderSeperator);
            var flxTemplatePreviewBody = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 20,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "150dp",
                "horizontalScrollIndicator": true,
                "id": "flxTemplatePreviewBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "91%"
            }, {}, {});
            flxTemplatePreviewBody.setDefaultUnit(kony.flex.DP);
            var lblPreviewTemplateBody = new kony.ui.Label({
                "id": "lblPreviewTemplateBody",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Title:Get Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.\n\nGet Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.\n Get Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.",
                "top": "30dp",
                "width": "730dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxViewer = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxViewer",
                "isVisible": false,
                "left": "0px",
                "requestURLConfig": {
                    "URL": "richtextViewer.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewBody.add(lblPreviewTemplateBody, rtxViewer);
            var flxAlertPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxAlertPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnAlertPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnAlertPopUpCancel",
                "isVisible": true,
                "right": "35px",
                "skin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.close\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPopUpButtons.add(btnAlertPopUpCancel);
            flxAlertPopUp.add(flxAlertPopUpTopColor, flxPreviewPopupHeader, flxAlertProductsTabs, flxTemplatePreviewHeader, flxTemplatePreviewBody, flxAlertPopUpButtons);
            flxPreviewPopup.add(flxAlertPopUp);
            flxMain.add(flxLeftPannel, flxRightPanel, flxToastMessage, flxPopUpConfirmation, flxPopUpError, flxPopupSelectEnrolEmail, flxEnableEstatement, flxPreviewPopup);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            var CSRAssist = new com.adminConsole.customerMang.CSRAssist({
                "clipBounds": true,
                "height": "100%",
                "id": "CSRAssist",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 50,
                "overrides": {
                    "CSRAssist": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.add(flxMain, flxEditCancelConfirmation, CSRAssist);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerProfileAccounts,
            "enabledForIdleTimeout": true,
            "id": "frmCustomerProfileAccounts",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_f1554e5b428b4f0cabbc8fe2682adabc(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_d800fc7df1d5421f8ca0fb7590629e08,
            "retainScrollPosition": false
        }]
    }
});