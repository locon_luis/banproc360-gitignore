define("CustomerManagementModule/userfrmCustomerProfileAlertsController", {
    prefData: {},
    isModified: false,
    isInitialLoad: false,
    supportedChannels: [],
    previewData: [],
    sortBy: null,
    scrollHeight: 0,
    channelCount: 0,
    sALL: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ALL"),
    sActive: kony.i18n.getLocalizedString("i18n.secureimage.Active"),
    sInactive: kony.i18n.getLocalizedString("i18n.permission.Inactive"),
    willUpdateUI: function(context) {
        if (context) {
            this.updateLeftMenu(context);
            if (context.LoadingScreen) {
                if (context.LoadingScreen.focus) {
                    kony.adminConsole.utils.showProgressBar(this.view);
                } else {
                    kony.adminConsole.utils.hideProgressBar(this.view);
                }
            } else if (context.toastModel) {
                if (context.toastModel.status === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SUCCESS")) {
                    this.view.toastMessage.showToastMessage(context.toastModel.message, this);
                } else {
                    this.view.toastMessage.showErrorToastMessage(context.toastModel.message, this);
                }
            } else if (context.CustomerBasicInfo) {
                this.view.flxGeneralInfoWrapper.setBasicInformation(context.CustomerBasicInfo, this);
            } else if (context.enrollACustomer) {
                this.view.flxGeneralInfoWrapper.setEnrollmentAccessandStatus();
            } else if (context.UpdateDBPUserStatus) {
                this.view.flxGeneralInfoWrapper.setLockStatus(context.UpdateDBPUserStatus.status.toUpperCase(), this);
            } else if (context.StatusGroup) {
                this.view.flxGeneralInfoWrapper.processAndFillStatusForEdit(context.StatusGroup, this);
            } else if (context.CustomerNotes) {
                this.view.Notes.displayNotes(this, context.CustomerNotes);
            } else if (context.prefData) {
                this.prefData = context.prefData;
                this.setAlertPrefData(context.prefData);
            } else if (context.alertHistory) {
                this.view.tabs.setSkinForInfoTabs(this.view.tabs.btnTabName7);
                this.resetAllSortImagesAlertHistory();
                this.unloadOnScrollEvent();
                this.presenter.getAlertCategories(this.presenter.getCurrentCustomerDetails().Customer_id);
                this.view.notificationsSearch.flxSubHeader.skin = "sknFlxffffffWithOutBorder";
                this.view.notificationsSearch.lblShowing.setVisibility(true);
                this.view.notificationsSearch.lblShowing.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Alert_history");
                this.view.btnNotifications.onClick();
                this.setDataForAlertHistorySegment(context.alertHistory);
            } else if (context.CustomerNotifications) {
                this.setDataForNotificationsSegment(context.CustomerNotifications);
            } else if (context.AlertPrefrences) {
                this.setAlertData(context.AlertPrefrences);
            } else if (context.OnlineBankingLogin) {
                this.view.CSRAssist.onlineBankingLogin(context.OnlineBankingLogin, this);
            } else if (context.AccountSpecificAlerts) {
                this.view.alerts.listBoxAccounts.info = {
                    "AccountSpecificAlerts": context.AccountSpecificAlerts
                };
                if (context.AccountSpecificAlerts.length > 0) {
                    var defaultAlertId = context.AccountSpecificAlerts[0].alertId;
                    this.setDataForListBoxAccounts(defaultAlertId);
                    this.setDataForAccountSpecificAlerts(defaultAlertId);
                } else {
                    this.view.alerts.lblStatus.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Disabled");
                    this.view.alerts.lblStatusIcon.text = "?";
                    this.view.alerts.lblStatusIcon.skin = "sknfontIconInactive";
                    this.view.alerts.flxOverLay.setVisibility(true);
                    this.view.alerts.flxSubAlerts.setVisibility(false);
                }
                this.view.forceLayout();
                this.view.flxMainContent.scrollToWidget(this.view.flxOtherInfoWrapper);
            }
        }
    },
    setDataForListBoxAccounts: function(defaultAlertId) {
        var AccountSpecificAlerts = this.view.alerts.listBoxAccounts.info.AccountSpecificAlerts;
        var accounts = [];
        for (var i = 0; i < AccountSpecificAlerts.length; i++) {
            accounts.push([AccountSpecificAlerts[i].alertId, AccountSpecificAlerts[i].accountType + " XXXXXX" + AccountSpecificAlerts[i].accountNumber.slice(AccountSpecificAlerts[i].accountNumber.length - 4)]);
        }
        this.view.alerts.listBoxAccounts.masterData = accounts;
        this.view.alerts.listBoxAccounts.selectedKey = defaultAlertId;
    },
    CustomerProfileAlertsPreshow: function() {
        this.view.tabs.setSkinForInfoTabs(this.view.tabs.btnTabName7);
        this.view.Notes.setDefaultNotesData(this);
        var screenHeight = kony.os.deviceInfo().screenHeight;
        this.view.flxMainContent.height = screenHeight - 135 + "px";
        this.view.flxGeneralInfoWrapper.changeSelectedTabColour(this.view.flxGeneralInfoWrapper.dashboardCommonTab.btnProfile);
        this.view.flxGeneralInfoWrapper.generalInfoHeader.setDefaultHeaderData(this);
        this.view.flxGeneralInfoWrapper.setFlowActionsForGeneralInformationComponent(this);
        //Notifications segment default behaviour
        this.AdminConsoleCommonUtils.setVisibility(this.view.flxAlertsHistorySegmentHeader, true);
        this.AdminConsoleCommonUtils.setVisibility(this.view.lblNotificationHeaderSeperator, true);
        this.AdminConsoleCommonUtils.setVisibility(this.view.rtxMsgNotifications, false);
        this.setFlowActions();
    },
    setFlowActions: function() {
        var scopeObj = this;
        //search on notifications
        this.view.notificationsSearch.tbxSearchBox.onKeyUp = function() {
            if (scopeObj.view.notificationsSearch.tbxSearchBox.text === "") {
                scopeObj.view.notificationsSearch.flxSearchCancel.setVisibility(false);
            } else {
                scopeObj.view.notificationsSearch.flxSearchCancel.setVisibility(true);
            }
            var searchParameters = [{
                "searchKey": "lblAlertName",
                "searchValue": scopeObj.view.notificationsSearch.tbxSearchBox.text
            }];
            var listOfWidgetsToHide = [scopeObj.view.flxAlertsHistorySegmentHeader, scopeObj.view.lblNotificationHeaderSeperator];
            scopeObj.search(scopeObj.view.segAlertHistory, searchParameters, scopeObj.view.rtxMsgNotifications, scopeObj.view.flxOtherInfoWrapper, scopeObj.sALL, null, listOfWidgetsToHide);
        };
        this.view.notificationsSearch.flxSearchCancel.onClick = function() {
            scopeObj.view.notificationsSearch.tbxSearchBox.text = "";
            var listOfWidgetsToHide = [scopeObj.view.flxAlertsHistorySegmentHeader, scopeObj.view.lblNotificationHeaderSeperator];
            scopeObj.clearSearch(scopeObj.view.segAlertHistory, scopeObj.view.rtxMsgNotifications, scopeObj.view.flxOtherInfoWrapper, scopeObj.sALL, null, listOfWidgetsToHide);
            scopeObj.view.notificationsSearch.flxSearchCancel.setVisibility(false);
        };
        this.view.btnEmail.onClick = function() {
            var previewTabData = scopeObj.previewData.CH_EMAIL;
            scopeObj.view.lblPreviewSubHeader1.text = previewTabData.subject;
            scopeObj.view.lblPreviewSubHeader2.text = previewTabData.sentDate;
            var emailDescription = "<p style=\"text-align:justify\">" + previewTabData.content + "</p>";
            if (document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer")) {
                document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML = emailDescription;
            } else {
                if (!document.getElementById("iframe_rtxViewer").newOnload) {
                    document.getElementById("iframe_rtxViewer").newOnload = document.getElementById("iframe_rtxViewer").onload;
                }
                document.getElementById("iframe_rtxViewer").onload = function() {
                    document.getElementById("iframe_rtxViewer").newOnload();
                    document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML = emailDescription;
                };
            }
            scopeObj.view.lblPreviewTemplateBody.setVisibility(false);
            scopeObj.view.rtxViewer.setVisibility(true);
            scopeObj.setSkinForChannelTabs(scopeObj.view.btnEmail);
            scopeObj.view.forceLayout();
        };
        this.view.btnAlertPopUpCancel.onClick = function() {
            scopeObj.view.flxPreviewPopup.setVisibility(false);
        };
        this.view.flxAlertPopUpClose.onClick = function() {
            scopeObj.view.flxPreviewPopup.setVisibility(false);
        };
        this.view.alerts.btnEditAlerts.onClick = function() {
            scopeObj.view.alerts.flxAlertDetailContainer.setVisibility(false);
            scopeObj.view.alerts.flxBackBtn.setVisibility(true);
            scopeObj.view.alerts.btnEditAlerts.setVisibility(false);
            scopeObj.view.alerts.flxButtonsSeperator.setVisibility(true);
            scopeObj.view.alerts.commonButtons.setVisibility(true);
            scopeObj.isModified = true;
            scopeObj.isInitialLoad = scopeObj.prefData.typePref.categorySubscription.isInitialLoad;
            scopeObj.setAlertsSegmentData();
            scopeObj.setChannelsDataToUI();
            scopeObj.view.alerts.flxClickBlocker.setVisibility(false);
            scopeObj.view.alerts.flxAlertEditContainer.setVisibility(true);
            scopeObj.view.forceLayout();
        };
        this.view.alerts.btnBack.onClick = function() {
            scopeObj.view.alerts.flxBackBtn.setVisibility(false);
            scopeObj.view.alerts.btnEditAlerts.setVisibility(true);
            scopeObj.view.alerts.flxClickBlocker.setVisibility(true);
            scopeObj.view.alerts.flxAlertEditContainer.setVisibility(false);
            scopeObj.view.alerts.flxButtonsSeperator.setVisibility(false);
            scopeObj.view.alerts.commonButtons.setVisibility(false);
            scopeObj.view.alerts.flxAlertChannelError.setVisibility(false);
            scopeObj.isModified = false;
            scopeObj.setAlertChannelsAndStatus();
            scopeObj.setAlertsSegmentData();
            scopeObj.view.alerts.flxAlertDetailContainer.setVisibility(true);
            scopeObj.view.forceLayout();
        };
        this.view.alerts.commonButtons.btnSave.onClick = function() {
            scopeObj.saveAlertPreferences(scopeObj.view.alerts);
        };
        this.view.alerts.commonButtons.btnCancel.onClick = function() {
            scopeObj.view.alerts.flxAlertChannelError.setVisibility(false);
            scopeObj.isInitialLoad = scopeObj.prefData.typePref.categorySubscription.isInitialLoad;
            scopeObj.setAlertChannelsAndStatus();
            scopeObj.setChannelsDataToUI();
            scopeObj.setAlertsSegmentData();
        };
        this.view.alerts.editAlertCategoryStatusSwitch.switchToggle.onSlide = function() {
            if (scopeObj.view.alerts.editAlertCategoryStatusSwitch.switchToggle.selectedIndex === 0) {
                scopeObj.view.alerts.lblAlertEditNotiStatus.text = "Enabled";
                scopeObj.view.alerts.lblAlertEditNotiStatus.skin = "sknlblLato5bc06cBold14px";
                scopeObj.view.alerts.lblAlertEditNotiStatusIcon.skin = "sknIcon13pxGreen";
                if (scopeObj.isInitialLoad === "true" || scopeObj.isInitialLoad === true) {
                    scopeObj.enableAllChannelsAndAlerts();
                    scopeObj.isInitialLoad = false;
                    return;
                } else {
                    scopeObj.disableEditAlertChannels(0);
                }
            } else {
                scopeObj.view.alerts.lblAlertEditNotiStatus.text = "Disabled";
                scopeObj.view.alerts.lblAlertEditNotiStatus.skin = "sknlblLatoDeactive";
                scopeObj.view.alerts.lblAlertEditNotiStatusIcon.skin = "sknIcon13pxGray";
                scopeObj.disableEditAlertChannels(1);
            }
            var imgSrc = scopeObj.getCheckboxImage();
            var hSkin = scopeObj.getHoverSkin();
            var data = scopeObj.view.alerts.segAlerts2.data;
            for (var i = 0; i < data.length; i++) {
                if (data[i].imgEnabledCheckBox.src !== scopeObj.AdminConsoleCommonUtils.checkboxnormal) {
                    data[i].imgEnabledCheckBox.src = imgSrc;
                }
                data[i].imgEnabledCheckBox.hoverSkin = hSkin;
                scopeObj.view.alerts.segAlerts2.setDataAt(data[i], i);
            }
        };
        this.view.alerts.flxChannel1.onClick = function() {
            if (scopeObj.view.alerts.editAlertCategoryStatusSwitch.switchToggle.selectedIndex === 1) {
                return;
            }
            var imgSrc = scopeObj.getCheckboxImage();
            if (scopeObj.view.alerts.imgCheckBoxChannel1.src === scopeObj.AdminConsoleCommonUtils.checkboxnormal) {
                scopeObj.view.alerts.imgCheckBoxChannel1.src = imgSrc;
                scopeObj.view.alerts.flxAlertChannelError.setVisibility(false);
            } else {
                scopeObj.view.alerts.imgCheckBoxChannel1.src = scopeObj.AdminConsoleCommonUtils.checkboxnormal;
            }
        };
        this.view.alerts.flxChannel2.onClick = function() {
            if (scopeObj.view.alerts.editAlertCategoryStatusSwitch.switchToggle.selectedIndex === 1) {
                return;
            }
            var imgSrc = scopeObj.getCheckboxImage();
            if (scopeObj.view.alerts.imgCheckBoxChannel2.src === scopeObj.AdminConsoleCommonUtils.checkboxnormal) {
                scopeObj.view.alerts.imgCheckBoxChannel2.src = imgSrc;
                scopeObj.view.alerts.flxAlertChannelError.setVisibility(false);
            } else {
                scopeObj.view.alerts.imgCheckBoxChannel2.src = scopeObj.AdminConsoleCommonUtils.checkboxnormal;
            }
        };
        this.view.alerts.flxChannel3.onClick = function() {
            if (scopeObj.view.alerts.editAlertCategoryStatusSwitch.switchToggle.selectedIndex === 1) {
                return;
            }
            var imgSrc = scopeObj.getCheckboxImage();
            if (scopeObj.view.alerts.imgCheckBoxChannel3.src === scopeObj.AdminConsoleCommonUtils.checkboxnormal) {
                scopeObj.view.alerts.imgCheckBoxChannel3.src = imgSrc;
                scopeObj.view.alerts.flxAlertChannelError.setVisibility(false);
            } else {
                scopeObj.view.alerts.imgCheckBoxChannel3.src = scopeObj.AdminConsoleCommonUtils.checkboxnormal;
            }
        };
        this.view.alerts.flxChannel4.onClick = function() {
            if (scopeObj.view.alerts.editAlertCategoryStatusSwitch.switchToggle.selectedIndex === 1) {
                return;
            }
            var imgSrc = scopeObj.getCheckboxImage();
            if (scopeObj.view.alerts.imgCheckBoxChannel4.src === scopeObj.AdminConsoleCommonUtils.checkboxnormal) {
                scopeObj.view.alerts.imgCheckBoxChannel4.src = imgSrc;
                scopeObj.view.alerts.flxAlertChannelError.setVisibility(false);
            } else {
                scopeObj.view.alerts.imgCheckBoxChannel4.src = scopeObj.AdminConsoleCommonUtils.checkboxnormal;
            }
        };
        this.view.btnSMS.onClick = function() {
            var previewTabData = scopeObj.previewData.CH_SMS;
            scopeObj.view.lblPreviewTemplateBody.setVisibility(true);
            scopeObj.view.rtxViewer.setVisibility(false);
            scopeObj.view.lblPreviewSubHeader1.text = previewTabData.subject;
            scopeObj.view.lblPreviewSubHeader2.text = previewTabData.sentDate;
            scopeObj.view.lblPreviewTemplateBody.text = previewTabData.content;
            scopeObj.setSkinForChannelTabs(scopeObj.view.btnSMS);
            scopeObj.view.forceLayout();
        };
        this.view.btnPushNoti.onClick = function() {
            var previewTabData = scopeObj.previewData.CH_PUSH_NOTIFICATION;
            scopeObj.view.lblPreviewTemplateBody.setVisibility(true);
            scopeObj.view.rtxViewer.setVisibility(false);
            scopeObj.view.lblPreviewSubHeader1.text = previewTabData.subject;
            scopeObj.view.lblPreviewSubHeader2.text = previewTabData.sentDate;
            scopeObj.view.lblPreviewTemplateBody.text = previewTabData.content;
            scopeObj.setSkinForChannelTabs(scopeObj.view.btnPushNoti);
            scopeObj.view.forceLayout();
        };
        this.view.btnNotifCenter.onClick = function() {
            var previewTabData = scopeObj.previewData.CH_NOTIFICATION_CENTER;
            scopeObj.view.lblPreviewTemplateBody.setVisibility(true);
            scopeObj.view.rtxViewer.setVisibility(false);
            scopeObj.view.lblPreviewSubHeader1.text = previewTabData.subject;
            scopeObj.view.lblPreviewSubHeader2.text = previewTabData.sentDate;
            scopeObj.view.lblPreviewTemplateBody.text = previewTabData.content;
            scopeObj.setSkinForChannelTabs(scopeObj.view.btnNotifCenter);
            scopeObj.view.forceLayout();
        };
        this.view.alerts.listBoxAccounts.onSelection = function() {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.setDataForAccountSpecificAlerts(scopeObj.view.alerts.listBoxAccounts.selectedKey);
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        this.view.btnNotifications.onClick = function() {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.changeTabSelection(scopeObj.view.btnNotifications);
            scopeObj.changeVisibilty(scopeObj.view.flxNotificationsWrapper);
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        this.view.btnPrefrences.onClick = function() {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.changeTabSelection(scopeObj.view.btnPrefrences);
            scopeObj.changeVisibilty(scopeObj.view.flxAlertWrapper);
            scopeObj.setCategorySegmentHeight();
            scopeObj.view.alerts.segAlertCategory.selectedRowIndex = [0, 0];
            scopeObj.view.alerts.segAlertCategory.onRowClick();
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        this.view.alerts.segAlertCategory.onRowClick = function() {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            var rowData = scopeObj.view.alerts.segAlertCategory.data;
            var index = scopeObj.view.alerts.segAlertCategory.selectedRowIndex[1];
            scopeObj.getAlertCatPref(rowData, index);
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        this.view.alerts.flxHeaderDescription.onClick = function() {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.sortBy.column("description");
            scopeObj.resetAlertPrefrenceSortImages();
            scopeObj.setSubAlerts(scopeObj.view.alerts.segSubAlerts.data.sort(scopeObj.sortBy.sortData), scopeObj.view.alerts.segAlertCategory.selectedRowIndex[1] === 3 ? true : false);
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        this.view.alerts.flxHeaderStatus.onClick = function() {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.sortBy.column("status.text");
            scopeObj.resetAlertPrefrenceSortImages();
            scopeObj.setSubAlerts(scopeObj.view.alerts.segSubAlerts.data.sort(scopeObj.sortBy.sortData), scopeObj.view.alerts.segAlertCategory.selectedRowIndex[1] === 3 ? true : false);
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        this.view.alerts.flxHeaderValue.onClick = function() {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.sortBy.column("value");
            scopeObj.resetAlertPrefrenceSortImages();
            scopeObj.setSubAlerts(scopeObj.view.alerts.segSubAlerts.data.sort(scopeObj.sortBy.sortData), scopeObj.view.alerts.segAlertCategory.selectedRowIndex[1] === 3 ? true : false);
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        //sorting on notifications
        this.view.flxAlertName.onClick = function() {
            scopeObj.resetAllSortImagesAlertHistory();
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segAlertHistory, "lblAlertName", scopeObj.view.fonticonSortAlertName, scopeObj.view.flxOtherInfoWrapper, scopeObj.sALL, null, scopeObj);
        };
        this.view.flxAlertSentDate.onClick = function() {
            scopeObj.resetAllSortImagesAlertHistory();
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segAlertHistory, "lblSentDate", scopeObj.view.fonticonSortSentDate, scopeObj.view.flxOtherInfoWrapper, scopeObj.sALL, null, scopeObj);
        };
        //Notification search skin
        this.view.notificationsSearch.tbxSearchBox.onBeginEditing = function() {
            scopeObj.view.notificationsSearch.flxSearchContainer.skin = "sknflx0cc44f028949b4cradius30px";
        };
        this.view.notificationsSearch.tbxSearchBox.onEndEditing = function() {
            scopeObj.view.notificationsSearch.flxSearchContainer.skin = "sknflxBgffffffBorderc1c9ceRadius30px";
        };
    },
    enableAllChannelsAndAlerts: function() {
        var scopeObj = this;
        var widget = scopeObj.view.alerts;
        var hSkin = scopeObj.getHoverSkin();
        var data = scopeObj.view.alerts.segAlerts2.data;
        var i;
        for (i = 1; i <= 4; i++) {
            widget["imgCheckBoxChannel" + i].src = scopeObj.AdminConsoleCommonUtils.checkboxSelected;
            widget["imgCheckBoxChannel" + i].hoverSkin = hSkin;
        }
        for (i = 0; i < data.length; i++) {
            data[i].imgEnabledCheckBox.src = scopeObj.AdminConsoleCommonUtils.checkboxSelected;
            data[i].imgEnabledCheckBox.hoverSkin = hSkin;
            scopeObj.view.alerts.segAlerts2.setDataAt(data[i], i);
        }
    },
    changeTabSelection: function(selectedButton) {
        var tabArr = [this.view.btnNotifications, this.view.btnPrefrences];
        for (var i = 0; i < tabArr.length; i++) {
            tabArr[i].skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
        }
        selectedButton.skin = "sknbtnBgffffffLato485c75Radius3Px12Px";
    },
    changeVisibilty: function(selectedFlex) {
        var flexArr = [this.view.flxNotificationsWrapper, this.view.flxAlertWrapper];
        for (var i = 0; i < flexArr.length; i++) {
            flexArr[i].setVisibility(false);
        }
        selectedFlex.setVisibility(true);
    },
    mapAccountSpecificAlertsData: function(data) {
        if (data.value) {
            if (data.value.toLowerCase() === "true") {
                data.value = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.On");
            } else if (data.value && data.value.toLowerCase() === "false") {
                data.value = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Off");
            }
        }
        return {
            "description": data.name,
            "status": {
                "text": data.status.toLowerCase() === "active" ? this.sActive : this.sInactive
            },
            "push": data.push ? "" : "",
            "sms": data.SMS ? "" : "",
            "email": data.email ? "" : "",
            "statusIcon": {
                "skin": data.status.toLowerCase() === "active" ? "sknFontIconActivate" : "sknfontIconInactive",
                "text": ""
            },
            "value": data.status.toLowerCase() === "active" && data.value ? data.value : kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.N.A")
        };
    },
    setDataForAccountSpecificAlerts: function(alertId) {
        var AccountSpecificAlerts = this.view.alerts.listBoxAccounts.info.AccountSpecificAlerts;
        this.view.alerts.listBoxAccounts.selectedKey = alertId;
        this.view.alerts.lblStatus.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Enabled");
        this.view.alerts.lblStatusIcon.text = "";
        this.view.alerts.lblStatusIcon.skin = "sknFontIconActivate";
        var alerts = AccountSpecificAlerts.filter(function(ele) {
            if (ele.alertId === alertId) return true;
            return false;
        })[0].alerts;
        this.setSubAlerts(alerts.map(this.mapAccountSpecificAlertsData), true);
        this.view.alerts.flxHeaderValue.setVisibility(true);
        this.view.alerts.listBoxAccounts.setVisibility(true);
        this.view.alerts.flxOverLay.setVisibility(false);
        this.view.alerts.flxSubAlerts.setVisibility(true);
        this.setCategorySegmentHeight();
        this.view.forceLayout();
    },
    setCategorySegmentHeight: function() {
        this.view.forceLayout();
        var height = this.view.alerts.frame.height - 20;
        if (height < 350) {
            this.view.alerts.flxAlertCategory.height = "350px";
        } else {
            this.view.alerts.flxAlertCategory.height = height + "px";
        }
    },
    enableAllCheckboxes: function(alertWidget) {
        var data = alertWidget.data;
        for (var index = 0; index < data.length; index++) {
            data[index].imgEnabledCheckBox.onClick();
        }
    },
    setChannelsDataToUI: function() {
        var self = this;
        var imgSrc = self.getCheckboxImage();
        var channelsData = self.prefData.channelPref.records;
        for (var i = 1, j = 0; i <= channelsData.length; i++, j++) {
            this.view.alerts["flxChannel" + i].isVisible = true;
            this.view.alerts["imgCheckBoxChannel" + i].src = channelsData[j].isSubscribed === "true" ? imgSrc : self.AdminConsoleCommonUtils.checkboxnormal;
            this.view.alerts["imgCheckBoxChannel" + i].hoverSkin = self.getHoverSkin();
            this.view.alerts["lblChannel" + i].text = channelsData[j].channeltext_Description;
            this.view.alerts["lblChannel" + i].isVisible = true;
        }
    },
    setAlertPrefData: function(response) {
        var self = this;
        self.isModified = false;
        self.view.alerts.flxAlertDetailContainer.setVisibility(true);
        self.view.alerts.flxAlertEditContainer.setVisibility(false);
        self.view.alerts.commonButtons.setVisibility(false);
        self.view.alerts.flxButtonsSeperator.setVisibility(false);
        self.view.alerts.flxBackBtn.setVisibility(false);
        self.view.alerts.btnEditAlerts.setVisibility(true);
        var categories = response.categories.records.map(this.mapAlertCategoryData);
        self.setAlertCategory(categories);
        self.changeSelectedArrow();
        self.setAlertChannelsAndStatus();
        self.setAlertsSegmentData();
    },
    mapAlertCategoryData: function(data) {
        return {
            "categoryName": {
                "text": data.alertcategory_Name.toUpperCase(),
                "skin": "sknlblLatoBold485c7512px"
            },
            "id": data.alertcategory_id,
            "Enabled": "",
            "alerts": "",
            "icon": ""
        };
    },
    changeSelectedArrow: function() {
        var segData = this.view.alerts.segAlertCategory.data;
        var newData = [];
        for (var i = 0; i < segData.length; i++) {
            var inter = this.view.alerts.segAlertCategory.data[i];
            if (inter.id === this.prefData.AlertCategoryId) {
                inter.icon = "";
                inter.categoryName = {
                    "text": inter.categoryName.text,
                    "skin": "sknLblLatoBold12px485C75",
                    "hoverSkin": "sknlblLatoBold485c7512pxHandCursor"
                };
                newData.push(inter);
            } else {
                inter.icon = "";
                inter.categoryName = {
                    "text": inter.categoryName.text,
                    "skin": "sknlblLatoBold485c7512px",
                    "hoverSkin": "sknLblLatoReg12px485C75HandCursor"
                };
                newData.push(inter);
            }
        }
        this.setAlertCategory(newData);
    },
    getAlertCatPref: function(rowData, index) {
        var segData = this.view.alerts.segAlertCategory.data;
        if (index === 3) {
            this.presenter.getAccountSpecificAlerts({
                "customerUsername": this.presenter.getCurrentCustomerDetails().Username
            });
        } else {
            this.setAlertMessageByCategoryId(segData[index].id);
            var context = {
                "AlertCategoryId": segData[index].id,
                "CustomerId": this.presenter.getCurrentCustomerDetails().Customer_id
            };
            this.presenter.getAlertCategoryPref(context);
        }
    },
    setAlertMessageByCategoryId: function(alertId) {
        if (alertId === "ALERT_CAT_SECURITY") {
            this.view.alerts.lblAlertDetailDisplayName.text = "Security Alert Preferences";
        } else if (alertId === "ALERT_CAT_TRANSACTIONAL") {
            this.view.alerts.lblAlertDetailDisplayName.text = "Transactional Alert Preferences";
        } else {
            this.view.alerts.lblAlertDetailDisplayName.text = "Receive Alert Preferences";
        }
    },
    setAlertCategory: function(data) {
        var widgetDataMap = {
            "lblAlertCategory": "categoryName",
            "id": "id",
            "lblArrow": "icon",
        };
        this.view.alerts.segAlertCategory.widgetDataMap = widgetDataMap;
        this.view.alerts.segAlertCategory.setData(data);
    },
    setAlertChannelsAndStatus: function() {
        var self = this;
        var channelsData = self.prefData.channelPref.records;
        var str = "";
        self.supportedChannels = [];
        channelsData.forEach(function(rowChannel, index) {
            if (rowChannel.isSubscribed === "true") {
                str += rowChannel.channeltext_Description + ", ";
                self.supportedChannels.push(rowChannel.channeltext_Description);
            }
        });
        if (str.substr(str.length - 2, 2) === ", ") {
            str = str.substring(0, str.length - 2);
        }
        self.view.alerts.lblAlertDetailChannels.text = str || "None";
        self.view.alerts.lblAlertNotiStatus.text = (self.prefData.typePref.categorySubscription.isSubscribed === "true") ? "Enabled" : "Disabled";
        self.view.alerts.lblAlertNotiStatus.skin = (self.prefData.typePref.categorySubscription.isSubscribed === "true") ? "sknlblLato5bc06cBold14px" : "sknlblLatoDeactive";
        self.view.alerts.lblAlertNotiStatusIcon.skin = (self.prefData.typePref.categorySubscription.isSubscribed === "true") ? "sknIcon13pxGreen" : "sknIcon13pxGray";
        self.view.alerts.lblAlertEditNotiStatus.text = self.view.alerts.lblAlertNotiStatus.text;
        self.view.alerts.lblAlertEditNotiStatus.skin = self.view.alerts.lblAlertNotiStatus.skin;
        self.view.alerts.lblAlertEditNotiStatusIcon.skin = self.view.alerts.lblAlertNotiStatusIcon.skin;
        self.view.alerts.editAlertCategoryStatusSwitch.switchToggle.selectedIndex = (self.prefData.typePref.categorySubscription.isSubscribed === "true") ? 0 : 1;
    },
    alertInputType: function(rowAlert) {
        var self = this;
        if (rowAlert.alertCondition) {
            if (rowAlert.alertCondition.NoOfFields === "2") {
                return self.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.Range;
            } else if (rowAlert.alertCondition.NoOfFields === "1") {
                if (rowAlert.alertAttribute && rowAlert.alertAttribute.values) {
                    return self.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.List;
                } else {
                    return self.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.EqualTo;
                }
            }
        }
        return self.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.NoInputNeeded;
    },
    getCheckboxImage: function() {
        var scopeObj = this;
        return scopeObj.view.alerts.editAlertCategoryStatusSwitch.switchToggle.selectedIndex === 0 ? scopeObj.AdminConsoleCommonUtils.checkboxSelected : scopeObj.AdminConsoleCommonUtils.checkboxDisable;
    },
    getHoverSkin: function() {
        var scopeObj = this;
        return scopeObj.view.alerts.editAlertCategoryStatusSwitch.switchToggle.selectedIndex === 0 ? "sknCursor" : "sknCursorDisabled";
    },
    fetchAlertsSegmentData: function(alertWidget) {
        var scopeObj = this;
        var alertsData = this.prefData.typePref.records;
        var data = alertWidget.data;
        var typePreference = [];
        var imgSrc = scopeObj.getCheckboxImage();
        data.forEach(function(rowAlert, i) {
            var type = scopeObj.alertInputType(alertsData[i]);
            var list = {};
            list.typeId = rowAlert.alerttype_id;
            list.isSubscribed = (rowAlert.imgEnabledCheckBox.src === imgSrc) ? "true" : "false";
            if (type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.Range) {
                list.value1 = rowAlert.tbxMinLimit.text;
                list.value2 = rowAlert.tbxMaxLimit.text;
            } else if (type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.EqualTo) {
                list.value1 = rowAlert.tbxAmount.text;
            } else if (type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.List) {
                list.value1 = rowAlert.lstNotify.selectedKey;
            }
            typePreference.push(list);
        });
        return typePreference;
    },
    showOrHideAlertsInputField: function(rowData, type, index) {
        var scopeObj = this;
        var data = rowData;
        var imgSrc = scopeObj.getCheckboxImage();
        rowData.flxAccBalanceInBetween.isVisible = (type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.Range && rowData.imgEnabledCheckBox.src === imgSrc) ? true : false;
        rowData.flxAccBalanceCreditedDebited.isVisible = (type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.EqualTo && rowData.imgEnabledCheckBox.src === imgSrc) ? true : false;
        rowData.flxNotifyBalance.isVisible = (type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.List && rowData.imgEnabledCheckBox.src === imgSrc) ? true : false;
        scopeObj.view.alerts.segAlerts2.setDataAt(data, index);
    },
    showNoAlertsFoundUI: function() {},
    setAlertsSegmentData: function() {
        var scopeObj = this;
        var dataMap = {
            "flxAccountAlertsCategoryDetails": "flxAccountAlertsCategoryDetails",
            "flxAccountAlertCategoryContainer": "flxAccountAlertCategoryContainer",
            "flxEnabledIcon": "flxEnabledIcon",
            "imgEnabledCheckBox": "imgEnabledCheckBox",
            "lblEnabledIcon": "lblEnabledIcon",
            "lblAlertDescription": "lblAlertDescription",
            "flxAccBalanceInBetween": "flxAccBalanceInBetween",
            "tbxMinLimit": "tbxMinLimit",
            "tbxMaxLimit": "tbxMaxLimit",
            "flxHorSeparator": "flxHorSeparator",
            "lblHorSeparator": "lblHorSeparator",
            "flxAccBalanceCreditedDebited": "flxAccBalanceCreditedDebited",
            "tbxAmount": "tbxAmount",
            "flxNotifyBalance": "flxNotifyBalance",
            "lstNotify": "lstNotify",
            "lblSeperator": "lblSeperator",
            "alerttype_id": "alerttype_id",
            "template": "template"
        };
        var list = [];
        var alertsData = this.prefData.typePref.records;
        if (alertsData.length === 0) {
            scopeObj.showNoAlertsFoundUI();
        }
        var isModified = scopeObj.isModified;
        var imgSrc = scopeObj.getCheckboxImage();
        var hSkin = scopeObj.getHoverSkin();
        alertsData.forEach(function(rowAlert, index) {
            var type = scopeObj.alertInputType(rowAlert);
            var data = {};
            data = {
                "imgEnabledCheckBox": {
                    isVisible: (isModified === true) ? true : false,
                    "src": rowAlert.isSubscribed === "true" ? imgSrc : scopeObj.AdminConsoleCommonUtils.checkboxnormal,
                    "hoverSkin": hSkin,
                    "onClick": function() {
                        if (scopeObj.view.alerts.editAlertCategoryStatusSwitch.switchToggle.selectedIndex === 1) {
                            return;
                        }
                        var imgSrc2 = scopeObj.getCheckboxImage();
                        var rowData = scopeObj.view.alerts.segAlerts2.data[index];
                        if (rowData.imgEnabledCheckBox.src === scopeObj.AdminConsoleCommonUtils.checkboxnormal) {
                            rowData.imgEnabledCheckBox.src = imgSrc2;
                        } else {
                            rowData.imgEnabledCheckBox.src = scopeObj.AdminConsoleCommonUtils.checkboxnormal;
                        }
                        scopeObj.view.alerts.segAlerts2.setDataAt(rowData, index);
                        scopeObj.showOrHideAlertsInputField(rowData, type, index);
                    }
                },
                "lblEnabledIcon": {
                    isVisible: (isModified === false) ? true : false,
                    "skin": (rowAlert.isSubscribed === "true") ? "sknIcon13pxGreen" : "sknIcon13pxGray",
                },
                "lblAlertDescription": rowAlert.alerttypetext_DisplayName,
                "flxAccBalanceInBetween": {
                    isVisible: type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.Range && rowAlert.isSubscribed === "true" ? true : false
                },
                "tbxMinLimit": {
                    "text": (type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.Range && rowAlert.preference && rowAlert.preference.Value1) ? rowAlert.preference.Value1 : "",
                },
                "tbxMaxLimit": {
                    "text": (type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.Range && rowAlert.preference && rowAlert.preference.Value2) ? rowAlert.preference.Value2 : "",
                },
                "flxHorSeparator": {
                    isVisible: true
                },
                "lblHorSeparator": " ",
                "flxAccBalanceCreditedDebited": {
                    isVisible: type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.EqualTo && rowAlert.isSubscribed === "true" ? true : false,
                },
                "tbxAmount": {
                    "text": (type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.EqualTo && rowAlert.preference && rowAlert.preference.Value1) ? rowAlert.preference.Value1 : "",
                },
                "flxNotifyBalance": {
                    isVisible: type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.List && rowAlert.isSubscribed === "true" ? true : false,
                },
                //Needs Refactor
                "lstNotify": {
                    selectedKey: (type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.List && rowAlert.preference) ? rowAlert.preference.Value1 : type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.List ? scopeObj.returnMasterDataFromAlerts(rowAlert.alertAttribute.values)[0][0] : "",
                    masterData: type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.List ? scopeObj.returnMasterDataFromAlerts(rowAlert.alertAttribute.values) : "",
                },
                "lblSeperator": "-",
                "alerttype_id": rowAlert.alerttype_id,
                "template": "flxAccountAlertsCategoryDetails"
            };
            list.push(data);
        });
        scopeObj.view.alerts.segAlerts2.widgetDataMap = dataMap;
        scopeObj.view.alerts.segAlerts2.setData([]);
        scopeObj.view.alerts.segAlerts2.setData(list);
        scopeObj.view.forceLayout();
    },
    returnMasterDataFromAlerts: function(values) {
        var data = [];
        values.forEach(function(value) {
            var list = [];
            list.push(value.alertattributelistvalues_id);
            list.push(value.alertattributelistvalues_name);
            data.push(list);
        });
        return data;
    },
    validateAlertsSegmentData: function(alertWidget) {
        var scopeObj = this;
        var data = alertWidget.data;
        var alertsData = this.prefData.typePref.records;
        var isValid = true;
        var imgSrc = scopeObj.getCheckboxImage();
        data.forEach(function(rowAlert, i) {
            var type = scopeObj.alertInputType(alertsData[i]);
            if (type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.Range) {
                if (rowAlert.imgEnabledCheckBox.src === imgSrc && (rowAlert.tbxMinLimit.text.trim().length === 0 || rowAlert.tbxMaxLimit.text.trim().length === 0)) {
                    alertWidget.setDataAt(rowAlert, i);
                    isValid = false;
                }
            } else if (type === scopeObj.AdminConsoleCommonUtils.ALERTS_INPUT_TYPES.EqualTo) {
                if (rowAlert.imgEnabledCheckBox.src === imgSrc && rowAlert.tbxAmount.text.trim().length === 0) {
                    alertWidget.setDataAt(rowAlert, i);
                    isValid = false;
                }
            }
        });
        return isValid;
    },
    getSelectedAlertsChannels: function(widget) {
        var self = this;
        var channelPreferences = [];
        var channelData = self.prefData.channelPref.records;
        var imgSrc = self.view.alerts.editAlertCategoryStatusSwitch.switchToggle.selectedIndex === 0 ? self.AdminConsoleCommonUtils.checkboxSelected : self.AdminConsoleCommonUtils.checkboxDisable;
        self.channelCount = 0;
        for (var i = 1, j = 0; widget["flxChannel" + i] !== undefined; i++, j++) {
            if (widget["flxChannel" + i] !== undefined && (widget["flxChannel" + i].isVisible === true || widget["flxChannel" + i].isVisible === "true")) {
                var temp = {};
                if (widget["imgCheckBoxChannel" + i].src === imgSrc) {
                    temp.isSubscribed = true;
                    self.channelCount++;
                } else {
                    temp.isSubscribed = false;
                }
                temp.channelId = channelData[j].channel_id;
                channelPreferences.push(temp);
            }
        }
        return channelPreferences;
    },
    /**
     *saveAlerts- function that saves/updates alerts changed
     * @param {Object} alerts - alerts array
     */
    saveAlertPreferences: function(alertWidget) {
        var self = this;
        var id = self.presenter.getCurrentCustomerDetails().Customer_id;
        var alertsChannelData = this.getSelectedAlertsChannels(alertWidget);
        var isvalidAlertTypePreference = this.validateAlertsSegmentData(alertWidget.segAlerts2);
        var subscriptionStatus = self.view.alerts.editAlertCategoryStatusSwitch.switchToggle.selectedIndex === 1 ? false : true;
        if (self.channelCount === 0) {
            this.view.alerts.flxAlertChannelError.setVisibility(true);
        }
        if (isvalidAlertTypePreference && self.channelCount !== 0) {
            var alertTypePreference = this.fetchAlertsSegmentData(alertWidget.segAlerts2);
            var alertsData = {
                channelPreference: alertsChannelData,
                typePreference: alertTypePreference,
                CustomerId: id,
                isSubscribed: subscriptionStatus,
                AlertCategoryId: self.prefData.AlertCategoryId,
            };
            this.presenter.setAlertPreferences(alertsData);
        }
    },
    disableEditAlertChannels: function(enable) {
        var widget = this.view.alerts;
        var i;
        if (enable === 0) { // category enabled
            for (i = 1; i <= 4; i++) {
                widget["imgCheckBoxChannel" + i].src = widget["imgCheckBoxChannel" + i].src === this.AdminConsoleCommonUtils.checkboxDisable ? this.AdminConsoleCommonUtils.checkboxSelected : this.AdminConsoleCommonUtils.checkboxnormal;
                widget["imgCheckBoxChannel" + i].hoverSkin = "sknCursor";
            }
        } else if (enable === 1) { //category disabled
            for (i = 1; i <= 4; i++) {
                widget["imgCheckBoxChannel" + i].src = widget["imgCheckBoxChannel" + i].src === this.AdminConsoleCommonUtils.checkboxSelected ? this.AdminConsoleCommonUtils.checkboxDisable : this.AdminConsoleCommonUtils.checkboxnormal;
                widget["imgCheckBoxChannel" + i].hoverSkin = "sknCursorDisabled";
            }
        }
    },
    sendSubAlerts: function(data) {
        var alerts = data.alerts.map(this.mapSubAlertsData);
        if (data.Enabled === "true") {
            this.view.alerts.lblStatus.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Enabled");
            this.view.alerts.lblStatusIcon.text = "";
            this.view.alerts.lblStatusIcon.skin = "sknFontIconActivate";
            this.setSubAlerts(alerts, false);
            this.view.alerts.flxHeaderValue.setVisibility(false);
            this.view.alerts.listBoxAccounts.setVisibility(false);
            this.view.alerts.flxOverLay.setVisibility(false);
            this.view.alerts.flxSubAlerts.setVisibility(true);
        } else {
            this.view.alerts.lblStatus.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Disabled");
            this.view.alerts.lblStatusIcon.text = "";
            this.view.alerts.lblStatusIcon.skin = "sknfontIconInactive";
            this.view.alerts.flxOverLay.setVisibility(true);
            this.view.alerts.flxSubAlerts.setVisibility(false);
        }
        this.setCategorySegmentHeight();
        this.view.forceLayout();
    },
    mapSubAlertsData: function(data) {
        return {
            "id": data.alertId,
            "description": data.name,
            "status": {
                "text": data.isSelected === "true" ? this.sActive : this.sInactive
            },
            "push": data.isPushActive === "true" ? "" : "",
            "sms": data.isEmailActive === "true" ? "" : "",
            "email": data.isSmsActive === "true" ? "" : "",
            "statusIcon": {
                "skin": data.isSelected === "true" ? "sknFontIconActivate" : "sknfontIconInactive",
                "text": ""
            },
            "value": data.value ? data.value : "N.A." // add value term
        };
    },
    setSubAlerts: function(data, isAccount) {
        var widgetDataMap = {
            "lblHeaderDescription": "description",
            "lblHeaderStatus": "status",
            "lblHeaderPush": "push",
            "lblHeaderSMS": "sms",
            "lblheaderEmail": "email",
            "lblHeaderStatusIcon": "statusIcon",
        };
        if (isAccount) widgetDataMap.lblValue = "value";
        this.view.alerts.segSubAlerts.widgetDataMap = widgetDataMap;
        this.view.alerts.segSubAlerts.setData(data);
    },
    resetAllSortImagesAlertHistory: function() {
        if (this.view.fonticonSortAlertName.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortAlertName.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortAlertName.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fonticonSortSentDate.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortSentDate.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortSentDate.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
    },
    resetAlertPrefrenceSortImages: function() {
        var self = this;
        self.determineSortFontIcon(this.sortBy, 'description', this.view.alerts.lblHeaderDescriptionIcon);
        self.determineSortFontIcon(this.sortBy, 'status.text', this.view.alerts.lblHeaderStatusIcon);
        self.determineSortFontIcon(this.sortBy, 'value', this.view.alerts.lblHeaderValueIcon);
    },
    showNotificationsScreen: function() {
        this.view.flxNotificationsWrapper.setVisibility(true);
        this.view.flxAlertPrefrenceWrapper.setVisibility(true);
        this.view.forceLayout();
        this.view.flxMainContent.scrollToWidget(this.view.flxOtherInfoWrapper);
    },
    unloadOnScrollEvent: function() {
        document.getElementById("frmCustomerProfileAlerts_flxMainContent").onscroll = function() {};
    },
    setAllAlertPreviewTabsData: function() {
        var scopeObj = this;
        var previewTabData = scopeObj.previewData.CH_NOTIFICATION_CENTER;
        if (previewTabData !== undefined) {
            scopeObj.view.lblPreviewSubHeader1.text = previewTabData.subject;
            scopeObj.view.lblPreviewSubHeader2.text = previewTabData.sentDate;
            scopeObj.view.lblPreviewTemplateBody.text = previewTabData.content;
            scopeObj.setSkinForChannelTabs(scopeObj.view.btnNotifCenter);
        }
        previewTabData = scopeObj.previewData.CH_EMAIL;
        if (previewTabData !== undefined) {
            scopeObj.view.lblPreviewSubHeader1.text = previewTabData.subject;
            scopeObj.view.lblPreviewSubHeader2.text = previewTabData.sentDate;
            scopeObj.setSkinForChannelTabs(scopeObj.view.btnEmail);
            var emailDescription = "<p style=\"text-align:justify\">" + previewTabData.content + "</p>";
            if (document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer")) {
                document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML = emailDescription;
            } else {
                if (!document.getElementById("iframe_rtxViewer").newOnload) {
                    document.getElementById("iframe_rtxViewer").newOnload = document.getElementById("iframe_rtxViewer").onload;
                }
                document.getElementById("iframe_rtxViewer").onload = function() {
                    document.getElementById("iframe_rtxViewer").newOnload();
                    document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML = emailDescription;
                };
            }
        }
        previewTabData = scopeObj.previewData.CH_PUSH_NOTIFICATION;
        if (previewTabData !== undefined) {
            scopeObj.view.lblPreviewSubHeader1.text = previewTabData.subject;
            scopeObj.view.lblPreviewSubHeader2.text = previewTabData.sentDate;
            scopeObj.view.lblPreviewTemplateBody.text = previewTabData.content;
            scopeObj.setSkinForChannelTabs(scopeObj.view.btnPushNoti);
        }
        previewTabData = scopeObj.previewData.CH_SMS;
        if (previewTabData !== undefined) {
            scopeObj.view.lblPreviewSubHeader1.text = previewTabData.subject;
            scopeObj.view.lblPreviewSubHeader2.text = previewTabData.sentDate;
            scopeObj.view.lblPreviewTemplateBody.text = previewTabData.content;
            scopeObj.setSkinForChannelTabs(scopeObj.view.btnSMS);
        }
    },
    showAlertPreview: function() {
        var scopeObj = this;
        scopeObj.view.flxPreviewPopup.setVisibility(true);
        var previewData = scopeObj.view.segAlertHistory.selecteditems[0].data;
        scopeObj.previewData = previewData;
        scopeObj.view.btnSMS.setVisibility(false);
        scopeObj.view.btnPushNoti.setVisibility(false);
        scopeObj.view.btnNotifCenter.setVisibility(false);
        scopeObj.view.btnEmail.setVisibility(false);
        scopeObj.setAllAlertPreviewTabsData();
        if (previewData.CH_SMS !== undefined) {
            this.view.btnSMS.setVisibility(true);
        }
        if (previewData.CH_PUSH_NOTIFICATION !== undefined) {
            this.view.btnPushNoti.setVisibility(true);
        }
        if (previewData.CH_NOTIFICATION_CENTER !== undefined) {
            this.view.btnNotifCenter.setVisibility(true);
        }
        if (previewData.CH_EMAIL !== undefined) {
            this.view.btnEmail.setVisibility(true);
        }
    },
    getAlertChannelData: function(channel) {
        var val;
        var json = {};
        if (!channel || (channel && channel.alerthistory_Status !== "SID_DELIVERYFAILED" && channel.alerthistory_Status !== "SID_DELIVERY_SUBMITTED")) {
            json = {
                "flxChannel": {
                    "isVisible": false
                },
                "imgIconChannel": {
                    "isVisible": false
                },
                "channel": {
                    "text": ""
                },
            };
        } else if (channel.alerthistory_Status === "SID_DELIVERY_SUBMITTED") {
            val = channel.alerthistory_ChannelId;
            json = {
                "flxChannel": {
                    "isVisible": true
                },
                "imgIconChannel": {
                    "isVisible": true,
                    "skin": "sknFontIconActivate",
                    "text": "\ue94f"
                },
                "channel": {
                    "text": channel.channeltext_Description
                },
            };
            json[val] = {
                "subject": channel.alerthistory_Subject,
                "sentDate": this.getLocaleDate(channel.alerthistory_sentDate),
                "content": channel.alerthistory_Message
            };
        } else if (channel.alerthistory_Status === "SID_DELIVERYFAILED") {
            val = channel.alerthistory_ChannelId;
            json = {
                "flxChannel": {
                    "isVisible": true
                },
                "imgIconChannel": {
                    "isVisible": true,
                    "skin": "sknFontIconSuspend",
                    "text": "\ue929"
                },
                "channel": {
                    "text": channel.channeltext_Description
                },
            };
            json[val] = {
                "subject": channel.alerthistory_Subject,
                "sentDate": this.getLocaleDate(channel.alerthistory_sentDate),
                "content": channel.alerthistory_Message
            };
        }
        return json;
    },
    getAlertNameAndSentDate: function(alertHistoryItem) {
        var lblAlertName;
        var lblSentDate;
        if (alertHistoryItem.CH_PUSH_NOTIFICATION !== null && alertHistoryItem.CH_PUSH_NOTIFICATION !== undefined) {
            lblAlertName = alertHistoryItem.CH_PUSH_NOTIFICATION.alertsubtype_Name;
            lblSentDate = this.getLocaleDate(alertHistoryItem.CH_PUSH_NOTIFICATION.alerthistory_sentDate);
        }
        if (alertHistoryItem.CH_SMS !== null && alertHistoryItem.CH_SMS !== undefined) {
            lblAlertName = alertHistoryItem.CH_SMS.alertsubtype_Name;
            lblSentDate = this.getLocaleDate(alertHistoryItem.CH_SMS.alerthistory_sentDate);
        }
        if (alertHistoryItem.CH_EMAIL !== null && alertHistoryItem.CH_EMAIL !== undefined) {
            lblAlertName = alertHistoryItem.CH_EMAIL.alertsubtype_Name;
            lblSentDate = this.getLocaleDate(alertHistoryItem.CH_EMAIL.alerthistory_sentDate);
        }
        if (alertHistoryItem.CH_NOTIFICATION_CENTER !== null && alertHistoryItem.CH_NOTIFICATION_CENTER !== undefined) {
            lblAlertName = alertHistoryItem.CH_NOTIFICATION_CENTER.alertsubtype_Name;
            lblSentDate = this.getLocaleDate(alertHistoryItem.CH_NOTIFICATION_CENTER.alerthistory_sentDate);
        }
        return {
            "lblAlertName": lblAlertName,
            "lblSentDate": lblSentDate
        };
    },
    setDataForAlertHistorySegment: function(alertHistory) {
        var self = this;
        var dataMap = {
            "flxCustMangAlertHistory": "flxCustMangAlertHistory",
            "data": "data",
            "flxAlertName": "flxAlertName",
            "lblAlertName": "lblAlertName",
            "flxSentDate": "flxSentDate",
            "lblSentDate": "lblSentDate",
            "flxFirstColoum": "flxFirstColoum",
            "flxChannel1": "flxChannel1",
            "lblChannel1": "lblChannel1",
            "imgIconChannel1": "imgIconChannel1",
            "flxChannel2": "flxChannel2",
            "lblChannel2": "lblChannel2",
            "imgIconChannel2": "imgIconChannel2",
            "flxChannel3": "flxChannel3",
            "lblChannel3": "lblChannel3",
            "imgIconChannel3": "imgIconChannel3",
            "flxChannel4": "flxChannel4",
            "lblChannel4": "lblChannel4",
            "imgIconChannel4": "imgIconChannel4",
            "lblSeperator": "lblSeperator",
            "flxAddAlertBtn": "flxAddAlertBtn",
            "btnAddAlerts": "btnAddAlerts"
        };
        var data = [];
        var toAdd;
        if (alertHistory.length > 0) {
            for (var i = 0; i < alertHistory.length; i++) {
                var channelAlertData = [];
                var item = self.getAlertNameAndSentDate(alertHistory[i]);
                var pushInfo = self.getAlertChannelData(alertHistory[i].CH_PUSH_NOTIFICATION);
                if (pushInfo.CH_PUSH_NOTIFICATION !== undefined) {
                    channelAlertData.CH_PUSH_NOTIFICATION = pushInfo.CH_PUSH_NOTIFICATION;
                }
                var smsInfo = self.getAlertChannelData(alertHistory[i].CH_SMS);
                if (smsInfo.CH_SMS !== undefined) {
                    channelAlertData.CH_SMS = smsInfo.CH_SMS;
                }
                var emailInfo = self.getAlertChannelData(alertHistory[i].CH_EMAIL);
                if (emailInfo.CH_EMAIL !== undefined) {
                    channelAlertData.CH_EMAIL = emailInfo.CH_EMAIL;
                }
                var notificationCenterInfo = self.getAlertChannelData(alertHistory[i].CH_NOTIFICATION_CENTER);
                if (notificationCenterInfo.CH_NOTIFICATION_CENTER !== undefined) {
                    channelAlertData.CH_NOTIFICATION_CENTER = notificationCenterInfo.CH_NOTIFICATION_CENTER;
                }
                toAdd = {
                    "data": channelAlertData,
                    "flxAlertName": "flxAlertName",
                    "lblAlertName": item.lblAlertName,
                    "flxSentDate": "flxSentDate",
                    "lblSentDate": item.lblSentDate,
                    "flxFirstColoum": "flxFirstColoum",
                    "flxChannel1": pushInfo.flxChannel,
                    "lblChannel1": "PUSH",
                    "imgIconChannel1": pushInfo.imgIconChannel,
                    "flxChannel2": smsInfo.flxChannel,
                    "lblChannel2": "SMS",
                    "imgIconChannel2": smsInfo.imgIconChannel,
                    "flxChannel3": emailInfo.flxChannel,
                    "lblChannel3": "EMAIL",
                    "imgIconChannel3": emailInfo.imgIconChannel,
                    "flxChannel4": notificationCenterInfo.flxChannel,
                    "lblChannel4": "NOTIFICATION CENTER",
                    "imgIconChannel4": notificationCenterInfo.imgIconChannel,
                    "lblSeperator": "lblSeperator",
                    "flxAddAlertBtn": "flxAddAlertBtn",
                    "btnAddAlerts": {
                        "text": "Preview",
                        "onClick": self.showAlertPreview
                    },
                    "template": "flxCustMangAlertHistory"
                };
                data.push(toAdd);
            }
            this.view.segAlertHistory.widgetDataMap = dataMap;
            this.view.segAlertHistory.setData(data);
            this.view.segAlertHistory.info = {
                "data": data,
                "searchAndSortData": data
            };
            this.view.flxAlertsHistorySegmentHeader.setVisibility(true);
            this.view.lblNotificationHeaderSeperator.setVisibility(true);
            this.view.rtxMsgNotifications.setVisibility(false);
            this.view.segAlertHistory.setVisibility(true);
            this.view.notificationsSearch.flxSubHeader.setVisibility(true);
        } else {
            this.view.segAlertHistory.setVisibility(false);
            this.view.lblNotificationHeaderSeperator.setVisibility(false);
            this.view.rtxMsgNotifications.setVisibility(true);
            this.view.flxAlertsHistorySegmentHeader.setVisibility(false);
            this.view.notificationsSearch.flxSubHeader.setVisibility(false);
        }
        this.view.notificationsSearch.tbxSearchBox.text = "";
        this.showNotificationsScreen();
    },
    setSkinForChannelTabs: function(btnWidget) {
        this.view.btnSMS.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
        this.view.btnPushNoti.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
        this.view.btnEmail.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
        this.view.btnNotifCenter.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
        btnWidget.skin = "sknbtnBgffffffLato485c75Radius3Px12Px";
    },
    setDataForNotificationsSegment: function(CustomerNotifications) {
        var dataMap = {
            "fonticonArrow": "fonticonArrow",
            "flxArrow": "flxArrow",
            "flxCustMangNotification": "flxCustMangNotification",
            "flxCustMangNotificationSelected": "flxCustMangNotificationSelected",
            "flxCustMangRequestDesc": "flxCustMangRequestDesc",
            "flxCustMangRequestHeader": "flxCustMangRequestHeader",
            "flxFirstColoum": "flxFirstColoum",
            "flxSeperator": "flxSeperator",
            "lblDesc": "lblDesc",
            "lblExpDate": "lblExpDate",
            "lblName": "lblName",
            "lblSeperator": "lblSeperator",
            "lblStartDate": "lblStartDate",
            "lblStatus": "lblStatus"
        };
        var data = [];
        var toAdd;
        if (CustomerNotifications.length > 0) {
            for (var i = 0; i < CustomerNotifications.length; i++) {
                var statusName;
                if (CustomerNotifications[i].Status_id.toUpperCase() === "SID_N_ACTIVE".toUpperCase()) {
                    statusName = this.sActive;
                } else if (CustomerNotifications[i].Status_id.toUpperCase() === "SID_STARTINGSOON".toUpperCase()) {
                    statusName = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Starting");
                } else if (CustomerNotifications[i].Status_id.toUpperCase() === "SID_EXPIRED".toUpperCase()) {
                    statusName = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Expired");
                }
                toAdd = {
                    "fonticonArrow": {
                        "text": "î¤¢",
                        "isVisible": true
                    },
                    "flxArrow": "flxArrow",
                    "flxCustMangNotification": "flxCustMangNotification",
                    "flxCustMangNotificationSelected": "flxCustMangNotificationSelected",
                    "flxCustMangRequestDesc": "flxCustMangRequestDesc",
                    "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                    "flxFirstColoum": "flxFirstColoum",
                    "flxSeperator": "flxSeperator",
                    "lblDesc": CustomerNotifications[i].Description,
                    "lblExpDate": this.getLocaleDate(CustomerNotifications[i].ExpirationDate),
                    "lblName": CustomerNotifications[i].Name,
                    "lblSeperator": ".",
                    "lblStartDate": this.getLocaleDate(CustomerNotifications[i].StartDate),
                    "lblStatus": {
                        "text": statusName
                    },
                    "template": "flxCustMangNotification"
                };
                data.push(toAdd);
            }
            this.view.segNotifications.widgetDataMap = dataMap;
            this.view.segNotifications.setData(data);
            this.view.segNotifications.info = {
                "data": data,
                "searchAndSortData": data
            };
            this.view.flxNotificationsSegmentHeader.setVisibility(true);
            this.view.lblNotificationHeaderSeperator.setVisibility(true);
            this.view.rtxMsgNotifications.setVisibility(false);
            this.view.segNotifications.setVisibility(true);
            this.view.notificationsSearch.flxSubHeader.setVisibility(true);
        } else {
            this.view.flxNotificationsSegmentHeader.setVisibility(false);
            this.view.lblNotificationHeaderSeperator.setVisibility(false);
            this.view.rtxMsgNotifications.setVisibility(true);
            this.view.segNotifications.setVisibility(false);
            this.view.notificationsSearch.flxSubHeader.setVisibility(false);
        }
        this.view.notificationsSearch.tbxSearchBox.text = "";
        this.showNotificationsScreen();
    },
});
define("CustomerManagementModule/frmCustomerProfileAlertsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmCustomerProfileAlerts **/
    AS_Form_a16511c8268f4751ac909afee384f0ee: function AS_Form_a16511c8268f4751ac909afee384f0ee(eventobject) {
        var self = this;
        this.CustomerProfileAlertsPreshow();
    },
    /** onDeviceBack defined for frmCustomerProfileAlerts **/
    AS_Form_a9f07620aedb42549163b3e801777730: function AS_Form_a9f07620aedb42549163b3e801777730(eventobject) {
        var self = this;
    }
});
define("CustomerManagementModule/frmCustomerProfileAlertsController", ["CustomerManagementModule/userfrmCustomerProfileAlertsController", "CustomerManagementModule/frmCustomerProfileAlertsControllerActions"], function() {
    var controller = require("CustomerManagementModule/userfrmCustomerProfileAlertsController");
    var controllerActions = ["CustomerManagementModule/frmCustomerProfileAlertsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
