define("CustomerManagementModule/frmLoansDashboard", function() {
    return function(controller) {
        function addWidgetsfrmLoansDashboard() {
            this.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305dp",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainLoansDashboard = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxMainLoansDashboard",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "CopysknFlxScrlf0d7845baf952542",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxMainLoansDashboard.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false,
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "top": "58px"
                    },
                    "imgLogout": {
                        "isVisible": true,
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "text": "Customer Management"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var btnNotes = new kony.ui.Button({
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "30px",
                "id": "btnNotes",
                "isVisible": true,
                "right": "35px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnNotes\")",
                "top": "58dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxMainHeader.add(mainHeader, btnNotes);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SearchCustomers\")",
                        "left": "35px"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "text": "BRYAN NASH",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            breadcrumbs.btnBackToMain.onClick = controller.AS_Button_ad742133b6444958bebfbace58ce5e65;
            flxBreadCrumbs.add(breadcrumbs);
            var CSRAssistToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "CSRAssistToolTip",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "50px",
                "skin": "slFbox",
                "top": "180dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": true,
                        "left": "viz.val_cleared",
                        "right": "50px",
                        "top": "180dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMainScrollContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxMainScrollContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": 0,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "140dp",
                "verticalScrollIndicator": true
            }, {}, {});
            flxMainScrollContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderAndMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderAndMainContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxHeaderAndMainContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderAndCardsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderAndCardsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBordere4e6ecCustomRadius5px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeaderAndCardsContainer.setDefaultUnit(kony.flex.DP);
            var flxGeneralInfoHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGeneralInfoHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknFlxFFFFFF100O",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxGeneralInfoHeader.setDefaultUnit(kony.flex.DP);
            var generalInfoHeader = new com.adminConsole.customerMang.generalInfoHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "generalInfoHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "overrides": {
                    "flxActionButtons": {
                        "reverseLayoutDirection": false
                    },
                    "flxCSRAssist": {
                        "isVisible": true
                    },
                    "flxHeader": {
                        "width": "100%"
                    },
                    "flxRiskStatus": {
                        "isVisible": true,
                        "zIndex": 10
                    },
                    "flxUnlock": {
                        "isVisible": true,
                        "left": "viz.val_cleared",
                        "right": 150
                    },
                    "generalInfoHeader": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "left": "35px",
                        "right": "35px",
                        "top": "0px",
                        "width": "viz.val_cleared"
                    },
                    "lblSeparator": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxGeneralInfoHeader.add(generalInfoHeader);
            var alertMessage = new com.adminConsole.customerMang.alertMessage({
                "clipBounds": true,
                "height": "70px",
                "id": "alertMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "width": "93%",
                "zIndex": 1,
                "overrides": {
                    "alertMessage": {
                        "bottom": "viz.val_cleared",
                        "isVisible": true,
                        "left": "35px",
                        "right": "viz.val_cleared",
                        "top": "viz.val_cleared",
                        "width": "93%",
                        "zIndex": 1
                    },
                    "lblData1": {
                        "centerY": "50%",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.requestsAndNotificationsCountPrefix\")",
                        "left": "5px",
                        "top": "15px"
                    },
                    "lblData2": {
                        "centerY": "50%",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.requestsAndNotificationsCountSufix\")",
                        "isVisible": true,
                        "left": "5px",
                        "top": "15px"
                    },
                    "lblData3": {
                        "centerY": "50%",
                        "left": "1px",
                        "top": "15px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxProductsTabsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxProductsTabsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxFFFFFF1000",
                "top": "0dp",
                "zIndex": 50
            }, {}, {});
            flxProductsTabsWrapper.setDefaultUnit(kony.flex.DP);
            var dashboardCommonTab = new com.adminConsole.commonTabsDashboard.dashboardCommonTab({
                "clipBounds": true,
                "height": "100%",
                "id": "dashboardCommonTab",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxProductsTabsWrapper.add(dashboardCommonTab);
            var flxLoansMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLoansMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxLoansMainContainer.setDefaultUnit(kony.flex.DP);
            var flx360degreeView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flx360degreeView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 100
            }, {}, {});
            flx360degreeView.setDefaultUnit(kony.flex.DP);
            var flxContainingPendingAndSubmittedApplications = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContainingPendingAndSubmittedApplications",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 100
            }, {}, {});
            flxContainingPendingAndSubmittedApplications.setDefaultUnit(kony.flex.DP);
            var lblApplicationsMeta = new kony.ui.Label({
                "id": "lblApplicationsMeta",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLblLato84939e13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Applications\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPendingApplication = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxPendingApplication",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_b2cf0804e52642d592da8109353a86f2,
                "skin": "sknFlxBordere1e5eedCustomRadius5px",
                "top": "10dp",
                "width": "180dp",
                "zIndex": 100
            }, {}, {
                "hoverSkin": "sknFlxBorder22dsd3Hover"
            });
            flxPendingApplication.setDefaultUnit(kony.flex.DP);
            var lblPendingApplicationIcon = new kony.ui.Label({
                "id": "lblPendingApplicationIcon",
                "isVisible": true,
                "left": "75%",
                "skin": "sknEyeIcon30px",
                "text": "",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblpendingApplciationNumber = new kony.ui.Label({
                "id": "lblpendingApplciationNumber",
                "isVisible": true,
                "left": "10%",
                "skin": "sknLblFont274060100o",
                "text": "00",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblpendingApplication = new kony.ui.Label({
                "bottom": "20dp",
                "id": "lblpendingApplication",
                "isVisible": true,
                "left": "10%",
                "skin": "sknLblFontf39752100O",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.StartedApplication\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPendingApplication.add(lblPendingApplicationIcon, lblpendingApplciationNumber, lblpendingApplication);
            var flxSubmittedApplications = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxSubmittedApplications",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_fcbee1b7c9b14bfdbb14fea68099798e,
                "skin": "sknFlxBordere1e5eedCustomRadius5px",
                "top": "20dp",
                "width": "180dp",
                "zIndex": 100
            }, {}, {
                "hoverSkin": "sknFlxBordere0j000Hover"
            });
            flxSubmittedApplications.setDefaultUnit(kony.flex.DP);
            var lblSubmittedApplicationsIcon = new kony.ui.Label({
                "id": "lblSubmittedApplicationsIcon",
                "isVisible": true,
                "left": "75%",
                "skin": "sknEyeIcon30px",
                "text": "",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSubmittedApplicationsNumber = new kony.ui.Label({
                "id": "lblSubmittedApplicationsNumber",
                "isVisible": true,
                "left": "10%",
                "skin": "sknLblFont274060100o",
                "text": "00",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSubmiitedApplicationsStatic = new kony.ui.Label({
                "id": "lblSubmiitedApplicationsStatic",
                "isVisible": true,
                "left": "10%",
                "skin": "sknLblFont6281be1000",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SubmittedApplication\")",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubmittedApplications.add(lblSubmittedApplicationsIcon, lblSubmittedApplicationsNumber, lblSubmiitedApplicationsStatic);
            flxContainingPendingAndSubmittedApplications.add(lblApplicationsMeta, flxPendingApplication, flxSubmittedApplications);
            var flxSeparatorHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSeparatorHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 500
            }, {}, {});
            flxSeparatorHeader.setDefaultUnit(kony.flex.DP);
            var lblSeparatorHeader = new kony.ui.Label({
                "id": "lblSeparatorHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSeparatorHeader.add(lblSeparatorHeader);
            var flxLoanTypesContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLoanTypesContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxLoanTypesContainer.setDefaultUnit(kony.flex.DP);
            var flxLoanTypes = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "135dp",
                "horizontalScrollIndicator": true,
                "id": "flxLoanTypes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "verticalScrollIndicator": true,
                "width": "93%",
                "zIndex": 100
            }, {}, {});
            flxLoanTypes.setDefaultUnit(kony.flex.DP);
            flxLoanTypes.add();
            var loaninfo = new com.adminConsole.loans.applications.loantype.loaninfo({
                "bottom": "7dp",
                "clipBounds": true,
                "height": "100dp",
                "id": "loaninfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBordere1e5eedCustomRadius5px",
                "top": "7dp",
                "width": "32.50%",
                "overrides": {
                    "btnApplyLoan": {
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "flxApplyLoan": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "20dp",
                        "left": "10dp",
                        "top": "viz.val_cleared"
                    },
                    "flxLearnMore": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerY": "viz.val_cleared",
                        "left": "73dp",
                        "top": "5dp"
                    },
                    "flxLoanAprDetails": {
                        "isVisible": true,
                        "left": "10dp",
                        "width": "99%"
                    },
                    "flxLoanTypeIcon": {
                        "height": "49dp",
                        "left": "20dp",
                        "width": "49dp"
                    },
                    "lblLoanApr": {
                        "left": "73dp"
                    },
                    "lblNewLoanType": {
                        "left": "83dp"
                    },
                    "lblNewLoanTypeId": {
                        "top": "30dp"
                    },
                    "loaninfo": {
                        "bottom": "7dp",
                        "centerX": "viz.val_cleared",
                        "height": "100dp",
                        "isVisible": false,
                        "left": "1%",
                        "top": "7dp",
                        "width": "32.50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLoanTypesContainer.add(flxLoanTypes, loaninfo);
            flx360degreeView.add(flxContainingPendingAndSubmittedApplications, flxSeparatorHeader, flxLoanTypesContainer);
            flxLoansMainContainer.add(flx360degreeView);
            var flxRegistrationWrapperKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRegistrationWrapperKA",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "81%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "16%",
                "zIndex": 51
            }, {}, {});
            flxRegistrationWrapperKA.setDefaultUnit(kony.flex.DP);
            var flxRegistrationLinkKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRegistrationLinkKA",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 52
            }, {}, {});
            flxRegistrationLinkKA.setDefaultUnit(kony.flex.DP);
            var lblRegistrationLinkKA = new kony.ui.Label({
                "id": "lblRegistrationLinkKA",
                "isVisible": true,
                "left": "5%",
                "onTouchEnd": controller.AS_Label_b5befc3b01724c89a3ada56766fe60f6,
                "skin": "sknLbl006CCA13pxKA",
                "text": "Send Registration Email",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblHover006CCA13pxKA"
            });
            flxRegistrationLinkKA.add(lblRegistrationLinkKA);
            var flxRegistrationLinkOptionsKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRegistrationLinkOptionsKA",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBordere1e5eedCustomRadius5px",
                "top": "45dp",
                "width": "95%",
                "zIndex": 11
            }, {}, {});
            flxRegistrationLinkOptionsKA.setDefaultUnit(kony.flex.DP);
            var lblSendLinkKA = new kony.ui.Label({
                "id": "lblSendLinkKA",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato5d6c7f12px",
                "text": "SEND LINK VIA",
                "top": "7dp",
                "width": "80%",
                "zIndex": 11
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailoptionKA = new kony.ui.Label({
                "id": "lblEmailoptionKA",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLblFont485C75100O",
                "text": "Email",
                "top": "10dp",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl11abeb100O"
            });
            var lblSMSOptionKA = new kony.ui.Label({
                "id": "lblSMSOptionKA",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLblFont485C75100O",
                "text": "SMS",
                "top": "5dp",
                "width": "80%",
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl11abeb100O"
            });
            var lblEmailAndSMSOptionKA = new kony.ui.Label({
                "bottom": "5dp",
                "id": "lblEmailAndSMSOptionKA",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLblFont485C75100O",
                "text": "Both email and SMS",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl11abeb100O"
            });
            flxRegistrationLinkOptionsKA.add(lblSendLinkKA, lblEmailoptionKA, lblSMSOptionKA, lblEmailAndSMSOptionKA);
            flxRegistrationWrapperKA.add(flxRegistrationLinkKA, flxRegistrationLinkOptionsKA);
            flxMainContainer.add(flxProductsTabsWrapper, flxLoansMainContainer, flxRegistrationWrapperKA);
            flxHeaderAndCardsContainer.add(flxGeneralInfoHeader, alertMessage, flxMainContainer);
            var flxListOfApplicationsandLeadsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxListOfApplicationsandLeadsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBordere4e6ecCustomRadius5px",
                "top": "12dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxListOfApplicationsandLeadsContainer.setDefaultUnit(kony.flex.DP);
            var flxApplicationAndLeadsStaticContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxApplicationAndLeadsStaticContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF1000",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxApplicationAndLeadsStaticContainer.setDefaultUnit(kony.flex.DP);
            var flxApplicationsStatic = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxApplicationsStatic",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "90dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxApplicationsStatic.setDefaultUnit(kony.flex.DP);
            var lblApplicationStatic = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblApplicationStatic",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblFont485C75100O",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplicationStatic\")",
                "top": "9dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblApplicationsBlueLineIndication = new kony.ui.Label({
                "height": "3dp",
                "id": "lblApplicationsBlueLineIndication",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl00a9ee100O",
                "top": "18dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxApplicationsStatic.add(lblApplicationStatic, lblApplicationsBlueLineIndication);
            var flxLeadsStatic = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxLeadsStatic",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxLeadsStatic.setDefaultUnit(kony.flex.DP);
            var lblLeadsStatic = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLeadsStatic",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.LeadsStatic\")",
                "top": "9dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLeadsBlueLineIndication = new kony.ui.Label({
                "height": "3dp",
                "id": "lblLeadsBlueLineIndication",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLbl00a9ee100O",
                "top": "18dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLeadsStatic.add(lblLeadsStatic, lblLeadsBlueLineIndication);
            flxApplicationAndLeadsStaticContainer.add(flxApplicationsStatic, flxLeadsStatic);
            var lblTabsSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblTabsSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblSeparator",
                "width": "100%",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPendingSubmittedTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxPendingSubmittedTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBge7eaeeOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPendingSubmittedTabs.setDefaultUnit(kony.flex.DP);
            var btnPendingTab = new kony.ui.Button({
                "height": "37dp",
                "id": "btnPendingTab",
                "isVisible": true,
                "left": "30dp",
                "skin": "sknBord7d9e0Rounded3px485c75",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.StartedApplication\")",
                "top": "13dp",
                "width": "85dp",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSubmittedTab = new kony.ui.Button({
                "height": "37dp",
                "id": "btnSubmittedTab",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SubmittedApplication\")",
                "top": "13dp",
                "width": "100dp",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPendingSubmittedTabs.add(btnPendingTab, btnSubmittedTab);
            var flxCreateLeadConatiner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxCreateLeadConatiner",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCreateLeadConatiner.setDefaultUnit(kony.flex.DP);
            var btnCreateLead = new kony.ui.Button({
                "height": "30dp",
                "id": "btnCreateLead",
                "isVisible": true,
                "right": "32dp",
                "skin": "sknCreateCustomer",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CreateLead\")",
                "top": "14dp",
                "width": "96dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCreateLeadConatiner.add(btnCreateLead);
            var flxListOfApplications = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxListOfApplications",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxListOfApplications.setDefaultUnit(kony.flex.DP);
            var flxListOfApplicationsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxListOfApplicationsHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF1000",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxListOfApplicationsHeader.setDefaultUnit(kony.flex.DP);
            var flxApplicationIDContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxApplicationIDContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxApplicationIDContainer.setDefaultUnit(kony.flex.DP);
            var lblApplicationIDHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblApplicationIDHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplicationId\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxApplicationIDContainer.add(lblApplicationIDHeader);
            var flxLoanTypeContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxLoanTypeContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "19%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxLoanTypeContainer.setDefaultUnit(kony.flex.DP);
            var lblLoanTypeHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLoanTypeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.LoanTypeHeader\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLoanTypeContainer.add(lblLoanTypeHeader);
            var flxAmountContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxAmountContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "32%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxAmountContainer.setDefaultUnit(kony.flex.DP);
            var lblAmountHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmountHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Amount\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblAmountHeaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmountHeaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxAmountContainer.add(lblAmountHeader, lblAmountHeaderImg);
            var flxApplicantContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxApplicantContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "48%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxApplicantContainer.setDefaultUnit(kony.flex.DP);
            var lblApplicantHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblApplicantHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Applicant\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxApplicantContainer.add(lblApplicantHeader);
            var flxModifyOnContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxModifyOnContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "64%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "14dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxModifyOnContainer.setDefaultUnit(kony.flex.DP);
            var lblModifyOnHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblModifyOnHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ModifiedOn\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblModifyOnHeaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblModifyOnHeaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxModifyOnContainer.add(lblModifyOnHeader, lblModifyOnHeaderImg);
            var flxCreateOnContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCreateOnContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "80%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "14dp",
                "width": "14%",
                "zIndex": 1
            }, {}, {});
            flxCreateOnContainer.setDefaultUnit(kony.flex.DP);
            var lblCreateOnHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCreateOnHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CreatedOn\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblCreateOnheaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCreateOnheaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCreateOnContainer.add(lblCreateOnHeader, lblCreateOnheaderImg);
            var lblSeparatorApplicationHeader = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeparatorApplicationHeader",
                "isVisible": true,
                "left": "10dp",
                "right": "22dp",
                "skin": "sknLblSeparator696C73",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxListOfApplicationsHeader.add(flxApplicationIDContainer, flxLoanTypeContainer, flxAmountContainer, flxApplicantContainer, flxModifyOnContainer, flxCreateOnContainer, lblSeparatorApplicationHeader);
            var flxListOfApplicationsSubmittedHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxListOfApplicationsSubmittedHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF1000",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxListOfApplicationsSubmittedHeader.setDefaultUnit(kony.flex.DP);
            var flxApplicationIDContainerSubmitted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxApplicationIDContainerSubmitted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxApplicationIDContainerSubmitted.setDefaultUnit(kony.flex.DP);
            var lblApplicationHeaderSubmitted = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblApplicationHeaderSubmitted",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplicationId\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxApplicationIDContainerSubmitted.add(lblApplicationHeaderSubmitted);
            var flxLoanTypeContainerSubmitted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxLoanTypeContainerSubmitted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "19%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxLoanTypeContainerSubmitted.setDefaultUnit(kony.flex.DP);
            var lblLoanTypeHeaderSubmitted = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLoanTypeHeaderSubmitted",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.LoanTypeHeader\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLoanTypeContainerSubmitted.add(lblLoanTypeHeaderSubmitted);
            var flxAmountContainerSubmitted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxAmountContainerSubmitted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "32%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxAmountContainerSubmitted.setDefaultUnit(kony.flex.DP);
            var lblAmountHeaderSubmitted = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmountHeaderSubmitted",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Amount\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblAmountHeaderSubmittedImage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmountHeaderSubmittedImage",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxAmountContainerSubmitted.add(lblAmountHeaderSubmitted, lblAmountHeaderSubmittedImage);
            var flxApplicantContainerSubmitted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxApplicantContainerSubmitted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "45%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxApplicantContainerSubmitted.setDefaultUnit(kony.flex.DP);
            var lblApplicantHeaderSubmitted = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblApplicantHeaderSubmitted",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Applicant\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxApplicantContainerSubmitted.add(lblApplicantHeaderSubmitted);
            var flxSubmitOnContainerSubmitted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSubmitOnContainerSubmitted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "61%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxSubmitOnContainerSubmitted.setDefaultUnit(kony.flex.DP);
            var lblSubmitOnHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubmitOnHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SubmittedOn\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblSubmitOnHeaderImage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubmitOnHeaderImage",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxSubmitOnContainerSubmitted.add(lblSubmitOnHeader, lblSubmitOnHeaderImage);
            var flxStatusContainerSubmitted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxStatusContainerSubmitted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "77%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxStatusContainerSubmitted.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Status\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStatusContainerSubmitted.add(lblStatus);
            var lblSeparatorApplicationHeaderSubmitted = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeparatorApplicationHeaderSubmitted",
                "isVisible": true,
                "left": "22dp",
                "right": "22dp",
                "skin": "sknLblSeparator696C73",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxListOfApplicationsSubmittedHeader.add(flxApplicationIDContainerSubmitted, flxLoanTypeContainerSubmitted, flxAmountContainerSubmitted, flxApplicantContainerSubmitted, flxSubmitOnContainerSubmitted, flxStatusContainerSubmitted, lblSeparatorApplicationHeaderSubmitted);
            var segmentListOfApplications = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAmount": "",
                    "lblApplicant": "Label",
                    "lblApplicationId": "",
                    "lblCreateOn": "",
                    "lblLoanType": "",
                    "lblModifyOn": "",
                    "lblResume": "",
                    "lblSeparatorListOfApplications": ""
                }],
                "groupCells": false,
                "id": "segmentListOfApplications",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxListOfApplications",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "60dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxListOfApplications": "flxListOfApplications",
                    "lblAmount": "lblAmount",
                    "lblApplicant": "lblApplicant",
                    "lblApplicationId": "lblApplicationId",
                    "lblCreateOn": "lblCreateOn",
                    "lblLoanType": "lblLoanType",
                    "lblModifyOn": "lblModifyOn",
                    "lblResume": "lblResume",
                    "lblSeparatorListOfApplications": "lblSeparatorListOfApplications"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segmentListOfApplicationsSubmitted = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAmount": "Label",
                    "lblApplicant": "Label",
                    "lblApplicationId": "Label",
                    "lblLoanType": "Label",
                    "lblResume": "",
                    "lblSeparatorListOfApplications": "",
                    "lblStatus": "Label",
                    "lblSubmitOn": "Label",
                    "lblTrack": ""
                }],
                "groupCells": false,
                "id": "segmentListOfApplicationsSubmitted",
                "isVisible": false,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxListOfApplicationsSubmitted",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "60dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxListOfApplicationsSubmitted": "flxListOfApplicationsSubmitted",
                    "lblAmount": "lblAmount",
                    "lblApplicant": "lblApplicant",
                    "lblApplicationId": "lblApplicationId",
                    "lblLoanType": "lblLoanType",
                    "lblResume": "lblResume",
                    "lblSeparatorListOfApplications": "lblSeparatorListOfApplications",
                    "lblStatus": "lblStatus",
                    "lblSubmitOn": "lblSubmitOn",
                    "lblTrack": "lblTrack"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelectOptionsApplications = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptionsApplications",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "33px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100px",
                "width": "200px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptionsApplications.setDefaultUnit(kony.flex.DP);
            var flxEditApplications = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEditApplications",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditApplications.setDefaultUnit(kony.flex.DP);
            var fontIconEditApplications = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconEditApplications",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOptionEditApplications = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOptionEditApplications",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold0hf492575252c4cStatic"
            });
            flxEditApplications.add(fontIconEditApplications, lblOptionEditApplications);
            var flxTrackApplications = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxTrackApplications",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a35230cb45374d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTrackApplications.setDefaultUnit(kony.flex.DP);
            var fonticonTrackApplications = new kony.ui.Label({
                "centerY": "50%",
                "id": "fonticonTrackApplications",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOptionTrackApplications = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOptionTrackApplications",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Track\")",
                "top": "4dp",
                "width": "130px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold0hf492575252c4cStatic"
            });
            flxTrackApplications.add(fonticonTrackApplications, lblOptionTrackApplications);
            flxSelectOptionsApplications.add(flxEditApplications, flxTrackApplications);
            flxListOfApplications.add(flxListOfApplicationsHeader, flxListOfApplicationsSubmittedHeader, segmentListOfApplications, segmentListOfApplicationsSubmitted, flxSelectOptionsApplications);
            var flxListOfLeads = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxListOfLeads",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxListOfLeads.setDefaultUnit(kony.flex.DP);
            var flxListOfLeadsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxListOfLeadsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF1000",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxListOfLeadsHeader.setDefaultUnit(kony.flex.DP);
            var flxListOfLeadsHeaderInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxListOfLeadsHeaderInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxListOfLeadsHeaderInner.setDefaultUnit(kony.flex.DP);
            var flxProductContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxProductContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "28dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "22%",
                "zIndex": 1
            }, {}, {});
            flxProductContainer.setDefaultUnit(kony.flex.DP);
            var lblProductHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProductHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Product\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblProductHeaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProductHeaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxProductContainer.add(lblProductHeader, lblProductHeaderImg);
            var flxStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxStatusContainer.setDefaultUnit(kony.flex.DP);
            var lblStatusHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatusHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Status\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblStatusHeaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatusHeaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxStatusContainer.add(lblStatusHeader, lblStatusHeaderImg);
            var flxLeadsCreatedOnContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxLeadsCreatedOnContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxLeadsCreatedOnContainer.setDefaultUnit(kony.flex.DP);
            var lblLeadsCreatedOnHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLeadsCreatedOnHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CreatedOn\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblLeadsCreatedOnHeaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLeadsCreatedOnHeaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxLeadsCreatedOnContainer.add(lblLeadsCreatedOnHeader, lblLeadsCreatedOnHeaderImg);
            var flxCreatedByContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCreatedByContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxCreatedByContainer.setDefaultUnit(kony.flex.DP);
            var lblCreatedByHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCreatedByHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CreatedBy\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblCreatedByHeaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCreatedByHeaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCreatedByContainer.add(lblCreatedByHeader, lblCreatedByHeaderImg);
            var flxAssignToContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxAssignToContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxAssignToContainer.setDefaultUnit(kony.flex.DP);
            var lblAssignToHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAssignToHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.AssignTo\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblAssignToHeaderImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAssignToHeaderImg",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxAssignToContainer.add(lblAssignToHeader, lblAssignToHeaderImg);
            flxListOfLeadsHeaderInner.add(flxProductContainer, flxStatusContainer, flxLeadsCreatedOnContainer, flxCreatedByContainer, flxAssignToContainer);
            var lblSeparatorLeadsHeader = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeparatorLeadsHeader",
                "isVisible": true,
                "left": "22dp",
                "right": "22dp",
                "skin": "sknLblSeparator696C73",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxListOfLeadsHeader.add(flxListOfLeadsHeaderInner, lblSeparatorLeadsHeader);
            var segmentListOfLeads = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "imgOptions": "dots3x.png",
                    "lblAssignTo": "Label",
                    "lblCreatedBy": "Label",
                    "lblCreatedOn": "Label",
                    "lblIconOptions": "",
                    "lblProduct": "Label",
                    "lblSeparatorListOfLeads": "",
                    "lblStatus": "Label"
                }],
                "groupCells": false,
                "id": "segmentListOfLeads",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxListOfLeads",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "60dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxListOfLeads": "flxListOfLeads",
                    "flxListOfLeadsInner": "flxListOfLeadsInner",
                    "lblAssignTo": "lblAssignTo",
                    "lblCreatedBy": "lblCreatedBy",
                    "lblCreatedOn": "lblCreatedOn",
                    "lblProduct": "lblProduct",
                    "lblSeparatorListOfLeads": "lblSeparatorListOfLeads",
                    "lblStatus": "lblStatus"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelectOptionsLeads = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptionsLeads",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "33px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100px",
                "width": "200px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptionsLeads.setDefaultUnit(kony.flex.DP);
            var flxEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEdit.setDefaultUnit(kony.flex.DP);
            var fonticonEdit = new kony.ui.Label({
                "centerY": "50%",
                "id": "fonticonEdit",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgOption1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption1",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold0hf492575252c4cStatic"
            });
            flxEdit.add(fonticonEdit, imgOption1, lblOption1);
            var flxMoveToProgress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxMoveToProgress",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a35230cb45374d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMoveToProgress.setDefaultUnit(kony.flex.DP);
            var fonticonMoveToProgress = new kony.ui.Label({
                "centerY": "50%",
                "id": "fonticonMoveToProgress",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDeactive\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgOption2 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption2",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "deactive_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Move to In Progress",
                "top": "4dp",
                "width": "130px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold0hf492575252c4cStatic"
            });
            flxMoveToProgress.add(fonticonMoveToProgress, imgOption2, lblOption2);
            var flxMarkAsClosed = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxMarkAsClosed",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a6ed527bebbb44",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMarkAsClosed.setDefaultUnit(kony.flex.DP);
            var fontIconOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconOption3",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconOptionMenuRow",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMarkAsClosedOption = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMarkAsClosedOption",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Mark as Closed",
                "top": "4dp",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato11ABEB13px"
            });
            flxMarkAsClosed.add(fontIconOption3, lblMarkAsClosedOption);
            flxSelectOptionsLeads.add(flxEdit, flxMoveToProgress, flxMarkAsClosed);
            flxListOfLeads.add(flxListOfLeadsHeader, segmentListOfLeads, flxSelectOptionsLeads);
            var flxNoPendingLoansAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120dp",
                "id": "flxNoPendingLoansAvailable",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxNoPendingLoansAvailable.setDefaultUnit(kony.flex.DP);
            var lblNoPendingLoans = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblNoPendingLoans",
                "isVisible": true,
                "skin": "sknLbl485c75FontSize12px",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoPendingLoansAvailable.add(lblNoPendingLoans);
            flxListOfApplicationsandLeadsContainer.add(flxApplicationAndLeadsStaticContainer, lblTabsSeperator, flxPendingSubmittedTabs, flxCreateLeadConatiner, flxListOfApplications, flxListOfLeads, flxNoPendingLoansAvailable);
            var flxBottomPadding = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "48dp",
                "id": "flxBottomPadding",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBottomPadding.setDefaultUnit(kony.flex.DP);
            flxBottomPadding.add();
            flxHeaderAndMainContainer.add(flxHeaderAndCardsContainer, flxListOfApplicationsandLeadsContainer, flxBottomPadding);
            flxMainScrollContainer.add(flxHeaderAndMainContainer);
            var flxSelectOptions = new com.adminConsole.customerMang.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "55px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "190dp",
                "width": "200px",
                "overrides": {
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementPersonal.upgradeToMicroBusiness\")"
                    },
                    "selectOptions": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "55px",
                        "top": "190dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainLoansDashboard.add(flxMainHeader, flxBreadCrumbs, CSRAssistToolTip, flxMainScrollContainer, flxSelectOptions);
            var flxNote = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNote",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "30%",
                "zIndex": 100
            }, {}, {});
            flxNote.setDefaultUnit(kony.flex.DP);
            var Notes = new com.adminConsole.customerMang.Notes({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "Notes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0d4d94de464db42CM",
                "width": "100%",
                "overrides": {
                    "flxNotes": {
                        "top": "viz.val_cleared"
                    },
                    "imgCloseNotes": {
                        "src": "close_blue.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNote.add(Notes);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            flxRightPanel.add(flxMainLoansDashboard, flxNote, flxHeaderDropdown);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoadingBlur",
                "top": "0px",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxCoAInfoNDisabledBackground = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxCoAInfoNDisabledBackground",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlx333333KA",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxCoAInfoNDisabledBackground.setDefaultUnit(kony.flex.DP);
            var flxCoAAddInformationPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "62.14%",
                "centerY": "50.00%",
                "clipBounds": true,
                "height": "410dp",
                "id": "flxCoAAddInformationPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "340dp",
                "isModalContainer": false,
                "skin": "sknFlxBordere4e6ecCustomRadius5px",
                "top": "150dp",
                "width": "71%",
                "zIndex": 10
            }, {}, {});
            flxCoAAddInformationPopup.setDefaultUnit(kony.flex.DP);
            var flxCoAInfoContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "15dp",
                "id": "flxCoAInfoContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "22dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxCoAInfoContainer.setDefaultUnit(kony.flex.DP);
            var lblCoApplicantInfo = new kony.ui.Label({
                "id": "lblCoApplicantInfo",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "text": "CO-APPLICANT INFORMATION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCancelCoAInfo = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgCancelCoAInfo",
                "isVisible": true,
                "onTouchEnd": controller.AS_Image_fc85e1cb4fa74b90883ede942e4ea59b,
                "right": "15dp",
                "skin": "slImage",
                "src": "close_blue.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCoAInfoContainer.add(lblCoApplicantInfo, imgCancelCoAInfo);
            var flxSeparatorBelowCoAInformation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "0.20%",
                "id": "flxSeparatorBelowCoAInformation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "90%",
                "zIndex": 500
            }, {}, {});
            flxSeparatorBelowCoAInformation.setDefaultUnit(kony.flex.DP);
            var lblSeparatorBelowCoAInformation = new kony.ui.Label({
                "height": "100%",
                "id": "lblSeparatorBelowCoAInformation",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSeparatorBelowCoAInformation.add(lblSeparatorBelowCoAInformation);
            var flxAddInfoFirstRowContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxAddInfoFirstRowContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "90%",
                "zIndex": 500
            }, {}, {});
            flxAddInfoFirstRowContainer.setDefaultUnit(kony.flex.DP);
            var flxFirstNameCoAInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxFirstNameCoAInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxFirstNameCoAInfo.setDefaultUnit(kony.flex.DP);
            var lblFirstNameCoAInfo = new kony.ui.Label({
                "id": "lblFirstNameCoAInfo",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstName\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtNameCoAInfo = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtNameCoAInfo",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "placeholder": "First Name",
                "right": "5dp",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblFirstNameErrorCoAInfo = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblFirstNameErrorCoAInfo",
                "isVisible": false,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.InformationRequired\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFirstNameCoAInfo.add(lblFirstNameCoAInfo, txtNameCoAInfo, lblFirstNameErrorCoAInfo);
            var flxMiddleNameCoAInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxMiddleNameCoAInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "280dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxMiddleNameCoAInfo.setDefaultUnit(kony.flex.DP);
            var lblMiddleNameStaticCoAInfo = new kony.ui.Label({
                "id": "lblMiddleNameStaticCoAInfo",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.MiddleName\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtMiddleNameCoAInfo = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtMiddleNameCoAInfo",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")",
                "right": "5dp",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxMiddleNameCoAInfo.add(lblMiddleNameStaticCoAInfo, txtMiddleNameCoAInfo);
            var flxLastNameCoAInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "78dp",
                "id": "flxLastNameCoAInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "560dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxLastNameCoAInfo.setDefaultUnit(kony.flex.DP);
            var lbllastNameCoAInfo = new kony.ui.Label({
                "id": "lbllastNameCoAInfo",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtLastNameCoAInfo = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtLastNameCoAInfo",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")",
                "right": "5dp",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblLastNameErrorCoAInfo = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblLastNameErrorCoAInfo",
                "isVisible": false,
                "left": "5dp",
                "right": "20dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.InformationRequired\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLastNameCoAInfo.add(lbllastNameCoAInfo, txtLastNameCoAInfo, lblLastNameErrorCoAInfo);
            flxAddInfoFirstRowContainer.add(flxFirstNameCoAInfo, flxMiddleNameCoAInfo, flxLastNameCoAInfo);
            var flxAddInfoSecondRowContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxAddInfoSecondRowContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "200dp",
                "width": "90%",
                "zIndex": 500
            }, {}, {});
            flxAddInfoSecondRowContainer.setDefaultUnit(kony.flex.DP);
            var flxPrimaryPhoneNumberCoAInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "78dp",
                "id": "flxPrimaryPhoneNumberCoAInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxPrimaryPhoneNumberCoAInfo.setDefaultUnit(kony.flex.DP);
            var lblPrimaryPhoneNumberCoAInfo = new kony.ui.Label({
                "id": "lblPrimaryPhoneNumberCoAInfo",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.PrimaryPhoneNumber\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtPrimaryPhoneNumberCoAInfo = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtPrimaryPhoneNumberCoAInfo",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Phone_Number\")",
                "right": "5dp",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblPrimaryPhoneNumberErrorCoAInfo = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblPrimaryPhoneNumberErrorCoAInfo",
                "isVisible": false,
                "left": "5dp",
                "right": "20dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.InformationRequired\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPrimaryPhoneNumberCoAInfo.add(lblPrimaryPhoneNumberCoAInfo, txtPrimaryPhoneNumberCoAInfo, lblPrimaryPhoneNumberErrorCoAInfo);
            var flxPrimaryEmailAddressCoAInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "78dp",
                "id": "flxPrimaryEmailAddressCoAInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "280dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxPrimaryEmailAddressCoAInfo.setDefaultUnit(kony.flex.DP);
            var lblPrimaryEmailAddressCoAInfo = new kony.ui.Label({
                "id": "lblPrimaryEmailAddressCoAInfo",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.PrimaryEmailId\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtEmailAddressCoAInfo = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtEmailAddressCoAInfo",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")",
                "right": "5dp",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblEmailAddressErrorCoAInfo = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblEmailAddressErrorCoAInfo",
                "isVisible": false,
                "left": "5dp",
                "right": "20dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.InformationRequired\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPrimaryEmailAddressCoAInfo.add(lblPrimaryEmailAddressCoAInfo, txtEmailAddressCoAInfo, lblEmailAddressErrorCoAInfo);
            flxAddInfoSecondRowContainer.add(flxPrimaryPhoneNumberCoAInfo, flxPrimaryEmailAddressCoAInfo);
            var flxSeparatorAboveCancelInvite = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "0.20%",
                "id": "flxSeparatorAboveCancelInvite",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "300dp",
                "width": "90%",
                "zIndex": 500
            }, {}, {});
            flxSeparatorAboveCancelInvite.setDefaultUnit(kony.flex.DP);
            var lblSepartorAboveCancelInvite = new kony.ui.Label({
                "height": "100%",
                "id": "lblSepartorAboveCancelInvite",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSeparatorAboveCancelInvite.add(lblSepartorAboveCancelInvite);
            var flxCancelAndInvite = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "70dp",
                "id": "flxCancelAndInvite",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "330dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxCancelAndInvite.setDefaultUnit(kony.flex.DP);
            var btnCancelCoAInfo = new kony.ui.Button({
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnCancelCoAInfo",
                "isVisible": true,
                "left": "0px",
                "onClick": controller.AS_Button_g4ec5c01bc144f208182de73f0413710,
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnInviteCoAInfo = new kony.ui.Button({
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40dp",
                "id": "btnInviteCoAInfo",
                "isVisible": true,
                "onClick": controller.AS_Button_h534afded1e6402e9290cdacfa8463cf,
                "right": "0dp",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "text": "INVITE",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxCancelAndInvite.add(btnCancelCoAInfo, btnInviteCoAInfo);
            flxCoAAddInformationPopup.add(flxCoAInfoContainer, flxSeparatorBelowCoAInformation, flxAddInfoFirstRowContainer, flxAddInfoSecondRowContainer, flxSeparatorAboveCancelInvite, flxCancelAndInvite);
            flxCoAInfoNDisabledBackground.add(flxCoAAddInformationPopup);
            var flxPopUpConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpConfirmation = new com.adminConsole.common.popUp({
                "clipBounds": true,
                "height": "100%",
                "id": "popUpConfirmation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "right": "180px",
                        "text": "NO, LEAVE AS IS",
                        "width": "180px"
                    },
                    "btnPopUpDelete": {
                        "text": "YES, OFFLINE",
                        "width": "150px"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Are_you_sure_to\")",
                        "maxWidth": "540px",
                        "minWidth": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpConfirmation.add(popUpConfirmation);
            var flxPopUpError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpError.setDefaultUnit(kony.flex.DP);
            var popUpError = new com.adminConsole.common.popUp({
                "clipBounds": true,
                "height": "100%",
                "id": "popUpError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "isVisible": true,
                        "right": "20px",
                        "text": "CLOSE",
                        "width": "100px"
                    },
                    "btnPopUpDelete": {
                        "isVisible": false,
                        "text": "YES, OFFLINE",
                        "width": "150px"
                    },
                    "flxPopUp": {
                        "centerY": "viz.val_cleared",
                        "top": "220px"
                    },
                    "flxPopUpButtons": {
                        "height": "80px",
                        "width": "100%",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure to set the Branch to Offline mode?<br><br>\n\nIf you set it offline, the Branch does not show operational to the customer."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpError.add(popUpError);
            var flxCSRAssistLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCSRAssistLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0px",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxCSRAssistLoading.setDefaultUnit(kony.flex.DP);
            var flxCSRLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "300px",
                "id": "flxCSRLoading",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "sknflxUploadImage0e65d5a93fe0f45",
                "width": "600px",
                "zIndex": 5
            }, {}, {});
            flxCSRLoading.setDefaultUnit(kony.flex.DP);
            var flxCSRLoadingIndicator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxCSRLoadingIndicator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxCSRLoadingIndicator.setDefaultUnit(kony.flex.DP);
            var imgCSRLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "75px",
                "id": "imgCSRLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "75px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCSRLoadingIndicator.add(imgCSRLoading);
            var lblInitializeAssist = new kony.ui.Label({
                "centerX": "51%",
                "height": "15px",
                "id": "lblInitializeAssist",
                "isVisible": true,
                "skin": "sknlbllatoRegular0bc8f2143d6204e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Initiating_assist\")",
                "top": "15px",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCSRLoading.add(flxCSRLoadingIndicator, lblInitializeAssist);
            flxCSRAssistLoading.add(flxCSRLoading);
            var CSRAssist = new com.adminConsole.customerMang.CSRAssist({
                "clipBounds": true,
                "height": "100%",
                "id": "CSRAssist",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 110,
                "overrides": {
                    "CSRAssist": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.add(flxLeftPannel, flxRightPanel, flxToastMessage, flxLoading, flxCoAInfoNDisabledBackground, flxPopUpConfirmation, flxPopUpError, flxCSRAssistLoading, CSRAssist);
        };
        return [{
            "addWidgets": addWidgetsfrmLoansDashboard,
            "enabledForIdleTimeout": true,
            "id": "frmLoansDashboard",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_df129253ad804fdfb39a916b3b67affe(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_gcc53706fc58432f9caa621d7237c388,
            "retainScrollPosition": false
        }]
    }
});