define("CustomerManagementModule/userfrmCustomerProfileEntitlementsController", {
    willUpdateUI: function(context) {
        if (context) {
            this.updateLeftMenu(context);
            if (context.LoadingScreen) {
                if (context.LoadingScreen.focus) {
                    kony.adminConsole.utils.showProgressBar(this.view);
                } else {
                    kony.adminConsole.utils.hideProgressBar(this.view);
                }
            } else if (context.toastModel) {
                if (context.toastModel.status === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SUCCESS")) {
                    this.view.toastMessage.showToastMessage(context.toastModel.message, this);
                } else {
                    this.view.toastMessage.showErrorToastMessage(context.toastModel.message, this);
                }
            } else if (context.CustomerBasicInfo) {
                this.view.flxGeneralInfoWrapper.setBasicInformation(context.CustomerBasicInfo, this);
            } else if (context.UpdateDBPUserStatus) {
                this.view.flxGeneralInfoWrapper.setLockStatus(context.UpdateDBPUserStatus.status.toUpperCase(), this);
            } else if (context.StatusGroup) {
                this.view.flxGeneralInfoWrapper.processAndFillStatusForEdit(context.StatusGroup, this);
            } else if (context.CustomerNotes) {
                this.view.Notes.displayNotes(this, context.CustomerNotes);
            } else if (context.OnlineBankingLogin) {
                this.view.CSRAssist.onlineBankingLogin(context.OnlineBankingLogin, this);
            } else if (context.CustomerEntitlements) {
                this.view.tabs.setSkinForInfoTabs(this.view.tabs.btnTabName5);
                this.unloadOnScrollEvent();
                this.resetAllSortImagesEntitlements();
                this.presenter.getAllEntitlements();
                if (context.CustomerEntitlements.target === "InfoScreen") {
                    this.setDataForEntitlementsSegment(context.CustomerEntitlements);
                } else {
                    this.setDataForEntitlementsEditScreen(context.CustomerEntitlements);
                }
            }
        }
    },
    CustomerProfileEntitlementsPreshow: function() {
        this.view.tabs.setSkinForInfoTabs(this.view.tabs.btnTabName5);
        this.view.Notes.setDefaultNotesData(this);
        var screenHeight = kony.os.deviceInfo().screenHeight;
        this.view.flxMainContent.height = screenHeight - 135 + "px";
        this.view.flxGeneralInfoWrapper.changeSelectedTabColour(this.view.flxGeneralInfoWrapper.dashboardCommonTab.btnProfile);
        this.view.flxGeneralInfoWrapper.generalInfoHeader.setDefaultHeaderData(this);
        this.view.flxGeneralInfoWrapper.setFlowActionsForGeneralInformationComponent(this);
        this.view.rtxMsgEntitlements.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.NO_RECORDS_FOUND");
        this.AdminConsoleCommonUtils.setVisibility(this.view.rtxMsgEntitlements, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.entitlementsContextualMenu.lblHeader, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.entitlementsContextualMenu.btnLink1, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.entitlementsContextualMenu.btnLink2, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.entitlementsContextualMenu.flxOptionsSeperator, false);
        this.view.entitlementsContextualMenu.lblOption1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Configure");
        this.view.entitlementsContextualMenu.imgOption1.src = "configure2x.png";
        this.view.entitlementsContextualMenu.lblOption2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Remove");
        this.view.entitlementsContextualMenu.imgOption2.src = "delete_2x.png";
        this.AdminConsoleCommonUtils.setVisibility(this.view.entitlementsContextualMenu.flxOption1, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.entitlementsContextualMenu.flxOption3, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.entitlementsContextualMenu.flxOption4, false);
        this.view.addAndRemoveOptions.lblSelectedOption.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Selected_Entitlements");
        this.AdminConsoleCommonUtils.setVisibility(this.view.entitlementsEditButtons.btnNext, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.addAndRemoveOptions.rtxAvailableOptionsMessage, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.addAndRemoveOptions.rtxSelectedOptionsMessage, false);
        this.setFlowActions();
    },
    unloadOnScrollEvent: function() {
        document.getElementById("frmCustomerProfileEntitlements_flxMainContent").onscroll = function() {};
    },
    resetAllSortImagesEntitlements: function() {
        if (this.view.fonticonSortPermission.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fonticonSortPermission.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fonticonSortPermission.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
    },
    clearSearchResultsEditEntitlement: function() {
        this.view.addAndRemoveOptions.tbxSearchBox.text = "";
        this.view.addAndRemoveOptions.flxClearSearchImage.setVisibility(false);
        var searchParameters = [{
            "searchKey": "lblName",
            "searchValue": ""
        }];
        this.search(this.view.addAndRemoveOptions.segAddOptions, searchParameters, this.view.addAndRemoveOptions.rtxAvailableOptionsMessage, this.view.flxOtherInfoWrapper, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ALL"), null);
        if (this.view.addAndRemoveOptions.rtxAvailableOptionsMessage.isVisible) {
            this.view.addAndRemoveOptions.btnSelectAll.setVisibility(false);
        } else {
            this.view.addAndRemoveOptions.btnSelectAll.setVisibility(true);
        }
    },
    setFlowActions: function() {
        var scopeObj = this;
        this.view.flxEntitlementsContextualMenu.onHover = function(widget, context) {
            if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
                scopeObj.view.flxEntitlementsContextualMenu.setVisibility(false);
                scopeObj.view.forceLayout();
                scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
            }
        };
        this.view.addAndRemoveOptions.tbxSearchBox.onKeyUp = function() {
            var searchParameters = [{
                "searchKey": "lblName",
                "searchValue": scopeObj.view.addAndRemoveOptions.tbxSearchBox.text
            }];
            scopeObj.view.addAndRemoveOptions.flxClearSearchImage.setVisibility(true);
            scopeObj.view.addAndRemoveOptions.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.No_results_found_with_parameters") + scopeObj.view.addAndRemoveOptions.tbxSearchBox.text + kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Try_with_another_keyword");
            scopeObj.search(scopeObj.view.addAndRemoveOptions.segAddOptions, searchParameters, scopeObj.view.addAndRemoveOptions.rtxAvailableOptionsMessage, scopeObj.view.flxOtherInfoWrapper, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ALL"), null);
            if (scopeObj.view.addAndRemoveOptions.rtxAvailableOptionsMessage.isVisible) {
                scopeObj.view.addAndRemoveOptions.btnSelectAll.setVisibility(false);
            } else {
                scopeObj.view.addAndRemoveOptions.btnSelectAll.setVisibility(true);
            }
        };
        this.view.addAndRemoveOptions.flxClearSearchImage.onClick = function() {
            scopeObj.clearSearchResultsEditEntitlement();
        };
        this.view.flxPermission.onClick = function() {
            scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segEntitlements, "lblPermission", scopeObj.view.fonticonSortPermission, scopeObj.view.flxOtherInfoWrapper, "ALL", null);
        };
        this.view.btnEditEntitlements.onClick = function() {
            if (scopeObj.view.flxEntitlementsContextualMenu.isVisible) {
                scopeObj.view.flxEntitlementsContextualMenu.setVisibility(false);
            }
            var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
            var target = "EditScreen";
            scopeObj.presenter.getCustomerEntitlements({
                "customerID": id
            }, target);
        };
        this.view.addAndRemoveOptions.btnSelectAll.onclick = function() {
            scopeObj.selectAllEntitlements();
        };
        this.view.addAndRemoveOptions.btnRemoveAll.onclick = function() {
            scopeObj.removeAllSelectedEntitlements();
        };
        this.view.entitlementsEditButtons.btnSave.onclick = function() {
            var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
            var listOfAddedEntitlements = scopeObj.getAllAddedEntitlements();
            var listOfRemovedEntitlements = scopeObj.getAllRemovedEntitlements();
            scopeObj.presenter.editCustomerEntitlements({
                "Customer_id": id,
                "listOfAddedPermissions": listOfAddedEntitlements,
                "listOfRemovedPermissions": listOfRemovedEntitlements
            });
        };
        this.view.backToEntitlements.flxBack.onClick = function() {
            scopeObj.view.flxEntitlementsEdit.setVisibility(false);
            scopeObj.view.flxEntitlementsListing.setVisibility(true);
        };
        this.view.backToEntitlements.btnBack.onClick = function() {
            scopeObj.view.flxEntitlementsEdit.setVisibility(false);
            scopeObj.view.flxEntitlementsListing.setVisibility(true);
        };
        this.view.entitlementsEditButtons.btnCancel.onClick = function() {
            scopeObj.view.flxEntitlementsEdit.setVisibility(false);
            scopeObj.view.flxEntitlementsListing.setVisibility(true);
        };
        this.view.entitlementsContextualMenu.flxOption2.onClick = function() {
            scopeObj.deleteEntitlement();
        };
        this.view.segEntitlements.onHover = this.saveScreenY;
    },
    saveScreenY: function(widget, context) {
        this.mouseYCoordinate = ((context.screenY + this.view.flxMainContent.contentOffsetMeasured.y) - (this.view.breadcrumbs.frame.height + this.view.mainHeader.flxMainHeader.frame.height));
    },
    showEntitlementsScreen: function() {
        this.view.flxEntitlementsWrapper.setVisibility(true);
        this.view.flxEntitlementsListing.setVisibility(true);
        this.view.flxEntitlementsConfigure.setVisibility(false);
        this.view.flxEntitlementsEdit.setVisibility(false);
        this.view.forceLayout();
        this.view.flxMainContent.scrollToWidget(this.view.flxOtherInfoWrapper);
    },
    showEntitlementsEditScreen: function() {
        this.view.flxEntitlementsEdit.setVisibility(true);
        this.view.flxEntitlementsWrapper.setVisibility(true);
        this.view.flxEntitlementsListing.setVisibility(false);
        this.view.flxEntitlementsConfigure.setVisibility(false);
        this.view.forceLayout();
        this.view.flxMainContent.scrollToWidget(this.view.flxOtherInfoWrapper);
    },
    toggleEntitlementsContextualMenu: function() {
        if (this.view.flxEntitlementsContextualMenu.isVisible) {
            this.view.flxEntitlementsContextualMenu.setVisibility(false);
        } else {
            this.view.flxEntitlementsContextualMenu.top = (this.mouseYCoordinate + 15) + "px";
            this.view.flxEntitlementsContextualMenu.right = "75px";
            this.view.flxEntitlementsContextualMenu.setVisibility(true);
        }
    },
    setDataForEntitlementsSegment: function(CustomerEntitlements) {
        // Determine the type of the customer
        var customerType = this.view.flxGeneralInfoWrapper.generalInfoHeader.flxDefaultSearchHeader.info ? (this.view.flxGeneralInfoWrapper.generalInfoHeader.flxDefaultSearchHeader.info.CustomerType_id ? this.view.flxGeneralInfoWrapper.generalInfoHeader.flxDefaultSearchHeader.info.CustomerType_id : "TYPE_ID_RETAIL") : "TYPE_ID_RETAIL";
        if (customerType === "TYPE_ID_MICRO_BUSINESS" || customerType === "TYPE_ID_SMALL_BUSINESS") {
            this.view.btnEditEntitlements.setVisibility(false);
        } else {
            this.view.btnEditEntitlements.setVisibility(true);
        }
        var indirectEntitlements = CustomerEntitlements.CustomerIndirectEntitlements.map(function(ele) {
            ele.isIndirect = true;
            ele.Service_name = ele.Service_Name;
            ele.Service_description = ele.Service_Description;
            return ele;
        });
        var assignedEntitlements = CustomerEntitlements.AssignedEntitlements;
        var data = [];
        data.push(indirectEntitlements);
        data.push(assignedEntitlements);
        var finalViewEntitlements = [];
        data.reduce(function(total, array) {
            array.forEach(function(element) {
                if (total.indexOf(element.Service_id) === -1) {
                    total.push(element.Service_id);
                    finalViewEntitlements.push(element)
                }
            });
            return total;
        }, []);
        var dataMap = {
            "flxCustomerMangEntitlements": "flxCustomerMangEntitlements",
            "flxOptions": "flxOptions",
            "flxRowWrapper": "flxRowWrapper",
            "flxSeperator": "flxSeperator",
            "imgOptions": "imgOptions",
            "lblPermission": "lblPermission",
            "lblSeperator": "lblSeperator",
            "rtxDescription": "rtxDescription"
        };
        var data = [];
        var toAdd;
        if (finalViewEntitlements.length > 0) {
            var self = this;
            for (var i = 0; i < finalViewEntitlements.length; i++) {
                toAdd = {
                    "flxCustomerMangEntitlements": "flxCustomerMangEntitlements",
                    "flxRowWrapper": "flxRowWrapper",
                    "flxSeperator": "flxSeperator",
                    "lblPermission": finalViewEntitlements[i].Service_name,
                    "lblSeperator": ".",
                    "rtxDescription": finalViewEntitlements[i].Service_description,
                    "template": "flxCustomerMangEntitlements",
                    "id": finalViewEntitlements[i].Service_id
                };
                if (finalViewEntitlements[i].isIndirect) {
                    toAdd.flxCustomerMangEntitlements = {
                        "toolTip": "Indirect entitlement, assigned from group \"" + finalViewEntitlements[i].Group_name + "\""
                    }
                } else {
                    toAdd.flxOptions = {
                        "onClick": function() {
                            self.toggleEntitlementsContextualMenu();
                        }
                    };
                    toAdd.imgOptions = {
                        "src": "dots3x.png"
                    }
                }
                data.push(toAdd);
            }
            this.view.segEntitlements.widgetDataMap = dataMap;
            this.view.segEntitlements.setData(data);
            this.view.segEntitlements.info = {
                "data": data,
                "searchAndSortData": data
            };
            this.view.rtxMsgEntitlements.setVisibility(false);
            this.view.segEntitlements.setVisibility(true);
            this.view.flxEntitlementsSegmentHeader.setVisibility(true);
            this.view.lblEntitlementsHeaderSeperator.setVisibility(true);
        } else {
            this.view.rtxMsgEntitlements.setVisibility(true);
            this.view.segEntitlements.setVisibility(false);
            this.view.flxEntitlementsSegmentHeader.setVisibility(false);
            this.view.lblEntitlementsHeaderSeperator.setVisibility(false);
        }
        this.showEntitlementsScreen();
        this.view.forceLayout();
        this.view.flxMainContent.scrollToWidget(this.view.flxOtherInfoWrapper);
    },
    deleteEntitlement: function() {
        var self = this;
        var confirmAction = function() {
            var Entitlementid = self.view.segEntitlements.selecteditems[0].id;
            var id = self.presenter.getCurrentCustomerDetails().Customer_id;
            self.presenter.editCustomerEntitlements({
                "Customer_id": id,
                "listOfAddedPermissions": [],
                "listOfRemovedPermissions": [Entitlementid]
            });
            self.view.flxEntitlementsContextualMenu.setVisibility(false);
        };
        var cancelAction = function() {
            self.view.flxEntitlementsContextualMenu.setVisibility(false);
        };
        this.AdminConsoleCommonUtils.openConfirm({
            header: 'Remove entitlement',
            message: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Remove_customer_from_Entitlement") + (self.view.segEntitlements.selecteditems[0].lblPermission) + kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Remove_customer_from_Entitlement_message"),
            confirmAction: confirmAction,
            cancelMsg: kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS"),
            cancelAction: cancelAction,
            confirmMsg: 'YES, REMOVE',
        }, this);
    },
    checkSizeAndToggleVisibilityForAddEntitlements: function() {
        if (this.view.addAndRemoveOptions.segAddOptions.data.length === 0) {
            this.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(true);
            this.view.addAndRemoveOptions.segAddOptions.setVisibility(false);
            this.view.addAndRemoveOptions.btnSelectAll.setVisibility(false);
            this.view.addAndRemoveOptions.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.No_available_records");
        } else {
            this.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(false);
            this.view.addAndRemoveOptions.segAddOptions.setVisibility(true);
            this.view.addAndRemoveOptions.btnSelectAll.setVisibility(true);
        }
    },
    checkSizeAndToggleVisibilityForRemoveEntitlements: function() {
        if (this.view.addAndRemoveOptions.segSelectedOptions.data.length === 0) {
            this.view.addAndRemoveOptions.rtxSelectedOptionsMessage.setVisibility(true);
            this.view.addAndRemoveOptions.segSelectedOptions.setVisibility(false);
            this.view.addAndRemoveOptions.btnRemoveAll.setVisibility(false);
            this.view.addAndRemoveOptions.rtxSelectedOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.No_available_records");
        } else {
            this.view.addAndRemoveOptions.rtxSelectedOptionsMessage.setVisibility(false);
            this.view.addAndRemoveOptions.segSelectedOptions.setVisibility(true);
            this.view.addAndRemoveOptions.btnRemoveAll.setVisibility(true);
        }
    },
    //Entitlement edit
    setDataForEntitlementsEditScreen: function(CustomerEntitlements) {
        var formattedIndirectEntitlements = CustomerEntitlements.CustomerIndirectEntitlements.map(this.mappingSelectedIndirectEntitlementsData);
        var formattedDirectEntitlements = CustomerEntitlements.AssignedEntitlements.map(this.mappingSelectedEntitlementsData);
        if (formattedIndirectEntitlements.length === 1 && !formattedIndirectEntitlements[0].id) {
            formattedIndirectEntitlements = [];
        }
        var data = [];
        data.push(formattedIndirectEntitlements);
        data.push(formattedDirectEntitlements);
        var finalFormattedSelectionData = [];
        data.reduce(function(total, array) {
            array.forEach(function(element) {
                if (total.indexOf(element.id) === -1) {
                    total.push(element.id);
                    finalFormattedSelectionData.push(element)
                }
            });
            return total;
        }, []);
        this.setSelectedEntitlementsSegmentData(finalFormattedSelectionData);
        var availableEntitlements = CustomerEntitlements.AllEntitlements;
        for (var i = 0; i < finalFormattedSelectionData.length; i++) {
            availableEntitlements = availableEntitlements.filter(function(el) {
                return el.id !== finalFormattedSelectionData[i].id;
            });
        }
        var formattedAvailableData = availableEntitlements.map(this.mappingAvailableEntitlementsData);
        this.setAvailableEntitlementsSegmentData(formattedAvailableData);
        this.showEntitlementsEditScreen();
    },
    setAvailableEntitlementsSegmentData: function(data) {
        var dataMap = {
            btnAdd: "btnAdd",
            flxAdd: "flxAdd",
            flxAddOption: "flxAddOption",
            lblName: "lblName",
            rtxDescription: "rtxDescription"
        };
        this.view.addAndRemoveOptions.segAddOptions.widgetDataMap = dataMap;
        this.view.addAndRemoveOptions.segAddOptions.setData(data);
        this.checkSizeAndToggleVisibilityForAddEntitlements();
        this.view.addAndRemoveOptions.segAddOptions.info = {
            "data": data
        };
        this.view.forceLayout();
    },
    setSelectedEntitlementsSegmentData: function(assignedEntitlements) {
        var dataMap = {
            flxAddOptionWrapper: "flxAddOptionWrapper",
            flxClose: "flxClose",
            flxOptionAdded: "flxOptionAdded",
            fontIconClose: "fontIconClose",
            lblOption: "lblOption"
        };
        var data = assignedEntitlements || [];
        this.view.addAndRemoveOptions.segSelectedOptions.widgetDataMap = dataMap;
        this.view.addAndRemoveOptions.segSelectedOptions.setData(data);
        this.checkSizeAndToggleVisibilityForRemoveEntitlements();
        this.view.forceLayout();
    },
    mappingSelectedIndirectEntitlementsData: function(data) {
        var self = this;
        return {
            "flxClose": {
                "onClick": function() {
                    self.removeSelectedEntitlement();
                }
            },
            "flxOptionAdded": {
                "toolTip": "Indirect entitlement, assigned from group \"" + data.Group_name + "\""
            },
            "flxAddOptionWrapper": {
                "skin": "sknflx5E7A91Br5E7A913px",
                "hoverSkin": "sknflx5E7A91Br5E7A913pxDisabled"
            },
            "lblOption": {
                "text": "" + data.Service_Name,
                "skin": "lblfffffflatoregular14px"
            },
            "hiddenDescription": "" + data.Service_Description,
            "id": data.Service_id,
            "isIndirect": true,
            "template": "flxOptionAdded"
        };
    },
    mappingSelectedEntitlementsData: function(data) {
        var self = this;
        return {
            "flxClose": {
                "onClick": function() {
                    self.removeSelectedEntitlement();
                }
            },
            "flxAddOptionWrapper": {
                "skin": "sknflxffffffop0e",
                "hoverSkin": "sknFlxSegRowHover11abeb"
            },
            "fontIconClose": "",
            "lblOption": {
                "text": "" + data.Service_name,
                "skin": "sknlbl485c7514px"
            },
            "hiddenDescription": "" + data.Service_description,
            "id": data.Service_id,
            "template": "flxOptionAdded"
        };
    },
    mappingAvailableEntitlementsData: function(data) {
        var self = this;
        return {
            "btnAdd": {
                "text": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                "onClick": function() {
                    self.addEntitlementToSegment();
                }
            },
            "flxAdd": "flxAdd",
            "flxAddOption": "flxAddOption",
            "template": "flxAddOption",
            "lblName": data.Name,
            "rtxDescription": data.Description,
            "id": data.id
        };
    },
    removeAllSelectedEntitlements: function() {
        //add data to left seg
        var self = this;
        var selectedEntitlementsData = this.view.addAndRemoveOptions.segSelectedOptions.data;
        var availableEntitlementsData = this.view.addAndRemoveOptions.segAddOptions.data;
        this.clearSearchResultsEditEntitlement();
        var formattedSelectionData = [];
        for (var i = 0; i < selectedEntitlementsData.length; i++) {
            if (selectedEntitlementsData[i].isIndirect) {
                formattedSelectionData.push(selectedEntitlementsData[i]);
                continue;
            }
            var toAddData = {
                "btnAdd": {
                    "text": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                    "onClick": function() {
                        self.addEntitlementToSegment();
                    }
                },
                "template": "flxAddOption",
                "lblName": "" + selectedEntitlementsData[i].lblOption.text,
                "rtxDescription": "" + selectedEntitlementsData[i].hiddenDescription,
                "id": selectedEntitlementsData[i].id
            };
            availableEntitlementsData.push(toAddData);
        }
        this.view.addAndRemoveOptions.segAddOptions.setData(availableEntitlementsData);
        this.checkSizeAndToggleVisibilityForAddEntitlements();
        this.view.addAndRemoveOptions.segAddOptions.info = {
            "data": availableEntitlementsData
        };
        //remove data from right
        this.view.addAndRemoveOptions.segSelectedOptions.setData(formattedSelectionData);
        this.checkSizeAndToggleVisibilityForRemoveEntitlements();
    },
    removeSelectedEntitlement: function() {
        var rowIndex = this.view.addAndRemoveOptions.segSelectedOptions.selectedIndex[1];
        //add row to right
        var self = this;
        this.clearSearchResultsEditEntitlement();
        var selectedEntitlementsData = this.view.addAndRemoveOptions.segSelectedOptions.data;
        var availableEntitlementsData = this.view.addAndRemoveOptions.segAddOptions.data;
        var toAddData = {
            "btnAdd": {
                "text": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                "onClick": function() {
                    self.addEntitlementToSegment();
                }
            },
            "template": "flxAddOption",
            "lblName": "" + selectedEntitlementsData[rowIndex].lblOption.text,
            "rtxDescription": "" + selectedEntitlementsData[rowIndex].hiddenDescription,
            "id": selectedEntitlementsData[rowIndex].id
        };
        availableEntitlementsData.push(toAddData);
        this.view.addAndRemoveOptions.segAddOptions.setData(availableEntitlementsData);
        this.checkSizeAndToggleVisibilityForAddEntitlements();
        this.view.addAndRemoveOptions.segAddOptions.info = {
            "data": availableEntitlementsData
        };
        //check if right segment is empty and display rtx
        this.view.addAndRemoveOptions.segSelectedOptions.removeAt(rowIndex);
        this.checkSizeAndToggleVisibilityForRemoveEntitlements();
        this.view.forceLayout();
    },
    addEntitlementToSegment: function() {
        var self = this;
        var selectedItemData = this.view.addAndRemoveOptions.segAddOptions.selectedItems[0];
        var toAdd = {
            "flxClose": {
                "onClick": function() {
                    self.removeSelectedEntitlement();
                }
            },
            "flxAddOptionWrapper": {
                "skin": "sknflxffffffop0e",
                "hoverSkin": "sknFlxSegRowHover11abeb"
            },
            "fontIconClose": "",
            "lblOption": {
                "text": "" + selectedItemData.lblName,
                "skin": "sknlbl485c7514px"
            },
            "hiddenDescription": "" + selectedItemData.rtxDescription,
            "template": "flxOptionAdded",
            "id": selectedItemData.id
        };
        var data = this.view.addAndRemoveOptions.segSelectedOptions.data;
        data.push(toAdd);
        var rowIndex = this.view.addAndRemoveOptions.segAddOptions.selectedIndex[1];
        var id = self.view.addAndRemoveOptions.segAddOptions.data[rowIndex].id;
        this.view.addAndRemoveOptions.segAddOptions.removeAt(rowIndex);
        this.checkSizeAndToggleVisibilityForAddEntitlements();
        this.view.addAndRemoveOptions.segAddOptions.info.data = this.view.addAndRemoveOptions.segAddOptions.info.data.filter(function(el) {
            return el.id !== id;
        });
        this.view.addAndRemoveOptions.segSelectedOptions.setData(data);
        this.checkSizeAndToggleVisibilityForRemoveEntitlements();
        this.view.forceLayout();
    },
    selectAllEntitlements: function() {
        var self = this;
        var entitlementsToAddData = this.view.addAndRemoveOptions.segAddOptions.info.searchAndSortData || this.view.addAndRemoveOptions.segAddOptions.info.data;
        var data = this.view.addAndRemoveOptions.segSelectedOptions.data;
        for (var i = 0; i < entitlementsToAddData.length; i++) {
            var toAdd = {
                "flxClose": {
                    "onClick": function() {
                        self.removeSelectedEntitlement();
                    }
                },
                "flxAddOptionWrapper": {
                    "skin": "sknflxffffffop0e",
                    "hoverSkin": "sknFlxSegRowHover11abeb"
                },
                "fontIconClose": "",
                "lblOption": {
                    "text": "" + entitlementsToAddData[i].lblName,
                    "skin": "sknlbl485c7514px"
                },
                "hiddenDescription": "" + entitlementsToAddData[i].rtxDescription,
                "template": "flxOptionAdded",
                "id": entitlementsToAddData[i].id
            };
            data.push(toAdd);
        }
        var newData = this.view.addAndRemoveOptions.segAddOptions.info.data;
        for (var i = 0; i < entitlementsToAddData.length; i++) {
            newData = newData.filter(function(x) {
                return x.id !== entitlementsToAddData[i].id;
            });
        }
        this.view.addAndRemoveOptions.segAddOptions.setData(newData);
        this.checkSizeAndToggleVisibilityForAddEntitlements();
        this.view.addAndRemoveOptions.segAddOptions.info = {
            "data": newData
        };
        this.view.addAndRemoveOptions.segSelectedOptions.setData(data);
        this.checkSizeAndToggleVisibilityForRemoveEntitlements();
        this.clearSearchResultsEditEntitlement();
        this.view.forceLayout();
    },
    getAllAddedEntitlements: function() {
        var CustomerEntitlements = this.presenter.getCurrentCustomerEntitlements();
        var originalEntitlementIds = CustomerEntitlements.AssignedEntitlements.map(function(g) {
            return g.Service_id;
        });
        var Entitlement_ids = this.view.addAndRemoveOptions.segSelectedOptions.data.filter(function(e) {
            return !e.isIndirect
        }).map(function(g) {
            return g.id;
        });
        var addedList = this.updatedEntitlementCollection(Entitlement_ids, originalEntitlementIds);
        var responseList = [];
        for (var i = 0; i < addedList.length; i++) {
            responseList.push({
                "Service_id": addedList[i]
            });
        }
        return responseList;
    },
    getAllRemovedEntitlements: function() {
        var CustomerEntitlements = this.presenter.getCurrentCustomerEntitlements();
        var originalEntitlementIds = CustomerEntitlements.AssignedEntitlements.map(function(g) {
            return g.Service_id;
        });
        var Entitlement_ids = this.view.addAndRemoveOptions.segSelectedOptions.data.map(function(g) {
            return g.id;
        });
        return this.updatedEntitlementCollection(originalEntitlementIds, Entitlement_ids);
    },
    updatedEntitlementCollection: function(a1, a2) {
        return a1.filter(function(x) {
            var result = false;
            if (a2.indexOf(x) < 0) result = true;
            return result;
        });
    },
});
define("CustomerManagementModule/frmCustomerProfileEntitlementsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmCustomerProfileEntitlements **/
    AS_Form_f0882d29980c4ee19b28ebde8ad1bff0: function AS_Form_f0882d29980c4ee19b28ebde8ad1bff0(eventobject) {
        var self = this;
        this.CustomerProfileEntitlementsPreshow();
    },
    /** onDeviceBack defined for frmCustomerProfileEntitlements **/
    AS_Form_de71c6ffbd01429bb8fe500873f3f25e: function AS_Form_de71c6ffbd01429bb8fe500873f3f25e(eventobject) {
        var self = this;
    }
});
define("CustomerManagementModule/frmCustomerProfileEntitlementsController", ["CustomerManagementModule/userfrmCustomerProfileEntitlementsController", "CustomerManagementModule/frmCustomerProfileEntitlementsControllerActions"], function() {
    var controller = require("CustomerManagementModule/userfrmCustomerProfileEntitlementsController");
    var controllerActions = ["CustomerManagementModule/frmCustomerProfileEntitlementsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
