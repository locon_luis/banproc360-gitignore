define("CustomerManagementModule/frmCustomerManagement", function() {
    return function(controller) {
        function addWidgetsfrmCustomerManagement() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "flxButtons": {
                        "top": "58px"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.customer_Management\")"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var btnNotes = new kony.ui.Button({
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "30px",
                "id": "btnNotes",
                "isVisible": true,
                "right": "35px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnNotes\")",
                "top": "58dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            var btnCreateCustomer = new kony.ui.Button({
                "centerY": "80px",
                "height": "30px",
                "id": "btnCreateCustomer",
                "isVisible": true,
                "right": "3%",
                "skin": "sknCreateCustomer",
                "text": "CREATE CUSTOMER",
                "top": "58px",
                "width": "170px",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnCustomerSearchHover"
            });
            flxMainHeader.add(mainHeader, btnNotes, btnCreateCustomer);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxMainContent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "100px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var CustomerSearchandResults = new com.adminConsole.CustomerManagement.CustomerSearchandResults({
                "clipBounds": true,
                "height": "800px",
                "id": "CustomerSearchandResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxCustomerSearchComponent",
                "top": "0px",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblSearchParam4": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Email\")"
                    },
                    "segCustomerResults": {
                        "data": [{
                            "fontIconCircle1": "",
                            "fontIconCircle2": "",
                            "fontIconCircle3": "",
                            "fontIconCircle4": "",
                            "fontIconCircle5": "",
                            "lblContent1": "Retail Customer",
                            "lblContent2": "Applicant",
                            "lblContent3": "Small Business User",
                            "lblContent4": "Micro Business User",
                            "lblContent5": "Applicant",
                            "lblCustomerName": "Label",
                            "lblData1": "TRN232422",
                            "lblData2": "*****2212",
                            "lblData3": "02/12/1995",
                            "lblData4": "Active",
                            "lblEmail": "bryan.nash@gmail.com",
                            "lblHeading1": "CUSTOMER ID",
                            "lblHeading2": "SSN",
                            "lblHeading3": "DOB",
                            "lblHeading4": "STATUS",
                            "lblPhone": "+180632434323",
                            "lblRowSeparator": "Label",
                            "lblSearchSeparator": "Label",
                            "lblSearchSeparator2": "Label",
                            "lblUsername": "+180632434323"
                        }, {
                            "fontIconCircle1": "",
                            "fontIconCircle2": "",
                            "fontIconCircle3": "",
                            "fontIconCircle4": "",
                            "fontIconCircle5": "",
                            "lblContent1": "Retail Customer",
                            "lblContent2": "Applicant",
                            "lblContent3": "Small Business User",
                            "lblContent4": "Micro Business User",
                            "lblContent5": "Applicant",
                            "lblCustomerName": "Label",
                            "lblData1": "TRN232422",
                            "lblData2": "*****2212",
                            "lblData3": "02/12/1995",
                            "lblData4": "Active",
                            "lblEmail": "bryan.nash@gmail.com",
                            "lblHeading1": "CUSTOMER ID",
                            "lblHeading2": "SSN",
                            "lblHeading3": "DOB",
                            "lblHeading4": "STATUS",
                            "lblPhone": "+180632434323",
                            "lblRowSeparator": "Label",
                            "lblSearchSeparator": "Label",
                            "lblSearchSeparator2": "Label",
                            "lblUsername": "+180632434323"
                        }, {
                            "fontIconCircle1": "",
                            "fontIconCircle2": "",
                            "fontIconCircle3": "",
                            "fontIconCircle4": "",
                            "fontIconCircle5": "",
                            "lblContent1": "Retail Customer",
                            "lblContent2": "Applicant",
                            "lblContent3": "Small Business User",
                            "lblContent4": "Micro Business User",
                            "lblContent5": "Applicant",
                            "lblCustomerName": "Label",
                            "lblData1": "TRN232422",
                            "lblData2": "*****2212",
                            "lblData3": "02/12/1995",
                            "lblData4": "Active",
                            "lblEmail": "bryan.nash@gmail.com",
                            "lblHeading1": "CUSTOMER ID",
                            "lblHeading2": "SSN",
                            "lblHeading3": "DOB",
                            "lblHeading4": "STATUS",
                            "lblPhone": "+180632434323",
                            "lblRowSeparator": "Label",
                            "lblSearchSeparator": "Label",
                            "lblSearchSeparator2": "Label",
                            "lblUsername": "+180632434323"
                        }, {
                            "fontIconCircle1": "",
                            "fontIconCircle2": "",
                            "fontIconCircle3": "",
                            "fontIconCircle4": "",
                            "fontIconCircle5": "",
                            "lblContent1": "Retail Customer",
                            "lblContent2": "Applicant",
                            "lblContent3": "Small Business User",
                            "lblContent4": "Micro Business User",
                            "lblContent5": "Applicant",
                            "lblCustomerName": "Label",
                            "lblData1": "TRN232422",
                            "lblData2": "*****2212",
                            "lblData3": "02/12/1995",
                            "lblData4": "Active",
                            "lblEmail": "bryan.nash@gmail.com",
                            "lblHeading1": "CUSTOMER ID",
                            "lblHeading2": "SSN",
                            "lblHeading3": "DOB",
                            "lblHeading4": "STATUS",
                            "lblPhone": "+180632434323",
                            "lblRowSeparator": "Label",
                            "lblSearchSeparator": "Label",
                            "lblSearchSeparator2": "Label",
                            "lblUsername": "+180632434323"
                        }, {
                            "fontIconCircle1": "",
                            "fontIconCircle2": "",
                            "fontIconCircle3": "",
                            "fontIconCircle4": "",
                            "fontIconCircle5": "",
                            "lblContent1": "Retail Customer",
                            "lblContent2": "Applicant",
                            "lblContent3": "Small Business User",
                            "lblContent4": "Micro Business User",
                            "lblContent5": "Applicant",
                            "lblCustomerName": "Label",
                            "lblData1": "TRN232422",
                            "lblData2": "*****2212",
                            "lblData3": "02/12/1995",
                            "lblData4": "Active",
                            "lblEmail": "bryan.nash@gmail.com",
                            "lblHeading1": "CUSTOMER ID",
                            "lblHeading2": "SSN",
                            "lblHeading3": "DOB",
                            "lblHeading4": "STATUS",
                            "lblPhone": "+180632434323",
                            "lblRowSeparator": "Label",
                            "lblSearchSeparator": "Label",
                            "lblSearchSeparator2": "Label",
                            "lblUsername": "+180632434323"
                        }, {
                            "fontIconCircle1": "",
                            "fontIconCircle2": "",
                            "fontIconCircle3": "",
                            "fontIconCircle4": "",
                            "fontIconCircle5": "",
                            "lblContent1": "Retail Customer",
                            "lblContent2": "Applicant",
                            "lblContent3": "Small Business User",
                            "lblContent4": "Micro Business User",
                            "lblContent5": "Applicant",
                            "lblCustomerName": "Label",
                            "lblData1": "TRN232422",
                            "lblData2": "*****2212",
                            "lblData3": "02/12/1995",
                            "lblData4": "Active",
                            "lblEmail": "bryan.nash@gmail.com",
                            "lblHeading1": "CUSTOMER ID",
                            "lblHeading2": "SSN",
                            "lblHeading3": "DOB",
                            "lblHeading4": "STATUS",
                            "lblPhone": "+180632434323",
                            "lblRowSeparator": "Label",
                            "lblSearchSeparator": "Label",
                            "lblSearchSeparator2": "Label",
                            "lblUsername": "+180632434323"
                        }, {
                            "fontIconCircle1": "",
                            "fontIconCircle2": "",
                            "fontIconCircle3": "",
                            "fontIconCircle4": "",
                            "fontIconCircle5": "",
                            "lblContent1": "Retail Customer",
                            "lblContent2": "Applicant",
                            "lblContent3": "Small Business User",
                            "lblContent4": "Micro Business User",
                            "lblContent5": "Applicant",
                            "lblCustomerName": "Label",
                            "lblData1": "TRN232422",
                            "lblData2": "*****2212",
                            "lblData3": "02/12/1995",
                            "lblData4": "Active",
                            "lblEmail": "bryan.nash@gmail.com",
                            "lblHeading1": "CUSTOMER ID",
                            "lblHeading2": "SSN",
                            "lblHeading3": "DOB",
                            "lblHeading4": "STATUS",
                            "lblPhone": "+180632434323",
                            "lblRowSeparator": "Label",
                            "lblSearchSeparator": "Label",
                            "lblSearchSeparator2": "Label",
                            "lblUsername": "+180632434323"
                        }, {
                            "fontIconCircle1": "",
                            "fontIconCircle2": "",
                            "fontIconCircle3": "",
                            "fontIconCircle4": "",
                            "fontIconCircle5": "",
                            "lblContent1": "Retail Customer",
                            "lblContent2": "Applicant",
                            "lblContent3": "Small Business User",
                            "lblContent4": "Micro Business User",
                            "lblContent5": "Applicant",
                            "lblCustomerName": "Label",
                            "lblData1": "TRN232422",
                            "lblData2": "*****2212",
                            "lblData3": "02/12/1995",
                            "lblData4": "Active",
                            "lblEmail": "bryan.nash@gmail.com",
                            "lblHeading1": "CUSTOMER ID",
                            "lblHeading2": "SSN",
                            "lblHeading3": "DOB",
                            "lblHeading4": "STATUS",
                            "lblPhone": "+180632434323",
                            "lblRowSeparator": "Label",
                            "lblSearchSeparator": "Label",
                            "lblSearchSeparator2": "Label",
                            "lblUsername": "+180632434323"
                        }, {
                            "fontIconCircle1": "",
                            "fontIconCircle2": "",
                            "fontIconCircle3": "",
                            "fontIconCircle4": "",
                            "fontIconCircle5": "",
                            "lblContent1": "Retail Customer",
                            "lblContent2": "Applicant",
                            "lblContent3": "Small Business User",
                            "lblContent4": "Micro Business User",
                            "lblContent5": "Applicant",
                            "lblCustomerName": "Label",
                            "lblData1": "TRN232422",
                            "lblData2": "*****2212",
                            "lblData3": "02/12/1995",
                            "lblData4": "Active",
                            "lblEmail": "bryan.nash@gmail.com",
                            "lblHeading1": "CUSTOMER ID",
                            "lblHeading2": "SSN",
                            "lblHeading3": "DOB",
                            "lblHeading4": "STATUS",
                            "lblPhone": "+180632434323",
                            "lblRowSeparator": "Label",
                            "lblSearchSeparator": "Label",
                            "lblSearchSeparator2": "Label",
                            "lblUsername": "+180632434323"
                        }, {
                            "fontIconCircle1": "",
                            "fontIconCircle2": "",
                            "fontIconCircle3": "",
                            "fontIconCircle4": "",
                            "fontIconCircle5": "",
                            "lblContent1": "Retail Customer",
                            "lblContent2": "Applicant",
                            "lblContent3": "Small Business User",
                            "lblContent4": "Micro Business User",
                            "lblContent5": "Applicant",
                            "lblCustomerName": "Label",
                            "lblData1": "TRN232422",
                            "lblData2": "*****2212",
                            "lblData3": "02/12/1995",
                            "lblData4": "Active",
                            "lblEmail": "bryan.nash@gmail.com",
                            "lblHeading1": "CUSTOMER ID",
                            "lblHeading2": "SSN",
                            "lblHeading3": "DOB",
                            "lblHeading4": "STATUS",
                            "lblPhone": "+180632434323",
                            "lblRowSeparator": "Label",
                            "lblSearchSeparator": "Label",
                            "lblSearchSeparator2": "Label",
                            "lblUsername": "+180632434323"
                        }]
                    },
                    "txtSearchParam4": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Email\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxCompanyDropdown = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxCompanyDropdown",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "6%",
                "maxHeight": "130px",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxffffffbordere1e5ed",
                "top": "255px",
                "verticalScrollIndicator": true,
                "width": "260px",
                "zIndex": 200
            }, {}, {
                "hoverSkin": "sknFlxHandCursor"
            });
            flxCompanyDropdown.setDefaultUnit(kony.flex.DP);
            var segCompanyDropdown = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10px",
                "data": [{
                    "lblViewFullName": "Edward"
                }, {
                    "lblViewFullName": "Edward"
                }, {
                    "lblViewFullName": "Edward"
                }],
                "groupCells": false,
                "id": "segCompanyDropdown",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "10px",
                "rowFocusSkin": "seg2Focus",
                "rowTemplate": "flxAssignUsers",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
                "selectionBehaviorConfig": {
                    "imageIdentifier": "imgCheckBox",
                    "selectedStateImage": "checkboxselected.png",
                    "unselectedStateImage": "checkbox.png"
                },
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "10dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAssignUsers": "flxAssignUsers",
                    "lblViewFullName": "lblViewFullName"
                },
                "zIndex": 500
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var richtextNoResult = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "richtextNoResult",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknRtxLatoReg84939e13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.prop773\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCompanyDropdown.add(segCompanyDropdown, richtextNoResult);
            flxMainContent.add(CustomerSearchandResults, flxCompanyDropdown);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0px",
                "width": "100%",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxToastMessageWithLink = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-70px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessageWithLink",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxToastMessageWithLink.setDefaultUnit(kony.flex.DP);
            var toastMessageWithLink = new com.adminConsole.common.toastMessageWithLink({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessageWithLink",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lblToastMessageLeft": {
                        "text": "20/100 branches uploaded to the system."
                    },
                    "lblToastMessageRight": {
                        "text": "to download the details."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessageWithLink.add(toastMessageWithLink);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxMainContent, flxLoading, flxToastMessageWithLink);
            var flxImportCustomers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxImportCustomers",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxImportCustomers.setDefaultUnit(kony.flex.DP);
            var flxUploadingIndiacator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "300px",
                "id": "flxUploadingIndiacator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxUploadImage0e65d5a93fe0f45",
                "width": "600px",
                "zIndex": 5
            }, {}, {});
            flxUploadingIndiacator.setDefaultUnit(kony.flex.DP);
            var lblUploading = new kony.ui.Label({
                "centerX": "50%",
                "height": "15px",
                "id": "lblUploading",
                "isVisible": true,
                "skin": "sknlbllatoRegular0bc8f2143d6204e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Uploading\")",
                "top": "118px",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUploadingStatusBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxUploadingStatusBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflx0c0989fa7ba8d45",
                "top": "145px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxUploadingStatusBar.setDefaultUnit(kony.flex.DP);
            flxUploadingStatusBar.add();
            var btnCancel = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnCancel",
                "isVisible": true,
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.CANCEL\")",
                "top": "180px",
                "width": "110px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            flxUploadingIndiacator.add(lblUploading, flxUploadingStatusBar, btnCancel);
            flxImportCustomers.add(flxUploadingIndiacator);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "toastMessage": {
                        "zIndex": 20
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            flxMain.add(flxLeftPannel, flxRightPanel, flxImportCustomers, flxToastMessage);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerManagement,
            "enabledForIdleTimeout": true,
            "id": "frmCustomerManagement",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_e7b729885a2745f1963c07857ae3c8a0,
            "preShow": function(eventobject) {
                controller.AS_Form_i50d06f7c58c43cfb8a147cf675ad286(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_de5e80b2c3f9491c8e8b7b5718c9add6,
            "retainScrollPosition": false
        }]
    }
});