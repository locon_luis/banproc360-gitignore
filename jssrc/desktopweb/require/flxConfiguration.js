define("flxConfiguration", function() {
    return function(controller) {
        var flxConfiguration = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "height": "50px",
            "id": "flxConfiguration",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknflxffffffop100"
        });
        flxConfiguration.setDefaultUnit(kony.flex.DP);
        var flxConfigurationArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxConfigurationArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "5%",
            "zIndex": 1
        }, {}, {});
        flxConfigurationArrow.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "15px",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "15px",
            "zIndex": 1
        }, {}, {});
        flxArrow.setDefaultUnit(kony.flex.DP);
        var fonticonArrow = new kony.ui.Label({
            "height": "15px",
            "id": "fonticonArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconDescRightArrow14px",
            "text": "",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(fonticonArrow);
        flxConfigurationArrow.add(flxArrow);
        var flxConfigurationKey = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxConfigurationKey",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "18%",
            "zIndex": 1
        }, {}, {});
        flxConfigurationKey.setDefaultUnit(kony.flex.DP);
        var lblConfigurationKey = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblConfigurationKey",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Configuration Key",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxConfigurationKey.add(lblConfigurationKey);
        var flxConfigurationValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxConfigurationValue",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "23%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "27%",
            "zIndex": 1
        }, {}, {});
        flxConfigurationValue.setDefaultUnit(kony.flex.DP);
        var lblConfigurationValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblConfigurationValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Configuration Value",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxConfigurationValue.add(lblConfigurationValue);
        var flxConfigurationType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxConfigurationType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxConfigurationType.setDefaultUnit(kony.flex.DP);
        var lblConfigurationType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblConfigurationType",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "PREFERENCE",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxConfigurationType.add(lblConfigurationType);
        var flxConfigurationTarget = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxConfigurationTarget",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "70%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "24%",
            "zIndex": 1
        }, {}, {});
        flxConfigurationTarget.setDefaultUnit(kony.flex.DP);
        var lblConfigurationTarget = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblConfigurationTarget",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "CLIENT",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxConfigurationTarget.add(lblConfigurationTarget);
        var flxConfigurationContextualMenu = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxConfigurationContextualMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "94%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "6%",
            "zIndex": 1
        }, {}, {});
        flxConfigurationContextualMenu.setDefaultUnit(kony.flex.DP);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30px",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e46c0024c34f42c5a0ea750a84ed1be9,
            "skin": "slFbox",
            "width": "30px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "lblIconOptions",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconOptions);
        flxConfigurationContextualMenu.add(flxOptions);
        var lblConfigSeparator = new kony.ui.Label({
            "bottom": "1px",
            "centerX": "50%",
            "height": "1px",
            "id": "lblConfigSeparator",
            "isVisible": true,
            "skin": "sknConfigSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxConfiguration.add(flxConfigurationArrow, flxConfigurationKey, flxConfigurationValue, flxConfigurationType, flxConfigurationTarget, flxConfigurationContextualMenu, lblConfigSeparator);
        return flxConfiguration;
    }
})