define("flxAlertGroupManagement", function() {
    return function(controller) {
        var flxAlertGroupManagement = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertGroupManagement",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxAlertGroupManagement.setDefaultUnit(kony.flex.DP);
        var flxSegHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "focusSkin": "sknfbfcfc",
            "id": "flxSegHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxSegHeader.setDefaultUnit(kony.flex.DP);
        var lblAlertName = new kony.ui.Label({
            "id": "lblAlertName",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "NAME",
            "top": "15dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAlertCode = new kony.ui.Label({
            "id": "lblAlertCode",
            "isVisible": true,
            "left": "22%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "CODE",
            "top": "15dp",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAlertType = new kony.ui.Label({
            "id": "lblAlertType",
            "isVisible": true,
            "left": "50%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "TYPE",
            "top": "15dp",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxAlertStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxAlertStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "71%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "13dp",
            "width": "8%",
            "zIndex": 1
        }, {}, {});
        flxAlertStatus.setDefaultUnit(kony.flex.DP);
        var imgAlertStatus = new kony.ui.Image2({
            "centerY": "50%",
            "height": "10dp",
            "id": "imgAlertStatus",
            "isVisible": false,
            "left": "0dp",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "width": "10dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAlertStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAlertStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAlertIconStatus = new kony.ui.Label({
            "centerY": "52%",
            "height": "15dp",
            "id": "lblAlertIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon13pxGreen",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAlertStatus.add(imgAlertStatus, lblAlertStatus, lblAlertIconStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "27dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "94%",
            "isModalContainer": false,
            "skin": "sknFlxBorffffff1pxRound",
            "top": "10dp",
            "width": "27px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var imgOptions = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "imgOptions",
            "isVisible": false,
            "left": 14,
            "skin": "slImage",
            "src": "dots3x.png",
            "width": "6dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOptions = new kony.ui.Label({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblOptions",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(imgOptions, lblOptions);
        flxSegHeader.add(lblAlertName, lblAlertCode, lblAlertType, flxAlertStatus, flxOptions);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAlertGroupManagement.add(flxSegHeader, lblSeperator);
        return flxAlertGroupManagement;
    }
})