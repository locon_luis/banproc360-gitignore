define(function() {
    var checkboxSelected = "checkboxselected.png";
    var checkbox = "checkbox.png";
    var scrollHeight = 0;

    function optimizeCalendarField(widgetID) {
        var text = document.getElementById(widgetID);
        text.onkeypress = text.onpaste = checkInput;
    }

    function checkInput(e) {
        var calendarExpression = /[^\d|/]/gi;
        e = e || event;
        var char = e.type === 'keypress' ? String.fromCharCode(e.keyCode || e.which) : (e.clipboardData || window.clipboardData).getData('Text');
        if (calendarExpression.test(char)) {
            return false;
        }
    }
    var optimizeNumberField = function(widgetID) {
        var text = document.getElementById(widgetID);
        text.onkeypress = text.onpaste = checkNumberInput;
    };

    function allowOnlyAlphaNumericValues(e) {
        var alphaNumeric = /[^\d|a-z|A-Z]/i;
        e = e || event;
        var char = e.type === 'keypress' ? String.fromCharCode(e.keyCode || e.which) : (e.clipboardData || window.clipboardData).getData('Text');
        if (alphaNumeric.test(char)) {
            return false;
        }
    }
    var restrictTextFieldToAlphaNumeric = function(widgetID) {
        var text = document.getElementById(widgetID);
        text.onkeypress = text.onpaste = allowOnlyAlphaNumericValues;
    }

    function allowOnlyNumericValues(e) {
        var alphaNumeric = /[^\d]/i;
        e = e || event;
        var char = e.type === 'keypress' ? String.fromCharCode(e.keyCode || e.which) : (e.clipboardData || window.clipboardData).getData('Text');
        if (alphaNumeric.test(char)) {
            return false;
        }
    }
    var restrictTextFieldToNumeric = function(widgetID) {
        var text = document.getElementById(widgetID);
        text.onkeypress = text.onpaste = allowOnlyNumericValues;
    }

    function allowOnlyPhoneNumber(e) {
        var phoneNumber = /^[+|\-|0-9]/;
        e = e || event;
        var char = e.type === 'keypress' ? String.fromCharCode(e.keyCode || e.which) : (e.clipboardData || window.clipboardData).getData('Text');
        return phoneNumber.test(char);
    }
    var restrictTextFieldToPhoneNumber = function(widgetID) {
        var text = document.getElementById(widgetID);
        text.onkeypress = text.onpaste = allowOnlyPhoneNumber;
    }
    var checkNumberInput = function(e) {
        var arr = "1234567890";
        var code;
        if (window.event) code = e.keyCode;
        else code = e.which;
        var char = keychar = String.fromCharCode(code);
        if (arr.indexOf(char) == -1) return false;
    };
    var deepEquals = function(obj1, obj2) {
        if (!obj1 || !obj2) {
            return false;
        }
        if (typeof obj1 !== typeof obj2) {
            return false;
        }
        if (typeof obj1 === 'object') {
            if (Object.keys(obj1).length !== Object.keys(obj2).length) {
                return false;
            }
            return Object.keys(obj1).every(function(key) {
                return deepEquals(obj1[key], obj2[key]);
            });
        } else {
            return obj1 === obj2;
        }
    };
    var rowLeftSwipeAnimation = function(formRef, segRef, rowObj) {
        var transfromObject = kony.ui.makeAffineTransform();
        transfromObject.translate(-1000, 0);
        var animationDef = {
            50: {
                "transform": transfromObject
            },
            100: {
                "height": "0dp",
                "stepConfig": {
                    "timingFunction": kony.anim.Linear
                }
            }
        };
        var animationConfig = {
            delay: 0.01,
            duration: 2.5,
            fillMode: kony.anim.FILL_MODE_FORWARDS,
            iterationCount: 1,
        };
        var rowObject = {
            sectionIndex: rowObj.sectionIndex, // section index of row to be swiped left
            rowIndex: rowObj.rowIndex // row index of row to be swiped left
        };
        var animationDefObject = kony.ui.createAnimation(animationDef);
        var CallBack = function(res) {
            console.log(res);
            return null;
        };
        var endCallBack = function(res) {
            console.log(res);
            var removeRow = function() {
                segRef.removeAt(rowObj.rowIndex, rowObj.sectionIndex);
            };
            window.setTimeout(removeRow, 2500);
        };
        segRef.animateRows({
            rows: [rowObject],
            animation: {
                definition: animationDefObject,
                config: animationConfig,
                animationStart: CallBack("start"),
                animationEnd: endCallBack("end")
            }
        });
        return null;
    };

    function isValidAmount(numStr) {
        try {
            if (numStr || numStr == "0") {
                numStr = +numStr;
                if (!isNaN(numStr)) {
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        } catch (e) {
            return false;
        }
    }
    var getTruncatedString = function(text, maxLimit, truncateSize) {
        var self = this;
        var truncatedStr = "";
        if (text) {
            truncatedStr = text.length > maxLimit ? (text.substr(0, truncateSize) + "...") : (text);
        }
        return truncatedStr;
    };
    var getParamValueOrNA = function(param) {
        return param ? param : "N/A";
    };
    var getParamValueOrEmptyString = function(param) {
        return param ? param : "";
    };

    function togglePreferredCheckbox(widget) {
        widget.src = (widget.src === checkbox) ? checkboxSelected : checkbox;
    }

    function maskSSN(ssn) {
        return "XX-XXX-" + ssn.slice(-4);
    }

    function getValidString(value) {
        return value ? value : "N/A";
    }

    function showError(Widget) {
        var ErrorSkinTbx = "skntbxBordereb30173px"; // Error skin for text box
        var ErrorSkinLst = "sknlbxeb30173px"; // Error skin for list box
        if (Widget.wType === "TextField") Widget.skin = ErrorSkinTbx;
        else Widget.skin = ErrorSkinLst;
    }

    function showNoError(Widget) {
        var NormalSkinTbx = "skntxtbxDetails0bbf1235271384a"; // Normal skin for text box
        var NormalSkinLst = "sknlstbxNormal0f9abd8e88aa64a"; // Normal skin for list box
        if (Widget.wType === "TextField") Widget.skin = NormalSkinTbx;
        else Widget.skin = NormalSkinLst;
    }

    function openEmailConfirm(popupModel, formInstance) {
        //Opens an email popup 
        formInstance.view.lblPopupHeader.text = popupModel.header;
        formInstance.view.lblPopUpMainMessage.text = popupModel.message;
        formInstance.view.btnPopUpDelete.text = popupModel.confirmMsg;
        formInstance.view.btnPopUpCancel.text = popupModel.cancelMsg;
        formInstance.view.btnPopUpDelete.onClick = function() {
            popupModel.confirmAction();
            formInstance.view.flxPopupSelectEnrolEmail.setVisibility(false);
        };
        formInstance.view.flxPopUpClose.onClick = function() {
            popupModel.cancelAction();
            formInstance.view.flxPopupSelectEnrolEmail.setVisibility(false);
        };
        formInstance.view.btnPopUpCancel.onClick = function() {
            formInstance.view.flxPopupSelectEnrolEmail.setVisibility(false);
        };
        formInstance.view.flxPopupSelectEnrolEmail.setVisibility(true);
    }

    function openErrorPopup(popupModel, formInstance) {
        //Opens an error popup 
        formInstance.view.popUpError.lblPopUpMainMessage.text = popupModel.header;
        formInstance.view.popUpError.rtxPopUpDisclaimer.text = popupModel.message;
        formInstance.view.popUpError.btnPopUpCancel.text = popupModel.closeMsg;
        formInstance.view.popUpError.btnPopUpCancel.onClick = function() {
            popupModel.closeAction();
            formInstance.view.flxPopUpError.setVisibility(false);
        };
        formInstance.view.popUpError.flxPopUpClose.onClick = function() {
            formInstance.view.flxPopUpError.setVisibility(false);
        };
        formInstance.view.flxPopUpError.setVisibility(true);
        formInstance.view.forceLayout();
    }

    function openConfirm(popupModel, formInstance) {
        //Opens a confirm popup 
        formInstance.view.popUpConfirmation.lblPopUpMainMessage.text = popupModel.header;
        formInstance.view.popUpConfirmation.rtxPopUpDisclaimer.text = popupModel.message;
        formInstance.view.popUpConfirmation.btnPopUpDelete.text = popupModel.confirmMsg;
        formInstance.view.popUpConfirmation.btnPopUpCancel.text = popupModel.cancelMsg;
        formInstance.view.popUpConfirmation.btnPopUpDelete.onClick = function() {
            popupModel.confirmAction();
            formInstance.view.flxPopUpConfirmation.setVisibility(false);
        };
        formInstance.view.popUpConfirmation.btnPopUpCancel.onClick = function() {
            popupModel.cancelAction();
            formInstance.view.flxPopUpConfirmation.setVisibility(false);
        };
        formInstance.view.popUpConfirmation.flxPopUpClose.onClick = function() {
            formInstance.view.flxPopUpConfirmation.setVisibility(false);
        };
        formInstance.view.flxPopUpConfirmation.setVisibility(true);
    }

    function setVisibility(widget, state) {
        if (state !== widget.isVisible) {
            widget.setVisibility(state);
        }
    }

    function getSortData(SegmentWidget, columnName, sortImgWidget, formInstance) {
        var data = SegmentWidget.info.searchAndSortData;
        if ((!formInstance.sortBy) || formInstance.sortBy.columnName !== columnName) {
            formInstance.sortBy = formInstance.getObjectSorter(columnName);
        }
        formInstance.sortBy.column(columnName);
        if (formInstance.sortBy.inAscendingOrder) {
            sortImgWidget.text = kony.adminConsole.utils.fonticons.DESCENDING_IMAGE;
            sortImgWidget.skin = kony.adminConsole.utils.fonticons.DESCENDING_SKIN;
        } else {
            sortImgWidget.text = kony.adminConsole.utils.fonticons.ASCENDING_IMAGE;
            sortImgWidget.skin = kony.adminConsole.utils.fonticons.ASCENDING_SKIN;
        }
        return data.sort(formInstance.sortBy.sortData);
    }

    function sort(SegmentWidget, columnName, sortImgWidget, defaultScrollWidget, NumberofRecords, LoadMoreRow, formInstance) {
        var data = SegmentWidget.info.searchAndSortData;
        var sortdata = getSortData(SegmentWidget, columnName, sortImgWidget, formInstance);
        if (NumberofRecords === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ALL")) {
            SegmentWidget.info.searchAndSortData = sortdata;
            SegmentWidget.setData(sortdata);
        } else if (data.length > NumberofRecords) {
            SegmentWidget.info.searchAndSortData = sortdata;
            var newData = sortdata.slice(0, NumberofRecords);
            if (LoadMoreRow !== null) newData.push(LoadMoreRow);
            SegmentWidget.setData(newData);
        } else {
            SegmentWidget.info.searchAndSortData = sortdata;
            SegmentWidget.setData(sortdata);
        }
        formInstance.view.forceLayout();
        if (defaultScrollWidget !== "NONE") {
            if (defaultScrollWidget === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.TOEND")) {
                formInstance.view.flxMainContent.scrollToEnd();
            } else {
                formInstance.view.flxMainContent.scrollToWidget(defaultScrollWidget);
            }
        }
    }

    function storeScrollHeight(widget) {
        try {
            scrollHeight = widget.contentOffsetMeasured.y < 0 ? (widget.contentOffsetMeasured.y) * (-1) : (widget.contentOffsetMeasured.y);
        } catch (ignored) {}
    }

    function scrollToDefaultHeight(widget) {
        try {
            widget.setContentOffset({
                y: scrollHeight,
                x: 0
            });
        } catch (ignored) {}
    }

    function copyTextToClipboard(text) {
        function selectElementText(element) {
            if (document.selection) {
                var range = document.body.createTextRange();
                range.moveToElementText(element);
                range.select();
            } else if (window.getSelection) {
                var range = document.createRange();
                range.selectNode(element);
                window.getSelection().removeAllRanges();
                window.getSelection().addRange(range);
            }
        }
        var element = document.createElement('DIV');
        element.textContent = text;
        document.body.appendChild(element);
        selectElementText(element);
        document.execCommand('copy');
        element.remove();
    }
    return {
        AdminConsoleCommonUtils: {
            optimizeCalendarField: optimizeCalendarField,
            optimizeNumberField: optimizeNumberField,
            deepEquals: deepEquals,
            restrictTextFieldToAlphaNumeric: restrictTextFieldToAlphaNumeric,
            restrictTextFieldToNumeric: restrictTextFieldToNumeric,
            restrictTextFieldToPhoneNumber: restrictTextFieldToPhoneNumber,
            rowLeftSwipeAnimation: rowLeftSwipeAnimation,
            isValidAmount: isValidAmount,
            getTruncatedString: getTruncatedString,
            getParamValueOrNA: getParamValueOrNA,
            getParamValueOrEmptyString: getParamValueOrEmptyString,
            togglePreferredCheckbox: togglePreferredCheckbox,
            maskSSN: maskSSN,
            getValidString: getValidString,
            showError: showError,
            showNoError: showNoError,
            openEmailConfirm: openEmailConfirm,
            openErrorPopup: openErrorPopup,
            openConfirm: openConfirm,
            setVisibility: setVisibility,
            radioNotSelected: "radio_notselected.png",
            radioSelected: "radio_selected.png",
            checkboxSelected: "checkboxselected.png",
            checkboxnormal: "checkboxnormal.png",
            checkbox: "checkbox.png",
            checkboxDisable: "checkboxdisable.png",
            ALERTS_INPUT_TYPES: {
                "Range": "Range",
                "List": "List",
                "EqualTo": "EqualTo",
                "NoInputNeeded": "NoInputNeeded"
            },
            getSortData: getSortData,
            sort: sort,
            storeScrollHeight: storeScrollHeight,
            scrollToDefaultHeight: scrollToDefaultHeight,
            copyTextToClipboard: copyTextToClipboard
        }
    };
});