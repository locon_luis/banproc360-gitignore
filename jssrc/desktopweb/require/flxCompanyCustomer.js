define("flxCompanyCustomer", function() {
    return function(controller) {
        var flxCompanyCustomer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanyCustomer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxCompanyCustomer.setDefaultUnit(kony.flex.DP);
        var flxContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxContainer.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato11ABEB13px",
            "text": "Label",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLato11ABEB13pxHover"
        });
        var lblRole = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblRole",
            "isVisible": true,
            "left": "22%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": "15dp",
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUsername = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblUsername",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": "15dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxCustomerStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustomerStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "55%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "16%"
        }, {}, {});
        flxCustomerStatus.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label({
            "bottom": "5dp",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0%",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCustomerStatus = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblCustomerStatus",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustomerStatus.add(lblIconStatus, lblCustomerStatus);
        var lblEmail = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblEmail",
            "isVisible": true,
            "left": "72%",
            "right": "35dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_c2e29b84746b49f38b2cc6c26dd6332c,
            "right": "4%",
            "skin": "slFbox",
            "top": "10dp",
            "width": "25px",
            "zIndex": 10
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblFontIconOptions = new kony.ui.Label({
            "centerX": "45%",
            "centerY": "50%",
            "height": "14dp",
            "id": "lblFontIconOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "10dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblFontIconOptions);
        flxContainer.add(lblName, lblRole, lblUsername, flxCustomerStatus, lblEmail, flxOptions);
        var lblSepartor = new kony.ui.Label({
            "height": "1dp",
            "id": "lblSepartor",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": "-",
            "top": "0dp",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCompanyCustomer.add(flxContainer, lblSepartor);
        return flxCompanyCustomer;
    }
})