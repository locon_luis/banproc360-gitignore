define("flxGroupsAssignedCustomers", function() {
    return function(controller) {
        var flxGroupsAssignedCustomers = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxGroupsAssignedCustomers",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxGroupsAssignedCustomers.setDefaultUnit(kony.flex.DP);
        var flxAssignedCustomers = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxAssignedCustomers",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, {}, {});
        flxAssignedCustomers.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15px",
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "11px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ebb7c15cd55444cd84969056cc624513,
            "right": "10px",
            "skin": "slFbox",
            "width": "15px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "imgCheckBox",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "12px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckbox.add(imgCheckBox);
        var lblName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblName",
            "isVisible": true,
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Name",
            "width": "23%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUserName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUserName",
            "isVisible": true,
            "left": "1%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "User name",
            "width": "22%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCustID = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCustID",
            "isVisible": true,
            "left": "1%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Customer ID",
            "width": "22%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblServicesStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblServicesStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Active",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCustomerStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCustomerStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblServicesStatus, fontIconCustomerStatus);
        var flxDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "30px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxDelete.setDefaultUnit(kony.flex.DP);
        var fontIconDelete = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "fontIconDelete",
            "isVisible": true,
            "skin": "sknLblIcon484b5218px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDelete.add(fontIconDelete);
        var lblRemoved = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRemoved",
            "isVisible": false,
            "left": "0%",
            "skin": "sknLbl484b52ItalicSz12px",
            "text": "Removed",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAssignedCustomers.add(flxCheckbox, lblName, lblUserName, lblCustID, flxStatus, flxDelete, lblRemoved);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxGroupsAssignedCustomers.add(flxAssignedCustomers, lblSeperator);
        return flxGroupsAssignedCustomers;
    }
})