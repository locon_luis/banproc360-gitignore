define("LogsModule/frmLogs", function() {
    return function(controller) {
        function addWidgetsfrmLogs() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxBrandLogo": {
                        "top": undefined
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "CopysknTextAreaNormal0i7deca82290347",
                "top": "0.00%",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false
                    },
                    "btnDropdownList": {
                        "isVisible": false,
                        "text": "DOWNLOAD LIST"
                    },
                    "flxButtons": {
                        "isVisible": true
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "flxHeaderUserOptions": {
                        "isVisible": true
                    },
                    "flxMainHeader": {
                        "zIndex": 4
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "text": "Logs"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrumbs",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 4
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "100%",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 100
            }, {}, {});
            breadcrumbs.setDefaultUnit(kony.flex.DP);
            var btnBackToMain = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnBackToMain",
                "isVisible": true,
                "left": "35px",
                "skin": "sknBtnLato11ABEB11Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.SYSTEM_LOGS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadcrumbsRight = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblBreadcrumbsRight",
                "isVisible": true,
                "left": "10dp",
                "right": 8,
                "skin": "sknIcon12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblBreadcrumbsRight\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPreviousPage = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnPreviousPage",
                "isVisible": false,
                "left": "0",
                "skin": "sknBtnLato11ABEB11Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.SECURITYQUESTIONS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadcrumbsRight2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblBreadcrumbsRight2",
                "isVisible": false,
                "left": "10dp",
                "right": 8,
                "skin": "sknIcon12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblBreadcrumbsRight\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPreviousPage1 = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnPreviousPage1",
                "isVisible": false,
                "left": "0",
                "skin": "sknBtnLato11ABEB11Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.SECURITYQUESTIONS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadcrumbsRight3 = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblBreadcrumbsRight3",
                "isVisible": false,
                "left": "10dp",
                "right": 8,
                "skin": "sknIcon12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblBreadcrumbsRight\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCurrent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCurrent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxCurrent.setDefaultUnit(kony.flex.DP);
            var lblCurrentScreen = new kony.ui.Label({
                "height": "20px",
                "id": "lblCurrentScreen",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoReg485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.TRANSACTIONAL\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadcrumbsDown = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblBreadcrumbsDown",
                "isVisible": false,
                "left": "10dp",
                "right": 8,
                "skin": "sknIcon12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": "14dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCurrent.add(lblCurrentScreen, lblBreadcrumbsDown);
            breadcrumbs.add(btnBackToMain, lblBreadcrumbsRight, btnPreviousPage, lblBreadcrumbsRight2, btnPreviousPage1, lblBreadcrumbsRight3, flxCurrent);
            flxBreadCrumbs.add(breadcrumbs);
            var flxLogsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxLogsList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "CopysknflxffffffOp0e013ad39096b4d",
                "top": "135px",
                "zIndex": 1
            }, {}, {});
            flxLogsList.setDefaultUnit(kony.flex.DP);
            var flxViewContainer2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "28dp",
                "id": "flxViewContainer2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "Copysknflxffffffop0e0ae7cd7f71746",
                "top": "0dp",
                "zIndex": 3
            }, {}, {});
            flxViewContainer2.setDefaultUnit(kony.flex.DP);
            var flxViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 4
            }, {}, {});
            flxViewTabs.setDefaultUnit(kony.flex.DP);
            var flxViewTab1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxViewTab1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "150px",
                "zIndex": 1
            }, {}, {});
            flxViewTab1.setDefaultUnit(kony.flex.DP);
            var lblTabName1 = new kony.ui.Label({
                "height": "100%",
                "id": "lblTabName1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblUtilActive485b7512pxBold",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Logs.defaultSystemLogs\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTabUnderline1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "3dp",
                "id": "flxTabUnderline1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "top": "23px",
                "width": "140px",
                "zIndex": 1
            }, {}, {});
            flxTabUnderline1.setDefaultUnit(kony.flex.DP);
            flxTabUnderline1.add();
            flxViewTab1.add(lblTabName1, flxTabUnderline1);
            var flxViewTab2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxViewTab2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "32dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "125px",
                "zIndex": 1
            }, {}, {});
            flxViewTab2.setDefaultUnit(kony.flex.DP);
            var lblTabName2 = new kony.ui.Label({
                "height": "100%",
                "id": "lblTabName2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblUtilActive485b7512pxBold",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Logs.myFilteredLogs\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTabUnderline2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "4dp",
                "id": "flxTabUnderline2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-1px",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "top": "23px",
                "width": "125px",
                "zIndex": 1
            }, {}, {});
            flxTabUnderline2.setDefaultUnit(kony.flex.DP);
            flxTabUnderline2.add();
            flxViewTab2.add(lblTabName2, flxTabUnderline2);
            flxViewTabs.add(flxViewTab1, flxViewTab2);
            var flxViewSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxD5D9DD",
                "top": "26px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxViewSeperator.setDefaultUnit(kony.flex.DP);
            flxViewSeperator.add();
            flxViewContainer2.add(flxViewTabs, flxViewSeperator);
            var flxSystemLogsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxSystemLogsList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "73dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSystemLogsList.setDefaultUnit(kony.flex.DP);
            var flxLogDefaultTabs1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxLogDefaultTabs1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30.80%"
            }, {}, {});
            flxLogDefaultTabs1.setDefaultUnit(kony.flex.DP);
            var LogDefaultTabs = new com.adminConsole.logs.LogDefaultTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "LogDefaultTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "LogDefaultTabs": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0px",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0px",
                        "width": "100%"
                    },
                    "imgLog": {
                        "isVisible": false,
                        "src": "icontransactional_2x.png"
                    },
                    "lblLog": {
                        "height": "60dp",
                        "left": "25dp",
                        "top": "30dp",
                        "width": "75dp"
                    },
                    "lblLogDesc": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.TransactionalLogTabDesc\")"
                    },
                    "lblLogHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.TransactionalLogTabHeading\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLogDefaultTabs1.add(LogDefaultTabs);
            var flxLogDefaultTabs2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxLogDefaultTabs2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30.80%"
            }, {}, {});
            flxLogDefaultTabs2.setDefaultUnit(kony.flex.DP);
            var LogDefaultTabs2 = new com.adminConsole.logs.LogDefaultTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "LogDefaultTabs2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "LogDefaultTabs": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0px",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0px",
                        "width": "100%"
                    },
                    "imgLog": {
                        "isVisible": false,
                        "src": "iconadminconsole_2x.png"
                    },
                    "lblLog": {
                        "height": "60dp",
                        "left": "30dp",
                        "width": "70dp"
                    },
                    "lblLogDesc": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.AdminConsoleLogTabDesc\")"
                    },
                    "lblLogHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.AdminConsoleLogTabHeading\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLogDefaultTabs2.add(LogDefaultTabs2);
            var flxLogDefaultTabs3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxLogDefaultTabs3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30.80%"
            }, {}, {});
            flxLogDefaultTabs3.setDefaultUnit(kony.flex.DP);
            var LogDefaultTabs3 = new com.adminConsole.logs.LogDefaultTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "LogDefaultTabs3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "LogDefaultTabs": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0%",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0",
                        "width": "100%"
                    },
                    "imgLog": {
                        "src": "iconcustomeractivity_2x.png"
                    },
                    "lblLog": {
                        "height": "60dp",
                        "left": "30dp",
                        "width": "70dp"
                    },
                    "lblLogDesc": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.CustomerActivityTabDesc\")"
                    },
                    "lblLogHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCustomerSpecificLogs\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLogDefaultTabs3.add(LogDefaultTabs3);
            flxSystemLogsList.add(flxLogDefaultTabs1, flxLogDefaultTabs2, flxLogDefaultTabs3);
            var flxCustomLogsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxCustomLogsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0eab7b4ccac7d40",
                "top": "28dp",
                "width": "100%",
                "zIndex": 4
            }, {}, {});
            flxCustomLogsList.setDefaultUnit(kony.flex.DP);
            var flxAllTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "26px",
                "id": "flxAllTypes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "20px",
                "width": "85px",
                "zIndex": 4
            }, {}, {});
            flxAllTypes.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "id": "lblName",
                "isVisible": true,
                "left": "0px",
                "skin": "CopysknlblLato0c696f51a15df4c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.All_Types\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 4
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Transaction Logs"
            });
            var lblDown = new kony.ui.Label({
                "id": "lblDown",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 4
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgDown = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgDown",
                "isVisible": false,
                "left": "5dp",
                "skin": "slImage",
                "src": "img_down_arrow.png",
                "top": "1px",
                "width": "15dp",
                "zIndex": 4
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAllTypes.add(lblName, lblDown, imgDown);
            var flxTypesList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTypesList",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "43dp",
                "width": "120px",
                "zIndex": 200
            }, {}, {});
            flxTypesList.setDefaultUnit(kony.flex.DP);
            var flxOption4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxOption4",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption4.setDefaultUnit(kony.flex.DP);
            var lblOption4 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption4",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.All_Types\")",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption4.add(lblOption4);
            var flxOption1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxOption1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption1.setDefaultUnit(kony.flex.DP);
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Transactional",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption1.add(lblOption1);
            var flxOption2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxOption2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption2.setDefaultUnit(kony.flex.DP);
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblOption2\")",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption2.add(lblOption2);
            var flxOption3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxOption3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption3.setDefaultUnit(kony.flex.DP);
            var lblOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption3",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblOption3\")",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption3.add(lblOption3);
            flxTypesList.add(flxOption4, flxOption1, flxOption2, flxOption3);
            var search = new com.adminConsole.header.search({
                "clipBounds": true,
                "height": "48dp",
                "id": "search",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "18px",
                "width": "99%",
                "overrides": {
                    "flxSearchContainer": {
                        "top": "0dp"
                    },
                    "imgSearchCancel": {
                        "src": "close_blue.png"
                    },
                    "imgSearchIcon": {
                        "src": "search_1x.png"
                    },
                    "search": {
                        "top": "18px",
                        "width": "99%"
                    },
                    "tbxSearchBox": {
                        "placeholder": "Search by Log name"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxScrollableList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "355px",
                "horizontalScrollIndicator": true,
                "id": "flxScrollableList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-15dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "91px",
                "verticalScrollIndicator": true,
                "zIndex": 100
            }, {}, {});
            flxScrollableList.setDefaultUnit(kony.flex.DP);
            var flxMainList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0px",
                "clipBounds": false,
                "id": "flxMainList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainList.setDefaultUnit(kony.flex.DP);
            flxMainList.add();
            flxScrollableList.add(flxMainList);
            var flxNoRecords = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300px",
                "id": "flxNoRecords",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "91dp",
                "zIndex": 5
            }, {}, {});
            flxNoRecords.setDefaultUnit(kony.flex.DP);
            var rtxNoResultsFound = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoResultsFound",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNoResultsFound\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecords.add(rtxNoResultsFound);
            flxCustomLogsList.add(flxAllTypes, flxTypesList, search, flxScrollableList, flxNoRecords);
            flxLogsList.add(flxViewContainer2, flxSystemLogsList, flxCustomLogsList);
            var flxCustomerActivityLog = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300dp",
                "id": "flxCustomerActivityLog",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "130px",
                "zIndex": 1
            }, {}, {});
            flxCustomerActivityLog.setDefaultUnit(kony.flex.DP);
            var searchCustomer = new com.adminConsole.logs.searchCustomer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "searchCustomer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0hfd18814fd664dCM",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "flxFirstRow": {
                        "right": "35px"
                    },
                    "flxSearchCriteria1": {
                        "width": "30%"
                    },
                    "flxSearchCriteria2": {
                        "left": "viz.val_cleared",
                        "width": "30%"
                    },
                    "flxSearchCriteria3": {
                        "left": "viz.val_cleared",
                        "right": "0px",
                        "width": "30%"
                    },
                    "imgSearchError": {
                        "isVisible": false,
                        "src": "error_2x.png",
                        "top": "0dp"
                    },
                    "lblSearchError": {
                        "isVisible": false
                    },
                    "lblSearchParam2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.customerSearch.MemberID\")",
                        "right": "viz.val_cleared"
                    },
                    "lblSearchParam3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.customerUserName_sentenceCase\")"
                    },
                    "searchCustomer": {
                        "left": "0dp",
                        "top": "30dp",
                        "width": "100%"
                    },
                    "txtSearchParam2": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.customerSearch.MemberID\")"
                    },
                    "txtSearchParam3": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.customerUserName_sentenceCase\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSearchButtonsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxSearchButtonsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchButtonsWrapper.setDefaultUnit(kony.flex.DP);
            var flxSearchSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxSearchSeperator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxSearchSeperator.setDefaultUnit(kony.flex.DP);
            flxSearchSeperator.add();
            var flxSearchButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxSearchButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxSearchButtons.setDefaultUnit(kony.flex.DP);
            var searchButtons = new com.adminConsole.common.commonButtons({
                "clipBounds": true,
                "height": "80px",
                "id": "searchButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "text": "RESET"
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "text": "SEARCH"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchButtons.add(searchButtons);
            flxSearchButtonsWrapper.add(flxSearchSeperator, flxSearchButtons);
            flxCustomerActivityLog.add(searchCustomer, flxSearchButtonsWrapper);
            var flxModifySearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxModifySearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "150px",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxModifySearch.setDefaultUnit(kony.flex.DP);
            var modifySearch = new com.adminConsole.search.modifySearch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "height": "20px",
                "id": "modifySearch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "modifySearch": {
                        "bottom": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxModifySearch.add(modifySearch);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainContent = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": false,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "top": "170px",
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxCustomerSearchResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomerSearchResults",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxCustomerSearchResults.setDefaultUnit(kony.flex.DP);
            var flxSearchResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxSearchResult",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxSearchResult.setDefaultUnit(kony.flex.DP);
            var flxSearchResultExistingCustomerHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxSearchResultExistingCustomerHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0",
                "zIndex": 2
            }, {}, {});
            flxSearchResultExistingCustomerHeader.setDefaultUnit(kony.flex.DP);
            var flxExistingUserHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxExistingUserHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxExistingUserHeader.setDefaultUnit(kony.flex.DP);
            var flxExistingCustomerName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingCustomerName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "3%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "21%",
                "zIndex": 1
            }, {}, {});
            flxExistingCustomerName.setDefaultUnit(kony.flex.DP);
            var lblExistingCustomerName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExistingCustomerName",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": "38px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortExistingCustomerName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortExistingCustomerName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortExistingCustomerName.setDefaultUnit(kony.flex.DP);
            var lblSortExistingCustomerName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortExistingCustomerName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortExistingCustomerName.add(lblSortExistingCustomerName);
            flxExistingCustomerName.add(lblExistingCustomerName, flxSortExistingCustomerName);
            var flxExistingCustomerUsername = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingCustomerUsername",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxExistingCustomerUsername.setDefaultUnit(kony.flex.DP);
            var lblExistingCustomerUsername = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExistingCustomerUsername",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.customerUserName\")",
                "top": 0,
                "width": "50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortExistingCustomerUsername = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortExistingCustomerUsername",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortExistingCustomerUsername.setDefaultUnit(kony.flex.DP);
            var lblSortExistingCustomerUsername = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortExistingCustomerUsername",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortExistingCustomerUsername.add(lblSortExistingCustomerUsername);
            flxExistingCustomerUsername.add(lblExistingCustomerUsername, flxSortExistingCustomerUsername);
            var flxExistingCustomerUserID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingCustomerUserID",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "16%",
                "zIndex": 1
            }, {}, {});
            flxExistingCustomerUserID.setDefaultUnit(kony.flex.DP);
            var lblExistingCustomerUserId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExistingCustomerUserId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailCustomerId\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortExistingCustomerUserId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortExistingCustomerUserId",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortExistingCustomerUserId.setDefaultUnit(kony.flex.DP);
            var imgSortExistingCustomerUserId = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortExistingCustomerUserId",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortExistingCustomerUserId.add(imgSortExistingCustomerUserId);
            flxExistingCustomerUserID.add(lblExistingCustomerUserId, flxSortExistingCustomerUserId);
            var flxExistingCustomerContactNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingCustomerContactNumber",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxExistingCustomerContactNumber.setDefaultUnit(kony.flex.DP);
            var lblExistingCustomerContactNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExistingCustomerContactNumber",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CONTACT_NUMBER\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortExistingCustomerContactNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortExistingCustomerContactNumber",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortExistingCustomerContactNumber.setDefaultUnit(kony.flex.DP);
            var imgSortExistingCustomerContactNumber = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortExistingCustomerContactNumber",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortExistingCustomerContactNumber.add(imgSortExistingCustomerContactNumber);
            flxExistingCustomerContactNumber.add(lblExistingCustomerContactNumber, flxSortExistingCustomerContactNumber);
            var flxExistingCustomerEmailId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingCustomerEmailId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxExistingCustomerEmailId.setDefaultUnit(kony.flex.DP);
            var lblExistingCustomerEmailId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExistingCustomerEmailId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.EMAILID\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortExistingCustomerEmailId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortExistingCustomerEmailId",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortExistingCustomerEmailId.setDefaultUnit(kony.flex.DP);
            var imgSortExistingCustomerEmailId = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortExistingCustomerEmailId",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortExistingCustomerEmailId.add(imgSortExistingCustomerEmailId);
            flxExistingCustomerEmailId.add(lblExistingCustomerEmailId, flxSortExistingCustomerEmailId);
            flxExistingUserHeader.add(flxExistingCustomerName, flxExistingCustomerUsername, flxExistingCustomerUserID, flxExistingCustomerContactNumber, flxExistingCustomerEmailId);
            var lblUsersHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblUsersHeaderSeperator",
                "isVisible": true,
                "left": "35px",
                "right": "35px",
                "skin": "sknLblHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchResultExistingCustomerHeader.add(flxExistingUserHeader, lblUsersHeaderSeperator);
            var flxSearchResultSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchResultSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchResultSegment.setDefaultUnit(kony.flex.DP);
            var searchResults = new com.adminConsole.common.listingSegmentLogs({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "searchResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "flxContextualMenu": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchResultSegment.add(searchResults);
            flxSearchResult.add(flxSearchResultExistingCustomerHeader, flxSearchResultSegment);
            flxCustomerSearchResults.add(flxSearchResult);
            var flxCustomerDetailedResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": false,
                "id": "flxCustomerDetailedResults",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxCustomerDetailedResults.setDefaultUnit(kony.flex.DP);
            var flxCustomerContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35dp",
                "clipBounds": true,
                "id": "flxCustomerContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "40dp",
                "isModalContainer": false,
                "right": "10dp",
                "skin": "sknflxffffffOp0b",
                "top": "0dp"
            }, {}, {});
            flxCustomerContent.setDefaultUnit(kony.flex.DP);
            var flxHeader3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeader3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxHeader3.setDefaultUnit(kony.flex.DP);
            var lblCustomerName3 = new kony.ui.Label({
                "id": "lblCustomerName3",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato18px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCustomerName3\")",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxColumn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumn",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxColumn.setDefaultUnit(kony.flex.DP);
            var lblUserHeading = new kony.ui.Label({
                "id": "lblUserHeading",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.customerUseName\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUserData = new kony.ui.Label({
                "id": "lblUserData",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustomerIdHeading = new kony.ui.Label({
                "id": "lblCustomerIdHeading",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLato5d6c7f12px",
                "text": "CUSTOMER ID:",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustomerIDData = new kony.ui.Label({
                "id": "lblCustomerIDData",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxColumn.add(lblUserHeading, lblUserData, lblCustomerIdHeading, lblCustomerIDData);
            flxHeader3.add(lblCustomerName3, flxColumn);
            var flxActivityType3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "72dp",
                "id": "flxActivityType3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-1dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknbor1pxe4e6ecbgf8f9faborrad3px",
                "top": "16dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActivityType3.setDefaultUnit(kony.flex.DP);
            var flxViewActivity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewActivity",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp"
            }, {}, {});
            flxViewActivity.setDefaultUnit(kony.flex.DP);
            var lblCustomerType = new kony.ui.Label({
                "id": "lblCustomerType",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCustomerType\")",
                "top": "13px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxType3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxType3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "7px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxType3.setDefaultUnit(kony.flex.DP);
            var flxRadioExistingCustomer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadioExistingCustomer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioExistingCustomer.setDefaultUnit(kony.flex.DP);
            var imgRadioExistingCustomer = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadioExistingCustomer",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioExistingCustomer.add(imgRadioExistingCustomer);
            var lblRadioExistingCustomer = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRadioExistingCustomer",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Customer\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRadioApplicant = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadioApplicant",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioApplicant.setDefaultUnit(kony.flex.DP);
            var imgRadioApplicant = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadioApplicant",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioApplicant.add(imgRadioApplicant);
            var lblRadioApplicant = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRadioApplicant",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.CSR_Assist\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxType3.add(flxRadioExistingCustomer, lblRadioExistingCustomer, flxRadioApplicant, lblRadioApplicant);
            flxViewActivity.add(lblCustomerType, flxType3);
            var flxSearchFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "16dp",
                "width": "500dp"
            }, {}, {});
            flxSearchFilter.setDefaultUnit(kony.flex.DP);
            var flxSearchCustomerContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxSearchCustomerContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "60dp",
                "skin": "sknflxd5d9ddop100",
                "top": "0px",
                "width": "410px",
                "zIndex": 1
            }, {}, {});
            flxSearchCustomerContainer.setDefaultUnit(kony.flex.DP);
            var tbxCustomerSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "40dp",
                "id": "tbxCustomerSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "5px",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.searchCustomerActivity\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearCustomerSearchImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxClearCustomerSearchImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearCustomerSearchImage.setDefaultUnit(kony.flex.DP);
            var fontIconCustomerCross = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fontIconCustomerCross",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearCustomerSearchImage.add(fontIconCustomerCross);
            var flxSearchIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "12px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "20dp"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxSearchIcon.setDefaultUnit(kony.flex.DP);
            var fontIconSearchCustomerImg = new kony.ui.Label({
                "bottom": "10dp",
                "id": "fontIconSearchCustomerImg",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchIcon.add(fontIconSearchCustomerImg);
            flxSearchCustomerContainer.add(tbxCustomerSearchBox, flxClearCustomerSearchImage, flxSearchIcon);
            var flxCustomerFilterIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxCustomerFilterIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "5dp",
                "width": "25dp"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCustomerFilterIcon.setDefaultUnit(kony.flex.DP);
            var lblFilterCustomerLogs = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "lblFilterCustomerLogs",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknIconBlack25px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerFilterIcon.add(lblFilterCustomerLogs);
            flxSearchFilter.add(flxSearchCustomerContainer, flxCustomerFilterIcon);
            flxActivityType3.add(flxViewActivity, flxSearchFilter);
            var flxResults = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "20dp",
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "335dp",
                "horizontalScrollIndicator": true,
                "id": "flxResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "35dp",
                "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
                "skin": "slFSbox",
                "top": "10dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxResults.setDefaultUnit(kony.flex.DP);
            var flxInnerResults = new kony.ui.FlexContainer({
                "clipBounds": false,
                "height": "100%",
                "id": "flxInnerResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxInnerResults.setDefaultUnit(kony.flex.DP);
            var flxDetailedResultHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60px",
                "id": "flxDetailedResultHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0e8dde5f64bde4c",
                "top": "0dp",
                "width": "1568dp",
                "zIndex": 1
            }, {}, {});
            flxDetailedResultHeader.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "width": "1568dp",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var flxModuleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxModuleName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxModuleName.setDefaultUnit(kony.flex.DP);
            var lblModuleName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblModuleName",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblAdminConsoleModuleName\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortModuleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortModuleName",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortModuleName.setDefaultUnit(kony.flex.DP);
            var imgSortModuleName = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortModuleName",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortModuleName.add(imgSortModuleName);
            flxModuleName.add(lblModuleName, flxSortModuleName);
            var flxLogType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxLogType.setDefaultUnit(kony.flex.DP);
            var lblLogType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogType",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "LOG TYPE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortLogType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortLogType",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortLogType.setDefaultUnit(kony.flex.DP);
            var CopyimgSortModuleName0ibb7f4e17a6c4b = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "CopyimgSortModuleName0ibb7f4e17a6c4b",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortLogType.add(CopyimgSortModuleName0ibb7f4e17a6c4b);
            flxLogType.add(lblLogType, flxSortLogType);
            var flxActivityType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActivityType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxActivityType.setDefaultUnit(kony.flex.DP);
            var lblActivityType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActivityType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblActivityType\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortActivityType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortActivityType",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortActivityType.setDefaultUnit(kony.flex.DP);
            var imgSortActivityType = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortActivityType",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortActivityType.add(imgSortActivityType);
            flxActivityType.add(lblActivityType, flxSortActivityType);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDescription",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDescription.setDefaultUnit(kony.flex.DP);
            var imgSortDescription = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDescription",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDescription.add(imgSortDescription);
            flxDescription.add(lblDescription, flxSortDescription);
            var flxDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxDateAndTime.setDefaultUnit(kony.flex.DP);
            var lblDateAndTime = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDateAndTime",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblDateAndTime\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDateAndTime.setDefaultUnit(kony.flex.DP);
            var imgSortDateAndTime = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDateAndTime",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortDateAndTime = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortDateAndTime",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDateAndTime.add(imgSortDateAndTime, lblSortDateAndTime);
            flxDateAndTime.add(lblDateAndTime, flxSortDateAndTime);
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "3.30%",
                "zIndex": 1
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortStatus.setDefaultUnit(kony.flex.DP);
            var imgSortStatus = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortStatus",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxSortStatus.add(imgSortStatus, lblSortStatus);
            flxStatus.add(lblStatus, flxSortStatus);
            var flxChanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChanel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxChanel.setDefaultUnit(kony.flex.DP);
            var lblChanel = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblChanel",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblChanel\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortChanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortChanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortChanel.setDefaultUnit(kony.flex.DP);
            var imgSortChanel = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortChanel",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortChanel = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortChanel",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortChanel.add(imgSortChanel, lblSortChanel);
            flxChanel.add(lblChanel, flxSortChanel);
            var flxIPAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxIPAddress",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxIPAddress.setDefaultUnit(kony.flex.DP);
            var lblIPAddress = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIPAddress",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblIPAddress\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortIPAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortIPAddress",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortIPAddress.setDefaultUnit(kony.flex.DP);
            var imgSortIPAddress = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortIPAddress",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortIPAddress.add(imgSortIPAddress);
            flxIPAddress.add(lblIPAddress, flxSortIPAddress);
            var flxDevice = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDevice",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxDevice.setDefaultUnit(kony.flex.DP);
            var lblDevice = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDevice",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblDevice\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortDevice = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDevice",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDevice.setDefaultUnit(kony.flex.DP);
            var imgSortDevice = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDevice",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDevice.add(imgSortDevice);
            flxDevice.add(lblDevice, flxSortDevice);
            var flxOS = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOS",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxOS.setDefaultUnit(kony.flex.DP);
            var lblOS = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOS",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblOS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortOS = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortOS",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortOS.setDefaultUnit(kony.flex.DP);
            var imgSortOS = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortOS",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortOS = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortOS",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortOS.add(imgSortOS, lblSortOS);
            flxOS.add(lblOS, flxSortOS);
            var flxReferenceId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxReferenceId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxReferenceId.setDefaultUnit(kony.flex.DP);
            var lblReferenceId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblReferenceId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblReferenceId\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortReferenceId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortReferenceId",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortReferenceId.setDefaultUnit(kony.flex.DP);
            var imgSortReferenceId = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortReferenceId",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortReferenceId.add(imgSortReferenceId);
            flxReferenceId.add(lblReferenceId, flxSortReferenceId);
            var flxErrorCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxErrorCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxErrorCode.setDefaultUnit(kony.flex.DP);
            var lblErrorCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorCode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblErrorCode\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortErrorCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortErrorCode",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortErrorCode.setDefaultUnit(kony.flex.DP);
            var imgSortErrorCode = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortErrorCode",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortErrorCode.add(imgSortErrorCode);
            flxErrorCode.add(lblErrorCode, flxSortErrorCode);
            var flxMFAType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMFAType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxMFAType.setDefaultUnit(kony.flex.DP);
            var lblMFAType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMFAType",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "TYPE OF MFA",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortMFAType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortMFAType",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortMFAType.setDefaultUnit(kony.flex.DP);
            var CopyimgSortModuleName0d73677cfe8d046 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "CopyimgSortModuleName0d73677cfe8d046",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortMFAType.add(CopyimgSortModuleName0d73677cfe8d046);
            flxMFAType.add(lblMFAType, flxSortMFAType);
            flxHeader.add(flxModuleName, flxLogType, flxActivityType, flxDescription, flxDateAndTime, flxStatus, flxChanel, flxIPAddress, flxDevice, flxOS, flxReferenceId, flxErrorCode, flxMFAType);
            var lblUsersHeaderSeperator1 = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblUsersHeaderSeperator1",
                "isVisible": true,
                "left": "35px",
                "right": 0,
                "skin": "sknLblHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "width": "1568dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailedResultHeader.add(flxHeader, lblUsersHeaderSeperator1);
            var flxDetailedResultHeaderAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxDetailedResultHeaderAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0e8dde5f64bde4c",
                "top": "0dp",
                "width": "2535dp",
                "zIndex": 1
            }, {}, {});
            flxDetailedResultHeaderAll.setDefaultUnit(kony.flex.DP);
            var flxHeaderAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxHeaderAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "2500dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderAll.setDefaultUnit(kony.flex.DP);
            var flxModuleNameAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxModuleNameAll",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxModuleNameAll.setDefaultUnit(kony.flex.DP);
            var lblModuleNameAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblModuleNameAll",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblAdminConsoleModuleName\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortModuleNameAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortModuleNameAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortModuleNameAll.setDefaultUnit(kony.flex.DP);
            var imgSortModuleNameAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortModuleNameAll",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortModuleNameAll.add(imgSortModuleNameAll);
            flxModuleNameAll.add(lblModuleNameAll, flxSortModuleNameAll);
            var flxLogTypeAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogTypeAll",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxLogTypeAll.setDefaultUnit(kony.flex.DP);
            var lblLogTypeAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogTypeAll",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "LOG TYPE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortLogTypeAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortLogTypeAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortLogTypeAll.setDefaultUnit(kony.flex.DP);
            var CopyimgSortModuleName0ca88d7d7a0d04e = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "CopyimgSortModuleName0ca88d7d7a0d04e",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortLogTypeAll.add(CopyimgSortModuleName0ca88d7d7a0d04e);
            flxLogTypeAll.add(lblLogTypeAll, flxSortLogTypeAll);
            var flxActivityTypeAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActivityTypeAll",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxActivityTypeAll.setDefaultUnit(kony.flex.DP);
            var lblActivityTypeAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActivityTypeAll",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblActivityType\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortActivityTypeAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortActivityTypeAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortActivityTypeAll.setDefaultUnit(kony.flex.DP);
            var imgSortActivityTypeAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortActivityTypeAll",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortActivityTypeAll.add(imgSortActivityTypeAll);
            flxActivityTypeAll.add(lblActivityTypeAll, flxSortActivityTypeAll);
            var flxDescriptionAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDescriptionAll",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "5.60%",
                "zIndex": 1
            }, {}, {});
            flxDescriptionAll.setDefaultUnit(kony.flex.DP);
            var lblDescriptionAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDescriptionAll",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortDescriptionAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDescriptionAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDescriptionAll.setDefaultUnit(kony.flex.DP);
            var CopyimgSortDescription0j3f6b39711e641 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "CopyimgSortDescription0j3f6b39711e641",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDescriptionAll.add(CopyimgSortDescription0j3f6b39711e641);
            flxDescriptionAll.add(lblDescriptionAll, flxSortDescriptionAll);
            var flxPayeeNameAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPayeeNameAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxPayeeNameAll.setDefaultUnit(kony.flex.DP);
            var lblPayeeNameAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPayeeNameAll",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "PAYEE NAME",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortPayeeNameAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortPayeeNameAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortPayeeNameAll.setDefaultUnit(kony.flex.DP);
            var imgSortPayeeNameAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortPayeeNameAll",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortPayeeNameAll.add(imgSortPayeeNameAll);
            flxPayeeNameAll.add(lblPayeeNameAll, flxSortPayeeNameAll);
            var flxDateAndTimeAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDateAndTimeAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxDateAndTimeAll.setDefaultUnit(kony.flex.DP);
            var lblDateAndTimeAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDateAndTimeAll",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblDateAndTime\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortDateAndTimeAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDateAndTimeAll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDateAndTimeAll.setDefaultUnit(kony.flex.DP);
            var imgSortDateAndTimeAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDateAndTimeAll",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortDateAndTimeAll = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortDateAndTimeAll",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDateAndTimeAll.add(imgSortDateAndTimeAll, lblSortDateAndTimeAll);
            flxDateAndTimeAll.add(lblDateAndTimeAll, flxSortDateAndTimeAll);
            var flxStatusAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatusAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "3%",
                "zIndex": 1
            }, {}, {});
            flxStatusAll.setDefaultUnit(kony.flex.DP);
            var lblStatusAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatusAll",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortStatusAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortStatusAll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortStatusAll.setDefaultUnit(kony.flex.DP);
            var imgSortStatusAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortStatusAll",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortStatusAll = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortStatusAll",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxSortStatusAll.add(imgSortStatusAll, lblSortStatusAll);
            flxStatusAll.add(lblStatusAll, flxSortStatusAll);
            var flxChanelAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChanelAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxChanelAll.setDefaultUnit(kony.flex.DP);
            var lblChanelAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblChanelAll",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblChanel\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortChanelAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortChanelAll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortChanelAll.setDefaultUnit(kony.flex.DP);
            var imgSortChanelAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortChanelAll",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortChanelAll = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortChanelAll",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortChanelAll.add(imgSortChanelAll, lblSortChanelAll);
            flxChanelAll.add(lblChanelAll, flxSortChanelAll);
            var flxIPAddressAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxIPAddressAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxIPAddressAll.setDefaultUnit(kony.flex.DP);
            var lblIPAddressAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIPAddressAll",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblIPAddress\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortIPAddressAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortIPAddressAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortIPAddressAll.setDefaultUnit(kony.flex.DP);
            var imgSortIPAddressAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortIPAddressAll",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortIPAddressAll.add(imgSortIPAddressAll);
            flxIPAddressAll.add(lblIPAddressAll, flxSortIPAddressAll);
            var flxDeviceAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeviceAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxDeviceAll.setDefaultUnit(kony.flex.DP);
            var lblDeviceAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeviceAll",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblDevice\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortDeviceAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDeviceAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDeviceAll.setDefaultUnit(kony.flex.DP);
            var imgSortDeviceAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDeviceAll",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDeviceAll.add(imgSortDeviceAll);
            flxDeviceAll.add(lblDeviceAll, flxSortDeviceAll);
            var flxOSAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOSAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "3%",
                "zIndex": 1
            }, {}, {});
            flxOSAll.setDefaultUnit(kony.flex.DP);
            var lblOSAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOSAll",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblOS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortOSAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortOSAll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortOSAll.setDefaultUnit(kony.flex.DP);
            var imgSortOSAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortOSAll",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortOSAll = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortOSAll",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortOSAll.add(imgSortOSAll, lblSortOSAll);
            flxOSAll.add(lblOSAll, flxSortOSAll);
            var flxReferenceIdAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxReferenceIdAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxReferenceIdAll.setDefaultUnit(kony.flex.DP);
            var lblReferenceIdAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblReferenceIdAll",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblReferenceId\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortReferenceIdAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortReferenceIdAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortReferenceIdAll.setDefaultUnit(kony.flex.DP);
            var imgSortReferenceIdAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortReferenceIdAll",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortReferenceIdAll.add(imgSortReferenceIdAll);
            flxReferenceIdAll.add(lblReferenceIdAll, flxSortReferenceIdAll);
            var flxErrorCodeAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxErrorCodeAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxErrorCodeAll.setDefaultUnit(kony.flex.DP);
            var lblErrorCodeAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorCodeAll",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblErrorCode\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortErrorCodeAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortErrorCodeAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortErrorCodeAll.setDefaultUnit(kony.flex.DP);
            var imgSortErrorCodeAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortErrorCodeAll",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortErrorCodeAll.add(imgSortErrorCodeAll);
            flxErrorCodeAll.add(lblErrorCodeAll, flxSortErrorCodeAll);
            var flxMFATypeAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMFATypeAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxMFATypeAll.setDefaultUnit(kony.flex.DP);
            var lblMFATypeAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMFATypeAll",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "TYPE OF MFA",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortMFATypeAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortMFATypeAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortMFATypeAll.setDefaultUnit(kony.flex.DP);
            var imgSortMFATypeAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortMFATypeAll",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortMFATypeAll.add(imgSortMFATypeAll);
            flxMFATypeAll.add(lblMFATypeAll, flxSortMFATypeAll);
            var flxAccountNumberAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountNumberAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxAccountNumberAll.setDefaultUnit(kony.flex.DP);
            var lblAccountNumberAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountNumberAll",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "ACCOUNT NAME",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortAccountNumberAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAccountNumberAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAccountNumberAll.setDefaultUnit(kony.flex.DP);
            var imgSortAccountNumberAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAccountNumberAll",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAccountNumberAll.add(imgSortAccountNumberAll);
            flxAccountNumberAll.add(lblAccountNumberAll, flxSortAccountNumberAll);
            var flxRelationshipNumberAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRelationshipNumberAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxRelationshipNumberAll.setDefaultUnit(kony.flex.DP);
            var lblRelationshipNumberAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRelationshipNumberAll",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "RELATIONSHIP NUMBER",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortRelationshipNumberAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortRelationshipNumberAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortRelationshipNumberAll.setDefaultUnit(kony.flex.DP);
            var imgSortRelationshipNumberAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortRelationshipNumberAll",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortRelationshipNumberAll.add(imgSortRelationshipNumberAll);
            flxRelationshipNumberAll.add(lblRelationshipNumberAll, flxSortRelationshipNumberAll);
            var flxPhoneNumberAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPhoneNumberAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxPhoneNumberAll.setDefaultUnit(kony.flex.DP);
            var lblPhoneNumberAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPhoneNumberAll",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "PHONE NUMBER",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortPhoneNumberAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortPhoneNumberAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortPhoneNumberAll.setDefaultUnit(kony.flex.DP);
            var imgSortPhoneNumberAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortPhoneNumberAll",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortPhoneNumberAll.add(imgSortPhoneNumberAll);
            flxPhoneNumberAll.add(lblPhoneNumberAll, flxSortPhoneNumberAll);
            var flxEmailAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEmailAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxEmailAll.setDefaultUnit(kony.flex.DP);
            var lblEmailAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailAll",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "EMAIL",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortEmailAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortEmailAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortEmailAll.setDefaultUnit(kony.flex.DP);
            var imgSortEmailAll = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortEmailAll",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortEmailAll.add(imgSortEmailAll);
            flxEmailAll.add(lblEmailAll, flxSortEmailAll);
            var flxBankNameAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBankNameAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4.30%",
                "zIndex": 1
            }, {}, {});
            flxBankNameAll.setDefaultUnit(kony.flex.DP);
            var lblBankNameAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBankNameAll",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "BANK NAME",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortBankNameAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortBankNameAll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortBankNameAll.setDefaultUnit(kony.flex.DP);
            var CopyimgSortPayeeNameAll0hd059843305342 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "CopyimgSortPayeeNameAll0hd059843305342",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortBankNameAll.add(CopyimgSortPayeeNameAll0hd059843305342);
            flxBankNameAll.add(lblBankNameAll, flxSortBankNameAll);
            var flxMaskedAccountNumberAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMaskedAccountNumberAll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxMaskedAccountNumberAll.setDefaultUnit(kony.flex.DP);
            var lblMaskedAccountNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMaskedAccountNumber",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "MASKED ACCOUNT NUMBER",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortMaskedAccountNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortMaskedAccountNumber",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortMaskedAccountNumber.setDefaultUnit(kony.flex.DP);
            var imgSortMaskedAccountNumber = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortMaskedAccountNumber",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortMaskedAccountNumber.add(imgSortMaskedAccountNumber);
            flxMaskedAccountNumberAll.add(lblMaskedAccountNumber, flxSortMaskedAccountNumber);
            flxHeaderAll.add(flxModuleNameAll, flxLogTypeAll, flxActivityTypeAll, flxDescriptionAll, flxPayeeNameAll, flxDateAndTimeAll, flxStatusAll, flxChanelAll, flxIPAddressAll, flxDeviceAll, flxOSAll, flxReferenceIdAll, flxErrorCodeAll, flxMFATypeAll, flxAccountNumberAll, flxRelationshipNumberAll, flxPhoneNumberAll, flxEmailAll, flxBankNameAll, flxMaskedAccountNumberAll);
            var lblUsersHeaderSeperatorAll = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblUsersHeaderSeperatorAll",
                "isVisible": true,
                "left": "35px",
                "right": 0,
                "skin": "sknLblHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "width": "3500dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailedResultHeaderAll.add(flxHeaderAll, lblUsersHeaderSeperatorAll);
            var flxDetailedResultHeaderPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60px",
                "id": "flxDetailedResultHeaderPayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0e8dde5f64bde4c",
                "top": "0dp",
                "width": "2545dp",
                "zIndex": 1
            }, {}, {});
            flxDetailedResultHeaderPayee.setDefaultUnit(kony.flex.DP);
            var flxHeaderPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxHeaderPayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "width": "2545dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderPayee.setDefaultUnit(kony.flex.DP);
            var flxModuleNamePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxModuleNamePayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxModuleNamePayee.setDefaultUnit(kony.flex.DP);
            var CopylblModuleNameAll0f1056196b5ca46 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblModuleNameAll0f1056196b5ca46",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblAdminConsoleModuleName\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyflxSortModuleNameAll0f88101cf4f6a41 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "CopyflxSortModuleNameAll0f88101cf4f6a41",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            CopyflxSortModuleNameAll0f88101cf4f6a41.setDefaultUnit(kony.flex.DP);
            var CopyimgSortModuleNameAll0fdab0080068142 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "CopyimgSortModuleNameAll0fdab0080068142",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxSortModuleNameAll0f88101cf4f6a41.add(CopyimgSortModuleNameAll0fdab0080068142);
            flxModuleNamePayee.add(CopylblModuleNameAll0f1056196b5ca46, CopyflxSortModuleNameAll0f88101cf4f6a41);
            var flxLogTypePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogTypePayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxLogTypePayee.setDefaultUnit(kony.flex.DP);
            var lblLogTypePayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogTypePayee",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "LOG TYPE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortLogTypePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortLogTypePayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortLogTypePayee.setDefaultUnit(kony.flex.DP);
            var imgSortModuleNamePayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortModuleNamePayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortLogTypePayee.add(imgSortModuleNamePayee);
            flxLogTypePayee.add(lblLogTypePayee, flxSortLogTypePayee);
            var flxActivityTypePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActivityTypePayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxActivityTypePayee.setDefaultUnit(kony.flex.DP);
            var lblActivityTypePayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActivityTypePayee",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblActivityType\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortActivityTypePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortActivityTypePayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortActivityTypePayee.setDefaultUnit(kony.flex.DP);
            var imgSortActivityTypePayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortActivityTypePayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortActivityTypePayee.add(imgSortActivityTypePayee);
            flxActivityTypePayee.add(lblActivityTypePayee, flxSortActivityTypePayee);
            var flxDescriptionPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDescriptionPayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxDescriptionPayee.setDefaultUnit(kony.flex.DP);
            var lblDescriptionPayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDescriptionPayee",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortDescriptionPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDescriptionPayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDescriptionPayee.setDefaultUnit(kony.flex.DP);
            var imgSortDescriptionPayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDescriptionPayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDescriptionPayee.add(imgSortDescriptionPayee);
            flxDescriptionPayee.add(lblDescriptionPayee, flxSortDescriptionPayee);
            var flxPayeeNamePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPayeeNamePayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxPayeeNamePayee.setDefaultUnit(kony.flex.DP);
            var lblPayeeNamePayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPayeeNamePayee",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "PAYEE NAME",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortPayeeNamePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortPayeeNamePayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortPayeeNamePayee.setDefaultUnit(kony.flex.DP);
            var imgSortPayeeNamePayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortPayeeNamePayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortPayeeNamePayee.add(imgSortPayeeNamePayee);
            flxPayeeNamePayee.add(lblPayeeNamePayee, flxSortPayeeNamePayee);
            var flxDateAndTimePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDateAndTimePayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxDateAndTimePayee.setDefaultUnit(kony.flex.DP);
            var lblDateAndTimePayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDateAndTimePayee",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblDateAndTime\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortDateAndTimePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDateAndTimePayee",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDateAndTimePayee.setDefaultUnit(kony.flex.DP);
            var imgSortDateAndTimePayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDateAndTimePayee",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortDateAndTimePayee = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortDateAndTimePayee",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDateAndTimePayee.add(imgSortDateAndTimePayee, lblSortDateAndTimePayee);
            flxDateAndTimePayee.add(lblDateAndTimePayee, flxSortDateAndTimePayee);
            var flxStatusPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatusPayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "3%",
                "zIndex": 1
            }, {}, {});
            flxStatusPayee.setDefaultUnit(kony.flex.DP);
            var lblStatusPayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatusPayee",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortStatusPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortStatusPayee",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortStatusPayee.setDefaultUnit(kony.flex.DP);
            var imgSortStatusPayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortStatusPayee",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortStatusPayee = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortStatusPayee",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxSortStatusPayee.add(imgSortStatusPayee, lblSortStatusPayee);
            flxStatusPayee.add(lblStatusPayee, flxSortStatusPayee);
            var flxChanelPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChanelPayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxChanelPayee.setDefaultUnit(kony.flex.DP);
            var lblChanelPayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblChanelPayee",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblChanel\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortChanelPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortChanelPayee",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortChanelPayee.setDefaultUnit(kony.flex.DP);
            var imgSortChanelPayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortChanelPayee",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortChanelPayee = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortChanelPayee",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortChanelPayee.add(imgSortChanelPayee, lblSortChanelPayee);
            flxChanelPayee.add(lblChanelPayee, flxSortChanelPayee);
            var flxIPAddressPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxIPAddressPayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxIPAddressPayee.setDefaultUnit(kony.flex.DP);
            var lblIPAddressPayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIPAddressPayee",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblIPAddress\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortIPAddressPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortIPAddressPayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortIPAddressPayee.setDefaultUnit(kony.flex.DP);
            var imgSortIPAddressPayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortIPAddressPayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortIPAddressPayee.add(imgSortIPAddressPayee);
            flxIPAddressPayee.add(lblIPAddressPayee, flxSortIPAddressPayee);
            var flxDevicePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDevicePayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxDevicePayee.setDefaultUnit(kony.flex.DP);
            var lblDevicePayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDevicePayee",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblDevice\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortDevicePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDevicePayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDevicePayee.setDefaultUnit(kony.flex.DP);
            var imgSortDevicePayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDevicePayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDevicePayee.add(imgSortDevicePayee);
            flxDevicePayee.add(lblDevicePayee, flxSortDevicePayee);
            var flxOSPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOSPayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxOSPayee.setDefaultUnit(kony.flex.DP);
            var lblOSPayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOSPayee",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblOS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortOSPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortOSPayee",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortOSPayee.setDefaultUnit(kony.flex.DP);
            var imgSortOSPayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortOSPayee",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortOSPayee = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortOSPayee",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortOSPayee.add(imgSortOSPayee, lblSortOSPayee);
            flxOSPayee.add(lblOSPayee, flxSortOSPayee);
            var flxReferenceIdPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxReferenceIdPayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4%",
                "zIndex": 1
            }, {}, {});
            flxReferenceIdPayee.setDefaultUnit(kony.flex.DP);
            var lblReferenceIdPayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblReferenceIdPayee",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblReferenceId\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortReferenceIdPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortReferenceIdPayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortReferenceIdPayee.setDefaultUnit(kony.flex.DP);
            var imgSortReferenceIdPayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortReferenceIdPayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortReferenceIdPayee.add(imgSortReferenceIdPayee);
            flxReferenceIdPayee.add(lblReferenceIdPayee, flxSortReferenceIdPayee);
            var flxErrorCodePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxErrorCodePayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxErrorCodePayee.setDefaultUnit(kony.flex.DP);
            var lblErrorCodePayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorCodePayee",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblErrorCode\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortErrorCodePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortErrorCodePayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortErrorCodePayee.setDefaultUnit(kony.flex.DP);
            var imgSortErrorCodePayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortErrorCodePayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortErrorCodePayee.add(imgSortErrorCodePayee);
            flxErrorCodePayee.add(lblErrorCodePayee, flxSortErrorCodePayee);
            var flxMFATypePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMFATypePayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4%",
                "zIndex": 1
            }, {}, {});
            flxMFATypePayee.setDefaultUnit(kony.flex.DP);
            var lblMFATypePayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMFATypePayee",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "TYPE OF MFA",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortMFATypePayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortMFATypePayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortMFATypePayee.setDefaultUnit(kony.flex.DP);
            var imgSortMFATypePayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortMFATypePayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortMFATypePayee.add(imgSortMFATypePayee);
            flxMFATypePayee.add(lblMFATypePayee, flxSortMFATypePayee);
            var flxAccountNumberPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountNumberPayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4%",
                "zIndex": 1
            }, {}, {});
            flxAccountNumberPayee.setDefaultUnit(kony.flex.DP);
            var lblAccountNumberPayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountNumberPayee",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "ACCOUNT NUMBER",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortAccountNumberPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAccountNumberPayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAccountNumberPayee.setDefaultUnit(kony.flex.DP);
            var imgSortAccountNumberPayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAccountNumberPayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAccountNumberPayee.add(imgSortAccountNumberPayee);
            flxAccountNumberPayee.add(lblAccountNumberPayee, flxSortAccountNumberPayee);
            var flxRelationshipNumberPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRelationshipNumberPayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxRelationshipNumberPayee.setDefaultUnit(kony.flex.DP);
            var lblRelationshipNumberPayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRelationshipNumberPayee",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "RELATIONSHIP NUMBER",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortRelationshipNumberPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortRelationshipNumberPayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortRelationshipNumberPayee.setDefaultUnit(kony.flex.DP);
            var imgSortRelationshipNumberPayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortRelationshipNumberPayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortRelationshipNumberPayee.add(imgSortRelationshipNumberPayee);
            flxRelationshipNumberPayee.add(lblRelationshipNumberPayee, flxSortRelationshipNumberPayee);
            var flxPhoneNumberPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPhoneNumberPayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "4%",
                "zIndex": 1
            }, {}, {});
            flxPhoneNumberPayee.setDefaultUnit(kony.flex.DP);
            var lblPhoneNumberPayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPhoneNumberPayee",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "PHONE NUMBER",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortPhoneNumberPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortPhoneNumberPayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortPhoneNumberPayee.setDefaultUnit(kony.flex.DP);
            var imgSortPhoneNumberPayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortPhoneNumberPayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortPhoneNumberPayee.add(imgSortPhoneNumberPayee);
            flxPhoneNumberPayee.add(lblPhoneNumberPayee, flxSortPhoneNumberPayee);
            var flxEmailPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEmailPayee",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxEmailPayee.setDefaultUnit(kony.flex.DP);
            var lblEmailPayee = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailPayee",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "EMAIL",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortEmailPayee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortEmailPayee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortEmailPayee.setDefaultUnit(kony.flex.DP);
            var imgSortEmailPayee = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortEmailPayee",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortEmailPayee.add(imgSortEmailPayee);
            flxEmailPayee.add(lblEmailPayee, flxSortEmailPayee);
            flxHeaderPayee.add(flxModuleNamePayee, flxLogTypePayee, flxActivityTypePayee, flxDescriptionPayee, flxPayeeNamePayee, flxDateAndTimePayee, flxStatusPayee, flxChanelPayee, flxIPAddressPayee, flxDevicePayee, flxOSPayee, flxReferenceIdPayee, flxErrorCodePayee, flxMFATypePayee, flxAccountNumberPayee, flxRelationshipNumberPayee, flxPhoneNumberPayee, flxEmailPayee);
            var lblUsersHeaderSeperatorPayee = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblUsersHeaderSeperatorPayee",
                "isVisible": true,
                "left": "35px",
                "right": 0,
                "skin": "sknLblHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "width": "2545dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailedResultHeaderPayee.add(flxHeaderPayee, lblUsersHeaderSeperatorPayee);
            var flxDetailedResultHeaderAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60px",
                "id": "flxDetailedResultHeaderAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0e8dde5f64bde4c",
                "top": "0dp",
                "width": "2555dp",
                "zIndex": 1
            }, {}, {});
            flxDetailedResultHeaderAccount.setDefaultUnit(kony.flex.DP);
            var flxHeaderAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxHeaderAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "width": "2525dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderAccount.setDefaultUnit(kony.flex.DP);
            var flxModuleNameAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxModuleNameAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "6%",
                "zIndex": 1
            }, {}, {});
            flxModuleNameAccount.setDefaultUnit(kony.flex.DP);
            var lblModuleNameAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblModuleNameAccount",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblAdminConsoleModuleName\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortModuleNameAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortModuleNameAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortModuleNameAccount.setDefaultUnit(kony.flex.DP);
            var imgSortModuleNameAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortModuleNameAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortModuleNameAccount.add(imgSortModuleNameAccount);
            flxModuleNameAccount.add(lblModuleNameAccount, flxSortModuleNameAccount);
            var flxLogTypeAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogTypeAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "6%",
                "zIndex": 1
            }, {}, {});
            flxLogTypeAccount.setDefaultUnit(kony.flex.DP);
            var lblLogTypeAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogTypeAccount",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "LOG TYPE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortLogTypeAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortLogTypeAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortLogTypeAccount.setDefaultUnit(kony.flex.DP);
            var CopyimgSortModuleName0bc6900826f934a = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "CopyimgSortModuleName0bc6900826f934a",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortLogTypeAccount.add(CopyimgSortModuleName0bc6900826f934a);
            flxLogTypeAccount.add(lblLogTypeAccount, flxSortLogTypeAccount);
            var flxActivityTypeAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActivityTypeAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "6%",
                "zIndex": 1
            }, {}, {});
            flxActivityTypeAccount.setDefaultUnit(kony.flex.DP);
            var lblActivityTypeAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActivityTypeAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblActivityType\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortActivityTypeAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortActivityTypeAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortActivityTypeAccount.setDefaultUnit(kony.flex.DP);
            var imgSortActivityTypeAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortActivityTypeAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortActivityTypeAccount.add(imgSortActivityTypeAccount);
            flxActivityTypeAccount.add(lblActivityTypeAccount, flxSortActivityTypeAccount);
            var flxDescriptionAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDescriptionAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxDescriptionAccount.setDefaultUnit(kony.flex.DP);
            var lblDescriptionAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDescriptionAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortDescriptionAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDescriptionAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDescriptionAccount.setDefaultUnit(kony.flex.DP);
            var imgSortDescriptionAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDescriptionAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDescriptionAccount.add(imgSortDescriptionAccount);
            flxDescriptionAccount.add(lblDescriptionAccount, flxSortDescriptionAccount);
            var flxDateAndTimeAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDateAndTimeAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "6%",
                "zIndex": 1
            }, {}, {});
            flxDateAndTimeAccount.setDefaultUnit(kony.flex.DP);
            var lblDateAndTimeAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDateAndTimeAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblDateAndTime\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortDateAndTimeAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDateAndTimeAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDateAndTimeAccount.setDefaultUnit(kony.flex.DP);
            var imgSortDateAndTimeAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDateAndTimeAccount",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortDateAndTimeAccount = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortDateAndTimeAccount",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDateAndTimeAccount.add(imgSortDateAndTimeAccount, lblSortDateAndTimeAccount);
            flxDateAndTimeAccount.add(lblDateAndTimeAccount, flxSortDateAndTimeAccount);
            var flxStatusAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatusAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "3%",
                "zIndex": 1
            }, {}, {});
            flxStatusAccount.setDefaultUnit(kony.flex.DP);
            var lblStatusAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatusAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortStatusAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortStatusAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortStatusAccount.setDefaultUnit(kony.flex.DP);
            var imgSortStatusAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortStatusAccount",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortStatusAccount = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortStatusAccount",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxSortStatusAccount.add(imgSortStatusAccount, lblSortStatusAccount);
            flxStatusAccount.add(lblStatusAccount, flxSortStatusAccount);
            var flxChanelAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChanelAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "6%",
                "zIndex": 1
            }, {}, {});
            flxChanelAccount.setDefaultUnit(kony.flex.DP);
            var lblChanelAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblChanelAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblChanel\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortChanelAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortChanelAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortChanelAccount.setDefaultUnit(kony.flex.DP);
            var imgSortChanelAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortChanelAccount",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortChanelAccount = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortChanelAccount",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortChanelAccount.add(imgSortChanelAccount, lblSortChanelAccount);
            flxChanelAccount.add(lblChanelAccount, flxSortChanelAccount);
            var flxIPAddressAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxIPAddressAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "6%",
                "zIndex": 1
            }, {}, {});
            flxIPAddressAccount.setDefaultUnit(kony.flex.DP);
            var lblIPAddressAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIPAddressAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblIPAddress\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortIPAddressAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortIPAddressAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortIPAddressAccount.setDefaultUnit(kony.flex.DP);
            var imgSortIPAddressAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortIPAddressAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortIPAddressAccount.add(imgSortIPAddressAccount);
            flxIPAddressAccount.add(lblIPAddressAccount, flxSortIPAddressAccount);
            var flxDeviceAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeviceAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "6%",
                "zIndex": 1
            }, {}, {});
            flxDeviceAccount.setDefaultUnit(kony.flex.DP);
            var lblDeviceAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeviceAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblDevice\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortDeviceAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDeviceAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDeviceAccount.setDefaultUnit(kony.flex.DP);
            var imgSortDeviceAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDeviceAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDeviceAccount.add(imgSortDeviceAccount);
            flxDeviceAccount.add(lblDeviceAccount, flxSortDeviceAccount);
            var flxOSAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOSAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "6%",
                "zIndex": 1
            }, {}, {});
            flxOSAccount.setDefaultUnit(kony.flex.DP);
            var lblOSAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOSAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblOS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortOSAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortOSAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortOSAccount.setDefaultUnit(kony.flex.DP);
            var imgSortOSAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortOSAccount",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortOSAccount = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortOSAccount",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortOSAccount.add(imgSortOSAccount, lblSortOSAccount);
            flxOSAccount.add(lblOSAccount, flxSortOSAccount);
            var flxReferenceIdAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxReferenceIdAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "6%",
                "zIndex": 1
            }, {}, {});
            flxReferenceIdAccount.setDefaultUnit(kony.flex.DP);
            var lblReferenceIdAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblReferenceIdAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblReferenceId\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortReferenceIdAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortReferenceIdAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortReferenceIdAccount.setDefaultUnit(kony.flex.DP);
            var imgSortReferenceIdAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortReferenceIdAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortReferenceIdAccount.add(imgSortReferenceIdAccount);
            flxReferenceIdAccount.add(lblReferenceIdAccount, flxSortReferenceIdAccount);
            var flxErrorCodeAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxErrorCodeAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "6%",
                "zIndex": 1
            }, {}, {});
            flxErrorCodeAccount.setDefaultUnit(kony.flex.DP);
            var lblErrorCodeAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorCodeAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblErrorCode\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortErrorCodeAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortErrorCodeAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortErrorCodeAccount.setDefaultUnit(kony.flex.DP);
            var CopyimgSortErrorCodeAll0h64a5659930e4c = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "CopyimgSortErrorCodeAll0h64a5659930e4c",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortErrorCodeAccount.add(CopyimgSortErrorCodeAll0h64a5659930e4c);
            flxErrorCodeAccount.add(lblErrorCodeAccount, flxSortErrorCodeAccount);
            var flxMFATypeAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMFATypeAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxMFATypeAccount.setDefaultUnit(kony.flex.DP);
            var lblMFATypeAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMFATypeAccount",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "TYPE OF MFA",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortMFATypeAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortMFATypeAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortMFATypeAccount.setDefaultUnit(kony.flex.DP);
            var imgSortMFATypeAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortMFATypeAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortMFATypeAccount.add(imgSortMFATypeAccount);
            flxMFATypeAccount.add(lblMFATypeAccount, flxSortMFATypeAccount);
            var flxBankNameAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBankNameAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {}, {});
            flxBankNameAccount.setDefaultUnit(kony.flex.DP);
            var lblBankNameAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBankNameAccount",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "BANK NAME",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortBankNameAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortBankNameAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortBankNameAccount.setDefaultUnit(kony.flex.DP);
            var imgSortPayeeNameAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortPayeeNameAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortBankNameAccount.add(imgSortPayeeNameAccount);
            flxBankNameAccount.add(lblBankNameAccount, flxSortBankNameAccount);
            var flxMaskedAccountNumberAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMaskedAccountNumberAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxMaskedAccountNumberAccount.setDefaultUnit(kony.flex.DP);
            var lblMaskedAccountNumberAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMaskedAccountNumberAccount",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "MASKED ACCOUNT NUMBER",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortMaskedAccountNumberAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortMaskedAccountNumberAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortMaskedAccountNumberAccount.setDefaultUnit(kony.flex.DP);
            var imgSortMaskedAccountNumberAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortMaskedAccountNumberAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortMaskedAccountNumberAccount.add(imgSortMaskedAccountNumberAccount);
            flxMaskedAccountNumberAccount.add(lblMaskedAccountNumberAccount, flxSortMaskedAccountNumberAccount);
            flxHeaderAccount.add(flxModuleNameAccount, flxLogTypeAccount, flxActivityTypeAccount, flxDescriptionAccount, flxDateAndTimeAccount, flxStatusAccount, flxChanelAccount, flxIPAddressAccount, flxDeviceAccount, flxOSAccount, flxReferenceIdAccount, flxErrorCodeAccount, flxMFATypeAccount, flxBankNameAccount, flxMaskedAccountNumberAccount);
            var lblUsersHeaderSeperatorAccount = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblUsersHeaderSeperatorAccount",
                "isVisible": true,
                "left": "35px",
                "right": 0,
                "skin": "sknLblHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "width": "2525dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailedResultHeaderAccount.add(flxHeaderAccount, lblUsersHeaderSeperatorAccount);
            var flxCustomerAdminLogHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60px",
                "id": "flxCustomerAdminLogHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0e8dde5f64bde4c",
                "top": "0dp",
                "width": "992dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerAdminLogHeader.setDefaultUnit(kony.flex.DP);
            var flxAdminHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxAdminHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAdminHeader.setDefaultUnit(kony.flex.DP);
            var flxAdminName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "16.50%",
                "zIndex": 1
            }, {}, {});
            flxAdminName.setDefaultUnit(kony.flex.DP);
            var lblAdminName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminName",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblAdminName\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortAdminName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAdminName",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAdminName.setDefaultUnit(kony.flex.DP);
            var imgSortAdminName = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAdminName",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAdminName.add(imgSortAdminName);
            flxAdminName.add(lblAdminName, flxSortAdminName);
            var flxAdminRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminRole",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15.50%",
                "zIndex": 1
            }, {}, {});
            flxAdminRole.setDefaultUnit(kony.flex.DP);
            var lblAdminRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ROLE\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAdminRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAdminRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAdminRole.setDefaultUnit(kony.flex.DP);
            var imgSortAdminRole = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAdminRole",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAdminRole = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortAdminRole",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAdminRole.add(imgSortAdminRole, lblSortAdminRole);
            flxAdminRole.add(lblAdminRole, flxSortAdminRole);
            var flxActivityTypeAdminRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActivityTypeAdminRole",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "12.50%",
                "zIndex": 1
            }, {}, {});
            flxActivityTypeAdminRole.setDefaultUnit(kony.flex.DP);
            var lblActivityTypeAdminRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActivityTypeAdminRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblActivityType\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortActivityTypeAdminRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortActivityTypeAdminRole",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortActivityTypeAdminRole.setDefaultUnit(kony.flex.DP);
            var imgSortActivityTypeAdminRole = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortActivityTypeAdminRole",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortActivityTypeAdminRole.add(imgSortActivityTypeAdminRole);
            flxActivityTypeAdminRole.add(lblActivityTypeAdminRole, flxSortActivityTypeAdminRole);
            var flxDescriptionAdminRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDescriptionAdminRole",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "36.50%",
                "zIndex": 1
            }, {}, {});
            flxDescriptionAdminRole.setDefaultUnit(kony.flex.DP);
            var lblDescriptionAdminRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDescriptionAdminRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortDescriptionAdminRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDescriptionAdminRole",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDescriptionAdminRole.setDefaultUnit(kony.flex.DP);
            var imgSortDescriptionAdminRole = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDescriptionAdminRole",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDescriptionAdminRole.add(imgSortDescriptionAdminRole);
            flxDescriptionAdminRole.add(lblDescriptionAdminRole, flxSortDescriptionAdminRole);
            var flxDateAndTimeAdminRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDateAndTimeAdminRole",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "17.50%",
                "zIndex": 1
            }, {}, {});
            flxDateAndTimeAdminRole.setDefaultUnit(kony.flex.DP);
            var lblDateAndTimeAdminRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDateAndTimeAdminRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblDateAndTime\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortDateAndTimeAdminRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDateAndTimeAdminRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDateAndTimeAdminRole.setDefaultUnit(kony.flex.DP);
            var imgSortDateAndTimeAdminRole = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortDateAndTimeAdminRole",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortDateAndTimeAdminRole = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortDateAndTimeAdminRole",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortDateAndTimeAdminRole.add(imgSortDateAndTimeAdminRole, lblSortDateAndTimeAdminRole);
            flxDateAndTimeAdminRole.add(lblDateAndTimeAdminRole, flxSortDateAndTimeAdminRole);
            flxAdminHeader.add(flxAdminName, flxAdminRole, flxActivityTypeAdminRole, flxDescriptionAdminRole, flxDateAndTimeAdminRole);
            var lblAdminHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblAdminHeaderSeperator",
                "isVisible": true,
                "left": "35px",
                "right": "0px",
                "skin": "sknLblHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerAdminLogHeader.add(flxAdminHeader, lblAdminHeaderSeperator);
            var flxCustomerLogsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxCustomerLogsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "top": "0dp",
                "width": "3120dp"
            }, {}, {});
            flxCustomerLogsHeader.setDefaultUnit(kony.flex.DP);
            var flxLogs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLogs.setDefaultUnit(kony.flex.DP);
            var flxcusEvent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxcusEvent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxcusEvent.setDefaultUnit(kony.flex.DP);
            var lblCusEvent = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCusEvent",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "MODULE",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxcusEvent.add(lblCusEvent);
            var flxCusEventSubType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCusEventSubType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxCusEventSubType.setDefaultUnit(kony.flex.DP);
            var lblEventSubType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEventSubType",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblActivityType\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCusEventSubType.add(lblEventSubType);
            var flxAdminsName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminsName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxAdminsName.setDefaultUnit(kony.flex.DP);
            var lblAdminsName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminsName",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.employeeUseName\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdminsName.add(lblAdminsName);
            var flxAdminsRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminsRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxAdminsRole.setDefaultUnit(kony.flex.DP);
            var lblAdminsRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminsRole",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.employeeRole\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdminsRole.add(lblAdminsRole);
            var flxDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxDate.setDefaultUnit(kony.flex.DP);
            var lblDate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDate",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblDateAndTime\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFilterDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFilterDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxFilterDateAndTime.setDefaultUnit(kony.flex.DP);
            var imgFilterDateAndTime = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgFilterDateAndTime",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFilterDateAndTime = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblFilterDateAndTime",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFilterDateAndTime.add(imgFilterDateAndTime, lblFilterDateAndTime);
            flxDate.add(lblDate, flxFilterDateAndTime);
            var flxFromAccountNum = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxFromAccountNum",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxFromAccountNum.setDefaultUnit(kony.flex.DP);
            var lblFromAccountNum = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFromAccountNum",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "FROM ACCOUNT",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFromAccountNum.add(lblFromAccountNum);
            var flxToAccountNum = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxToAccountNum",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxToAccountNum.setDefaultUnit(kony.flex.DP);
            var lblToAccountNum = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblToAccountNum",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "TO ACCOUNT",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToAccountNum.add(lblToAccountNum);
            var flxAmt = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAmt",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxAmt.setDefaultUnit(kony.flex.DP);
            var lblAmnt = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmnt",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "AMOUNT",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAmt.add(lblAmnt);
            var flxCustCurrency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustCurrency",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxCustCurrency.setDefaultUnit(kony.flex.DP);
            var lblCustCurrency = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustCurrency",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "CURRENCY",
                "top": 0,
                "width": "200dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustCurrency.add(lblCustCurrency);
            var flxRefNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRefNumber",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxRefNumber.setDefaultUnit(kony.flex.DP);
            var lblRefNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRefNumber",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "REFERENCE NUMBER",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRefNumber.add(lblRefNumber);
            var flxMaskedCardNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMaskedCardNo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxMaskedCardNo.setDefaultUnit(kony.flex.DP);
            var lblMaskedCardNo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMaskedCardNo",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "MASKED CARD NUMBER",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaskedCardNo.add(lblMaskedCardNo);
            var flxPayeesName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPayeesName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxPayeesName.setDefaultUnit(kony.flex.DP);
            var lblPayeesName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPayeesName",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "PAYEE NAME / RECIPIENT NAME",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPayeesName.add(lblPayeesName);
            var flxPayeesId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPayeesId",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxPayeesId.setDefaultUnit(kony.flex.DP);
            var lblPayeesId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPayeesId",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "PAYEE ID",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPayeesId.add(lblPayeesId);
            var flxPersonsId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPersonsId",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxPersonsId.setDefaultUnit(kony.flex.DP);
            var lblPersonsId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPersonsId",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "PERSON ID",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPersonsId.add(lblPersonsId);
            var flxAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxAccount.setDefaultUnit(kony.flex.DP);
            var lblAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccount",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "ACCOUNT / RELATIONSHIP NUMBER",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccount.add(lblAccount);
            var flxLogStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxLogStatus.setDefaultUnit(kony.flex.DP);
            var lblLogStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogStatus",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFilterStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFilterStatus",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxFilterStatus.setDefaultUnit(kony.flex.DP);
            var imgFilterStatus = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgFilterStatus",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFilterStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblFilterStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxFilterStatus.add(imgFilterStatus, lblFilterStatus);
            flxLogStatus.add(lblLogStatus, flxFilterStatus);
            var flxLogMFAType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogMFAType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxLogMFAType.setDefaultUnit(kony.flex.DP);
            var lblLogMFAType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogMFAType",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "MFA TYPE",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLogMFAType.add(lblLogMFAType);
            var flxLogMFAKey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogMFAKey",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxLogMFAKey.setDefaultUnit(kony.flex.DP);
            var lblLogMFAKey = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogMFAKey",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "MFA SERVICE KEY",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLogMFAKey.add(lblLogMFAKey);
            var flxMFAState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMFAState",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxMFAState.setDefaultUnit(kony.flex.DP);
            var lblMFAState = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMFAState",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "MFA STATE",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMFAState.add(lblMFAState);
            var flxLogChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogChannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxLogChannel.setDefaultUnit(kony.flex.DP);
            var lblLogChannel = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogChannel",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLogChannel.add(lblLogChannel);
            var flxDeviceBrowser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeviceBrowser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxDeviceBrowser.setDefaultUnit(kony.flex.DP);
            var lblDeviceBrowser = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeviceBrowser",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DEVICEORBROWSER\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeviceBrowser.add(lblDeviceBrowser);
            var flxLogOS = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogOS",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxLogOS.setDefaultUnit(kony.flex.DP);
            var lblLogOS = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogOS",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.OS\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLogOS.add(lblLogOS);
            var flxDeviceID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeviceID",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxDeviceID.setDefaultUnit(kony.flex.DP);
            var lblDeviceId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeviceId",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DEVICEID\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeviceID.add(lblDeviceId);
            var flxLogIPAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogIPAddress",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxLogIPAddress.setDefaultUnit(kony.flex.DP);
            var lblLogIPAddress = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogIPAddress",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblIPAddress\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLogIPAddress.add(lblLogIPAddress);
            var flxOtherInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOtherInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxOtherInfo.setDefaultUnit(kony.flex.DP);
            var lblOtherInfo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOtherInfo",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "OTHER INFO",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOtherInfo.add(lblOtherInfo);
            flxLogs.add(flxcusEvent, flxCusEventSubType, flxAdminsName, flxAdminsRole, flxDate, flxFromAccountNum, flxToAccountNum, flxAmt, flxCustCurrency, flxRefNumber, flxMaskedCardNo, flxPayeesName, flxPayeesId, flxPersonsId, flxAccount, flxLogStatus, flxLogMFAType, flxLogMFAKey, flxMFAState, flxLogChannel, flxDeviceBrowser, flxLogOS, flxDeviceID, flxLogIPAddress, flxOtherInfo);
            var lblSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "35dp",
                "skin": "sknLblTableHeaderLine",
                "text": ".",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerLogsHeader.add(flxLogs, lblSeperator);
            var flxDetailedResultSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxDetailedResultSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "3120px",
                "zIndex": 1
            }, {}, {});
            flxDetailedResultSegment.setDefaultUnit(kony.flex.DP);
            var segDetailedResult = new com.adminConsole.common.listingSegmentLogs({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "segDetailedResult",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "flxContextualMenu": {
                        "isVisible": false
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentLogs": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDynamicLogsScroll = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "240dp",
                "horizontalScrollIndicator": true,
                "id": "flxDynamicLogsScroll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxDynamicLogsScroll.setDefaultUnit(kony.flex.DP);
            var segDynamicCustomerLogs = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }],
                "groupCells": false,
                "height": "1024dp",
                "id": "segDynamicCustomerLogs",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxDynamicLogs",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxDynamicLogs": "flxDynamicLogs",
                    "flxLogs": "flxLogs",
                    "flxOtherInfo": "flxOtherInfo",
                    "flxStatus": "flxStatus",
                    "lblAccount": "lblAccount",
                    "lblAdminName": "lblAdminName",
                    "lblAdminRole": "lblAdminRole",
                    "lblAmount": "lblAmount",
                    "lblChannel": "lblChannel",
                    "lblCurrency": "lblCurrency",
                    "lblDate": "lblDate",
                    "lblDeviceBrowser": "lblDeviceBrowser",
                    "lblDeviceId": "lblDeviceId",
                    "lblEvent": "lblEvent",
                    "lblEventSubType": "lblEventSubType",
                    "lblFromAccount": "lblFromAccount",
                    "lblIPAddress": "lblIPAddress",
                    "lblIconStatus": "lblIconStatus",
                    "lblMFAServiceKey": "lblMFAServiceKey",
                    "lblMFAState": "lblMFAState",
                    "lblMFAType": "lblMFAType",
                    "lblMaskedCardNo": "lblMaskedCardNo",
                    "lblOs": "lblOs",
                    "lblOtherInfo": "lblOtherInfo",
                    "lblPayeeId": "lblPayeeId",
                    "lblPayeeName": "lblPayeeName",
                    "lblPersonId": "lblPersonId",
                    "lblRefNumber": "lblRefNumber",
                    "lblSeperator": "lblSeperator",
                    "lblStatus": "lblStatus",
                    "lblToAccount": "lblToAccount"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDynamicLogsScroll.add(segDynamicCustomerLogs);
            flxDetailedResultSegment.add(segDetailedResult, flxDynamicLogsScroll);
            flxInnerResults.add(flxDetailedResultHeader, flxDetailedResultHeaderAll, flxDetailedResultHeaderPayee, flxDetailedResultHeaderAccount, flxCustomerAdminLogHeader, flxCustomerLogsHeader, flxDetailedResultSegment);
            var flxStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "760px",
                "skin": "slFbox",
                "top": "10px",
                "width": "14%",
                "zIndex": 500
            }, {}, {});
            flxStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Posted"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Pending"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Failed"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilter.add(statusFilterMenu);
            flxResults.add(flxInnerResults, flxStatusFilter);
            flxCustomerContent.add(flxHeader3, flxActivityType3, flxResults);
            var flxPaginationCustomerLog = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35dp",
                "clipBounds": true,
                "height": "70px",
                "id": "flxPaginationCustomerLog",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "CopyCopyCopyslFbox0d090d27f416e48",
                "top": "0dp",
                "width": "992dp",
                "zIndex": 1
            }, {}, {});
            flxPaginationCustomerLog.setDefaultUnit(kony.flex.DP);
            var pagination0 = new com.adminConsole.common.pagination1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "220dp",
                "id": "pagination0",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "pagination1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "220dp",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPaginationCustomerLog.add(pagination0);
            flxCustomerDetailedResults.add(flxCustomerContent, flxPaginationCustomerLog);
            var flxTransactionLog = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": false,
                "id": "flxTransactionLog",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknheaderLogs",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {}, {});
            flxTransactionLog.setDefaultUnit(kony.flex.DP);
            var flxTransactionFiltersAndHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60px",
                "id": "flxTransactionFiltersAndHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactionFiltersAndHeaders.setDefaultUnit(kony.flex.DP);
            var flxTransactionFilterIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxTransactionFilterIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30dp",
                "skin": "slFbox",
                "top": "12px",
                "width": "25dp"
            }, {}, {});
            flxTransactionFilterIcon.setDefaultUnit(kony.flex.DP);
            var lblTransactionFilterIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "lblTransactionFilterIcon",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknIconBlack25px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTransactionFilterIcon.add(lblTransactionFilterIcon);
            var flxTransactionSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxTransactionSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30px",
                "skin": "sknflxd5d9ddop100",
                "top": "10px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxTransactionSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconTransactionSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconTransactionSearch",
                "isVisible": true,
                "right": "12dp",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxTransactionSearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxTransactionSearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "5px",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.searchTransactionalLogs\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearTransactionSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearTransactionSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "25dp",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearTransactionSearch.setDefaultUnit(kony.flex.DP);
            var fontIconTransactionSearchClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconTransactionSearchClose",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearTransactionSearch.add(fontIconTransactionSearchClose);
            flxTransactionSearchContainer.add(fontIconTransactionSearch, tbxTransactionSearch, flxClearTransactionSearch);
            flxTransactionFiltersAndHeaders.add(flxTransactionFilterIcon, flxTransactionSearchContainer);
            var flxTransactionResults = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "85%",
                "horizontalScrollIndicator": true,
                "id": "flxTransactionResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
                "skin": "slFSbox",
                "top": "60dp",
                "verticalScrollIndicator": true,
                "zIndex": 70
            }, {}, {});
            flxTransactionResults.setDefaultUnit(kony.flex.DP);
            var flxInnerTransactionResults = new kony.ui.FlexContainer({
                "clipBounds": false,
                "height": "100%",
                "id": "flxInnerTransactionResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxInnerTransactionResults.setDefaultUnit(kony.flex.DP);
            var flxTransactionResultHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60px",
                "id": "flxTransactionResultHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "CopyCopyslFbox0e8dde5f64bde4c",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactionResultHeader.setDefaultUnit(kony.flex.DP);
            var flxTransactionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxTransactionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactionHeader.setDefaultUnit(kony.flex.DP);
            var flxModule = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxModule",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxModule.setDefaultUnit(kony.flex.DP);
            var lblModule = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblModule",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "MODULE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortModule = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortModule",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortModule.setDefaultUnit(kony.flex.DP);
            var lblSortModule = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortModule",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortModule.add(lblSortModule);
            flxModule.add(lblModule, flxSortModule);
            var flxTransactionLogType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionLogType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionLogType.setDefaultUnit(kony.flex.DP);
            var lblTransactionLogType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionLogType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "ACTIVITY TYPE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionLogType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionLogType",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionLogType.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionLogType = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgSortTransactionLogType",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionLogType.add(imgSortTransactionLogType);
            flxTransactionLogType.add(lblTransactionLogType, flxSortTransactionLogType);
            var flxCustomerId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustomerId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxCustomerId.setDefaultUnit(kony.flex.DP);
            var lblCustomerId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustomerId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "CUSTOMER ID",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortCustomerId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortCustomerId",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortCustomerId.setDefaultUnit(kony.flex.DP);
            var lblSortCustomerId = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortCustomerId",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortCustomerId.add(lblSortCustomerId);
            flxCustomerId.add(lblCustomerId, flxSortCustomerId);
            var flxUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUserName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxUserName.setDefaultUnit(kony.flex.DP);
            var lblUserName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUserName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.customerUserName\")",
                "top": 0,
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortUserName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortUserName.setDefaultUnit(kony.flex.DP);
            var lblSortUserName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortUserName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortUserName.add(lblSortUserName);
            flxUserName.add(lblUserName, flxSortUserName);
            var flxTransactionDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionDate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionDate.setDefaultUnit(kony.flex.DP);
            var lblTransactionDate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionDate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "DATE & TIME",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortTransactionDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionDate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionDate.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionDate = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortTransactionDate",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionDate.add(imgSortTransactionDate);
            flxTransactionDate.add(lblTransactionDate, flxSortTransactionDate);
            var flxFromAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxFromAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxFromAccount.setDefaultUnit(kony.flex.DP);
            var lblFromAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFromAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblFromAccount\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortFromAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortFromAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortFromAccount.setDefaultUnit(kony.flex.DP);
            var imgSortFromAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortFromAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortFromAccount.add(imgSortFromAccount);
            flxFromAccount.add(lblFromAccount, flxSortFromAccount);
            var flxToAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxToAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxToAccount.setDefaultUnit(kony.flex.DP);
            var lblToAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblToAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblToAccount\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortToAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortToAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortToAccount.setDefaultUnit(kony.flex.DP);
            var imgSortToAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortToAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortToAccount.add(imgSortToAccount);
            flxToAccount.add(lblToAccount, flxSortToAccount);
            var flxAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAmount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxAmount.setDefaultUnit(kony.flex.DP);
            var lblAmount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblTransactionAmount\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAmount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAmount.setDefaultUnit(kony.flex.DP);
            var lblSortAmount = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortAmount",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAmount.add(lblSortAmount);
            flxAmount.add(lblAmount, flxSortAmount);
            var flxCurrency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCurrency",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxCurrency.setDefaultUnit(kony.flex.DP);
            var lblCurrency = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCurrency",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCurrency\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortCurrency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortCurrency",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortCurrency.setDefaultUnit(kony.flex.DP);
            var lblSortCurrency = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortCurrency",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortCurrency.add(lblSortCurrency);
            flxCurrency.add(lblCurrency, flxSortCurrency);
            var flxReferenceNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxReferenceNumber",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxReferenceNumber.setDefaultUnit(kony.flex.DP);
            var lblReferenceNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblReferenceNumber",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "REFERENCE NUMBER",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortReferenceNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortReferenceNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortReferenceNumber.setDefaultUnit(kony.flex.DP);
            var lblSortReferenceNumber = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortReferenceNumber",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortReferenceNumber.add(lblSortReferenceNumber);
            flxReferenceNumber.add(lblReferenceNumber, flxSortReferenceNumber);
            var flxPayeeName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPayeeName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxPayeeName.setDefaultUnit(kony.flex.DP);
            var lblPayeeName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPayeeName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblPayeeName\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortPayeeName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortPayeeName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortPayeeName.setDefaultUnit(kony.flex.DP);
            var lblSortPayeeName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortPayeeName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortPayeeName.add(lblSortPayeeName);
            flxPayeeName.add(lblPayeeName, flxSortPayeeName);
            var flxPayeeId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPayeeId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxPayeeId.setDefaultUnit(kony.flex.DP);
            var lblPayeeId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPayeeId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "PAYEE ID",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            flxPayeeId.add(lblPayeeId);
            var flxPersonId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPersonId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxPersonId.setDefaultUnit(kony.flex.DP);
            var lblPersonId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPersonId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "PERSON ID",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            flxPersonId.add(lblPersonId);
            var flxTransactionStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionStatus.setDefaultUnit(kony.flex.DP);
            var lblTransactionStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionStatus",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionStatus.setDefaultUnit(kony.flex.DP);
            var lblSortTransactionStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortTransactionStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionStatus.add(lblSortTransactionStatus);
            flxTransactionStatus.add(lblTransactionStatus, flxSortTransactionStatus);
            var flxTransactionMFAType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionMFAType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionMFAType.setDefaultUnit(kony.flex.DP);
            var lblTransactionMFAType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionMFAType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "MFA TYPE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionMFAType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionMFAType",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionMFAType.setDefaultUnit(kony.flex.DP);
            var lblSortTransactionMFAType = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortTransactionMFAType",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionMFAType.add(lblSortTransactionMFAType);
            flxTransactionMFAType.add(lblTransactionMFAType, flxSortTransactionMFAType);
            var flxTransactionMFAServiceKey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionMFAServiceKey",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionMFAServiceKey.setDefaultUnit(kony.flex.DP);
            var lblTransactionMFAServiceKey = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionMFAServiceKey",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "MFA SERVICE KEY",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionMFAServiceKey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionMFAServiceKey",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionMFAServiceKey.setDefaultUnit(kony.flex.DP);
            var lblSortTransactionMFAServiceKey = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortTransactionMFAServiceKey",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionMFAServiceKey.add(lblSortTransactionMFAServiceKey);
            flxTransactionMFAServiceKey.add(lblTransactionMFAServiceKey, flxSortTransactionMFAServiceKey);
            var flxTransactionMFAState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionMFAState",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionMFAState.setDefaultUnit(kony.flex.DP);
            var lblTransactionMFAState = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionMFAState",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "MFA STATE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionMFAState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionMFAState",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionMFAState.setDefaultUnit(kony.flex.DP);
            var lblSortTransactionMFAState = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortTransactionMFAState",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionMFAState.add(lblSortTransactionMFAState);
            flxTransactionMFAState.add(lblTransactionMFAState, flxSortTransactionMFAState);
            var flxChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChannel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxChannel.setDefaultUnit(kony.flex.DP);
            var lblChannel = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblChannel",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "CHANNEL",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortChannel",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortChannel.setDefaultUnit(kony.flex.DP);
            var imgSortChannel = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortChannel",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortChannel.add(imgSortChannel);
            flxChannel.add(lblChannel, flxSortChannel);
            var flxTransactionDevice = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionDevice",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionDevice.setDefaultUnit(kony.flex.DP);
            var lblTransactionDevice = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionDevice",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "DEVICE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortTransactionDevice = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionDevice",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionDevice.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionDevice = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortTransactionDevice",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionDevice.add(imgSortTransactionDevice);
            flxTransactionDevice.add(lblTransactionDevice, flxSortTransactionDevice);
            var flxTransactionOS = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionOS",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionOS.setDefaultUnit(kony.flex.DP);
            var lblTransactionOS = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionOS",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "OS",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortTransactionOS = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionOS",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionOS.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionOS = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortTransactionOS",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionOS.add(imgSortTransactionOS);
            flxTransactionOS.add(lblTransactionOS, flxSortTransactionOS);
            var flxTransactionDeviceId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionDeviceId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionDeviceId.setDefaultUnit(kony.flex.DP);
            var lblTransactionDeviceId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionDeviceId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "DEVICE ID",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortTransactionDeviceId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionDeviceId",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionDeviceId.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionDeviceId = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortTransactionDeviceId",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionDeviceId.add(imgSortTransactionDeviceId);
            flxTransactionDeviceId.add(lblTransactionDeviceId, flxSortTransactionDeviceId);
            var flxTransactionIPAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionIPAddress",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionIPAddress.setDefaultUnit(kony.flex.DP);
            var lblTransactionIPAddress = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionIPAddress",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "IP ADDRESS",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortTransactionIPAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionIPAddress",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionIPAddress.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionIPAddress = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortTransactionIPAddress",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionIPAddress.add(imgSortTransactionIPAddress);
            flxTransactionIPAddress.add(lblTransactionIPAddress, flxSortTransactionIPAddress);
            var flxTransactionErrorCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionErrorCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionErrorCode.setDefaultUnit(kony.flex.DP);
            var lblTransactionErrorCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionErrorCode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "OTHER INFO",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionErrorCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionErrorCode",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionErrorCode.setDefaultUnit(kony.flex.DP);
            var lblSortTransactionErrorCode = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortTransactionErrorCode",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionErrorCode.add(lblSortTransactionErrorCode);
            flxTransactionErrorCode.add(lblTransactionErrorCode, flxSortTransactionErrorCode);
            flxTransactionHeader.add(flxModule, flxTransactionLogType, flxCustomerId, flxUserName, flxTransactionDate, flxFromAccount, flxToAccount, flxAmount, flxCurrency, flxReferenceNumber, flxPayeeName, flxPayeeId, flxPersonId, flxTransactionStatus, flxTransactionMFAType, flxTransactionMFAServiceKey, flxTransactionMFAState, flxChannel, flxTransactionDevice, flxTransactionOS, flxTransactionDeviceId, flxTransactionIPAddress, flxTransactionErrorCode);
            var lblTransactionHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblTransactionHeaderSeperator",
                "isVisible": true,
                "left": "35px",
                "right": "0px",
                "skin": "sknLblHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTransactionResultHeader.add(flxTransactionHeader, lblTransactionHeaderSeperator);
            var flxSegResults = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "85%",
                "horizontalScrollIndicator": true,
                "id": "flxSegResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSegResults.setDefaultUnit(kony.flex.DP);
            var segTransactionresult = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }],
                "groupCells": false,
                "id": "segTransactionresult",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxTransactionLogs",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxStatus": "flxStatus",
                    "flxTransactionLogs": "flxTransactionLogs",
                    "flxTransactionWrapper": "flxTransactionWrapper",
                    "imgStatus": "imgStatus",
                    "lblAmount": "lblAmount",
                    "lblChannel": "lblChannel",
                    "lblCurrency": "lblCurrency",
                    "lblCustomerId": "lblCustomerId",
                    "lblDevice": "lblDevice",
                    "lblDeviceId": "lblDeviceId",
                    "lblErrorCode": "lblErrorCode",
                    "lblFromAccount": "lblFromAccount",
                    "lblIPAddress": "lblIPAddress",
                    "lblIconStatus": "lblIconStatus",
                    "lblLogType": "lblLogType",
                    "lblMFAServiceType": "lblMFAServiceType",
                    "lblMFAState": "lblMFAState",
                    "lblMFAType": "lblMFAType",
                    "lblModule": "lblModule",
                    "lblOS": "lblOS",
                    "lblPayeeId": "lblPayeeId",
                    "lblPayeeName": "lblPayeeName",
                    "lblPersonId": "lblPersonId",
                    "lblReferenceNumber": "lblReferenceNumber",
                    "lblSeperator": "lblSeperator",
                    "lblStatus": "lblStatus",
                    "lblToAccount": "lblToAccount",
                    "lblTransactionDate": "lblTransactionDate",
                    "lblUserName": "lblUserName"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegResults.add(segTransactionresult);
            flxInnerTransactionResults.add(flxTransactionResultHeader, flxSegResults);
            var flxStatusFilterTransaction = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxStatusFilterTransaction",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "760px",
                "skin": "slFbox",
                "top": "10px",
                "width": "13%",
                "zIndex": 500
            }, {}, {});
            flxStatusFilterTransaction.setDefaultUnit(kony.flex.DP);
            var statusFilterMenuTransaction = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "statusFilterMenuTransaction",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Posted"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Pending"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Failed"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilterTransaction.add(statusFilterMenuTransaction);
            flxTransactionResults.add(flxInnerTransactionResults, flxStatusFilterTransaction);
            flxTransactionLog.add(flxTransactionFiltersAndHeaders, flxTransactionResults);
            var flxAdminConsoleLog = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": false,
                "id": "flxAdminConsoleLog",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknheaderLogs",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxAdminConsoleLog.setDefaultUnit(kony.flex.DP);
            var flxAdminConsoleFiltersAndHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "80dp",
                "id": "flxAdminConsoleFiltersAndHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleFiltersAndHeaders.setDefaultUnit(kony.flex.DP);
            var flxAdminConsoleFilterIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxAdminConsoleFilterIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30dp",
                "skin": "slFbox",
                "top": "12px",
                "width": "25dp"
            }, {}, {});
            flxAdminConsoleFilterIcon.setDefaultUnit(kony.flex.DP);
            var lblAdminConsoleFilterIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "lblAdminConsoleFilterIcon",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknIconBlack25px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdminConsoleFilterIcon.add(lblAdminConsoleFilterIcon);
            var flxAdminConsoleSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxAdminConsoleSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30px",
                "skin": "sknflxd5d9ddop100",
                "top": "10px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearch",
                "isVisible": true,
                "right": "12dp",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxAdminConsoleSearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxAdminConsoleSearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "5px",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.searchAdminConsoleLogs\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearAdminConsoleSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearAdminConsoleSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "25dp",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearAdminConsoleSearch.setDefaultUnit(kony.flex.DP);
            var fontIconAdminConsoleClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconAdminConsoleClose",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearAdminConsoleSearch.add(fontIconAdminConsoleClose);
            flxAdminConsoleSearchContainer.add(fontIconSearch, tbxAdminConsoleSearch, flxClearAdminConsoleSearch);
            flxAdminConsoleFiltersAndHeaders.add(flxAdminConsoleFilterIcon, flxAdminConsoleSearchContainer);
            var flxAdminConsoleResults = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "290px",
                "horizontalScrollIndicator": true,
                "id": "flxAdminConsoleResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
                "skin": "slFSbox",
                "top": "80dp",
                "verticalScrollIndicator": true,
                "width": "992dp"
            }, {}, {});
            flxAdminConsoleResults.setDefaultUnit(kony.flex.DP);
            var flxInnerAdminConsoleResults = new kony.ui.FlexContainer({
                "clipBounds": false,
                "height": "100%",
                "id": "flxInnerAdminConsoleResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "1027dp",
                "zIndex": 1
            }, {}, {});
            flxInnerAdminConsoleResults.setDefaultUnit(kony.flex.DP);
            var flxAdminConsoleResultHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60px",
                "id": "flxAdminConsoleResultHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "CopyCopyslFbox0e8dde5f64bde4c",
                "top": "0dp",
                "width": 1254,
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleResultHeader.setDefaultUnit(kony.flex.DP);
            var flxAdminConsoleHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxAdminConsoleHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "width": "1200dp",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleHeader.setDefaultUnit(kony.flex.DP);
            var flxEvent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEvent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxEvent.setDefaultUnit(kony.flex.DP);
            var lblEvent = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEvent",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblEvent\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fllxSortEvent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "fllxSortEvent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            fllxSortEvent.setDefaultUnit(kony.flex.DP);
            var imgSortEvent = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortEvent",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortEvent = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortEvent",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            fllxSortEvent.add(imgSortEvent, lblSortEvent);
            flxEvent.add(lblEvent, fllxSortEvent);
            var flxAdminConsoleUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminConsoleUserName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "9%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleUserName.setDefaultUnit(kony.flex.DP);
            var lblAdminConsoleUserName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminConsoleUserName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.employeeUseName\")",
                "top": 0,
                "width": "65%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAdminConsoleUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAdminConsoleUserName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAdminConsoleUserName.setDefaultUnit(kony.flex.DP);
            var imgSortAdminConsoleUserName = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAdminConsoleUserName",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAdminConsoleUserName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortAdminConsoleUserName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAdminConsoleUserName.add(imgSortAdminConsoleUserName, lblSortAdminConsoleUserName);
            flxAdminConsoleUserName.add(lblAdminConsoleUserName, flxSortAdminConsoleUserName);
            var flxUserRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUserRole",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxUserRole.setDefaultUnit(kony.flex.DP);
            var lblUserRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUserRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblUserRole\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortUserRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortUserRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortUserRole.setDefaultUnit(kony.flex.DP);
            var imgSortUserRole = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortUserRole",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortUserRole = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortUserRole",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortUserRole.add(imgSortUserRole, lblSortUserRole);
            flxUserRole.add(lblUserRole, flxSortUserRole);
            var flxAdminConsoleModuleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminConsoleModuleName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleModuleName.setDefaultUnit(kony.flex.DP);
            var lblAdminConsoleModuleName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminConsoleModuleName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblAdminConsoleModuleName\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAdminConsoleModuleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAdminConsoleModuleName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAdminConsoleModuleName.setDefaultUnit(kony.flex.DP);
            var imgSortAdminConsoleModuleName = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAdminConsoleModuleName",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAdminConsoleModuleName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortAdminConsoleModuleName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAdminConsoleModuleName.add(imgSortAdminConsoleModuleName, lblSortAdminConsoleModuleName);
            flxAdminConsoleModuleName.add(lblAdminConsoleModuleName, flxSortAdminConsoleModuleName);
            var flxAdminConsoleStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminConsoleStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "9%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleStatus.setDefaultUnit(kony.flex.DP);
            var lblAdminConsoleStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminConsoleStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAdminConsoleStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAdminConsoleStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAdminConsoleStatus.setDefaultUnit(kony.flex.DP);
            var imgSortAdminConsoleStatus = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAdminConsoleStatus",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAdminConsoleStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortAdminConsoleStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAdminConsoleStatus.add(imgSortAdminConsoleStatus, lblSortAdminConsoleStatus);
            flxAdminConsoleStatus.add(lblAdminConsoleStatus, flxSortAdminConsoleStatus);
            var flxAdminConsoleDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminConsoleDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "13%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleDateAndTime.setDefaultUnit(kony.flex.DP);
            var lblAdminConsoleDateAndTime = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminConsoleDateAndTime",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblDateAndTime\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAdminConsoleDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAdminConsoleDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAdminConsoleDateAndTime.setDefaultUnit(kony.flex.DP);
            var imgSortAdminConsoleDateAndTime = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAdminConsoleDateAndTime",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAdminConsoleDateAndTime = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortAdminConsoleDateAndTime",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAdminConsoleDateAndTime.add(imgSortAdminConsoleDateAndTime, lblSortAdminConsoleDateAndTime);
            flxAdminConsoleDateAndTime.add(lblAdminConsoleDateAndTime, flxSortAdminConsoleDateAndTime);
            var flxAdminConsoleDesc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminConsoleDesc",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleDesc.setDefaultUnit(kony.flex.DP);
            var lblAdminConsoleDesc = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminConsoleDesc",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortAdminConsoleDesc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAdminConsoleDesc",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAdminConsoleDesc.setDefaultUnit(kony.flex.DP);
            var imgSortAdminConsoleDesc = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAdminConsoleDesc",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAdminConsoleDesc.add(imgSortAdminConsoleDesc);
            flxAdminConsoleDesc.add(lblAdminConsoleDesc, flxSortAdminConsoleDesc);
            flxAdminConsoleHeader.add(flxEvent, flxAdminConsoleUserName, flxUserRole, flxAdminConsoleModuleName, flxAdminConsoleStatus, flxAdminConsoleDateAndTime, flxAdminConsoleDesc);
            var lblAdminConsoleHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblAdminConsoleHeaderSeperator",
                "isVisible": true,
                "left": "35px",
                "right": "35px",
                "skin": "sknLblHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdminConsoleResultHeader.add(flxAdminConsoleHeader, lblAdminConsoleHeaderSeperator);
            var flxAdminConsoleResultSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxAdminConsoleResultSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "1254dp",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleResultSegment.setDefaultUnit(kony.flex.DP);
            var segAdminConsoleResult = new com.adminConsole.common.listingSegmentLogs({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "segAdminConsoleResult",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "flxContextualMenu": {
                        "isVisible": false
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentLogs": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAdminConsoleResultSegment.add(segAdminConsoleResult);
            flxInnerAdminConsoleResults.add(flxAdminConsoleResultHeader, flxAdminConsoleResultSegment);
            var flxStatusFilterAdmin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxStatusFilterAdmin",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "760px",
                "skin": "slFbox",
                "top": "10px",
                "width": "14.50%",
                "zIndex": 500
            }, {}, {});
            flxStatusFilterAdmin.setDefaultUnit(kony.flex.DP);
            var statusFilterMenuAdmin = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "statusFilterMenuAdmin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Posted"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Pending"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Failed"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilterAdmin.add(statusFilterMenuAdmin);
            flxAdminConsoleResults.add(flxInnerAdminConsoleResults, flxStatusFilterAdmin);
            var flxPaginationAdminConsoleLog = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35dp",
                "clipBounds": true,
                "height": "70px",
                "id": "flxPaginationAdminConsoleLog",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "CopyCopyCopyslFbox0d090d27f416e48",
                "top": "0dp",
                "width": "992dp",
                "zIndex": 1
            }, {}, {});
            flxPaginationAdminConsoleLog.setDefaultUnit(kony.flex.DP);
            var pagination1 = new com.adminConsole.common.pagination1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "220dp",
                "id": "pagination1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "pagination1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "220dp",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPaginationAdminConsoleLog.add(pagination1);
            flxAdminConsoleLog.add(flxAdminConsoleFiltersAndHeaders, flxAdminConsoleResults, flxPaginationAdminConsoleLog);
            var flxDropDownDetail1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "380px",
                "id": "flxDropDownDetail1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "440dp",
                "isModalContainer": false,
                "skin": "skntxtbxNormald7d9e0",
                "top": "185px",
                "width": "200dp",
                "zIndex": 200
            }, {}, {
                "hoverSkin": "skntxtbxhover11abeb"
            });
            flxDropDownDetail1.setDefaultUnit(kony.flex.DP);
            var selectDate = new com.adminConsole.selectDate.selectDate({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "selectDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnApply": {
                        "centerX": "viz.val_cleared",
                        "right": "5%",
                        "top": undefined
                    },
                    "calFrom": {
                        "centerX": "viz.val_cleared",
                        "left": "10px",
                        "right": "viz.val_cleared",
                        "width": undefined
                    },
                    "calTo": {
                        "centerX": "viz.val_cleared",
                        "left": "10px",
                        "right": undefined,
                        "width": undefined
                    },
                    "flxDate": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "width": "100%"
                    },
                    "flxDatepicker": {
                        "bottom": undefined,
                        "top": undefined
                    },
                    "flxDays": {
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "selectDate": {
                        "height": "100%",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDropDownDetail1.add(selectDate);
            var flxAmountDropDown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "360px",
                "id": "flxAmountDropDown",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "250dp",
                "isModalContainer": false,
                "skin": "skntxtbxNormald7d9e0",
                "top": "85px",
                "width": "160px",
                "zIndex": 210
            }, {}, {
                "hoverSkin": "skntxtbxhover11abeb"
            });
            flxAmountDropDown.setDefaultUnit(kony.flex.DP);
            var selectAmount = new com.adminConsole.logs.selectAmount({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "99%",
                "id": "selectAmount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxAmount": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxAmountpicker": {
                        "height": "210px"
                    },
                    "selectAmount": {
                        "height": "99%",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAmountDropDown.add(selectAmount);
            var flxNoResultsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "250dp",
                "id": "flxNoResultsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "992dp",
                "zIndex": 15
            }, {}, {});
            flxNoResultsFound.setDefaultUnit(kony.flex.DP);
            var rtxNorecords = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNorecords",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNorecords\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultsFound.add(rtxNorecords);
            var flxFiltersAndHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxFiltersAndHeaders",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "55dp",
                "isModalContainer": false,
                "right": "55dp",
                "skin": "slFbox",
                "top": "145dp",
                "zIndex": 20
            }, {}, {});
            flxFiltersAndHeaders.setDefaultUnit(kony.flex.DP);
            var flxArrowImageCustomer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12dp",
                "id": "flxArrowImageCustomer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxArrowImageCustomer.setDefaultUnit(kony.flex.DP);
            var imgUpArrowCustomer = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrowCustomer",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowImageCustomer.add(imgUpArrowCustomer);
            var flxFilters3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "200dp",
                "id": "flxFilters3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknheaderLogs",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFilters3.setDefaultUnit(kony.flex.DP);
            var flxFontIconCustomerClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "16dp",
                "id": "flxFontIconCustomerClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "sknCursor",
                "top": "15dp",
                "width": "16dp"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxFontIconCustomerClose.setDefaultUnit(kony.flex.DP);
            var lblFontIconCustomerClose = new kony.ui.Label({
                "id": "lblFontIconCustomerClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFontIconCustomerClose.add(lblFontIconCustomerClose);
            var flxHeaderCustomerFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderCustomerFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxHeaderCustomerFilter.setDefaultUnit(kony.flex.DP);
            var lblFilterByCustomer = new kony.ui.Label({
                "id": "lblFilterByCustomer",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "text": "Filter Data By",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderCustomerFilter.add(lblFilterByCustomer);
            var flxFiltersList3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxFiltersList3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxFiltersList3.setDefaultUnit(kony.flex.DP);
            var flxFilterCriteriaCustomer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68dp",
                "id": "flxFilterCriteriaCustomer1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteriaCustomer1.setDefaultUnit(kony.flex.DP);
            var lblSearchParamCustomer1 = new kony.ui.Label({
                "id": "lblSearchParamCustomer1",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblSearchParamAdmin1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var listBoxSearchParamCustomer3 = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40dp",
                "id": "listBoxSearchParamCustomer3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxFilterCriteriaCustomer1.add(lblSearchParamCustomer1, listBoxSearchParamCustomer3);
            var flxFilterCriteriaCustomer2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68dp",
                "id": "flxFilterCriteriaCustomer2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteriaCustomer2.setDefaultUnit(kony.flex.DP);
            var lblSearchParam2 = new kony.ui.Label({
                "id": "lblSearchParam2",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblSearchParam2\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var listBoxSearchParam2 = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40dp",
                "id": "listBoxSearchParam2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxFilterCriteriaCustomer2.add(lblSearchParam2, listBoxSearchParam2);
            var flxFilterCriteriaCustomer3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "68px",
                "id": "flxFilterCriteriaCustomer3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteriaCustomer3.setDefaultUnit(kony.flex.DP);
            var lblCol3 = new kony.ui.Label({
                "id": "lblCol3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCol1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropDown3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "41px",
                "id": "flxDropDown3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffope1e5edcsr",
                "top": "25px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "skntxtbxhover11abeb"
            });
            flxDropDown3.setDefaultUnit(kony.flex.DP);
            var datePickerCustomerLog = new kony.ui.CustomWidget({
                "id": "datePickerCustomerLog",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": "175px",
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "maxDate": "true",
                "opens": "center",
                "rangeType": "",
                "resetData": null,
                "type": "list",
                "value": ""
            });
            var flxCloseCal3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxCloseCal3",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal3.setDefaultUnit(kony.flex.DP);
            var lblCloseCal3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCloseCal3",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal3.add(lblCloseCal3);
            flxDropDown3.add(datePickerCustomerLog, flxCloseCal3);
            flxFilterCriteriaCustomer3.add(lblCol3, flxDropDown3);
            var flxFilterCriteriaCustomer4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxFilterCriteriaCustomer4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteriaCustomer4.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeApps = new kony.ui.Label({
                "id": "lblAddAlertTypeApps",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "text": "View Column Items",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customListboxApps = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "customListboxApps",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "zIndex": 25,
                "overrides": {
                    "customListbox": {
                        "top": "25dp",
                        "zIndex": 25
                    },
                    "flxSegmentList": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "clipBounds": false,
                        "isVisible": false,
                        "zIndex": 1
                    },
                    "flxSelectAll": {
                        "isVisible": true
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "segList": {
                        "data": [{
                            "imgCheckBox": "",
                            "lblDescription": ""
                        }],
                        "height": "100dp",
                        "zIndex": 1
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFilterCriteriaCustomer4.add(lblAddAlertTypeApps, customListboxApps);
            flxFiltersList3.add(flxFilterCriteriaCustomer1, flxFilterCriteriaCustomer2, flxFilterCriteriaCustomer3, flxFilterCriteriaCustomer4);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "150dp",
                "width": "150dp"
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var btnCustomerActivityLogsSaveFilter = new kony.ui.Button({
                "bottom": "10px",
                "id": "btnCustomerActivityLogsSaveFilter",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknBtnLatoRegular11abeb14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.btnAdvSearch\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAdd3 = new kony.ui.Button({
                "bottom": "15px",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22dp",
                "id": "btnAdd3",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
                "top": "0dp",
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            flxButtons.add(btnCustomerActivityLogsSaveFilter, btnAdd3);
            flxFilters3.add(flxFontIconCustomerClose, flxHeaderCustomerFilter, flxFiltersList3, flxButtons);
            flxFiltersAndHeaders.add(flxArrowImageCustomer, flxFilters3);
            var flxTrasactionalFilter = new kony.ui.FlexContainer({
                "clipBounds": false,
                "height": "220dp",
                "id": "flxTrasactionalFilter",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "50dp",
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "45dp",
                "zIndex": 50
            }, {}, {});
            flxTrasactionalFilter.setDefaultUnit(kony.flex.DP);
            var flxArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12dp",
                "id": "flxArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxArrowImage.setDefaultUnit(kony.flex.DP);
            var imgUpArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrow",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowImage.add(imgUpArrow);
            var flxTransactionLogsFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": false,
                "height": "180px",
                "id": "flxTransactionLogsFilter",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknheaderLogs",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactionLogsFilter.setDefaultUnit(kony.flex.DP);
            var flxHeader1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxHeader1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxHeader1.setDefaultUnit(kony.flex.DP);
            var lblCustomerName = new kony.ui.Label({
                "id": "lblCustomerName",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoRegular484b5213px",
                "text": "Filter Data By",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "12px",
                "width": "20px"
            }, {}, {});
            flxImage.setDefaultUnit(kony.flex.DP);
            var imgStatus = new kony.ui.Image2({
                "height": "100%",
                "id": "imgStatus",
                "isVisible": false,
                "left": "0px",
                "skin": "slImage",
                "src": "img_down_arrow.png",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblIconStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon13pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImage.add(imgStatus, lblIconStatus);
            flxHeader1.add(lblCustomerName, flxImage);
            var flxFilters = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxFilters",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxFilters.setDefaultUnit(kony.flex.DP);
            var flxFiltersList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxFiltersList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 60
            }, {}, {});
            flxFiltersList.setDefaultUnit(kony.flex.DP);
            var flxFilterCriteria1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68dp",
                "id": "flxFilterCriteria1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteria1.setDefaultUnit(kony.flex.DP);
            var lblSearchParam1 = new kony.ui.Label({
                "id": "lblSearchParam1",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "text": "Activity Type",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var listBoxSearchParam1 = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40dp",
                "id": "listBoxSearchParam1",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["Select", "Select service"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxFilterCriteria1.add(lblSearchParam1, listBoxSearchParam1);
            var flxFilterCriteria2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "68px",
                "id": "flxFilterCriteria2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200px",
                "zIndex": 20
            }, {}, {});
            flxFilterCriteria2.setDefaultUnit(kony.flex.DP);
            var flxDropDown01 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "41px",
                "id": "flxDropDown01",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffope1e5edcsr",
                "top": "25px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "skntxtbxhover11abeb"
            });
            flxDropDown01.setDefaultUnit(kony.flex.DP);
            var datePickerTransaction = new kony.ui.CustomWidget({
                "id": "datePickerTransaction",
                "isVisible": true,
                "left": "1dp",
                "right": "3dp",
                "bottom": "1dp",
                "top": "5dp",
                "width": "175px",
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "left",
                "event": null,
                "maxDate": "true",
                "opens": "left",
                "rangeType": null,
                "resetData": null,
                "type": "list",
                "value": ""
            });
            var flxCloseCal1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxCloseCal1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal1.setDefaultUnit(kony.flex.DP);
            var lblCloseCal1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCloseCal1",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal1.add(lblCloseCal1);
            flxDropDown01.add(datePickerTransaction, flxCloseCal1);
            var lblCol1 = new kony.ui.Label({
                "id": "lblCol1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCol1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFilterCriteria2.add(flxDropDown01, lblCol1);
            var flxFilterCriteria3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68dp",
                "id": "flxFilterCriteria3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteria3.setDefaultUnit(kony.flex.DP);
            var blSearchParam3 = new kony.ui.Label({
                "id": "blSearchParam3",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAmountDropDown1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "41px",
                "id": "flxAmountDropDown1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffope1e5edcsr",
                "top": "25px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAmountDropDown1.setDefaultUnit(kony.flex.DP);
            var flxAmountImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAmountImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "40px",
                "zIndex": 1
            }, {}, {});
            flxAmountImage.setDefaultUnit(kony.flex.DP);
            var lblAmountCurrencySymbol = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblAmountCurrencySymbol",
                "isVisible": true,
                "skin": "sknFontIconCurrency",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeparator = new kony.ui.Label({
                "height": "100%",
                "id": "lblSeparator",
                "isVisible": true,
                "left": "39dp",
                "skin": "sknlblSeperator",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblDollar\")",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAmountImage.add(lblAmountCurrencySymbol, lblSeparator);
            var lblSelectedAmount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectedAmount",
                "isVisible": true,
                "left": "40px",
                "skin": "sknlbl0h2ff0d6b13f947AD",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Select_amount_range\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAmountClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "12px",
                "id": "flxAmountClose",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAmountClose.setDefaultUnit(kony.flex.DP);
            var lblAmountClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmountClose",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAmountClose.add(lblAmountClose);
            flxAmountDropDown1.add(flxAmountImage, lblSelectedAmount, flxAmountClose);
            flxFilterCriteria3.add(blSearchParam3, flxAmountDropDown1);
            var flxFilterCriteria4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "68px",
                "id": "flxFilterCriteria4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200px",
                "zIndex": 70
            }, {}, {});
            flxFilterCriteria4.setDefaultUnit(kony.flex.DP);
            var lblViewColumnItems = new kony.ui.Label({
                "id": "lblViewColumnItems",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "text": "View Column Items",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customListBoxTransactional = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "customListBoxTransactional",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "zIndex": 80,
                "overrides": {
                    "customListbox": {
                        "top": "25dp",
                        "zIndex": 80
                    },
                    "flxSegmentList": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "zIndex": 150
                    },
                    "flxSelectAll": {
                        "isVisible": true
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "segList": {
                        "height": "160dp",
                        "zIndex": 200
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFilterCriteria4.add(lblViewColumnItems, customListBoxTransactional);
            flxFiltersList.add(flxFilterCriteria1, flxFilterCriteria2, flxFilterCriteria3, flxFilterCriteria4);
            var btnAdd = new kony.ui.Button({
                "bottom": "10px",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "btnAdd",
                "isVisible": true,
                "right": "15px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
                "top": "75px",
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            var btnTransactionLogsSaveFilter = new kony.ui.Button({
                "bottom": "10px",
                "id": "btnTransactionLogsSaveFilter",
                "isVisible": true,
                "right": "85px",
                "skin": "sknBtnLatoRegular11abeb14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.btnAdvSearch\")",
                "top": "78px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFilters.add(flxFiltersList, btnAdd, btnTransactionLogsSaveFilter);
            flxTransactionLogsFilter.add(flxHeader1, flxFilters);
            flxTrasactionalFilter.add(flxArrowImage, flxTransactionLogsFilter);
            var flxAminConsoleFilterContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "190dp",
                "id": "flxAminConsoleFilterContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "50px",
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "40px",
                "zIndex": 50
            }, {}, {});
            flxAminConsoleFilterContainer.setDefaultUnit(kony.flex.DP);
            var flxArraowImageAdminConsole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12dp",
                "id": "flxArraowImageAdminConsole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxArraowImageAdminConsole.setDefaultUnit(kony.flex.DP);
            var imgUpArrowAdminConsole = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrowAdminConsole",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArraowImageAdminConsole.add(imgUpArrowAdminConsole);
            var flxAdminConsoleFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "178dp",
                "id": "flxAdminConsoleFilter",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknheaderLogs",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxAdminConsoleFilter.setDefaultUnit(kony.flex.DP);
            var flxHeader2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxHeader2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxHeader2.setDefaultUnit(kony.flex.DP);
            var lblCustomerName2 = new kony.ui.Label({
                "id": "lblCustomerName2",
                "isVisible": true,
                "left": "35px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCustomerName\")",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImage2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxImage2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "2px",
                "width": "15px"
            }, {}, {});
            flxImage2.setDefaultUnit(kony.flex.DP);
            var imgStatus2 = new kony.ui.Image2({
                "height": "100%",
                "id": "imgStatus2",
                "isVisible": false,
                "left": "0px",
                "skin": "slImage",
                "src": "img_down_arrow.png",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconStatus2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblIconStatus2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon13pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImage2.add(imgStatus2, lblIconStatus2);
            flxHeader2.add(lblCustomerName2, flxImage2);
            var flxFilters2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFilters2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFilters2.setDefaultUnit(kony.flex.DP);
            var flxFiltersList2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFiltersList2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxFiltersList2.setDefaultUnit(kony.flex.DP);
            var flxFilterCriteriaAdmin1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68dp",
                "id": "flxFilterCriteriaAdmin1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteriaAdmin1.setDefaultUnit(kony.flex.DP);
            var lblSearchParamAdmin1 = new kony.ui.Label({
                "id": "lblSearchParamAdmin1",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblSearchParamAdmin1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var listBoxSearchParamAdmin1 = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40dp",
                "id": "listBoxSearchParamAdmin1",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["op1", "All"],
                    ["op2", "Internal User Management"],
                    ["op3", "Security Images"],
                    ["op4", "Transaction Limits"],
                    ["op5", "CSR Portal"],
                    ["op6", "Permissions"],
                    ["op7", "Access Customer"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxFilterCriteriaAdmin1.add(lblSearchParamAdmin1, listBoxSearchParamAdmin1);
            var flxFilterCriteriaAdmin2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "68px",
                "id": "flxFilterCriteriaAdmin2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteriaAdmin2.setDefaultUnit(kony.flex.DP);
            var lblSearchParamAdmin2 = new kony.ui.Label({
                "id": "lblSearchParamAdmin2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular485c75Font12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCol1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropDownAdmin2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "41px",
                "id": "flxDropDownAdmin2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffope1e5edcsr",
                "top": "25px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "skntxtbxhover11abeb"
            });
            flxDropDownAdmin2.setDefaultUnit(kony.flex.DP);
            var datePickerAdminConsole = new kony.ui.CustomWidget({
                "id": "datePickerAdminConsole",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": "175dp",
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "left",
                "event": null,
                "maxDate": "true",
                "opens": "left",
                "rangeType": "",
                "resetData": null,
                "type": "list",
                "value": ""
            });
            var flxCloseCal2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxCloseCal2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal2.setDefaultUnit(kony.flex.DP);
            var lblCloseCal2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCloseCal2",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal2.add(lblCloseCal2);
            flxDropDownAdmin2.add(datePickerAdminConsole, flxCloseCal2);
            flxFilterCriteriaAdmin2.add(lblSearchParamAdmin2, flxDropDownAdmin2);
            flxFiltersList2.add(flxFilterCriteriaAdmin1, flxFilterCriteriaAdmin2);
            var btnAdminConsoleLogsSaveFilter = new kony.ui.Button({
                "bottom": "10px",
                "id": "btnAdminConsoleLogsSaveFilter",
                "isVisible": true,
                "right": "100dp",
                "skin": "sknBtnLatoRegular11abeb14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.btnAdvSearch\")",
                "top": "75px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAdd2 = new kony.ui.Button({
                "bottom": "15px",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "btnAdd2",
                "isVisible": true,
                "right": "20px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
                "top": "75px",
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            flxFilters2.add(flxFiltersList2, btnAdminConsoleLogsSaveFilter, btnAdd2);
            flxAdminConsoleFilter.add(flxHeader2, flxFilters2);
            flxAminConsoleFilterContainer.add(flxArraowImageAdminConsole, flxAdminConsoleFilter);
            flxMainContent.add(flxCustomerSearchResults, flxCustomerDetailedResults, flxTransactionLog, flxAdminConsoleLog, flxDropDownDetail1, flxAmountDropDown, flxNoResultsFound, flxFiltersAndHeaders, flxTrasactionalFilter, flxAminConsoleFilterContainer);
            var flxbreadcrumbList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxbreadcrumbList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "125dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "125px",
                "width": "125px",
                "zIndex": 300
            }, {}, {});
            flxbreadcrumbList.setDefaultUnit(kony.flex.DP);
            var flxList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop0a",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxList.setDefaultUnit(kony.flex.DP);
            var flxNo1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNo1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNo1.setDefaultUnit(kony.flex.DP);
            var lblNo1 = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblNo1",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Transactional",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNo1.add(lblNo1);
            var flxNo2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNo2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNo2.setDefaultUnit(kony.flex.DP);
            var lblNo2 = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblNo2",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblNo2\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNo2.add(lblNo2);
            var flxNo3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNo3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNo3.setDefaultUnit(kony.flex.DP);
            var lblNo3 = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblNo3",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Customer_Activity\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNo3.add(lblNo3);
            flxList.add(flxNo1, flxNo2, flxNo3);
            flxbreadcrumbList.add(flxList);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 11
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 250,
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPannel.add(flxMainHeader, flxBreadCrumbs, flxLogsList, flxCustomerActivityLog, flxModifySearch, flxHeaderDropdown, flxMainContent, flxbreadcrumbList, flxLoading);
            var flxPopUpSaveFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpSaveFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpSaveFilter.setDefaultUnit(kony.flex.DP);
            var popUpSaveFilter = new com.adminConsole.logs.popUpSaveFilter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUpSaveFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxPopUp": {
                        "centerX": "50%",
                        "isVisible": true,
                        "top": "250px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpSaveFilter.add(popUpSaveFilter);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35dp",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp({
                "clipBounds": true,
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "right": "150px",
                        "text": "NO, LEAVE AS IS"
                    },
                    "btnPopUpDelete": {
                        "text": "YES, DELETE"
                    },
                    "flxPopUpButtons": {
                        "isVisible": true
                    },
                    "lblPopUpMainMessage": {
                        "text": "Delete Saved log"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure you want to delete this filtered log?"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUp.add(popUp);
            flxMain.add(flxLeftPannel, flxRightPannel, flxPopUpSaveFilter, flxToastMessage, flxPopUp);
            var flxChequeImageDsplay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChequeImageDsplay",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxChequeImageDsplay.setDefaultUnit(kony.flex.DP);
            var flxImageChqContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "400px",
                "id": "flxImageChqContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "700px",
                "zIndex": 1
            }, {}, {});
            flxImageChqContainer.setDefaultUnit(kony.flex.DP);
            var CopyimgChq = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "CopyimgChq",
                "isVisible": true,
                "skin": "slImage",
                "src": "right_arrow2x.png",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageChqContainer.add(CopyimgChq);
            var flxImageClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "74%",
                "centerY": "25.56%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxImageClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "40px",
                "zIndex": 1
            }, {}, {});
            flxImageClose.setDefaultUnit(kony.flex.DP);
            var imgChqClose = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "imgChqClose",
                "isVisible": true,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageClose.add(imgChqClose);
            flxChequeImageDsplay.add(flxImageChqContainer, flxImageClose);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            var flxOtherInfoPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50.00%",
                "clipBounds": false,
                "height": "100%",
                "id": "flxOtherInfoPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "flxPopupTrans0db4ac791cac04d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxOtherInfoPopup.setDefaultUnit(kony.flex.DP);
            var flxOtherInfoContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "508dp",
                "id": "flxOtherInfoContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox0e35d4d07b2054b",
                "top": "60dp",
                "width": "986dp"
            }, {}, {});
            flxOtherInfoContainer.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abeb",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "sknFlxPointer",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblPopUpClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPopUpClose",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(lblPopUpClose);
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "40dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var lblPopUpMainMessage = new kony.ui.Label({
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Other Information",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopupHeader.add(lblPopUpMainMessage);
            var flxViewInfoContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewInfoContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxViewInfoContainer.setDefaultUnit(kony.flex.DP);
            var tabs = new com.adminConsole.common.tabs({
                "clipBounds": true,
                "height": "100%",
                "id": "tabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnTab1": {
                        "left": "20dp",
                        "text": "OTHER INFORMATION"
                    },
                    "btnTab2": {
                        "text": "JSON DATA"
                    },
                    "flxTabsContainer": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "width": "100%",
                        "zIndex": 2
                    },
                    "lblSeperator": {
                        "height": "1px",
                        "isVisible": true,
                        "top": "59dp"
                    },
                    "tabs": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxScrollOtherInfo = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrollOtherInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-15dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "89dp",
                "verticalScrollIndicator": true,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxScrollOtherInfo.setDefaultUnit(kony.flex.DP);
            flxScrollOtherInfo.add();
            var flxScrollViewJson = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrollViewJson",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "89dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxScrollViewJson.setDefaultUnit(kony.flex.DP);
            var rtxViewJson = new kony.ui.RichText({
                "id": "rtxViewJson",
                "isVisible": true,
                "left": "20dp",
                "linkSkin": "defRichTextLink",
                "right": "20dp",
                "skin": "sknrtxLato35475f14px",
                "text": "\n",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxScrollViewJson.add(rtxViewJson);
            flxViewInfoContainer.add(tabs, flxScrollOtherInfo, flxScrollViewJson);
            flxOtherInfoContainer.add(flxPopUpTopColor, flxPopUpClose, flxPopupHeader, flxViewInfoContainer);
            flxOtherInfoPopup.add(flxOtherInfoContainer);
            this.add(flxMain, flxChequeImageDsplay, flxEditCancelConfirmation, flxOtherInfoPopup);
        };
        return [{
            "addWidgets": addWidgetsfrmLogs,
            "enabledForIdleTimeout": true,
            "id": "frmLogs",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_f586e203dd3a4bc994a27a291226f29f(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_a1a61b9e8db34b7b96bfc96342558f1e,
            "retainScrollPosition": false
        }]
    }
});