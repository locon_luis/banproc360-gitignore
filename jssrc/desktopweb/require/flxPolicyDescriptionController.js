define("userflxPolicyDescriptionController", {
    //Type your controller code here 
});
define("flxPolicyDescriptionControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxEdit **/
    AS_FlexContainer_b035d7dcf52546248a11d56e5b8ee6f6: function AS_FlexContainer_b035d7dcf52546248a11d56e5b8ee6f6(eventobject, context) {
        var self = this;
        //added
    },
    /** onClick defined for flxDelete **/
    AS_FlexContainer_h43e332b32624c87a4361f632e581db3: function AS_FlexContainer_h43e332b32624c87a4361f632e581db3(eventobject, context) {
        var self = this;
        //added
    }
});
define("flxPolicyDescriptionController", ["userflxPolicyDescriptionController", "flxPolicyDescriptionControllerActions"], function() {
    var controller = require("userflxPolicyDescriptionController");
    var controllerActions = ["flxPolicyDescriptionControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
