define("DashboardModule/frmDashboard", function() {
    return function(controller) {
        function addWidgetsfrmDashboard() {
            this.setDefaultUnit(kony.flex.DP);
            var flxDashboard = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxDashboard",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDashboard.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20%",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var lblHeading = new kony.ui.Label({
                "centerY": "55%",
                "height": "30dp",
                "id": "lblHeading",
                "isVisible": true,
                "left": "35px",
                "skin": "sknLblLatoRegular22px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmDashboard.lblHeading\")",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDate = new kony.ui.Label({
                "centerY": "35%",
                "height": "20dp",
                "id": "lblDate",
                "isVisible": true,
                "left": "35px",
                "skin": "sknLblLatoLight13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmDashboard.lblDate\")",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxHeaderUserOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "24dp",
                "id": "flxHeaderUserOptions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "20dp",
                "width": "30%",
                "zIndex": 2
            }, {}, {});
            flxHeaderUserOptions.setDefaultUnit(kony.flex.DP);
            var lblUserName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUserName",
                "isVisible": true,
                "right": 25,
                "skin": "slLabel0d21bb75081954f",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblUser\")",
                "top": "3dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgLogout = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "imgLogout",
                "isVisible": true,
                "onTouchStart": controller.AS_Image_ba3054180b9948c180685a109ad8fea8,
                "right": "0px",
                "skin": "sknImgHandCursor",
                "src": "img_logout.png",
                "width": "18dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Sign Out"
            });
            flxHeaderUserOptions.add(lblUserName, imgLogout);
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxHeaderSeperator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxHeader.add(lblHeading, lblDate, flxHeaderUserOptions, flxHeaderSeperator);
            flxMainHeader.add(flxHeader);
            var flxTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "20%",
                "width": "438px",
                "zIndex": 5
            }, {}, {});
            flxTabs.setDefaultUnit(kony.flex.DP);
            var flxMyQueue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxMyQueue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8.3%",
                "isModalContainer": false,
                "right": "11.50%",
                "skin": "sknFlxf5f6f8",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxMyQueue.setDefaultUnit(kony.flex.DP);
            var lblMyQueue = new kony.ui.Label({
                "id": "lblMyQueue",
                "isVisible": true,
                "left": "0%",
                "skin": "sknLblLatoReg12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmDashboard.lblMyQueue\")",
                "top": "2px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMyQueueCount = new kony.ui.Label({
                "height": "20px",
                "id": "lblMyQueueCount",
                "isVisible": true,
                "left": "20%",
                "skin": "sknLblfb6345Latoreg12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.dashboardCount\")",
                "top": "0dp",
                "width": "32px",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {});
            var flxlMyQueueViewAllContaner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxlMyQueueViewAllContaner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0%",
                "skin": "sknFlxPointer",
                "top": "0%",
                "width": "20%",
                "zIndex": 2
            }, {}, {});
            flxlMyQueueViewAllContaner.setDefaultUnit(kony.flex.DP);
            var lblMyQueueViewAll = new kony.ui.Label({
                "id": "lblMyQueueViewAll",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLbl00AAEELatoMed12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmDashboard.lblMyQueueViewAll\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxlMyQueueViewAllContaner.add(lblMyQueueViewAll);
            flxMyQueue.add(lblMyQueue, lblMyQueueCount, flxlMyQueueViewAllContaner);
            var dashBoardTabAssignNew = new com.adminConsole.dashBoard.dashBoardTab({
                "clipBounds": true,
                "height": "100px",
                "id": "dashBoardTabAssignNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8.30%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLblTabShadow",
                "top": "52px",
                "width": "165px",
                "zIndex": 1,
                "overrides": {
                    "dashBoardTab": {
                        "top": "52px",
                        "width": "165px"
                    },
                    "flxClick": {
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var dashBoardTabInProgress = new com.adminConsole.dashBoard.dashBoardTab({
                "clipBounds": true,
                "height": "100px",
                "id": "dashBoardTabInProgress",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50.90%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLblTabShadow",
                "top": "52px",
                "width": "165px",
                "zIndex": 1,
                "overrides": {
                    "dashBoardTab": {
                        "left": "50.90%",
                        "top": "52px",
                        "width": "165px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxNewMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxNewMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8.30%",
                "isModalContainer": false,
                "right": "11.50%",
                "skin": "sknFlxf5f6f8",
                "top": "190px",
                "zIndex": 1
            }, {}, {});
            flxNewMessage.setDefaultUnit(kony.flex.DP);
            var lblNewMessage = new kony.ui.Label({
                "id": "lblNewMessage",
                "isVisible": true,
                "left": "0%",
                "skin": "sknLblLatoReg12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmDashboard.lblNewMessage\")",
                "top": "2px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNewMessageCount = new kony.ui.Label({
                "height": "20px",
                "id": "lblNewMessageCount",
                "isVisible": true,
                "left": "29%",
                "skin": "sknLblfb6345Latoreg12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.dashboardCount\")",
                "top": "0dp",
                "width": "32px",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {});
            var flxNewMeaasgeViewAllContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNewMeaasgeViewAllContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0%",
                "skin": "sknFlxPointer",
                "top": "0%",
                "width": "20%",
                "zIndex": 2
            }, {}, {});
            flxNewMeaasgeViewAllContainer.setDefaultUnit(kony.flex.DP);
            var lblNewMeaasgeViewAll = new kony.ui.Label({
                "id": "lblNewMeaasgeViewAll",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLbl00AAEELatoMed12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmDashboard.lblMyQueueViewAll\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNewMeaasgeViewAllContainer.add(lblNewMeaasgeViewAll);
            flxNewMessage.add(lblNewMessage, lblNewMessageCount, flxNewMeaasgeViewAllContainer);
            var newMessageTab1 = new com.adminConsole.dashBoard.dashBoardTab({
                "clipBounds": true,
                "height": "100px",
                "id": "newMessageTab1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8.30%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLblTabShadow",
                "top": "247px",
                "width": "165px",
                "zIndex": 1,
                "overrides": {
                    "dashBoardTab": {
                        "left": "8.30%",
                        "top": "247px",
                        "width": "165px"
                    },
                    "lblCount": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.dashboardCount\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var newMessageTab2 = new com.adminConsole.dashBoard.dashBoardTab({
                "clipBounds": true,
                "height": "100px",
                "id": "newMessageTab2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50.90%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLblTabShadow",
                "top": "247px",
                "width": "165px",
                "zIndex": 1,
                "overrides": {
                    "dashBoardTab": {
                        "left": "50.90%",
                        "top": "247px",
                        "width": "165px"
                    },
                    "lblCount": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.dashboardCount\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var newMessageTab3 = new com.adminConsole.dashBoard.dashBoardTab({
                "clipBounds": true,
                "height": "100px",
                "id": "newMessageTab3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8.30%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLblTabShadow",
                "top": "370px",
                "width": "165px",
                "zIndex": 1,
                "overrides": {
                    "dashBoardTab": {
                        "left": "8.30%",
                        "top": "370px",
                        "width": "165px"
                    },
                    "lblCount": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.dashboardCount\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var newMessageTab4 = new com.adminConsole.dashBoard.dashBoardTab({
                "clipBounds": true,
                "height": "100px",
                "id": "newMessageTab4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50.90%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLblTabShadow",
                "top": "370px",
                "width": "165px",
                "zIndex": 1,
                "overrides": {
                    "dashBoardTab": {
                        "left": "50.90%",
                        "top": "370px",
                        "width": "165px"
                    },
                    "lblCount": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.dashboardCount\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTabs.add(flxMyQueue, dashBoardTabAssignNew, dashBoardTabInProgress, flxNewMessage, newMessageTab1, newMessageTab2, newMessageTab3, newMessageTab4);
            var flxAlertSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75%",
                "id": "flxAlertSegment",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "438px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknLblTabShadow",
                "top": "20%",
                "zIndex": 5
            }, {}, {});
            flxAlertSegment.setDefaultUnit(kony.flex.DP);
            var segDashBoard = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [
                    [{
                            "lblRecivedOn": "Today"
                        },
                        [{
                            "lblCategory": "EMPLOYEE",
                            "lblPriority": "",
                            "lblSubTitle": "Scheduled maintenance of the admin console is coming up on 31st Mar, 2018. Please ensure all pending messages are responded to correctly in accordance with the SLA",
                            "lblTitle": "Scheduled Maintainance"
                        }, {
                            "lblCategory": "EMPLOYEE",
                            "lblPriority": "",
                            "lblSubTitle": "Scheduled maintenance of the admin console is coming up on 31st Mar, 2018. Please ensure all pending messages are responded to correctly in accordance with the SLA",
                            "lblTitle": "Scheduled Maintainance"
                        }]
                    ],
                    [{
                            "lblRecivedOn": "Today"
                        },
                        [{
                            "lblCategory": "EMPLOYEE",
                            "lblPriority": "",
                            "lblSubTitle": "Scheduled maintenance of the admin console is coming up on 31st Mar, 2018. Please ensure all pending messages are responded to correctly in accordance with the SLA",
                            "lblTitle": "Scheduled Maintainance"
                        }, {
                            "lblCategory": "EMPLOYEE",
                            "lblPriority": "",
                            "lblSubTitle": "Scheduled maintenance of the admin console is coming up on 31st Mar, 2018. Please ensure all pending messages are responded to correctly in accordance with the SLA",
                            "lblTitle": "Scheduled Maintainance"
                        }, {
                            "lblCategory": "EMPLOYEE",
                            "lblPriority": "",
                            "lblSubTitle": "Scheduled maintenance of the admin console is coming up on 31st Mar, 2018. Please ensure all pending messages are responded to correctly in accordance with the SLA",
                            "lblTitle": "Scheduled Maintainance"
                        }, {
                            "lblCategory": "EMPLOYEE",
                            "lblPriority": "",
                            "lblSubTitle": "Scheduled maintenance of the admin console is coming up on 31st Mar, 2018. Please ensure all pending messages are responded to correctly in accordance with the SLA",
                            "lblTitle": "Scheduled Maintainance"
                        }, {
                            "lblCategory": "EMPLOYEE",
                            "lblPriority": "",
                            "lblSubTitle": "Scheduled maintenance of the admin console is coming up on 31st Mar, 2018. Please ensure all pending messages are responded to correctly in accordance with the SLA",
                            "lblTitle": "Scheduled Maintainance"
                        }, {
                            "lblCategory": "EMPLOYEE",
                            "lblPriority": "",
                            "lblSubTitle": "Scheduled maintenance of the admin console is coming up on 31st Mar, 2018. Please ensure all pending messages are responded to correctly in accordance with the SLA",
                            "lblTitle": "Scheduled Maintainance"
                        }]
                    ]
                ],
                "groupCells": false,
                "height": "87.50%",
                "id": "segDashBoard",
                "isVisible": true,
                "left": "0%",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxDashBoardAlert",
                "sectionHeaderTemplate": "flxHeaderDashBoardAlert",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "9.50%",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxDashBoardAlert": "flxDashBoardAlert",
                    "flxHeaderDashBoardAlert": "flxHeaderDashBoardAlert",
                    "flxSeprator": "flxSeprator",
                    "flxVariable": "flxVariable",
                    "lblCategory": "lblCategory",
                    "lblPriority": "lblPriority",
                    "lblRecivedOn": "lblRecivedOn",
                    "lblSubTitle": "lblSubTitle",
                    "lblTitle": "lblTitle"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAlerts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxAlerts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxffffffLowerBorder",
                "top": "3%",
                "zIndex": 1
            }, {}, {});
            flxAlerts.setDefaultUnit(kony.flex.DP);
            var lblAlert = new kony.ui.Label({
                "id": "lblAlert",
                "isVisible": true,
                "left": "0%",
                "skin": "sknLblLatoReg12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.AlertsManagement.ALERT\")",
                "top": "2px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertCount = new kony.ui.Label({
                "height": "20px",
                "id": "lblAlertCount",
                "isVisible": true,
                "left": "52px",
                "skin": "sknLblfb6345Latoreg12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmDashboard.lblAlertCount\")",
                "top": "0dp",
                "width": "32px",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var flxAlertFilterContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertFilterContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0%",
                "skin": "sknFlxPointer",
                "top": "0%",
                "width": "5%",
                "zIndex": 2
            }, {}, {});
            flxAlertFilterContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertFilter = new kony.ui.Label({
                "height": "25dp",
                "id": "lblAlertFilter",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLblIcomoon20px485c75",
                "text": "",
                "top": "0dp",
                "width": "25dp",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertFilterContainer.add(lblAlertFilter);
            flxAlerts.add(lblAlert, lblAlertCount, flxAlertFilterContainer);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "statusFilterMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "1.50%",
                "skin": "slFbox",
                "top": "45dp",
                "width": "20%",
                "zIndex": 5,
                "overrides": {
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "width": "100%"
                    },
                    "statusFilterMenu": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "1.50%",
                        "top": "45dp",
                        "width": "20%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxNoResultsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoResultsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoResultsFound.setDefaultUnit(kony.flex.DP);
            var rtxSearchMesg = new kony.ui.RichText({
                "bottom": "40px",
                "centerX": "50%",
                "id": "rtxSearchMesg",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "40px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultsFound.add(rtxSearchMesg);
            flxAlertSegment.add(segDashBoard, flxAlerts, statusFilterMenu, flxNoResultsFound);
            flxRightPanel.add(flxMainHeader, flxTabs, flxAlertSegment);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            flxDashboard.add(flxLeftPannel, flxRightPanel, flxHeaderDropdown, flxLoading, flxToastMessage);
            this.add(flxDashboard);
        };
        return [{
            "addWidgets": addWidgetsfrmDashboard,
            "enabledForIdleTimeout": true,
            "id": "frmDashboard",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_a7392d1d72024b2c9dbf5c07011a7abe(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_ga9181d33bfe402ab5e9a712972ef96c,
            "retainScrollPosition": false
        }]
    }
});