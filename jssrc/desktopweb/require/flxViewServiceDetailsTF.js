define("flxViewServiceDetailsTF", function() {
    return function(controller) {
        var flxViewServiceDetailsTF = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxViewServiceDetailsTF",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "CopyslFbox0ha7099bd299d4e"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxViewServiceDetailsTF.setDefaultUnit(kony.flex.DP);
        var flxViewServiceDetailsTF1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxViewServiceDetailsTF1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 0,
            "width": "190dp",
            "zIndex": 1
        }, {}, {});
        flxViewServiceDetailsTF1.setDefaultUnit(kony.flex.DP);
        var lblViewServiceDetailsTF1 = new kony.ui.Label({
            "height": "100%",
            "id": "lblViewServiceDetailsTF1",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopyslLabel0c28da9c37b8d44",
            "text": "$1",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewServiceDetailsTF1.add(lblViewServiceDetailsTF1);
        var flxViewServiceDetailsTF2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxViewServiceDetailsTF2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "300dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 0,
            "width": "190dp",
            "zIndex": 1
        }, {}, {});
        flxViewServiceDetailsTF2.setDefaultUnit(kony.flex.DP);
        var lblViewServiceDetailsTF2 = new kony.ui.Label({
            "height": "100%",
            "id": "lblViewServiceDetailsTF2",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopyslLabel0c28da9c37b8d44",
            "text": "$50000",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewServiceDetailsTF2.add(lblViewServiceDetailsTF2);
        var flxViewServiceDetailsTF3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxViewServiceDetailsTF3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "570dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 0,
            "width": "190dp",
            "zIndex": 1
        }, {}, {});
        flxViewServiceDetailsTF3.setDefaultUnit(kony.flex.DP);
        var lblViewServiceDetailsTF3 = new kony.ui.Label({
            "height": "100%",
            "id": "lblViewServiceDetailsTF3",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopyslLabel0c28da9c37b8d44",
            "text": "$5",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewServiceDetailsTF3.add(lblViewServiceDetailsTF3);
        flxViewServiceDetailsTF.add(flxViewServiceDetailsTF1, flxViewServiceDetailsTF2, flxViewServiceDetailsTF3);
        return flxViewServiceDetailsTF;
    }
})