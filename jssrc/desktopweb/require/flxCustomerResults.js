define("flxCustomerResults", function() {
    return function(controller) {
        var flxCustomerResults = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "140dp",
            "id": "flxCustomerResults",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxCustomerResults.setDefaultUnit(kony.flex.DP);
        var flxInnerContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15px",
            "clipBounds": true,
            "id": "flxInnerContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCustomerResultsSegrow",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCustomerResultsSegrowHover"
        });
        flxInnerContainer.setDefaultUnit(kony.flex.DP);
        var flxUpperContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "46%",
            "id": "flxUpperContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxUpperContainer.setDefaultUnit(kony.flex.DP);
        var flxLeftContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeftContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "40%",
            "zIndex": 1
        }, {}, {});
        flxLeftContainer.setDefaultUnit(kony.flex.DP);
        var lblCustomerName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCustomerName",
            "isVisible": true,
            "left": "0%",
            "skin": "sknCustomerSegCustomerName",
            "text": "Bryann Nash",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxRetailTag = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxRetailTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagRed",
            "top": "0dp",
            "width": "105px"
        }, {}, {});
        flxRetailTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle1 = new kony.ui.Label({
            "centerY": "48%",
            "id": "fontIconCircle1",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContent1 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblContent1",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "text": "Retail Customer",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRetailTag.add(fontIconCircle1, lblContent1);
        var flxApplicantTag = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxApplicantTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagBlue",
            "top": "0dp",
            "width": "70px"
        }, {}, {});
        flxApplicantTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle2 = new kony.ui.Label({
            "centerY": "48%",
            "id": "fontIconCircle2",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContent2 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblContent2",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "text": "Applicant",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxApplicantTag.add(fontIconCircle2, lblContent2);
        var flxSmallBusinessTag = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxSmallBusinessTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagLiteGreen",
            "top": "0dp",
            "width": "125px"
        }, {}, {});
        flxSmallBusinessTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle3 = new kony.ui.Label({
            "centerY": "48%",
            "id": "fontIconCircle3",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContent3 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblContent3",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "text": "Small Business User",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSmallBusinessTag.add(fontIconCircle3, lblContent3);
        var flxMicroBusinessTag = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxMicroBusinessTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagPurple",
            "top": "0dp",
            "width": "128px"
        }, {}, {});
        flxMicroBusinessTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle4 = new kony.ui.Label({
            "centerY": "48%",
            "id": "fontIconCircle4",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContent4 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblContent4",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "text": "Micro Business User",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMicroBusinessTag.add(fontIconCircle4, lblContent4);
        var flxLeadTag = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxLeadTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagDarkBlue",
            "top": "0dp",
            "width": "50px"
        }, {}, {});
        flxLeadTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle5 = new kony.ui.Label({
            "centerY": "48%",
            "id": "fontIconCircle5",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContent5 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblContent5",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "text": "Lead",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeadTag.add(fontIconCircle5, lblContent5);
        flxLeftContainer.add(lblCustomerName, flxRetailTag, flxApplicantTag, flxSmallBusinessTag, flxMicroBusinessTag, flxLeadTag);
        var flxRightContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRightContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "58%",
            "zIndex": 1
        }, {}, {});
        flxRightContainer.setDefaultUnit(kony.flex.DP);
        var lblEmail = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblEmail",
            "isVisible": true,
            "right": "25px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "bryan.nash@gmail.com",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSearchSeparator = new kony.ui.Label({
            "bottom": "1px",
            "centerY": "50%",
            "height": "15px",
            "id": "lblSearchSeparator",
            "isVisible": true,
            "right": 10,
            "skin": "sknConfigSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
            "width": "1px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPhone = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPhone",
            "isVisible": true,
            "right": "15px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "+180632434323",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSearchSeparator2 = new kony.ui.Label({
            "bottom": "1px",
            "centerY": "50%",
            "height": "15px",
            "id": "lblSearchSeparator2",
            "isVisible": true,
            "right": 10,
            "skin": "sknConfigSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
            "width": "1px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUsername = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUsername",
            "isVisible": true,
            "right": "15px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "Jonh.Bailey",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRightContainer.add(lblEmail, lblSearchSeparator, lblPhone, lblSearchSeparator2, lblUsername);
        flxUpperContainer.add(flxLeftContainer, flxRightContainer);
        var lblRowSeparator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblRowSeparator",
            "isVisible": true,
            "left": 0,
            "right": 0,
            "skin": "sknConfigSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLowerContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "54%",
            "id": "flxLowerContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCustomerSegRowLower",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxLowerContainer.setDefaultUnit(kony.flex.DP);
        var flxDataContainer1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDataContainer1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%"
        }, {}, {});
        flxDataContainer1.setDefaultUnit(kony.flex.DP);
        var lblHeading1 = new kony.ui.Label({
            "id": "lblHeading1",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "CUSTOMER ID",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData1 = new kony.ui.Label({
            "id": "lblData1",
            "isVisible": true,
            "left": 0,
            "right": "0px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "TRN232422",
            "top": "3px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataContainer1.add(lblHeading1, lblData1);
        var flxDataContainer2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDataContainer2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%"
        }, {}, {});
        flxDataContainer2.setDefaultUnit(kony.flex.DP);
        var lblHeading2 = new kony.ui.Label({
            "id": "lblHeading2",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "SSN",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData2 = new kony.ui.Label({
            "id": "lblData2",
            "isVisible": true,
            "left": 0,
            "right": "0px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "*****2212",
            "top": "3px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataContainer2.add(lblHeading2, lblData2);
        var flxDataContainer3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDataContainer3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%"
        }, {}, {});
        flxDataContainer3.setDefaultUnit(kony.flex.DP);
        var lblHeading3 = new kony.ui.Label({
            "id": "lblHeading3",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "DOB",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData3 = new kony.ui.Label({
            "id": "lblData3",
            "isVisible": true,
            "left": 0,
            "right": "0px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "02/12/1995",
            "top": "3px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataContainer3.add(lblHeading3, lblData3);
        var flxDataContainer4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDataContainer4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "22%"
        }, {}, {});
        flxDataContainer4.setDefaultUnit(kony.flex.DP);
        var lblHeading4 = new kony.ui.Label({
            "id": "lblHeading4",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "STATUS",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData4 = new kony.ui.Label({
            "id": "lblData4",
            "isVisible": true,
            "left": 0,
            "right": "0px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "Active",
            "top": "3px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataContainer4.add(lblHeading4, lblData4);
        flxLowerContainer.add(flxDataContainer1, flxDataContainer2, flxDataContainer3, flxDataContainer4);
        flxInnerContainer.add(flxUpperContainer, lblRowSeparator, flxLowerContainer);
        flxCustomerResults.add(flxInnerContainer);
        return flxCustomerResults;
    }
})