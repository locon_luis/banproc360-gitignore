define("CopyFBox0d56e3756273342", function() {
    return function(controller) {
        CopyFBox0d56e3756273342 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "35dp",
            "id": "CopyFBox0d56e3756273342",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "width": "100%"
        }, {
            "containerWeight": 100
        }, {});
        CopyFBox0d56e3756273342.setDefaultUnit(kony.flex.DP);
        var lblEligibilityCriteria = new kony.ui.Label({
            "id": "lblEligibilityCriteria",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknTextDarkGreyLightBold",
            "text": "Label",
            "top": "11dp",
            "width": "720dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "hExpand": true,
            "margin": [1, 1, 1, 1],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var lblIconCheckBox = new kony.ui.Label({
            "id": "lblIconCheckBox",
            "isVisible": false,
            "left": "5dp",
            "skin": "lblIconGrey14px",
            "text": "Label",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "hExpand": true,
            "margin": [1, 1, 1, 1],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var radioBtn = new kony.ui.RadioButtonGroup({
            "height": "25dp",
            "id": "radioBtn",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["rbg1", "Radiobutton One"],
                ["rbg2", "Radiobutton Two"],
                ["rbg3", "Radiobutton Three"]
            ],
            "skin": "CopyslRadioButtonGroup0a864c0dd7aaf4a",
            "top": "5dp",
            "width": "30dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "hExpand": true,
            "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_VERTICAL,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        CopyFBox0d56e3756273342.add(lblEligibilityCriteria, lblIconCheckBox, radioBtn);
        return CopyFBox0d56e3756273342;
    }
})