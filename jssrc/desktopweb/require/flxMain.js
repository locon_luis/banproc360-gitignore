define("flxMain", function() {
    return function(controller) {
        var flxMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "75dp",
            "id": "flxMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknfbfcfc"
        }, {}, {});
        flxMain.setDefaultUnit(kony.flex.DP);
        var flxHeading = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeading",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 10,
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxHeading.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "70%"
        }, {}, {});
        flxHeader.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "id": "lblName",
            "isVisible": true,
            "left": "25dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Nicky Paretla",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDate = new kony.ui.Label({
            "id": "lblDate",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLato5d6c7f12px",
            "text": "12 December 2017 09:30 a.m.",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeader.add(lblName, lblDate);
        var btnDraft = new kony.ui.Button({
            "id": "btnDraft",
            "isVisible": true,
            "right": "25px",
            "skin": "sknBtnLatoee6565Draft",
            "text": "Draft",
            "top": "0dp",
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxAttatchment = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "8dp",
            "clipBounds": true,
            "height": "17px",
            "id": "flxAttatchment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "70px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "17px",
            "zIndex": 1
        }, {}, {});
        flxAttatchment.setDefaultUnit(kony.flex.DP);
        var lblAttach = new kony.ui.Label({
            "centerY": 8,
            "id": "lblAttach",
            "isVisible": true,
            "left": 0,
            "skin": "sknIcon20px",
            "text": "",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgAttatch = new kony.ui.Image2({
            "centerX": "50%",
            "height": "12dp",
            "id": "imgAttatch",
            "isVisible": false,
            "skin": "slImage",
            "src": "attachment_2x.png",
            "top": "5dp",
            "width": "12dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttatchment.add(lblAttach, imgAttatch);
        flxHeading.add(flxHeader, btnDraft, flxAttatchment);
        var rtxDescription = new kony.ui.RichText({
            "height": "20px",
            "id": "rtxDescription",
            "isVisible": true,
            "left": "25px",
            "right": "20px",
            "skin": "sknrtxLato485c7514px",
            "text": "\nLorem ipsum dolor sit amet, <br>\nconsectetur adipiscing elit......",
            "top": "40px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknSeparator",
            "text": "'",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMain.add(flxHeading, rtxDescription, lblSeperator);
        return flxMain;
    }
})