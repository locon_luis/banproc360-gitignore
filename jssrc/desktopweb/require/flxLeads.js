define("flxLeads", function() {
    return function(controller) {
        var flxLeads = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "height": "60px",
            "id": "flxLeads",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxLeads.setDefaultUnit(kony.flex.DP);
        var flxLeadCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeadCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "30px",
            "zIndex": 1
        }, {}, {});
        flxLeadCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "imgCheckBox",
            "isVisible": false,
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "12px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeadCheckbox.add(imgCheckBox);
        var flxLeadType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeadType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "30px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "40px",
            "zIndex": 1
        }, {}, {});
        flxLeadType.setDefaultUnit(kony.flex.DP);
        var fonticonLeadType = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "fonticonLeadType",
            "isVisible": true,
            "skin": "sknlblIcomoonLeadType",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fonticonCustomer\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeadType.add(fonticonLeadType);
        var flxLeadMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeadMain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "70px",
            "isModalContainer": false,
            "right": "60px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 2
        }, {}, {});
        flxLeadMain.setDefaultUnit(kony.flex.DP);
        var flxLeadName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeadName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "18%",
            "zIndex": 1
        }, {}, {});
        flxLeadName.setDefaultUnit(kony.flex.DP);
        var lblLeadName = new kony.ui.Label({
            "centerY": "50%",
            "height": "16px",
            "id": "lblLeadName",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Jasneet Pal Kaur",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeadName.add(lblLeadName);
        var flxLeadPhone = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeadPhone",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "19.60%",
            "zIndex": 1
        }, {}, {});
        flxLeadPhone.setDefaultUnit(kony.flex.DP);
        var lblLeadPhone = new kony.ui.Label({
            "centerY": "50%",
            "height": "16px",
            "id": "lblLeadPhone",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "+123-1234567890-123",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeadPhone.add(lblLeadPhone);
        var flxLeadEmail = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeadEmail",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "21.80%",
            "zIndex": 1
        }, {}, {});
        flxLeadEmail.setDefaultUnit(kony.flex.DP);
        var lblLeadEmail = new kony.ui.Label({
            "centerY": "50%",
            "height": "16px",
            "id": "lblLeadEmail",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "jasneetpal.kaur@kony.com",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeadEmail.add(lblLeadEmail);
        var flxLeadProduct = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeadProduct",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "18.80%",
            "zIndex": 1
        }, {}, {});
        flxLeadProduct.setDefaultUnit(kony.flex.DP);
        var lblLeadProduct = new kony.ui.Label({
            "centerY": "50%",
            "height": "16px",
            "id": "lblLeadProduct",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Freedom credit card",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeadProduct.add(lblLeadProduct);
        var flxLeadAssignedTo = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeadAssignedTo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "21.80%",
            "zIndex": 1
        }, {}, {});
        flxLeadAssignedTo.setDefaultUnit(kony.flex.DP);
        var lblLeadAssignedTo = new kony.ui.Label({
            "centerY": "50%",
            "height": "16px",
            "id": "lblLeadAssignedTo",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "C360 Adminstrator k kony",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeadAssignedTo.add(lblLeadAssignedTo);
        var flxLeadModifiedOn = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeadModifiedOn",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "14%",
            "zIndex": 1
        }, {}, {});
        flxLeadModifiedOn.setDefaultUnit(kony.flex.DP);
        var lblLeadModifiedOn = new kony.ui.Label({
            "centerY": "50%",
            "height": "16px",
            "id": "lblLeadModifiedOn",
            "isVisible": true,
            "left": "0px",
            "right": "35px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Modified on",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeadModifiedOn.add(lblLeadModifiedOn);
        flxLeadMain.add(flxLeadName, flxLeadPhone, flxLeadEmail, flxLeadProduct, flxLeadAssignedTo, flxLeadModifiedOn);
        var flxLeadOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeadOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "width": "60px",
            "zIndex": 1
        }, {}, {});
        flxLeadOptions.setDefaultUnit(kony.flex.DP);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30px",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b2c24ab86083483bae9d1d23854fe16f,
            "right": "20px",
            "skin": "sknFlxBorffffff1pxRound",
            "width": "30px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconOptions = new kony.ui.Label({
            "centerX": "44%",
            "centerY": "50%",
            "height": "30px",
            "id": "lblIconOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconOptions);
        flxLeadOptions.add(flxOptions);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknLeadSeparator",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeads.add(flxLeadCheckbox, flxLeadType, flxLeadMain, flxLeadOptions, lblSeparator);
        return flxLeads;
    }
})