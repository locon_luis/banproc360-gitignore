define(function() {
    return function(controller) {
        var map1 = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "map1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_f103c4f10de64a5ab1f62966ca1bb57e(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "map1"), extendConfig({}, controller.args[1], "map1"), extendConfig({}, controller.args[2], "map1"));
        map1.setDefaultUnit(kony.flex.DP);
        var mapLocations = new kony.ui.Map(extendConfig({
            "calloutWidth": 80,
            "defaultPinImage": "pinb.png",
            "height": "100%",
            "id": "mapLocations",
            "isVisible": true,
            "left": "0%",
            "provider": constants.MAP_PROVIDER_GOOGLE,
            "top": "0%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "mapLocations"), extendConfig({}, controller.args[1], "mapLocations"), extendConfig({}, controller.args[2], "mapLocations"));
        map1.add(mapLocations);
        return map1;
    }
})