define("com/customer360/common/policies/userpoliciesController", function() {
    return {};
});
define("com/customer360/common/policies/policiesControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/customer360/common/policies/policiesController", ["com/customer360/common/policies/userpoliciesController", "com/customer360/common/policies/policiesControllerActions"], function() {
    var controller = require("com/customer360/common/policies/userpoliciesController");
    var actions = require("com/customer360/common/policies/policiesControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
