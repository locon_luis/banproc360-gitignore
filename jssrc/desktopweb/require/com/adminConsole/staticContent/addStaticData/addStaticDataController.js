define("com/adminConsole/staticContent/addStaticData/useraddStaticDataController", function() {
    return {
        setCompFlowActions: function() {},
    };
});
define("com/adminConsole/staticContent/addStaticData/addStaticDataControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for addStaticData **/
    AS_FlexContainer_d5c760d573e94af6b91ba69e0e2c1c19: function AS_FlexContainer_d5c760d573e94af6b91ba69e0e2c1c19(eventobject) {
        var self = this;
    }
});
define("com/adminConsole/staticContent/addStaticData/addStaticDataController", ["com/adminConsole/staticContent/addStaticData/useraddStaticDataController", "com/adminConsole/staticContent/addStaticData/addStaticDataControllerActions"], function() {
    var controller = require("com/adminConsole/staticContent/addStaticData/useraddStaticDataController");
    var actions = require("com/adminConsole/staticContent/addStaticData/addStaticDataControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
