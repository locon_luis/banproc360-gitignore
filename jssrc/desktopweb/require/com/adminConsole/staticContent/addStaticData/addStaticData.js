define(function() {
    return function(controller) {
        var addStaticData = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "610px",
            "id": "addStaticData",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_d5c760d573e94af6b91ba69e0e2c1c19(eventobject);
            },
            "right": "35px",
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "addStaticData"), extendConfig({}, controller.args[1], "addStaticData"), extendConfig({}, controller.args[2], "addStaticData"));
        addStaticData.setDefaultUnit(kony.flex.DP);
        var flxAddStaticContentWrapper = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "0px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "100%",
            "horizontalScrollIndicator": true,
            "id": "flxAddStaticContentWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddStaticContentWrapper"), extendConfig({}, controller.args[1], "flxAddStaticContentWrapper"), extendConfig({}, controller.args[2], "flxAddStaticContentWrapper"));
        flxAddStaticContentWrapper.setDefaultUnit(kony.flex.DP);
        var flxDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxDescription",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDescription"), extendConfig({}, controller.args[1], "flxDescription"), extendConfig({}, controller.args[2], "flxDescription"));
        flxDescription.setDefaultUnit(kony.flex.DP);
        var flxLeft = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "flxLeft"), extendConfig({}, controller.args[1], "flxLeft"), extendConfig({}, controller.args[2], "flxLeft"));
        flxLeft.setDefaultUnit(kony.flex.DP);
        var lblDescription = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "centerY": "50%",
            "id": "lblDescription",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        var imgMandatory = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12dp",
            "id": "imgMandatory",
            "isVisible": true,
            "left": "5dp",
            "right": "5px",
            "skin": "slImage",
            "src": "mandatory_2x.png",
            "top": "2dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgMandatory"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgMandatory"), extendConfig({}, controller.args[2], "imgMandatory"));
        flxLeft.add(lblDescription, imgMandatory);
        var lblMessageSize = new kony.ui.Label(extendConfig({
            "centerY": "65%",
            "id": "lblMessageSize",
            "isVisible": false,
            "right": "35px",
            "skin": "sknllbl485c75Lato13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupDescriptionCount\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMessageSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMessageSize"), extendConfig({}, controller.args[2], "lblMessageSize"));
        flxDescription.add(flxLeft, lblMessageSize);
        var flxRichText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRichText",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "10dp",
            "zIndex": 1
        }, controller.args[0], "flxRichText"), extendConfig({}, controller.args[1], "flxRichText"), extendConfig({}, controller.args[2], "flxRichText"));
        flxRichText.setDefaultUnit(kony.flex.DP);
        var rtxAddStaticContent = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "bottom": "0px",
            "height": "400px",
            "id": "rtxAddStaticContent",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 200,
            "numberOfVisibleLines": 3,
            "right": "70px",
            "skin": "skntxtAreaLato35475f14Px",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "rtxAddStaticContent"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "rtxAddStaticContent"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "rtxAddStaticContent"));
        flxRichText.add(rtxAddStaticContent);
        var flxNoDescriptionError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxNoDescriptionError",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "200dp",
            "zIndex": 1
        }, controller.args[0], "flxNoDescriptionError"), extendConfig({}, controller.args[1], "flxNoDescriptionError"), extendConfig({}, controller.args[2], "flxNoDescriptionError"));
        flxNoDescriptionError.setDefaultUnit(kony.flex.DP);
        var lblNoDescriptionErrorIcon = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblNoDescriptionErrorIcon",
            "isVisible": true,
            "left": "0px",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblNoDescriptionErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoDescriptionErrorIcon"), extendConfig({}, controller.args[2], "lblNoDescriptionErrorIcon"));
        var lblNoDescriptionError = new kony.ui.Label(extendConfig({
            "id": "lblNoDescriptionError",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblNoQuestionDescription\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoDescriptionError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoDescriptionError"), extendConfig({}, controller.args[2], "lblNoDescriptionError"));
        flxNoDescriptionError.add(lblNoDescriptionErrorIcon, lblNoDescriptionError);
        var flxPhraseStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxPhraseStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35px",
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox0h43cfab67a134d",
            "top": 10,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxPhraseStatus"), extendConfig({}, controller.args[1], "flxPhraseStatus"), extendConfig({}, controller.args[2], "flxPhraseStatus"));
        flxPhraseStatus.setDefaultUnit(kony.flex.DP);
        var lblPhraseStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPhraseStatus",
            "isVisible": true,
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Active\")",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "lblPhraseStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPhraseStatus"), extendConfig({}, controller.args[2], "lblPhraseStatus"));
        var SwitchToggleStatus = new kony.ui.Switch(extendConfig({
            "centerY": "50%",
            "height": "25px",
            "id": "SwitchToggleStatus",
            "isVisible": true,
            "left": "0%",
            "leftSideText": "ON",
            "rightSideText": "OFF",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "width": "36dp",
            "zIndex": 1
        }, controller.args[0], "SwitchToggleStatus"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "SwitchToggleStatus"), extendConfig({}, controller.args[2], "SwitchToggleStatus"));
        flxPhraseStatus.add(lblPhraseStatus, SwitchToggleStatus);
        flxAddStaticContentWrapper.add(flxDescription, flxRichText, flxNoDescriptionError, flxPhraseStatus);
        addStaticData.add(flxAddStaticContentWrapper);
        return addStaticData;
    }
})