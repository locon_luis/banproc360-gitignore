define("com/adminConsole/staticContent/staticData/userstaticDataController", function() {
    return {
        setCompFlowActions: function() {
            var scopeObj = this;
            scopeObj.view.flxSelectOptions.isVisible = false;
            this.view.flxOptions.onClick = function() {
                scopeObj.toggleContextualMenu();
            };
        },
        toggleContextualMenu: function() {
            if (this.view.flxSelectOptions.isVisible) {
                this.view.flxSelectOptions.isVisible = false;
                this.view.forceLayout();
            } else {
                this.view.flxSelectOptions.isVisible = true;
                this.view.forceLayout();
            }
        },
        updateContextualMenu: function() {
            kony.print("updating contextual menu");
        },
        deactivateContant: function() {
            this.view.lblUsersStatus.text = "InActive";
        }
    };
});
define("com/adminConsole/staticContent/staticData/staticDataControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for staticData **/
    AS_FlexContainer_gd36cabed06d463cb80493b6dfe08033: function AS_FlexContainer_gd36cabed06d463cb80493b6dfe08033(eventobject) {
        var self = this;
        this.setCompFlowActions();
    }
});
define("com/adminConsole/staticContent/staticData/staticDataController", ["com/adminConsole/staticContent/staticData/userstaticDataController", "com/adminConsole/staticContent/staticData/staticDataControllerActions"], function() {
    var controller = require("com/adminConsole/staticContent/staticData/userstaticDataController");
    var actions = require("com/adminConsole/staticContent/staticData/staticDataControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
