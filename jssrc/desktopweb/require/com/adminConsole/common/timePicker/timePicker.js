define(function() {
    return function(controller) {
        var timePicker = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100px",
            "id": "timePicker",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0b6ab2ac21ea948",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "timePicker"), extendConfig({}, controller.args[1], "timePicker"), extendConfig({}, controller.args[2], "timePicker"));
        timePicker.setDefaultUnit(kony.flex.DP);
        var lstbxAMPM = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstbxAMPM",
            "isVisible": true,
            "left": "150px",
            "masterData": [
                ["AM", "AM"],
                ["PM", "PM"]
            ],
            "selectedKey": "AM",
            "selectedKeyValue": ["AM", "AM"],
            "skin": "CopysknlbxBorderc0b0e9d087e19a44",
            "top": "23dp",
            "width": "60px",
            "zIndex": 1
        }, controller.args[0], "lstbxAMPM"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstbxAMPM"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstbxAMPM"));
        var flxOuterBorder = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxOuterBorder",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBorder",
            "top": "23dp",
            "width": "140px",
            "zIndex": 1
        }, controller.args[0], "flxOuterBorder"), extendConfig({}, controller.args[1], "flxOuterBorder"), extendConfig({}, controller.args[2], "flxOuterBorder"));
        flxOuterBorder.setDefaultUnit(kony.flex.DP);
        var lstbxHours = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstbxHours",
            "isVisible": true,
            "left": "10px",
            "masterData": [
                ["hh", "hh"],
                ["1", "1"],
                ["2", "2"],
                ["3", "3"],
                ["4", "4"],
                ["5", "5"],
                ["6", "6"],
                ["7", "7"],
                ["8", "8"],
                ["9", "9"],
                ["10", "10"],
                ["11", "11"],
                ["12", "12"]
            ],
            "selectedKey": "hh",
            "selectedKeyValue": ["hh", "hh"],
            "skin": "CopysknlbxBorderc0f235394b4a5547",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 2
        }, controller.args[0], "lstbxHours"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstbxHours"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstbxHours"));
        var lblColen = new kony.ui.Label(extendConfig({
            "id": "lblColen",
            "isVisible": true,
            "left": "70px",
            "skin": "CopyslFbox0b6ab2ac21ea948",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblColen\")",
            "top": "10px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblColen"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblColen"), extendConfig({}, controller.args[2], "lblColen"));
        var lstbxMinutes = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstbxMinutes",
            "isVisible": true,
            "masterData": [
                ["mm", "mm"],
                ["0", "00"],
                ["1", "01"],
                ["2", "02"],
                ["3", "03"],
                ["4", "04"],
                ["5", "05"],
                ["6", "06"],
                ["7", "07"],
                ["8", "08"],
                ["9", "09"],
                ["10", "10"],
                ["11", "11"],
                ["12", "12"],
                ["13", "13"],
                ["14", "14"],
                ["15", "15"],
                ["16", "16"],
                ["17", "17"],
                ["18", "18"],
                ["19", "19"],
                ["20", "20"],
                ["21", "21"],
                ["22", "22"],
                ["23", "23"],
                ["24", "24"],
                ["25", "25"],
                ["26", "26"],
                ["27", "27"],
                ["28", "28"],
                ["29", "29"],
                ["30", "30"],
                ["31", "31"],
                ["32", "32"],
                ["33", "33"],
                ["34", "34"],
                ["35", "35"],
                ["36", "36"],
                ["37", "37"],
                ["38", "38"],
                ["39", "39"],
                ["40", "40"],
                ["41", "41"],
                ["42", "42"],
                ["43", "43"],
                ["44", "44"],
                ["45", "45"],
                ["46", "46"],
                ["47", "47"],
                ["48", "48"],
                ["49", "49"],
                ["50", "50"],
                ["51", "51"],
                ["52", "52"],
                ["53", "53"],
                ["54", "54"],
                ["55", "55"],
                ["56", "56"],
                ["57", "57"],
                ["58", "58"],
                ["59", "59"]
            ],
            "right": "10px",
            "selectedKey": "mm",
            "selectedKeyValue": ["mm", "mm"],
            "skin": "CopysknlbxBorderc0f235394b4a5547",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 2
        }, controller.args[0], "lstbxMinutes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstbxMinutes"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstbxMinutes"));
        flxOuterBorder.add(lstbxHours, lblColen, lstbxMinutes);
        timePicker.add(lstbxAMPM, flxOuterBorder);
        return timePicker;
    }
})