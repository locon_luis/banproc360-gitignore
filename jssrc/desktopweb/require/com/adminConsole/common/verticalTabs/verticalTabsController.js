define("com/adminConsole/common/verticalTabs/userverticalTabsController", function() {
    return {
        showSelectedOptionImage: function(imgWidget) {
            this.view.fontIconSelected1.setVisibility(false);
            this.view.fontIconSelected2.setVisibility(false);
            this.view.fontIconSelected3.setVisibility(false);
            this.view.fontIconSelected4.setVisibility(false);
            imgWidget.setVisibility(true);
        },
        setSelectedOptionButtonStyle: function(btnWidget) {
            this.view.btnOption1.skin = "Btn84939efont13px";
            this.view.btnOption2.skin = "Btn84939efont13px";
            this.view.btnOption3.skin = "Btn84939efont13px";
            this.view.btnOption4.skin = "Btn84939efont13px";
            btnWidget.skin = "Btn000000font13px";
        },
        verticalTabsPreShow: function() {
                this.showSelectedOptionImage(this.view.fontIconSelected1);
                this.setSelectedOptionButtonStyle(this.view.btnOption1);
            }
            /*
            example to use above functions
                	setFlowActions : function(){
                		var scopeObj=this;
                		this.view.btnOption1.onClick= function(){
            	            scopeObj.showSelectedOptionImage(scopeObj.view.imgSelected1);
            	            scopeObj.setSelectedOptionButtonStyle(scopeObj.view.btnOption1);
            	            //additional functions which needs to be called
            	        };
                	}
            */
    };
});
define("com/adminConsole/common/verticalTabs/verticalTabsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for verticalTabs **/
    AS_FlexContainer_e3325f619a57467d8ced35976ca07b03: function AS_FlexContainer_e3325f619a57467d8ced35976ca07b03(eventobject) {
        var self = this;
        this.verticalTabsPreShow();
        kony.print("verticalTabs preshow called");
    }
});
define("com/adminConsole/common/verticalTabs/verticalTabsController", ["com/adminConsole/common/verticalTabs/userverticalTabsController", "com/adminConsole/common/verticalTabs/verticalTabsControllerActions"], function() {
    var controller = require("com/adminConsole/common/verticalTabs/userverticalTabsController");
    var actions = require("com/adminConsole/common/verticalTabs/verticalTabsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
