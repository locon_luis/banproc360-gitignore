define("com/adminConsole/common/HeaderGuest/userHeaderGuestController", function() {
    return {};
});
define("com/adminConsole/common/HeaderGuest/HeaderGuestControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/HeaderGuest/HeaderGuestController", ["com/adminConsole/common/HeaderGuest/userHeaderGuestController", "com/adminConsole/common/HeaderGuest/HeaderGuestControllerActions"], function() {
    var controller = require("com/adminConsole/common/HeaderGuest/userHeaderGuestController");
    var actions = require("com/adminConsole/common/HeaderGuest/HeaderGuestControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
