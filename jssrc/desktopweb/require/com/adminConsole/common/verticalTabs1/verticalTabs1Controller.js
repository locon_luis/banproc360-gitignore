define("com/adminConsole/common/verticalTabs1/userverticalTabs1Controller", function() {
    return {
        showSelectedOptionImage: function(imgWidget) {
            this.view.imgSelected1.setVisibility(false);
            this.view.imgSelected2.setVisibility(false);
            this.view.imgSelected3.setVisibility(false);
            this.view.imgSelected4.setVisibility(false);
            imgWidget.setVisibility(true);
        },
        setSelectedOptionButtonStyle: function(btnWidget) {
            this.view.btnOption1.skin = "Btn84939efont13px";
            this.view.btnOption2.skin = "Btn84939efont13px";
            this.view.btnOption3.skin = "Btn84939efont13px";
            this.view.btnOption4.skin = "Btn84939efont13px";
            btnWidget.skin = "Btn000000font13px";
        },
        verticalTabsPreShow: function() {
                this.showSelectedOptionImage(this.view.imgSelected1);
                this.setSelectedOptionButtonStyle(this.view.btnOption1);
            }
            /*
            example to use above functions
                	setFlowActions : function(){
                		var scopeObj=this;
                		this.view.btnOption1.onClick= function(){
            	            scopeObj.showSelectedOptionImage(scopeObj.view.imgSelected1);
            	            scopeObj.setSelectedOptionButtonStyle(scopeObj.view.btnOption1);
            	            //additional functions which needs to be called
            	        };
                	}
            */
    };
});
define("com/adminConsole/common/verticalTabs1/verticalTabs1ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/common/verticalTabs1/verticalTabs1Controller", ["com/adminConsole/common/verticalTabs1/userverticalTabs1Controller", "com/adminConsole/common/verticalTabs1/verticalTabs1ControllerActions"], function() {
    var controller = require("com/adminConsole/common/verticalTabs1/userverticalTabs1Controller");
    var actions = require("com/adminConsole/common/verticalTabs1/verticalTabs1ControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
