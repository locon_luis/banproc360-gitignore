define(function() {
    return function(controller) {
        var verticalTabs1 = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "verticalTabs1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf9f9f9NoBorder",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "verticalTabs1"), extendConfig({}, controller.args[1], "verticalTabs1"), extendConfig({}, controller.args[2], "verticalTabs1"));
        verticalTabs1.setDefaultUnit(kony.flex.DP);
        var flxOption1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "85dp",
            "id": "flxOption1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption1"), extendConfig({}, controller.args[1], "flxOption1"), extendConfig({}, controller.args[2], "flxOption1"));
        flxOption1.setDefaultUnit(kony.flex.DP);
        var btnOption1 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnOption1",
            "isVisible": true,
            "left": "30dp",
            "skin": "Btn000000font14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnOption1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption1"), extendConfig({
            "hoverSkin": "Btn000000font14px"
        }, controller.args[2], "btnOption1"));
        var flxoptionSeperator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "centerX": "50%",
            "clipBounds": true,
            "height": "1px",
            "id": "flxoptionSeperator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30px",
            "isModalContainer": false,
            "right": "30px",
            "skin": "sknflxe1e5edop100",
            "zIndex": 1
        }, controller.args[0], "flxoptionSeperator1"), extendConfig({}, controller.args[1], "flxoptionSeperator1"), extendConfig({}, controller.args[2], "flxoptionSeperator1"));
        flxoptionSeperator1.setDefaultUnit(kony.flex.DP);
        flxoptionSeperator1.add();
        var flxImgArrow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow1"), extendConfig({}, controller.args[1], "flxImgArrow1"), extendConfig({}, controller.args[2], "flxImgArrow1"));
        flxImgArrow1.setDefaultUnit(kony.flex.DP);
        var imgSelected1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "imgSelected1",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected1"), extendConfig({}, controller.args[2], "imgSelected1"));
        var lblSelected1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected1",
            "isVisible": true,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected1"), extendConfig({}, controller.args[2], "lblSelected1"));
        flxImgArrow1.add(imgSelected1, lblSelected1);
        flxOption1.add(btnOption1, flxoptionSeperator1, flxImgArrow1);
        var flxOption2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "85dp",
            "id": "flxOption2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption2"), extendConfig({}, controller.args[1], "flxOption2"), extendConfig({}, controller.args[2], "flxOption2"));
        flxOption2.setDefaultUnit(kony.flex.DP);
        var btnOption2 = new kony.ui.Button(extendConfig({
            "id": "btnOption2",
            "isVisible": true,
            "left": "30dp",
            "skin": "Btna1acbafont12pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnOption2\")",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption2"), extendConfig({
            "hoverSkin": "Btn000000font14px"
        }, controller.args[2], "btnOption2"));
        var lblOptional2 = new kony.ui.Label(extendConfig({
            "id": "lblOptional2",
            "isVisible": true,
            "left": "30px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "45px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional2"), extendConfig({}, controller.args[2], "lblOptional2"));
        var flxAddSeperator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "centerX": "50%",
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeperator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30px",
            "isModalContainer": false,
            "right": "30px",
            "skin": "sknflxe1e5edop100",
            "zIndex": 1
        }, controller.args[0], "flxAddSeperator2"), extendConfig({}, controller.args[1], "flxAddSeperator2"), extendConfig({}, controller.args[2], "flxAddSeperator2"));
        flxAddSeperator2.setDefaultUnit(kony.flex.DP);
        flxAddSeperator2.add();
        var flxImgArrow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "top": "30px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow2"), extendConfig({}, controller.args[1], "flxImgArrow2"), extendConfig({}, controller.args[2], "flxImgArrow2"));
        flxImgArrow2.setDefaultUnit(kony.flex.DP);
        var imgSelected2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "imgSelected2",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected2"), extendConfig({}, controller.args[2], "imgSelected2"));
        var lblSelected2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected2",
            "isVisible": true,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected2"), extendConfig({}, controller.args[2], "lblSelected2"));
        flxImgArrow2.add(imgSelected2, lblSelected2);
        flxOption2.add(btnOption2, lblOptional2, flxAddSeperator2, flxImgArrow2);
        var flxOption3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "85dp",
            "id": "flxOption3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption3"), extendConfig({}, controller.args[1], "flxOption3"), extendConfig({}, controller.args[2], "flxOption3"));
        flxOption3.setDefaultUnit(kony.flex.DP);
        var btnOption3 = new kony.ui.Button(extendConfig({
            "id": "btnOption3",
            "isVisible": true,
            "left": "30dp",
            "skin": "Btna1acbafont12pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnOption3\")",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption3"), extendConfig({
            "hoverSkin": "Btn000000font14px"
        }, controller.args[2], "btnOption3"));
        var lblOptional3 = new kony.ui.Label(extendConfig({
            "id": "lblOptional3",
            "isVisible": true,
            "left": "30px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "45px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional3"), extendConfig({}, controller.args[2], "lblOptional3"));
        var flxAddSeperator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "centerX": "50%",
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeperator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30px",
            "isModalContainer": false,
            "right": "30px",
            "skin": "sknflxe1e5edop100",
            "zIndex": 1
        }, controller.args[0], "flxAddSeperator3"), extendConfig({}, controller.args[1], "flxAddSeperator3"), extendConfig({}, controller.args[2], "flxAddSeperator3"));
        flxAddSeperator3.setDefaultUnit(kony.flex.DP);
        flxAddSeperator3.add();
        var flxImgArrow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "top": "30px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow3"), extendConfig({}, controller.args[1], "flxImgArrow3"), extendConfig({}, controller.args[2], "flxImgArrow3"));
        flxImgArrow3.setDefaultUnit(kony.flex.DP);
        var imgSelected3 = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgSelected3",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected3"), extendConfig({}, controller.args[2], "imgSelected3"));
        var lblSelected3 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected3",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "top": "6dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected3"), extendConfig({}, controller.args[2], "lblSelected3"));
        flxImgArrow3.add(imgSelected3, lblSelected3);
        flxOption3.add(btnOption3, lblOptional3, flxAddSeperator3, flxImgArrow3);
        var flxOption4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "85dp",
            "id": "flxOption4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption4"), extendConfig({}, controller.args[1], "flxOption4"), extendConfig({}, controller.args[2], "flxOption4"));
        flxOption4.setDefaultUnit(kony.flex.DP);
        var btnOption4 = new kony.ui.Button(extendConfig({
            "id": "btnOption4",
            "isVisible": true,
            "left": "30dp",
            "skin": "Btna1acbafont12pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnOption4\")",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption4"), extendConfig({
            "hoverSkin": "Btn000000font14px"
        }, controller.args[2], "btnOption4"));
        var lblOptional4 = new kony.ui.Label(extendConfig({
            "id": "lblOptional4",
            "isVisible": true,
            "left": "30px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "45px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional4"), extendConfig({}, controller.args[2], "lblOptional4"));
        var flxAddSeperator4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "centerX": "50%",
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeperator4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30px",
            "isModalContainer": false,
            "right": "30px",
            "skin": "sknflxe1e5edop100",
            "zIndex": 1
        }, controller.args[0], "flxAddSeperator4"), extendConfig({}, controller.args[1], "flxAddSeperator4"), extendConfig({}, controller.args[2], "flxAddSeperator4"));
        flxAddSeperator4.setDefaultUnit(kony.flex.DP);
        flxAddSeperator4.add();
        var flxImgArrow4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "top": "30px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow4"), extendConfig({}, controller.args[1], "flxImgArrow4"), extendConfig({}, controller.args[2], "flxImgArrow4"));
        flxImgArrow4.setDefaultUnit(kony.flex.DP);
        var imgSelected4 = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgSelected4",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected4"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected4"), extendConfig({}, controller.args[2], "imgSelected4"));
        var lblSelected4 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected4",
            "isVisible": true,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected4"), extendConfig({}, controller.args[2], "lblSelected4"));
        flxImgArrow4.add(imgSelected4, lblSelected4);
        flxOption4.add(btnOption4, lblOptional4, flxAddSeperator4, flxImgArrow4);
        verticalTabs1.add(flxOption1, flxOption2, flxOption3, flxOption4);
        return verticalTabs1;
    }
})