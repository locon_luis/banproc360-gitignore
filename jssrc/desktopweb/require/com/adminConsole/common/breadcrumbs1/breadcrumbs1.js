define(function() {
    return function(controller) {
        var breadcrumbs1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "20px",
            "id": "breadcrumbs1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "breadcrumbs1"), extendConfig({}, controller.args[1], "breadcrumbs1"), extendConfig({}, controller.args[2], "breadcrumbs1"));
        breadcrumbs1.setDefaultUnit(kony.flex.DP);
        var btnBackToMain = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "btnBackToMain",
            "isVisible": true,
            "left": "35px",
            "skin": "sknBtnLato11abeb12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.SECURITYQUESTIONS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnBackToMain"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnBackToMain"), extendConfig({}, controller.args[2], "btnBackToMain"));
        var fontIconBreadcrumbsRight = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconBreadcrumbsRight",
            "isVisible": true,
            "left": "12dp",
            "right": 12,
            "skin": "sknFontIconBreadcrumb",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsRight\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconBreadcrumbsRight"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconBreadcrumbsRight"), extendConfig({}, controller.args[2], "fontIconBreadcrumbsRight"));
        var btnPreviousPage = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "btnPreviousPage",
            "isVisible": false,
            "skin": "sknBtnLato11abeb12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.SECURITYQUESTIONS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnPreviousPage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPreviousPage"), extendConfig({}, controller.args[2], "btnPreviousPage"));
        var fontIconBreadcrumbsRight2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconBreadcrumbsRight2",
            "isVisible": false,
            "left": "12dp",
            "right": "12dp",
            "skin": "sknFontIconBreadcrumb",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsRight\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconBreadcrumbsRight2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconBreadcrumbsRight2"), extendConfig({}, controller.args[2], "fontIconBreadcrumbsRight2"));
        var btnPreviousPage1 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "btnPreviousPage1",
            "isVisible": false,
            "left": "0",
            "skin": "sknBtnLato11abeb12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.SECURITYQUESTIONS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnPreviousPage1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPreviousPage1"), extendConfig({}, controller.args[2], "btnPreviousPage1"));
        var fontIconBreadcrumbsRight3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconBreadcrumbsRight3",
            "isVisible": false,
            "left": "12dp",
            "right": "12dp",
            "skin": "sknFontIconBreadcrumb",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsRight\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconBreadcrumbsRight3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconBreadcrumbsRight3"), extendConfig({}, controller.args[2], "fontIconBreadcrumbsRight3"));
        var lblCurrentScreen = new kony.ui.Label(extendConfig({
            "height": "20px",
            "id": "lblCurrentScreen",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7512px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.ADDQUESTION\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCurrentScreen"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrentScreen"), extendConfig({}, controller.args[2], "lblCurrentScreen"));
        var fontIconBreadcrumbsDown = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconBreadcrumbsDown",
            "isVisible": false,
            "left": "12dp",
            "right": "12dp",
            "skin": "sknfontIconDescDownArrow12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconBreadcrumbsDown"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconBreadcrumbsDown"), extendConfig({}, controller.args[2], "fontIconBreadcrumbsDown"));
        breadcrumbs1.add(btnBackToMain, fontIconBreadcrumbsRight, btnPreviousPage, fontIconBreadcrumbsRight2, btnPreviousPage1, fontIconBreadcrumbsRight3, lblCurrentScreen, fontIconBreadcrumbsDown);
        return breadcrumbs1;
    }
})