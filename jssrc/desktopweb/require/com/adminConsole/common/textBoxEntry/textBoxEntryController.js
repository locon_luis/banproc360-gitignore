define("com/adminConsole/common/textBoxEntry/usertextBoxEntryController", function() {
    return {};
});
define("com/adminConsole/common/textBoxEntry/textBoxEntryControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/textBoxEntry/textBoxEntryController", ["com/adminConsole/common/textBoxEntry/usertextBoxEntryController", "com/adminConsole/common/textBoxEntry/textBoxEntryControllerActions"], function() {
    var controller = require("com/adminConsole/common/textBoxEntry/usertextBoxEntryController");
    var actions = require("com/adminConsole/common/textBoxEntry/textBoxEntryControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
