define(function() {
    return function(controller) {
        var textBoxEntry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "textBoxEntry",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "textBoxEntry"), extendConfig({}, controller.args[1], "textBoxEntry"), extendConfig({}, controller.args[2], "textBoxEntry"));
        textBoxEntry.setDefaultUnit(kony.flex.DP);
        var lblHeadingText = new kony.ui.Label(extendConfig({
            "id": "lblHeadingText",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLato485c7514px",
            "text": "Label",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeadingText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeadingText"), extendConfig({}, controller.args[2], "lblHeadingText"));
        var lblCount = new kony.ui.Label(extendConfig({
            "id": "lblCount",
            "isVisible": false,
            "right": "0dp",
            "skin": "slLabel0d20174dce8ea42",
            "text": "0/10",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCount"), extendConfig({}, controller.args[2], "lblCount"));
        var lblOptional = new kony.ui.Label(extendConfig({
            "id": "lblOptional",
            "isVisible": false,
            "left": "65dp",
            "skin": "slLabel0d4f692dab05249",
            "text": "Label",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptional"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional"), extendConfig({}, controller.args[2], "lblOptional"));
        var flxEnterValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxEnterValue",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxEnterValueNormal",
            "top": "25dp",
            "zIndex": 1
        }, controller.args[0], "flxEnterValue"), extendConfig({}, controller.args[1], "flxEnterValue"), extendConfig({}, controller.args[2], "flxEnterValue"));
        flxEnterValue.setDefaultUnit(kony.flex.DP);
        var tbxEnterValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "40dp",
            "id": "tbxEnterValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "placeholder": "Placeholder",
            "right": 0,
            "secureTextEntry": false,
            "skin": "skntbxEnterValue",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "tbxEnterValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxEnterValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxEnterValue"));
        var flxBtnCheck = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxBtnCheck",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "75dp"
        }, controller.args[0], "flxBtnCheck"), extendConfig({}, controller.args[1], "flxBtnCheck"), extendConfig({}, controller.args[2], "flxBtnCheck"));
        flxBtnCheck.setDefaultUnit(kony.flex.DP);
        var btnCheck = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "btnCheck",
            "isVisible": true,
            "left": "-5dp",
            "skin": "sknbtnEnterValueCheck",
            "text": "Check",
            "top": "0dp",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "btnCheck"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCheck"), extendConfig({}, controller.args[2], "btnCheck"));
        var flxSepartor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSepartor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSeparatorCsr",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 2
        }, controller.args[0], "flxSepartor"), extendConfig({}, controller.args[1], "flxSepartor"), extendConfig({}, controller.args[2], "flxSepartor"));
        flxSepartor.setDefaultUnit(kony.flex.DP);
        flxSepartor.add();
        flxBtnCheck.add(btnCheck, flxSepartor);
        flxEnterValue.add(tbxEnterValue, flxBtnCheck);
        var flxInlineError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxInlineError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "70dp",
            "width": "95%"
        }, controller.args[0], "flxInlineError"), extendConfig({}, controller.args[1], "flxInlineError"), extendConfig({}, controller.args[2], "flxInlineError"));
        flxInlineError.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon"), extendConfig({}, controller.args[2], "lblErrorIcon"));
        var lblErrorText = new kony.ui.Label(extendConfig({
            "id": "lblErrorText",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblError",
            "text": "Error",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "lblErrorText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText"), extendConfig({}, controller.args[2], "lblErrorText"));
        flxInlineError.add(lblErrorIcon, lblErrorText);
        textBoxEntry.add(lblHeadingText, lblCount, lblOptional, flxEnterValue, flxInlineError);
        return textBoxEntry;
    }
})