define(function() {
    return function(controller) {
        var tabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "tabs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "tabs"), extendConfig({}, controller.args[1], "tabs"), extendConfig({}, controller.args[2], "tabs"));
        tabs.setDefaultUnit(kony.flex.DP);
        var flxTabsContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxTabsContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxedefefOpNoBorder",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTabsContainer"), extendConfig({}, controller.args[1], "flxTabsContainer"), extendConfig({}, controller.args[2], "flxTabsContainer"));
        flxTabsContainer.setDefaultUnit(kony.flex.DP);
        var btnTab1 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "height": "45dp",
            "id": "btnTab1",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
            "text": "TAB",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTab1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [2, 0, 2, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTab1"), extendConfig({}, controller.args[2], "btnTab1"));
        var btnTab2 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "height": "45dp",
            "id": "btnTab2",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
            "text": "TAB",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTab2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [2, 0, 2, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTab2"), extendConfig({}, controller.args[2], "btnTab2"));
        var btnTab3 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "focusSkin": "defBtnFocus",
            "height": "45dp",
            "id": "btnTab3",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
            "text": "TAB",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTab3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTab3"), extendConfig({}, controller.args[2], "btnTab3"));
        var btnTab4 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "focusSkin": "defBtnFocus",
            "height": "45dp",
            "id": "btnTab4",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
            "text": "TAB",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTab4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTab4"), extendConfig({}, controller.args[2], "btnTab4"));
        flxTabsContainer.add(btnTab1, btnTab2, btnTab3, btnTab4);
        var lblSeperator = new kony.ui.Label(extendConfig({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": false,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": ".",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        tabs.add(flxTabsContainer, lblSeperator);
        return tabs;
    }
})