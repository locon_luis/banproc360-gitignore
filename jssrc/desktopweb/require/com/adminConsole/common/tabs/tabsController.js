define("com/adminConsole/common/tabs/usertabsController", function() {
    return {};
});
define("com/adminConsole/common/tabs/tabsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/tabs/tabsController", ["com/adminConsole/common/tabs/usertabsController", "com/adminConsole/common/tabs/tabsControllerActions"], function() {
    var controller = require("com/adminConsole/common/tabs/usertabsController");
    var actions = require("com/adminConsole/common/tabs/tabsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
