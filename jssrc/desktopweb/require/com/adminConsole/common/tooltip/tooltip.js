define(function() {
    return function(controller) {
        var tooltip = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "tooltip",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "230px",
            "zIndex": 10
        }, controller.args[0], "tooltip"), extendConfig({}, controller.args[1], "tooltip"), extendConfig({}, controller.args[2], "tooltip"));
        tooltip.setDefaultUnit(kony.flex.DP);
        var flxToolTip = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxToolTip",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "65px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "233px",
            "zIndex": 10
        }, controller.args[0], "flxToolTip"), extendConfig({}, controller.args[1], "flxToolTip"), extendConfig({}, controller.args[2], "flxToolTip"));
        flxToolTip.setDefaultUnit(kony.flex.DP);
        var lblarrow = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "10dp",
            "id": "lblarrow",
            "isVisible": true,
            "left": "0px",
            "skin": "Copyfonticonhighrisk0b42a522e274941",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconUp\")",
            "top": "0px",
            "width": "17px",
            "zIndex": 20
        }, controller.args[0], "lblarrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblarrow"), extendConfig({}, controller.args[2], "lblarrow"));
        var flxToolTipMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxToolTipMessage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxTooltip",
            "top": "9px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxToolTipMessage"), extendConfig({}, controller.args[1], "flxToolTipMessage"), extendConfig({}, controller.args[2], "flxToolTipMessage"));
        flxToolTipMessage.setDefaultUnit(kony.flex.DP);
        var lblNoConcentToolTip = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "45%",
            "id": "lblNoConcentToolTip",
            "isVisible": true,
            "skin": "sknlblCSRAssist",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblNoConcentToolTip\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoConcentToolTip"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoConcentToolTip"), extendConfig({}, controller.args[2], "lblNoConcentToolTip"));
        flxToolTipMessage.add(lblNoConcentToolTip);
        flxToolTip.add(lblarrow, flxToolTipMessage);
        tooltip.add(flxToolTip);
        return tooltip;
    }
})