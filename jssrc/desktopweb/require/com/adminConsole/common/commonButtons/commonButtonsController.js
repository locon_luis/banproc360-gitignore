define("com/adminConsole/common/commonButtons/usercommonButtonsController", function() {
    return {};
});
define("com/adminConsole/common/commonButtons/commonButtonsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/commonButtons/commonButtonsController", ["com/adminConsole/common/commonButtons/usercommonButtonsController", "com/adminConsole/common/commonButtons/commonButtonsControllerActions"], function() {
    var controller = require("com/adminConsole/common/commonButtons/usercommonButtonsController");
    var actions = require("com/adminConsole/common/commonButtons/commonButtonsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
