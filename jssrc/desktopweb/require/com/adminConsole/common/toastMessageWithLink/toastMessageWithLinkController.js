define("com/adminConsole/common/toastMessageWithLink/usertoastMessageWithLinkController", function() {
    return {};
});
define("com/adminConsole/common/toastMessageWithLink/toastMessageWithLinkControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/toastMessageWithLink/toastMessageWithLinkController", ["com/adminConsole/common/toastMessageWithLink/usertoastMessageWithLinkController", "com/adminConsole/common/toastMessageWithLink/toastMessageWithLinkControllerActions"], function() {
    var controller = require("com/adminConsole/common/toastMessageWithLink/usertoastMessageWithLinkController");
    var actions = require("com/adminConsole/common/toastMessageWithLink/toastMessageWithLinkControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
