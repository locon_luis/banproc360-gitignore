define(function() {
    return function(controller) {
        var toastMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "70px",
            "id": "toastMessage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "toastMessage"), extendConfig({}, controller.args[1], "toastMessage"), extendConfig({}, controller.args[2], "toastMessage"));
        toastMessage.setDefaultUnit(kony.flex.DP);
        var flxToastContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "45px",
            "id": "flxToastContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "sknflxSuccessToast1F844D",
            "top": "0%",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "flxToastContainer"), extendConfig({}, controller.args[1], "flxToastContainer"), extendConfig({}, controller.args[2], "flxToastContainer"));
        flxToastContainer.setDefaultUnit(kony.flex.DP);
        var fontIconImgLeft = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "fontIconImgLeft",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknFontIcontoastSuccess",
            "text": "",
            "top": "0dp",
            "width": "17dp",
            "zIndex": 1
        }, controller.args[0], "fontIconImgLeft"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgLeft"), extendConfig({}, controller.args[2], "fontIconImgLeft"));
        var lbltoastMessage = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "30px",
            "id": "lbltoastMessage",
            "isVisible": true,
            "left": "45px",
            "right": "45px",
            "skin": "lblfffffflatoregular14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.SecurityQuestionsavedsuccessfully\")",
            "zIndex": 1
        }, controller.args[0], "lbltoastMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbltoastMessage"), extendConfig({}, controller.args[2], "lbltoastMessage"));
        var flxRightImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35px",
            "id": "flxRightImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 5,
            "skin": "slFbox",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxRightImage"), extendConfig({}, controller.args[1], "flxRightImage"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxRightImage"));
        flxRightImage.setDefaultUnit(kony.flex.DP);
        var lblIconRight = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "lblIconRight",
            "isVisible": true,
            "right": "10dp",
            "skin": "sknIcon18pxWhite",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "18dp",
            "zIndex": 1
        }, controller.args[0], "lblIconRight"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconRight"), extendConfig({}, controller.args[2], "lblIconRight"));
        flxRightImage.add(lblIconRight);
        flxToastContainer.add(fontIconImgLeft, lbltoastMessage, flxRightImage);
        toastMessage.add(flxToastContainer);
        return toastMessage;
    }
})