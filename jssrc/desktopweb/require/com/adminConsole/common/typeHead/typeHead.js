define(function() {
    return function(controller) {
        var typeHead = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "typeHead",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "typeHead"), extendConfig({}, controller.args[1], "typeHead"), extendConfig({}, controller.args[2], "typeHead"));
        typeHead.setDefaultUnit(kony.flex.DP);
        var tbxSearchKey = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxSearchKey",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
            "secureTextEntry": false,
            "skin": "skntbxLato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxSearchKey"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchKey"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxSearchKey"));
        var segSearchResult = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "lblAddress": "country",
                "lblPinIcon": ""
            }, {
                "lblAddress": "country",
                "lblPinIcon": ""
            }, {
                "lblAddress": "country",
                "lblPinIcon": ""
            }, {
                "lblAddress": "country",
                "lblPinIcon": ""
            }, {
                "lblAddress": "country",
                "lblPinIcon": ""
            }],
            "groupCells": false,
            "height": "145dp",
            "id": "segSearchResult",
            "isVisible": false,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxSearchCompanyMap",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "40dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxSearchCompanyMap": "flxSearchCompanyMap",
                "lblAddress": "lblAddress",
                "lblPinIcon": "lblPinIcon"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segSearchResult"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segSearchResult"), extendConfig({}, controller.args[2], "segSearchResult"));
        var flxNoResultFound = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxNoResultFound",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknbackGroundffffff100",
            "top": "40dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultFound"), extendConfig({}, controller.args[1], "flxNoResultFound"), extendConfig({}, controller.args[2], "flxNoResultFound"));
        flxNoResultFound.setDefaultUnit(kony.flex.DP);
        var lblNoResultFound = new kony.ui.Label(extendConfig({
            "id": "lblNoResultFound",
            "isVisible": true,
            "left": "0dp",
            "skin": "slLabel0d20174dce8ea42",
            "text": "No result found",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoResultFound"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResultFound"), extendConfig({}, controller.args[2], "lblNoResultFound"));
        flxNoResultFound.add(lblNoResultFound);
        typeHead.add(tbxSearchKey, segSearchResult, flxNoResultFound);
        return typeHead;
    }
})