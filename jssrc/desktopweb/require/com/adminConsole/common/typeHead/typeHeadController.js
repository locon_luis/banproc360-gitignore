define("com/adminConsole/common/typeHead/usertypeHeadController", function() {
    return {};
});
define("com/adminConsole/common/typeHead/typeHeadControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/typeHead/typeHeadController", ["com/adminConsole/common/typeHead/usertypeHeadController", "com/adminConsole/common/typeHead/typeHeadControllerActions"], function() {
    var controller = require("com/adminConsole/common/typeHead/usertypeHeadController");
    var actions = require("com/adminConsole/common/typeHead/typeHeadControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
