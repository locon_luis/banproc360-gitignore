define(function() {
    return function(controller) {
        var breadcrumbs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "20px",
            "id": "breadcrumbs",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "breadcrumbs"), extendConfig({}, controller.args[1], "breadcrumbs"), extendConfig({}, controller.args[2], "breadcrumbs"));
        breadcrumbs.setDefaultUnit(kony.flex.DP);
        var btnBackToMain = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "btnBackToMain",
            "isVisible": true,
            "left": "35px",
            "skin": "sknBtnLato11abeb12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.SECURITYQUESTIONS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnBackToMain"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnBackToMain"), extendConfig({}, controller.args[2], "btnBackToMain"));
        var imgBreadcrumbsRight = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12dp",
            "id": "imgBreadcrumbsRight",
            "isVisible": false,
            "left": "12dp",
            "right": 12,
            "skin": "slImage",
            "src": "img_breadcrumb_arrow.png",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgBreadcrumbsRight"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgBreadcrumbsRight"), extendConfig({}, controller.args[2], "imgBreadcrumbsRight"));
        var fontIconBreadcrumbsRight = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconBreadcrumbsRight",
            "isVisible": true,
            "left": "12dp",
            "right": 12,
            "skin": "sknFontIconBreadcrumb",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsRight\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconBreadcrumbsRight"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconBreadcrumbsRight"), extendConfig({}, controller.args[2], "fontIconBreadcrumbsRight"));
        var btnPreviousPage = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "btnPreviousPage",
            "isVisible": false,
            "skin": "sknBtnLato11abeb12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.SECURITYQUESTIONS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnPreviousPage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPreviousPage"), extendConfig({}, controller.args[2], "btnPreviousPage"));
        var imgBreadcrumbsRight2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12dp",
            "id": "imgBreadcrumbsRight2",
            "isVisible": false,
            "left": "12dp",
            "right": 12,
            "skin": "slImage",
            "src": "img_breadcrumb_arrow.png",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgBreadcrumbsRight2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgBreadcrumbsRight2"), extendConfig({}, controller.args[2], "imgBreadcrumbsRight2"));
        var fontIconBreadcrumbsRight2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconBreadcrumbsRight2",
            "isVisible": false,
            "left": "12dp",
            "right": "12dp",
            "skin": "sknFontIconBreadcrumb",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsRight\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconBreadcrumbsRight2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconBreadcrumbsRight2"), extendConfig({}, controller.args[2], "fontIconBreadcrumbsRight2"));
        var btnPreviousPage1 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "btnPreviousPage1",
            "isVisible": false,
            "left": "0",
            "skin": "sknBtnLato11abeb12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.SECURITYQUESTIONS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnPreviousPage1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPreviousPage1"), extendConfig({}, controller.args[2], "btnPreviousPage1"));
        var fontIconBreadcrumbsRight3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconBreadcrumbsRight3",
            "isVisible": false,
            "left": "12dp",
            "right": "12dp",
            "skin": "sknFontIconBreadcrumb",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsRight\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconBreadcrumbsRight3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconBreadcrumbsRight3"), extendConfig({}, controller.args[2], "fontIconBreadcrumbsRight3"));
        var imgBreadcrumbsRight3 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12dp",
            "id": "imgBreadcrumbsRight3",
            "isVisible": false,
            "left": "12dp",
            "right": 12,
            "skin": "slImage",
            "src": "img_breadcrumb_arrow.png",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgBreadcrumbsRight3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgBreadcrumbsRight3"), extendConfig({}, controller.args[2], "imgBreadcrumbsRight3"));
        var lblCurrentScreen = new kony.ui.Label(extendConfig({
            "height": "20px",
            "id": "lblCurrentScreen",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular00000012px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.ADDQUESTION\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCurrentScreen"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrentScreen"), extendConfig({}, controller.args[2], "lblCurrentScreen"));
        var fontIconBreadcrumbsDown = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconBreadcrumbsDown",
            "isVisible": false,
            "left": "12dp",
            "right": "12dp",
            "skin": "sknfontIconDescDownArrow12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconBreadcrumbsDown"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconBreadcrumbsDown"), extendConfig({}, controller.args[2], "fontIconBreadcrumbsDown"));
        var imgBreadcrumbsDown = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12dp",
            "id": "imgBreadcrumbsDown",
            "isVisible": false,
            "left": "12dp",
            "right": 12,
            "skin": "slImage",
            "src": "img_down_arrow.png",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgBreadcrumbsDown"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgBreadcrumbsDown"), extendConfig({}, controller.args[2], "imgBreadcrumbsDown"));
        breadcrumbs.add(btnBackToMain, imgBreadcrumbsRight, fontIconBreadcrumbsRight, btnPreviousPage, imgBreadcrumbsRight2, fontIconBreadcrumbsRight2, btnPreviousPage1, fontIconBreadcrumbsRight3, imgBreadcrumbsRight3, lblCurrentScreen, fontIconBreadcrumbsDown, imgBreadcrumbsDown);
        return breadcrumbs;
    }
})