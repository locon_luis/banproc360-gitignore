define("com/adminConsole/common/breadcrumbs/userbreadcrumbsController", function() {
    return {};
});
define("com/adminConsole/common/breadcrumbs/breadcrumbsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/breadcrumbs/breadcrumbsController", ["com/adminConsole/common/breadcrumbs/userbreadcrumbsController", "com/adminConsole/common/breadcrumbs/breadcrumbsControllerActions"], function() {
    var controller = require("com/adminConsole/common/breadcrumbs/userbreadcrumbsController");
    var actions = require("com/adminConsole/common/breadcrumbs/breadcrumbsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
