define("com/adminConsole/common/contextualMenu1/usercontextualMenu1Controller", function() {
    return {};
});
define("com/adminConsole/common/contextualMenu1/contextualMenu1ControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/contextualMenu1/contextualMenu1Controller", ["com/adminConsole/common/contextualMenu1/usercontextualMenu1Controller", "com/adminConsole/common/contextualMenu1/contextualMenu1ControllerActions"], function() {
    var controller = require("com/adminConsole/common/contextualMenu1/usercontextualMenu1Controller");
    var actions = require("com/adminConsole/common/contextualMenu1/contextualMenu1ControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
