define(function() {
    return function(controller) {
        var contextualMenu1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "contextualMenu1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "contextualMenu1"), extendConfig({}, controller.args[1], "contextualMenu1"), extendConfig({}, controller.args[2], "contextualMenu1"));
        contextualMenu1.setDefaultUnit(kony.flex.DP);
        var lblHeader = new kony.ui.Label(extendConfig({
            "id": "lblHeader",
            "isVisible": true,
            "left": "16px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ASSIGN\")",
            "top": 30,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeader"), extendConfig({}, controller.args[2], "lblHeader"));
        var btnLink1 = new kony.ui.Button(extendConfig({
            "id": "btnLink1",
            "isVisible": true,
            "left": "16dp",
            "skin": "contextuallinks",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.Users\")",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnLink1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnLink1"), extendConfig({}, controller.args[2], "btnLink1"));
        var btnLink2 = new kony.ui.Button(extendConfig({
            "bottom": 15,
            "id": "btnLink2",
            "isVisible": true,
            "left": "16dp",
            "skin": "contextuallinks",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.Permissions\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnLink2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnLink2"), extendConfig({}, controller.args[2], "btnLink2"));
        var flxOptionsSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "1px",
            "id": "flxOptionsSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOptionsSeperator"), extendConfig({}, controller.args[1], "flxOptionsSeperator"), extendConfig({}, controller.args[2], "flxOptionsSeperator"));
        flxOptionsSeperator.setDefaultUnit(kony.flex.DP);
        flxOptionsSeperator.add();
        var flxOption1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxOption1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0i3b050eeb8134e",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption1"), extendConfig({}, controller.args[1], "flxOption1"), extendConfig({}, controller.args[2], "flxOption1"));
        flxOption1.setDefaultUnit(kony.flex.DP);
        var imgOption1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgOption1",
            "isVisible": false,
            "left": "16dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgOption1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgOption1"), extendConfig({}, controller.args[2], "imgOption1"));
        var lblIconOption1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIconOption1",
            "isVisible": true,
            "left": "14px",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIconOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconOption1"), extendConfig({}, controller.args[2], "lblIconOption1"));
        var lblOption1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption1",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption1"), extendConfig({
            "hoverSkin": "sknlblLato11ABEB13px"
        }, controller.args[2], "lblOption1"));
        flxOption1.add(imgOption1, lblIconOption1, lblOption1);
        var flxOption2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxOption2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0i3b050eeb8134e",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption2"), extendConfig({}, controller.args[1], "flxOption2"), extendConfig({}, controller.args[2], "flxOption2"));
        flxOption2.setDefaultUnit(kony.flex.DP);
        var imgOption2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgOption2",
            "isVisible": false,
            "left": "16dp",
            "skin": "slImage",
            "src": "edit2x.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgOption2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgOption2"), extendConfig({}, controller.args[2], "imgOption2"));
        var lblIconOption2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIconOption2",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIconOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconOption2"), extendConfig({}, controller.args[2], "lblIconOption2"));
        var lblOption2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption2",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
            "top": "4dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption2"), extendConfig({
            "hoverSkin": "sknlblLato11ABEB13px"
        }, controller.args[2], "lblOption2"));
        flxOption2.add(imgOption2, lblIconOption2, lblOption2);
        var flxOption3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxOption3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0i3b050eeb8134e",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption3"), extendConfig({}, controller.args[1], "flxOption3"), extendConfig({}, controller.args[2], "flxOption3"));
        flxOption3.setDefaultUnit(kony.flex.DP);
        var imgOption3 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgOption3",
            "isVisible": false,
            "left": "16dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgOption3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgOption3"), extendConfig({}, controller.args[2], "imgOption3"));
        var lblIconOption3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIconOption3",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIconOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconOption3"), extendConfig({}, controller.args[2], "lblIconOption3"));
        var lblOption3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption3",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Suspend\")",
            "top": "4dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption3"), extendConfig({
            "hoverSkin": "sknlblLato11ABEB13px"
        }, controller.args[2], "lblOption3"));
        flxOption3.add(imgOption3, lblIconOption3, lblOption3);
        var flxOption4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxOption4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0i3b050eeb8134e",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption4"), extendConfig({}, controller.args[1], "flxOption4"), extendConfig({}, controller.args[2], "flxOption4"));
        flxOption4.setDefaultUnit(kony.flex.DP);
        var imgOption4 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgOption4",
            "isVisible": false,
            "left": "16dp",
            "skin": "slImage",
            "src": "deactive_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgOption4"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgOption4"), extendConfig({}, controller.args[2], "imgOption4"));
        var lblIconOption4 = new kony.ui.Label(extendConfig({
            "centerY": "50.00%",
            "id": "lblIconOption4",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption4\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconOption4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconOption4"), extendConfig({}, controller.args[2], "lblIconOption4"));
        var lblOption4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption4",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
            "top": "4dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOption4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption4"), extendConfig({
            "hoverSkin": "sknlblLato11ABEB13px"
        }, controller.args[2], "lblOption4"));
        flxOption4.add(imgOption4, lblIconOption4, lblOption4);
        contextualMenu1.add(lblHeader, btnLink1, btnLink2, flxOptionsSeperator, flxOption1, flxOption2, flxOption3, flxOption4);
        return contextualMenu1;
    }
})