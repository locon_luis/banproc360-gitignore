define(function() {
    return function(controller) {
        var popUpCancelEdits = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "popUpCancelEdits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBg000000Op50",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "popUpCancelEdits"), extendConfig({}, controller.args[1], "popUpCancelEdits"), extendConfig({}, controller.args[2], "popUpCancelEdits"));
        popUpCancelEdits.setDefaultUnit(kony.flex.DP);
        var flxPopUp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": false,
            "id": "flxPopUp",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox0j9f841cc563e4e",
            "top": "250px",
            "width": "600px",
            "zIndex": 10
        }, controller.args[0], "flxPopUp"), extendConfig({}, controller.args[1], "flxPopUp"), extendConfig({}, controller.args[2], "flxPopUp"));
        flxPopUp.setDefaultUnit(kony.flex.DP);
        var flxPopUpTopColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxPopUpTopColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxebb54cOp100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpTopColor"), extendConfig({}, controller.args[1], "flxPopUpTopColor"), extendConfig({}, controller.args[2], "flxPopUpTopColor"));
        flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
        flxPopUpTopColor.add();
        var flxPopupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0d9c3974835234d",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopupHeader"), extendConfig({}, controller.args[1], "flxPopupHeader"), extendConfig({}, controller.args[2], "flxPopupHeader"));
        flxPopupHeader.setDefaultUnit(kony.flex.DP);
        var flxPopUpClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxPopUpClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "slFbox",
            "top": "15dp",
            "width": "25px",
            "zIndex": 20
        }, controller.args[0], "flxPopUpClose"), extendConfig({}, controller.args[1], "flxPopUpClose"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxPopUpClose"));
        flxPopUpClose.setDefaultUnit(kony.flex.DP);
        var imgPopUpClose = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgPopUpClose",
            "isVisible": false,
            "skin": "slImage",
            "src": "close_blue.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgPopUpClose"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgPopUpClose"), extendConfig({}, controller.args[2], "imgPopUpClose"));
        var lblPopupClose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "lblPopupClose",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon18pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblPopupClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopupClose"), extendConfig({}, controller.args[2], "lblPopupClose"));
        flxPopUpClose.add(imgPopUpClose, lblPopupClose);
        var lblPopUpMainMessage = new kony.ui.Label(extendConfig({
            "id": "lblPopUpMainMessage",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f23px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")",
            "top": "0px",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblPopUpMainMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpMainMessage"), extendConfig({}, controller.args[2], "lblPopUpMainMessage"));
        var rtxPopUpDisclaimer = new kony.ui.RichText(extendConfig({
            "bottom": 30,
            "id": "rtxPopUpDisclaimer",
            "isVisible": true,
            "left": "20px",
            "right": 20,
            "skin": "sknrtxLato0df8337c414274d",
            "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "rtxPopUpDisclaimer"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxPopUpDisclaimer"), extendConfig({}, controller.args[2], "rtxPopUpDisclaimer"));
        flxPopupHeader.add(flxPopUpClose, lblPopUpMainMessage, rtxPopUpDisclaimer);
        var flxPopUpButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxPopUpButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox0dc900c4572aa40",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpButtons"), extendConfig({}, controller.args[1], "flxPopUpButtons"), extendConfig({}, controller.args[2], "flxPopUpButtons"));
        flxPopUpButtons.setDefaultUnit(kony.flex.DP);
        var btnPopUpCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
            "height": "40px",
            "id": "btnPopUpCancel",
            "isVisible": true,
            "right": "130px",
            "skin": "sknBtnLatoRegulara5abc413pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
        }, controller.args[2], "btnPopUpCancel"));
        var btnPopUpDelete = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
            "height": "40px",
            "id": "btnPopUpDelete",
            "isVisible": true,
            "minWidth": "100px",
            "right": "20px",
            "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpDelete"), extendConfig({
            "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
        }, controller.args[2], "btnPopUpDelete"));
        flxPopUpButtons.add(btnPopUpCancel, btnPopUpDelete);
        flxPopUp.add(flxPopUpTopColor, flxPopupHeader, flxPopUpButtons);
        popUpCancelEdits.add(flxPopUp);
        return popUpCancelEdits;
    }
})