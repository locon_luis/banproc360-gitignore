define("com/adminConsole/common/customSwitch/usercustomSwitchController", function() {
    return {};
});
define("com/adminConsole/common/customSwitch/customSwitchControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/customSwitch/customSwitchController", ["com/adminConsole/common/customSwitch/usercustomSwitchController", "com/adminConsole/common/customSwitch/customSwitchControllerActions"], function() {
    var controller = require("com/adminConsole/common/customSwitch/usercustomSwitchController");
    var actions = require("com/adminConsole/common/customSwitch/customSwitchControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
