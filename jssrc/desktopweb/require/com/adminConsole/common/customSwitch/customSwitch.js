define(function() {
    return function(controller) {
        var customSwitch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "customSwitch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "customSwitch"), extendConfig({}, controller.args[1], "customSwitch"), extendConfig({}, controller.args[2], "customSwitch"));
        customSwitch.setDefaultUnit(kony.flex.DP);
        var switchToggle = new kony.ui.Switch(extendConfig({
            "height": "25px",
            "id": "switchToggle",
            "isVisible": true,
            "left": "0dp",
            "leftSideText": "ON",
            "right": 0,
            "rightSideText": "OFF",
            "selectedIndex": 1,
            "skin": "sknSwitchServiceManagement",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "switchToggle"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "switchToggle"), extendConfig({}, controller.args[2], "switchToggle"));
        customSwitch.add(switchToggle);
        return customSwitch;
    }
})