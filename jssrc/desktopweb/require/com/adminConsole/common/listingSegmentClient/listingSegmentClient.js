define(function() {
    return function(controller) {
        var listingSegmentClient = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "listingSegmentClient",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "listingSegmentClient"), extendConfig({}, controller.args[1], "listingSegmentClient"), extendConfig({}, controller.args[2], "listingSegmentClient"));
        listingSegmentClient.setDefaultUnit(kony.flex.DP);
        var flxListingSegmentWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxListingSegmentWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxListingSegmentWrapper"), extendConfig({}, controller.args[1], "flxListingSegmentWrapper"), extendConfig({}, controller.args[2], "flxListingSegmentWrapper"));
        flxListingSegmentWrapper.setDefaultUnit(kony.flex.DP);
        var segListing = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20px",
            "groupCells": false,
            "id": "segListing",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": true,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {},
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segListing"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segListing"), extendConfig({}, controller.args[2], "segListing"));
        var rtxNoResultsFound = new kony.ui.RichText(extendConfig({
            "bottom": "150px",
            "centerX": "50%",
            "id": "rtxNoResultsFound",
            "isVisible": false,
            "skin": "sknRtxLato84939e12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
            "top": "150px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxNoResultsFound"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxNoResultsFound"), extendConfig({}, controller.args[2], "rtxNoResultsFound"));
        var flxPagination = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "70px",
            "id": "flxPagination",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPagination"), extendConfig({}, controller.args[1], "flxPagination"), extendConfig({}, controller.args[2], "flxPagination"));
        flxPagination.setDefaultUnit(kony.flex.DP);
        var pagination = new com.adminConsole.common.pagination1(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "pagination",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "pagination"), extendConfig({
            "overrides": {}
        }, controller.args[1], "pagination"), extendConfig({
            "overrides": {}
        }, controller.args[2], "pagination"));
        flxPagination.add(pagination);
        flxListingSegmentWrapper.add(segListing, rtxNoResultsFound, flxPagination);
        var flxContextualMenu = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxContextualMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35px",
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "100px",
            "width": "200px",
            "zIndex": 1
        }, controller.args[0], "flxContextualMenu"), extendConfig({}, controller.args[1], "flxContextualMenu"), extendConfig({}, controller.args[2], "flxContextualMenu"));
        flxContextualMenu.setDefaultUnit(kony.flex.DP);
        var contextualMenu = new com.adminConsole.common.contextualMenu1(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "contextualMenu",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "0dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "contextualMenu"), extendConfig({
            "overrides": {}
        }, controller.args[1], "contextualMenu"), extendConfig({
            "overrides": {}
        }, controller.args[2], "contextualMenu"));
        flxContextualMenu.add(contextualMenu);
        listingSegmentClient.add(flxListingSegmentWrapper, flxContextualMenu);
        return listingSegmentClient;
    }
})