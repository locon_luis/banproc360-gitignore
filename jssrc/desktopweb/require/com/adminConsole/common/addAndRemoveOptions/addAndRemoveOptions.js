define(function() {
    return function(controller) {
        var addAndRemoveOptions = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "addAndRemoveOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "addAndRemoveOptions"), extendConfig({}, controller.args[1], "addAndRemoveOptions"), extendConfig({}, controller.args[2], "addAndRemoveOptions"));
        addAndRemoveOptions.setDefaultUnit(kony.flex.DP);
        var flxAvailableOptionsWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxAvailableOptionsWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "57.14%",
            "zIndex": 1
        }, controller.args[0], "flxAvailableOptionsWrapper"), extendConfig({}, controller.args[1], "flxAvailableOptionsWrapper"), extendConfig({}, controller.args[2], "flxAvailableOptionsWrapper"));
        flxAvailableOptionsWrapper.setDefaultUnit(kony.flex.DP);
        var flxAvailableOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAvailableOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "right": "18px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxAvailableOptions"), extendConfig({}, controller.args[1], "flxAvailableOptions"), extendConfig({}, controller.args[2], "flxAvailableOptions"));
        flxAvailableOptions.setDefaultUnit(kony.flex.DP);
        var lblAvailableOptionsHeading = new kony.ui.Label(extendConfig({
            "id": "lblAvailableOptionsHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAvailableOptionsHeading\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAvailableOptionsHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailableOptionsHeading"), extendConfig({}, controller.args[2], "lblAvailableOptionsHeading"));
        var btnSelectAll = new kony.ui.Button(extendConfig({
            "id": "btnSelectAll",
            "isVisible": true,
            "right": "0px",
            "skin": "sknBtnLato11ABEB11Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Add_All\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnSelectAll"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSelectAll"), extendConfig({}, controller.args[2], "btnSelectAll"));
        var flxSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "55px",
            "id": "flxSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxf5f6f8Op100",
            "top": "30px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSearch"), extendConfig({}, controller.args[1], "flxSearch"), extendConfig({}, controller.args[2], "flxSearch"));
        flxSearch.setDefaultUnit(kony.flex.DP);
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "focusSkin": "sknflxBgffffffBorder1293ccRadius30Px",
            "height": "35px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": 10,
            "skin": "sknflxd5d9ddop100",
            "zIndex": 1
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var flxSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSearchImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxSearchImage"), extendConfig({}, controller.args[1], "flxSearchImage"), extendConfig({}, controller.args[2], "flxSearchImage"));
        flxSearchImage.setDefaultUnit(kony.flex.DP);
        var imgSearchIcon = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "imgSearchIcon",
            "isVisible": true,
            "left": "15px",
            "skin": "CopyslImage2",
            "src": "search_1x.png",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "imgSearchIcon"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSearchIcon"), extendConfig({}, controller.args[2], "imgSearchIcon"));
        flxSearchImage.add(imgSearchIcon);
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "100%",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "35dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var flxClearSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxClearSearchImage",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxClearSearchImage"), extendConfig({}, controller.args[1], "flxClearSearchImage"), extendConfig({}, controller.args[2], "flxClearSearchImage"));
        flxClearSearchImage.setDefaultUnit(kony.flex.DP);
        var imgClearSearch = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "imgClearSearch",
            "isVisible": true,
            "right": "15px",
            "skin": "CopyslImage2",
            "src": "close_blue.png",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "imgClearSearch"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClearSearch"), extendConfig({}, controller.args[2], "imgClearSearch"));
        flxClearSearchImage.add(imgClearSearch);
        flxSearchContainer.add(flxSearchImage, tbxSearchBox, flxClearSearchImage);
        flxSearch.add(flxSearchContainer);
        var flxAddOptionsSegment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20px",
            "clipBounds": true,
            "id": "flxAddOptionsSegment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxffffffOp100BorderE1E5EdNoRadius",
            "top": "85px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddOptionsSegment"), extendConfig({}, controller.args[1], "flxAddOptionsSegment"), extendConfig({}, controller.args[2], "flxAddOptionsSegment"));
        flxAddOptionsSegment.setDefaultUnit(kony.flex.DP);
        var segAddOptions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "btnAdd": "Add",
                "lblFullName": "John Doe",
                "lblUserIdValue": "TDU34256987",
                "lblUsername": "USERNAME"
            }, {
                "btnAdd": "Add",
                "lblFullName": "John Doe",
                "lblUserIdValue": "TDU34256987",
                "lblUsername": "USERNAME"
            }, {
                "btnAdd": "Add",
                "lblFullName": "John Doe",
                "lblUserIdValue": "TDU34256987",
                "lblUsername": "USERNAME"
            }, {
                "btnAdd": "Add",
                "lblFullName": "John Doe",
                "lblUserIdValue": "TDU34256987",
                "lblUsername": "USERNAME"
            }, {
                "btnAdd": "Add",
                "lblFullName": "John Doe",
                "lblUserIdValue": "TDU34256987",
                "lblUsername": "USERNAME"
            }, {
                "btnAdd": "Add",
                "lblFullName": "John Doe",
                "lblUserIdValue": "TDU34256987",
                "lblUsername": "USERNAME"
            }, {
                "btnAdd": "Add",
                "lblFullName": "John Doe",
                "lblUserIdValue": "TDU34256987",
                "lblUsername": "USERNAME"
            }, {
                "btnAdd": "Add",
                "lblFullName": "John Doe",
                "lblUserIdValue": "TDU34256987",
                "lblUsername": "USERNAME"
            }, {
                "btnAdd": "Add",
                "lblFullName": "John Doe",
                "lblUserIdValue": "TDU34256987",
                "lblUsername": "USERNAME"
            }],
            "groupCells": false,
            "height": "100%",
            "id": "segAddOptions",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknsegffffffOp100Bordere1e5ed",
            "rowTemplate": "flxAddUsers",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": true,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "btnAdd": "btnAdd",
                "flxAddUsers": "flxAddUsers",
                "flxAddUsersWrapper": "flxAddUsersWrapper",
                "flxxUsernameWrapper": "flxxUsernameWrapper",
                "lblFullName": "lblFullName",
                "lblUserIdValue": "lblUserIdValue",
                "lblUsername": "lblUsername"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segAddOptions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAddOptions"), extendConfig({}, controller.args[2], "segAddOptions"));
        var rtxAvailableOptionsMessage = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxAvailableOptionsMessage",
            "isVisible": true,
            "skin": "sknRtxLato84939e12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "rtxAvailableOptionsMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxAvailableOptionsMessage"), extendConfig({}, controller.args[2], "rtxAvailableOptionsMessage"));
        flxAddOptionsSegment.add(segAddOptions, rtxAvailableOptionsMessage);
        flxAvailableOptions.add(lblAvailableOptionsHeading, btnSelectAll, flxSearch, flxAddOptionsSegment);
        flxAvailableOptionsWrapper.add(flxAvailableOptions);
        var flxSelectedOptionsWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxSelectedOptionsWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "20px",
            "width": "42.85%",
            "zIndex": 1
        }, controller.args[0], "flxSelectedOptionsWrapper"), extendConfig({}, controller.args[1], "flxSelectedOptionsWrapper"), extendConfig({}, controller.args[2], "flxSelectedOptionsWrapper"));
        flxSelectedOptionsWrapper.setDefaultUnit(kony.flex.DP);
        var flxSelectedOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSelectedOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "17px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxSelectedOptions"), extendConfig({}, controller.args[1], "flxSelectedOptions"), extendConfig({}, controller.args[2], "flxSelectedOptions"));
        flxSelectedOptions.setDefaultUnit(kony.flex.DP);
        var lblSelectedOption = new kony.ui.Label(extendConfig({
            "id": "lblSelectedOption",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SelectedUsers\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectedOption"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedOption"), extendConfig({}, controller.args[2], "lblSelectedOption"));
        var btnRemoveAll = new kony.ui.Button(extendConfig({
            "id": "btnRemoveAll",
            "isVisible": true,
            "right": "0px",
            "skin": "sknBtnLato11ABEB11Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SelectedOptions.RemoveAll\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnRemoveAll"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnRemoveAll"), extendConfig({}, controller.args[2], "btnRemoveAll"));
        var flxSegSelectedOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20px",
            "clipBounds": true,
            "id": "flxSegSelectedOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf9f9f9NoBorder",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSegSelectedOptions"), extendConfig({}, controller.args[1], "flxSegSelectedOptions"), extendConfig({}, controller.args[2], "flxSegSelectedOptions"));
        flxSegSelectedOptions.setDefaultUnit(kony.flex.DP);
        var segSelectedOptions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "imgClose": "",
                "lblOption": ""
            }],
            "groupCells": false,
            "height": "100%",
            "id": "segSelectedOptions",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowSkin": "sknsegf9f9f9Op100",
            "rowTemplate": "flxOptionAdded",
            "sectionHeaderSkin": "sliPhoneSegmentHeader0h3448bdd9d8941",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "separatorThickness": 0,
            "showScrollbars": true,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAddOptionWrapper": "flxAddOptionWrapper",
                "flxClose": "flxClose",
                "flxOptionAdded": "flxOptionAdded",
                "imgClose": "imgClose",
                "lblOption": "lblOption"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segSelectedOptions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segSelectedOptions"), extendConfig({}, controller.args[2], "segSelectedOptions"));
        var rtxSelectedOptionsMessage = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxSelectedOptionsMessage",
            "isVisible": true,
            "skin": "sknRtxLato84939e12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxSelectedOptionsMessage\")",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "rtxSelectedOptionsMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxSelectedOptionsMessage"), extendConfig({}, controller.args[2], "rtxSelectedOptionsMessage"));
        flxSegSelectedOptions.add(segSelectedOptions, rtxSelectedOptionsMessage);
        flxSelectedOptions.add(lblSelectedOption, btnRemoveAll, flxSegSelectedOptions);
        flxSelectedOptionsWrapper.add(flxSelectedOptions);
        addAndRemoveOptions.add(flxAvailableOptionsWrapper, flxSelectedOptionsWrapper);
        return addAndRemoveOptions;
    }
})