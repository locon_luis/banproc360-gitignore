define("com/adminConsole/common/addAndRemoveOptions/useraddAndRemoveOptionsController", function() {
    return {};
});
define("com/adminConsole/common/addAndRemoveOptions/addAndRemoveOptionsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/common/addAndRemoveOptions/addAndRemoveOptionsController", ["com/adminConsole/common/addAndRemoveOptions/useraddAndRemoveOptionsController", "com/adminConsole/common/addAndRemoveOptions/addAndRemoveOptionsControllerActions"], function() {
    var controller = require("com/adminConsole/common/addAndRemoveOptions/useraddAndRemoveOptionsController");
    var actions = require("com/adminConsole/common/addAndRemoveOptions/addAndRemoveOptionsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
