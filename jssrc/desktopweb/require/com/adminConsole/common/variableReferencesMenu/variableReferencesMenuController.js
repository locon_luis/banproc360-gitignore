define("com/adminConsole/common/variableReferencesMenu/uservariableReferencesMenuController", function() {
    return {};
});
define("com/adminConsole/common/variableReferencesMenu/variableReferencesMenuControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/variableReferencesMenu/variableReferencesMenuController", ["com/adminConsole/common/variableReferencesMenu/uservariableReferencesMenuController", "com/adminConsole/common/variableReferencesMenu/variableReferencesMenuControllerActions"], function() {
    var controller = require("com/adminConsole/common/variableReferencesMenu/uservariableReferencesMenuController");
    var actions = require("com/adminConsole/common/variableReferencesMenu/variableReferencesMenuControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
