define("com/adminConsole/common/statusFilterMenu/userstatusFilterMenuController", function() {
    return {};
});
define("com/adminConsole/common/statusFilterMenu/statusFilterMenuControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/statusFilterMenu/statusFilterMenuController", ["com/adminConsole/common/statusFilterMenu/userstatusFilterMenuController", "com/adminConsole/common/statusFilterMenu/statusFilterMenuControllerActions"], function() {
    var controller = require("com/adminConsole/common/statusFilterMenu/userstatusFilterMenuController");
    var actions = require("com/adminConsole/common/statusFilterMenu/statusFilterMenuControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
