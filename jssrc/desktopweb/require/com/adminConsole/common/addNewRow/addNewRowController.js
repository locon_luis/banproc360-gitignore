define("com/adminConsole/common/addNewRow/useraddNewRowController", function() {
    return {};
});
define("com/adminConsole/common/addNewRow/addNewRowControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/addNewRow/addNewRowController", ["com/adminConsole/common/addNewRow/useraddNewRowController", "com/adminConsole/common/addNewRow/addNewRowControllerActions"], function() {
    var controller = require("com/adminConsole/common/addNewRow/useraddNewRowController");
    var actions = require("com/adminConsole/common/addNewRow/addNewRowControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
