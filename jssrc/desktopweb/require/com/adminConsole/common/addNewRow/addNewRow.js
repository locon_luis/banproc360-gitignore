define(function() {
    return function(controller) {
        var addNewRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "addNewRow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "addNewRow"), extendConfig({}, controller.args[1], "addNewRow"), extendConfig({}, controller.args[2], "addNewRow"));
        addNewRow.setDefaultUnit(kony.flex.DP);
        var flxSegmentWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxSegmentWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSegmentWrapper"), extendConfig({}, controller.args[1], "flxSegmentWrapper"), extendConfig({}, controller.args[2], "flxSegmentWrapper"));
        flxSegmentWrapper.setDefaultUnit(kony.flex.DP);
        var segData = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "SwitchActive": {
                    "selectedIndex": 0
                },
                "SwitchActiveStatus": {
                    "selectedIndex": 0
                },
                "lblActive": "Active",
                "lblActiveStatus": "Active",
                "lblSeparator": ".",
                "txtbxEmailid": {
                    "placeholder": "",
                    "text": ""
                },
                "txtbxPhoneNumber": {
                    "placeholder": "",
                    "text": ""
                }
            }],
            "groupCells": false,
            "id": "segData",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "0dp",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxCustomerInformation",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "SwitchActive": "SwitchActive",
                "SwitchActiveStatus": "SwitchActiveStatus",
                "flxCustomerInformation": "flxCustomerInformation",
                "lblActive": "lblActive",
                "lblActiveStatus": "lblActiveStatus",
                "lblSeparator": "lblSeparator",
                "txtbxEmailid": "txtbxEmailid",
                "txtbxPhoneNumber": "txtbxPhoneNumber"
            },
            "zIndex": 1
        }, controller.args[0], "segData"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segData"), extendConfig({}, controller.args[2], "segData"));
        flxSegmentWrapper.add(segData);
        var btnAddMore = new kony.ui.Button(extendConfig({
            "bottom": "10px",
            "focusSkin": "btnClick",
            "id": "btnAddMore",
            "isVisible": true,
            "left": "75dp",
            "skin": "btnClick",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.btnAddMore\")",
            "top": "15px",
            "width": "4%",
            "zIndex": 1
        }, controller.args[0], "btnAddMore"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddMore"), extendConfig({
            "hoverSkin": "btnClick"
        }, controller.args[2], "btnAddMore"));
        addNewRow.add(flxSegmentWrapper, btnAddMore);
        return addNewRow;
    }
})