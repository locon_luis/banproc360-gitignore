define(function() {
    return function(controller) {
        var dataEntry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "85dp",
            "id": "dataEntry",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "dataEntry"), extendConfig({}, controller.args[1], "dataEntry"), extendConfig({}, controller.args[2], "dataEntry"));
        dataEntry.setDefaultUnit(kony.flex.DP);
        var flxDataEntry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "85dp",
            "id": "flxDataEntry",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": 0,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataEntry"), extendConfig({}, controller.args[1], "flxDataEntry"), extendConfig({}, controller.args[2], "flxDataEntry"));
        flxDataEntry.setDefaultUnit(kony.flex.DP);
        var lblData = new kony.ui.Label(extendConfig({
            "id": "lblData",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbl485c7514px",
            "text": "Enter Text",
            "top": "0dp",
            "width": "140dp",
            "zIndex": 1
        }, controller.args[0], "lblData"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData"), extendConfig({}, controller.args[2], "lblData"));
        var lblTextCounter = new kony.ui.Label(extendConfig({
            "id": "lblTextCounter",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknlbl485c7514px",
            "text": "0/35",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "lblTextCounter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTextCounter"), extendConfig({}, controller.args[2], "lblTextCounter"));
        var tbxData = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntbxLato35475f14px",
            "height": "40dp",
            "id": "tbxData",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": null,
            "secureTextEntry": false,
            "skin": "skntbxLato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxData"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxData"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxData"));
        var flxError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "68dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxError"), extendConfig({}, controller.args[1], "flxError"), extendConfig({}, controller.args[2], "flxError"));
        flxError.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon"), extendConfig({}, controller.args[2], "lblErrorIcon"));
        var lblErrorMsg = new kony.ui.Label(extendConfig({
            "id": "lblErrorMsg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Invalid data",
            "top": "0dp",
            "width": "170dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorMsg"), extendConfig({}, controller.args[2], "lblErrorMsg"));
        flxError.add(lblErrorIcon, lblErrorMsg);
        flxDataEntry.add(lblData, lblTextCounter, tbxData, flxError);
        dataEntry.add(flxDataEntry);
        return dataEntry;
    }
})