define(function() {
    return function(controller) {
        var toastMessageWithWarning = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "70px",
            "id": "toastMessageWithWarning",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "toastMessageWithWarning"), extendConfig({}, controller.args[1], "toastMessageWithWarning"), extendConfig({}, controller.args[2], "toastMessageWithWarning"));
        toastMessageWithWarning.setDefaultUnit(kony.flex.DP);
        var flxToastContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "45px",
            "id": "flxToastContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "sknFlxWarningToastBorderCF9C37",
            "top": "0%",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "flxToastContainer"), extendConfig({}, controller.args[1], "flxToastContainer"), extendConfig({}, controller.args[2], "flxToastContainer"));
        flxToastContainer.setDefaultUnit(kony.flex.DP);
        var flxMessageWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMessageWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "55dp",
            "isModalContainer": false,
            "right": "45px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxMessageWrapper"), extendConfig({}, controller.args[1], "flxMessageWrapper"), extendConfig({}, controller.args[2], "flxMessageWrapper"));
        flxMessageWrapper.setDefaultUnit(kony.flex.DP);
        var lblToastMessageLeft = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "30px",
            "id": "lblToastMessageLeft",
            "isVisible": true,
            "left": "0px",
            "skin": "lblLato14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblToastMessageLeft\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblToastMessageLeft"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblToastMessageLeft"), extendConfig({}, controller.args[2], "lblToastMessageLeft"));
        var btnToastLink = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "slButtonGlossRed",
            "id": "btnToastLink",
            "isVisible": false,
            "left": "3px",
            "right": "3px",
            "skin": "sknBtnLatoffffff14pxNoBorder",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.btnClickHere\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnToastLink"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnToastLink"), extendConfig({}, controller.args[2], "btnToastLink"));
        var lblToastMessageRight = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "30px",
            "id": "lblToastMessageRight",
            "isVisible": true,
            "left": "0px",
            "skin": "lblLato14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblToastMessageRight\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblToastMessageRight"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblToastMessageRight"), extendConfig({}, controller.args[2], "lblToastMessageRight"));
        flxMessageWrapper.add(lblToastMessageLeft, btnToastLink, lblToastMessageRight);
        var flxRightImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35px",
            "id": "flxRightImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 5,
            "skin": "slFbox",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxRightImage"), extendConfig({}, controller.args[1], "flxRightImage"), extendConfig({}, controller.args[2], "flxRightImage"));
        flxRightImage.setDefaultUnit(kony.flex.DP);
        var lblIconRight = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "18dp",
            "id": "lblIconRight",
            "isVisible": true,
            "skin": "sknIcon18pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "18dp",
            "zIndex": 1
        }, controller.args[0], "lblIconRight"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconRight"), extendConfig({}, controller.args[2], "lblIconRight"));
        flxRightImage.add(lblIconRight);
        var flxLeftImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeftImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxWarningToastBgCF9C37Radius0px",
            "top": "0dp",
            "width": "50dp"
        }, controller.args[0], "flxLeftImage"), extendConfig({}, controller.args[1], "flxLeftImage"), extendConfig({}, controller.args[2], "flxLeftImage"));
        flxLeftImage.setDefaultUnit(kony.flex.DP);
        var fontIconImgLeft = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "fontIconImgLeft",
            "isVisible": true,
            "skin": "sknIcon20pxWhite",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "fontIconImgLeft"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgLeft"), extendConfig({}, controller.args[2], "fontIconImgLeft"));
        flxLeftImage.add(fontIconImgLeft);
        flxToastContainer.add(flxMessageWrapper, flxRightImage, flxLeftImage);
        toastMessageWithWarning.add(flxToastContainer);
        return toastMessageWithWarning;
    }
})