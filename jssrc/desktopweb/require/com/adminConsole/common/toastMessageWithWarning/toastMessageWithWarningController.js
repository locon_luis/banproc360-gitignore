define("com/adminConsole/common/toastMessageWithWarning/usertoastMessageWithWarningController", function() {
    return {};
});
define("com/adminConsole/common/toastMessageWithWarning/toastMessageWithWarningControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/toastMessageWithWarning/toastMessageWithWarningController", ["com/adminConsole/common/toastMessageWithWarning/usertoastMessageWithWarningController", "com/adminConsole/common/toastMessageWithWarning/toastMessageWithWarningControllerActions"], function() {
    var controller = require("com/adminConsole/common/toastMessageWithWarning/usertoastMessageWithWarningController");
    var actions = require("com/adminConsole/common/toastMessageWithWarning/toastMessageWithWarningControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
