define(function() {
    return function(controller) {
        var pagination1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "pagination1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "pagination1"), extendConfig({}, controller.args[1], "pagination1"), extendConfig({}, controller.args[2], "pagination1"));
        pagination1.setDefaultUnit(kony.flex.DP);
        var flxPaginationWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 20,
            "clipBounds": false,
            "height": "30px",
            "id": "flxPaginationWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox0d9c3974835234d",
            "top": 20,
            "width": "222px",
            "zIndex": 1
        }, controller.args[0], "flxPaginationWrapper"), extendConfig({}, controller.args[1], "flxPaginationWrapper"), extendConfig({}, controller.args[2], "flxPaginationWrapper"));
        flxPaginationWrapper.setDefaultUnit(kony.flex.DP);
        var lbxPagination = new kony.ui.ListBox(extendConfig({
            "height": "30dp",
            "id": "lbxPagination",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["lb1", "Page 1 of 20"],
                ["lb2", "Page 2 of 20"],
                ["lb3", "Page 3 of 20"],
                ["lb4", "Page 4 of 20"]
            ],
            "selectedKey": "lb1",
            "selectedKeyValue": ["lb1", "Page 1 of 20"],
            "skin": "sknlbxNobgNoBorderPagination",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lbxPagination"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbxPagination"), extendConfig({
            "hoverSkin": "lstboxcursor",
            "multiSelect": false
        }, controller.args[2], "lbxPagination"));
        var flxPaginationSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxPaginationSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "2dp",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxPaginationSeperator"), extendConfig({}, controller.args[1], "flxPaginationSeperator"), extendConfig({}, controller.args[2], "flxPaginationSeperator"));
        flxPaginationSeperator.setDefaultUnit(kony.flex.DP);
        flxPaginationSeperator.add();
        var flxPrevious = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "32dp",
            "id": "flxPrevious",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "right": "3dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "32px",
            "zIndex": 1
        }, controller.args[0], "flxPrevious"), extendConfig({}, controller.args[1], "flxPrevious"), extendConfig({}, controller.args[2], "flxPrevious"));
        flxPrevious.setDefaultUnit(kony.flex.DP);
        var lblIconPrevious = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconPrevious",
            "isVisible": true,
            "skin": "sknFontIconPrevNextDisable",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconPrevious\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconPrevious"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconPrevious"), extendConfig({}, controller.args[2], "lblIconPrevious"));
        flxPrevious.add(lblIconPrevious);
        var flxNext = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "32dp",
            "id": "flxNext",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32px",
            "zIndex": 1
        }, controller.args[0], "flxNext"), extendConfig({}, controller.args[1], "flxNext"), extendConfig({}, controller.args[2], "flxNext"));
        flxNext.setDefaultUnit(kony.flex.DP);
        var lblIconNext = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconNext",
            "isVisible": true,
            "skin": "sknFontIconPrevNextDisable",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconNext\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconNext"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconNext"), extendConfig({}, controller.args[2], "lblIconNext"));
        flxNext.add(lblIconNext);
        flxPaginationWrapper.add(lbxPagination, flxPaginationSeperator, flxPrevious, flxNext);
        pagination1.add(flxPaginationWrapper);
        return pagination1;
    }
})