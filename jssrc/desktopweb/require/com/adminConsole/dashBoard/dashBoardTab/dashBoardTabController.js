define("com/adminConsole/dashBoard/dashBoardTab/userdashBoardTabController", function() {
    return {};
});
define("com/adminConsole/dashBoard/dashBoardTab/dashBoardTabControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/dashBoard/dashBoardTab/dashBoardTabController", ["com/adminConsole/dashBoard/dashBoardTab/userdashBoardTabController", "com/adminConsole/dashBoard/dashBoardTab/dashBoardTabControllerActions"], function() {
    var controller = require("com/adminConsole/dashBoard/dashBoardTab/userdashBoardTabController");
    var actions = require("com/adminConsole/dashBoard/dashBoardTab/dashBoardTabControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
