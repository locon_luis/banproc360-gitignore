define(function() {
    return function(controller) {
        var AdvancedSearchDropDown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "AdvancedSearchDropDown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_ab2e67b6a33d41fb9a7bfe3ada231b88(eventobject);
            },
            "skin": "sknNormalDefault",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 100
        }, controller.args[0], "AdvancedSearchDropDown"), extendConfig({}, controller.args[1], "AdvancedSearchDropDown"), extendConfig({}, controller.args[2], "AdvancedSearchDropDown"));
        AdvancedSearchDropDown.setDefaultUnit(kony.flex.DP);
        var flxDropDown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "167px",
            "id": "flxDropDown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxffffffBorderd6dbe7Radius4px",
            "top": "0px",
            "width": "100%",
            "zIndex": 200
        }, controller.args[0], "flxDropDown"), extendConfig({}, controller.args[1], "flxDropDown"), extendConfig({}, controller.args[2], "flxDropDown"));
        flxDropDown.setDefaultUnit(kony.flex.DP);
        var flxSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "isModalContainer": false,
            "right": "10px",
            "skin": "CopyslFbox2",
            "top": "2px",
            "zIndex": 100
        }, controller.args[0], "flxSearch"), extendConfig({}, controller.args[1], "flxSearch"), extendConfig({}, controller.args[2], "flxSearch"));
        flxSearch.setDefaultUnit(kony.flex.DP);
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxffffffBorderd6dbe7Radius4px",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var flxSearchCancel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "12px",
            "id": "flxSearchCancel",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxSearchCancel"), extendConfig({}, controller.args[1], "flxSearchCancel"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxSearchCancel"));
        flxSearchCancel.setDefaultUnit(kony.flex.DP);
        var fontIconClearSearch = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconClearSearch",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknfontIconSearch",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconClearSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconClearSearch"), extendConfig({}, controller.args[2], "fontIconClearSearch"));
        flxSearchCancel.add(fontIconClearSearch);
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "46%",
            "height": "30px",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "35dp",
            "placeholder": "Search",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var fontIconSearch = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSearch",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknFontIconSearchCross20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSearch"), extendConfig({}, controller.args[2], "fontIconSearch"));
        flxSearchContainer.add(flxSearchCancel, tbxSearchBox, fontIconSearch);
        flxSearch.add(flxSearchContainer);
        var flxPopupBody = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "height": "125px",
            "id": "flxPopupBody",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "42dp",
            "width": "100%",
            "zIndex": 100
        }, controller.args[0], "flxPopupBody"), extendConfig({}, controller.args[1], "flxPopupBody"), extendConfig({}, controller.args[2], "flxPopupBody"));
        flxPopupBody.setDefaultUnit(kony.flex.DP);
        var sgmentData = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "KL098234 - Westheimer"
            }],
            "groupCells": false,
            "height": "125dp",
            "id": "sgmentData",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": true,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxSearchDropDown",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkbox.png"
            },
            "separatorColor": "64646400",
            "separatorRequired": true,
            "separatorThickness": 0,
            "showScrollbars": true,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCheckBox": "flxCheckBox",
                "flxSearchDropDown": "flxSearchDropDown",
                "imgCheckBox": "imgCheckBox",
                "lblDescription": "lblDescription"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "sgmentData"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "sgmentData"), extendConfig({}, controller.args[2], "sgmentData"));
        var richTexNoResult = new kony.ui.RichText(extendConfig({
            "centerY": "50%",
            "height": "30dp",
            "id": "richTexNoResult",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknRtxLato84939e12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNorecords\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "richTexNoResult"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "richTexNoResult"), extendConfig({}, controller.args[2], "richTexNoResult"));
        flxPopupBody.add(sgmentData, richTexNoResult);
        flxDropDown.add(flxSearch, flxPopupBody);
        AdvancedSearchDropDown.add(flxDropDown);
        return AdvancedSearchDropDown;
    }
})