define(function() {
    return function(controller) {
        var SelectScheduler = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "SelectScheduler",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBg000000Op50",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "SelectScheduler"), extendConfig({}, controller.args[1], "SelectScheduler"), extendConfig({}, controller.args[2], "SelectScheduler"));
        SelectScheduler.setDefaultUnit(kony.flex.DP);
        var flxPopUp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": false,
            "height": "500dp",
            "id": "flxPopUp",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox0j9f841cc563e4e",
            "top": "120px",
            "width": "710px",
            "zIndex": 10
        }, controller.args[0], "flxPopUp"), extendConfig({}, controller.args[1], "flxPopUp"), extendConfig({}, controller.args[2], "flxPopUp"));
        flxPopUp.setDefaultUnit(kony.flex.DP);
        var flxPopUpTopColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxPopUpTopColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflx11abeb",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpTopColor"), extendConfig({}, controller.args[1], "flxPopUpTopColor"), extendConfig({}, controller.args[2], "flxPopUpTopColor"));
        flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
        flxPopUpTopColor.add();
        var flxPopupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0d9c3974835234d",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopupHeader"), extendConfig({}, controller.args[1], "flxPopupHeader"), extendConfig({}, controller.args[2], "flxPopupHeader"));
        flxPopupHeader.setDefaultUnit(kony.flex.DP);
        var flxPopUpClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxPopUpClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "slFbox",
            "top": "15dp",
            "width": "25px",
            "zIndex": 20
        }, controller.args[0], "flxPopUpClose"), extendConfig({}, controller.args[1], "flxPopUpClose"), extendConfig({}, controller.args[2], "flxPopUpClose"));
        flxPopUpClose.setDefaultUnit(kony.flex.DP);
        var imgPopUpClose = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgPopUpClose",
            "isVisible": true,
            "skin": "slImage",
            "src": "close_blue.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgPopUpClose"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgPopUpClose"), extendConfig({}, controller.args[2], "imgPopUpClose"));
        flxPopUpClose.add(imgPopUpClose);
        var lblPopUpMainMessage = new kony.ui.Label(extendConfig({
            "id": "lblPopUpMainMessage",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f23px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SelectScheduleMaster\")",
            "top": "0px",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblPopUpMainMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpMainMessage"), extendConfig({}, controller.args[2], "lblPopUpMainMessage"));
        flxPopupHeader.add(flxPopUpClose, lblPopUpMainMessage);
        var flxPopupBody = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "350dp",
            "id": "flxPopupBody",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopupBody"), extendConfig({}, controller.args[1], "flxPopupBody"), extendConfig({}, controller.args[2], "flxPopupBody"));
        flxPopupBody.setDefaultUnit(kony.flex.DP);
        var flxbodyMain = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "330dp",
            "id": "flxbodyMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": 20,
            "skin": "sknflxBorder",
            "top": "10dp",
            "zIndex": 1
        }, controller.args[0], "flxbodyMain"), extendConfig({}, controller.args[1], "flxbodyMain"), extendConfig({}, controller.args[2], "flxbodyMain"));
        flxbodyMain.setDefaultUnit(kony.flex.DP);
        var flxMasterList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "330dp",
            "id": "flxMasterList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf2f2f2",
            "top": "0dp",
            "width": "280dp",
            "zIndex": 1
        }, controller.args[0], "flxMasterList"), extendConfig({}, controller.args[1], "flxMasterList"), extendConfig({}, controller.args[2], "flxMasterList"));
        flxMasterList.setDefaultUnit(kony.flex.DP);
        var lblMasterList = new kony.ui.Label(extendConfig({
            "id": "lblMasterList",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.MasterList\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMasterList"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMasterList"), extendConfig({}, controller.args[2], "lblMasterList"));
        var flxSeparator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "46dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator"), extendConfig({}, controller.args[1], "flxSeparator"), extendConfig({}, controller.args[2], "flxSeparator"));
        flxSeparator.setDefaultUnit(kony.flex.DP);
        flxSeparator.add();
        var segMasterList = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "data": [{
                "imgCheckbox": "radio_notselected.png",
                "lblSchedulerName": "24x7 Scheduler"
            }, {
                "imgCheckbox": "radioselected_2x.png",
                "lblSchedulerName": "Bi-Weekly"
            }],
            "groupCells": false,
            "id": "segMasterList",
            "isVisible": true,
            "left": "15dp",
            "needPageIndicator": true,
            "onRowClick": controller.AS_Segment_b767df7c8e0647a087391c42e1b9d453,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "15dp",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "segf2f2f2noBorder",
            "rowTemplate": "flxMasterList",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "60dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCheckbox": "flxCheckbox",
                "flxMain": "flxMain",
                "flxMasterList": "flxMasterList",
                "imgCheckbox": "imgCheckbox",
                "lblSchedulerName": "lblSchedulerName"
            },
            "zIndex": 1
        }, controller.args[0], "segMasterList"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segMasterList"), extendConfig({}, controller.args[2], "segMasterList"));
        flxMasterList.add(lblMasterList, flxSeparator, segMasterList);
        var flxRight = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "330dp",
            "id": "flxRight",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "280dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxRight"), extendConfig({}, controller.args[1], "flxRight"), extendConfig({}, controller.args[2], "flxRight"));
        flxRight.setDefaultUnit(kony.flex.DP);
        var flxNoDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxNoDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoDetails"), extendConfig({}, controller.args[1], "flxNoDetails"), extendConfig({}, controller.args[2], "flxNoDetails"));
        flxNoDetails.setDefaultUnit(kony.flex.DP);
        var lblSelectSchedule = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblSelectSchedule",
            "isVisible": true,
            "skin": "sknlblLatoBold35475fFont14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SelectSchedule\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectSchedule"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectSchedule"), extendConfig({}, controller.args[2], "lblSelectSchedule"));
        flxNoDetails.add(lblSelectSchedule);
        var flxDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDetails",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetails"), extendConfig({}, controller.args[1], "flxDetails"), extendConfig({}, controller.args[2], "flxDetails"));
        flxDetails.setDefaultUnit(kony.flex.DP);
        var flxScheduleDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxScheduleDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxScheduleDetails"), extendConfig({}, controller.args[1], "flxScheduleDetails"), extendConfig({}, controller.args[2], "flxScheduleDetails"));
        flxScheduleDetails.setDefaultUnit(kony.flex.DP);
        var lblSchedulerName = new kony.ui.Label(extendConfig({
            "id": "lblSchedulerName",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.24x7_Scheduler\")",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSchedulerName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSchedulerName"), extendConfig({}, controller.args[2], "lblSchedulerName"));
        var flxSeparator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "46dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator2"), extendConfig({}, controller.args[1], "flxSeparator2"), extendConfig({}, controller.args[2], "flxSeparator2"));
        flxSeparator2.setDefaultUnit(kony.flex.DP);
        flxSeparator2.add();
        var lblWeekdays = new kony.ui.Label(extendConfig({
            "id": "lblWeekdays",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i8n.Group.Weekdays\")",
            "top": "60dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblWeekdays"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblWeekdays"), extendConfig({}, controller.args[2], "lblWeekdays"));
        var lblWeekdaysValue = new kony.ui.Label(extendConfig({
            "id": "lblWeekdaysValue",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSatValue\")",
            "top": "80dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblWeekdaysValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblWeekdaysValue"), extendConfig({}, controller.args[2], "lblWeekdaysValue"));
        var lblSatSun = new kony.ui.Label(extendConfig({
            "id": "lblSatSun",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SatSun\")",
            "top": "115dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSatSun"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSatSun"), extendConfig({}, controller.args[2], "lblSatSun"));
        var lblSatValue = new kony.ui.Label(extendConfig({
            "id": "lblSatValue",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSatValue\")",
            "top": "135dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSatValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSatValue"), extendConfig({}, controller.args[2], "lblSatValue"));
        var lblSunValue = new kony.ui.Label(extendConfig({
            "bottom": "10px",
            "id": "lblSunValue",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSatValue\")",
            "top": "155dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSunValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSunValue"), extendConfig({}, controller.args[2], "lblSunValue"));
        flxScheduleDetails.add(lblSchedulerName, flxSeparator2, lblWeekdays, lblWeekdaysValue, lblSatSun, lblSatValue, lblSunValue);
        var flxScheduleExceptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0dp",
            "clipBounds": true,
            "id": "flxScheduleExceptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxScheduleExceptions"), extendConfig({}, controller.args[1], "flxScheduleExceptions"), extendConfig({}, controller.args[2], "flxScheduleExceptions"));
        flxScheduleExceptions.setDefaultUnit(kony.flex.DP);
        var lblExceptions = new kony.ui.Label(extendConfig({
            "id": "lblExceptions",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Exceptions\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblExceptions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblExceptions"), extendConfig({}, controller.args[2], "lblExceptions"));
        var flxSeparator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "35dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator3"), extendConfig({}, controller.args[1], "flxSeparator3"), extendConfig({}, controller.args[2], "flxSeparator3"));
        flxSeparator3.setDefaultUnit(kony.flex.DP);
        flxSeparator3.add();
        var lblPublicHolidays = new kony.ui.Label(extendConfig({
            "id": "lblPublicHolidays",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblPublicHolidays\")",
            "top": "50dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPublicHolidays"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPublicHolidays"), extendConfig({}, controller.args[2], "lblPublicHolidays"));
        var lblGeneralHolidays = new kony.ui.Label(extendConfig({
            "id": "lblGeneralHolidays",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblGeneralHolidays\")",
            "top": "70dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGeneralHolidays"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGeneralHolidays"), extendConfig({}, controller.args[2], "lblGeneralHolidays"));
        var lblBankHolidays = new kony.ui.Label(extendConfig({
            "id": "lblBankHolidays",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblBankHolidays\")",
            "top": "90dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBankHolidays"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBankHolidays"), extendConfig({}, controller.args[2], "lblBankHolidays"));
        var lblNationalHolidays = new kony.ui.Label(extendConfig({
            "id": "lblNationalHolidays",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblNationalHolidays\")",
            "top": "110dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNationalHolidays"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNationalHolidays"), extendConfig({}, controller.args[2], "lblNationalHolidays"));
        flxScheduleExceptions.add(lblExceptions, flxSeparator3, lblPublicHolidays, lblGeneralHolidays, lblBankHolidays, lblNationalHolidays);
        flxDetails.add(flxScheduleDetails, flxScheduleExceptions);
        flxRight.add(flxNoDetails, flxDetails);
        flxbodyMain.add(flxMasterList, flxRight);
        flxPopupBody.add(flxbodyMain);
        var flxPopUpButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "80px",
            "id": "flxPopUpButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox0dc900c4572aa40",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpButtons"), extendConfig({}, controller.args[1], "flxPopUpButtons"), extendConfig({}, controller.args[2], "flxPopUpButtons"));
        flxPopUpButtons.setDefaultUnit(kony.flex.DP);
        var btnPopUpCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
            "height": "40px",
            "id": "btnPopUpCancel",
            "isVisible": true,
            "right": "150px",
            "skin": "sknBtnLatoRegulara5abc413pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
        }, controller.args[2], "btnPopUpCancel"));
        var btnPopUpAdd = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
            "height": "40px",
            "id": "btnPopUpAdd",
            "isVisible": true,
            "minWidth": "100px",
            "right": "20px",
            "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpAdd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpAdd"), extendConfig({
            "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
        }, controller.args[2], "btnPopUpAdd"));
        flxPopUpButtons.add(btnPopUpCancel, btnPopUpAdd);
        flxPopUp.add(flxPopUpTopColor, flxPopupHeader, flxPopupBody, flxPopUpButtons);
        SelectScheduler.add(flxPopUp);
        return SelectScheduler;
    }
})