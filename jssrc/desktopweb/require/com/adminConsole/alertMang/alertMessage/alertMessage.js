define(function() {
    return function(controller) {
        var alertMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "35px",
            "id": "alertMessage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknYellowBorder",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "alertMessage"), extendConfig({}, controller.args[1], "alertMessage"), extendConfig({}, controller.args[2], "alertMessage"));
        alertMessage.setDefaultUnit(kony.flex.DP);
        var flxLeftImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeftImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknYellowFill",
            "top": "0dp",
            "width": "50px"
        }, controller.args[0], "flxLeftImage"), extendConfig({}, controller.args[1], "flxLeftImage"), extendConfig({}, controller.args[2], "flxLeftImage"));
        flxLeftImage.setDefaultUnit(kony.flex.DP);
        var fontIconImgLeft = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "fontIconImgLeft",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknFontIconWhite",
            "text": "",
            "top": "15dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "fontIconImgLeft"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgLeft"), extendConfig({}, controller.args[2], "fontIconImgLeft"));
        flxLeftImage.add(fontIconImgLeft);
        var flxMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMessage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "flxMessage"), extendConfig({}, controller.args[1], "flxMessage"), extendConfig({}, controller.args[2], "flxMessage"));
        flxMessage.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 15,
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var lblData = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Alert notifications will not work as alert category is inactive currently!",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData"), extendConfig({}, controller.args[2], "lblData"));
        flxRow1.add(lblData);
        flxMessage.add(flxRow1);
        alertMessage.add(flxLeftImage, flxMessage);
        return alertMessage;
    }
})