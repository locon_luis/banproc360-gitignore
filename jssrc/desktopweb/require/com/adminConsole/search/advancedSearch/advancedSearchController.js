define("com/adminConsole/search/advancedSearch/useradvancedSearchController", function() {
    return {};
});
define("com/adminConsole/search/advancedSearch/advancedSearchControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/search/advancedSearch/advancedSearchController", ["com/adminConsole/search/advancedSearch/useradvancedSearchController", "com/adminConsole/search/advancedSearch/advancedSearchControllerActions"], function() {
    var controller = require("com/adminConsole/search/advancedSearch/useradvancedSearchController");
    var actions = require("com/adminConsole/search/advancedSearch/advancedSearchControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
