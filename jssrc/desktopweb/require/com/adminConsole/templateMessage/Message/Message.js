define(function() {
    return function(controller) {
        var Message = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "Message",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxffffffop0j3debc02cb1248resize",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "Message"), extendConfig({}, controller.args[1], "Message"), extendConfig({}, controller.args[2], "Message"));
        Message.setDefaultUnit(kony.flex.DP);
        var flxMessageHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxMessageHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflx2f4b6cborderDrag",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxMessageHeader"), extendConfig({}, controller.args[1], "flxMessageHeader"), extendConfig({}, controller.args[2], "flxMessageHeader"));
        flxMessageHeader.setDefaultUnit(kony.flex.DP);
        var lblMsg = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblMsg",
            "isVisible": true,
            "left": "20px",
            "skin": "slLabel0ea43a2a5545d46",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblMsg\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMsg"), extendConfig({}, controller.args[2], "lblMsg"));
        var flxButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxButtons",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "0%",
            "skin": "slFbox",
            "width": "80px",
            "zIndex": 1
        }, controller.args[0], "flxButtons"), extendConfig({}, controller.args[1], "flxButtons"), extendConfig({}, controller.args[2], "flxButtons"));
        flxButtons.setDefaultUnit(kony.flex.DP);
        var flxDraggable = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDraggable",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "skin": "sknCursorDragabble",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxDraggable"), extendConfig({}, controller.args[1], "flxDraggable"), extendConfig({}, controller.args[2], "flxDraggable"));
        flxDraggable.setDefaultUnit(kony.flex.DP);
        var imgDraggable = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "18px",
            "id": "imgDraggable",
            "isVisible": true,
            "skin": "slImage",
            "src": "move_2x.png",
            "width": "18px",
            "zIndex": 1
        }, controller.args[0], "imgDraggable"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDraggable"), extendConfig({}, controller.args[2], "imgDraggable"));
        flxDraggable.add(imgDraggable);
        var flxMaximize = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxMaximize",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxMaximize"), extendConfig({}, controller.args[1], "flxMaximize"), extendConfig({}, controller.args[2], "flxMaximize"));
        flxMaximize.setDefaultUnit(kony.flex.DP);
        var lblMaximize = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "lblMaximize",
            "isVisible": true,
            "left": "1dp",
            "skin": "sknIcon18pxWhiteNormal",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblMaximize\")",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblMaximize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaximize"), extendConfig({
            "toolTip": "Maximize"
        }, controller.args[2], "lblMaximize"));
        flxMaximize.add(lblMaximize);
        var flxMinimize = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxMinimize",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxMinimize"), extendConfig({}, controller.args[1], "flxMinimize"), extendConfig({}, controller.args[2], "flxMinimize"));
        flxMinimize.setDefaultUnit(kony.flex.DP);
        var lblMinimize = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "lblMinimize",
            "isVisible": true,
            "left": "1dp",
            "skin": "sknIcon18pxWhiteNormal",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblMinimize\")",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblMinimize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMinimize"), extendConfig({
            "toolTip": "Minimize"
        }, controller.args[2], "lblMinimize"));
        flxMinimize.add(lblMinimize);
        var flxClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "12px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxClose"), extendConfig({}, controller.args[1], "flxClose"), extendConfig({}, controller.args[2], "flxClose"));
        flxClose.setDefaultUnit(kony.flex.DP);
        var lblClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "lblClose",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon18pxWhiteNormal",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblClose"), extendConfig({}, controller.args[2], "lblClose"));
        flxClose.add(lblClose);
        flxButtons.add(flxDraggable, flxMaximize, flxMinimize, flxClose);
        var lblSeperator = new kony.ui.Label(extendConfig({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": false,
            "left": "0px",
            "right": "0px",
            "skin": "sknSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        flxMessageHeader.add(lblMsg, flxButtons, lblSeperator);
        var flxBody = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBody",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "skne5e8ef",
            "top": "41dp",
            "width": "100%",
            "zIndex": 100
        }, controller.args[0], "flxBody"), extendConfig({}, controller.args[1], "flxBody"), extendConfig({}, controller.args[2], "flxBody"));
        flxBody.setDefaultUnit(kony.flex.DP);
        var flxTo = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "40px",
            "horizontalScrollIndicator": true,
            "id": "flxTo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "maxHeight": "80px",
            "minWidth": "40px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0dp",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTo"), extendConfig({}, controller.args[1], "flxTo"), extendConfig({}, controller.args[2], "flxTo"));
        flxTo.setDefaultUnit(kony.flex.DP);
        var lblTo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblTo",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblTo\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTo"), extendConfig({}, controller.args[2], "lblTo"));
        var flxUserGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxUserGroup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "140px"
        }, controller.args[0], "flxUserGroup"), extendConfig({}, controller.args[1], "flxUserGroup"), extendConfig({}, controller.args[2], "flxUserGroup"));
        flxUserGroup.setDefaultUnit(kony.flex.DP);
        var flxUser1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxUser1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0ica8ce013fdc40",
            "top": "10dp",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "flxUser1"), extendConfig({}, controller.args[1], "flxUser1"), extendConfig({}, controller.args[2], "flxUser1"));
        flxUser1.setDefaultUnit(kony.flex.DP);
        var lblUserName1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblUserName1",
            "isVisible": true,
            "left": "5dp",
            "skin": "CopyslLabel0d2920a8c52fc41",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblUserName1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUserName1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUserName1"), extendConfig({}, controller.args[2], "lblUserName1"));
        var imgCross1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "10dp",
            "id": "imgCross1",
            "isVisible": true,
            "right": "3px",
            "skin": "slImage",
            "src": "close_blue.png",
            "width": "10dp",
            "zIndex": 1
        }, controller.args[0], "imgCross1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCross1"), extendConfig({}, controller.args[2], "imgCross1"));
        var lblIconCross1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconCross1",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknIcon12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "top": "12dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconCross1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconCross1"), extendConfig({}, controller.args[2], "lblIconCross1"));
        flxUser1.add(lblUserName1, imgCross1, lblIconCross1);
        flxUserGroup.add(flxUser1);
        var txtTo = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "40px",
            "id": "txtTo",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "40px",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknMessagetxtbox",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0px",
            "zIndex": 10
        }, controller.args[0], "txtTo"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtTo"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtTo"));
        flxTo.add(lblTo, flxUserGroup, txtTo);
        var lblSeperator1 = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeperator1",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator1"), extendConfig({}, controller.args[2], "lblSeperator1"));
        var flxSubject = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSubject",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSubject"), extendConfig({}, controller.args[1], "flxSubject"), extendConfig({}, controller.args[2], "flxSubject"));
        flxSubject.setDefaultUnit(kony.flex.DP);
        var lblSubject = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSubject",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSubject\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSubject"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSubject"), extendConfig({}, controller.args[2], "lblSubject"));
        var txtSubject = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "40dp",
            "id": "txtSubject",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "70dp",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknMessagetxtbox",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "txtSubject"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSubject"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSubject"));
        var lblSeperator2 = new kony.ui.Label(extendConfig({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator2",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator2"), extendConfig({}, controller.args[2], "lblSeperator2"));
        flxSubject.add(lblSubject, txtSubject, lblSeperator2);
        var flxEmail = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxEmail",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEmail"), extendConfig({}, controller.args[1], "flxEmail"), extendConfig({}, controller.args[2], "flxEmail"));
        flxEmail.setDefaultUnit(kony.flex.DP);
        var lstbocCategory = new kony.ui.ListBox(extendConfig({
            "centerY": "50%",
            "height": "40dp",
            "id": "lstbocCategory",
            "isVisible": true,
            "left": "20px",
            "masterData": [
                ["lb1", "Select Category"],
                ["lb2", "Thank you message template"],
                ["lb3", "Ackonwladgement template"],
                ["lb4", "Custom template"]
            ],
            "selectedKey": "lb1",
            "selectedKeyValue": ["lb1", "Select Category"],
            "skin": "lstboxcursor",
            "width": "190px",
            "zIndex": 1
        }, controller.args[0], "lstbocCategory"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstbocCategory"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstbocCategory"));
        var lstboxMail = new kony.ui.ListBox(extendConfig({
            "centerY": "50%",
            "height": "40dp",
            "id": "lstboxMail",
            "isVisible": true,
            "left": "220px",
            "masterData": [
                ["lb1", "Select email template"],
                ["lb2", "Thank you message template"],
                ["lb3", "Ackonwladgement template"],
                ["lb4", "Custom template"]
            ],
            "selectedKey": "lb1",
            "selectedKeyValue": ["lb1", "Select email template"],
            "skin": "lstboxcursor",
            "width": "280px",
            "zIndex": 1
        }, controller.args[0], "lstboxMail"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxMail"), extendConfig({
            "hoverSkin": "lstboxcursor",
            "multiSelect": false
        }, controller.args[2], "lstboxMail"));
        var lblSeperator3 = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeperator3",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator3"), extendConfig({}, controller.args[2], "lblSeperator3"));
        flxEmail.add(lstbocCategory, lstboxMail, lblSeperator3);
        flxBody.add(flxTo, lblSeperator1, flxSubject, flxEmail);
        var flxContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "80px",
            "clipBounds": true,
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "162px",
            "width": "100%",
            "zIndex": 100
        }, controller.args[0], "flxContent"), extendConfig({}, controller.args[1], "flxContent"), extendConfig({}, controller.args[2], "flxContent"));
        flxContent.setDefaultUnit(kony.flex.DP);
        var rtxMessage = new kony.ui.Browser(extendConfig({
            "detectTelNumber": true,
            "enableZoom": false,
            "height": "100%",
            "id": "rtxMessage",
            "isVisible": true,
            "left": "0dp",
            "requestURLConfig": {
                "URL": "richtext.html",
                "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "rtxMessage"), extendConfig({}, controller.args[1], "rtxMessage"), extendConfig({}, controller.args[2], "rtxMessage"));
        flxContent.add(rtxMessage);
        var flxAttatchments = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "90px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "40px",
            "horizontalScrollIndicator": true,
            "id": "flxAttatchments",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 100
        }, controller.args[0], "flxAttatchments"), extendConfig({}, controller.args[1], "flxAttatchments"), extendConfig({}, controller.args[2], "flxAttatchments"));
        flxAttatchments.setDefaultUnit(kony.flex.DP);
        var flxAttatchment1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxAttatchment1",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknf8f9fabordere1e5ed",
            "top": "5px",
            "width": "45%",
            "zIndex": 1
        }, controller.args[0], "flxAttatchment1"), extendConfig({}, controller.args[1], "flxAttatchment1"), extendConfig({}, controller.args[2], "flxAttatchment1"));
        flxAttatchment1.setDefaultUnit(kony.flex.DP);
        var imgattatchment1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12px",
            "id": "imgattatchment1",
            "isVisible": false,
            "left": "5px",
            "skin": "slImage",
            "src": "attachment_2x.png",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgattatchment1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgattatchment1"), extendConfig({}, controller.args[2], "imgattatchment1"));
        var lblIconAttachment1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconAttachment1",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAttach\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconAttachment1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconAttachment1"), extendConfig({}, controller.args[2], "lblIconAttachment1"));
        var lblAttatchmnet1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAttatchmnet1",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAttatchmnet1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttatchmnet1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttatchmnet1"), extendConfig({}, controller.args[2], "lblAttatchmnet1"));
        var flxClose1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxClose1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "3px",
            "skin": "slFbox",
            "width": "20px"
        }, controller.args[0], "flxClose1"), extendConfig({}, controller.args[1], "flxClose1"), extendConfig({}, controller.args[2], "flxClose1"));
        flxClose1.setDefaultUnit(kony.flex.DP);
        var imgClose1 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "imgClose1",
            "isVisible": false,
            "skin": "slImage",
            "src": "close_blue.png",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "imgClose1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose1"), extendConfig({}, controller.args[2], "imgClose1"));
        var lblClose1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblClose1",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblClose1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblClose1"), extendConfig({}, controller.args[2], "lblClose1"));
        flxClose1.add(imgClose1, lblClose1);
        flxAttatchment1.add(imgattatchment1, lblIconAttachment1, lblAttatchmnet1, flxClose1);
        var flxAttatchment2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxAttatchment2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "52%",
            "isModalContainer": false,
            "skin": "sknf8f9fabordere1e5ed",
            "top": "5px",
            "width": "45%",
            "zIndex": 1
        }, controller.args[0], "flxAttatchment2"), extendConfig({}, controller.args[1], "flxAttatchment2"), extendConfig({}, controller.args[2], "flxAttatchment2"));
        flxAttatchment2.setDefaultUnit(kony.flex.DP);
        var imgattatchment2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12px",
            "id": "imgattatchment2",
            "isVisible": false,
            "left": "5px",
            "skin": "slImage",
            "src": "attachment_2x.png",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgattatchment2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgattatchment2"), extendConfig({}, controller.args[2], "imgattatchment2"));
        var lblIconAttachment2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconAttachment2",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAttach\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconAttachment2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconAttachment2"), extendConfig({}, controller.args[2], "lblIconAttachment2"));
        var lblAttatchmnet2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAttatchmnet2",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAttatchmnet1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttatchmnet2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttatchmnet2"), extendConfig({}, controller.args[2], "lblAttatchmnet2"));
        var flxClose2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxClose2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "3px",
            "skin": "slFbox",
            "width": "20px"
        }, controller.args[0], "flxClose2"), extendConfig({}, controller.args[1], "flxClose2"), extendConfig({}, controller.args[2], "flxClose2"));
        flxClose2.setDefaultUnit(kony.flex.DP);
        var imgClose2 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "imgClose2",
            "isVisible": false,
            "skin": "slImage",
            "src": "close_blue.png",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "imgClose2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose2"), extendConfig({}, controller.args[2], "imgClose2"));
        var lblClose2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblClose2",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblClose2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblClose2"), extendConfig({}, controller.args[2], "lblClose2"));
        flxClose2.add(imgClose2, lblClose2);
        flxAttatchment2.add(imgattatchment2, lblIconAttachment2, lblAttatchmnet2, flxClose2);
        var flxAttatchment3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxAttatchment3",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknf8f9fabordere1e5ed",
            "top": "45px",
            "width": "45%",
            "zIndex": 1
        }, controller.args[0], "flxAttatchment3"), extendConfig({}, controller.args[1], "flxAttatchment3"), extendConfig({}, controller.args[2], "flxAttatchment3"));
        flxAttatchment3.setDefaultUnit(kony.flex.DP);
        var imgattatchment3 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12px",
            "id": "imgattatchment3",
            "isVisible": false,
            "left": "5px",
            "skin": "slImage",
            "src": "attachment_2x.png",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgattatchment3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgattatchment3"), extendConfig({}, controller.args[2], "imgattatchment3"));
        var lblIconAttachment3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconAttachment3",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAttach\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconAttachment3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconAttachment3"), extendConfig({}, controller.args[2], "lblIconAttachment3"));
        var lblAttatchmnet3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAttatchmnet3",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAttatchmnet1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttatchmnet3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttatchmnet3"), extendConfig({}, controller.args[2], "lblAttatchmnet3"));
        var flxClose3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxClose3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "3px",
            "skin": "slFbox",
            "width": "20px"
        }, controller.args[0], "flxClose3"), extendConfig({}, controller.args[1], "flxClose3"), extendConfig({}, controller.args[2], "flxClose3"));
        flxClose3.setDefaultUnit(kony.flex.DP);
        var imgClose3 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "imgClose3",
            "isVisible": false,
            "skin": "slImage",
            "src": "close_blue.png",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "imgClose3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose3"), extendConfig({}, controller.args[2], "imgClose3"));
        var lblClose3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblClose3",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblClose3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblClose3"), extendConfig({}, controller.args[2], "lblClose3"));
        flxClose3.add(imgClose3, lblClose3);
        flxAttatchment3.add(imgattatchment3, lblIconAttachment3, lblAttatchmnet3, flxClose3);
        var flxAttatchment4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxAttatchment4",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "52%",
            "isModalContainer": false,
            "skin": "sknf8f9fabordere1e5ed",
            "top": "45px",
            "width": "45%",
            "zIndex": 1
        }, controller.args[0], "flxAttatchment4"), extendConfig({}, controller.args[1], "flxAttatchment4"), extendConfig({}, controller.args[2], "flxAttatchment4"));
        flxAttatchment4.setDefaultUnit(kony.flex.DP);
        var imgattatchment4 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12px",
            "id": "imgattatchment4",
            "isVisible": false,
            "left": "5px",
            "skin": "slImage",
            "src": "attachment_2x.png",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgattatchment4"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgattatchment4"), extendConfig({}, controller.args[2], "imgattatchment4"));
        var lblIconAttachment4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconAttachment4",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAttach\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconAttachment4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconAttachment4"), extendConfig({}, controller.args[2], "lblIconAttachment4"));
        var lblAttatchmnet4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAttatchmnet4",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAttatchmnet1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttatchmnet4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttatchmnet4"), extendConfig({}, controller.args[2], "lblAttatchmnet4"));
        var flxClose4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxClose4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "3px",
            "skin": "slFbox",
            "width": "20px"
        }, controller.args[0], "flxClose4"), extendConfig({}, controller.args[1], "flxClose4"), extendConfig({}, controller.args[2], "flxClose4"));
        flxClose4.setDefaultUnit(kony.flex.DP);
        var imgClose4 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "imgClose4",
            "isVisible": false,
            "skin": "slImage",
            "src": "close_blue.png",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "imgClose4"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose4"), extendConfig({}, controller.args[2], "imgClose4"));
        var lblClose4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblClose4",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblClose4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblClose4"), extendConfig({}, controller.args[2], "lblClose4"));
        flxClose4.add(imgClose4, lblClose4);
        flxAttatchment4.add(imgattatchment4, lblIconAttachment4, lblAttatchmnet4, flxClose4);
        var flxAttatchment5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxAttatchment5",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknf8f9fabordere1e5ed",
            "top": "85px",
            "width": "45%",
            "zIndex": 1
        }, controller.args[0], "flxAttatchment5"), extendConfig({}, controller.args[1], "flxAttatchment5"), extendConfig({}, controller.args[2], "flxAttatchment5"));
        flxAttatchment5.setDefaultUnit(kony.flex.DP);
        var imgattatchment5 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12px",
            "id": "imgattatchment5",
            "isVisible": false,
            "left": "5px",
            "skin": "slImage",
            "src": "attachment_2x.png",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgattatchment5"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgattatchment5"), extendConfig({}, controller.args[2], "imgattatchment5"));
        var lblIconAttachment5 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconAttachment5",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAttach\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconAttachment5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconAttachment5"), extendConfig({}, controller.args[2], "lblIconAttachment5"));
        var lblAttatchmnet5 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAttatchmnet5",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAttatchmnet1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttatchmnet5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttatchmnet5"), extendConfig({}, controller.args[2], "lblAttatchmnet5"));
        var flxClose5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxClose5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "3px",
            "skin": "slFbox",
            "width": "20px"
        }, controller.args[0], "flxClose5"), extendConfig({}, controller.args[1], "flxClose5"), extendConfig({}, controller.args[2], "flxClose5"));
        flxClose5.setDefaultUnit(kony.flex.DP);
        var imgClose5 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "imgClose5",
            "isVisible": false,
            "skin": "slImage",
            "src": "close_blue.png",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "imgClose5"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose5"), extendConfig({}, controller.args[2], "imgClose5"));
        var lblClose5 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblClose5",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblClose5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblClose5"), extendConfig({}, controller.args[2], "lblClose5"));
        flxClose5.add(imgClose5, lblClose5);
        flxAttatchment5.add(imgattatchment5, lblIconAttachment5, lblAttatchmnet5, flxClose5);
        flxAttatchments.add(flxAttatchment1, flxAttatchment2, flxAttatchment3, flxAttatchment4, flxAttatchment5);
        var flxErrorMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "80px",
            "clipBounds": true,
            "height": "40px",
            "id": "flxErrorMessage",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknerrorflx",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxErrorMessage"), extendConfig({}, controller.args[1], "flxErrorMessage"), extendConfig({}, controller.args[2], "flxErrorMessage"));
        flxErrorMessage.setDefaultUnit(kony.flex.DP);
        var lblIconOptions = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblIconOptions",
            "isVisible": true,
            "left": 20,
            "skin": "sknFontIconError",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblIconOptions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconOptions"), extendConfig({}, controller.args[2], "lblIconOptions"));
        var lblErrormsg = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblErrormsg",
            "isVisible": true,
            "left": "40px",
            "skin": "sknLabelRed",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblErrormsg\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrormsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrormsg"), extendConfig({}, controller.args[2], "lblErrormsg"));
        flxErrorMessage.add(lblIconOptions, lblErrormsg);
        var flxEditButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "80px",
            "id": "flxEditButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFootereceff3",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEditButtons"), extendConfig({}, controller.args[1], "flxEditButtons"), extendConfig({}, controller.args[2], "flxEditButtons"));
        flxEditButtons.setDefaultUnit(kony.flex.DP);
        var btnCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "slButtonGlossBlue0cf04ba12b8ce4d",
            "height": "40px",
            "id": "btnCancel",
            "isVisible": false,
            "left": "20px",
            "onClick": controller.AS_Button_d66222d93d9448b6a7f9c2c4379fe48a,
            "skin": "btnffffff",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.DISCARD\")",
            "top": "0%",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "btnCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCancel"), extendConfig({
            "hoverSkin": "btnf6f7f9latoRegular12px"
        }, controller.args[2], "btnCancel"));
        var btnsave = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "slButtonGlossBlue0b7ec2606d7d543",
            "height": "40dp",
            "id": "btnsave",
            "isVisible": true,
            "onClick": controller.AS_Button_b4266e96525f44b0b0c381cfa6f72174,
            "right": "20px",
            "skin": "btn24ace8latoregular12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.Send\")",
            "top": "0%",
            "width": "100dp",
            "zIndex": 1
        }, controller.args[0], "btnsave"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnsave"), extendConfig({
            "hoverSkin": "btn1782b2latoregular12px"
        }, controller.args[2], "btnsave"));
        var lblSaving = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSaving",
            "isVisible": true,
            "right": "150px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSaving\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSaving"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSaving"), extendConfig({}, controller.args[2], "lblSaving"));
        var flxDelete = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0%",
            "width": "21px",
            "zIndex": 1
        }, controller.args[0], "flxDelete"), extendConfig({}, controller.args[1], "flxDelete"), extendConfig({}, controller.args[2], "flxDelete"));
        flxDelete.setDefaultUnit(kony.flex.DP);
        var lblDelete = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDelete",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDelete"), extendConfig({}, controller.args[2], "lblDelete"));
        flxDelete.add(lblDelete);
        var flxFlagSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxFlagSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "60dp",
            "isModalContainer": false,
            "skin": "sknflxOuterBorderdfd9dd",
            "top": "0%",
            "width": "1px",
            "zIndex": 111
        }, controller.args[0], "flxFlagSeperator"), extendConfig({}, controller.args[1], "flxFlagSeperator"), extendConfig({}, controller.args[2], "flxFlagSeperator"));
        flxFlagSeperator.setDefaultUnit(kony.flex.DP);
        flxFlagSeperator.add();
        var flxAttatchment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxAttatchment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "80px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0%",
            "width": "22px",
            "zIndex": 1
        }, controller.args[0], "flxAttatchment"), extendConfig({}, controller.args[1], "flxAttatchment"), extendConfig({}, controller.args[2], "flxAttatchment"));
        flxAttatchment.setDefaultUnit(kony.flex.DP);
        var lblAttach = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAttach",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAttach\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttach"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttach"), extendConfig({}, controller.args[2], "lblAttach"));
        flxAttatchment.add(lblAttach);
        flxEditButtons.add(btnCancel, btnsave, lblSaving, flxDelete, flxFlagSeperator, flxAttatchment);
        var flxFilter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "240px",
            "id": "flxFilter",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "75px",
            "isModalContainer": false,
            "skin": "CopysknFlxffffffbordere0fa70ae09eb4f45",
            "top": "80px",
            "width": "200px",
            "zIndex": 200
        }, controller.args[0], "flxFilter"), extendConfig({}, controller.args[1], "flxFilter"), extendConfig({}, controller.args[2], "flxFilter"));
        flxFilter.setDefaultUnit(kony.flex.DP);
        var segFilterDropdown = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "data": [{
                "lblSeperator": "'",
                "lblUserName": "UserID:",
                "lblViewFullName": "Edward"
            }, {
                "lblSeperator": "'",
                "lblUserName": "UserID:",
                "lblViewFullName": "Edward"
            }],
            "groupCells": false,
            "height": "205px",
            "id": "segFilterDropdown",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "0px",
            "rowFocusSkin": "seg2Focus",
            "rowTemplate": "flxsegCustomerGroup",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkbox.png"
            },
            "separatorRequired": false,
            "showScrollbars": true,
            "top": 0,
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxFullName": "flxFullName",
                "flxUserName": "flxUserName",
                "flxsegCustomerGroup": "flxsegCustomerGroup",
                "lblSeperator": "lblSeperator",
                "lblUserName": "lblUserName",
                "lblViewFullName": "lblViewFullName"
            },
            "zIndex": 500
        }, controller.args[0], "segFilterDropdown"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segFilterDropdown"), extendConfig({}, controller.args[2], "segFilterDropdown"));
        var richtextNoResult = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20px",
            "id": "richtextNoResult",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknrichtext12pxlatoregular",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Messages.SuggestionsNoResultsFound\")",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "richtextNoResult"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "richtextNoResult"), extendConfig({}, controller.args[2], "richtextNoResult"));
        var richTxtTopSuggestions = new kony.ui.RichText(extendConfig({
            "bottom": 0,
            "height": "25px",
            "id": "richTxtTopSuggestions",
            "isVisible": true,
            "left": "0px",
            "skin": "sknrichtext12pxlatoregular",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Messages.TopSuggestions\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "richTxtTopSuggestions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "richTxtTopSuggestions"), extendConfig({}, controller.args[2], "richTxtTopSuggestions"));
        var flxCSRInnerLoading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxCSRInnerLoading",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": 0,
            "skin": "sknLoadingBlur",
            "top": "0px",
            "width": "100%",
            "zIndex": 600
        }, controller.args[0], "flxCSRInnerLoading"), extendConfig({}, controller.args[1], "flxCSRInnerLoading"), extendConfig({}, controller.args[2], "flxCSRInnerLoading"));
        flxCSRInnerLoading.setDefaultUnit(kony.flex.DP);
        var flxMessagesImageContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "75px",
            "id": "flxMessagesImageContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "75px",
            "zIndex": 1
        }, controller.args[0], "flxMessagesImageContainer"), extendConfig({}, controller.args[1], "flxMessagesImageContainer"), extendConfig({}, controller.args[2], "flxMessagesImageContainer"));
        flxMessagesImageContainer.setDefaultUnit(kony.flex.DP);
        var imgLoading = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "50px",
            "id": "imgLoading",
            "isVisible": true,
            "skin": "slImage",
            "src": "loadingscreenimage.gif",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "imgLoading"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgLoading"), extendConfig({}, controller.args[2], "imgLoading"));
        flxMessagesImageContainer.add(imgLoading);
        flxCSRInnerLoading.add(flxMessagesImageContainer);
        flxFilter.add(segFilterDropdown, richtextNoResult, richTxtTopSuggestions, flxCSRInnerLoading);
        Message.add(flxMessageHeader, flxBody, flxContent, flxAttatchments, flxErrorMessage, flxEditButtons, flxFilter);
        return Message;
    }
})