define("com/adminConsole/reports/search/usersearchController", function() {
    return {};
});
define("com/adminConsole/reports/search/searchControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/reports/search/searchController", ["com/adminConsole/reports/search/usersearchController", "com/adminConsole/reports/search/searchControllerActions"], function() {
    var controller = require("com/adminConsole/reports/search/usersearchController");
    var actions = require("com/adminConsole/reports/search/searchControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
