define(function() {
    return function(controller) {
        var search = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "80px",
            "id": "search",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlx",
            "top": "0dp"
        }, controller.args[0], "search"), extendConfig({}, controller.args[1], "search"), extendConfig({}, controller.args[2], "search"));
        search.setDefaultUnit(kony.flex.DP);
        var flxDateSelection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxDateSelection",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxDateSelection"), extendConfig({}, controller.args[1], "flxDateSelection"), extendConfig({}, controller.args[2], "flxDateSelection"));
        flxDateSelection.setDefaultUnit(kony.flex.DP);
        var flxDate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxDate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "200px",
            "zIndex": 1
        }, controller.args[0], "flxDate"), extendConfig({}, controller.args[1], "flxDate"), extendConfig({}, controller.args[2], "flxDate"));
        flxDate.setDefaultUnit(kony.flex.DP);
        var lblSelectDate = new kony.ui.Label(extendConfig({
            "id": "lblSelectDate",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Select_date\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectDate"), extendConfig({}, controller.args[2], "lblSelectDate"));
        var flxDateContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxDateContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknFlxCalendar",
            "top": "25px",
            "width": "200px",
            "zIndex": 1
        }, controller.args[0], "flxDateContainer"), extendConfig({}, controller.args[1], "flxDateContainer"), extendConfig({}, controller.args[2], "flxDateContainer"));
        flxDateContainer.setDefaultUnit(kony.flex.DP);
        flxDateContainer.add();
        flxDate.add(lblSelectDate, flxDateContainer);
        var flxCategory = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxCategory",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "220dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "200px",
            "zIndex": 1
        }, controller.args[0], "flxCategory"), extendConfig({}, controller.args[1], "flxCategory"), extendConfig({}, controller.args[2], "flxCategory"));
        flxCategory.setDefaultUnit(kony.flex.DP);
        var lblSelectCategory = new kony.ui.Label(extendConfig({
            "id": "lblSelectCategory",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblCategory\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectCategory"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectCategory"), extendConfig({}, controller.args[2], "lblSelectCategory"));
        var lstCategory = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstCategory",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["lb1", "Select category"],
                ["lb2", "General banking"],
                ["lb3", "Deposits"],
                ["Key1516491934", "Mobile banking"],
                ["Key3964983850", "Online banking"],
                ["Key702342914", "ATM"],
                ["Key39915979", "Customer service"]
            ],
            "selectedKey": "lb1",
            "selectedKeyValue": ["lb1", "Select category"],
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "25dp",
            "width": "200px",
            "zIndex": 1
        }, controller.args[0], "lstCategory"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstCategory"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstCategory"));
        flxCategory.add(lblSelectCategory, lstCategory);
        var flxCSR = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxCSR",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "440px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "200px",
            "zIndex": 1
        }, controller.args[0], "flxCSR"), extendConfig({}, controller.args[1], "flxCSR"), extendConfig({}, controller.args[2], "flxCSR"));
        flxCSR.setDefaultUnit(kony.flex.DP);
        var tbxCSR = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40px",
            "id": "tbxCSR",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "placeholder": "Select CSR name",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25px",
            "width": "200px",
            "zIndex": 1
        }, controller.args[0], "tbxCSR"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [5, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxCSR"), extendConfig({
            "autoCorrect": false,
            "hoverSkin": "skntxtbxDetailsFocus14px",
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxCSR"));
        var lblCSR = new kony.ui.Label(extendConfig({
            "id": "lblCSR",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCSR\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCSR"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCSR"), extendConfig({}, controller.args[2], "lblCSR"));
        var flxSuggestionsClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxSuggestionsClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "170dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "33px",
            "width": "25px",
            "zIndex": 10
        }, controller.args[0], "flxSuggestionsClose"), extendConfig({}, controller.args[1], "flxSuggestionsClose"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxSuggestionsClose"));
        flxSuggestionsClose.setDefaultUnit(kony.flex.DP);
        var imgClose = new kony.ui.Image2(extendConfig({
            "bottom": "8px",
            "id": "imgClose",
            "isVisible": true,
            "left": "8px",
            "right": "8px",
            "skin": "slImage",
            "src": "closetags_2x.png",
            "top": "8px",
            "zIndex": 1
        }, controller.args[0], "imgClose"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose"), extendConfig({}, controller.args[2], "imgClose"));
        flxSuggestionsClose.add(imgClose);
        flxCSR.add(tbxCSR, lblCSR, flxSuggestionsClose);
        var btnAdd = new kony.ui.Button(extendConfig({
            "bottom": "15px",
            "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "height": "28px",
            "id": "btnAdd",
            "isVisible": true,
            "left": "660px",
            "right": "0px",
            "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
            "top": "47px",
            "width": "70px",
            "zIndex": 1
        }, controller.args[0], "btnAdd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAdd"), extendConfig({
            "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
        }, controller.args[2], "btnAdd"));
        var flxDownload = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "flxDownload",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "50px",
            "skin": "sknflxDownload",
            "top": "40px",
            "width": "30px",
            "zIndex": 1
        }, controller.args[0], "flxDownload"), extendConfig({}, controller.args[1], "flxDownload"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxDownload"));
        flxDownload.setDefaultUnit(kony.flex.DP);
        var imgDownload = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "imgDownload",
            "isVisible": false,
            "src": "download_2x.png",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgDownload"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDownload"), extendConfig({}, controller.args[2], "imgDownload"));
        var lblIconDownload = new kony.ui.Label(extendConfig({
            "centerX": "47%",
            "centerY": "48%",
            "height": "20dp",
            "id": "lblIconDownload",
            "isVisible": true,
            "skin": "sknIcon20pxWhite",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconDownload\")",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblIconDownload"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDownload"), extendConfig({}, controller.args[2], "lblIconDownload"));
        flxDownload.add(imgDownload, lblIconDownload);
        flxDateSelection.add(flxDate, flxCategory, flxCSR, btnAdd, flxDownload);
        search.add(flxDateSelection);
        return search;
    }
})