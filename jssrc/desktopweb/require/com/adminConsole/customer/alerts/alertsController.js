define("com/adminConsole/customer/alerts/useralertsController", function() {
    return {};
});
define("com/adminConsole/customer/alerts/alertsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/customer/alerts/alertsController", ["com/adminConsole/customer/alerts/useralertsController", "com/adminConsole/customer/alerts/alertsControllerActions"], function() {
    var controller = require("com/adminConsole/customer/alerts/useralertsController");
    var actions = require("com/adminConsole/customer/alerts/alertsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
