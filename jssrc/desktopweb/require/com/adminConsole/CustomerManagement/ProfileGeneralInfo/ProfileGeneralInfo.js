define(function() {
    return function(controller) {
        var ProfileGeneralInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "ProfileGeneralInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "maxHeight": "550px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "ProfileGeneralInfo"), extendConfig({}, controller.args[1], "ProfileGeneralInfo"), extendConfig({}, controller.args[2], "ProfileGeneralInfo"));
        ProfileGeneralInfo.setDefaultUnit(kony.flex.DP);
        var flxViewGeneralInformation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewGeneralInformation",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffBorderd6dbe7Radius4px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxViewGeneralInformation"), extendConfig({}, controller.args[1], "flxViewGeneralInformation"), extendConfig({}, controller.args[2], "flxViewGeneralInformation"));
        flxViewGeneralInformation.setDefaultUnit(kony.flex.DP);
        var flxGeneralInfoHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGeneralInfoHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlxFFFFFF100O",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxGeneralInfoHeader"), extendConfig({}, controller.args[1], "flxGeneralInfoHeader"), extendConfig({}, controller.args[2], "flxGeneralInfoHeader"));
        flxGeneralInfoHeader.setDefaultUnit(kony.flex.DP);
        var generalInfoHeader = new com.adminConsole.customerMang.generalInfoHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "generalInfoHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0px",
            "width": "95%",
            "overrides": {
                "flxActionButtons": {
                    "reverseLayoutDirection": false
                },
                "flxCSRAssist": {
                    "isVisible": true
                },
                "flxRiskStatus": {
                    "isVisible": true,
                    "zIndex": 10
                },
                "flxUnlock": {
                    "isVisible": false,
                    "left": "viz.val_cleared",
                    "right": 150
                },
                "generalInfoHeader": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "viz.val_cleared",
                    "centerX": "viz.val_cleared",
                    "centerY": "viz.val_cleared",
                    "left": "35px",
                    "right": "35px",
                    "top": "0px",
                    "width": "95%"
                },
                "lblSeparator": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "generalInfoHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "generalInfoHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "generalInfoHeader"));
        flxGeneralInfoHeader.add(generalInfoHeader);
        var alertMessage = new com.adminConsole.customerMang.alertMessage(extendConfig({
            "bottom": "0px",
            "clipBounds": true,
            "height": "70px",
            "id": "alertMessage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "sknYellowBorder",
            "width": "93%",
            "zIndex": 1,
            "overrides": {
                "alertMessage": {
                    "bottom": "0px",
                    "isVisible": true,
                    "left": "35px",
                    "right": "viz.val_cleared",
                    "top": "viz.val_cleared",
                    "width": "93%",
                    "zIndex": 1
                },
                "lblData1": {
                    "centerY": "50%",
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.requestsAndNotificationsCountPrefix\")",
                    "left": "5px",
                    "top": "15px"
                },
                "lblData2": {
                    "centerY": "50%",
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.requestsAndNotificationsCountSufix\")",
                    "isVisible": true,
                    "left": "5px",
                    "top": "15px"
                },
                "lblData3": {
                    "centerY": "50%",
                    "left": "1px",
                    "top": "15px"
                }
            }
        }, controller.args[0], "alertMessage"), extendConfig({
            "overrides": {}
        }, controller.args[1], "alertMessage"), extendConfig({
            "overrides": {}
        }, controller.args[2], "alertMessage"));
        var flxTabWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxTabWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlxFFFFFF1000",
            "top": "20px",
            "zIndex": 1
        }, controller.args[0], "flxTabWrapper"), extendConfig({}, controller.args[1], "flxTabWrapper"), extendConfig({}, controller.args[2], "flxTabWrapper"));
        flxTabWrapper.setDefaultUnit(kony.flex.DP);
        var dashboardCommonTab = new com.adminConsole.commonTabsDashboard.dashboardCommonTab(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "dashboardCommonTab",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0dp",
            "overrides": {
                "dashboardCommonTab": {
                    "height": "100%",
                    "left": "35px",
                    "right": "35px",
                    "top": "0dp",
                    "width": "viz.val_cleared"
                }
            }
        }, controller.args[0], "dashboardCommonTab"), extendConfig({
            "overrides": {}
        }, controller.args[1], "dashboardCommonTab"), extendConfig({
            "overrides": {}
        }, controller.args[2], "dashboardCommonTab"));
        flxTabWrapper.add(dashboardCommonTab);
        var flxGeneralInformation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGeneralInformation",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlxFFFFFF1000",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxGeneralInformation"), extendConfig({}, controller.args[1], "flxGeneralInformation"), extendConfig({}, controller.args[2], "flxGeneralInformation"));
        flxGeneralInformation.setDefaultUnit(kony.flex.DP);
        var flxInfoHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxInfoHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%"
        }, controller.args[0], "flxInfoHeader"), extendConfig({}, controller.args[1], "flxInfoHeader"), extendConfig({}, controller.args[2], "flxInfoHeader"));
        flxInfoHeader.setDefaultUnit(kony.flex.DP);
        var flxGMHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxGMHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_fb56777828c04be396c6df912f2cf28b,
            "skin": "slFbox",
            "top": "20px",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flxGMHeader"), extendConfig({}, controller.args[1], "flxGMHeader"), extendConfig({}, controller.args[2], "flxGMHeader"));
        flxGMHeader.setDefaultUnit(kony.flex.DP);
        var lblGeneralInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblGeneralInfo",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblGeneralInfo\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGeneralInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGeneralInfo"), extendConfig({
            "hoverSkin": "lblCursorlatoreg12px"
        }, controller.args[2], "lblGeneralInfo"));
        var flxArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0px",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxArrow"), extendConfig({}, controller.args[1], "flxArrow"), extendConfig({}, controller.args[2], "flxArrow"));
        flxArrow.setDefaultUnit(kony.flex.DP);
        var imgArrow = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "10dp",
            "id": "imgArrow",
            "isVisible": false,
            "skin": "slImage",
            "src": "img_down_arrow.png",
            "width": "10dp",
            "zIndex": 1
        }, controller.args[0], "imgArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgArrow"), extendConfig({}, controller.args[2], "imgArrow"));
        var fonticonrightarrow = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "fonticonrightarrow",
            "isVisible": true,
            "left": "0px",
            "skin": "sknfontIconDescDownArrow12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonrightarrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonrightarrow"), extendConfig({}, controller.args[2], "fonticonrightarrow"));
        flxArrow.add(imgArrow, fonticonrightarrow);
        flxGMHeader.add(lblGeneralInfo, flxArrow);
        var flxGeneralInfoEditButton = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxGeneralInfoEditButton",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "sknflxffffffop100Border424242Radius100px",
            "width": "60px",
            "zIndex": 1
        }, controller.args[0], "flxGeneralInfoEditButton"), extendConfig({}, controller.args[1], "flxGeneralInfoEditButton"), extendConfig({
            "hoverSkin": "sknHoverUnlock"
        }, controller.args[2], "flxGeneralInfoEditButton"));
        flxGeneralInfoEditButton.setDefaultUnit(kony.flex.DP);
        var lblGeneralInfoEditButton = new kony.ui.Label(extendConfig({
            "centerX": "48%",
            "centerY": "47%",
            "id": "lblGeneralInfoEditButton",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGeneralInfoEditButton"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGeneralInfoEditButton"), extendConfig({}, controller.args[2], "lblGeneralInfoEditButton"));
        var imgGeneralInfoEditButton = new kony.ui.Image2(extendConfig({
            "centerY": "47%",
            "height": "15dp",
            "id": "imgGeneralInfoEditButton",
            "isVisible": false,
            "left": "10dp",
            "skin": "slImage",
            "src": "img_edit_btn.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgGeneralInfoEditButton"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgGeneralInfoEditButton"), extendConfig({}, controller.args[2], "imgGeneralInfoEditButton"));
        flxGeneralInfoEditButton.add(lblGeneralInfoEditButton, imgGeneralInfoEditButton);
        flxInfoHeader.add(flxGMHeader, flxGeneralInfoEditButton);
        var flxGmInfoDetailWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGmInfoDetailWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_bcc55522ae104729b543f8c6aa1375b3,
            "skin": "slFbox",
            "top": "5px",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxGmInfoDetailWrapper"), extendConfig({}, controller.args[1], "flxGmInfoDetailWrapper"), extendConfig({}, controller.args[2], "flxGmInfoDetailWrapper"));
        flxGmInfoDetailWrapper.setDefaultUnit(kony.flex.DP);
        var row1 = new com.adminConsole.view.details(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "25dp",
            "clipBounds": true,
            "id": "row1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnLink3": {
                    "centerY": "viz.val_cleared"
                },
                "details": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "25dp"
                },
                "flxColumn3": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                },
                "flxDataAndImage3": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "top": "21dp"
                },
                "imgData1": {
                    "src": "active_circle2x.png"
                },
                "imgData2": {
                    "src": "active_circle2x.png"
                },
                "imgData3": {
                    "centerY": "viz.val_cleared",
                    "src": "active_circle2x.png"
                },
                "lblData3": {
                    "centerY": "viz.val_cleared",
                    "width": "100%"
                }
            }
        }, controller.args[0], "row1"), extendConfig({
            "overrides": {}
        }, controller.args[1], "row1"), extendConfig({
            "overrides": {}
        }, controller.args[2], "row1"));
        var row2 = new com.adminConsole.view.details(extendConfig({
            "clipBounds": true,
            "height": "60px",
            "id": "row2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "overrides": {
                "details": {
                    "height": "60px",
                    "top": "viz.val_cleared"
                },
                "imgData1": {
                    "src": "active_circle2x.png"
                },
                "imgData2": {
                    "src": "active_circle2x.png"
                },
                "imgData3": {
                    "src": "active_circle2x.png"
                }
            }
        }, controller.args[0], "row2"), extendConfig({
            "overrides": {}
        }, controller.args[1], "row2"), extendConfig({
            "overrides": {}
        }, controller.args[2], "row2"));
        var row3 = new com.adminConsole.view.details(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "row3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "details": {
                    "height": "60px"
                },
                "imgData1": {
                    "src": "active_circle2x.png"
                },
                "imgData2": {
                    "src": "active_circle2x.png"
                },
                "imgData3": {
                    "src": "active_circle2x.png"
                }
            }
        }, controller.args[0], "row3"), extendConfig({
            "overrides": {}
        }, controller.args[1], "row3"), extendConfig({
            "overrides": {}
        }, controller.args[2], "row3"));
        var row4 = new com.adminConsole.view.details(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "row4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "details": {
                    "height": "60px"
                },
                "imgData1": {
                    "src": "active_circle2x.png"
                },
                "imgData2": {
                    "src": "active_circle2x.png"
                },
                "imgData3": {
                    "src": "active_circle2x.png"
                }
            }
        }, controller.args[0], "row4"), extendConfig({
            "overrides": {}
        }, controller.args[1], "row4"), extendConfig({
            "overrides": {}
        }, controller.args[2], "row4"));
        flxGmInfoDetailWrapper.add(row1, row2, row3, row4);
        flxGeneralInformation.add(flxInfoHeader, flxGmInfoDetailWrapper);
        flxViewGeneralInformation.add(flxGeneralInfoHeader, alertMessage, flxTabWrapper, flxGeneralInformation);
        var flxEditGeneralInformation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "35px",
            "clipBounds": true,
            "height": "391px",
            "id": "flxEditGeneralInformation",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "sknflxffffffBorderd6dbe7Radius4px",
            "top": "15px",
            "zIndex": 1
        }, controller.args[0], "flxEditGeneralInformation"), extendConfig({}, controller.args[1], "flxEditGeneralInformation"), extendConfig({}, controller.args[2], "flxEditGeneralInformation"));
        flxEditGeneralInformation.setDefaultUnit(kony.flex.DP);
        var flxBack = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxBack",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBack"), extendConfig({}, controller.args[1], "flxBack"), extendConfig({}, controller.args[2], "flxBack"));
        flxBack.setDefaultUnit(kony.flex.DP);
        var backToGeneralInformation = new com.adminConsole.customerMang.backToPageHeader(extendConfig({
            "clipBounds": true,
            "height": "20px",
            "id": "backToGeneralInformation",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "backToGeneralInformation"), extendConfig({
            "overrides": {}
        }, controller.args[1], "backToGeneralInformation"), extendConfig({
            "overrides": {}
        }, controller.args[2], "backToGeneralInformation"));
        flxBack.add(backToGeneralInformation);
        var flxGeneralInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "240px",
            "id": "flxGeneralInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox0b7c2de721b0a4fCM",
            "top": "20px",
            "zIndex": 1
        }, controller.args[0], "flxGeneralInfo"), extendConfig({}, controller.args[1], "flxGeneralInfo"), extendConfig({}, controller.args[2], "flxGeneralInfo"));
        flxGeneralInfo.setDefaultUnit(kony.flex.DP);
        var flxGeneralInfoEditHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxGeneralInfoEditHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox0b7c2de721b0a4fCM",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxGeneralInfoEditHeader"), extendConfig({}, controller.args[1], "flxGeneralInfoEditHeader"), extendConfig({}, controller.args[2], "flxGeneralInfoEditHeader"));
        flxGeneralInfoEditHeader.setDefaultUnit(kony.flex.DP);
        var lblGenaralInfoHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblGenaralInfoHeader",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblGenaralInfoHeader\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGenaralInfoHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGenaralInfoHeader"), extendConfig({}, controller.args[2], "lblGenaralInfoHeader"));
        flxGeneralInfoEditHeader.add(lblGenaralInfoHeader);
        var flxEditGenralInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEditGenralInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEditGenralInfo"), extendConfig({}, controller.args[1], "flxEditGenralInfo"), extendConfig({}, controller.args[2], "flxEditGenralInfo"));
        flxEditGenralInfo.setDefaultUnit(kony.flex.DP);
        var EditGeneralInfo = new com.adminConsole.customerMang.EditGeneralInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "EditGeneralInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox0hfd18814fd664dCM",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "EditGeneralInfo": {
                    "left": "0dp",
                    "right": "viz.val_cleared",
                    "top": "0dp",
                    "width": "100%"
                },
                "flxDetails3": {
                    "isVisible": true
                },
                "imgFlag1": {
                    "src": "checkbox.png"
                },
                "imgFlag2": {
                    "src": "checkbox.png"
                },
                "imgFlag3": {
                    "src": "checkbox.png"
                },
                "lblContactDetails7": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Edit_eagreementStatus\")"
                }
            }
        }, controller.args[0], "EditGeneralInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "EditGeneralInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "EditGeneralInfo"));
        flxEditGenralInfo.add(EditGeneralInfo);
        flxGeneralInfo.add(flxGeneralInfoEditHeader, flxEditGenralInfo);
        var flxEditButtonsWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "81px",
            "id": "flxEditButtonsWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEditButtonsWrapper"), extendConfig({}, controller.args[1], "flxEditButtonsWrapper"), extendConfig({}, controller.args[2], "flxEditButtonsWrapper"));
        flxEditButtonsWrapper.setDefaultUnit(kony.flex.DP);
        var flxEditSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxEditSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEditSeperator"), extendConfig({}, controller.args[1], "flxEditSeperator"), extendConfig({}, controller.args[2], "flxEditSeperator"));
        flxEditSeperator.setDefaultUnit(kony.flex.DP);
        flxEditSeperator.add();
        var flxEditButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxEditButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "1dp",
            "zIndex": 1
        }, controller.args[0], "flxEditButtons"), extendConfig({}, controller.args[1], "flxEditButtons"), extendConfig({}, controller.args[2], "flxEditButtons"));
        flxEditButtons.setDefaultUnit(kony.flex.DP);
        var editButtons = new com.adminConsole.common.commonButtons(extendConfig({
            "clipBounds": true,
            "height": "80px",
            "id": "editButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnNext": {
                    "isVisible": false,
                    "left": "1px",
                    "minWidth": "viz.val_cleared",
                    "width": "100px"
                },
                "commonButtons": {
                    "left": "0px",
                    "right": "viz.val_cleared",
                    "top": "0dp",
                    "width": "100%"
                },
                "flxRightButtons": {
                    "right": "0px"
                }
            }
        }, controller.args[0], "editButtons"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editButtons"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editButtons"));
        flxEditButtons.add(editButtons);
        flxEditButtonsWrapper.add(flxEditSeperator, flxEditButtons);
        flxEditGeneralInformation.add(flxBack, flxGeneralInfo, flxEditButtonsWrapper);
        ProfileGeneralInfo.add(flxViewGeneralInformation, flxEditGeneralInformation);
        return ProfileGeneralInfo;
    }
})