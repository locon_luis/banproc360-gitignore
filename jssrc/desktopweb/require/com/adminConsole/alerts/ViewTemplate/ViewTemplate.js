define(function() {
    return function(controller) {
        var ViewTemplate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "ViewTemplate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxffffffop100",
            "top": "0dp"
        }, controller.args[0], "ViewTemplate"), extendConfig({}, controller.args[1], "ViewTemplate"), extendConfig({}, controller.args[2], "ViewTemplate"));
        ViewTemplate.setDefaultUnit(kony.flex.DP);
        var lblChannelName = new kony.ui.Label(extendConfig({
            "id": "lblChannelName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192b45LatoBold13px",
            "text": "Push Notification",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblChannelName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannelName"), extendConfig({}, controller.args[2], "lblChannelName"));
        var flxChannelText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxChannelText",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "115px",
            "skin": "slFbox",
            "top": "12dp",
            "zIndex": 1
        }, controller.args[0], "flxChannelText"), extendConfig({}, controller.args[1], "flxChannelText"), extendConfig({}, controller.args[2], "flxChannelText"));
        flxChannelText.setDefaultUnit(kony.flex.DP);
        var flxTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTitle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "10dp"
        }, controller.args[0], "flxTitle"), extendConfig({}, controller.args[1], "flxTitle"), extendConfig({}, controller.args[2], "flxTitle"));
        flxTitle.setDefaultUnit(kony.flex.DP);
        var lblTitle = new kony.ui.Label(extendConfig({
            "id": "lblTitle",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Title:",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTitle"), extendConfig({}, controller.args[2], "lblTitle"));
        var lblTitleValue = new kony.ui.Label(extendConfig({
            "id": "lblTitleValue",
            "isVisible": true,
            "left": "35dp",
            "right": "30dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "lblTitleValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTitleValue"), extendConfig({}, controller.args[2], "lblTitleValue"));
        flxTitle.add(lblTitle, lblTitleValue);
        var lblChannelMsg = new kony.ui.Label(extendConfig({
            "bottom": "10dp",
            "id": "lblChannelMsg",
            "isVisible": true,
            "left": "0dp",
            "right": "10dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor.",
            "top": "10dp",
            "width": "98.50%",
            "zIndex": 1
        }, controller.args[0], "lblChannelMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannelMsg"), extendConfig({}, controller.args[2], "lblChannelMsg"));
        flxChannelText.add(flxTitle, lblChannelMsg);
        ViewTemplate.add(lblChannelName, flxChannelText);
        return ViewTemplate;
    }
})