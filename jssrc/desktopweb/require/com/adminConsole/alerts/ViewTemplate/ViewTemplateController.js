define("com/adminConsole/alerts/ViewTemplate/userViewTemplateController", function() {
    return {};
});
define("com/adminConsole/alerts/ViewTemplate/ViewTemplateControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/alerts/ViewTemplate/ViewTemplateController", ["com/adminConsole/alerts/ViewTemplate/userViewTemplateController", "com/adminConsole/alerts/ViewTemplate/ViewTemplateControllerActions"], function() {
    var controller = require("com/adminConsole/alerts/ViewTemplate/userViewTemplateController");
    var actions = require("com/adminConsole/alerts/ViewTemplate/ViewTemplateControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
