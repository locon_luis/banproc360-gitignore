define("com/adminConsole/alerts/customListbox/usercustomListboxController", function() {
    return {};
});
define("com/adminConsole/alerts/customListbox/customListboxControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/alerts/customListbox/customListboxController", ["com/adminConsole/alerts/customListbox/usercustomListboxController", "com/adminConsole/alerts/customListbox/customListboxControllerActions"], function() {
    var controller = require("com/adminConsole/alerts/customListbox/usercustomListboxController");
    var actions = require("com/adminConsole/alerts/customListbox/customListboxControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
