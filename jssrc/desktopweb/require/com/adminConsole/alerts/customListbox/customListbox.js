define(function() {
    return function(controller) {
        var customListbox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "customListbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "customListbox"), extendConfig({}, controller.args[1], "customListbox"), extendConfig({}, controller.args[2], "customListbox"));
        customListbox.setDefaultUnit(kony.flex.DP);
        var flxSelectedText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSelectedText",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSelectedText"), extendConfig({}, controller.args[1], "flxSelectedText"), extendConfig({}, controller.args[2], "flxSelectedText"));
        flxSelectedText.setDefaultUnit(kony.flex.DP);
        var lblSelectedValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelectedValue",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectedValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedValue"), extendConfig({}, controller.args[2], "lblSelectedValue"));
        var flxDropdown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "30dp"
        }, controller.args[0], "flxDropdown"), extendConfig({}, controller.args[1], "flxDropdown"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxDropdown"));
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var lblIconDropdown = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconDropdown",
            "isVisible": true,
            "right": "10dp",
            "skin": "sknicon15pxBlack",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDropdown"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDropdown"), extendConfig({}, controller.args[2], "lblIconDropdown"));
        flxDropdown.add(lblIconDropdown);
        flxSelectedText.add(lblSelectedValue, flxDropdown);
        var flxSegmentList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegmentList",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxFFFFFFBr1pxe1e5edR3px3sided",
            "top": "35dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSegmentList"), extendConfig({}, controller.args[1], "flxSegmentList"), extendConfig({}, controller.args[2], "flxSegmentList"));
        flxSegmentList.setDefaultUnit(kony.flex.DP);
        var flxSelectAll = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "id": "flxSelectAll",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxSelectAll"), extendConfig({}, controller.args[1], "flxSelectAll"), extendConfig({
            "hoverSkin": "sknFlxffffffOp100Cursor"
        }, controller.args[2], "flxSelectAll"));
        flxSelectAll.setDefaultUnit(kony.flex.DP);
        var flxCheckBox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12px",
            "id": "flxCheckBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "9dp",
            "width": "12px",
            "zIndex": 2
        }, controller.args[0], "flxCheckBox"), extendConfig({}, controller.args[1], "flxCheckBox"), extendConfig({}, controller.args[2], "flxCheckBox"));
        flxCheckBox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "imgCheckBox",
            "isVisible": true,
            "left": "0%",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "imgCheckBox"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCheckBox"), extendConfig({}, controller.args[2], "imgCheckBox"));
        flxCheckBox.add(imgCheckBox);
        var lblDescription = new kony.ui.Label(extendConfig({
            "bottom": "6dp",
            "id": "lblDescription",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "All",
            "top": "6dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        flxSelectAll.add(flxCheckBox, lblDescription);
        var segList = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "3dp",
            "data": [{
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "KL098234 - Westheimer"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "KL098234 - Westheimer"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "KL098234 - Westheimer"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "KL098234 - Westheimer"
            }],
            "groupCells": false,
            "id": "segList",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxSearchDropDown",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkboxnormal.png"
            },
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCheckBox": "flxCheckBox",
                "flxSearchDropDown": "flxSearchDropDown",
                "imgCheckBox": "imgCheckBox",
                "lblDescription": "lblDescription"
            },
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "segList"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segList"), extendConfig({}, controller.args[2], "segList"));
        var btnOk = new kony.ui.Button(extendConfig({
            "bottom": "10dp",
            "focusSkin": "sknBtnFFFFFFBor1pxR4pxShad5px",
            "height": "35dp",
            "id": "btnOk",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknBtnFFFFFFBor1pxR4pxShad5px",
            "text": "OK",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "btnOk"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOk"), extendConfig({
            "hoverSkin": "sknBtnFFFFFFBor1pxR4pxShad5px"
        }, controller.args[2], "btnOk"));
        flxSegmentList.add(flxSelectAll, segList, btnOk);
        var flxListboxError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxListboxError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "45dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxListboxError"), extendConfig({}, controller.args[1], "flxListboxError"), extendConfig({}, controller.args[2], "flxListboxError"));
        flxListboxError.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon"), extendConfig({}, controller.args[2], "lblErrorIcon"));
        var lblErrorMsg = new kony.ui.Label(extendConfig({
            "id": "lblErrorMsg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Invalid data",
            "top": "0dp",
            "width": "170dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorMsg"), extendConfig({}, controller.args[2], "lblErrorMsg"));
        flxListboxError.add(lblErrorIcon, lblErrorMsg);
        customListbox.add(flxSelectedText, flxSegmentList, flxListboxError);
        return customListbox;
    }
})