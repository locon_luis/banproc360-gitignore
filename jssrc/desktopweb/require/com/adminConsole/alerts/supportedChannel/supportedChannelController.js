define("com/adminConsole/alerts/supportedChannel/usersupportedChannelController", function() {
    return {};
});
define("com/adminConsole/alerts/supportedChannel/supportedChannelControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/alerts/supportedChannel/supportedChannelController", ["com/adminConsole/alerts/supportedChannel/usersupportedChannelController", "com/adminConsole/alerts/supportedChannel/supportedChannelControllerActions"], function() {
    var controller = require("com/adminConsole/alerts/supportedChannel/usersupportedChannelController");
    var actions = require("com/adminConsole/alerts/supportedChannel/supportedChannelControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
