define(function() {
    return function(controller) {
        var supportedChannel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "30px",
            "id": "supportedChannel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxLocationCurrency",
            "top": "0px",
            "width": "150px"
        }, controller.args[0], "supportedChannel"), extendConfig({}, controller.args[1], "supportedChannel"), extendConfig({
            "hoverSkin": "tabsHoverSkin"
        }, controller.args[2], "supportedChannel"));
        supportedChannel.setDefaultUnit(kony.flex.DP);
        var flxcbSupportedChannel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15px",
            "id": "flxcbSupportedChannel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxcbSupportedChannel"), extendConfig({}, controller.args[1], "flxcbSupportedChannel"), extendConfig({}, controller.args[2], "flxcbSupportedChannel"));
        flxcbSupportedChannel.setDefaultUnit(kony.flex.DP);
        var imgcbSupportedChannel = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "imgcbSupportedChannel",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "checkboxselected.png",
            "top": "0px",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "imgcbSupportedChannel"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgcbSupportedChannel"), extendConfig({}, controller.args[2], "imgcbSupportedChannel"));
        flxcbSupportedChannel.add(imgcbSupportedChannel);
        var lblSupportedChannel = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSupportedChannel",
            "isVisible": true,
            "left": "25px",
            "skin": "slLabel0d20174dce8ea42",
            "text": "Notification Center",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSupportedChannel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSupportedChannel"), extendConfig({}, controller.args[2], "lblSupportedChannel"));
        supportedChannel.add(flxcbSupportedChannel, lblSupportedChannel);
        return supportedChannel;
    }
})