define("com/adminConsole/decisionManagement/DecisionFolder/userDecisionFolderController", function() {
    return {};
});
define("com/adminConsole/decisionManagement/DecisionFolder/DecisionFolderControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/decisionManagement/DecisionFolder/DecisionFolderController", ["com/adminConsole/decisionManagement/DecisionFolder/userDecisionFolderController", "com/adminConsole/decisionManagement/DecisionFolder/DecisionFolderControllerActions"], function() {
    var controller = require("com/adminConsole/decisionManagement/DecisionFolder/userDecisionFolderController");
    var actions = require("com/adminConsole/decisionManagement/DecisionFolder/DecisionFolderControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
