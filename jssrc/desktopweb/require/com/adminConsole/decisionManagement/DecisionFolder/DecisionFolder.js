define(function() {
    return function(controller) {
        var DecisionFolder = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "231px",
            "id": "DecisionFolder",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "336px"
        }, controller.args[0], "DecisionFolder"), extendConfig({}, controller.args[1], "DecisionFolder"), extendConfig({}, controller.args[2], "DecisionFolder"));
        DecisionFolder.setDefaultUnit(kony.flex.DP);
        var flxDecisionFolder = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDecisionFolder",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFolderBackground",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDecisionFolder"), extendConfig({}, controller.args[1], "flxDecisionFolder"), extendConfig({}, controller.args[2], "flxDecisionFolder"));
        flxDecisionFolder.setDefaultUnit(kony.flex.DP);
        var flxDecisionButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxDecisionButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "40dp",
            "width": "100dp",
            "zIndex": 2
        }, controller.args[0], "flxDecisionButtons"), extendConfig({}, controller.args[1], "flxDecisionButtons"), extendConfig({}, controller.args[2], "flxDecisionButtons"));
        flxDecisionButtons.setDefaultUnit(kony.flex.DP);
        var flxLblImgEdit = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxLblImgEdit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "30px",
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "20px",
            "zIndex": 2
        }, controller.args[0], "flxLblImgEdit"), extendConfig({}, controller.args[1], "flxLblImgEdit"), extendConfig({
            "hoverSkin": "tabsHoverSkin"
        }, controller.args[2], "flxLblImgEdit"));
        flxLblImgEdit.setDefaultUnit(kony.flex.DP);
        var lblImgEdit = new kony.ui.Label(extendConfig({
            "id": "lblImgEdit",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.editIcon\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblImgEdit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgEdit"), extendConfig({
            "hoverSkin": "sknIcon20pxWhiteHover"
        }, controller.args[2], "lblImgEdit"));
        flxLblImgEdit.add(lblImgEdit);
        var flxDelete = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlxPointer",
            "top": "0px",
            "width": "20px"
        }, controller.args[0], "flxDelete"), extendConfig({}, controller.args[1], "flxDelete"), extendConfig({
            "hoverSkin": "tabsHoverSkin"
        }, controller.args[2], "flxDelete"));
        flxDelete.setDefaultUnit(kony.flex.DP);
        var lblDelete = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblDelete",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDelete"), extendConfig({
            "hoverSkin": "sknIcon20pxWhiteHover"
        }, controller.args[2], "lblDelete"));
        flxDelete.add(lblDelete);
        flxDecisionButtons.add(flxLblImgEdit, flxDelete);
        var lblDecisionHeading = new kony.ui.Label(extendConfig({
            "id": "lblDecisionHeading",
            "isVisible": true,
            "left": "30dp",
            "right": "30dp",
            "skin": "LblLatoRegular485c7516px",
            "text": "Loan Simulation",
            "top": "10dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblDecisionHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDecisionHeading"), extendConfig({}, controller.args[2], "lblDecisionHeading"));
        var lblDecisionDescription = new kony.ui.Label(extendConfig({
            "height": "45dp",
            "id": "lblDecisionDescription",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLblLatoReg78818A13px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat tortor quis arcu interdum pretium vitae at lorem. Nullam sit amet nisl a diam suscipit ullamcorper vel non dolor. Proin sed interdum lectus, et laoreet neque. Aliquam pretium orci.",
            "top": "20dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblDecisionDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDecisionDescription"), extendConfig({}, controller.args[2], "lblDecisionDescription"));
        var flxlMyQueueViewAllContaner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxlMyQueueViewAllContaner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "30dp",
            "skin": "sknFlxPointer",
            "top": "-14dp",
            "width": "100dp",
            "zIndex": 2
        }, controller.args[0], "flxlMyQueueViewAllContaner"), extendConfig({}, controller.args[1], "flxlMyQueueViewAllContaner"), extendConfig({}, controller.args[2], "flxlMyQueueViewAllContaner"));
        flxlMyQueueViewAllContaner.setDefaultUnit(kony.flex.DP);
        var lblMyQueueViewAll = new kony.ui.Label(extendConfig({
            "id": "lblMyQueueViewAll",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknLbl117eb0bdffffff",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.lblViewMore\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblMyQueueViewAll"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [15, 0, 15, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMyQueueViewAll"), extendConfig({}, controller.args[2], "lblMyQueueViewAll"));
        flxlMyQueueViewAllContaner.add(lblMyQueueViewAll);
        var flxClick = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxClick",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "-130dp",
            "width": "100%"
        }, controller.args[0], "flxClick"), extendConfig({}, controller.args[1], "flxClick"), extendConfig({}, controller.args[2], "flxClick"));
        flxClick.setDefaultUnit(kony.flex.DP);
        flxClick.add();
        flxDecisionFolder.add(flxDecisionButtons, lblDecisionHeading, lblDecisionDescription, flxlMyQueueViewAllContaner, flxClick);
        DecisionFolder.add(flxDecisionFolder);
        return DecisionFolder;
    }
})