define(function() {
    return function(controller) {
        var createUpdate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "754px",
            "id": "createUpdate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "right": "25px",
            "skin": "sknflxLeadView",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "createUpdate"), extendConfig({}, controller.args[1], "createUpdate"), extendConfig({}, controller.args[2], "createUpdate"));
        createUpdate.setDefaultUnit(kony.flex.DP);
        var flxCreateUpdate = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "754px",
            "horizontalScrollIndicator": true,
            "id": "flxCreateUpdate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0dp",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxCreateUpdate"), extendConfig({}, controller.args[1], "flxCreateUpdate"), extendConfig({}, controller.args[2], "flxCreateUpdate"));
        flxCreateUpdate.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35dp",
            "width": "100%"
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxLeadFirstName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxLeadFirstName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxLeadFirstName"), extendConfig({}, controller.args[1], "flxLeadFirstName"), extendConfig({}, controller.args[2], "flxLeadFirstName"));
        flxLeadFirstName.setDefaultUnit(kony.flex.DP);
        var lblLeadFirstName = new kony.ui.Label(extendConfig({
            "id": "lblLeadFirstName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstName\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadFirstName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadFirstName"), extendConfig({}, controller.args[2], "lblLeadFirstName"));
        var txtLeadFirstName = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtLeadFirstName",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtLeadFirstName"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtLeadFirstName"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtLeadFirstName"));
        var flxErrorLeadFirstName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxErrorLeadFirstName",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxErrorLeadFirstName"), extendConfig({}, controller.args[1], "flxErrorLeadFirstName"), extendConfig({}, controller.args[2], "flxErrorLeadFirstName"));
        flxErrorLeadFirstName.setDefaultUnit(kony.flex.DP);
        var lblIconErrorLeadFirstName = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblIconErrorLeadFirstName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblIconErrorLeadFirstName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconErrorLeadFirstName"), extendConfig({}, controller.args[2], "lblIconErrorLeadFirstName"));
        var lblErrorLeadFirstName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblErrorLeadFirstName",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.criteriaCannotBeBlank\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorLeadFirstName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorLeadFirstName"), extendConfig({}, controller.args[2], "lblErrorLeadFirstName"));
        flxErrorLeadFirstName.add(lblIconErrorLeadFirstName, lblErrorLeadFirstName);
        flxLeadFirstName.add(lblLeadFirstName, txtLeadFirstName, flxErrorLeadFirstName);
        var flxLeadMiddleName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxLeadMiddleName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxLeadMiddleName"), extendConfig({}, controller.args[1], "flxLeadMiddleName"), extendConfig({}, controller.args[2], "flxLeadMiddleName"));
        flxLeadMiddleName.setDefaultUnit(kony.flex.DP);
        var lblLeadMiddleName = new kony.ui.Label(extendConfig({
            "id": "lblLeadMiddleName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.MiddleName\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadMiddleName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadMiddleName"), extendConfig({}, controller.args[2], "lblLeadMiddleName"));
        var txtLeadMiddleName = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtLeadMiddleName",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtLeadMiddleName"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtLeadMiddleName"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtLeadMiddleName"));
        flxLeadMiddleName.add(lblLeadMiddleName, txtLeadMiddleName);
        var flxLeadLastName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxLeadLastName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxLeadLastName"), extendConfig({}, controller.args[1], "flxLeadLastName"), extendConfig({}, controller.args[2], "flxLeadLastName"));
        flxLeadLastName.setDefaultUnit(kony.flex.DP);
        var lblLeadLastName = new kony.ui.Label(extendConfig({
            "id": "lblLeadLastName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadLastName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadLastName"), extendConfig({}, controller.args[2], "lblLeadLastName"));
        var txtLeadLastName = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtLeadLastName",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtLeadLastName"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtLeadLastName"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtLeadLastName"));
        var flxErrorLeadLastName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxErrorLeadLastName",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxErrorLeadLastName"), extendConfig({}, controller.args[1], "flxErrorLeadLastName"), extendConfig({}, controller.args[2], "flxErrorLeadLastName"));
        flxErrorLeadLastName.setDefaultUnit(kony.flex.DP);
        var lblIconErrorLeadLastName = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblIconErrorLeadLastName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblIconErrorLeadLastName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconErrorLeadLastName"), extendConfig({}, controller.args[2], "lblIconErrorLeadLastName"));
        var lblErrorLeadLastName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblErrorLeadLastName",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.criteriaCannotBeBlank\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorLeadLastName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorLeadLastName"), extendConfig({}, controller.args[2], "lblErrorLeadLastName"));
        flxErrorLeadLastName.add(lblIconErrorLeadLastName, lblErrorLeadLastName);
        flxLeadLastName.add(lblLeadLastName, txtLeadLastName, flxErrorLeadLastName);
        flxRow1.add(flxLeadFirstName, flxLeadMiddleName, flxLeadLastName);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35px",
            "width": "100%"
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxLeadMail = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxLeadMail",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxLeadMail"), extendConfig({}, controller.args[1], "flxLeadMail"), extendConfig({}, controller.args[2], "flxLeadMail"));
        flxLeadMail.setDefaultUnit(kony.flex.DP);
        var lblLeadMail = new kony.ui.Label(extendConfig({
            "id": "lblLeadMail",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Email\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadMail"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadMail"), extendConfig({}, controller.args[2], "lblLeadMail"));
        var txtLeadMail = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtLeadMail",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtLeadMail"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtLeadMail"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtLeadMail"));
        var flxErrorLeadEmail = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxErrorLeadEmail",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxErrorLeadEmail"), extendConfig({}, controller.args[1], "flxErrorLeadEmail"), extendConfig({}, controller.args[2], "flxErrorLeadEmail"));
        flxErrorLeadEmail.setDefaultUnit(kony.flex.DP);
        var lblIconErrorLeadEmail = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblIconErrorLeadEmail",
            "isVisible": true,
            "left": "0px",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblIconErrorLeadEmail"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconErrorLeadEmail"), extendConfig({}, controller.args[2], "lblIconErrorLeadEmail"));
        var lblErrorLeadEmail = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblErrorLeadEmail",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.criteriaCannotBeBlank\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorLeadEmail"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorLeadEmail"), extendConfig({}, controller.args[2], "lblErrorLeadEmail"));
        flxErrorLeadEmail.add(lblIconErrorLeadEmail, lblErrorLeadEmail);
        flxLeadMail.add(lblLeadMail, txtLeadMail, flxErrorLeadEmail);
        var flxLeadPhoneNumber = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxLeadPhoneNumber",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxLeadPhoneNumber"), extendConfig({}, controller.args[1], "flxLeadPhoneNumber"), extendConfig({}, controller.args[2], "flxLeadPhoneNumber"));
        flxLeadPhoneNumber.setDefaultUnit(kony.flex.DP);
        var lblLeadPhoneNumber = new kony.ui.Label(extendConfig({
            "id": "lblLeadPhoneNumber",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Phone_Number\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadPhoneNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadPhoneNumber"), extendConfig({}, controller.args[2], "lblLeadPhoneNumber"));
        var leadPhoneNumber = new com.adminConsole.common.contactNumber(extendConfig({
            "clipBounds": true,
            "height": "40px",
            "id": "leadPhoneNumber",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "overrides": {
                "contactNumber": {
                    "height": "40px",
                    "left": "0dp",
                    "top": "5dp",
                    "width": "100%"
                },
                "flxContactNumberWrapper": {
                    "height": "40px",
                    "width": "100%"
                },
                "flxError": {
                    "isVisible": false,
                    "top": "45dp"
                },
                "txtContactNumber": {
                    "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Phone_Number\")",
                    "right": "0dp",
                    "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                    "width": "viz.val_cleared"
                },
                "txtISDCode": {
                    "placeholder": " ",
                    "top": "0dp",
                    "width": "70dp"
                }
            }
        }, controller.args[0], "leadPhoneNumber"), extendConfig({
            "overrides": {}
        }, controller.args[1], "leadPhoneNumber"), extendConfig({
            "overrides": {}
        }, controller.args[2], "leadPhoneNumber"));
        var flxErrorLeadPhoneNumber = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxErrorLeadPhoneNumber",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxErrorLeadPhoneNumber"), extendConfig({}, controller.args[1], "flxErrorLeadPhoneNumber"), extendConfig({}, controller.args[2], "flxErrorLeadPhoneNumber"));
        flxErrorLeadPhoneNumber.setDefaultUnit(kony.flex.DP);
        var lblIconErrorLeadPhoneNumber = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblIconErrorLeadPhoneNumber",
            "isVisible": true,
            "left": "0px",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblIconErrorLeadPhoneNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconErrorLeadPhoneNumber"), extendConfig({}, controller.args[2], "lblIconErrorLeadPhoneNumber"));
        var lblErrorLeadPhoneNumber = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblErrorLeadPhoneNumber",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.criteriaCannotBeBlank\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorLeadPhoneNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorLeadPhoneNumber"), extendConfig({}, controller.args[2], "lblErrorLeadPhoneNumber"));
        flxErrorLeadPhoneNumber.add(lblIconErrorLeadPhoneNumber, lblErrorLeadPhoneNumber);
        flxLeadPhoneNumber.add(lblLeadPhoneNumber, leadPhoneNumber, flxErrorLeadPhoneNumber);
        var flxLeadExtention = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxLeadExtention",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flxLeadExtention"), extendConfig({}, controller.args[1], "flxLeadExtention"), extendConfig({}, controller.args[2], "flxLeadExtention"));
        flxLeadExtention.setDefaultUnit(kony.flex.DP);
        var lblLeadExtention = new kony.ui.Label(extendConfig({
            "id": "lblLeadExtention",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.extn\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadExtention"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadExtention"), extendConfig({}, controller.args[2], "lblLeadExtention"));
        var txtLeadExtention = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtLeadExtention",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.extention\")",
            "secureTextEntry": false,
            "skin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtLeadExtention"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtLeadExtention"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtLeadExtention"));
        flxLeadExtention.add(lblLeadExtention, txtLeadExtention);
        flxRow2.add(flxLeadMail, flxLeadPhoneNumber, flxLeadExtention);
        var flxRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35px",
            "width": "100%"
        }, controller.args[0], "flxRow3"), extendConfig({}, controller.args[1], "flxRow3"), extendConfig({}, controller.args[2], "flxRow3"));
        flxRow3.setDefaultUnit(kony.flex.DP);
        var flxLeadProductType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxLeadProductType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxLeadProductType"), extendConfig({}, controller.args[1], "flxLeadProductType"), extendConfig({}, controller.args[2], "flxLeadProductType"));
        flxLeadProductType.setDefaultUnit(kony.flex.DP);
        var lblLeadProductType = new kony.ui.Label(extendConfig({
            "id": "lblLeadProductType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Product Type",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadProductType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadProductType"), extendConfig({}, controller.args[2], "lblLeadProductType"));
        var listBoxLeadProductType = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "listBoxLeadProductType",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["lb1", "Select Product"]
            ],
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "5dp",
            "width": "100%",
            "zIndex": 40
        }, controller.args[0], "listBoxLeadProductType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "listBoxLeadProductType"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "listBoxLeadProductType"));
        var flxErrorLeadProductType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxErrorLeadProductType",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxErrorLeadProductType"), extendConfig({}, controller.args[1], "flxErrorLeadProductType"), extendConfig({}, controller.args[2], "flxErrorLeadProductType"));
        flxErrorLeadProductType.setDefaultUnit(kony.flex.DP);
        var lblIconErrorLeadProductType = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblIconErrorLeadProductType",
            "isVisible": true,
            "left": "0px",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblIconErrorLeadProductType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconErrorLeadProductType"), extendConfig({}, controller.args[2], "lblIconErrorLeadProductType"));
        var lblErroLeadProductType = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblErroLeadProductType",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.criteriaCannotBeBlank\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErroLeadProductType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErroLeadProductType"), extendConfig({}, controller.args[2], "lblErroLeadProductType"));
        flxErrorLeadProductType.add(lblIconErrorLeadProductType, lblErroLeadProductType);
        flxLeadProductType.add(lblLeadProductType, listBoxLeadProductType, flxErrorLeadProductType);
        var flxLeadProduct = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxLeadProduct",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxLeadProduct"), extendConfig({}, controller.args[1], "flxLeadProduct"), extendConfig({}, controller.args[2], "flxLeadProduct"));
        flxLeadProduct.setDefaultUnit(kony.flex.DP);
        var lblLeadProduct = new kony.ui.Label(extendConfig({
            "id": "lblLeadProduct",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.product\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadProduct"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadProduct"), extendConfig({}, controller.args[2], "lblLeadProduct"));
        var listBoxLeadProduct = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "listBoxLeadProduct",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["lb1", "Select Product"]
            ],
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "5dp",
            "width": "100%",
            "zIndex": 40
        }, controller.args[0], "listBoxLeadProduct"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "listBoxLeadProduct"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "listBoxLeadProduct"));
        var flxErrorLeadProduct = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxErrorLeadProduct",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxErrorLeadProduct"), extendConfig({}, controller.args[1], "flxErrorLeadProduct"), extendConfig({}, controller.args[2], "flxErrorLeadProduct"));
        flxErrorLeadProduct.setDefaultUnit(kony.flex.DP);
        var lblIconErrorLeadProduct = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblIconErrorLeadProduct",
            "isVisible": true,
            "left": "0px",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblIconErrorLeadProduct"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconErrorLeadProduct"), extendConfig({}, controller.args[2], "lblIconErrorLeadProduct"));
        var lblErrorLeadProduct = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblErrorLeadProduct",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.criteriaCannotBeBlank\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorLeadProduct"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorLeadProduct"), extendConfig({}, controller.args[2], "lblErrorLeadProduct"));
        flxErrorLeadProduct.add(lblIconErrorLeadProduct, lblErrorLeadProduct);
        flxLeadProduct.add(lblLeadProduct, listBoxLeadProduct, flxErrorLeadProduct);
        flxRow3.add(flxLeadProductType, flxLeadProduct);
        var flxRow4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "175px",
            "id": "flxRow4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35dp",
            "width": "100%"
        }, controller.args[0], "flxRow4"), extendConfig({}, controller.args[1], "flxRow4"), extendConfig({}, controller.args[2], "flxRow4"));
        flxRow4.setDefaultUnit(kony.flex.DP);
        var flxLeadNote = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxLeadNote",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxLeadNote"), extendConfig({}, controller.args[1], "flxLeadNote"), extendConfig({}, controller.args[2], "flxLeadNote"));
        flxLeadNote.setDefaultUnit(kony.flex.DP);
        var lblLeadNote = new kony.ui.Label(extendConfig({
            "id": "lblLeadNote",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.note\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadNote"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadNote"), extendConfig({}, controller.args[2], "lblLeadNote"));
        var lblLeadNoteCount = new kony.ui.Label(extendConfig({
            "height": "20px",
            "id": "lblLeadNoteCount",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "0/1000",
            "top": "-15px",
            "width": "150dp",
            "zIndex": 2
        }, controller.args[0], "lblLeadNoteCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadNoteCount"), extendConfig({}, controller.args[2], "lblLeadNoteCount"));
        var txtAreaLeadNotes = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextAreaFocus",
            "height": "120dp",
            "id": "txtAreaLeadNotes",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 1000,
            "numberOfVisibleLines": 3,
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAreaLeadNotes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [1, 1, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtAreaLeadNotes"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        }, controller.args[2], "txtAreaLeadNotes"));
        flxLeadNote.add(lblLeadNote, lblLeadNoteCount, txtAreaLeadNotes);
        flxRow4.add(flxLeadNote);
        flxCreateUpdate.add(flxRow1, flxRow2, flxRow3, flxRow4);
        var flxSeprator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "100px",
            "clipBounds": true,
            "id": "flxSeprator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxSeprator"), extendConfig({}, controller.args[1], "flxSeprator"), extendConfig({}, controller.args[2], "flxSeprator"));
        flxSeprator.setDefaultUnit(kony.flex.DP);
        var flxSeperatorInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeperatorInner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflx115678",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxSeperatorInner"), extendConfig({}, controller.args[1], "flxSeperatorInner"), extendConfig({}, controller.args[2], "flxSeperatorInner"));
        flxSeperatorInner.setDefaultUnit(kony.flex.DP);
        flxSeperatorInner.add();
        flxSeprator.add(flxSeperatorInner);
        var flxButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxButtons"), extendConfig({}, controller.args[1], "flxButtons"), extendConfig({}, controller.args[2], "flxButtons"));
        flxButtons.setDefaultUnit(kony.flex.DP);
        var leadsCommonButtons = new com.adminConsole.common.commonButtons(extendConfig({
            "bottom": "35px",
            "clipBounds": true,
            "height": "40px",
            "id": "leadsCommonButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnCancel": {
                    "left": "20px"
                },
                "btnNext": {
                    "isVisible": false
                },
                "btnSave": {
                    "right": "20px"
                },
                "commonButtons": {
                    "bottom": "35px",
                    "height": "40px"
                }
            }
        }, controller.args[0], "leadsCommonButtons"), extendConfig({
            "overrides": {}
        }, controller.args[1], "leadsCommonButtons"), extendConfig({
            "overrides": {}
        }, controller.args[2], "leadsCommonButtons"));
        flxButtons.add(leadsCommonButtons);
        createUpdate.add(flxCreateUpdate, flxSeprator, flxButtons);
        return createUpdate;
    }
})