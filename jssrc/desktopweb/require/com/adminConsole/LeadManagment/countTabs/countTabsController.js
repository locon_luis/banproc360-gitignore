define("com/adminConsole/LeadManagment/countTabs/usercountTabsController", function() {
    return {};
});
define("com/adminConsole/LeadManagment/countTabs/countTabsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/LeadManagment/countTabs/countTabsController", ["com/adminConsole/LeadManagment/countTabs/usercountTabsController", "com/adminConsole/LeadManagment/countTabs/countTabsControllerActions"], function() {
    var controller = require("com/adminConsole/LeadManagment/countTabs/usercountTabsController");
    var actions = require("com/adminConsole/LeadManagment/countTabs/countTabsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
