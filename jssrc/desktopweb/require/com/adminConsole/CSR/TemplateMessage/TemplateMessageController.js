define("com/adminConsole/CSR/TemplateMessage/userTemplateMessageController", function() {
    return {};
});
define("com/adminConsole/CSR/TemplateMessage/TemplateMessageControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/CSR/TemplateMessage/TemplateMessageController", ["com/adminConsole/CSR/TemplateMessage/userTemplateMessageController", "com/adminConsole/CSR/TemplateMessage/TemplateMessageControllerActions"], function() {
    var controller = require("com/adminConsole/CSR/TemplateMessage/userTemplateMessageController");
    var actions = require("com/adminConsole/CSR/TemplateMessage/TemplateMessageControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
