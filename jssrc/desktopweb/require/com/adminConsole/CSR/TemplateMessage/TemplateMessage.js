define(function() {
    return function(controller) {
        var TemplateMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "TemplateMessage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxffffffop100"
        }, controller.args[0], "TemplateMessage"), extendConfig({}, controller.args[1], "TemplateMessage"), extendConfig({}, controller.args[2], "TemplateMessage"));
        TemplateMessage.setDefaultUnit(kony.flex.DP);
        var flxTemplateHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxTemplateHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxTemplateHeader"), extendConfig({}, controller.args[1], "flxTemplateHeader"), extendConfig({}, controller.args[2], "flxTemplateHeader"));
        flxTemplateHeader.setDefaultUnit(kony.flex.DP);
        var flxButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "22px",
            "id": "flxButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "15px",
            "width": "115px",
            "zIndex": 1
        }, controller.args[0], "flxButtons"), extendConfig({}, controller.args[1], "flxButtons"), extendConfig({}, controller.args[2], "flxButtons"));
        flxButtons.setDefaultUnit(kony.flex.DP);
        var btnAdd = new kony.ui.Button(extendConfig({
            "bottom": "0px",
            "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "height": "22px",
            "id": "btnAdd",
            "isVisible": true,
            "left": 0,
            "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "btnAdd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAdd"), extendConfig({
            "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
        }, controller.args[2], "btnAdd"));
        var btnEdit = new kony.ui.Button(extendConfig({
            "bottom": "0px",
            "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "height": "22px",
            "id": "btnEdit",
            "isVisible": true,
            "right": "0px",
            "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "btnEdit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnEdit"), extendConfig({
            "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
        }, controller.args[2], "btnEdit"));
        flxButtons.add(btnAdd, btnEdit);
        var lblTemplateBodyHeader = new kony.ui.Label(extendConfig({
            "id": "lblTemplateBodyHeader",
            "isVisible": true,
            "left": "44dp",
            "skin": "sknLatoRegular485c7514px",
            "text": "TEMPLATE BODY",
            "top": "44dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTemplateBodyHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTemplateBodyHeader"), extendConfig({}, controller.args[2], "lblTemplateBodyHeader"));
        flxTemplateHeader.add(flxButtons, lblTemplateBodyHeader);
        var flxTemplateBody = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "160dp",
            "horizontalScrollIndicator": true,
            "id": "flxTemplateBody",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "pagingEnabled": false,
            "right": "0px",
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTemplateBody"), extendConfig({}, controller.args[1], "flxTemplateBody"), extendConfig({}, controller.args[2], "flxTemplateBody"));
        flxTemplateBody.setDefaultUnit(kony.flex.DP);
        var flxAddressDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 3
        }, controller.args[0], "flxAddressDetails"), extendConfig({}, controller.args[1], "flxAddressDetails"), extendConfig({}, controller.args[2], "flxAddressDetails"));
        flxAddressDetails.setDefaultUnit(kony.flex.DP);
        var rtxAddressAdditional1 = new kony.ui.RichText(extendConfig({
            "id": "rtxAddressAdditional1",
            "isVisible": true,
            "left": "37px",
            "right": "33px",
            "skin": "sknRtxLatoReg84939e13px",
            "text": "Dear { User name }\n<BR>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet turpis purus. Sed consectetur quam non est lacinia blandit. Nullam condimentum turpis metus, sed egestas ante sagittis ut. Nam bibendum lorem turpis, non venenatis dui imperdiet at.\n<BR>\nAliquam mollis ipsum ac purus vestibulum, vel viverra metus finibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel lacus et quam consectetur accumsan.\n<BR>\nWarm regards,<BR>\n{ Sender name }<BR>\n{ Designation }<BR>\n{ Bank name }",
            "top": "5px",
            "zIndex": 3
        }, controller.args[0], "rtxAddressAdditional1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 1, 1, 1],
            "paddingInPixel": false
        }, controller.args[1], "rtxAddressAdditional1"), extendConfig({}, controller.args[2], "rtxAddressAdditional1"));
        flxAddressDetails.add(rtxAddressAdditional1);
        flxTemplateBody.add(flxAddressDetails);
        var flxAdditionalNote = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "50px",
            "clipBounds": true,
            "id": "flxAdditionalNote",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknNormalDefault",
            "zIndex": 1
        }, controller.args[0], "flxAdditionalNote"), extendConfig({}, controller.args[1], "flxAdditionalNote"), extendConfig({}, controller.args[2], "flxAdditionalNote"));
        flxAdditionalNote.setDefaultUnit(kony.flex.DP);
        var lblAdditionalNoteHeader = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalNoteHeader",
            "isVisible": true,
            "left": "44dp",
            "skin": "sknLatoRegular485c7514px",
            "text": "ADDITIONAL NOTE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalNoteHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalNoteHeader"), extendConfig({}, controller.args[2], "lblAdditionalNoteHeader"));
        var lblAdditionalNoteValue = new kony.ui.Label(extendConfig({
            "bottom": "10dp",
            "id": "lblAdditionalNoteValue",
            "isVisible": true,
            "left": "45dp",
            "skin": "sknLblLato84939e13px",
            "text": "Label",
            "top": "25dp",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "lblAdditionalNoteValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalNoteValue"), extendConfig({}, controller.args[2], "lblAdditionalNoteValue"));
        flxAdditionalNote.add(lblAdditionalNoteHeader, lblAdditionalNoteValue);
        TemplateMessage.add(flxTemplateHeader, flxTemplateBody, flxAdditionalNote);
        return TemplateMessage;
    }
})