define(function() {
    return function(controller) {
        var detailHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "62px",
            "id": "detailHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "625px"
        }, controller.args[0], "detailHeader"), extendConfig({}, controller.args[1], "detailHeader"), extendConfig({}, controller.args[2], "detailHeader"));
        detailHeader.setDefaultUnit(kony.flex.DP);
        var flxDetailHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDetailHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknheaderCSR1",
            "top": "0dp",
            "width": "617px",
            "zIndex": 1
        }, controller.args[0], "flxDetailHeader"), extendConfig({}, controller.args[1], "flxDetailHeader"), extendConfig({}, controller.args[2], "flxDetailHeader"));
        flxDetailHeader.setDefaultUnit(kony.flex.DP);
        var flxHeader1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeader1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop0i2986200ef4e48cursor",
            "top": "0dp",
            "width": "104px",
            "zIndex": 1
        }, controller.args[0], "flxHeader1"), extendConfig({}, controller.args[1], "flxHeader1"), extendConfig({}, controller.args[2], "flxHeader1"));
        flxHeader1.setDefaultUnit(kony.flex.DP);
        var lblCount1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblCount1",
            "isVisible": true,
            "skin": "sknnumericCsr",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCount1\")",
            "top": "14px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCount1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCount1"), extendConfig({}, controller.args[2], "lblCount1"));
        var flxDetials1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxDetials1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "28px",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "CopyslFbox",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "flxDetials1"), extendConfig({}, controller.args[1], "flxDetials1"), extendConfig({}, controller.args[2], "flxDetials1"));
        flxDetials1.setDefaultUnit(kony.flex.DP);
        var lblFontIconStatus1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblFontIconStatus1",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon13pxBlue",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblFontIconStatus1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconStatus1"), extendConfig({}, controller.args[2], "lblFontIconStatus1"));
        var lblStatus1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatus1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular485c75Font12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.NEW\")",
            "width": "40px",
            "zIndex": 1
        }, controller.args[0], "lblStatus1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus1"), extendConfig({}, controller.args[2], "lblStatus1"));
        flxDetials1.add(lblFontIconStatus1, lblStatus1);
        var flxSelected = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "5px",
            "id": "flxSelected",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "skncsrSelected647390",
            "width": "102px",
            "zIndex": 3
        }, controller.args[0], "flxSelected"), extendConfig({}, controller.args[1], "flxSelected"), extendConfig({}, controller.args[2], "flxSelected"));
        flxSelected.setDefaultUnit(kony.flex.DP);
        flxSelected.add();
        var flxseparator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxseparator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknSeparatorCsr",
            "top": 0,
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxseparator"), extendConfig({}, controller.args[1], "flxseparator"), extendConfig({}, controller.args[2], "flxseparator"));
        flxseparator.setDefaultUnit(kony.flex.DP);
        flxseparator.add();
        flxHeader1.add(lblCount1, flxDetials1, flxSelected, flxseparator);
        var flxHeader2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeader2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop0jb4a0a5ca39742Cursor",
            "top": "0dp",
            "width": "137px",
            "zIndex": 1
        }, controller.args[0], "flxHeader2"), extendConfig({}, controller.args[1], "flxHeader2"), extendConfig({}, controller.args[2], "flxHeader2"));
        flxHeader2.setDefaultUnit(kony.flex.DP);
        var lblCount2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblCount2",
            "isVisible": true,
            "skin": "sknnumericCsr",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCount2\")",
            "top": "14px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCount2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCount2"), extendConfig({}, controller.args[2], "lblCount2"));
        var flxDetials2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxDetials2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "CopyslFbox",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "flxDetials2"), extendConfig({}, controller.args[1], "flxDetials2"), extendConfig({}, controller.args[2], "flxDetials2"));
        flxDetials2.setDefaultUnit(kony.flex.DP);
        var lblFontIconStatus2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblFontIconStatus2",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIconInProgress",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblFontIconStatus2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconStatus2"), extendConfig({}, controller.args[2], "lblFontIconStatus2"));
        var lblStatus2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatus2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular485c75Font12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.IN_PROGRESS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus2"), extendConfig({}, controller.args[2], "lblStatus2"));
        flxDetials2.add(lblFontIconStatus2, lblStatus2);
        var flxSelected2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "5dp",
            "id": "flxSelected2",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "skncsrSelected647390",
            "width": "135px",
            "zIndex": 3
        }, controller.args[0], "flxSelected2"), extendConfig({}, controller.args[1], "flxSelected2"), extendConfig({}, controller.args[2], "flxSelected2"));
        flxSelected2.setDefaultUnit(kony.flex.DP);
        flxSelected2.add();
        var flxseparator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxseparator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknSeparatorCsr",
            "top": 0,
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxseparator2"), extendConfig({}, controller.args[1], "flxseparator2"), extendConfig({}, controller.args[2], "flxseparator2"));
        flxseparator2.setDefaultUnit(kony.flex.DP);
        flxseparator2.add();
        flxHeader2.add(lblCount2, flxDetials2, flxSelected2, flxseparator2);
        var flxHeader3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeader3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop0jb4a0a5ca39742Cursor",
            "top": "0dp",
            "width": "127px",
            "zIndex": 1
        }, controller.args[0], "flxHeader3"), extendConfig({}, controller.args[1], "flxHeader3"), extendConfig({}, controller.args[2], "flxHeader3"));
        flxHeader3.setDefaultUnit(kony.flex.DP);
        var lblCount3 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblCount3",
            "isVisible": true,
            "skin": "sknnumericCsr",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCount2\")",
            "top": "14px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCount3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCount3"), extendConfig({}, controller.args[2], "lblCount3"));
        var flxDetials3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxDetials3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "CopyslFbox",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "flxDetials3"), extendConfig({}, controller.args[1], "flxDetials3"), extendConfig({}, controller.args[2], "flxDetials3"));
        flxDetials3.setDefaultUnit(kony.flex.DP);
        var lblFontIconStatus3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblFontIconStatus3",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon13pxGreen",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblFontIconStatus3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconStatus3"), extendConfig({}, controller.args[2], "lblFontIconStatus3"));
        var lblStatus3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatus3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular485c75Font12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.RESOLVED\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus3"), extendConfig({}, controller.args[2], "lblStatus3"));
        flxDetials3.add(lblFontIconStatus3, lblStatus3);
        var flxSelected3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "5dp",
            "id": "flxSelected3",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "right": "0px",
            "skin": "skncsrSelected647390",
            "width": "125px",
            "zIndex": 3
        }, controller.args[0], "flxSelected3"), extendConfig({}, controller.args[1], "flxSelected3"), extendConfig({}, controller.args[2], "flxSelected3"));
        flxSelected3.setDefaultUnit(kony.flex.DP);
        flxSelected3.add();
        var flxseparator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxseparator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknSeparatorCsr",
            "top": 0,
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxseparator3"), extendConfig({}, controller.args[1], "flxseparator3"), extendConfig({}, controller.args[2], "flxseparator3"));
        flxseparator3.setDefaultUnit(kony.flex.DP);
        flxseparator3.add();
        flxHeader3.add(lblCount3, flxDetials3, flxSelected3, flxseparator3);
        var flxHeader4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeader4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop0jb4a0a5ca39742Cursor",
            "top": "0dp",
            "width": "127px",
            "zIndex": 1
        }, controller.args[0], "flxHeader4"), extendConfig({}, controller.args[1], "flxHeader4"), extendConfig({}, controller.args[2], "flxHeader4"));
        flxHeader4.setDefaultUnit(kony.flex.DP);
        var lblCount4 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblCount4",
            "isVisible": true,
            "skin": "sknnumericCsr",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCount2\")",
            "top": "14px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCount4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCount4"), extendConfig({}, controller.args[2], "lblCount4"));
        var flxDetials4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxDetials4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "CopyslFbox",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "flxDetials4"), extendConfig({}, controller.args[1], "flxDetials4"), extendConfig({}, controller.args[2], "flxDetials4"));
        flxDetials4.setDefaultUnit(kony.flex.DP);
        var lblFontIconStatus4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblFontIconStatus4",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIconArchived",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblFontIconStatus4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconStatus4"), extendConfig({}, controller.args[2], "lblFontIconStatus4"));
        var lblStatus4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatus4",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular485c75Font12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.ARCHIVED\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus4"), extendConfig({}, controller.args[2], "lblStatus4"));
        flxDetials4.add(lblFontIconStatus4, lblStatus4);
        var flxSelected4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "5dp",
            "id": "flxSelected4",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "right": "0px",
            "skin": "skncsrSelected647390",
            "width": "125px",
            "zIndex": 3
        }, controller.args[0], "flxSelected4"), extendConfig({}, controller.args[1], "flxSelected4"), extendConfig({}, controller.args[2], "flxSelected4"));
        flxSelected4.setDefaultUnit(kony.flex.DP);
        flxSelected4.add();
        var flxseparator4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxseparator4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknSeparatorCsr",
            "top": 0,
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxseparator4"), extendConfig({}, controller.args[1], "flxseparator4"), extendConfig({}, controller.args[2], "flxseparator4"));
        flxseparator4.setDefaultUnit(kony.flex.DP);
        flxseparator4.add();
        flxHeader4.add(lblCount4, flxDetials4, flxSelected4, flxseparator4);
        var flxHeader5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeader5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "2px",
            "skin": "sknflxffffffop0jb4a0a5ca39742Cursor",
            "top": "0dp",
            "width": "120px",
            "zIndex": 2
        }, controller.args[0], "flxHeader5"), extendConfig({}, controller.args[1], "flxHeader5"), extendConfig({}, controller.args[2], "flxHeader5"));
        flxHeader5.setDefaultUnit(kony.flex.DP);
        var lblCount5 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblCount5",
            "isVisible": true,
            "skin": "sknnumericCsr",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCount2\")",
            "top": "14px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCount5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCount5"), extendConfig({}, controller.args[2], "lblCount5"));
        var flxDetials5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxDetials5",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "CopyslFbox",
            "top": "30px",
            "width": "90dp",
            "zIndex": 1
        }, controller.args[0], "flxDetials5"), extendConfig({}, controller.args[1], "flxDetials5"), extendConfig({}, controller.args[2], "flxDetials5"));
        flxDetials5.setDefaultUnit(kony.flex.DP);
        var lblFontIconStatus5 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblFontIconStatus5",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon13pxDarkBlue",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblFontIconStatus5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconStatus5"), extendConfig({}, controller.args[2], "lblFontIconStatus5"));
        var lblStatus5 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatus5",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular485c75Font12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmDashboard.lblMyQueue\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus5"), extendConfig({}, controller.args[2], "lblStatus5"));
        flxDetials5.add(lblFontIconStatus5, lblStatus5);
        var flxSelected5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "5dp",
            "id": "flxSelected5",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "right": "0px",
            "skin": "skncsrSelected647390",
            "width": "126px",
            "zIndex": 3
        }, controller.args[0], "flxSelected5"), extendConfig({}, controller.args[1], "flxSelected5"), extendConfig({}, controller.args[2], "flxSelected5"));
        flxSelected5.setDefaultUnit(kony.flex.DP);
        flxSelected5.add();
        flxHeader5.add(lblCount5, flxDetials5, flxSelected5);
        flxDetailHeader.add(flxHeader1, flxHeader2, flxHeader3, flxHeader4, flxHeader5);
        detailHeader.add(flxDetailHeader);
        return detailHeader;
    }
})