define("com/adminConsole/header/subHeader/usersubHeaderController", function() {
    return {
        setFlowActions: function() {
            var scopeObj = this;
            this.view.tbxSearchBox.onTouchStart = function() {
                scopeObj.view.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
            };
            this.view.tbxSearchBox.onEndEditing = function() {
                scopeObj.view.flxSearchContainer.skin = "sknflxd5d9ddop100";
            };
        },
    };
});
define("com/adminConsole/header/subHeader/subHeaderControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for subHeader **/
    AS_FlexContainer_f11d59456811475ba309998f08be5a1b: function AS_FlexContainer_f11d59456811475ba309998f08be5a1b(eventobject) {
        var self = this;
        this.setFlowActions();
    }
});
define("com/adminConsole/header/subHeader/subHeaderController", ["com/adminConsole/header/subHeader/usersubHeaderController", "com/adminConsole/header/subHeader/subHeaderControllerActions"], function() {
    var controller = require("com/adminConsole/header/subHeader/usersubHeaderController");
    var actions = require("com/adminConsole/header/subHeader/subHeaderControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
