define("com/adminConsole/header/search/usersearchController", function() {
    return {
        setCompFlowActions: function() {
            var scopeObj = this;
            this.view.fonticonClose.isVisible = false;
            this.view.tbxSearchBox.onKeyDown = function() {
                scopeObj.showCancel();
            };
            //          this.view.flxSearchCancel.onClick = function(){
            //           scopeObj.resetTextBox();
            //         };
        },
        showCancel: function() {
            this.view.fonticonClose.isVisible = true;
            this.view.forceLayout();
        },
        resetTextBox: function() {
            this.view.tbxSearchBox.text = "";
            this.view.fonticonClose.isVisible = false;
            this.view.forceLayout();
        }
    };
});
define("com/adminConsole/header/search/searchControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for search **/
    AS_FlexContainer_df83650c21f04fea99b9cd0ae0e26e4c: function AS_FlexContainer_df83650c21f04fea99b9cd0ae0e26e4c(eventobject) {
        var self = this;
        this.setCompFlowActions();
    }
});
define("com/adminConsole/header/search/searchController", ["com/adminConsole/header/search/usersearchController", "com/adminConsole/header/search/searchControllerActions"], function() {
    var controller = require("com/adminConsole/header/search/usersearchController");
    var actions = require("com/adminConsole/header/search/searchControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
