define(function() {
    return function(controller) {
        var search = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "48dp",
            "id": "search",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_df83650c21f04fea99b9cd0ae0e26e4c(eventobject);
            },
            "skin": "CopyslFbox2",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "search"), extendConfig({}, controller.args[1], "search"), extendConfig({}, controller.args[2], "search"));
        search.setDefaultUnit(kony.flex.DP);
        var flxSubHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSubHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0b7aaa2e3bfa340",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSubHeader"), extendConfig({}, controller.args[1], "flxSubHeader"), extendConfig({}, controller.args[2], "flxSubHeader"));
        flxSubHeader.setDefaultUnit(kony.flex.DP);
        var lblShowing = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblShowing",
            "isVisible": false,
            "left": "20dp",
            "skin": "sknlblLatoRegular484b5216px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblShowing\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblShowing"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblShowing"), extendConfig({}, controller.args[2], "lblShowing"));
        var flxSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 32,
            "skin": "CopyslFbox2",
            "top": "0px",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxSearch"), extendConfig({}, controller.args[1], "flxSearch"), extendConfig({}, controller.args[2], "flxSearch"));
        flxSearch.setDefaultUnit(kony.flex.DP);
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxd5d9ddop100",
            "top": "8dp",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var imgSearchIcon = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "imgSearchIcon",
            "isVisible": false,
            "left": "15px",
            "skin": "CopyslImage2",
            "src": "search_1x.png",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "imgSearchIcon"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSearchIcon"), extendConfig({}, controller.args[2], "imgSearchIcon"));
        var fonticonSearch = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonSearch",
            "isVisible": true,
            "left": "15px",
            "skin": "sknfontIconSearch",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonSearch"), extendConfig({}, controller.args[2], "fonticonSearch"));
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "46%",
            "height": "30px",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30dp",
            "placeholder": "Search",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var flxSearchCancel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "12px",
            "id": "flxSearchCancel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15px",
            "skin": "sknCursor",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxSearchCancel"), extendConfig({}, controller.args[1], "flxSearchCancel"), extendConfig({}, controller.args[2], "flxSearchCancel"));
        flxSearchCancel.setDefaultUnit(kony.flex.DP);
        var imgSearchCancel = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12px",
            "id": "imgSearchCancel",
            "isVisible": false,
            "right": "0px",
            "skin": "CopyslImage2",
            "src": "close_blue.png",
            "width": "12px",
            "zIndex": 2
        }, controller.args[0], "imgSearchCancel"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSearchCancel"), extendConfig({}, controller.args[2], "imgSearchCancel"));
        var fonticonClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonClose",
            "isVisible": false,
            "right": "0px",
            "skin": "sknfontIconSearch",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonClose"), extendConfig({}, controller.args[2], "fonticonClose"));
        flxSearchCancel.add(imgSearchCancel, fonticonClose);
        flxSearchContainer.add(imgSearchIcon, fonticonSearch, tbxSearchBox, flxSearchCancel);
        flxSearch.add(flxSearchContainer);
        flxSubHeader.add(lblShowing, flxSearch);
        search.add(flxSubHeader);
        return search;
    }
})