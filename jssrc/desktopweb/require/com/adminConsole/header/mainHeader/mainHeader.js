define(function() {
    return function(controller) {
        var mainHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "106px",
            "id": "mainHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": 0,
            "isModalContainer": false,
            "skin": "CopyslFbox1",
            "top": 0,
            "width": "100%"
        }, controller.args[0], "mainHeader"), extendConfig({}, controller.args[1], "mainHeader"), extendConfig({}, controller.args[2], "mainHeader"));
        mainHeader.setDefaultUnit(kony.flex.DP);
        var flxMainHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMainHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxMainHeader"), extendConfig({}, controller.args[1], "flxMainHeader"), extendConfig({}, controller.args[2], "flxMainHeader"));
        flxMainHeader.setDefaultUnit(kony.flex.DP);
        var lblHeading = new kony.ui.Label(extendConfig({
            "centerY": 80,
            "height": "40dp",
            "id": "lblHeading",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknLblLato",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmDashboard.lblDate\")",
            "width": "40%",
            "zIndex": 1
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        var flxButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "32dp",
            "id": "flxButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "60px",
            "width": "400dp",
            "zIndex": 2
        }, controller.args[0], "flxButtons"), extendConfig({}, controller.args[1], "flxButtons"), extendConfig({}, controller.args[2], "flxButtons"));
        flxButtons.setDefaultUnit(kony.flex.DP);
        var flxAddNewOption = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "32dp",
            "id": "flxAddNewOption",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "12dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%",
            "zIndex": 2
        }, controller.args[0], "flxAddNewOption"), extendConfig({}, controller.args[1], "flxAddNewOption"), extendConfig({}, controller.args[2], "flxAddNewOption"));
        flxAddNewOption.setDefaultUnit(kony.flex.DP);
        var lblAddNewOption = new kony.ui.Label(extendConfig({
            "height": "32dp",
            "id": "lblAddNewOption",
            "isVisible": true,
            "left": 0,
            "right": 0,
            "skin": "sknLblOpacity0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.ADDNEWPERMISSION\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblAddNewOption"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [10, 4, 10, 5],
            "paddingInPixel": false
        }, controller.args[1], "lblAddNewOption"), extendConfig({}, controller.args[2], "lblAddNewOption"));
        flxAddNewOption.add(lblAddNewOption);
        var flxDownloadList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "32dp",
            "id": "flxDownloadList",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "40%",
            "zIndex": 2
        }, controller.args[0], "flxDownloadList"), extendConfig({}, controller.args[1], "flxDownloadList"), extendConfig({}, controller.args[2], "flxDownloadList"));
        flxDownloadList.setDefaultUnit(kony.flex.DP);
        var lblDownloadList = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "32dp",
            "id": "lblDownloadList",
            "isVisible": true,
            "left": 0,
            "right": 0,
            "skin": "sknLblOpacity0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblDownloadList"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [10, 5, 10, 5],
            "paddingInPixel": false
        }, controller.args[1], "lblDownloadList"), extendConfig({}, controller.args[2], "lblDownloadList"));
        flxDownloadList.add(lblDownloadList);
        var btnAddNewOption = new kony.ui.Button(extendConfig({
            "focusSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px",
            "height": "30px",
            "id": "btnAddNewOption",
            "isVisible": true,
            "right": "168dp",
            "skin": "sknBtn205584LatoBoldffffff13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.ADDNEWPERMISSION\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "btnAddNewOption"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [6, 1, 6, 1],
            "paddingInPixel": false
        }, controller.args[1], "btnAddNewOption"), extendConfig({
            "hoverSkin": "sknBtn1682b2LatoBoldffffff13px"
        }, controller.args[2], "btnAddNewOption"));
        var btnDropdownList = new kony.ui.Button(extendConfig({
            "height": "30px",
            "id": "btnDropdownList",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknBtn5bc06cLatoBoldffffff10px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DOWNLOADLIST\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "btnDropdownList"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [6, 1, 6, 1],
            "paddingInPixel": false
        }, controller.args[1], "btnDropdownList"), extendConfig({}, controller.args[2], "btnDropdownList"));
        flxButtons.add(flxAddNewOption, flxDownloadList, btnAddNewOption, btnDropdownList);
        var flxHeaderUserOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "24dp",
            "id": "flxHeaderUserOptions",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "20dp",
            "width": "30%",
            "zIndex": 2
        }, controller.args[0], "flxHeaderUserOptions"), extendConfig({}, controller.args[1], "flxHeaderUserOptions"), extendConfig({}, controller.args[2], "flxHeaderUserOptions"));
        flxHeaderUserOptions.setDefaultUnit(kony.flex.DP);
        var lblUserName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblUserName",
            "isVisible": true,
            "right": 25,
            "skin": "slLabel0d21bb75081954f",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblUser\")",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUserName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUserName"), extendConfig({}, controller.args[2], "lblUserName"));
        var imgLogout = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "imgLogout",
            "isVisible": true,
            "onTouchStart": controller.AS_Image_j15f5719709148459da2ca8b6b9f27da,
            "right": "0px",
            "skin": "sknImgHandCursor",
            "src": "img_logout.png",
            "width": "18dp",
            "zIndex": 1
        }, controller.args[0], "imgLogout"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgLogout"), extendConfig({
            "toolTip": "Sign Out"
        }, controller.args[2], "imgLogout"));
        flxHeaderUserOptions.add(lblUserName, imgLogout);
        var flxHeaderSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "1dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxHeaderSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "right": "35dp",
            "skin": "sknflx115678",
            "zIndex": 1
        }, controller.args[0], "flxHeaderSeperator"), extendConfig({}, controller.args[1], "flxHeaderSeperator"), extendConfig({}, controller.args[2], "flxHeaderSeperator"));
        flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
        flxHeaderSeperator.add();
        flxMainHeader.add(lblHeading, flxButtons, flxHeaderUserOptions, flxHeaderSeperator);
        mainHeader.add(flxMainHeader);
        return mainHeader;
    }
})