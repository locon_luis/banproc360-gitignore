define(function() {
    return function(controller) {
        var dateRangePicker = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "dateRangePicker",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "postShow": controller.AS_FlexContainer_g435b753a90b4a15b1fec7daa5aeaae0,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_ce738bae91614533a035f7216c3471e4(eventobject);
            },
            "skin": "sknflxBg000000Op50",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "dateRangePicker"), extendConfig({}, controller.args[1], "dateRangePicker"), extendConfig({}, controller.args[2], "dateRangePicker"));
        dateRangePicker.setDefaultUnit(kony.flex.DP);
        var flxRangePicker = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": false,
            "id": "flxRangePicker",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox0j9f841cc563e4e",
            "top": "125px",
            "width": "350px",
            "zIndex": 10
        }, controller.args[0], "flxRangePicker"), extendConfig({}, controller.args[1], "flxRangePicker"), extendConfig({}, controller.args[2], "flxRangePicker"));
        flxRangePicker.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxCloseRangePicker = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxCloseRangePicker",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "slFbox",
            "width": "25px",
            "zIndex": 20
        }, controller.args[0], "flxCloseRangePicker"), extendConfig({}, controller.args[1], "flxCloseRangePicker"), extendConfig({}, controller.args[2], "flxCloseRangePicker"));
        flxCloseRangePicker.setDefaultUnit(kony.flex.DP);
        var imgCloseRangePicker = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgCloseRangePicker",
            "isVisible": true,
            "skin": "slImage",
            "src": "close_blue.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgCloseRangePicker"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCloseRangePicker"), extendConfig({}, controller.args[2], "imgCloseRangePicker"));
        flxCloseRangePicker.add(imgCloseRangePicker);
        var lblHeading = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeading",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato00000012px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading\")",
            "top": "13dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        flxHeader.add(flxCloseRangePicker, lblHeading);
        var flxRangePickerBody = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxRangePickerBody",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0d9c3974835234d",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRangePickerBody"), extendConfig({}, controller.args[1], "flxRangePickerBody"), extendConfig({}, controller.args[2], "flxRangePickerBody"));
        flxRangePickerBody.setDefaultUnit(kony.flex.DP);
        var lblFrom = new kony.ui.Label(extendConfig({
            "id": "lblFrom",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato00000012px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblFrom\")",
            "top": "13dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFrom"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFrom"), extendConfig({}, controller.args[2], "lblFrom"));
        var calFrom = new kony.ui.Calendar(extendConfig({
            "calendarIcon": "calbtn.png",
            "dateFormat": "MM/dd/yyyy",
            "height": "40dp",
            "id": "calFrom",
            "isVisible": true,
            "left": "20dp",
            "placeholder": "Select Date",
            "top": "40dp",
            "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
            "width": "145px",
            "zIndex": 1
        }, controller.args[0], "calFrom"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "calFrom"), extendConfig({
            "noOfMonths": 1
        }, controller.args[2], "calFrom"));
        var lblTo = new kony.ui.Label(extendConfig({
            "id": "lblTo",
            "isVisible": true,
            "left": "185px",
            "skin": "sknlblLato00000012px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblTo\")",
            "top": "13dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTo"), extendConfig({}, controller.args[2], "lblTo"));
        var calTo = new kony.ui.Calendar(extendConfig({
            "calendarIcon": "calbtn.png",
            "dateFormat": "MM/dd/yyyy",
            "height": "40dp",
            "id": "calTo",
            "isVisible": true,
            "left": "185px",
            "placeholder": "Select Date",
            "top": "40dp",
            "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
            "width": "145px",
            "zIndex": 1
        }, controller.args[0], "calTo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "calTo"), extendConfig({
            "noOfMonths": 1
        }, controller.args[2], "calTo"));
        var flxInlineError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxInlineError",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "80px",
            "zIndex": 1
        }, controller.args[0], "flxInlineError"), extendConfig({}, controller.args[1], "flxInlineError"), extendConfig({}, controller.args[2], "flxInlineError"));
        flxInlineError.setDefaultUnit(kony.flex.DP);
        var lblInlineError = new kony.ui.Label(extendConfig({
            "id": "lblInlineError",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.Fill_date_to_proceed\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblInlineError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblInlineError"), extendConfig({}, controller.args[2], "lblInlineError"));
        flxInlineError.add(lblInlineError);
        flxRangePickerBody.add(lblFrom, calFrom, lblTo, calTo, flxInlineError);
        var flxRangePickerButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxRangePickerButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRangePickerButtons"), extendConfig({}, controller.args[1], "flxRangePickerButtons"), extendConfig({}, controller.args[2], "flxRangePickerButtons"));
        flxRangePickerButtons.setDefaultUnit(kony.flex.DP);
        var btnCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
            "height": "30px",
            "id": "btnCancel",
            "isVisible": true,
            "right": "110px",
            "skin": "sknBtnLatoRegulara5abc413pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [5, 0, 5, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
        }, controller.args[2], "btnCancel"));
        var btnSelect = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
            "height": "30px",
            "id": "btnSelect",
            "isVisible": true,
            "right": "20px",
            "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnSelect\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnSelect"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [5, 0, 5, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSelect"), extendConfig({
            "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
        }, controller.args[2], "btnSelect"));
        var flxSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxd6dbe7",
            "top": "0px",
            "zIndex": 20
        }, controller.args[0], "flxSeperator"), extendConfig({}, controller.args[1], "flxSeperator"), extendConfig({}, controller.args[2], "flxSeperator"));
        flxSeperator.setDefaultUnit(kony.flex.DP);
        flxSeperator.add();
        flxRangePickerButtons.add(btnCancel, btnSelect, flxSeperator);
        flxRangePicker.add(flxHeader, flxRangePickerBody, flxRangePickerButtons);
        dateRangePicker.add(flxRangePicker);
        return dateRangePicker;
    }
})