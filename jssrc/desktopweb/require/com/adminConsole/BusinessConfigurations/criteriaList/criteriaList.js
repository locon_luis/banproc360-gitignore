define(function() {
    return function(controller) {
        var criteriaList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "isMaster": true,
            "id": "criteriaList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_ed824b012e1847b89f735ba85a1de434(eventobject);
            },
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "criteriaList"), extendConfig({}, controller.args[1], "criteriaList"), extendConfig({}, controller.args[2], "criteriaList"));
        criteriaList.setDefaultUnit(kony.flex.DP);
        var flxTableView = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxTableView",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTableView"), extendConfig({}, controller.args[1], "flxTableView"), extendConfig({}, controller.args[2], "flxTableView"));
        flxTableView.setDefaultUnit(kony.flex.DP);
        var flxCriteriaHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65px",
            "id": "flxCriteriaHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": 0,
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxCriteriaHeader"), extendConfig({}, controller.args[1], "flxCriteriaHeader"), extendConfig({}, controller.args[2], "flxCriteriaHeader"));
        flxCriteriaHeader.setDefaultUnit(kony.flex.DP);
        var flxEligibilityCriterias = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxEligibilityCriterias",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "70px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 0,
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxEligibilityCriterias"), extendConfig({}, controller.args[1], "flxEligibilityCriterias"), extendConfig({}, controller.args[2], "flxEligibilityCriterias"));
        flxEligibilityCriterias.setDefaultUnit(kony.flex.DP);
        var lblCriteriaDesc = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCriteriaDesc",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "ELIGIBILITY CRITERIAS",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCriteriaDesc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCriteriaDesc"), extendConfig({}, controller.args[2], "lblCriteriaDesc"));
        flxEligibilityCriterias.add(lblCriteriaDesc);
        var flxCriteriaStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxCriteriaStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "11%",
            "skin": "slFbox",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "flxCriteriaStatus"), extendConfig({}, controller.args[1], "flxCriteriaStatus"), extendConfig({}, controller.args[2], "flxCriteriaStatus"));
        flxCriteriaStatus.setDefaultUnit(kony.flex.DP);
        var lblStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
            "top": 0,
            "width": "46px",
            "zIndex": 1
        }, controller.args[0], "lblStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblStatus"));
        var fontIconFilterStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconFilterStatus",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconFilterStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconFilterStatus"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconFilterStatus"));
        flxCriteriaStatus.add(lblStatus, fontIconFilterStatus);
        var lblHeaderSeperator = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1px",
            "id": "lblHeaderSeperator",
            "isVisible": true,
            "left": "35dp",
            "right": "35dp",
            "skin": "sknLblTableHeaderLine",
            "text": "-",
            "zIndex": 1
        }, controller.args[0], "lblHeaderSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderSeperator"), extendConfig({}, controller.args[2], "lblHeaderSeperator"));
        flxCriteriaHeader.add(flxEligibilityCriterias, flxCriteriaStatus, lblHeaderSeperator);
        var segCriteria = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "fonticonActive": "",
                "lblCriteriaDesc": "Jompay",
                "lblCriteriaStatus": "Active",
                "lblOptions": "",
                "lblSeparator": "Label"
            }, {
                "fonticonActive": "",
                "lblCriteriaDesc": "Jompay",
                "lblCriteriaStatus": "Active",
                "lblOptions": "",
                "lblSeparator": "Label"
            }, {
                "fonticonActive": "",
                "lblCriteriaDesc": "Jompay",
                "lblCriteriaStatus": "Active",
                "lblOptions": "",
                "lblSeparator": "Label"
            }, {
                "fonticonActive": "",
                "lblCriteriaDesc": "Jompay",
                "lblCriteriaStatus": "Active",
                "lblOptions": "",
                "lblSeparator": "Label"
            }, {
                "fonticonActive": "",
                "lblCriteriaDesc": "Jompay",
                "lblCriteriaStatus": "Active",
                "lblOptions": "",
                "lblSeparator": "Label"
            }, {
                "fonticonActive": "",
                "lblCriteriaDesc": "Jompay",
                "lblCriteriaStatus": "Active",
                "lblOptions": "",
                "lblSeparator": "Label"
            }, {
                "fonticonActive": "",
                "lblCriteriaDesc": "Jompay",
                "lblCriteriaStatus": "Active",
                "lblOptions": "",
                "lblSeparator": "Label"
            }, {
                "fonticonActive": "",
                "lblCriteriaDesc": "Jompay",
                "lblCriteriaStatus": "Active",
                "lblOptions": "",
                "lblSeparator": "Label"
            }, {
                "fonticonActive": "",
                "lblCriteriaDesc": "Jompay",
                "lblCriteriaStatus": "Active",
                "lblOptions": "",
                "lblSeparator": "Label"
            }],
            "groupCells": false,
            "height": "250px",
            "id": "segCriteria",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxCriterias",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": true,
            "top": "65px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCriterias": "flxCriterias",
                "flxOptions": "flxOptions",
                "flxStatus": "flxStatus",
                "fonticonActive": "fonticonActive",
                "lblCriteriaDesc": "lblCriteriaDesc",
                "lblCriteriaStatus": "lblCriteriaStatus",
                "lblOptions": "lblOptions",
                "lblSeparator": "lblSeparator"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segCriteria"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segCriteria"), extendConfig({}, controller.args[2], "segCriteria"));
        flxTableView.add(flxCriteriaHeader, segCriteria);
        var flxSelectOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxSelectOptions",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "right": "35px",
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "140px",
            "width": "200px",
            "zIndex": 100
        }, controller.args[0], "flxSelectOptions"), extendConfig({}, controller.args[1], "flxSelectOptions"), extendConfig({}, controller.args[2], "flxSelectOptions"));
        flxSelectOptions.setDefaultUnit(kony.flex.DP);
        var flxEdit = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxEdit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0i3963a83703242",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEdit"), extendConfig({}, controller.args[1], "flxEdit"), extendConfig({}, controller.args[2], "flxEdit"));
        flxEdit.setDefaultUnit(kony.flex.DP);
        var fonticonEdit = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonEdit",
            "isVisible": true,
            "left": "15px",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonEdit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonEdit"), extendConfig({}, controller.args[2], "fonticonEdit"));
        var imgOption1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgOption1",
            "isVisible": false,
            "left": "15dp",
            "skin": "slImage",
            "src": "edit2x.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgOption1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgOption1"), extendConfig({}, controller.args[2], "imgOption1"));
        var lblOption1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption1",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
            "top": "4dp",
            "width": "70px",
            "zIndex": 1
        }, controller.args[0], "lblOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption1"), extendConfig({
            "hoverSkin": "sknlblLatoBold0hf492575252c4cStatic"
        }, controller.args[2], "lblOption1"));
        flxEdit.add(fonticonEdit, imgOption1, lblOption1);
        var flxDelete = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0h1f34f2be70f43",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDelete"), extendConfig({}, controller.args[1], "flxDelete"), extendConfig({}, controller.args[2], "flxDelete"));
        flxDelete.setDefaultUnit(kony.flex.DP);
        var fonticonDelete = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonDelete",
            "isVisible": true,
            "left": "15px",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonDelete"), extendConfig({}, controller.args[2], "fonticonDelete"));
        var imgOption3 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgOption3",
            "isVisible": false,
            "left": "15dp",
            "skin": "slImage",
            "src": "delete_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgOption3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgOption3"), extendConfig({}, controller.args[2], "imgOption3"));
        var lblOption3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption3",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
            "top": "4dp",
            "width": "70px",
            "zIndex": 1
        }, controller.args[0], "lblOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption3"), extendConfig({
            "hoverSkin": "sknlblLatoBold0hf492575252c4cStatic"
        }, controller.args[2], "lblOption3"));
        flxDelete.add(fonticonDelete, imgOption3, lblOption3);
        var flxDeactivate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxDeactivate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0a35230cb45374d",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDeactivate"), extendConfig({}, controller.args[1], "flxDeactivate"), extendConfig({}, controller.args[2], "flxDeactivate"));
        flxDeactivate.setDefaultUnit(kony.flex.DP);
        var fonticonDeactive = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonDeactive",
            "isVisible": true,
            "left": "15px",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDeactive\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonDeactive"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonDeactive"), extendConfig({}, controller.args[2], "fonticonDeactive"));
        var imgOption2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgOption2",
            "isVisible": false,
            "left": "15dp",
            "skin": "slImage",
            "src": "deactive_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgOption2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgOption2"), extendConfig({}, controller.args[2], "imgOption2"));
        var lblOption2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption2",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
            "top": "4dp",
            "width": "70px",
            "zIndex": 1
        }, controller.args[0], "lblOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption2"), extendConfig({
            "hoverSkin": "sknlblLatoBold0hf492575252c4cStatic"
        }, controller.args[2], "lblOption2"));
        flxDeactivate.add(fonticonDeactive, imgOption2, lblOption2);
        flxSelectOptions.add(flxEdit, flxDelete, flxDeactivate);
        criteriaList.add(flxTableView, flxSelectOptions);
        return criteriaList;
    }
})