define("com/adminConsole/BusinessConfigurations/configLeftMenu/userconfigLeftMenuController", function() {
    return {};
});
define("com/adminConsole/BusinessConfigurations/configLeftMenu/configLeftMenuControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/BusinessConfigurations/configLeftMenu/configLeftMenuController", ["com/adminConsole/BusinessConfigurations/configLeftMenu/userconfigLeftMenuController", "com/adminConsole/BusinessConfigurations/configLeftMenu/configLeftMenuControllerActions"], function() {
    var controller = require("com/adminConsole/BusinessConfigurations/configLeftMenu/userconfigLeftMenuController");
    var actions = require("com/adminConsole/BusinessConfigurations/configLeftMenu/configLeftMenuControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
