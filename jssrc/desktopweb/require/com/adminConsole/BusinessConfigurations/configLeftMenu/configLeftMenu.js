define(function() {
    return function(controller) {
        var configLeftMenu = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "configLeftMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "configLeftMenu"), extendConfig({}, controller.args[1], "configLeftMenu"), extendConfig({}, controller.args[2], "configLeftMenu"));
        configLeftMenu.setDefaultUnit(kony.flex.DP);
        var flxDetailHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDetailHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknheaderCSR1",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetailHeader"), extendConfig({}, controller.args[1], "flxDetailHeader"), extendConfig({}, controller.args[2], "flxDetailHeader"));
        flxDetailHeader.setDefaultUnit(kony.flex.DP);
        var flxHeader1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "52px",
            "id": "flxHeader1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeader1"), extendConfig({}, controller.args[1], "flxHeader1"), extendConfig({
            "hoverSkin": "sknCursorHoverDark"
        }, controller.args[2], "flxHeader1"));
        flxHeader1.setDefaultUnit(kony.flex.DP);
        var lblStatus1 = new kony.ui.Label(extendConfig({
            "centerY": "50.00%",
            "id": "lblStatus1",
            "isVisible": true,
            "left": "25px",
            "text": "ELIGIBILITY CRITERIA",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblStatus1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus1"), extendConfig({}, controller.args[2], "lblStatus1"));
        var flxSelected = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "52px",
            "id": "flxSelected",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "SknEligibilityCriteria",
            "width": "5px",
            "zIndex": 3
        }, controller.args[0], "flxSelected"), extendConfig({}, controller.args[1], "flxSelected"), extendConfig({}, controller.args[2], "flxSelected"));
        flxSelected.setDefaultUnit(kony.flex.DP);
        flxSelected.add();
        var flxseparator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1px",
            "id": "flxseparator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknSeparatorCsr",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxseparator1"), extendConfig({}, controller.args[1], "flxseparator1"), extendConfig({}, controller.args[2], "flxseparator1"));
        flxseparator1.setDefaultUnit(kony.flex.DP);
        flxseparator1.add();
        var flxSeperator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeperator2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflx115678",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeperator2"), extendConfig({}, controller.args[1], "flxSeperator2"), extendConfig({}, controller.args[2], "flxSeperator2"));
        flxSeperator2.setDefaultUnit(kony.flex.DP);
        flxSeperator2.add();
        flxHeader1.add(lblStatus1, flxSelected, flxseparator1, flxSeperator2);
        flxDetailHeader.add(flxHeader1);
        configLeftMenu.add(flxDetailHeader);
        return configLeftMenu;
    }
})