define("com/adminConsole/adManagement/channelScreen/userchannelScreenController", function() {
    return {};
});
define("com/adminConsole/adManagement/channelScreen/channelScreenControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/adManagement/channelScreen/channelScreenController", ["com/adminConsole/adManagement/channelScreen/userchannelScreenController", "com/adminConsole/adManagement/channelScreen/channelScreenControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/channelScreen/userchannelScreenController");
    var actions = require("com/adminConsole/adManagement/channelScreen/channelScreenControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
