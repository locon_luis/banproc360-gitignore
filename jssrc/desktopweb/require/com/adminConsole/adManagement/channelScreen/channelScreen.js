define(function() {
    return function(controller) {
        var channelScreen = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "channelScreen",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "channelScreen"), extendConfig({}, controller.args[1], "channelScreen"), extendConfig({}, controller.args[2], "channelScreen"));
        channelScreen.setDefaultUnit(kony.flex.DP);
        var flxScreenNameText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "84px",
            "id": "flxScreenNameText",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxScreenNameText"), extendConfig({}, controller.args[1], "flxScreenNameText"), extendConfig({}, controller.args[2], "flxScreenNameText"));
        flxScreenNameText.setDefaultUnit(kony.flex.DP);
        var lblScreenName = new kony.ui.Label(extendConfig({
            "height": "17px",
            "id": "lblScreenName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl192B45LatoRegular14px",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblScreenName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblScreenName"), extendConfig({}, controller.args[2], "lblScreenName"));
        var flxSeparator1Screen = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparator1Screen",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "top": "29px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator1Screen"), extendConfig({}, controller.args[1], "flxSeparator1Screen"), extendConfig({}, controller.args[2], "flxSeparator1Screen"));
        flxSeparator1Screen.setDefaultUnit(kony.flex.DP);
        flxSeparator1Screen.add();
        var flxULRLabels = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "33px",
            "id": "flxULRLabels",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "isModalContainer": false,
            "right": "10px",
            "skin": "slFbox",
            "top": "50px",
            "zIndex": 1
        }, controller.args[0], "flxULRLabels"), extendConfig({}, controller.args[1], "flxULRLabels"), extendConfig({}, controller.args[2], "flxULRLabels"));
        flxULRLabels.setDefaultUnit(kony.flex.DP);
        var lblResolution = new kony.ui.Label(extendConfig({
            "height": "13px",
            "id": "lblResolution",
            "isVisible": true,
            "left": "0%",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ResolutionCAPS\")",
            "top": "0%",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "lblResolution"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResolution"), extendConfig({}, controller.args[2], "lblResolution"));
        var lblImgSourceURLTag = new kony.ui.Label(extendConfig({
            "height": "13px",
            "id": "lblImgSourceURLTag",
            "isVisible": true,
            "left": "30%",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ImageSourceURLCAPS\")",
            "top": "0%",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "lblImgSourceURLTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgSourceURLTag"), extendConfig({}, controller.args[2], "lblImgSourceURLTag"));
        var lblTargetURLTag = new kony.ui.Label(extendConfig({
            "height": "13px",
            "id": "lblTargetURLTag",
            "isVisible": true,
            "left": "65%",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.TargetURLCAPS\")",
            "top": "0px",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "lblTargetURLTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTargetURLTag"), extendConfig({}, controller.args[2], "lblTargetURLTag"));
        flxULRLabels.add(lblResolution, lblImgSourceURLTag, lblTargetURLTag);
        var flxSeparator2Screen = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparator2Screen",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "top": "83px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator2Screen"), extendConfig({}, controller.args[1], "flxSeparator2Screen"), extendConfig({}, controller.args[2], "flxSeparator2Screen"));
        flxSeparator2Screen.setDefaultUnit(kony.flex.DP);
        flxSeparator2Screen.add();
        flxScreenNameText.add(lblScreenName, flxSeparator1Screen, flxULRLabels, flxSeparator2Screen);
        channelScreen.add(flxScreenNameText);
        return channelScreen;
    }
})