define("com/adminConsole/adManagement/channelDefaultScreen/userchannelDefaultScreenController", function() {
    return {};
});
define("com/adminConsole/adManagement/channelDefaultScreen/channelDefaultScreenControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/adManagement/channelDefaultScreen/channelDefaultScreenController", ["com/adminConsole/adManagement/channelDefaultScreen/userchannelDefaultScreenController", "com/adminConsole/adManagement/channelDefaultScreen/channelDefaultScreenControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/channelDefaultScreen/userchannelDefaultScreenController");
    var actions = require("com/adminConsole/adManagement/channelDefaultScreen/channelDefaultScreenControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
