define(function() {
    return function(controller) {
        var moduleTemplate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "moduleTemplate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "moduleTemplate"), extendConfig({}, controller.args[1], "moduleTemplate"), extendConfig({}, controller.args[2], "moduleTemplate"));
        moduleTemplate.setDefaultUnit(kony.flex.DP);
        var flxModuleTemplate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxModuleTemplate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxModuleTemplate"), extendConfig({}, controller.args[1], "flxModuleTemplate"), extendConfig({}, controller.args[2], "flxModuleTemplate"));
        flxModuleTemplate.setDefaultUnit(kony.flex.DP);
        var lblModuleName = new kony.ui.Label(extendConfig({
            "id": "lblModuleName",
            "isVisible": true,
            "left": "0dp",
            "skin": "skn485C75LatoRegular11Px",
            "text": "MODULE NAME",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblModuleName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblModuleName"), extendConfig({}, controller.args[2], "lblModuleName"));
        var switchModule = new kony.ui.Switch(extendConfig({
            "height": "20dp",
            "id": "switchModule",
            "isVisible": true,
            "left": "20dp",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "width": "35dp",
            "zIndex": 1
        }, controller.args[0], "switchModule"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "switchModule"), extendConfig({}, controller.args[2], "switchModule"));
        flxModuleTemplate.add(lblModuleName, switchModule);
        var flxModuleDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxModuleDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxModuleDetails"), extendConfig({}, controller.args[1], "flxModuleDetails"), extendConfig({}, controller.args[2], "flxModuleDetails"));
        flxModuleDetails.setDefaultUnit(kony.flex.DP);
        var flxFileTypeInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFileTypeInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxFileTypeInfo"), extendConfig({}, controller.args[1], "flxFileTypeInfo"), extendConfig({}, controller.args[2], "flxFileTypeInfo"));
        flxFileTypeInfo.setDefaultUnit(kony.flex.DP);
        var lblInfoIcon = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblInfoIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblIIcoMoon485c7514px",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblInfoIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblInfoIcon"), extendConfig({}, controller.args[2], "lblInfoIcon"));
        var lblFileType = new kony.ui.Label(extendConfig({
            "id": "lblFileType",
            "isVisible": true,
            "left": "10dp",
            "skin": "skn485C75LatoRegular11Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.FileType\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFileType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFileType"), extendConfig({}, controller.args[2], "lblFileType"));
        flxFileTypeInfo.add(lblInfoIcon, lblFileType);
        flxModuleDetails.add(flxFileTypeInfo);
        moduleTemplate.add(flxModuleTemplate, flxModuleDetails);
        return moduleTemplate;
    }
})