define("com/adminConsole/adManagement/moduleTemplate/usermoduleTemplateController", function() {
    return {};
});
define("com/adminConsole/adManagement/moduleTemplate/moduleTemplateControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/adManagement/moduleTemplate/moduleTemplateController", ["com/adminConsole/adManagement/moduleTemplate/usermoduleTemplateController", "com/adminConsole/adManagement/moduleTemplate/moduleTemplateControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/moduleTemplate/usermoduleTemplateController");
    var actions = require("com/adminConsole/adManagement/moduleTemplate/moduleTemplateControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
