define(function() {
    return function(controller) {
        var channelTemplate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "channelTemplate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25dp",
            "width": "100%"
        }, controller.args[0], "channelTemplate"), extendConfig({}, controller.args[1], "channelTemplate"), extendConfig({}, controller.args[2], "channelTemplate"));
        channelTemplate.setDefaultUnit(kony.flex.DP);
        var flxChannelHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxChannelHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxChannelHeader"), extendConfig({}, controller.args[1], "flxChannelHeader"), extendConfig({}, controller.args[2], "flxChannelHeader"));
        flxChannelHeader.setDefaultUnit(kony.flex.DP);
        var lblChannelArrow = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblChannelArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconDescRightArrow14px",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblChannelArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannelArrow"), extendConfig({}, controller.args[2], "lblChannelArrow"));
        var lblChannelName = new kony.ui.Label(extendConfig({
            "id": "lblChannelName",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLblLato485c7513px",
            "text": "MODULE NAME",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblChannelName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannelName"), extendConfig({}, controller.args[2], "lblChannelName"));
        flxChannelHeader.add(lblChannelArrow, lblChannelName);
        var flxChannelDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxChannelDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxChannelDetails"), extendConfig({}, controller.args[1], "flxChannelDetails"), extendConfig({}, controller.args[2], "flxChannelDetails"));
        flxChannelDetails.setDefaultUnit(kony.flex.DP);
        flxChannelDetails.add();
        var lblLine = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblLine",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "l",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLine"), extendConfig({}, controller.args[2], "lblLine"));
        channelTemplate.add(flxChannelHeader, flxChannelDetails, lblLine);
        return channelTemplate;
    }
})