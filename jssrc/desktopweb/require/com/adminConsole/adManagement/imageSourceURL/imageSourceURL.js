define(function() {
    return function(controller) {
        var imageSourceURL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "imageSourceURL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "imageSourceURL"), extendConfig({}, controller.args[1], "imageSourceURL"), extendConfig({}, controller.args[2], "imageSourceURL"));
        imageSourceURL.setDefaultUnit(kony.flex.DP);
        var flxImgSource = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxImgSource",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "49%"
        }, controller.args[0], "flxImgSource"), extendConfig({}, controller.args[1], "flxImgSource"), extendConfig({}, controller.args[2], "flxImgSource"));
        flxImgSource.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var lblImgSource = new kony.ui.Label(extendConfig({
            "id": "lblImgSource",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ImageSourceURL\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImgSource"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgSource"), extendConfig({}, controller.args[2], "lblImgSource"));
        var lblImageIndex = new kony.ui.Label(extendConfig({
            "id": "lblImageIndex",
            "isVisible": true,
            "skin": "sknLblLato485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ImageSourceURL\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImageIndex"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImageIndex"), extendConfig({}, controller.args[2], "lblImageIndex"));
        flxHeader.add(lblImgSource, lblImageIndex);
        var flxImgSourceValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxImgSourceValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxImgSourceValue"), extendConfig({}, controller.args[1], "flxImgSourceValue"), extendConfig({}, controller.args[2], "flxImgSourceValue"));
        flxImgSourceValue.setDefaultUnit(kony.flex.DP);
        var txtImgSource = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "100%",
            "id": "txtImgSource",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "80%",
            "zIndex": 10
        }, controller.args[0], "txtImgSource"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtImgSource"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtImgSource"));
        var btnImgVerify = new kony.ui.Button(extendConfig({
            "height": "100%",
            "id": "btnImgVerify",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnd7d9e0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Verify\")",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "btnImgVerify"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnImgVerify"), extendConfig({
            "hoverSkin": "sknBtnd7d9e0"
        }, controller.args[2], "btnImgVerify"));
        flxImgSourceValue.add(txtImgSource, btnImgVerify);
        var flxInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "13dp",
            "width": "100%"
        }, controller.args[0], "flxInfo"), extendConfig({}, controller.args[1], "flxInfo"), extendConfig({}, controller.args[2], "flxInfo"));
        flxInfo.setDefaultUnit(kony.flex.DP);
        var lblResolution = new kony.ui.Label(extendConfig({
            "id": "lblResolution",
            "isVisible": true,
            "left": "0dp",
            "skin": "skn485C75LatoRegular11Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Resolution\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblResolution"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResolution"), extendConfig({}, controller.args[2], "lblResolution"));
        var lblResolutionValue = new kony.ui.Label(extendConfig({
            "id": "lblResolutionValue",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblLatoRegular00000011px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Resolution\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblResolutionValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResolutionValue"), extendConfig({}, controller.args[2], "lblResolutionValue"));
        flxInfo.add(lblResolution, lblResolutionValue);
        var imageSrcError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "imageSrcError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                },
                "lblErrorText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorImageSourceURL\")"
                }
            }
        }, controller.args[0], "imageSrcError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "imageSrcError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "imageSrcError"));
        flxImgSource.add(flxHeader, flxImgSourceValue, flxInfo, imageSrcError);
        var flxTargetSource = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTargetSource",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "49%"
        }, controller.args[0], "flxTargetSource"), extendConfig({}, controller.args[1], "flxTargetSource"), extendConfig({}, controller.args[2], "flxTargetSource"));
        flxTargetSource.setDefaultUnit(kony.flex.DP);
        var lblTargetSource = new kony.ui.Label(extendConfig({
            "id": "lblTargetSource",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.TargetURL\")",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblTargetSource"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTargetSource"), extendConfig({}, controller.args[2], "lblTargetSource"));
        var flxTargetSourceValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxTargetSourceValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTargetSourceValue"), extendConfig({}, controller.args[1], "flxTargetSourceValue"), extendConfig({}, controller.args[2], "flxTargetSourceValue"));
        flxTargetSourceValue.setDefaultUnit(kony.flex.DP);
        var txtTargetSource = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "100%",
            "id": "txtTargetSource",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "80%",
            "zIndex": 10
        }, controller.args[0], "txtTargetSource"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtTargetSource"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtTargetSource"));
        var btnTargetVerify = new kony.ui.Button(extendConfig({
            "height": "100%",
            "id": "btnTargetVerify",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnd7d9e0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Verify\")",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "btnTargetVerify"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTargetVerify"), extendConfig({
            "hoverSkin": "sknBtnd7d9e0"
        }, controller.args[2], "btnTargetVerify"));
        flxTargetSourceValue.add(txtTargetSource, btnTargetVerify);
        var imgTargetError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "imgTargetError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                },
                "lblErrorText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorTargetURL\")"
                }
            }
        }, controller.args[0], "imgTargetError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "imgTargetError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "imgTargetError"));
        flxTargetSource.add(lblTargetSource, flxTargetSourceValue, imgTargetError);
        imageSourceURL.add(flxImgSource, flxTargetSource);
        return imageSourceURL;
    }
})