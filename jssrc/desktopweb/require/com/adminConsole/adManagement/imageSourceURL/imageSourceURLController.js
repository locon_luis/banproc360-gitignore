define("com/adminConsole/adManagement/imageSourceURL/userimageSourceURLController", function() {
    return {};
});
define("com/adminConsole/adManagement/imageSourceURL/imageSourceURLControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/adManagement/imageSourceURL/imageSourceURLController", ["com/adminConsole/adManagement/imageSourceURL/userimageSourceURLController", "com/adminConsole/adManagement/imageSourceURL/imageSourceURLControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/imageSourceURL/userimageSourceURLController");
    var actions = require("com/adminConsole/adManagement/imageSourceURL/imageSourceURLControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
