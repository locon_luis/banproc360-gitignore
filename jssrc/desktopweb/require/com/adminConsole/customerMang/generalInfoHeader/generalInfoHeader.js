define(function() {
    return function(controller) {
        var generalInfoHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "generalInfoHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "generalInfoHeader"), extendConfig({}, controller.args[1], "generalInfoHeader"), extendConfig({}, controller.args[2], "generalInfoHeader"));
        generalInfoHeader.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxTagsandbuttons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxTagsandbuttons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%"
        }, controller.args[0], "flxTagsandbuttons"), extendConfig({}, controller.args[1], "flxTagsandbuttons"), extendConfig({}, controller.args[2], "flxTagsandbuttons"));
        flxTagsandbuttons.setDefaultUnit(kony.flex.DP);
        var flxCustomerTags = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxCustomerTags",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "70%",
            "zIndex": 10
        }, controller.args[0], "flxCustomerTags"), extendConfig({}, controller.args[1], "flxCustomerTags"), extendConfig({}, controller.args[2], "flxCustomerTags"));
        flxCustomerTags.setDefaultUnit(kony.flex.DP);
        var flxRetailTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxRetailTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagRed",
            "top": "0dp",
            "width": "105px"
        }, controller.args[0], "flxRetailTag"), extendConfig({}, controller.args[1], "flxRetailTag"), extendConfig({}, controller.args[2], "flxRetailTag"));
        flxRetailTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle1 = new kony.ui.Label(extendConfig({
            "centerY": "48%",
            "id": "fontIconCircle1",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconCircle1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCircle1"), extendConfig({}, controller.args[2], "fontIconCircle1"));
        var lblContent1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContent1",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "text": "Retail Customer",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContent1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContent1"), extendConfig({}, controller.args[2], "lblContent1"));
        flxRetailTag.add(fontIconCircle1, lblContent1);
        var flxSmallBusinessTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxSmallBusinessTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagLiteGreen",
            "top": "0dp",
            "width": "125px"
        }, controller.args[0], "flxSmallBusinessTag"), extendConfig({}, controller.args[1], "flxSmallBusinessTag"), extendConfig({}, controller.args[2], "flxSmallBusinessTag"));
        flxSmallBusinessTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle3 = new kony.ui.Label(extendConfig({
            "centerY": "48%",
            "id": "fontIconCircle3",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconCircle3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCircle3"), extendConfig({}, controller.args[2], "fontIconCircle3"));
        var lblContent3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContent3",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "text": "Small Business User",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContent3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContent3"), extendConfig({}, controller.args[2], "lblContent3"));
        flxSmallBusinessTag.add(fontIconCircle3, lblContent3);
        var flxMicroBusinessTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxMicroBusinessTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagPurple",
            "top": "0dp",
            "width": "128px"
        }, controller.args[0], "flxMicroBusinessTag"), extendConfig({}, controller.args[1], "flxMicroBusinessTag"), extendConfig({}, controller.args[2], "flxMicroBusinessTag"));
        flxMicroBusinessTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle4 = new kony.ui.Label(extendConfig({
            "centerY": "48%",
            "id": "fontIconCircle4",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconCircle4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCircle4"), extendConfig({}, controller.args[2], "fontIconCircle4"));
        var lblContent4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContent4",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "text": "Micro Business User",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContent4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContent4"), extendConfig({}, controller.args[2], "lblContent4"));
        flxMicroBusinessTag.add(fontIconCircle4, lblContent4);
        var flxApplicantTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxApplicantTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagBlue",
            "top": "0dp",
            "width": "70px"
        }, controller.args[0], "flxApplicantTag"), extendConfig({}, controller.args[1], "flxApplicantTag"), extendConfig({}, controller.args[2], "flxApplicantTag"));
        flxApplicantTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle2 = new kony.ui.Label(extendConfig({
            "centerY": "48%",
            "id": "fontIconCircle2",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconCircle2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCircle2"), extendConfig({}, controller.args[2], "fontIconCircle2"));
        var lblContent2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContent2",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "text": "Applicant",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContent2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContent2"), extendConfig({}, controller.args[2], "lblContent2"));
        flxApplicantTag.add(fontIconCircle2, lblContent2);
        flxCustomerTags.add(flxRetailTag, flxSmallBusinessTag, flxMicroBusinessTag, flxApplicantTag);
        var flxActionButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxActionButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "reverseLayoutDirection": false,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "width": "30%"
        }, controller.args[0], "flxActionButtons"), extendConfig({}, controller.args[1], "flxActionButtons"), extendConfig({}, controller.args[2], "flxActionButtons"));
        flxActionButtons.setDefaultUnit(kony.flex.DP);
        var flxUnlock = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "22px",
            "id": "flxUnlock",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": 150,
            "skin": "sknflxBorder485c75Radius20PxPointer",
            "width": "80px",
            "zIndex": 1
        }, controller.args[0], "flxUnlock"), extendConfig({}, controller.args[1], "flxUnlock"), extendConfig({
            "hoverSkin": "sknHoverUnlock"
        }, controller.args[2], "flxUnlock"));
        flxUnlock.setDefaultUnit(kony.flex.DP);
        var fonticonUnlock = new kony.ui.Label(extendConfig({
            "centerY": "45%",
            "id": "fonticonUnlock",
            "isVisible": false,
            "left": "9px",
            "skin": "sknfontIconLock",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fonticonCSRAssist\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonUnlock"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonUnlock"), extendConfig({}, controller.args[2], "fonticonUnlock"));
        var lblUnlock = new kony.ui.Label(extendConfig({
            "centerY": "45%",
            "id": "lblUnlock",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Suspend\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblUnlock"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUnlock"), extendConfig({}, controller.args[2], "lblUnlock"));
        flxUnlock.add(fonticonUnlock, lblUnlock);
        var flxCSRAssist = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "22px",
            "id": "flxCSRAssist",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "65px",
            "skin": "sknflxCSRAssist",
            "width": "140px",
            "zIndex": 1
        }, controller.args[0], "flxCSRAssist"), extendConfig({}, controller.args[1], "flxCSRAssist"), extendConfig({
            "hoverSkin": "sknflxCSRAssistBlueHover"
        }, controller.args[2], "flxCSRAssist"));
        flxCSRAssist.setDefaultUnit(kony.flex.DP);
        var fonticonCSRAssist = new kony.ui.Label(extendConfig({
            "centerY": "45%",
            "height": "15px",
            "id": "fonticonCSRAssist",
            "isVisible": true,
            "left": "10px",
            "skin": "sknIcon13pxWhite",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconPrevious\")",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "fonticonCSRAssist"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonCSRAssist"), extendConfig({}, controller.args[2], "fonticonCSRAssist"));
        var lblCSRAssist = new kony.ui.Label(extendConfig({
            "centerY": "45%",
            "id": "lblCSRAssist",
            "isVisible": true,
            "skin": "sknlblCSRAssist",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCSRAssist\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCSRAssist"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCSRAssist"), extendConfig({}, controller.args[2], "lblCSRAssist"));
        flxCSRAssist.add(fonticonCSRAssist, lblCSRAssist);
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30px",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "12px",
            "skin": "sknFlxBgFFFFFFRounded",
            "width": "30px",
            "zIndex": 3
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        }, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconAlertTypeOptions = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconAlertTypeOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconImgOptions\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconAlertTypeOptions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconAlertTypeOptions"), extendConfig({}, controller.args[2], "lblIconAlertTypeOptions"));
        flxOptions.add(lblIconAlertTypeOptions);
        flxActionButtons.add(flxUnlock, flxCSRAssist, flxOptions);
        flxTagsandbuttons.add(flxCustomerTags, flxActionButtons);
        var flxDefaultSearchHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDefaultSearchHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "70%",
            "zIndex": 1
        }, controller.args[0], "flxDefaultSearchHeader"), extendConfig({}, controller.args[1], "flxDefaultSearchHeader"), extendConfig({}, controller.args[2], "flxDefaultSearchHeader"));
        flxDefaultSearchHeader.setDefaultUnit(kony.flex.DP);
        var lblCustomerName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCustomerName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknCustomerProfileName",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCustomerName3\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustomerName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustomerName"), extendConfig({}, controller.args[2], "lblCustomerName"));
        var fonticonActive = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonActive",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fonticonActive"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonActive"), extendConfig({}, controller.args[2], "fonticonActive"));
        var lblStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus"), extendConfig({}, controller.args[2], "lblStatus"));
        flxDefaultSearchHeader.add(lblCustomerName, fonticonActive, lblStatus);
        var flxRiskStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxRiskStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "70%",
            "zIndex": 10
        }, controller.args[0], "flxRiskStatus"), extendConfig({}, controller.args[1], "flxRiskStatus"), extendConfig({}, controller.args[2], "flxRiskStatus"));
        flxRiskStatus.setDefaultUnit(kony.flex.DP);
        var fonticonRisk = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonRisk",
            "isVisible": true,
            "left": "0px",
            "skin": "fonticonhighrisk",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fonticonRisk\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonRisk"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonRisk"), extendConfig({}, controller.args[2], "fonticonRisk"));
        var lblRisk = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRisk",
            "isVisible": true,
            "left": "5dp",
            "skin": "slLabelf6b3133bfe92394b",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.High_Risk\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRisk"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRisk"), extendConfig({}, controller.args[2], "lblRisk"));
        var fonticonFraud = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonFraud",
            "isVisible": true,
            "left": "25px",
            "skin": "fonticonfraud",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fonticonFraud\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonFraud"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonFraud"), extendConfig({}, controller.args[2], "fonticonFraud"));
        var lblFraud = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFraud",
            "isVisible": true,
            "left": "5px",
            "skin": "sknLabelEE6565a2f3bb4d",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Fraud_Detected\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFraud"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFraud"), extendConfig({}, controller.args[2], "lblFraud"));
        var fonticonDefaulter = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonDefaulter",
            "isVisible": true,
            "left": "25px",
            "skin": "sknfontIcoDefaulter",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fonticonDefaulter\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonDefaulter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonDefaulter"), extendConfig({}, controller.args[2], "fonticonDefaulter"));
        var lblDefaulter = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDefaulter",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlLabelfc6c21abe24974f",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Defaulter\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDefaulter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDefaulter"), extendConfig({}, controller.args[2], "lblDefaulter"));
        flxRiskStatus.add(fonticonRisk, lblRisk, fonticonFraud, lblFraud, fonticonDefaulter, lblDefaulter);
        var lblSeparator = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": 0,
            "right": 0,
            "skin": "sknConfigSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator"), extendConfig({}, controller.args[2], "lblSeparator"));
        flxHeader.add(flxTagsandbuttons, flxDefaultSearchHeader, flxRiskStatus, lblSeparator);
        generalInfoHeader.add(flxHeader);
        return generalInfoHeader;
    }
})