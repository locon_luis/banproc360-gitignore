define("com/adminConsole/customerMang/selectOptions/userselectOptionsController", function() {
    return {};
});
define("com/adminConsole/customerMang/selectOptions/selectOptionsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/customerMang/selectOptions/selectOptionsController", ["com/adminConsole/customerMang/selectOptions/userselectOptionsController", "com/adminConsole/customerMang/selectOptions/selectOptionsControllerActions"], function() {
    var controller = require("com/adminConsole/customerMang/selectOptions/userselectOptionsController");
    var actions = require("com/adminConsole/customerMang/selectOptions/selectOptionsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
