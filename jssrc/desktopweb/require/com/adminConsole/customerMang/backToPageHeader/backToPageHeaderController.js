define("com/adminConsole/customerMang/backToPageHeader/userbackToPageHeaderController", function() {
    return {};
});
define("com/adminConsole/customerMang/backToPageHeader/backToPageHeaderControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/customerMang/backToPageHeader/backToPageHeaderController", ["com/adminConsole/customerMang/backToPageHeader/userbackToPageHeaderController", "com/adminConsole/customerMang/backToPageHeader/backToPageHeaderControllerActions"], function() {
    var controller = require("com/adminConsole/customerMang/backToPageHeader/userbackToPageHeaderController");
    var actions = require("com/adminConsole/customerMang/backToPageHeader/backToPageHeaderControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
