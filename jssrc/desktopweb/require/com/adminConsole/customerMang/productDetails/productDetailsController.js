define("com/adminConsole/customerMang/productDetails/userproductDetailsController", function() {
    return {};
});
define("com/adminConsole/customerMang/productDetails/productDetailsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/customerMang/productDetails/productDetailsController", ["com/adminConsole/customerMang/productDetails/userproductDetailsController", "com/adminConsole/customerMang/productDetails/productDetailsControllerActions"], function() {
    var controller = require("com/adminConsole/customerMang/productDetails/userproductDetailsController");
    var actions = require("com/adminConsole/customerMang/productDetails/productDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
