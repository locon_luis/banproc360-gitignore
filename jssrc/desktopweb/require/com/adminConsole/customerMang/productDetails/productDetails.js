define(function() {
    return function(controller) {
        var productDetails = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "productDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "productDetails"), extendConfig({}, controller.args[1], "productDetails"), extendConfig({}, controller.args[2], "productDetails"));
        productDetails.setDefaultUnit(kony.flex.DP);
        var flxColumn1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "70px",
            "id": "flxColumn1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxColumn1"), extendConfig({}, controller.args[1], "flxColumn1"), extendConfig({}, controller.args[2], "flxColumn1"));
        flxColumn1.setDefaultUnit(kony.flex.DP);
        var lblHeading1 = new kony.ui.Label(extendConfig({
            "id": "lblHeading1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading1"), extendConfig({}, controller.args[2], "lblHeading1"));
        var flxDataRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataRow1"), extendConfig({}, controller.args[1], "flxDataRow1"), extendConfig({}, controller.args[2], "flxDataRow1"));
        flxDataRow1.setDefaultUnit(kony.flex.DP);
        var lblData11 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData11",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData11"), extendConfig({}, controller.args[2], "lblData11"));
        var btnData11 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnData11",
            "isVisible": true,
            "left": 0,
            "skin": "sknBtnLato11ABEB14Px",
            "text": "John",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnData11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnData11"), extendConfig({}, controller.args[2], "btnData11"));
        flxDataRow1.add(lblData11, btnData11);
        var flxDataRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 5,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataRow2"), extendConfig({}, controller.args[1], "flxDataRow2"), extendConfig({}, controller.args[2], "flxDataRow2"));
        flxDataRow2.setDefaultUnit(kony.flex.DP);
        var lblData21 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData21",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData21"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData21"), extendConfig({}, controller.args[2], "lblData21"));
        var btnData21 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnData21",
            "isVisible": true,
            "left": 0,
            "skin": "sknBtnLato11ABEB14Px",
            "text": "John",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnData21"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnData21"), extendConfig({}, controller.args[2], "btnData21"));
        var lblComma1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblComma1",
            "isVisible": true,
            "left": "3px",
            "skin": "sknlblLatoBold35475f14px",
            "text": ",",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblComma1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblComma1"), extendConfig({}, controller.args[2], "lblComma1"));
        var btnData22 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnData22",
            "isVisible": true,
            "left": 5,
            "skin": "sknBtnLato11ABEB14Px",
            "text": "John",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnData22"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnData22"), extendConfig({}, controller.args[2], "btnData22"));
        var lblComma2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblComma2",
            "isVisible": true,
            "left": "3px",
            "skin": "sknlblLatoBold35475f14px",
            "text": ",",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblComma2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblComma2"), extendConfig({}, controller.args[2], "lblComma2"));
        var btnData23 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnData23",
            "isVisible": true,
            "left": 5,
            "skin": "sknBtnLato11ABEB14Px",
            "text": "John",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnData23"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnData23"), extendConfig({}, controller.args[2], "btnData23"));
        flxDataRow2.add(lblData21, btnData21, lblComma1, btnData22, lblComma2, btnData23);
        flxColumn1.add(lblHeading1, flxDataRow1, flxDataRow2);
        var flxColumn2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxColumn2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxColumn2"), extendConfig({}, controller.args[1], "flxColumn2"), extendConfig({}, controller.args[2], "flxColumn2"));
        flxColumn2.setDefaultUnit(kony.flex.DP);
        var lblHeading2 = new kony.ui.Label(extendConfig({
            "id": "lblHeading2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading2"), extendConfig({}, controller.args[2], "lblHeading2"));
        var flxDataAndImage2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataAndImage2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataAndImage2"), extendConfig({}, controller.args[1], "flxDataAndImage2"), extendConfig({}, controller.args[2], "flxDataAndImage2"));
        flxDataAndImage2.setDefaultUnit(kony.flex.DP);
        var imgData2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12px",
            "id": "imgData2",
            "isVisible": false,
            "left": "0dp",
            "right": "5px",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "imgData2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgData2"), extendConfig({}, controller.args[2], "imgData2"));
        var lblData2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData2"), extendConfig({}, controller.args[2], "lblData2"));
        flxDataAndImage2.add(imgData2, lblData2);
        flxColumn2.add(lblHeading2, flxDataAndImage2);
        var flxColumn3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxColumn3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "72%",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxColumn3"), extendConfig({}, controller.args[1], "flxColumn3"), extendConfig({}, controller.args[2], "flxColumn3"));
        flxColumn3.setDefaultUnit(kony.flex.DP);
        var lblHeading3 = new kony.ui.Label(extendConfig({
            "id": "lblHeading3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading3"), extendConfig({}, controller.args[2], "lblHeading3"));
        var flxDataAndImage3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataAndImage3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataAndImage3"), extendConfig({}, controller.args[1], "flxDataAndImage3"), extendConfig({}, controller.args[2], "flxDataAndImage3"));
        flxDataAndImage3.setDefaultUnit(kony.flex.DP);
        var imgData3 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12px",
            "id": "imgData3",
            "isVisible": false,
            "left": "0dp",
            "right": "5px",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "imgData3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgData3"), extendConfig({}, controller.args[2], "imgData3"));
        var lblData3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData3",
            "isVisible": true,
            "left": "0px",
            "right": "7px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData3"), extendConfig({}, controller.args[2], "lblData3"));
        var btnLink3 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnLink3",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknBtnLato11ABEB11Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Enroll\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnLink3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnLink3"), extendConfig({}, controller.args[2], "btnLink3"));
        flxDataAndImage3.add(imgData3, lblData3, btnLink3);
        flxColumn3.add(lblHeading3, flxDataAndImage3);
        productDetails.add(flxColumn1, flxColumn2, flxColumn3);
        return productDetails;
    }
})