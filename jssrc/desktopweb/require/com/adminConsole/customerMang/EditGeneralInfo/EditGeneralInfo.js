define(function() {
    return function(controller) {
        var EditGeneralInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "EditGeneralInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_ba7818631ed740b9b8f871667b666d56(eventobject);
            },
            "right": "0px",
            "skin": "slFbox0hfd18814fd664dCM"
        }, controller.args[0], "EditGeneralInfo"), extendConfig({}, controller.args[1], "EditGeneralInfo"), extendConfig({}, controller.args[2], "EditGeneralInfo"));
        EditGeneralInfo.setDefaultUnit(kony.flex.DP);
        var flxGeneralDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGeneralDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 2
        }, controller.args[0], "flxGeneralDetails"), extendConfig({}, controller.args[1], "flxGeneralDetails"), extendConfig({}, controller.args[2], "flxGeneralDetails"));
        flxGeneralDetails.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxDetails1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "13%",
            "zIndex": 2
        }, controller.args[0], "flxDetails1"), extendConfig({}, controller.args[1], "flxDetails1"), extendConfig({}, controller.args[2], "flxDetails1"));
        flxDetails1.setDefaultUnit(kony.flex.DP);
        var lblContactDetails1 = new kony.ui.Label(extendConfig({
            "id": "lblContactDetails1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblContactDetails1\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactDetails1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactDetails1"), extendConfig({}, controller.args[2], "lblContactDetails1"));
        var lstboxDetails1 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxDetails1",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["k1", "Select Salutation"],
                ["k2", "Mrs."],
                ["k3", "Dr."],
                ["k4", "Ms."]
            ],
            "selectedKey": "k1",
            "selectedKeyValue": ["k1", "Select Salutation"],
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "30dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxDetails1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxDetails1"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstboxDetails1"));
        var lblError1 = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblError1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.selectSalutaion\")",
            "top": 75,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblError1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblError1"), extendConfig({}, controller.args[2], "lblError1"));
        flxDetails1.add(lblContactDetails1, lstboxDetails1, lblError1);
        var flxDetails2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "27%",
            "zIndex": 2
        }, controller.args[0], "flxDetails2"), extendConfig({}, controller.args[1], "flxDetails2"), extendConfig({}, controller.args[2], "flxDetails2"));
        flxDetails2.setDefaultUnit(kony.flex.DP);
        var lblDetails2 = new kony.ui.Label(extendConfig({
            "id": "lblDetails2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCol13\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetails2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetails2"), extendConfig({}, controller.args[2], "lblDetails2"));
        var lstboxDetails2 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxDetails2",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["key1", "Active"],
                ["key2", "Suspended"]
            ],
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "30dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxDetails2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxDetails2"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstboxDetails2"));
        var lblError2 = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblError2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SelectAStatus\")",
            "top": 75,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblError2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblError2"), extendConfig({}, controller.args[2], "lblError2"));
        flxDetails2.add(lblDetails2, lstboxDetails2, lblError2);
        var flxDetails3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "27%",
            "zIndex": 2
        }, controller.args[0], "flxDetails3"), extendConfig({}, controller.args[1], "flxDetails3"), extendConfig({}, controller.args[2], "flxDetails3"));
        flxDetails3.setDefaultUnit(kony.flex.DP);
        var lblDetails3 = new kony.ui.Label(extendConfig({
            "id": "lblDetails3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails3\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetails3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetails3"), extendConfig({}, controller.args[2], "lblDetails3"));
        var lstboxDetails3 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxDetails3",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["k1", "Married"],
                ["k2", "Unmarried"],
                ["k3", "Divorced"],
                ["k4", "Widowed"],
                ["k5", "Unknown"]
            ],
            "selectedKey": "k1",
            "selectedKeyValue": ["k1", "Married"],
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "30dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxDetails3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxDetails3"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstboxDetails3"));
        var lblError3 = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblError3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SelectAStatus\")",
            "top": 75,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblError3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblError3"), extendConfig({}, controller.args[2], "lblError3"));
        flxDetails3.add(lblDetails3, lstboxDetails3, lblError3);
        var flxDetails4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "27%",
            "zIndex": 2
        }, controller.args[0], "flxDetails4"), extendConfig({}, controller.args[1], "flxDetails4"), extendConfig({}, controller.args[2], "flxDetails4"));
        flxDetails4.setDefaultUnit(kony.flex.DP);
        var lblContactDetails4 = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblContactDetails4",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblContactDetails4\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactDetails4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactDetails4"), extendConfig({}, controller.args[2], "lblContactDetails4"));
        var lstboxDetails4 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxDetails4",
            "isVisible": true,
            "masterData": [
                ["k1", "Employed"],
                ["k2", "Unemployed"],
                ["k3", "Retired"],
                ["k4", "Student"]
            ],
            "selectedKey": "k1",
            "selectedKeyValue": ["k1", "Employed"],
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "30dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxDetails4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxDetails4"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstboxDetails4"));
        var lblError4 = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblError4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SelectAStatus\")",
            "top": 75,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblError4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblError4"), extendConfig({}, controller.args[2], "lblError4"));
        flxDetails4.add(lblContactDetails4, lstboxDetails4, lblError4);
        flxRow1.add(flxDetails1, flxDetails2, flxDetails3, flxDetails4);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65px",
            "id": "flxRow2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "25dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxDetails5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "34%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxDetails5"), extendConfig({}, controller.args[1], "flxDetails5"), extendConfig({}, controller.args[2], "flxDetails5"));
        flxDetails5.setDefaultUnit(kony.flex.DP);
        var lblContactDetails5 = new kony.ui.Label(extendConfig({
            "id": "lblContactDetails5",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblContactDetails5\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactDetails5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactDetails5"), extendConfig({}, controller.args[2], "lblContactDetails5"));
        var lstboxDetails5 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxDetails5",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["k1", "Employed"],
                ["k2", "Unemployed"],
                ["k3", "Retired"],
                ["k4", "Student"]
            ],
            "selectedKey": "k1",
            "selectedKeyValue": ["k1", "Employed"],
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "25dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxDetails5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxDetails5"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstboxDetails5"));
        flxDetails5.add(lblContactDetails5, lstboxDetails5);
        flxRow2.add(flxDetails5);
        var flxRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxRow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "105dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow3"), extendConfig({}, controller.args[1], "flxRow3"), extendConfig({}, controller.args[2], "flxRow3"));
        flxRow3.setDefaultUnit(kony.flex.DP);
        var flxDetails8 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails8",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "42%"
        }, controller.args[0], "flxDetails8"), extendConfig({}, controller.args[1], "flxDetails8"), extendConfig({}, controller.args[2], "flxDetails8"));
        flxDetails8.setDefaultUnit(kony.flex.DP);
        var lblRisksHeader = new kony.ui.Label(extendConfig({
            "id": "lblRisksHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails8\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRisksHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRisksHeader"), extendConfig({}, controller.args[2], "lblRisksHeader"));
        var flxSubDetails8 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36px",
            "id": "flxSubDetails8",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxOuterBorder",
            "top": "10px",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxSubDetails8"), extendConfig({}, controller.args[1], "flxSubDetails8"), extendConfig({}, controller.args[2], "flxSubDetails8"));
        flxSubDetails8.setDefaultUnit(kony.flex.DP);
        var flxRiskStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36dp",
            "id": "flxRiskStatus",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxfdf6f1bg100px",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 111
        }, controller.args[0], "flxRiskStatus"), extendConfig({}, controller.args[1], "flxRiskStatus"), extendConfig({}, controller.args[2], "flxRiskStatus"));
        flxRiskStatus.setDefaultUnit(kony.flex.DP);
        var lblDetails8 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblDetails8",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails8\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetails8"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetails8"), extendConfig({}, controller.args[2], "lblDetails8"));
        flxRiskStatus.add(lblDetails8);
        var flxFlagSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36dp",
            "id": "flxFlagSeperator",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxOuterBorder",
            "top": "0dp",
            "width": "1px",
            "zIndex": 111
        }, controller.args[0], "flxFlagSeperator"), extendConfig({}, controller.args[1], "flxFlagSeperator"), extendConfig({}, controller.args[2], "flxFlagSeperator"));
        flxFlagSeperator.setDefaultUnit(kony.flex.DP);
        flxFlagSeperator.add();
        var flxSelectFlags = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "36px",
            "id": "flxSelectFlags",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 111
        }, controller.args[0], "flxSelectFlags"), extendConfig({}, controller.args[1], "flxSelectFlags"), extendConfig({}, controller.args[2], "flxSelectFlags"));
        flxSelectFlags.setDefaultUnit(kony.flex.DP);
        var flxFlag3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "13dp",
            "id": "flxFlag3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "flxFlag3"), extendConfig({}, controller.args[1], "flxFlag3"), extendConfig({}, controller.args[2], "flxFlag3"));
        flxFlag3.setDefaultUnit(kony.flex.DP);
        var imgFlag3 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgFlag3",
            "isVisible": true,
            "left": "15dp",
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgFlag3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFlag3"), extendConfig({
            "toolTip": "Delete"
        }, controller.args[2], "imgFlag3"));
        flxFlag3.add(imgFlag3);
        var lblFlag3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFlag3",
            "isVisible": true,
            "left": "6px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFlag3\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlag3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlag3"), extendConfig({}, controller.args[2], "lblFlag3"));
        var flxFlag2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "13dp",
            "id": "flxFlag2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "flxFlag2"), extendConfig({}, controller.args[1], "flxFlag2"), extendConfig({}, controller.args[2], "flxFlag2"));
        flxFlag2.setDefaultUnit(kony.flex.DP);
        var imgFlag2 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgFlag2",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgFlag2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFlag2"), extendConfig({
            "toolTip": "Deactive"
        }, controller.args[2], "imgFlag2"));
        flxFlag2.add(imgFlag2);
        var lblFlag2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFlag2",
            "isVisible": true,
            "left": "6px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Defaulter\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlag2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlag2"), extendConfig({}, controller.args[2], "lblFlag2"));
        var flxFlag1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "13dp",
            "id": "flxFlag1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": 15,
            "width": "13px",
            "zIndex": 2
        }, controller.args[0], "flxFlag1"), extendConfig({}, controller.args[1], "flxFlag1"), extendConfig({}, controller.args[2], "flxFlag1"));
        flxFlag1.setDefaultUnit(kony.flex.DP);
        var imgFlag1 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgFlag1",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgFlag1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFlag1"), extendConfig({
            "toolTip": "Active"
        }, controller.args[2], "imgFlag1"));
        flxFlag1.add(imgFlag1);
        var lblFlag1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFlag1",
            "isVisible": true,
            "left": "6px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.High_Risk\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlag1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlag1"), extendConfig({}, controller.args[2], "lblFlag1"));
        flxSelectFlags.add(flxFlag3, lblFlag3, flxFlag2, lblFlag2, flxFlag1, lblFlag1);
        flxSubDetails8.add(flxRiskStatus, flxFlagSeperator, flxSelectFlags);
        flxDetails8.add(lblRisksHeader, flxSubDetails8);
        var flxDetails7 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails7",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "44%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "44%",
            "zIndex": 2
        }, controller.args[0], "flxDetails7"), extendConfig({}, controller.args[1], "flxDetails7"), extendConfig({}, controller.args[2], "flxDetails7"));
        flxDetails7.setDefaultUnit(kony.flex.DP);
        var lblContactDetails7 = new kony.ui.Label(extendConfig({
            "id": "lblContactDetails7",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblContactDetails7\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactDetails7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactDetails7"), extendConfig({}, controller.args[2], "lblContactDetails7"));
        var switchToggle = new kony.ui.Switch(extendConfig({
            "height": "25dp",
            "id": "switchToggle",
            "isVisible": true,
            "left": "0dp",
            "leftSideText": "ON",
            "rightSideText": "OFF",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "top": "17px",
            "width": "38dp",
            "zIndex": 1
        }, controller.args[0], "switchToggle"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "switchToggle"), extendConfig({}, controller.args[2], "switchToggle"));
        flxDetails7.add(lblContactDetails7, switchToggle);
        flxRow3.add(flxDetails8, flxDetails7);
        flxGeneralDetails.add(flxRow1, flxRow2, flxRow3);
        EditGeneralInfo.add(flxGeneralDetails);
        return EditGeneralInfo;
    }
})