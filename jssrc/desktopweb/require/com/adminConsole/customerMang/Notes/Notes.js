define(function() {
    return function(controller) {
        var Notes = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "Notes",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_a5cd41c52f6d4ff6aafa0ddbcd72933d(eventobject);
            },
            "skin": "slFbox0d4d94de464db42CM",
            "width": "100%"
        }, controller.args[0], "Notes"), extendConfig({}, controller.args[1], "Notes"), extendConfig({}, controller.args[2], "Notes"));
        Notes.setDefaultUnit(kony.flex.DP);
        var flxNotesHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxNotesHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0d0037667d34648CM",
            "top": "10px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNotesHeader"), extendConfig({}, controller.args[1], "flxNotesHeader"), extendConfig({}, controller.args[2], "flxNotesHeader"));
        flxNotesHeader.setDefaultUnit(kony.flex.DP);
        var lblNotes = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblNotes",
            "isVisible": true,
            "left": "15px",
            "right": "90px",
            "skin": "lblfffffflatoregularCM",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblNotes\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNotes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNotes"), extendConfig({}, controller.args[2], "lblNotes"));
        var flxCloseNotes = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30px",
            "id": "flxCloseNotes",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "sknCursor",
            "width": "30px",
            "zIndex": 1
        }, controller.args[0], "flxCloseNotes"), extendConfig({}, controller.args[1], "flxCloseNotes"), extendConfig({}, controller.args[2], "flxCloseNotes"));
        flxCloseNotes.setDefaultUnit(kony.flex.DP);
        var imgCloseNotes = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgCloseNotes",
            "isVisible": true,
            "skin": "slImage",
            "src": "close_blue.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgCloseNotes"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCloseNotes"), extendConfig({}, controller.args[2], "imgCloseNotes"));
        flxCloseNotes.add(imgCloseNotes);
        flxNotesHeader.add(lblNotes, flxCloseNotes);
        var flxNotesSegment = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": 140,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxNotesSegment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "45dp",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNotesSegment"), extendConfig({}, controller.args[1], "flxNotesSegment"), extendConfig({}, controller.args[2], "flxNotesSegment"));
        flxNotesSegment.setDefaultUnit(kony.flex.DP);
        var segNotes = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [
                [{
                        "fonticonArrow": "",
                        "imgArrow": "img_down_arrow.png",
                        "lblDate": "October 21, 2017"
                    },
                    [{
                        "imgUser": "option3.png",
                        "lblTime": " 6:46 am",
                        "lblUserName": "John Doe",
                        "rtxNotesDescription": "\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quis dignissim quam. Curabitur enim eros, aliquet sit amet maximus et, porttitor et ligula."
                    }, {
                        "imgUser": "option3.png",
                        "lblTime": " 6:46 am",
                        "lblUserName": "John Doe",
                        "rtxNotesDescription": "\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quis dignissim quam. Curabitur enim eros, aliquet sit amet maximus et, porttitor et ligula."
                    }]
                ]
            ],
            "groupCells": false,
            "id": "segNotes",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": true,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxCustomerMangNotes",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "sectionHeaderTemplate": "segNotes",
            "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": true,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxArrow": "flxArrow",
                "flxCustMangContent": "flxCustMangContent",
                "flxCustomerMangNotes": "flxCustomerMangNotes",
                "flxDetails": "flxDetails",
                "flxNotesHeader": "flxNotesHeader",
                "flxNotesUserProfile": "flxNotesUserProfile",
                "flxUserPic": "flxUserPic",
                "fonticonArrow": "fonticonArrow",
                "imgArrow": "imgArrow",
                "imgUser": "imgUser",
                "lblDate": "lblDate",
                "lblTime": "lblTime",
                "lblUserName": "lblUserName",
                "rtxNotesDescription": "rtxNotesDescription",
                "segNotes": "segNotes"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segNotes"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segNotes"), extendConfig({}, controller.args[2], "segNotes"));
        var rtxMsgNotes = new kony.ui.RichText(extendConfig({
            "bottom": "200px",
            "centerX": "50%",
            "id": "rtxMsgNotes",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknRtxLato84939e12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxMsgNotes\")",
            "top": "200dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxMsgNotes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxMsgNotes"), extendConfig({}, controller.args[2], "rtxMsgNotes"));
        flxNotesSegment.add(segNotes, rtxMsgNotes);
        var flxNotes = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10px",
            "clipBounds": true,
            "height": "130px",
            "id": "flxNotes",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "10px",
            "skin": "slFbox",
            "zIndex": 1
        }, controller.args[0], "flxNotes"), extendConfig({}, controller.args[1], "flxNotes"), extendConfig({}, controller.args[2], "flxNotes"));
        flxNotes.setDefaultUnit(kony.flex.DP);
        var lblNotesSize = new kony.ui.Label(extendConfig({
            "id": "lblNotesSize",
            "isVisible": true,
            "right": "0px",
            "skin": "sknllbl485c75Lato13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblNotesSize\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 5
        }, controller.args[0], "lblNotesSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNotesSize"), extendConfig({}, controller.args[2], "lblNotesSize"));
        var txtAreaNotes = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "height": "80px",
            "id": "txtAreaNotes",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 1000,
            "numberOfVisibleLines": 3,
            "placeholder": "Add Note",
            "skin": "skntxtAreaLato35475f14Px",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAreaNotes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "txtAreaNotes"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtAreaNotes"));
        var addBtn = new kony.ui.Button(extendConfig({
            "bottom": "2px",
            "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
            "height": "30px",
            "id": "addBtn",
            "isVisible": true,
            "right": "5px",
            "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
            "top": "100px",
            "width": "70px",
            "zIndex": 1
        }, controller.args[0], "addBtn"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "addBtn"), extendConfig({
            "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
        }, controller.args[2], "addBtn"));
        flxNotes.add(lblNotesSize, txtAreaNotes, addBtn);
        Notes.add(flxNotesHeader, flxNotesSegment, flxNotes);
        return Notes;
    }
})