define("com/adminConsole/customerMang/EditAddress/userEditAddressController", function() {
    return {};
});
define("com/adminConsole/customerMang/EditAddress/EditAddressControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/customerMang/EditAddress/EditAddressController", ["com/adminConsole/customerMang/EditAddress/userEditAddressController", "com/adminConsole/customerMang/EditAddress/EditAddressControllerActions"], function() {
    var controller = require("com/adminConsole/customerMang/EditAddress/userEditAddressController");
    var actions = require("com/adminConsole/customerMang/EditAddress/EditAddressControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
