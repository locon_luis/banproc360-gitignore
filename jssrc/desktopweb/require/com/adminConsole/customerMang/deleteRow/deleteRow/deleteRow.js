define(function() {
    return function(controller) {
        var deleteRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "deleteRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_ad2f60a295af4dbab21e537eab83ce26(eventobject);
            },
            "right": "35px",
            "skin": "slFbox",
            "top": "0dp"
        }, controller.args[0], "deleteRow"), extendConfig({}, controller.args[1], "deleteRow"), extendConfig({}, controller.args[2], "deleteRow"));
        deleteRow.setDefaultUnit(kony.flex.DP);
        var flxHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxHeading",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeading"), extendConfig({}, controller.args[1], "flxHeading"), extendConfig({}, controller.args[2], "flxHeading"));
        flxHeading.setDefaultUnit(kony.flex.DP);
        var lblLimits = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblLimits",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSubHeader0b73b6af9628b4e",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblLimits\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLimits"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLimits"), extendConfig({}, controller.args[2], "lblLimits"));
        var flxViewEditButton = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxViewEditButton",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxffffffop100Border424242Radius100px",
            "width": "60px",
            "zIndex": 1
        }, controller.args[0], "flxViewEditButton"), extendConfig({}, controller.args[1], "flxViewEditButton"), extendConfig({}, controller.args[2], "flxViewEditButton"));
        flxViewEditButton.setDefaultUnit(kony.flex.DP);
        var lblViewEditButton = new kony.ui.Label(extendConfig({
            "centerX": "48%",
            "centerY": "47%",
            "id": "lblViewEditButton",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewEditButton"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewEditButton"), extendConfig({}, controller.args[2], "lblViewEditButton"));
        flxViewEditButton.add(lblViewEditButton);
        var btnAdd = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px",
            "height": "25dp",
            "id": "btnAdd",
            "isVisible": true,
            "right": "0px",
            "skin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
            "width": "60dp",
            "zIndex": 1
        }, controller.args[0], "btnAdd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAdd"), extendConfig({
            "hoverSkin": "sknBtnLatoRegulara5abc413pxKA"
        }, controller.args[2], "btnAdd"));
        flxHeading.add(lblLimits, flxViewEditButton, btnAdd);
        var flxtableHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxtableHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxtableHeader"), extendConfig({}, controller.args[1], "flxtableHeader"), extendConfig({}, controller.args[2], "flxtableHeader"));
        flxtableHeader.setDefaultUnit(kony.flex.DP);
        var lblMinAmount = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblMinAmount",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblMinimumAmount\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMinAmount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMinAmount"), extendConfig({}, controller.args[2], "lblMinAmount"));
        var lblMaxAmount = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblMaxAmount",
            "isVisible": true,
            "left": "34%",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblMaximumAmount\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMaxAmount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaxAmount"), extendConfig({}, controller.args[2], "lblMaxAmount"));
        var lblFee = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFee",
            "isVisible": true,
            "left": "68%",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblFee\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFee"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFee"), extendConfig({}, controller.args[2], "lblFee"));
        var lblSeparator = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "zIndex": 1
        }, controller.args[0], "lblSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator"), extendConfig({}, controller.args[2], "lblSeparator"));
        flxtableHeader.add(lblMinAmount, lblMaxAmount, lblFee, lblSeparator);
        var flxTable = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "10dp",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxTable",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "110dp",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTable"), extendConfig({}, controller.args[1], "flxTable"), extendConfig({}, controller.args[2], "flxTable"));
        flxTable.setDefaultUnit(kony.flex.DP);
        var segFeeRange = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "groupCells": false,
            "id": "segFeeRange",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {},
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segFeeRange"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segFeeRange"), extendConfig({}, controller.args[2], "segFeeRange"));
        flxTable.add(segFeeRange);
        deleteRow.add(flxHeading, flxtableHeader, flxTable);
        return deleteRow;
    }
})