define(function() {
    return function(controller) {
        var Limits = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "Limits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_feef057e6b2249b7b888dacf8397096e(eventobject);
            },
            "right": "35dp",
            "skin": "slFbox",
            "top": "0dp"
        }, controller.args[0], "Limits"), extendConfig({}, controller.args[1], "Limits"), extendConfig({}, controller.args[2], "Limits"));
        Limits.setDefaultUnit(kony.flex.DP);
        var flxHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxHeading",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeading"), extendConfig({}, controller.args[1], "flxHeading"), extendConfig({}, controller.args[2], "flxHeading"));
        flxHeading.setDefaultUnit(kony.flex.DP);
        var lblLimits = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblLimits",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSubHeader0b73b6af9628b4e",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblLimits\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLimits"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLimits"), extendConfig({}, controller.args[2], "lblLimits"));
        var btnAdd = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px",
            "height": "25dp",
            "id": "btnAdd",
            "isVisible": true,
            "right": "0px",
            "skin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
            "width": "60dp",
            "zIndex": 1
        }, controller.args[0], "btnAdd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAdd"), extendConfig({
            "hoverSkin": "sknBtnLatoRegulara5abc413pxKA"
        }, controller.args[2], "btnAdd"));
        flxHeading.add(lblLimits, btnAdd);
        var flxtableHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxtableHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxtableHeader"), extendConfig({}, controller.args[1], "flxtableHeader"), extendConfig({}, controller.args[2], "flxtableHeader"));
        flxtableHeader.setDefaultUnit(kony.flex.DP);
        var lblPeriod = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPeriod",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDefineLimitsPeriod\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPeriod"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPeriod"), extendConfig({}, controller.args[2], "lblPeriod"));
        var lblLimit = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblLimit",
            "isVisible": true,
            "left": "52%",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDefineLimitsLimit\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLimit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLimit"), extendConfig({}, controller.args[2], "lblLimit"));
        var lblSeparator = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "zIndex": 1
        }, controller.args[0], "lblSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator"), extendConfig({}, controller.args[2], "lblSeparator"));
        flxtableHeader.add(lblPeriod, lblLimit, lblSeparator);
        var flxTable = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTable",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "110dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTable"), extendConfig({}, controller.args[1], "flxTable"), extendConfig({}, controller.args[2], "flxTable"));
        flxTable.setDefaultUnit(kony.flex.DP);
        var flxLimit1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "flxLimit1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 0,
            "width": "100%"
        }, controller.args[0], "flxLimit1"), extendConfig({}, controller.args[1], "flxLimit1"), extendConfig({}, controller.args[2], "flxLimit1"));
        flxLimit1.setDefaultUnit(kony.flex.DP);
        var flxMain1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxMain1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "50px",
            "skin": "slFbox",
            "zIndex": 1
        }, controller.args[0], "flxMain1"), extendConfig({}, controller.args[1], "flxMain1"), extendConfig({}, controller.args[2], "flxMain1"));
        flxMain1.setDefaultUnit(kony.flex.DP);
        var lstboxPeriod1 = new kony.ui.ListBox(extendConfig({
            "centerY": "50%",
            "height": "40dp",
            "id": "lstboxPeriod1",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["ko", "Select Period"],
                ["k1", "Daily"],
                ["k2", "Weekly"],
                ["k3", "Monthly"]
            ],
            "selectedKey": "ko",
            "selectedKeyValue": ["ko", "Select Period"],
            "skin": "sknlbxE1E5ED3pxRadius",
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "lstboxPeriod1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxPeriod1"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstboxPeriod1"));
        var flxMaxLimit1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "focusSkin": "skntxtbxhover11abeb",
            "height": "40dp",
            "id": "flxMaxLimit1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "52%",
            "isModalContainer": false,
            "skin": "skntxtbxNormald7d9e0",
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "flxMaxLimit1"), extendConfig({}, controller.args[1], "flxMaxLimit1"), extendConfig({}, controller.args[2], "flxMaxLimit1"));
        flxMaxLimit1.setDefaultUnit(kony.flex.DP);
        var flxDollar1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDollar1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxNoneditable",
            "top": "0dp",
            "width": "40px",
            "zIndex": 1
        }, controller.args[0], "flxDollar1"), extendConfig({}, controller.args[1], "flxDollar1"), extendConfig({}, controller.args[2], "flxDollar1"));
        flxDollar1.setDefaultUnit(kony.flex.DP);
        var lblDollar1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblDollar1",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblDollar\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDollar1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDollar1"), extendConfig({}, controller.args[2], "lblDollar1"));
        flxDollar1.add(lblDollar1);
        var flxSeparator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSeparator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40dp",
            "isModalContainer": false,
            "top": "0dp",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxSeparator1"), extendConfig({}, controller.args[1], "flxSeparator1"), extendConfig({}, controller.args[2], "flxSeparator1"));
        flxSeparator1.setDefaultUnit(kony.flex.DP);
        flxSeparator1.add();
        var txtbxMaxLimit1 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtbxMaxLimit1",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
            "left": "41dp",
            "placeholder": "0.00",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbxRadius0px0a12b1c5c6fbf4e",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "txtbxMaxLimit1"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtbxMaxLimit1"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtbxMaxLimit1"));
        flxMaxLimit1.add(flxDollar1, flxSeparator1, txtbxMaxLimit1);
        flxMain1.add(lstboxPeriod1, flxMaxLimit1);
        var flxDelete1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxDelete1",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "width": "25px",
            "zIndex": 1
        }, controller.args[0], "flxDelete1"), extendConfig({}, controller.args[1], "flxDelete1"), extendConfig({}, controller.args[2], "flxDelete1"));
        flxDelete1.setDefaultUnit(kony.flex.DP);
        var imgDelete1 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDelete1",
            "isVisible": true,
            "skin": "slImage",
            "src": "delete_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgDelete1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDelete1"), extendConfig({}, controller.args[2], "imgDelete1"));
        flxDelete1.add(imgDelete1);
        var flxHint1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "focusSkin": "skntxtbxhover11abeb",
            "height": "12dp",
            "id": "flxHint1",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "51%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "flxHint1"), extendConfig({}, controller.args[1], "flxHint1"), extendConfig({}, controller.args[2], "flxHint1"));
        flxHint1.setDefaultUnit(kony.flex.DP);
        var lblHintHeader1 = new kony.ui.Label(extendConfig({
            "id": "lblHintHeader1",
            "isVisible": true,
            "left": "41dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHintHeader1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHintHeader1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHintHeader1"), extendConfig({}, controller.args[2], "lblHintHeader1"));
        var lblHintMessage1 = new kony.ui.Label(extendConfig({
            "id": "lblHintMessage1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHintMessage1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHintMessage1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHintMessage1"), extendConfig({}, controller.args[2], "lblHintMessage1"));
        var lblAmount1 = new kony.ui.Label(extendConfig({
            "id": "lblAmount1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAmount1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAmount1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmount1"), extendConfig({}, controller.args[2], "lblAmount1"));
        flxHint1.add(lblHintHeader1, lblHintMessage1, lblAmount1);
        flxLimit1.add(flxMain1, flxDelete1, flxHint1);
        var flxLimit2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "flxLimit2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxLimit2"), extendConfig({}, controller.args[1], "flxLimit2"), extendConfig({}, controller.args[2], "flxLimit2"));
        flxLimit2.setDefaultUnit(kony.flex.DP);
        var flxMain2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxMain2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "50px",
            "skin": "slFbox",
            "zIndex": 1
        }, controller.args[0], "flxMain2"), extendConfig({}, controller.args[1], "flxMain2"), extendConfig({}, controller.args[2], "flxMain2"));
        flxMain2.setDefaultUnit(kony.flex.DP);
        var lstboxPeriod2 = new kony.ui.ListBox(extendConfig({
            "centerY": "50%",
            "height": "40dp",
            "id": "lstboxPeriod2",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["ko", "Select Period"],
                ["k1", "Daily"],
                ["k2", "Weekly"],
                ["k3", "Monthly"]
            ],
            "selectedKey": "ko",
            "selectedKeyValue": ["ko", "Select Period"],
            "skin": "sknlbxE1E5ED3pxRadius",
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "lstboxPeriod2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxPeriod2"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstboxPeriod2"));
        var flxMaxLimit2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "focusSkin": "skntxtbxhover11abeb",
            "height": "40dp",
            "id": "flxMaxLimit2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "52%",
            "isModalContainer": false,
            "skin": "skntxtbxNormald7d9e0",
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "flxMaxLimit2"), extendConfig({}, controller.args[1], "flxMaxLimit2"), extendConfig({}, controller.args[2], "flxMaxLimit2"));
        flxMaxLimit2.setDefaultUnit(kony.flex.DP);
        var flxDollar2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDollar2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxNoneditable",
            "top": "0dp",
            "width": "40px",
            "zIndex": 1
        }, controller.args[0], "flxDollar2"), extendConfig({}, controller.args[1], "flxDollar2"), extendConfig({}, controller.args[2], "flxDollar2"));
        flxDollar2.setDefaultUnit(kony.flex.DP);
        var lblDollar2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblDollar2",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblDollar\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDollar2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDollar2"), extendConfig({}, controller.args[2], "lblDollar2"));
        flxDollar2.add(lblDollar2);
        var flxSeparator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSeparator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40dp",
            "isModalContainer": false,
            "top": "0dp",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxSeparator2"), extendConfig({}, controller.args[1], "flxSeparator2"), extendConfig({}, controller.args[2], "flxSeparator2"));
        flxSeparator2.setDefaultUnit(kony.flex.DP);
        flxSeparator2.add();
        var txtbxMaxLimit2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtbxMaxLimit2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
            "left": "41dp",
            "placeholder": "0.00",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbxRadius0px0a12b1c5c6fbf4e",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "txtbxMaxLimit2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtbxMaxLimit2"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtbxMaxLimit2"));
        flxMaxLimit2.add(flxDollar2, flxSeparator2, txtbxMaxLimit2);
        flxMain2.add(lstboxPeriod2, flxMaxLimit2);
        var flxDelete2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxDelete2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "width": "25px",
            "zIndex": 1
        }, controller.args[0], "flxDelete2"), extendConfig({}, controller.args[1], "flxDelete2"), extendConfig({}, controller.args[2], "flxDelete2"));
        flxDelete2.setDefaultUnit(kony.flex.DP);
        var imgDelete2 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDelete2",
            "isVisible": true,
            "skin": "slImage",
            "src": "delete_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgDelete2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDelete2"), extendConfig({}, controller.args[2], "imgDelete2"));
        flxDelete2.add(imgDelete2);
        var flxHint2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "focusSkin": "skntxtbxhover11abeb",
            "height": "12dp",
            "id": "flxHint2",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "51%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "flxHint2"), extendConfig({}, controller.args[1], "flxHint2"), extendConfig({}, controller.args[2], "flxHint2"));
        flxHint2.setDefaultUnit(kony.flex.DP);
        var lblHintHeader2 = new kony.ui.Label(extendConfig({
            "id": "lblHintHeader2",
            "isVisible": true,
            "left": "41dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHintHeader1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHintHeader2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHintHeader2"), extendConfig({}, controller.args[2], "lblHintHeader2"));
        var lblHintMessage2 = new kony.ui.Label(extendConfig({
            "id": "lblHintMessage2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHintMessage1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHintMessage2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHintMessage2"), extendConfig({}, controller.args[2], "lblHintMessage2"));
        var lblAmount2 = new kony.ui.Label(extendConfig({
            "id": "lblAmount2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAmount1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAmount2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmount2"), extendConfig({}, controller.args[2], "lblAmount2"));
        flxHint2.add(lblHintHeader2, lblHintMessage2, lblAmount2);
        flxLimit2.add(flxMain2, flxDelete2, flxHint2);
        var flxLimit3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "flxLimit3",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxLimit3"), extendConfig({}, controller.args[1], "flxLimit3"), extendConfig({}, controller.args[2], "flxLimit3"));
        flxLimit3.setDefaultUnit(kony.flex.DP);
        var flxMain3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxMain3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "50px",
            "skin": "slFbox",
            "zIndex": 1
        }, controller.args[0], "flxMain3"), extendConfig({}, controller.args[1], "flxMain3"), extendConfig({}, controller.args[2], "flxMain3"));
        flxMain3.setDefaultUnit(kony.flex.DP);
        var lstboxPeriod3 = new kony.ui.ListBox(extendConfig({
            "centerY": "50%",
            "height": "40dp",
            "id": "lstboxPeriod3",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["ko", "Select Period"],
                ["k1", "Daily"],
                ["k2", "Weekly"],
                ["k3", "Monthly"]
            ],
            "selectedKey": "ko",
            "selectedKeyValue": ["ko", "Select Period"],
            "skin": "sknlbxE1E5ED3pxRadius",
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "lstboxPeriod3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxPeriod3"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstboxPeriod3"));
        var flxMaxLimit3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "focusSkin": "skntxtbxhover11abeb",
            "height": "40dp",
            "id": "flxMaxLimit3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "52%",
            "isModalContainer": false,
            "skin": "skntxtbxNormald7d9e0",
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "flxMaxLimit3"), extendConfig({}, controller.args[1], "flxMaxLimit3"), extendConfig({}, controller.args[2], "flxMaxLimit3"));
        flxMaxLimit3.setDefaultUnit(kony.flex.DP);
        var flxDollar3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDollar3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxNoneditable",
            "top": "0dp",
            "width": "40px",
            "zIndex": 1
        }, controller.args[0], "flxDollar3"), extendConfig({}, controller.args[1], "flxDollar3"), extendConfig({}, controller.args[2], "flxDollar3"));
        flxDollar3.setDefaultUnit(kony.flex.DP);
        var lblDollar3 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblDollar3",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblDollar\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDollar3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDollar3"), extendConfig({}, controller.args[2], "lblDollar3"));
        flxDollar3.add(lblDollar3);
        var flxSeparator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSeparator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40dp",
            "isModalContainer": false,
            "top": "0dp",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxSeparator3"), extendConfig({}, controller.args[1], "flxSeparator3"), extendConfig({}, controller.args[2], "flxSeparator3"));
        flxSeparator3.setDefaultUnit(kony.flex.DP);
        flxSeparator3.add();
        var txtbxMaxLimit3 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtbxMaxLimit3",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
            "left": "41dp",
            "placeholder": "0.00",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbxRadius0px0a12b1c5c6fbf4e",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "txtbxMaxLimit3"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtbxMaxLimit3"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtbxMaxLimit3"));
        flxMaxLimit3.add(flxDollar3, flxSeparator3, txtbxMaxLimit3);
        flxMain3.add(lstboxPeriod3, flxMaxLimit3);
        var flxDelete3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxDelete3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "width": "25px",
            "zIndex": 1
        }, controller.args[0], "flxDelete3"), extendConfig({}, controller.args[1], "flxDelete3"), extendConfig({}, controller.args[2], "flxDelete3"));
        flxDelete3.setDefaultUnit(kony.flex.DP);
        var imgDelete3 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDelete3",
            "isVisible": true,
            "skin": "slImage",
            "src": "delete_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgDelete3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDelete3"), extendConfig({}, controller.args[2], "imgDelete3"));
        flxDelete3.add(imgDelete3);
        var flxHint3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "focusSkin": "skntxtbxhover11abeb",
            "height": "12dp",
            "id": "flxHint3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "51%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "flxHint3"), extendConfig({}, controller.args[1], "flxHint3"), extendConfig({}, controller.args[2], "flxHint3"));
        flxHint3.setDefaultUnit(kony.flex.DP);
        var lblHintHeader3 = new kony.ui.Label(extendConfig({
            "id": "lblHintHeader3",
            "isVisible": true,
            "left": "41dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHintHeader1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHintHeader3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHintHeader3"), extendConfig({}, controller.args[2], "lblHintHeader3"));
        var lblHintMessage3 = new kony.ui.Label(extendConfig({
            "id": "lblHintMessage3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHintMessage1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHintMessage3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHintMessage3"), extendConfig({}, controller.args[2], "lblHintMessage3"));
        var lblAmount3 = new kony.ui.Label(extendConfig({
            "id": "lblAmount3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl9CA9BALato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAmount1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAmount3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmount3"), extendConfig({}, controller.args[2], "lblAmount3"));
        flxHint3.add(lblHintHeader3, lblHintMessage3, lblAmount3);
        flxLimit3.add(flxMain3, flxDelete3, flxHint3);
        flxTable.add(flxLimit1, flxLimit2, flxLimit3);
        Limits.add(flxHeading, flxtableHeader, flxTable);
        return Limits;
    }
})