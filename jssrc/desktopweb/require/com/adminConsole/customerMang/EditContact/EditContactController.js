define("com/adminConsole/customerMang/EditContact/userEditContactController", {
    editContactPreShow: function() {
        this.setFlowActions();
    },
    setFlowActions: function() {
        var scopeObj = this;
        this.view.flxRadio1.onClick = function() {
            if (scopeObj.view.imgRadio1.src === "radio_notselected.png") {
                scopeObj.view.imgRadio1.src = "radio_selected.png";
                scopeObj.view.imgRadio2.src = "radio_notselected.png";
                scopeObj.view.imgRadio3.src = "radio_notselected.png";
            }
        };
        this.view.flxRadio2.onClick = function() {
            if (scopeObj.view.imgRadio2.src === "radio_notselected.png") {
                scopeObj.view.imgRadio2.src = "radio_selected.png";
                scopeObj.view.imgRadio1.src = "radio_notselected.png";
                scopeObj.view.imgRadio3.src = "radio_notselected.png";
            }
        };
        this.view.flxRadio3.onClick = function() {
            if (scopeObj.view.imgRadio3.src === "radio_notselected.png") {
                scopeObj.view.imgRadio3.src = "radio_selected.png";
                scopeObj.view.imgRadio1.src = "radio_notselected.png";
                scopeObj.view.imgRadio2.src = "radio_notselected.png";
            }
        };
    }
});
define("com/adminConsole/customerMang/EditContact/EditContactControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for EditContact **/
    AS_FlexContainer_he82ddc60f0a44c5b91d4e0e1d5482e9: function AS_FlexContainer_he82ddc60f0a44c5b91d4e0e1d5482e9(eventobject) {
        var self = this;
        this.editContactPreShow();
    }
});
define("com/adminConsole/customerMang/EditContact/EditContactController", ["com/adminConsole/customerMang/EditContact/userEditContactController", "com/adminConsole/customerMang/EditContact/EditContactControllerActions"], function() {
    var controller = require("com/adminConsole/customerMang/EditContact/userEditContactController");
    var actions = require("com/adminConsole/customerMang/EditContact/EditContactControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
