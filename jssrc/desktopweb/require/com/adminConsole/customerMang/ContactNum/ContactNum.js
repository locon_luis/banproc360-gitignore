define(function() {
    return function(controller) {
        var ContactNum = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "ContactNum",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "ContactNum"), extendConfig({}, controller.args[1], "ContactNum"), extendConfig({}, controller.args[2], "ContactNum"));
        ContactNum.setDefaultUnit(kony.flex.DP);
        var flxDetailsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetailsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetailsHeader"), extendConfig({}, controller.args[1], "flxDetailsHeader"), extendConfig({}, controller.args[2], "flxDetailsHeader"));
        flxDetailsHeader.setDefaultUnit(kony.flex.DP);
        var lblDetailsHeader = new kony.ui.Label(extendConfig({
            "bottom": "-2px",
            "id": "lblDetailsHeader",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknlblLatoBold0hCM",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Contact_numbers\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetailsHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetailsHeader"), extendConfig({}, controller.args[2], "lblDetailsHeader"));
        var flxButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 35,
            "skin": "slFbox",
            "width": "115px",
            "zIndex": 1
        }, controller.args[0], "flxButtons"), extendConfig({}, controller.args[1], "flxButtons"), extendConfig({}, controller.args[2], "flxButtons"));
        flxButtons.setDefaultUnit(kony.flex.DP);
        var btnAdd = new kony.ui.Button(extendConfig({
            "bottom": "0px",
            "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "height": "22px",
            "id": "btnAdd",
            "isVisible": true,
            "left": "0px",
            "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "btnAdd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAdd"), extendConfig({
            "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
        }, controller.args[2], "btnAdd"));
        var btnEdit = new kony.ui.Button(extendConfig({
            "bottom": "0px",
            "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "height": "22px",
            "id": "btnEdit",
            "isVisible": true,
            "right": "0px",
            "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "btnEdit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnEdit"), extendConfig({
            "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
        }, controller.args[2], "btnEdit"));
        flxButtons.add(btnAdd, btnEdit);
        flxDetailsHeader.add(lblDetailsHeader, flxButtons);
        var flxDetailsRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetailsRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetailsRow1"), extendConfig({}, controller.args[1], "flxDetailsRow1"), extendConfig({}, controller.args[2], "flxDetailsRow1"));
        flxDetailsRow1.setDefaultUnit(kony.flex.DP);
        var flxDetails1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10px",
            "width": "35%",
            "zIndex": 2
        }, controller.args[0], "flxDetails1"), extendConfig({}, controller.args[1], "flxDetails1"), extendConfig({}, controller.args[2], "flxDetails1"));
        flxDetails1.setDefaultUnit(kony.flex.DP);
        var lblPrimaryValue = new kony.ui.Label(extendConfig({
            "id": "lblPrimaryValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAdditionalDetailsValue1\")",
            "top": "30px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "lblPrimaryValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrimaryValue"), extendConfig({}, controller.args[2], "lblPrimaryValue"));
        var flxPrimaryLabelAndTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "18px",
            "id": "flxPrimaryLabelAndTag",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxPrimaryLabelAndTag"), extendConfig({}, controller.args[1], "flxPrimaryLabelAndTag"), extendConfig({}, controller.args[2], "flxPrimaryLabelAndTag"));
        flxPrimaryLabelAndTag.setDefaultUnit(kony.flex.DP);
        var lblPrimaryKey = new kony.ui.Label(extendConfig({
            "id": "lblPrimaryKey",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblPrimaryKey\")",
            "top": "2px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "lblPrimaryKey"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrimaryKey"), extendConfig({}, controller.args[2], "lblPrimaryKey"));
        var flxPrimaryCommunication = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "18dp",
            "id": "flxPrimaryCommunication",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "75px",
            "isModalContainer": false,
            "skin": "sknflx28a761Radius20px",
            "top": "0dp",
            "width": "200px",
            "zIndex": 3
        }, controller.args[0], "flxPrimaryCommunication"), extendConfig({}, controller.args[1], "flxPrimaryCommunication"), extendConfig({}, controller.args[2], "flxPrimaryCommunication"));
        flxPrimaryCommunication.setDefaultUnit(kony.flex.DP);
        var lblPrimaryCommunicationContact = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblPrimaryCommunicationContact",
            "isVisible": true,
            "skin": "sknlblffffffLato12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblPrimaryCommunicationContact\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblPrimaryCommunicationContact"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrimaryCommunicationContact"), extendConfig({}, controller.args[2], "lblPrimaryCommunicationContact"));
        flxPrimaryCommunication.add(lblPrimaryCommunicationContact);
        flxPrimaryLabelAndTag.add(lblPrimaryKey, flxPrimaryCommunication);
        flxDetails1.add(lblPrimaryValue, flxPrimaryLabelAndTag);
        var flxAdditionalDetails1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAdditionalDetails1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10px",
            "width": "35%",
            "zIndex": 1
        }, controller.args[0], "flxAdditionalDetails1"), extendConfig({}, controller.args[1], "flxAdditionalDetails1"), extendConfig({}, controller.args[2], "flxAdditionalDetails1"));
        flxAdditionalDetails1.setDefaultUnit(kony.flex.DP);
        var flxAddtionalDetailsHeader1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddtionalDetailsHeader1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxAddtionalDetailsHeader1"), extendConfig({}, controller.args[1], "flxAddtionalDetailsHeader1"), extendConfig({}, controller.args[2], "flxAddtionalDetailsHeader1"));
        flxAddtionalDetailsHeader1.setDefaultUnit(kony.flex.DP);
        var lblAdditionalDetailsKey1 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalDetailsKey1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAdditionalDetailsKey1\")",
            "top": "2px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalDetailsKey1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalDetailsKey1"), extendConfig({}, controller.args[2], "lblAdditionalDetailsKey1"));
        var btnDeleteAdditionalDetails1 = new kony.ui.Button(extendConfig({
            "id": "btnDeleteAdditionalDetails1",
            "isVisible": true,
            "left": "15px",
            "skin": "sknBtnLatoRegular11abeb14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
            "top": "2px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "btnDeleteAdditionalDetails1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnDeleteAdditionalDetails1"), extendConfig({
            "hoverSkin": "sknBtnLatoRegular1682b213pxHover"
        }, controller.args[2], "btnDeleteAdditionalDetails1"));
        flxAddtionalDetailsHeader1.add(lblAdditionalDetailsKey1, btnDeleteAdditionalDetails1);
        var lblAdditionalDetailsValue1 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalDetailsValue1",
            "isVisible": true,
            "left": "-1dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAdditionalDetailsValue1\")",
            "top": "30px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "lblAdditionalDetailsValue1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalDetailsValue1"), extendConfig({}, controller.args[2], "lblAdditionalDetailsValue1"));
        flxAdditionalDetails1.add(flxAddtionalDetailsHeader1, lblAdditionalDetailsValue1);
        flxDetailsRow1.add(flxDetails1, flxAdditionalDetails1);
        var flxDetailsRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10px",
            "clipBounds": true,
            "id": "flxDetailsRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetailsRow2"), extendConfig({}, controller.args[1], "flxDetailsRow2"), extendConfig({}, controller.args[2], "flxDetailsRow2"));
        flxDetailsRow2.setDefaultUnit(kony.flex.DP);
        var flxAdditionalDetails2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAdditionalDetails2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10px",
            "width": "35%",
            "zIndex": 1
        }, controller.args[0], "flxAdditionalDetails2"), extendConfig({}, controller.args[1], "flxAdditionalDetails2"), extendConfig({}, controller.args[2], "flxAdditionalDetails2"));
        flxAdditionalDetails2.setDefaultUnit(kony.flex.DP);
        var lblAdditionalDetailsValue2 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalDetailsValue2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAdditionalDetailsValue1\")",
            "top": "30px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "lblAdditionalDetailsValue2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalDetailsValue2"), extendConfig({}, controller.args[2], "lblAdditionalDetailsValue2"));
        var flxAddtionalDetailsHeader2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddtionalDetailsHeader2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 3
        }, controller.args[0], "flxAddtionalDetailsHeader2"), extendConfig({}, controller.args[1], "flxAddtionalDetailsHeader2"), extendConfig({}, controller.args[2], "flxAddtionalDetailsHeader2"));
        flxAddtionalDetailsHeader2.setDefaultUnit(kony.flex.DP);
        var lblAdditionalDetailsKey2 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalDetailsKey2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAdditionalDetailsKey2\")",
            "top": "2px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalDetailsKey2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalDetailsKey2"), extendConfig({}, controller.args[2], "lblAdditionalDetailsKey2"));
        var btnDeleteAdditionalDetails2 = new kony.ui.Button(extendConfig({
            "id": "btnDeleteAdditionalDetails2",
            "isVisible": true,
            "left": "15px",
            "skin": "sknBtnLatoRegular11abeb14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
            "top": "2px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "btnDeleteAdditionalDetails2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnDeleteAdditionalDetails2"), extendConfig({
            "hoverSkin": "sknBtnLatoRegular1682b213pxHover"
        }, controller.args[2], "btnDeleteAdditionalDetails2"));
        flxAddtionalDetailsHeader2.add(lblAdditionalDetailsKey2, btnDeleteAdditionalDetails2);
        flxAdditionalDetails2.add(lblAdditionalDetailsValue2, flxAddtionalDetailsHeader2);
        flxDetailsRow2.add(flxAdditionalDetails2);
        ContactNum.add(flxDetailsHeader, flxDetailsRow1, flxDetailsRow2);
        return ContactNum;
    }
})