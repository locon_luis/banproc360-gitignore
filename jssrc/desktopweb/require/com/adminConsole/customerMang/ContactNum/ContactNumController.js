define("com/adminConsole/customerMang/ContactNum/userContactNumController", function() {
    return {};
});
define("com/adminConsole/customerMang/ContactNum/ContactNumControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/customerMang/ContactNum/ContactNumController", ["com/adminConsole/customerMang/ContactNum/userContactNumController", "com/adminConsole/customerMang/ContactNum/ContactNumControllerActions"], function() {
    var controller = require("com/adminConsole/customerMang/ContactNum/userContactNumController");
    var actions = require("com/adminConsole/customerMang/ContactNum/ContactNumControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
