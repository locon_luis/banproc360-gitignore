define(function() {
    return function(controller) {
        var RequestsHelpCenter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "RequestsHelpCenter",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_b0c0b03602c1499eb6900f1b828d7bf0(eventobject);
            },
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "RequestsHelpCenter"), extendConfig({}, controller.args[1], "RequestsHelpCenter"), extendConfig({}, controller.args[2], "RequestsHelpCenter"));
        RequestsHelpCenter.setDefaultUnit(kony.flex.DP);
        var flxHeadingFilters = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxHeadingFilters",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeadingFilters"), extendConfig({}, controller.args[1], "flxHeadingFilters"), extendConfig({}, controller.args[2], "flxHeadingFilters"));
        flxHeadingFilters.setDefaultUnit(kony.flex.DP);
        var lblHeadingFilter = new kony.ui.Label(extendConfig({
            "id": "lblHeadingFilter",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknlblLatoBold485c7516px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Customer_Requests\")",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeadingFilter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeadingFilter"), extendConfig({}, controller.args[2], "lblHeadingFilter"));
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "sknflxd5d9ddop100",
            "top": "8dp",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var fontIconSearchImg = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSearchImg",
            "isVisible": true,
            "left": "12px",
            "skin": "sknFontIconSearchImg20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSearchImg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSearchImg"), extendConfig({}, controller.args[2], "fontIconSearchImg"));
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "30px",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30dp",
            "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
            "placeholder": "Search",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var flxClearSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxClearSearchImage",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxClearSearchImage"), extendConfig({}, controller.args[1], "flxClearSearchImage"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxClearSearchImage"));
        flxClearSearchImage.setDefaultUnit(kony.flex.DP);
        var fontIconCross = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconCross",
            "isVisible": true,
            "left": "5px",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconCross"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCross"), extendConfig({}, controller.args[2], "fontIconCross"));
        flxClearSearchImage.add(fontIconCross);
        flxSearchContainer.add(fontIconSearchImg, tbxSearchBox, flxClearSearchImage);
        var flxBtmLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxBtmLine",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "right": "35dp",
            "skin": "sknTableHeaderLine",
            "zIndex": 1
        }, controller.args[0], "flxBtmLine"), extendConfig({}, controller.args[1], "flxBtmLine"), extendConfig({}, controller.args[2], "flxBtmLine"));
        flxBtmLine.setDefaultUnit(kony.flex.DP);
        flxBtmLine.add();
        flxHeadingFilters.add(lblHeadingFilter, flxSearchContainer, flxBtmLine);
        var flxSegmentData = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegmentData",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "60dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSegmentData"), extendConfig({}, controller.args[1], "flxSegmentData"), extendConfig({}, controller.args[2], "flxSegmentData"));
        flxSegmentData.setDefaultUnit(kony.flex.DP);
        var flxRequestSegHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxRequestSegHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxRequestSegHeader"), extendConfig({}, controller.args[1], "flxRequestSegHeader"), extendConfig({}, controller.args[2], "flxRequestSegHeader"));
        flxRequestSegHeader.setDefaultUnit(kony.flex.DP);
        var flxRequestId = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxRequestId",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "13%",
            "zIndex": 1
        }, controller.args[0], "flxRequestId"), extendConfig({}, controller.args[1], "flxRequestId"), extendConfig({}, controller.args[2], "flxRequestId"));
        flxRequestId.setDefaultUnit(kony.flex.DP);
        var lblRequestId = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRequestId",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblRequestNumber\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRequestId"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRequestId"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblRequestId"));
        var lblIconIdSort = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconIdSort",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconIdSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconIdSort"), extendConfig({}, controller.args[2], "lblIconIdSort"));
        flxRequestId.add(lblRequestId, lblIconIdSort);
        var flxDate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxDate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "17%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "13.05%",
            "zIndex": 1
        }, controller.args[0], "flxDate"), extendConfig({}, controller.args[1], "flxDate"), extendConfig({}, controller.args[2], "flxDate"));
        flxDate.setDefaultUnit(kony.flex.DP);
        var lblDate = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDate",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblRequestDate\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDate"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblDate"));
        var lblIconDateSort = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconDateSort",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDateSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDateSort"), extendConfig({}, controller.args[2], "lblIconDateSort"));
        flxDate.add(lblDate, lblIconDateSort);
        var flxType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "36.50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "12.03%",
            "zIndex": 1
        }, controller.args[0], "flxType"), extendConfig({}, controller.args[1], "flxType"), extendConfig({}, controller.args[2], "flxType"));
        flxType.setDefaultUnit(kony.flex.DP);
        var lblTypeHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblTypeHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TYPE\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTypeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTypeHeader"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblTypeHeader"));
        var lblIconTypeSort = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconTypeSort",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconTypeSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconTypeSort"), extendConfig({}, controller.args[2], "lblIconTypeSort"));
        flxType.add(lblTypeHeader, lblIconTypeSort);
        var flxCardAccountHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxCardAccountHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "53%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxCardAccountHeader"), extendConfig({}, controller.args[1], "flxCardAccountHeader"), extendConfig({}, controller.args[2], "flxCardAccountHeader"));
        flxCardAccountHeader.setDefaultUnit(kony.flex.DP);
        var lblCardHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCardHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CARD_ACCOUNT\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCardHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCardHeader"), extendConfig({}, controller.args[2], "lblCardHeader"));
        var lblIconCardAcc = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconCardAcc",
            "isVisible": false,
            "left": "5dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconCardAcc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconCardAcc"), extendConfig({}, controller.args[2], "lblIconCardAcc"));
        flxCardAccountHeader.add(lblCardHeader, lblIconCardAcc);
        var flxChannel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxChannel",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "75%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "10.02%",
            "zIndex": 1
        }, controller.args[0], "flxChannel"), extendConfig({}, controller.args[1], "flxChannel"), extendConfig({}, controller.args[2], "flxChannel"));
        flxChannel.setDefaultUnit(kony.flex.DP);
        var lblChannelHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblChannelHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblChannelHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannelHeader"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblChannelHeader"));
        var lblIconChannelSort = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconChannelSort",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconChannelSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconChannelSort"), extendConfig({}, controller.args[2], "lblIconChannelSort"));
        flxChannel.add(lblChannelHeader, lblIconChannelSort);
        var flxStatusHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxStatusHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "85%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "12%",
            "zIndex": 1
        }, controller.args[0], "flxStatusHeader"), extendConfig({}, controller.args[1], "flxStatusHeader"), extendConfig({}, controller.args[2], "flxStatusHeader"));
        flxStatusHeader.setDefaultUnit(kony.flex.DP);
        var lblStatusHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatusHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.STATUS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatusHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatusHeader"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblStatusHeader"));
        var lblIconStatusSort = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconStatusSort",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconStatusSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconStatusSort"), extendConfig({}, controller.args[2], "lblIconStatusSort"));
        flxStatusHeader.add(lblStatusHeader, lblIconStatusSort);
        var flxTopSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxTopSeperator",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknTableHeaderLine",
            "top": 0,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTopSeperator"), extendConfig({}, controller.args[1], "flxTopSeperator"), extendConfig({}, controller.args[2], "flxTopSeperator"));
        flxTopSeperator.setDefaultUnit(kony.flex.DP);
        flxTopSeperator.add();
        var flxBottomSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxBottomSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknTableHeaderLine",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBottomSeperator"), extendConfig({}, controller.args[1], "flxBottomSeperator"), extendConfig({}, controller.args[2], "flxBottomSeperator"));
        flxBottomSeperator.setDefaultUnit(kony.flex.DP);
        flxBottomSeperator.add();
        flxRequestSegHeader.add(flxRequestId, flxDate, flxType, flxCardAccountHeader, flxChannel, flxStatusHeader, flxTopSeperator, flxBottomSeperator);
        var flxRequestsSeg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRequestsSeg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "60dp",
            "zIndex": 1
        }, controller.args[0], "flxRequestsSeg"), extendConfig({}, controller.args[1], "flxRequestsSeg"), extendConfig({}, controller.args[2], "flxRequestsSeg"));
        flxRequestsSeg.setDefaultUnit(kony.flex.DP);
        var segRequestsHelpCenter = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblCardAccount": "",
                "lblChannel": "",
                "lblDate": "",
                "lblIconStatus": "",
                "lblRequestId": "",
                "lblRequestType": "",
                "lblStatus": ""
            }],
            "groupCells": false,
            "id": "segRequestsHelpCenter",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxRequestsOuter",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "e1e5ed14",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxRequestData": "flxRequestData",
                "flxRequestsOuter": "flxRequestsOuter",
                "flxRowLine": "flxRowLine",
                "flxStatusCont": "flxStatusCont",
                "lblCardAccount": "lblCardAccount",
                "lblChannel": "lblChannel",
                "lblDate": "lblDate",
                "lblIconStatus": "lblIconStatus",
                "lblRequestId": "lblRequestId",
                "lblRequestType": "lblRequestType",
                "lblStatus": "lblStatus"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segRequestsHelpCenter"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segRequestsHelpCenter"), extendConfig({}, controller.args[2], "segRequestsHelpCenter"));
        var flxNoResults = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200dp",
            "id": "flxNoResults",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResults"), extendConfig({}, controller.args[1], "flxNoResults"), extendConfig({}, controller.args[2], "flxNoResults"));
        flxNoResults.setDefaultUnit(kony.flex.DP);
        var rtxNoResults = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxNoResults",
            "isVisible": true,
            "skin": "sknrtxLato485c7514px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.NO_RECORDS_FOUND\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxNoResults"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxNoResults"), extendConfig({}, controller.args[2], "rtxNoResults"));
        flxNoResults.add(rtxNoResults);
        flxRequestsSeg.add(segRequestsHelpCenter, flxNoResults);
        var flxnotificationSegHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxnotificationSegHeader",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxnotificationSegHeader"), extendConfig({}, controller.args[1], "flxnotificationSegHeader"), extendConfig({}, controller.args[2], "flxnotificationSegHeader"));
        flxnotificationSegHeader.setDefaultUnit(kony.flex.DP);
        var flxNotificationId = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxNotificationId",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "17%",
            "zIndex": 1
        }, controller.args[0], "flxNotificationId"), extendConfig({}, controller.args[1], "flxNotificationId"), extendConfig({}, controller.args[2], "flxNotificationId"));
        flxNotificationId.setDefaultUnit(kony.flex.DP);
        var lblNotificationIdHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblNotificationIdHeader",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato5d6c7f12px",
            "text": "NOTIFICATION ID",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNotificationIdHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNotificationIdHeader"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblNotificationIdHeader"));
        var lblIconNotificationIdSort = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconNotificationIdSort",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconNotificationIdSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconNotificationIdSort"), extendConfig({}, controller.args[2], "lblIconNotificationIdSort"));
        flxNotificationId.add(lblNotificationIdHeader, lblIconNotificationIdSort);
        var flxDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "21%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flxDetails"), extendConfig({}, controller.args[1], "flxDetails"), extendConfig({}, controller.args[2], "flxDetails"));
        flxDetails.setDefaultUnit(kony.flex.DP);
        var lblDetailsHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDetailsHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TYPE\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetailsHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetailsHeader"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblDetailsHeader"));
        var lblIconDetailsSort = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconDetailsSort",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDetailsSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDetailsSort"), extendConfig({}, controller.args[2], "lblIconDetailsSort"));
        flxDetails.add(lblDetailsHeader, lblIconDetailsSort);
        var flxSelectedCards = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxSelectedCards",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "39%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxSelectedCards"), extendConfig({}, controller.args[1], "flxSelectedCards"), extendConfig({}, controller.args[2], "flxSelectedCards"));
        flxSelectedCards.setDefaultUnit(kony.flex.DP);
        var lblSelCardsHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelCardsHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CARDS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelCardsHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelCardsHeader"), extendConfig({}, controller.args[2], "lblSelCardsHeader"));
        var lblIconSelCardsSort = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconSelCardsSort",
            "isVisible": false,
            "left": "5dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconSelCardsSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconSelCardsSort"), extendConfig({}, controller.args[2], "lblIconSelCardsSort"));
        flxSelectedCards.add(lblSelCardsHeader, lblIconSelCardsSort);
        var flxNotificationStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxNotificationStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "79%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flxNotificationStatus"), extendConfig({}, controller.args[1], "flxNotificationStatus"), extendConfig({}, controller.args[2], "flxNotificationStatus"));
        flxNotificationStatus.setDefaultUnit(kony.flex.DP);
        var lblNotifStatusHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblNotifStatusHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.STATUS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNotifStatusHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNotifStatusHeader"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblNotifStatusHeader"));
        var lblIconNotifStatusSort = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconNotifStatusSort",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconNotifStatusSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconNotifStatusSort"), extendConfig({}, controller.args[2], "lblIconNotifStatusSort"));
        flxNotificationStatus.add(lblNotifStatusHeader, lblIconNotifStatusSort);
        var flxTopLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxTopLine",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknTableHeaderLine",
            "top": 0,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTopLine"), extendConfig({}, controller.args[1], "flxTopLine"), extendConfig({}, controller.args[2], "flxTopLine"));
        flxTopLine.setDefaultUnit(kony.flex.DP);
        flxTopLine.add();
        var flxBottomLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxBottomLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknTableHeaderLine",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBottomLine"), extendConfig({}, controller.args[1], "flxBottomLine"), extendConfig({}, controller.args[2], "flxBottomLine"));
        flxBottomLine.setDefaultUnit(kony.flex.DP);
        flxBottomLine.add();
        flxnotificationSegHeader.add(flxNotificationId, flxDetails, flxSelectedCards, flxNotificationStatus, flxTopLine, flxBottomLine);
        var flxNotificationSegHC = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxNotificationSegHC",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "60dp",
            "zIndex": 1
        }, controller.args[0], "flxNotificationSegHC"), extendConfig({}, controller.args[1], "flxNotificationSegHC"), extendConfig({}, controller.args[2], "flxNotificationSegHC"));
        flxNotificationSegHC.setDefaultUnit(kony.flex.DP);
        var segNotificationsHC = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblDetails": "",
                "lblIconAction": "",
                "lblIconStatus": "",
                "lblNotificationId": "",
                "lblSelectedCards": "",
                "lblStatus": ""
            }],
            "groupCells": false,
            "id": "segNotificationsHC",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxNotificationsOuter",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "e1e5ed14",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxNotificationRow": "flxNotificationRow",
                "flxNotificationsOuter": "flxNotificationsOuter",
                "flxRowLine": "flxRowLine",
                "flxStatusCont": "flxStatusCont",
                "lblDetails": "lblDetails",
                "lblIconAction": "lblIconAction",
                "lblIconStatus": "lblIconStatus",
                "lblNotificationId": "lblNotificationId",
                "lblSelectedCards": "lblSelectedCards",
                "lblStatus": "lblStatus"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segNotificationsHC"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segNotificationsHC"), extendConfig({}, controller.args[2], "segNotificationsHC"));
        var flxNotificationNoResults = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200dp",
            "id": "flxNotificationNoResults",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNotificationNoResults"), extendConfig({}, controller.args[1], "flxNotificationNoResults"), extendConfig({}, controller.args[2], "flxNotificationNoResults"));
        flxNotificationNoResults.setDefaultUnit(kony.flex.DP);
        var rtxNoNotification = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxNoNotification",
            "isVisible": true,
            "skin": "sknrtxLato485c7514px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.NO_RECORDS_FOUND\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxNoNotification"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxNoNotification"), extendConfig({}, controller.args[2], "rtxNoNotification"));
        flxNotificationNoResults.add(rtxNoNotification);
        flxNotificationSegHC.add(segNotificationsHC, flxNotificationNoResults);
        var flxRequestStatusFilter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRequestStatusFilter",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1113dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35dp",
            "width": "130dp",
            "zIndex": 1
        }, controller.args[0], "flxRequestStatusFilter"), extendConfig({}, controller.args[1], "flxRequestStatusFilter"), extendConfig({}, controller.args[2], "flxRequestStatusFilter"));
        flxRequestStatusFilter.setDefaultUnit(kony.flex.DP);
        var statusFilterMenu = new com.adminConsole.common.statusFilterMenu(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "statusFilterMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 5,
            "overrides": {}
        }, controller.args[0], "statusFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[1], "statusFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[2], "statusFilterMenu"));
        flxRequestStatusFilter.add(statusFilterMenu);
        flxSegmentData.add(flxRequestSegHeader, flxRequestsSeg, flxnotificationSegHeader, flxNotificationSegHC, flxRequestStatusFilter);
        var flxRequestsOnClick = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRequestsOnClick",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRequestsOnClick"), extendConfig({}, controller.args[1], "flxRequestsOnClick"), extendConfig({}, controller.args[2], "flxRequestsOnClick"));
        flxRequestsOnClick.setDefaultUnit(kony.flex.DP);
        var backToPageHeader = new com.adminConsole.customerMang.backToPageHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "backToPageHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "overrides": {
                "backToPageHeader": {
                    "height": "40px",
                    "top": "10dp"
                },
                "flxBack": {
                    "width": "25dp"
                },
                "fontIconBack": {
                    "height": kony.flex.USE_PREFFERED_SIZE,
                    "width": kony.flex.USE_PREFFERED_SIZE
                }
            }
        }, controller.args[0], "backToPageHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "backToPageHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "backToPageHeader"));
        var flxDetailsContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetailsContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "45dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetailsContainer"), extendConfig({}, controller.args[1], "flxDetailsContainer"), extendConfig({}, controller.args[2], "flxDetailsContainer"));
        flxDetailsContainer.setDefaultUnit(kony.flex.DP);
        var flxHeadingContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxHeadingContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeadingContainer"), extendConfig({}, controller.args[1], "flxHeadingContainer"), extendConfig({}, controller.args[2], "flxHeadingContainer"));
        flxHeadingContainer.setDefaultUnit(kony.flex.DP);
        var flxDataContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "48dp",
            "id": "flxDataContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataContainer"), extendConfig({}, controller.args[1], "flxDataContainer"), extendConfig({}, controller.args[2], "flxDataContainer"));
        flxDataContainer.setDefaultUnit(kony.flex.DP);
        var lblIdHeading = new kony.ui.Label(extendConfig({
            "id": "lblIdHeading",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknlblLatoRegular485c7518Px",
            "text": "Request Id",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIdHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIdHeading"), extendConfig({}, controller.args[2], "lblIdHeading"));
        var lblIdValue = new kony.ui.Label(extendConfig({
            "id": "lblIdValue",
            "isVisible": true,
            "left": "12dp",
            "skin": "sknlblLatoRegular485c7518Px",
            "text": "12345",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIdValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIdValue"), extendConfig({}, controller.args[2], "lblIdValue"));
        var flxStatusCont = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxStatusCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "13%",
            "zIndex": 1
        }, controller.args[0], "flxStatusCont"), extendConfig({}, controller.args[1], "flxStatusCont"), extendConfig({}, controller.args[2], "flxStatusCont"));
        flxStatusCont.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label(extendConfig({
            "centerY": "51%",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconStatus"), extendConfig({}, controller.args[2], "lblIconStatus"));
        var lblStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "18dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "In progress",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus"), extendConfig({}, controller.args[2], "lblStatus"));
        flxStatusCont.add(lblIconStatus, lblStatus);
        flxDataContainer.add(lblIdHeading, lblIdValue, flxStatusCont);
        var flxBottomSeparator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxBottomSeparator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSeparatorCsr",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBottomSeparator"), extendConfig({}, controller.args[1], "flxBottomSeparator"), extendConfig({}, controller.args[2], "flxBottomSeparator"));
        flxBottomSeparator.setDefaultUnit(kony.flex.DP);
        flxBottomSeparator.add();
        flxHeadingContainer.add(flxDataContainer, flxBottomSeparator);
        var flxDetailsContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxDetailsContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetailsContent"), extendConfig({}, controller.args[1], "flxDetailsContent"), extendConfig({}, controller.args[2], "flxDetailsContent"));
        flxDetailsContent.setDefaultUnit(kony.flex.DP);
        var flxRequestDetailsRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRequestDetailsRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRequestDetailsRow1"), extendConfig({}, controller.args[1], "flxRequestDetailsRow1"), extendConfig({}, controller.args[2], "flxRequestDetailsRow1"));
        flxRequestDetailsRow1.setDefaultUnit(kony.flex.DP);
        var detailsRow1 = new com.adminConsole.view.details1(extendConfig({
            "clipBounds": true,
            "height": "40px",
            "id": "detailsRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "flxColumn3": {
                    "left": "73%"
                },
                "lblIconData2": {
                    "isVisible": false
                },
                "lblIconData3": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "detailsRow1"), extendConfig({
            "overrides": {}
        }, controller.args[1], "detailsRow1"), extendConfig({
            "overrides": {}
        }, controller.args[2], "detailsRow1"));
        flxRequestDetailsRow1.add(detailsRow1);
        var flxRequestDetailsRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRequestDetailsRow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRequestDetailsRow2"), extendConfig({}, controller.args[1], "flxRequestDetailsRow2"), extendConfig({}, controller.args[2], "flxRequestDetailsRow2"));
        flxRequestDetailsRow2.setDefaultUnit(kony.flex.DP);
        var flxColumn1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxColumn1"), extendConfig({}, controller.args[1], "flxColumn1"), extendConfig({}, controller.args[2], "flxColumn1"));
        flxColumn1.setDefaultUnit(kony.flex.DP);
        var lblHeading1 = new kony.ui.Label(extendConfig({
            "id": "lblHeading1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading1"), extendConfig({}, controller.args[2], "lblHeading1"));
        var flxDataAndImage1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDataAndImage1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataAndImage1"), extendConfig({}, controller.args[1], "flxDataAndImage1"), extendConfig({}, controller.args[2], "flxDataAndImage1"));
        flxDataAndImage1.setDefaultUnit(kony.flex.DP);
        var lblData1 = new kony.ui.Label(extendConfig({
            "id": "lblData1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "lblData1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData1"), extendConfig({}, controller.args[2], "lblData1"));
        flxDataAndImage1.add(lblData1);
        flxColumn1.add(lblHeading1, flxDataAndImage1);
        var flxColumn2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxColumn2"), extendConfig({}, controller.args[1], "flxColumn2"), extendConfig({}, controller.args[2], "flxColumn2"));
        flxColumn2.setDefaultUnit(kony.flex.DP);
        var lblHeading2 = new kony.ui.Label(extendConfig({
            "id": "lblHeading2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading2"), extendConfig({}, controller.args[2], "lblHeading2"));
        var flxDataAndImage2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDataAndImage2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataAndImage2"), extendConfig({}, controller.args[1], "flxDataAndImage2"), extendConfig({}, controller.args[2], "flxDataAndImage2"));
        flxDataAndImage2.setDefaultUnit(kony.flex.DP);
        var lblData2 = new kony.ui.Label(extendConfig({
            "id": "lblData2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "lblData2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData2"), extendConfig({}, controller.args[2], "lblData2"));
        flxDataAndImage2.add(lblData2);
        flxColumn2.add(lblHeading2, flxDataAndImage2);
        var flxColumn3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "73%",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxColumn3"), extendConfig({}, controller.args[1], "flxColumn3"), extendConfig({}, controller.args[2], "flxColumn3"));
        flxColumn3.setDefaultUnit(kony.flex.DP);
        var lblHeading3 = new kony.ui.Label(extendConfig({
            "id": "lblHeading3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading3"), extendConfig({}, controller.args[2], "lblHeading3"));
        var flxDataAndImage3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxDataAndImage3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataAndImage3"), extendConfig({}, controller.args[1], "flxDataAndImage3"), extendConfig({}, controller.args[2], "flxDataAndImage3"));
        flxDataAndImage3.setDefaultUnit(kony.flex.DP);
        var lblData3 = new kony.ui.Label(extendConfig({
            "id": "lblData3",
            "isVisible": true,
            "left": "0px",
            "right": "7px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "lblData3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData3"), extendConfig({}, controller.args[2], "lblData3"));
        flxDataAndImage3.add(lblData3);
        flxColumn3.add(lblHeading3, flxDataAndImage3);
        flxRequestDetailsRow2.add(flxColumn1, flxColumn2, flxColumn3);
        var flxRequestDetailsRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRequestDetailsRow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRequestDetailsRow3"), extendConfig({}, controller.args[1], "flxRequestDetailsRow3"), extendConfig({}, controller.args[2], "flxRequestDetailsRow3"));
        flxRequestDetailsRow3.setDefaultUnit(kony.flex.DP);
        var flxRow3Col1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow3Col1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxRow3Col1"), extendConfig({}, controller.args[1], "flxRow3Col1"), extendConfig({}, controller.args[2], "flxRow3Col1"));
        flxRow3Col1.setDefaultUnit(kony.flex.DP);
        var lblHeadingRow3Col1 = new kony.ui.Label(extendConfig({
            "id": "lblHeadingRow3Col1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeadingRow3Col1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeadingRow3Col1"), extendConfig({}, controller.args[2], "lblHeadingRow3Col1"));
        var flxDataRow3Col1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDataRow3Col1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataRow3Col1"), extendConfig({}, controller.args[1], "flxDataRow3Col1"), extendConfig({}, controller.args[2], "flxDataRow3Col1"));
        flxDataRow3Col1.setDefaultUnit(kony.flex.DP);
        var lblDataRow3Col1 = new kony.ui.Label(extendConfig({
            "id": "lblDataRow3Col1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "lblDataRow3Col1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDataRow3Col1"), extendConfig({}, controller.args[2], "lblDataRow3Col1"));
        flxDataRow3Col1.add(lblDataRow3Col1);
        flxRow3Col1.add(lblHeadingRow3Col1, flxDataRow3Col1);
        var flxRow3Col2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow3Col2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxRow3Col2"), extendConfig({}, controller.args[1], "flxRow3Col2"), extendConfig({}, controller.args[2], "flxRow3Col2"));
        flxRow3Col2.setDefaultUnit(kony.flex.DP);
        var lblHeadingRow3Col2 = new kony.ui.Label(extendConfig({
            "id": "lblHeadingRow3Col2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeadingRow3Col2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeadingRow3Col2"), extendConfig({}, controller.args[2], "lblHeadingRow3Col2"));
        var flxDataRow3Col2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDataRow3Col2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataRow3Col2"), extendConfig({}, controller.args[1], "flxDataRow3Col2"), extendConfig({}, controller.args[2], "flxDataRow3Col2"));
        flxDataRow3Col2.setDefaultUnit(kony.flex.DP);
        var lblDataRow3Col2 = new kony.ui.Label(extendConfig({
            "id": "lblDataRow3Col2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": "95px",
            "zIndex": 1
        }, controller.args[0], "lblDataRow3Col2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDataRow3Col2"), extendConfig({}, controller.args[2], "lblDataRow3Col2"));
        flxDataRow3Col2.add(lblDataRow3Col2);
        flxRow3Col2.add(lblHeadingRow3Col2, flxDataRow3Col2);
        flxRequestDetailsRow3.add(flxRow3Col1, flxRow3Col2);
        flxDetailsContent.add(flxRequestDetailsRow1, flxRequestDetailsRow2, flxRequestDetailsRow3);
        flxDetailsContainer.add(flxHeadingContainer, flxDetailsContent);
        flxRequestsOnClick.add(backToPageHeader, flxDetailsContainer);
        RequestsHelpCenter.add(flxHeadingFilters, flxSegmentData, flxRequestsOnClick);
        return RequestsHelpCenter;
    }
})