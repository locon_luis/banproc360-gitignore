define("com/adminConsole/customerMang/tabs/usertabsController", [], function() {
    return {
        setSkinForInfoTabs: function(btnWidget) {
            var widgetArray = [this.view.btnTabName1,
                this.view.btnTabName2,
                this.view.btnTabName3,
                this.view.btnTabName4,
                this.view.btnTabName5,
                this.view.btnTabName6,
                this.view.btnTabName7,
                this.view.btnTabName8
            ];
            require('TabUtil_FormExtn').tabUtilButtonFunction(widgetArray, btnWidget);
        },
        setFlowActions: function() {
            var scopeObj = this;
            var CustomerManagementPresentation = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CustomerManagementModule").presentationController;
            this.view.btnTabName1.onClick = function() {
                scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName1);
                CustomerManagementPresentation.navigateToContactsTab();
            };
            this.view.btnTabName2.onClick = function() {
                scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName2);
                CustomerManagementPresentation.navigateToAccountsTab();
            };
            this.view.btnTabName3.onClick = function() {
                scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName3);
                CustomerManagementPresentation.navigateToHelpCenterTab();
            };
            this.view.btnTabName4.onClick = function() {
                scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName4);
                CustomerManagementPresentation.navigateToRolesTab();
            };
            this.view.btnTabName5.onClick = function() {
                scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName5);
                CustomerManagementPresentation.navigateToEntitlementsTab();
            };
            this.view.btnTabName6.onClick = function() {
                scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName6);
                CustomerManagementPresentation.navigateToActivityHistoryTab();
            };
            this.view.btnTabName7.onClick = function() {
                scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName7);
                CustomerManagementPresentation.navigateToAlertHistoryTab();
            };
            this.view.btnTabName8.onClick = function() {
                scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName8);
                CustomerManagementPresentation.navigateToDeviceInfoTab();
            };
        }
    };
});
define("com/adminConsole/customerMang/tabs/tabsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for tabs **/
    AS_FlexContainer_b1a3158c443a4c42acb8d6a705fbc11f: function AS_FlexContainer_b1a3158c443a4c42acb8d6a705fbc11f(eventobject) {
        var self = this;
        this.setFlowActions();
    }
});
define("com/adminConsole/customerMang/tabs/tabsController", ["com/adminConsole/customerMang/tabs/usertabsController", "com/adminConsole/customerMang/tabs/tabsControllerActions"], function() {
    var controller = require("com/adminConsole/customerMang/tabs/usertabsController");
    var actions = require("com/adminConsole/customerMang/tabs/tabsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
