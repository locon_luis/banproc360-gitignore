define(function() {
    return function(controller) {
        var tabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "60px",
            "id": "tabs",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_b1a3158c443a4c42acb8d6a705fbc11f(eventobject);
            },
            "skin": "slFbox0hfd18814fd664dCM",
            "width": "100%"
        }, controller.args[0], "tabs"), extendConfig({}, controller.args[1], "tabs"), extendConfig({}, controller.args[2], "tabs"));
        tabs.setDefaultUnit(kony.flex.DP);
        var btnTabName1 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName1",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtn9ca9ba12PxNoBorder",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CONTACT\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName1"), extendConfig({
            "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
        }, controller.args[2], "btnTabName1"));
        var btnTabName2 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName2",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtn9ca9ba12PxNoBorder",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PRODUCTS\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName2"), extendConfig({
            "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
        }, controller.args[2], "btnTabName2"));
        var btnTabName3 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName3",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtn9ca9ba12PxNoBorder",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.HELP_CENTER\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName3"), extendConfig({
            "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
        }, controller.args[2], "btnTabName3"));
        var btnTabName4 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName4",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtn9ca9ba12PxNoBorder",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Profile_tabs_CUSTOMER_Roles\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName4"), extendConfig({
            "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
        }, controller.args[2], "btnTabName4"));
        var btnTabName5 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName5",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtn9ca9ba12PxNoBorder",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Profile_tabs_CUSTOMER_Permissions\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName5"), extendConfig({
            "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
        }, controller.args[2], "btnTabName5"));
        var btnTabName6 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName6",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtn9ca9ba12PxNoBorder",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACTIVITY_HISTORY\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName6"), extendConfig({
            "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
        }, controller.args[2], "btnTabName6"));
        var btnTabName7 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName7",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtn9ca9ba12PxNoBorder",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.AlertsManagement.ALERT\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName7"), extendConfig({
            "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
        }, controller.args[2], "btnTabName7"));
        var btnTabName8 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName8",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtn9ca9ba12PxNoBorder",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DEVICE_INFO\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName8"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName8"), extendConfig({
            "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
        }, controller.args[2], "btnTabName8"));
        tabs.add(btnTabName1, btnTabName2, btnTabName3, btnTabName4, btnTabName5, btnTabName6, btnTabName7, btnTabName8);
        return tabs;
    }
})