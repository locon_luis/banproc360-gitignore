define(function() {
    return function(controller) {
        var search = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "60px",
            "id": "search",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0b7aaa2e3bfa340",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "search"), extendConfig({}, controller.args[1], "search"), extendConfig({}, controller.args[2], "search"));
        search.setDefaultUnit(kony.flex.DP);
        var flxListboxAndCalendar = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxListboxAndCalendar",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox2",
            "top": "0dp",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "flxListboxAndCalendar"), extendConfig({}, controller.args[1], "flxListboxAndCalendar"), extendConfig({}, controller.args[2], "flxListboxAndCalendar"));
        flxListboxAndCalendar.setDefaultUnit(kony.flex.DP);
        var lblShowing = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblShowing",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblShowing\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblShowing"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblShowing"), extendConfig({}, controller.args[2], "lblShowing"));
        var lbxPageNumbers = new kony.ui.ListBox(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lbxPageNumbers",
            "isVisible": true,
            "left": "15px",
            "masterData": [
                ["k1", "last 7 days"],
                ["k2", "last 30 days"],
                ["k3", "last 3 months"],
                ["k4", "custom"]
            ],
            "selectedKey": "k1",
            "selectedKeyValue": ["k1", "last 7 days"],
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "width": "120px",
            "zIndex": 1
        }, controller.args[0], "lbxPageNumbers"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbxPageNumbers"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lbxPageNumbers"));
        var flxStartDate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxStartDate",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "sknFlxCalendar",
            "top": "10px",
            "width": "140px"
        }, controller.args[0], "flxStartDate"), extendConfig({}, controller.args[1], "flxStartDate"), extendConfig({
            "hoverSkin": "skntxtbxhover11abeb"
        }, controller.args[2], "flxStartDate"));
        flxStartDate.setDefaultUnit(kony.flex.DP);
        flxStartDate.add();
        var flxEndDate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxEndDate",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "sknFlxCalendar",
            "top": "10px",
            "width": "140px"
        }, controller.args[0], "flxEndDate"), extendConfig({}, controller.args[1], "flxEndDate"), extendConfig({
            "hoverSkin": "skntxtbxhover11abeb"
        }, controller.args[2], "flxEndDate"));
        flxEndDate.setDefaultUnit(kony.flex.DP);
        flxEndDate.add();
        flxListboxAndCalendar.add(lblShowing, lbxPageNumbers, flxStartDate, flxEndDate);
        var flxSearchAndDownload = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSearchAndDownload",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "CopyslFbox2",
            "top": "0px",
            "width": "40%",
            "zIndex": 1
        }, controller.args[0], "flxSearchAndDownload"), extendConfig({}, controller.args[1], "flxSearchAndDownload"), extendConfig({}, controller.args[2], "flxSearchAndDownload"));
        flxSearchAndDownload.setDefaultUnit(kony.flex.DP);
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "36px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": 70,
            "skin": "sknflxBgffffffBorderc1c9ceRadius30px",
            "top": "8dp",
            "zIndex": 1
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var imgSearchIcon = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "imgSearchIcon",
            "isVisible": true,
            "left": "15px",
            "skin": "CopyslImage2",
            "src": "search_1x.png",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "imgSearchIcon"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSearchIcon"), extendConfig({}, controller.args[2], "imgSearchIcon"));
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "30px",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30dp",
            "onTouchStart": controller.AS_TextField_8869ed77fd9d4cc3bb4d7faea4e0afdb,
            "placeholder": "Search",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var flxClearSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxClearSearchImage",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknCursor",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxClearSearchImage"), extendConfig({}, controller.args[1], "flxClearSearchImage"), extendConfig({}, controller.args[2], "flxClearSearchImage"));
        flxClearSearchImage.setDefaultUnit(kony.flex.DP);
        var imgClearSearch = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "imgClearSearch",
            "isVisible": true,
            "right": "15px",
            "skin": "CopyslImage2",
            "src": "close_blue.png",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "imgClearSearch"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClearSearch"), extendConfig({}, controller.args[2], "imgClearSearch"));
        flxClearSearchImage.add(imgClearSearch);
        flxSearchContainer.add(imgSearchIcon, tbxSearchBox, flxClearSearchImage);
        var flxDownload = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30px",
            "id": "flxDownload",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "width": "30px",
            "zIndex": 1
        }, controller.args[0], "flxDownload"), extendConfig({}, controller.args[1], "flxDownload"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxDownload"));
        flxDownload.setDefaultUnit(kony.flex.DP);
        var imgDownload = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "imgDownload",
            "isVisible": true,
            "skin": "slImage",
            "src": "download_2x.png",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgDownload"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDownload"), extendConfig({}, controller.args[2], "imgDownload"));
        flxDownload.add(imgDownload);
        flxSearchAndDownload.add(flxSearchContainer, flxDownload);
        search.add(flxListboxAndCalendar, flxSearchAndDownload);
        return search;
    }
})