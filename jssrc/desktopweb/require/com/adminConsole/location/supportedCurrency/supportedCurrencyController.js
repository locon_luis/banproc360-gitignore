define("com/adminConsole/location/supportedCurrency/usersupportedCurrencyController", function() {
    return {};
});
define("com/adminConsole/location/supportedCurrency/supportedCurrencyControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/location/supportedCurrency/supportedCurrencyController", ["com/adminConsole/location/supportedCurrency/usersupportedCurrencyController", "com/adminConsole/location/supportedCurrency/supportedCurrencyControllerActions"], function() {
    var controller = require("com/adminConsole/location/supportedCurrency/usersupportedCurrencyController");
    var actions = require("com/adminConsole/location/supportedCurrency/supportedCurrencyControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
