define("com/adminConsole/commonTabsDashboard/dashboardCommonTab/userdashboardCommonTabController", function() {
    return {
        setFlowActions: function() {
            this.view.btnProfile.onClick = function() {
                var customerManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CustomerManagementModule");
                customerManagementModule.presentationController.navigateToContactsTab();
            };
            this.view.btnLoans.onClick = function() {
                var customerManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CustomerManagementModule");
                customerManagementModule.presentationController.showLoansForm();
            };
            this.view.btnDeposits.onClick = function() {
                var customerManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CustomerManagementModule");
                customerManagementModule.presentationController.showDepositsForm();
            };
        }
    };
});
define("com/adminConsole/commonTabsDashboard/dashboardCommonTab/dashboardCommonTabControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for dashboardCommonTab **/
    AS_FlexContainer_ab7d0f743ba94ff98d2abb57bb65cbaa: function AS_FlexContainer_ab7d0f743ba94ff98d2abb57bb65cbaa(eventobject) {
        var self = this;
        this.setFlowActions();
    }
});
define("com/adminConsole/commonTabsDashboard/dashboardCommonTab/dashboardCommonTabController", ["com/adminConsole/commonTabsDashboard/dashboardCommonTab/userdashboardCommonTabController", "com/adminConsole/commonTabsDashboard/dashboardCommonTab/dashboardCommonTabControllerActions"], function() {
    var controller = require("com/adminConsole/commonTabsDashboard/dashboardCommonTab/userdashboardCommonTabController");
    var actions = require("com/adminConsole/commonTabsDashboard/dashboardCommonTab/dashboardCommonTabControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
