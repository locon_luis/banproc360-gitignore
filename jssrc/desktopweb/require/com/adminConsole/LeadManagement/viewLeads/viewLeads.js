define(function() {
    return function(controller) {
        var viewLeads = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewLeads",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "viewLeads"), extendConfig({}, controller.args[1], "viewLeads"), extendConfig({}, controller.args[2], "viewLeads"));
        viewLeads.setDefaultUnit(kony.flex.DP);
        var flxHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxffffffop100",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxHeaders"), extendConfig({}, controller.args[1], "flxHeaders"), extendConfig({}, controller.args[2], "flxHeaders"));
        flxHeaders.setDefaultUnit(kony.flex.DP);
        var flxHeaderLeadCheckbox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxHeaderLeadCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "30px",
            "zIndex": 1
        }, controller.args[0], "flxHeaderLeadCheckbox"), extendConfig({}, controller.args[1], "flxHeaderLeadCheckbox"), extendConfig({}, controller.args[2], "flxHeaderLeadCheckbox"));
        flxHeaderLeadCheckbox.setDefaultUnit(kony.flex.DP);
        var imgHeaderCheckBox = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "imgHeaderCheckBox",
            "isVisible": false,
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "12px",
            "zIndex": 2
        }, controller.args[0], "imgHeaderCheckBox"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgHeaderCheckBox"), extendConfig({}, controller.args[2], "imgHeaderCheckBox"));
        flxHeaderLeadCheckbox.add(imgHeaderCheckBox);
        var flxHeaderLeadType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxHeaderLeadType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "30px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "40px",
            "zIndex": 1
        }, controller.args[0], "flxHeaderLeadType"), extendConfig({}, controller.args[1], "flxHeaderLeadType"), extendConfig({}, controller.args[2], "flxHeaderLeadType"));
        flxHeaderLeadType.setDefaultUnit(kony.flex.DP);
        flxHeaderLeadType.add();
        var flxHeaderLeadMain = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxHeaderLeadMain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "70px",
            "isModalContainer": false,
            "right": "60px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxHeaderLeadMain"), extendConfig({}, controller.args[1], "flxHeaderLeadMain"), extendConfig({}, controller.args[2], "flxHeaderLeadMain"));
        flxHeaderLeadMain.setDefaultUnit(kony.flex.DP);
        var flxLeadName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxLeadName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "18%",
            "zIndex": 1
        }, controller.args[0], "flxLeadName"), extendConfig({}, controller.args[1], "flxLeadName"), extendConfig({}, controller.args[2], "flxLeadName"));
        flxLeadName.setDefaultUnit(kony.flex.DP);
        var lblLeadName = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblLeadName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLeadViewHeader",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.segmentheaderName\")",
            "top": "22px",
            "width": "40px",
            "zIndex": 1
        }, controller.args[0], "lblLeadName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadName"), extendConfig({}, controller.args[2], "lblLeadName"));
        var flxLeadNameSort = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxLeadNameSort",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "22px",
            "width": "20px"
        }, controller.args[0], "flxLeadNameSort"), extendConfig({}, controller.args[1], "flxLeadNameSort"), extendConfig({}, controller.args[2], "flxLeadNameSort"));
        flxLeadNameSort.setDefaultUnit(kony.flex.DP);
        var fonticonLeadNameSort = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "fonticonLeadNameSort",
            "isVisible": true,
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonLeadNameSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonLeadNameSort"), extendConfig({}, controller.args[2], "fonticonLeadNameSort"));
        flxLeadNameSort.add(fonticonLeadNameSort);
        flxLeadName.add(lblLeadName, flxLeadNameSort);
        var flxLeadPhone = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxLeadPhone",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "19.60%",
            "zIndex": 1
        }, controller.args[0], "flxLeadPhone"), extendConfig({}, controller.args[1], "flxLeadPhone"), extendConfig({}, controller.args[2], "flxLeadPhone"));
        flxLeadPhone.setDefaultUnit(kony.flex.DP);
        var lblLeadPhone = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblLeadPhone",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLeadViewHeader",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.segmentheaderPhone\")",
            "top": "22px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadPhone"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadPhone"), extendConfig({}, controller.args[2], "lblLeadPhone"));
        var flxLeadPhoneSort = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxLeadPhoneSort",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "22px",
            "width": "20px"
        }, controller.args[0], "flxLeadPhoneSort"), extendConfig({}, controller.args[1], "flxLeadPhoneSort"), extendConfig({}, controller.args[2], "flxLeadPhoneSort"));
        flxLeadPhoneSort.setDefaultUnit(kony.flex.DP);
        var fonticonLeadPhoneSort = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "fonticonLeadPhoneSort",
            "isVisible": true,
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonLeadPhoneSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonLeadPhoneSort"), extendConfig({}, controller.args[2], "fonticonLeadPhoneSort"));
        flxLeadPhoneSort.add(fonticonLeadPhoneSort);
        flxLeadPhone.add(lblLeadPhone, flxLeadPhoneSort);
        var flxLeadEmail = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxLeadEmail",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "21.80%",
            "zIndex": 1
        }, controller.args[0], "flxLeadEmail"), extendConfig({}, controller.args[1], "flxLeadEmail"), extendConfig({}, controller.args[2], "flxLeadEmail"));
        flxLeadEmail.setDefaultUnit(kony.flex.DP);
        var lblLeadEmail = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblLeadEmail",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLeadViewHeader",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.segmentheaderEmail\")",
            "top": "22px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadEmail"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadEmail"), extendConfig({}, controller.args[2], "lblLeadEmail"));
        var flxLeadEmailSort = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxLeadEmailSort",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "22px",
            "width": "20px"
        }, controller.args[0], "flxLeadEmailSort"), extendConfig({}, controller.args[1], "flxLeadEmailSort"), extendConfig({}, controller.args[2], "flxLeadEmailSort"));
        flxLeadEmailSort.setDefaultUnit(kony.flex.DP);
        var fonticonLeadEmailSort = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "fonticonLeadEmailSort",
            "isVisible": true,
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonLeadEmailSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonLeadEmailSort"), extendConfig({}, controller.args[2], "fonticonLeadEmailSort"));
        flxLeadEmailSort.add(fonticonLeadEmailSort);
        flxLeadEmail.add(lblLeadEmail, flxLeadEmailSort);
        var flxLeadProduct = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxLeadProduct",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "18.80%",
            "zIndex": 1
        }, controller.args[0], "flxLeadProduct"), extendConfig({}, controller.args[1], "flxLeadProduct"), extendConfig({}, controller.args[2], "flxLeadProduct"));
        flxLeadProduct.setDefaultUnit(kony.flex.DP);
        var lblLeadProduct = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblLeadProduct",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLeadViewHeader",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.segmentheaderProduct\")",
            "top": "22px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadProduct"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadProduct"), extendConfig({}, controller.args[2], "lblLeadProduct"));
        var flxLeadProductSort = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxLeadProductSort",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "22px",
            "width": "20px"
        }, controller.args[0], "flxLeadProductSort"), extendConfig({}, controller.args[1], "flxLeadProductSort"), extendConfig({}, controller.args[2], "flxLeadProductSort"));
        flxLeadProductSort.setDefaultUnit(kony.flex.DP);
        var fonticonLeadProductSort = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "fonticonLeadProductSort",
            "isVisible": true,
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonLeadProductSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonLeadProductSort"), extendConfig({}, controller.args[2], "fonticonLeadProductSort"));
        flxLeadProductSort.add(fonticonLeadProductSort);
        flxLeadProduct.add(lblLeadProduct, flxLeadProductSort);
        var flxLeadAssignedTo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxLeadAssignedTo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "21.80%",
            "zIndex": 1
        }, controller.args[0], "flxLeadAssignedTo"), extendConfig({}, controller.args[1], "flxLeadAssignedTo"), extendConfig({}, controller.args[2], "flxLeadAssignedTo"));
        flxLeadAssignedTo.setDefaultUnit(kony.flex.DP);
        var lblLeadAssignedTo = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblLeadAssignedTo",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLeadViewHeader",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.segmentheaderAssignedTo\")",
            "top": "22px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadAssignedTo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadAssignedTo"), extendConfig({}, controller.args[2], "lblLeadAssignedTo"));
        var flxLeadAssignedToSort = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxLeadAssignedToSort",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "22px",
            "width": "20px"
        }, controller.args[0], "flxLeadAssignedToSort"), extendConfig({}, controller.args[1], "flxLeadAssignedToSort"), extendConfig({}, controller.args[2], "flxLeadAssignedToSort"));
        flxLeadAssignedToSort.setDefaultUnit(kony.flex.DP);
        var fonticonLeadAssignedToSort = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "fonticonLeadAssignedToSort",
            "isVisible": true,
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonLeadAssignedToSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonLeadAssignedToSort"), extendConfig({}, controller.args[2], "fonticonLeadAssignedToSort"));
        flxLeadAssignedToSort.add(fonticonLeadAssignedToSort);
        flxLeadAssignedTo.add(lblLeadAssignedTo, flxLeadAssignedToSort);
        var flxLeadModifiedOn = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxLeadModifiedOn",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "14%",
            "zIndex": 1
        }, controller.args[0], "flxLeadModifiedOn"), extendConfig({}, controller.args[1], "flxLeadModifiedOn"), extendConfig({}, controller.args[2], "flxLeadModifiedOn"));
        flxLeadModifiedOn.setDefaultUnit(kony.flex.DP);
        var lblLeadModifiedOn = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblLeadModifiedOn",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLeadViewHeader",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.segmentheaderModifiedOn\")",
            "top": "22px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeadModifiedOn"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeadModifiedOn"), extendConfig({}, controller.args[2], "lblLeadModifiedOn"));
        var flxLeadModifiedOnSort = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxLeadModifiedOnSort",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "22px",
            "width": "20px"
        }, controller.args[0], "flxLeadModifiedOnSort"), extendConfig({}, controller.args[1], "flxLeadModifiedOnSort"), extendConfig({}, controller.args[2], "flxLeadModifiedOnSort"));
        flxLeadModifiedOnSort.setDefaultUnit(kony.flex.DP);
        var fonticonLeadModifiedOnSort = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "fonticonLeadModifiedOnSort",
            "isVisible": true,
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonLeadModifiedOnSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonLeadModifiedOnSort"), extendConfig({}, controller.args[2], "fonticonLeadModifiedOnSort"));
        flxLeadModifiedOnSort.add(fonticonLeadModifiedOnSort);
        flxLeadModifiedOn.add(lblLeadModifiedOn, flxLeadModifiedOnSort);
        flxHeaderLeadMain.add(flxLeadName, flxLeadPhone, flxLeadEmail, flxLeadProduct, flxLeadAssignedTo, flxLeadModifiedOn);
        var flxHeaderLeadContextualMenu = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxHeaderLeadContextualMenu",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "width": "60px",
            "zIndex": 1
        }, controller.args[0], "flxHeaderLeadContextualMenu"), extendConfig({}, controller.args[1], "flxHeaderLeadContextualMenu"), extendConfig({}, controller.args[2], "flxHeaderLeadContextualMenu"));
        flxHeaderLeadContextualMenu.setDefaultUnit(kony.flex.DP);
        flxHeaderLeadContextualMenu.add();
        var lblHeaderLeadSeparator = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1px",
            "id": "lblHeaderLeadSeparator",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknConfigurationBundleSeparator",
            "zIndex": 1
        }, controller.args[0], "lblHeaderLeadSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderLeadSeparator"), extendConfig({}, controller.args[2], "lblHeaderLeadSeparator"));
        flxHeaders.add(flxHeaderLeadCheckbox, flxHeaderLeadType, flxHeaderLeadMain, flxHeaderLeadContextualMenu, lblHeaderLeadSeparator);
        var flxTableView = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "558px",
            "horizontalScrollIndicator": true,
            "id": "flxTableView",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "pagingEnabled": false,
            "right": "0px",
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "60px",
            "verticalScrollIndicator": true,
            "zIndex": 1
        }, controller.args[0], "flxTableView"), extendConfig({}, controller.args[1], "flxTableView"), extendConfig({}, controller.args[2], "flxTableView"));
        flxTableView.setDefaultUnit(kony.flex.DP);
        var segLeads = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "fonticonLeadType": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fonticonCustomer"),
                "imgCheckBox": "checkbox.png",
                "lblIconOptions": "",
                "lblLeadAssignedTo": "C360 Adminstrator k kony",
                "lblLeadEmail": "jasneetpal.kaur@kony.com",
                "lblLeadModifiedOn": "Modified on",
                "lblLeadName": "Jasneet Pal Kaur",
                "lblLeadPhone": "+123-1234567890-123",
                "lblLeadProduct": "Freedom credit card",
                "lblSeparator": "Label"
            }, {
                "fonticonLeadType": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fonticonCustomer"),
                "imgCheckBox": "checkbox.png",
                "lblIconOptions": "",
                "lblLeadAssignedTo": "C360 Adminstrator k kony",
                "lblLeadEmail": "jasneetpal.kaur@kony.com",
                "lblLeadModifiedOn": "Modified on",
                "lblLeadName": "Jasneet Pal Kaur",
                "lblLeadPhone": "+123-1234567890-123",
                "lblLeadProduct": "Freedom credit card",
                "lblSeparator": "Label"
            }, {
                "fonticonLeadType": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fonticonCustomer"),
                "imgCheckBox": "checkbox.png",
                "lblIconOptions": "",
                "lblLeadAssignedTo": "C360 Adminstrator k kony",
                "lblLeadEmail": "jasneetpal.kaur@kony.com",
                "lblLeadModifiedOn": "Modified on",
                "lblLeadName": "Jasneet Pal Kaur",
                "lblLeadPhone": "+123-1234567890-123",
                "lblLeadProduct": "Freedom credit card",
                "lblSeparator": "Label"
            }, {
                "fonticonLeadType": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fonticonCustomer"),
                "imgCheckBox": "checkbox.png",
                "lblIconOptions": "",
                "lblLeadAssignedTo": "C360 Adminstrator k kony",
                "lblLeadEmail": "jasneetpal.kaur@kony.com",
                "lblLeadModifiedOn": "Modified on",
                "lblLeadName": "Jasneet Pal Kaur",
                "lblLeadPhone": "+123-1234567890-123",
                "lblLeadProduct": "Freedom credit card",
                "lblSeparator": "Label"
            }, {
                "fonticonLeadType": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fonticonCustomer"),
                "imgCheckBox": "checkbox.png",
                "lblIconOptions": "",
                "lblLeadAssignedTo": "C360 Adminstrator k kony",
                "lblLeadEmail": "jasneetpal.kaur@kony.com",
                "lblLeadModifiedOn": "Modified on",
                "lblLeadName": "Jasneet Pal Kaur",
                "lblLeadPhone": "+123-1234567890-123",
                "lblLeadProduct": "Freedom credit card",
                "lblSeparator": "Label"
            }, {
                "fonticonLeadType": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fonticonCustomer"),
                "imgCheckBox": "checkbox.png",
                "lblIconOptions": "",
                "lblLeadAssignedTo": "C360 Adminstrator k kony",
                "lblLeadEmail": "jasneetpal.kaur@kony.com",
                "lblLeadModifiedOn": "Modified on",
                "lblLeadName": "Jasneet Pal Kaur",
                "lblLeadPhone": "+123-1234567890-123",
                "lblLeadProduct": "Freedom credit card",
                "lblSeparator": "Label"
            }, {
                "fonticonLeadType": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fonticonCustomer"),
                "imgCheckBox": "checkbox.png",
                "lblIconOptions": "",
                "lblLeadAssignedTo": "C360 Adminstrator k kony",
                "lblLeadEmail": "jasneetpal.kaur@kony.com",
                "lblLeadModifiedOn": "Modified on",
                "lblLeadName": "Jasneet Pal Kaur",
                "lblLeadPhone": "+123-1234567890-123",
                "lblLeadProduct": "Freedom credit card",
                "lblSeparator": "Label"
            }, {
                "fonticonLeadType": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fonticonCustomer"),
                "imgCheckBox": "checkbox.png",
                "lblIconOptions": "",
                "lblLeadAssignedTo": "C360 Adminstrator k kony",
                "lblLeadEmail": "jasneetpal.kaur@kony.com",
                "lblLeadModifiedOn": "Modified on",
                "lblLeadName": "Jasneet Pal Kaur",
                "lblLeadPhone": "+123-1234567890-123",
                "lblLeadProduct": "Freedom credit card",
                "lblSeparator": "Label"
            }, {
                "fonticonLeadType": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fonticonCustomer"),
                "imgCheckBox": "checkbox.png",
                "lblIconOptions": "",
                "lblLeadAssignedTo": "C360 Adminstrator k kony",
                "lblLeadEmail": "jasneetpal.kaur@kony.com",
                "lblLeadModifiedOn": "Modified on",
                "lblLeadName": "Jasneet Pal Kaur",
                "lblLeadPhone": "+123-1234567890-123",
                "lblLeadProduct": "Freedom credit card",
                "lblSeparator": "Label"
            }, {
                "fonticonLeadType": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fonticonCustomer"),
                "imgCheckBox": "checkbox.png",
                "lblIconOptions": "",
                "lblLeadAssignedTo": "C360 Adminstrator k kony",
                "lblLeadEmail": "jasneetpal.kaur@kony.com",
                "lblLeadModifiedOn": "Modified on",
                "lblLeadName": "Jasneet Pal Kaur",
                "lblLeadPhone": "+123-1234567890-123",
                "lblLeadProduct": "Freedom credit card",
                "lblSeparator": "Label"
            }],
            "groupCells": false,
            "id": "segLeads",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxLeads",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxLeadAssignedTo": "flxLeadAssignedTo",
                "flxLeadCheckbox": "flxLeadCheckbox",
                "flxLeadEmail": "flxLeadEmail",
                "flxLeadMain": "flxLeadMain",
                "flxLeadModifiedOn": "flxLeadModifiedOn",
                "flxLeadName": "flxLeadName",
                "flxLeadOptions": "flxLeadOptions",
                "flxLeadPhone": "flxLeadPhone",
                "flxLeadProduct": "flxLeadProduct",
                "flxLeadType": "flxLeadType",
                "flxLeads": "flxLeads",
                "flxOptions": "flxOptions",
                "fonticonLeadType": "fonticonLeadType",
                "imgCheckBox": "imgCheckBox",
                "lblIconOptions": "lblIconOptions",
                "lblLeadAssignedTo": "lblLeadAssignedTo",
                "lblLeadEmail": "lblLeadEmail",
                "lblLeadModifiedOn": "lblLeadModifiedOn",
                "lblLeadName": "lblLeadName",
                "lblLeadPhone": "lblLeadPhone",
                "lblLeadProduct": "lblLeadProduct",
                "lblSeparator": "lblSeparator"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segLeads"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segLeads"), extendConfig({}, controller.args[2], "segLeads"));
        flxTableView.add(segLeads);
        var flxNoRecordsFound = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200dp",
            "id": "flxNoRecordsFound",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoRecordsFound"), extendConfig({}, controller.args[1], "flxNoRecordsFound"), extendConfig({}, controller.args[2], "flxNoRecordsFound"));
        flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
        var rtxNoRecords = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxNoRecords",
            "isVisible": true,
            "skin": "sknRtxLato84939e12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxNoRecords"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxNoRecords"), extendConfig({}, controller.args[2], "rtxNoRecords"));
        flxNoRecordsFound.add(rtxNoRecords);
        viewLeads.add(flxHeaders, flxTableView, flxNoRecordsFound);
        return viewLeads;
    }
})