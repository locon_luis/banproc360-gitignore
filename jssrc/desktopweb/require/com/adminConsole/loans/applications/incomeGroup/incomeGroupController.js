define("com/adminConsole/loans/applications/incomeGroup/userincomeGroupController", function() {
    return {};
});
define("com/adminConsole/loans/applications/incomeGroup/incomeGroupControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/loans/applications/incomeGroup/incomeGroupController", ["com/adminConsole/loans/applications/incomeGroup/userincomeGroupController", "com/adminConsole/loans/applications/incomeGroup/incomeGroupControllerActions"], function() {
    var controller = require("com/adminConsole/loans/applications/incomeGroup/userincomeGroupController");
    var actions = require("com/adminConsole/loans/applications/incomeGroup/incomeGroupControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
