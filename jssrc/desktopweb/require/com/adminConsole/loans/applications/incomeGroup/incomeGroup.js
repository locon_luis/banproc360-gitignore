define(function() {
    return function(controller) {
        var incomeGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "incomeGroup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "incomeGroup"), extendConfig({}, controller.args[1], "incomeGroup"), extendConfig({}, controller.args[2], "incomeGroup"));
        incomeGroup.setDefaultUnit(kony.flex.DP);
        var flxIncomeInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncomeInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxIncomeInfo"), extendConfig({}, controller.args[1], "flxIncomeInfo"), extendConfig({}, controller.args[2], "flxIncomeInfo"));
        flxIncomeInfo.setDefaultUnit(kony.flex.DP);
        var flxIncomeType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncomeType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxIncomeType"), extendConfig({}, controller.args[1], "flxIncomeType"), extendConfig({}, controller.args[2], "flxIncomeType"));
        flxIncomeType.setDefaultUnit(kony.flex.DP);
        var lblIncomeType = new kony.ui.Label(extendConfig({
            "id": "lblIncomeType",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.TypeOfIncomeCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIncomeType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIncomeType"), extendConfig({}, controller.args[2], "lblIncomeType"));
        var IncomeTypeValue = new kony.ui.Label(extendConfig({
            "id": "IncomeTypeValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Type of Income",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "IncomeTypeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "IncomeTypeValue"), extendConfig({}, controller.args[2], "IncomeTypeValue"));
        flxIncomeType.add(lblIncomeType, IncomeTypeValue);
        var flxGrossIncome = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGrossIncome",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncome"), extendConfig({}, controller.args[1], "flxGrossIncome"), extendConfig({}, controller.args[2], "flxGrossIncome"));
        flxGrossIncome.setDefaultUnit(kony.flex.DP);
        var lblGrossIncome = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncome",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.GrossIncomeCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGrossIncome"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncome"), extendConfig({}, controller.args[2], "lblGrossIncome"));
        var lblGrossIncomeValue = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncomeValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Gross Income",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblGrossIncomeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncomeValue"), extendConfig({}, controller.args[2], "lblGrossIncomeValue"));
        flxGrossIncome.add(lblGrossIncome, lblGrossIncomeValue);
        var flxPayPeriod = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPayPeriod",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPayPeriod"), extendConfig({}, controller.args[1], "flxPayPeriod"), extendConfig({}, controller.args[2], "flxPayPeriod"));
        flxPayPeriod.setDefaultUnit(kony.flex.DP);
        var lblPayPeriod = new kony.ui.Label(extendConfig({
            "id": "lblPayPeriod",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PayPeriodCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPayPeriod"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPayPeriod"), extendConfig({}, controller.args[2], "lblPayPeriod"));
        var lblPayPeriodValue = new kony.ui.Label(extendConfig({
            "id": "lblPayPeriodValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Pay Period",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPayPeriodValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPayPeriodValue"), extendConfig({}, controller.args[2], "lblPayPeriodValue"));
        flxPayPeriod.add(lblPayPeriod, lblPayPeriodValue);
        flxIncomeInfo.add(flxIncomeType, flxGrossIncome, flxPayPeriod);
        var flxDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescription",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDescription"), extendConfig({}, controller.args[1], "flxDescription"), extendConfig({}, controller.args[2], "flxDescription"));
        flxDescription.setDefaultUnit(kony.flex.DP);
        var lblDescription = new kony.ui.Label(extendConfig({
            "id": "lblDescription",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.DescriptionCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        var lblDescriptionValue = new kony.ui.Label(extendConfig({
            "id": "lblDescriptionValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Income details of other source of income with in 200 words ,Income details of other source of income with in 200 words, income details of other source of income with in 200 words , Income details of other source of income with in 200 words",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblDescriptionValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescriptionValue"), extendConfig({}, controller.args[2], "lblDescriptionValue"));
        flxDescription.add(lblDescription, lblDescriptionValue);
        incomeGroup.add(flxIncomeInfo, flxDescription);
        return incomeGroup;
    }
})