define(function() {
    return function(controller) {
        var editEmployementInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "editEmployementInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "editEmployementInfo"), extendConfig({}, controller.args[1], "editEmployementInfo"), extendConfig({}, controller.args[2], "editEmployementInfo"));
        editEmployementInfo.setDefaultUnit(kony.flex.DP);
        var flxSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxSection"), extendConfig({}, controller.args[1], "flxSection"), extendConfig({}, controller.args[2], "flxSection"));
        flxSection.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.IncomeSource\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxEmploymentStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxEmploymentStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 20,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxEmploymentStatus"), extendConfig({}, controller.args[1], "flxEmploymentStatus"), extendConfig({}, controller.args[2], "flxEmploymentStatus"));
        flxEmploymentStatus.setDefaultUnit(kony.flex.DP);
        var lblEmploymentStatus = new kony.ui.Label(extendConfig({
            "id": "lblEmploymentStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.EmploymentStatusHeader\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmploymentStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmploymentStatus"), extendConfig({}, controller.args[2], "lblEmploymentStatus"));
        var lstEmploymentStatusValue = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstEmploymentStatusValue",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["Select", "Select"],
                ["Full Time Employed", "Full Time Employed "],
                ["Part Time Employed", "Part Time Employed "],
                ["Self Employed", "Self Employed"],
                ["Military", "Military "],
                ["Unemployed", "Unemployed"],
                ["Other", "Other "]
            ],
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstEmploymentStatusValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstEmploymentStatusValue"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstEmploymentStatusValue"));
        var employmentStatusErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "employmentStatusErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "employmentStatusErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "employmentStatusErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "employmentStatusErrorMsg"));
        flxEmploymentStatus.add(lblEmploymentStatus, lstEmploymentStatusValue, employmentStatusErrorMsg);
        var flxPresentEmployer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPresentEmployer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxPresentEmployer"), extendConfig({}, controller.args[1], "flxPresentEmployer"), extendConfig({}, controller.args[2], "flxPresentEmployer"));
        flxPresentEmployer.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader1 = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "loansSectionHeader1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PresentEmployerDetailsHeader\")"
                },
                "loansSectionHeader": {
                    "top": "viz.val_cleared"
                }
            }
        }, controller.args[0], "loansSectionHeader1"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader1"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader1"));
        var flxEmploymentDtls = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmploymentDtls",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 20,
            "isModalContainer": false,
            "right": 20,
            "skin": "slFbox",
            "top": "42dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxEmploymentDtls"), extendConfig({}, controller.args[1], "flxEmploymentDtls"), extendConfig({}, controller.args[2], "flxEmploymentDtls"));
        flxEmploymentDtls.setDefaultUnit(kony.flex.DP);
        var flxFulltimeEmployment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFulltimeEmployment",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxFulltimeEmployment"), extendConfig({}, controller.args[1], "flxFulltimeEmployment"), extendConfig({}, controller.args[2], "flxFulltimeEmployment"));
        flxFulltimeEmployment.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxEmployerName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmployerName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "66%",
            "zIndex": 1
        }, controller.args[0], "flxEmployerName"), extendConfig({}, controller.args[1], "flxEmployerName"), extendConfig({}, controller.args[2], "flxEmployerName"));
        flxEmployerName.setDefaultUnit(kony.flex.DP);
        var flxEmployerLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmployerLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxEmployerLabel"), extendConfig({}, controller.args[1], "flxEmployerLabel"), extendConfig({}, controller.args[2], "flxEmployerLabel"));
        flxEmployerLabel.setDefaultUnit(kony.flex.DP);
        var lblEmployerName = new kony.ui.Label(extendConfig({
            "id": "lblEmployerName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.EmployerName\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmployerName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmployerName"), extendConfig({}, controller.args[2], "lblEmployerName"));
        var lblEmployerNameSize = new kony.ui.Label(extendConfig({
            "id": "lblEmployerNameSize",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/70",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1,
            "blur": {
                "enabled": true,
                "value": 0
            }
        }, controller.args[0], "lblEmployerNameSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmployerNameSize"), extendConfig({}, controller.args[2], "lblEmployerNameSize"));
        flxEmployerLabel.add(lblEmployerName, lblEmployerNameSize);
        var txtEmployerNameValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtEmployerNameValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 70,
            "placeholder": "Employer Name",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtEmployerNameValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtEmployerNameValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtEmployerNameValue"));
        var employerNameErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "employerNameErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "employerNameErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "employerNameErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "employerNameErrorMsg"));
        flxEmployerName.add(flxEmployerLabel, txtEmployerNameValue, employerNameErrorMsg);
        var flxDesignation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDesignation",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxDesignation"), extendConfig({}, controller.args[1], "flxDesignation"), extendConfig({}, controller.args[2], "flxDesignation"));
        flxDesignation.setDefaultUnit(kony.flex.DP);
        var flxDesignationLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDesignationLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDesignationLabel"), extendConfig({}, controller.args[1], "flxDesignationLabel"), extendConfig({}, controller.args[2], "flxDesignationLabel"));
        flxDesignationLabel.setDefaultUnit(kony.flex.DP);
        var lblDesignation = new kony.ui.Label(extendConfig({
            "id": "lblDesignation",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Designation\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDesignation"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDesignation"), extendConfig({}, controller.args[2], "lblDesignation"));
        var lblDesignationSize = new kony.ui.Label(extendConfig({
            "id": "lblDesignationSize",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/70",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblDesignationSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDesignationSize"), extendConfig({}, controller.args[2], "lblDesignationSize"));
        flxDesignationLabel.add(lblDesignation, lblDesignationSize);
        var txtDesignationValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtDesignationValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 70,
            "placeholder": "Designation",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtDesignationValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtDesignationValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtDesignationValue"));
        var designationErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "designationErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "designationErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "designationErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "designationErrorMsg"));
        flxDesignation.add(flxDesignationLabel, txtDesignationValue, designationErrorMsg);
        flxRow1.add(flxEmployerName, flxDesignation);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxEmpStartDate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmpStartDate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxEmpStartDate"), extendConfig({}, controller.args[1], "flxEmpStartDate"), extendConfig({}, controller.args[2], "flxEmpStartDate"));
        flxEmpStartDate.setDefaultUnit(kony.flex.DP);
        var lblStartDate = new kony.ui.Label(extendConfig({
            "id": "lblStartDate",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.StartDate\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStartDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDate"), extendConfig({}, controller.args[2], "lblStartDate"));
        var flxStartDateValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxStartDateValue",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxStartDateValue"), extendConfig({}, controller.args[1], "flxStartDateValue"), extendConfig({}, controller.args[2], "flxStartDateValue"));
        flxStartDateValue.setDefaultUnit(kony.flex.DP);
        flxStartDateValue.add();
        var txtStartDateValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtStartDateValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 10,
            "placeholder": "MM/DD/YYYY",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtStartDateValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtStartDateValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtStartDateValue"));
        var startDateErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "startDateErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "startDateErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "startDateErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "startDateErrorMsg"));
        flxEmpStartDate.add(lblStartDate, flxStartDateValue, txtStartDateValue, startDateErrorMsg);
        var flxGrossIncome = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGrossIncome",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncome"), extendConfig({}, controller.args[1], "flxGrossIncome"), extendConfig({}, controller.args[2], "flxGrossIncome"));
        flxGrossIncome.setDefaultUnit(kony.flex.DP);
        var lblGrossIncome = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncome",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.GrossIncome\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGrossIncome"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncome"), extendConfig({}, controller.args[2], "lblGrossIncome"));
        var flxGrossIncomeAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxGrossIncomeAmount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncomeAmount"), extendConfig({}, controller.args[1], "flxGrossIncomeAmount"), extendConfig({}, controller.args[2], "flxGrossIncomeAmount"));
        flxGrossIncomeAmount.setDefaultUnit(kony.flex.DP);
        var lblCurrency = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblCurrency",
            "isVisible": true,
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DollarSymbol\")",
            "width": "20%",
            "zIndex": 10
        }, controller.args[0], "lblCurrency"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrency"), extendConfig({}, controller.args[2], "lblCurrency"));
        var txtGrossIncomeValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtGrossIncomeValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "txtGrossIncomeValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtGrossIncomeValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtGrossIncomeValue"));
        flxGrossIncomeAmount.add(lblCurrency, txtGrossIncomeValue);
        var grossIncomeErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "grossIncomeErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "grossIncomeErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "grossIncomeErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "grossIncomeErrorMsg"));
        flxGrossIncome.add(lblGrossIncome, flxGrossIncomeAmount, grossIncomeErrorMsg);
        var flxPayPeriod = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxPayPeriod",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPayPeriod"), extendConfig({}, controller.args[1], "flxPayPeriod"), extendConfig({}, controller.args[2], "flxPayPeriod"));
        flxPayPeriod.setDefaultUnit(kony.flex.DP);
        var lblPayPeriod = new kony.ui.Label(extendConfig({
            "id": "lblPayPeriod",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PayPeriod\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPayPeriod"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPayPeriod"), extendConfig({}, controller.args[2], "lblPayPeriod"));
        var lstPayPeriodValue = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstPayPeriodValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstPayPeriodValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstPayPeriodValue"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstPayPeriodValue"));
        var payPeriodErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "payPeriodErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "payPeriodErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "payPeriodErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "payPeriodErrorMsg"));
        flxPayPeriod.add(lblPayPeriod, lstPayPeriodValue, payPeriodErrorMsg);
        flxRow2.add(flxEmpStartDate, flxGrossIncome, flxPayPeriod);
        var flxRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRow3"), extendConfig({}, controller.args[1], "flxRow3"), extendConfig({}, controller.args[2], "flxRow3"));
        flxRow3.setDefaultUnit(kony.flex.DP);
        var flxWorkHours = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxWorkHours",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxWorkHours"), extendConfig({}, controller.args[1], "flxWorkHours"), extendConfig({}, controller.args[2], "flxWorkHours"));
        flxWorkHours.setDefaultUnit(kony.flex.DP);
        var lblWorkHours = new kony.ui.Label(extendConfig({
            "id": "lblWorkHours",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.TotalWorkingHours\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblWorkHours"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblWorkHours"), extendConfig({}, controller.args[2], "lblWorkHours"));
        var flxWorkHoursAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxWorkHoursAmount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxWorkHoursAmount"), extendConfig({}, controller.args[1], "flxWorkHoursAmount"), extendConfig({}, controller.args[2], "flxWorkHoursAmount"));
        flxWorkHoursAmount.setDefaultUnit(kony.flex.DP);
        var txtWorkHoursValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtWorkHoursValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Hours\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "txtWorkHoursValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtWorkHoursValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtWorkHoursValue"));
        var lblUnits = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblUnits",
            "isVisible": true,
            "left": "0",
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.HoursPerWeek\")",
            "top": "0",
            "width": "40%",
            "zIndex": 10
        }, controller.args[0], "lblUnits"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUnits"), extendConfig({}, controller.args[2], "lblUnits"));
        flxWorkHoursAmount.add(txtWorkHoursValue, lblUnits);
        var workHoursErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "workHoursErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "workHoursErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "workHoursErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "workHoursErrorMsg"));
        flxWorkHours.add(lblWorkHours, flxWorkHoursAmount, workHoursErrorMsg);
        flxRow3.add(flxWorkHours);
        var flxAddressInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddressInfo"), extendConfig({}, controller.args[1], "flxAddressInfo"), extendConfig({}, controller.args[2], "flxAddressInfo"));
        flxAddressInfo.setDefaultUnit(kony.flex.DP);
        var flxAddressLine1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressLine1"), extendConfig({}, controller.args[1], "flxAddressLine1"), extendConfig({}, controller.args[2], "flxAddressLine1"));
        flxAddressLine1.setDefaultUnit(kony.flex.DP);
        var flxAddressLine1Label = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine1Label",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddressLine1Label"), extendConfig({}, controller.args[1], "flxAddressLine1Label"), extendConfig({}, controller.args[2], "flxAddressLine1Label"));
        flxAddressLine1Label.setDefaultUnit(kony.flex.DP);
        var lblAddressLine1 = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AddressLine1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressLine1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine1"), extendConfig({}, controller.args[2], "lblAddressLine1"));
        var lblAddressLine1Size = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine1Size",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/40",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblAddressLine1Size"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine1Size"), extendConfig({}, controller.args[2], "lblAddressLine1Size"));
        flxAddressLine1Label.add(lblAddressLine1, lblAddressLine1Size);
        var txtAddressLine1Value = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtAddressLine1Value",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 40,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine1User\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAddressLine1Value"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtAddressLine1Value"), extendConfig({
            "autoCorrect": false,
            "onKeyUp": controller.AS_TextField_af0d9ef3e8e54611a16523018c8af23c
        }, controller.args[2], "txtAddressLine1Value"));
        var addressLine1ErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "addressLine1ErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "addressLine1ErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "addressLine1ErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "addressLine1ErrorMsg"));
        flxAddressLine1.add(flxAddressLine1Label, txtAddressLine1Value, addressLine1ErrorMsg);
        var flxAddressLine2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressLine2"), extendConfig({}, controller.args[1], "flxAddressLine2"), extendConfig({}, controller.args[2], "flxAddressLine2"));
        flxAddressLine2.setDefaultUnit(kony.flex.DP);
        var flxAddressLine2Label = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine2Label",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddressLine2Label"), extendConfig({}, controller.args[1], "flxAddressLine2Label"), extendConfig({}, controller.args[2], "flxAddressLine2Label"));
        flxAddressLine2Label.setDefaultUnit(kony.flex.DP);
        var lblAddressLine2 = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine2",
            "isVisible": true,
            "left": 0,
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AddressLine2\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressLine2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine2"), extendConfig({}, controller.args[2], "lblAddressLine2"));
        var lblAddressLine2Size = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine2Size",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/40",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblAddressLine2Size"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine2Size"), extendConfig({}, controller.args[2], "lblAddressLine2Size"));
        flxAddressLine2Label.add(lblAddressLine2, lblAddressLine2Size);
        var txtAddressLine2Value = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtAddressLine2Value",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 40,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine2User\")",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAddressLine2Value"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtAddressLine2Value"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtAddressLine2Value"));
        var addressLine2ErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "addressLine2ErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "addressLine2ErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "addressLine2ErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "addressLine2ErrorMsg"));
        flxAddressLine2.add(flxAddressLine2Label, txtAddressLine2Value, addressLine2ErrorMsg);
        var flxAddressSection3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressSection3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressSection3"), extendConfig({}, controller.args[1], "flxAddressSection3"), extendConfig({}, controller.args[2], "flxAddressSection3"));
        flxAddressSection3.setDefaultUnit(kony.flex.DP);
        var flxAddressCountry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCountry",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCountry"), extendConfig({}, controller.args[1], "flxAddressCountry"), extendConfig({}, controller.args[2], "flxAddressCountry"));
        flxAddressCountry.setDefaultUnit(kony.flex.DP);
        var lblAddress1Country = new kony.ui.Label(extendConfig({
            "id": "lblAddress1Country",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Country\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddress1Country"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddress1Country"), extendConfig({}, controller.args[2], "lblAddress1Country"));
        var txtCountryValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtCountryValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtCountryValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCountryValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtCountryValue"));
        var currentCountryErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "currentCountryErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentCountryErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentCountryErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentCountryErrorMsg"));
        flxAddressCountry.add(lblAddress1Country, txtCountryValue, currentCountryErrorMsg);
        var flxAddressState = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressState",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressState"), extendConfig({}, controller.args[1], "flxAddressState"), extendConfig({}, controller.args[2], "flxAddressState"));
        flxAddressState.setDefaultUnit(kony.flex.DP);
        var lblAddressState = new kony.ui.Label(extendConfig({
            "id": "lblAddressState",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.State\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressState"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressState"), extendConfig({}, controller.args[2], "lblAddressState"));
        var txtStateValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtStateValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtStateValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtStateValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtStateValue"));
        var currentStateErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "currentStateErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentStateErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentStateErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentStateErrorMsg"));
        flxAddressState.add(lblAddressState, txtStateValue, currentStateErrorMsg);
        var flxAddressCity = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCity",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCity"), extendConfig({}, controller.args[1], "flxAddressCity"), extendConfig({}, controller.args[2], "flxAddressCity"));
        flxAddressCity.setDefaultUnit(kony.flex.DP);
        var lblAddressCity = new kony.ui.Label(extendConfig({
            "id": "lblAddressCity",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.City\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressCity"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressCity"), extendConfig({}, controller.args[2], "lblAddressCity"));
        var txtCityValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtCityValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCityUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "txtCityValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCityValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtCityValue"));
        var currentCityErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "currentCityErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentCityErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentCityErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentCityErrorMsg"));
        flxAddressCity.add(lblAddressCity, txtCityValue, currentCityErrorMsg);
        flxAddressSection3.add(flxAddressCountry, flxAddressState, flxAddressCity);
        var flxAddressSection4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressSection4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAddressSection4"), extendConfig({}, controller.args[1], "flxAddressSection4"), extendConfig({}, controller.args[2], "flxAddressSection4"));
        flxAddressSection4.setDefaultUnit(kony.flex.DP);
        var flxAddressZipCode = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressZipCode",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressZipCode"), extendConfig({}, controller.args[1], "flxAddressZipCode"), extendConfig({}, controller.args[2], "flxAddressZipCode"));
        flxAddressZipCode.setDefaultUnit(kony.flex.DP);
        var lblAddressZipCode = new kony.ui.Label(extendConfig({
            "id": "lblAddressZipCode",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.zip\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressZipCode"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressZipCode"), extendConfig({}, controller.args[2], "lblAddressZipCode"));
        var txtZipCodeValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtZipCodeValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblPostalCodeUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtZipCodeValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtZipCodeValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtZipCodeValue"));
        var currentZipCodeErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "currentZipCodeErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentZipCodeErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentZipCodeErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentZipCodeErrorMsg"));
        flxAddressZipCode.add(lblAddressZipCode, txtZipCodeValue, currentZipCodeErrorMsg);
        flxAddressSection4.add(flxAddressZipCode);
        var flxAddressValidation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAddressValidation",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxf9f9f9NoBorder",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAddressValidation"), extendConfig({}, controller.args[1], "flxAddressValidation"), extendConfig({}, controller.args[2], "flxAddressValidation"));
        flxAddressValidation.setDefaultUnit(kony.flex.DP);
        var flxErrorIconConatiner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxErrorIconConatiner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "44%"
        }, controller.args[0], "flxErrorIconConatiner"), extendConfig({}, controller.args[1], "flxErrorIconConatiner"), extendConfig({}, controller.args[2], "flxErrorIconConatiner"));
        flxErrorIconConatiner.setDefaultUnit(kony.flex.DP);
        var lblUspsRecommendationErrorIcon = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblUspsRecommendationErrorIcon",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "11dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblUspsRecommendationErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUspsRecommendationErrorIcon"), extendConfig({}, controller.args[2], "lblUspsRecommendationErrorIcon"));
        flxErrorIconConatiner.add(lblUspsRecommendationErrorIcon);
        var lblUspsRecommendation = new kony.ui.Label(extendConfig({
            "id": "lblUspsRecommendation",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818A13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.USPSRecommendation\")",
            "top": "11px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUspsRecommendation"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUspsRecommendation"), extendConfig({}, controller.args[2], "lblUspsRecommendation"));
        var flxValidate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxValidate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "11dp",
            "width": "50dp"
        }, controller.args[0], "flxValidate"), extendConfig({}, controller.args[1], "flxValidate"), extendConfig({}, controller.args[2], "flxValidate"));
        flxValidate.setDefaultUnit(kony.flex.DP);
        var lblValidate = new kony.ui.Label(extendConfig({
            "id": "lblValidate",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblHyperLink14PX0a9c098be02ca43",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Validate\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValidate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValidate"), extendConfig({}, controller.args[2], "lblValidate"));
        flxValidate.add(lblValidate);
        flxAddressValidation.add(flxErrorIconConatiner, lblUspsRecommendation, flxValidate);
        flxAddressInfo.add(flxAddressLine1, flxAddressLine2, flxAddressSection3, flxAddressSection4, flxAddressValidation);
        var flxPrevEmpStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxPrevEmpStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxPrevEmpStatus"), extendConfig({}, controller.args[1], "flxPrevEmpStatus"), extendConfig({}, controller.args[2], "flxPrevEmpStatus"));
        flxPrevEmpStatus.setDefaultUnit(kony.flex.DP);
        var lblPrevEmpStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPrevEmpStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Isthereapreviousemployment\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevEmpStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevEmpStatus"), extendConfig({}, controller.args[2], "lblPrevEmpStatus"));
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({}, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var flxRadioButton1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRadioButton1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "55dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton1"), extendConfig({}, controller.args[1], "flxRadioButton1"), extendConfig({}, controller.args[2], "flxRadioButton1"));
        flxRadioButton1.setDefaultUnit(kony.flex.DP);
        var imgRadioButton1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadioButton1",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton1"), extendConfig({}, controller.args[2], "imgRadioButton1"));
        var lblRadioButtonValue1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRadioButtonValue1",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Yes\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue1"), extendConfig({}, controller.args[2], "lblRadioButtonValue1"));
        flxRadioButton1.add(imgRadioButton1, lblRadioButtonValue1);
        var flxRadioButton2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRadioButton2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "55dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton2"), extendConfig({}, controller.args[1], "flxRadioButton2"), extendConfig({}, controller.args[2], "flxRadioButton2"));
        flxRadioButton2.setDefaultUnit(kony.flex.DP);
        var imgRadioButton2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadioButton2",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton2"), extendConfig({}, controller.args[2], "imgRadioButton2"));
        var lblRadioButtonValue2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRadioButtonValue2",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.No\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue2"), extendConfig({}, controller.args[2], "lblRadioButtonValue2"));
        flxRadioButton2.add(imgRadioButton2, lblRadioButtonValue2);
        flxOptions.add(flxRadioButton1, flxRadioButton2);
        flxPrevEmpStatus.add(lblPrevEmpStatus, flxOptions);
        flxFulltimeEmployment.add(flxRow1, flxRow2, flxRow3, flxAddressInfo, flxPrevEmpStatus);
        var flxRowMilitary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRowMilitary",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRowMilitary"), extendConfig({}, controller.args[1], "flxRowMilitary"), extendConfig({}, controller.args[2], "flxRowMilitary"));
        flxRowMilitary.setDefaultUnit(kony.flex.DP);
        var flxDesignationMilitary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDesignationMilitary",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxDesignationMilitary"), extendConfig({}, controller.args[1], "flxDesignationMilitary"), extendConfig({}, controller.args[2], "flxDesignationMilitary"));
        flxDesignationMilitary.setDefaultUnit(kony.flex.DP);
        var flxDesignationLabelM = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDesignationLabelM",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDesignationLabelM"), extendConfig({}, controller.args[1], "flxDesignationLabelM"), extendConfig({}, controller.args[2], "flxDesignationLabelM"));
        flxDesignationLabelM.setDefaultUnit(kony.flex.DP);
        var lblDesignationMilitary = new kony.ui.Label(extendConfig({
            "id": "lblDesignationMilitary",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Designation\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDesignationMilitary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDesignationMilitary"), extendConfig({}, controller.args[2], "lblDesignationMilitary"));
        var lblDesignationSizeMilitary = new kony.ui.Label(extendConfig({
            "id": "lblDesignationSizeMilitary",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/70",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblDesignationSizeMilitary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDesignationSizeMilitary"), extendConfig({}, controller.args[2], "lblDesignationSizeMilitary"));
        flxDesignationLabelM.add(lblDesignationMilitary, lblDesignationSizeMilitary);
        var txtDesignationValueMilitary = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtDesignationValueMilitary",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 70,
            "placeholder": "Designation",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtDesignationValueMilitary"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtDesignationValueMilitary"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtDesignationValueMilitary"));
        var designationErrorMilitaryMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "designationErrorMilitaryMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "designationErrorMilitaryMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "designationErrorMilitaryMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "designationErrorMilitaryMsg"));
        flxDesignationMilitary.add(flxDesignationLabelM, txtDesignationValueMilitary, designationErrorMilitaryMsg);
        var flxEmpStartDateMilitary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmpStartDateMilitary",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxEmpStartDateMilitary"), extendConfig({}, controller.args[1], "flxEmpStartDateMilitary"), extendConfig({}, controller.args[2], "flxEmpStartDateMilitary"));
        flxEmpStartDateMilitary.setDefaultUnit(kony.flex.DP);
        var lblStartDateMilitary = new kony.ui.Label(extendConfig({
            "id": "lblStartDateMilitary",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.StartDate\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStartDateMilitary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDateMilitary"), extendConfig({}, controller.args[2], "lblStartDateMilitary"));
        var flxStartDateValueMilitary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxStartDateValueMilitary",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxStartDateValueMilitary"), extendConfig({}, controller.args[1], "flxStartDateValueMilitary"), extendConfig({}, controller.args[2], "flxStartDateValueMilitary"));
        flxStartDateValueMilitary.setDefaultUnit(kony.flex.DP);
        flxStartDateValueMilitary.add();
        var txtStartDateValueMilitary = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtStartDateValueMilitary",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 10,
            "placeholder": "MM/DD/YYYY",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtStartDateValueMilitary"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtStartDateValueMilitary"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtStartDateValueMilitary"));
        var startDateErrorMilitaryMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "startDateErrorMilitaryMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "startDateErrorMilitaryMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "startDateErrorMilitaryMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "startDateErrorMilitaryMsg"));
        flxEmpStartDateMilitary.add(lblStartDateMilitary, flxStartDateValueMilitary, txtStartDateValueMilitary, startDateErrorMilitaryMsg);
        var flxGrossIncomeMilitary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGrossIncomeMilitary",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncomeMilitary"), extendConfig({}, controller.args[1], "flxGrossIncomeMilitary"), extendConfig({}, controller.args[2], "flxGrossIncomeMilitary"));
        flxGrossIncomeMilitary.setDefaultUnit(kony.flex.DP);
        var lblGrossIncomeMilitary = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncomeMilitary",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.GrossIncome\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGrossIncomeMilitary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncomeMilitary"), extendConfig({}, controller.args[2], "lblGrossIncomeMilitary"));
        var flxGrossIncomeAmountMilitary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxGrossIncomeAmountMilitary",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncomeAmountMilitary"), extendConfig({}, controller.args[1], "flxGrossIncomeAmountMilitary"), extendConfig({}, controller.args[2], "flxGrossIncomeAmountMilitary"));
        flxGrossIncomeAmountMilitary.setDefaultUnit(kony.flex.DP);
        var lblCurrencyMilitary = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblCurrencyMilitary",
            "isVisible": true,
            "left": "0",
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DollarSymbol\")",
            "top": "0",
            "width": "20%",
            "zIndex": 10
        }, controller.args[0], "lblCurrencyMilitary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencyMilitary"), extendConfig({}, controller.args[2], "lblCurrencyMilitary"));
        var txtGrossIncomeValueMilitary = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtGrossIncomeValueMilitary",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "txtGrossIncomeValueMilitary"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtGrossIncomeValueMilitary"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtGrossIncomeValueMilitary"));
        flxGrossIncomeAmountMilitary.add(lblCurrencyMilitary, txtGrossIncomeValueMilitary);
        var grossIncomeErrorMilitaryMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "grossIncomeErrorMilitaryMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "grossIncomeErrorMilitaryMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "grossIncomeErrorMilitaryMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "grossIncomeErrorMilitaryMsg"));
        flxGrossIncomeMilitary.add(lblGrossIncomeMilitary, flxGrossIncomeAmountMilitary, grossIncomeErrorMilitaryMsg);
        flxRowMilitary.add(flxDesignationMilitary, flxEmpStartDateMilitary, flxGrossIncomeMilitary);
        var flxOtherEmp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxOtherEmp",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxOtherEmp"), extendConfig({}, controller.args[1], "flxOtherEmp"), extendConfig({}, controller.args[2], "flxOtherEmp"));
        flxOtherEmp.setDefaultUnit(kony.flex.DP);
        var flxEmpType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmpType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxEmpType"), extendConfig({}, controller.args[1], "flxEmpType"), extendConfig({}, controller.args[2], "flxEmpType"));
        flxEmpType.setDefaultUnit(kony.flex.DP);
        var flxEmpTypeLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmpTypeLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxEmpTypeLabel"), extendConfig({}, controller.args[1], "flxEmpTypeLabel"), extendConfig({}, controller.args[2], "flxEmpTypeLabel"));
        flxEmpTypeLabel.setDefaultUnit(kony.flex.DP);
        var lblEmpType = new kony.ui.Label(extendConfig({
            "id": "lblEmpType",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.EmploymentType\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmpType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmpType"), extendConfig({}, controller.args[2], "lblEmpType"));
        var lblEmpTypeSize = new kony.ui.Label(extendConfig({
            "id": "lblEmpTypeSize",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/70",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblEmpTypeSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmpTypeSize"), extendConfig({}, controller.args[2], "lblEmpTypeSize"));
        flxEmpTypeLabel.add(lblEmpType, lblEmpTypeSize);
        var txtEmpTypeValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtEmpTypeValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 70,
            "placeholder": "Employment Type",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtEmpTypeValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtEmpTypeValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtEmpTypeValue"));
        var empTypeErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "empTypeErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "empTypeErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "empTypeErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "empTypeErrorMsg"));
        flxEmpType.add(flxEmpTypeLabel, txtEmpTypeValue, empTypeErrorMsg);
        var flxOtherRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOtherRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxOtherRow2"), extendConfig({}, controller.args[1], "flxOtherRow2"), extendConfig({}, controller.args[2], "flxOtherRow2"));
        flxOtherRow2.setDefaultUnit(kony.flex.DP);
        var flxGrossIncomeOther = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGrossIncomeOther",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncomeOther"), extendConfig({}, controller.args[1], "flxGrossIncomeOther"), extendConfig({}, controller.args[2], "flxGrossIncomeOther"));
        flxGrossIncomeOther.setDefaultUnit(kony.flex.DP);
        var lblGrossIncomeOther = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncomeOther",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.GrossIncome\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGrossIncomeOther"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncomeOther"), extendConfig({}, controller.args[2], "lblGrossIncomeOther"));
        var flxGrossIncomeOtherAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxGrossIncomeOtherAmount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncomeOtherAmount"), extendConfig({}, controller.args[1], "flxGrossIncomeOtherAmount"), extendConfig({}, controller.args[2], "flxGrossIncomeOtherAmount"));
        flxGrossIncomeOtherAmount.setDefaultUnit(kony.flex.DP);
        var lblCurrencyOther = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblCurrencyOther",
            "isVisible": true,
            "left": "0",
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DollarSymbol\")",
            "top": "0",
            "width": "20%",
            "zIndex": 10
        }, controller.args[0], "lblCurrencyOther"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencyOther"), extendConfig({}, controller.args[2], "lblCurrencyOther"));
        var txtGrossIncomeValueOther = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtGrossIncomeValueOther",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "txtGrossIncomeValueOther"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtGrossIncomeValueOther"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtGrossIncomeValueOther"));
        flxGrossIncomeOtherAmount.add(lblCurrencyOther, txtGrossIncomeValueOther);
        var grossIncomeErrorOtherMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "grossIncomeErrorOtherMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "grossIncomeErrorOtherMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "grossIncomeErrorOtherMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "grossIncomeErrorOtherMsg"));
        flxGrossIncomeOther.add(lblGrossIncomeOther, flxGrossIncomeOtherAmount, grossIncomeErrorOtherMsg);
        var flxPayPeriodOther = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxPayPeriodOther",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPayPeriodOther"), extendConfig({}, controller.args[1], "flxPayPeriodOther"), extendConfig({}, controller.args[2], "flxPayPeriodOther"));
        flxPayPeriodOther.setDefaultUnit(kony.flex.DP);
        var lblPayPeriodOther = new kony.ui.Label(extendConfig({
            "id": "lblPayPeriodOther",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PayPeriod\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPayPeriodOther"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPayPeriodOther"), extendConfig({}, controller.args[2], "lblPayPeriodOther"));
        var lstPayPeriodValueOther = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstPayPeriodValueOther",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstPayPeriodValueOther"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstPayPeriodValueOther"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstPayPeriodValueOther"));
        var payPeriodErrorOtherMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "payPeriodErrorOtherMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "payPeriodErrorOtherMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "payPeriodErrorOtherMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "payPeriodErrorOtherMsg"));
        flxPayPeriodOther.add(lblPayPeriodOther, lstPayPeriodValueOther, payPeriodErrorOtherMsg);
        flxOtherRow2.add(flxGrossIncomeOther, flxPayPeriodOther);
        var flxDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescription",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDescription"), extendConfig({}, controller.args[1], "flxDescription"), extendConfig({}, controller.args[2], "flxDescription"));
        flxDescription.setDefaultUnit(kony.flex.DP);
        var flxDescriptionLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescriptionLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDescriptionLabel"), extendConfig({}, controller.args[1], "flxDescriptionLabel"), extendConfig({}, controller.args[2], "flxDescriptionLabel"));
        flxDescriptionLabel.setDefaultUnit(kony.flex.DP);
        var lblDescription = new kony.ui.Label(extendConfig({
            "id": "lblDescription",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Description\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        var lblDescriptionSize = new kony.ui.Label(extendConfig({
            "id": "lblDescriptionSize",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/200",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblDescriptionSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescriptionSize"), extendConfig({}, controller.args[2], "lblDescriptionSize"));
        flxDescriptionLabel.add(lblDescription, lblDescriptionSize);
        var txtDescription = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextAreaFocus",
            "height": "60dp",
            "id": "txtDescription",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "maxTextLength": 200,
            "numberOfVisibleLines": 3,
            "onTouchStart": controller.AS_TextArea_c64086f11dcc487a8444bdf0a18e9a02,
            "placeholder": "Description",
            "skin": "txtArea485c7513px",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [1, 1, 1, 1],
            "paddingInPixel": false
        }, controller.args[1], "txtDescription"), extendConfig({
            "autoCorrect": false,
            "onKeyUp": controller.AS_TextArea_d1068c1c380a4eb5a5faa76f3caea26c,
            "placeholderSkin": "defTextAreaPlaceholder"
        }, controller.args[2], "txtDescription"));
        flxDescription.add(flxDescriptionLabel, txtDescription);
        flxOtherEmp.add(flxEmpType, flxOtherRow2, flxDescription);
        flxEmploymentDtls.add(flxFulltimeEmployment, flxRowMilitary, flxOtherEmp);
        flxPresentEmployer.add(loansSectionHeader1, flxEmploymentDtls);
        var flxPreviousEmployer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPreviousEmployer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxPreviousEmployer"), extendConfig({}, controller.args[1], "flxPreviousEmployer"), extendConfig({}, controller.args[2], "flxPreviousEmployer"));
        flxPreviousEmployer.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader2 = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "loansSectionHeader2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PreviousEmployerDetails\")"
                },
                "loansSectionHeader": {
                    "top": "0"
                }
            }
        }, controller.args[0], "loansSectionHeader2"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader2"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader2"));
        var flxPreviousEmployerDtls = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPreviousEmployerDtls",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 20,
            "isModalContainer": false,
            "right": 20,
            "skin": "slFbox",
            "top": "42dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxPreviousEmployerDtls"), extendConfig({}, controller.args[1], "flxPreviousEmployerDtls"), extendConfig({}, controller.args[2], "flxPreviousEmployerDtls"));
        flxPreviousEmployerDtls.setDefaultUnit(kony.flex.DP);
        var flxPrevRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxPrevRow1"), extendConfig({}, controller.args[1], "flxPrevRow1"), extendConfig({}, controller.args[2], "flxPrevRow1"));
        flxPrevRow1.setDefaultUnit(kony.flex.DP);
        var flxPrevEmployerName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevEmployerName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "66%",
            "zIndex": 1
        }, controller.args[0], "flxPrevEmployerName"), extendConfig({}, controller.args[1], "flxPrevEmployerName"), extendConfig({}, controller.args[2], "flxPrevEmployerName"));
        flxPrevEmployerName.setDefaultUnit(kony.flex.DP);
        var flxPrevEmployerNameLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevEmployerNameLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxPrevEmployerNameLabel"), extendConfig({}, controller.args[1], "flxPrevEmployerNameLabel"), extendConfig({}, controller.args[2], "flxPrevEmployerNameLabel"));
        flxPrevEmployerNameLabel.setDefaultUnit(kony.flex.DP);
        var lblEmployerNamePrev = new kony.ui.Label(extendConfig({
            "id": "lblEmployerNamePrev",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.EmployerName\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmployerNamePrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmployerNamePrev"), extendConfig({}, controller.args[2], "lblEmployerNamePrev"));
        var lblEmployerNameSizePrev = new kony.ui.Label(extendConfig({
            "id": "lblEmployerNameSizePrev",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/70",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblEmployerNameSizePrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmployerNameSizePrev"), extendConfig({}, controller.args[2], "lblEmployerNameSizePrev"));
        flxPrevEmployerNameLabel.add(lblEmployerNamePrev, lblEmployerNameSizePrev);
        var txtEmployerNameValuePrev = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtEmployerNameValuePrev",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 70,
            "placeholder": "Employer Name",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtEmployerNameValuePrev"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtEmployerNameValuePrev"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtEmployerNameValuePrev"));
        var employerNameErrorPrevMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "employerNameErrorPrevMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "employerNameErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "employerNameErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "employerNameErrorPrevMsg"));
        flxPrevEmployerName.add(flxPrevEmployerNameLabel, txtEmployerNameValuePrev, employerNameErrorPrevMsg);
        var flxDesignationPrev = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDesignationPrev",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxDesignationPrev"), extendConfig({}, controller.args[1], "flxDesignationPrev"), extendConfig({}, controller.args[2], "flxDesignationPrev"));
        flxDesignationPrev.setDefaultUnit(kony.flex.DP);
        var flxDesignationPrevLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDesignationPrevLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxDesignationPrevLabel"), extendConfig({}, controller.args[1], "flxDesignationPrevLabel"), extendConfig({}, controller.args[2], "flxDesignationPrevLabel"));
        flxDesignationPrevLabel.setDefaultUnit(kony.flex.DP);
        var lblDesignationPrev = new kony.ui.Label(extendConfig({
            "id": "lblDesignationPrev",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Designation\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDesignationPrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDesignationPrev"), extendConfig({}, controller.args[2], "lblDesignationPrev"));
        var lblDesignationSizePrev = new kony.ui.Label(extendConfig({
            "id": "lblDesignationSizePrev",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/70",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblDesignationSizePrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDesignationSizePrev"), extendConfig({}, controller.args[2], "lblDesignationSizePrev"));
        flxDesignationPrevLabel.add(lblDesignationPrev, lblDesignationSizePrev);
        var txtDesignationValuePrev = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtDesignationValuePrev",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 70,
            "placeholder": "Designation",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtDesignationValuePrev"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtDesignationValuePrev"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtDesignationValuePrev"));
        var designationErrorPrevMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "designationErrorPrevMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "designationErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "designationErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "designationErrorPrevMsg"));
        flxDesignationPrev.add(flxDesignationPrevLabel, txtDesignationValuePrev, designationErrorPrevMsg);
        flxPrevRow1.add(flxPrevEmployerName, flxDesignationPrev);
        var flxAddressLine1Prev = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine1Prev",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressLine1Prev"), extendConfig({}, controller.args[1], "flxAddressLine1Prev"), extendConfig({}, controller.args[2], "flxAddressLine1Prev"));
        flxAddressLine1Prev.setDefaultUnit(kony.flex.DP);
        var flxAddressLine1PrevLabels = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine1PrevLabels",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddressLine1PrevLabels"), extendConfig({}, controller.args[1], "flxAddressLine1PrevLabels"), extendConfig({}, controller.args[2], "flxAddressLine1PrevLabels"));
        flxAddressLine1PrevLabels.setDefaultUnit(kony.flex.DP);
        var lblAddressLine1Prev = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine1Prev",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AddressLine1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressLine1Prev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine1Prev"), extendConfig({}, controller.args[2], "lblAddressLine1Prev"));
        var lblAddressLine1SizePrev = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine1SizePrev",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/40",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblAddressLine1SizePrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine1SizePrev"), extendConfig({}, controller.args[2], "lblAddressLine1SizePrev"));
        flxAddressLine1PrevLabels.add(lblAddressLine1Prev, lblAddressLine1SizePrev);
        var txtAddressLine1ValuePrev = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtAddressLine1ValuePrev",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 40,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine1User\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAddressLine1ValuePrev"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtAddressLine1ValuePrev"), extendConfig({
            "autoCorrect": false,
            "onKeyUp": controller.AS_TextField_af0d9ef3e8e54611a16523018c8af23c
        }, controller.args[2], "txtAddressLine1ValuePrev"));
        var addressLine1ErrorPrevMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "addressLine1ErrorPrevMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "addressLine1ErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "addressLine1ErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "addressLine1ErrorPrevMsg"));
        flxAddressLine1Prev.add(flxAddressLine1PrevLabels, txtAddressLine1ValuePrev, addressLine1ErrorPrevMsg);
        var flxAddressLine2Prev = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine2Prev",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressLine2Prev"), extendConfig({}, controller.args[1], "flxAddressLine2Prev"), extendConfig({}, controller.args[2], "flxAddressLine2Prev"));
        flxAddressLine2Prev.setDefaultUnit(kony.flex.DP);
        var flxAddressLine2PrevLabels = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine2PrevLabels",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddressLine2PrevLabels"), extendConfig({}, controller.args[1], "flxAddressLine2PrevLabels"), extendConfig({}, controller.args[2], "flxAddressLine2PrevLabels"));
        flxAddressLine2PrevLabels.setDefaultUnit(kony.flex.DP);
        var lblAddressLine2Prev = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine2Prev",
            "isVisible": true,
            "left": 0,
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AddressLine2\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressLine2Prev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine2Prev"), extendConfig({}, controller.args[2], "lblAddressLine2Prev"));
        var lblAddressLine2SizePrev = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine2SizePrev",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/40",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblAddressLine2SizePrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine2SizePrev"), extendConfig({}, controller.args[2], "lblAddressLine2SizePrev"));
        flxAddressLine2PrevLabels.add(lblAddressLine2Prev, lblAddressLine2SizePrev);
        var txtAddressLine2ValuePrev = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtAddressLine2ValuePrev",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 40,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine2User\")",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAddressLine2ValuePrev"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtAddressLine2ValuePrev"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtAddressLine2ValuePrev"));
        var addressLine2ErrorPrevMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "addressLine2ErrorPrevMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "addressLine2ErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "addressLine2ErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "addressLine2ErrorPrevMsg"));
        flxAddressLine2Prev.add(flxAddressLine2PrevLabels, txtAddressLine2ValuePrev, addressLine2ErrorPrevMsg);
        var flxAddressSection3Prev = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressSection3Prev",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressSection3Prev"), extendConfig({}, controller.args[1], "flxAddressSection3Prev"), extendConfig({}, controller.args[2], "flxAddressSection3Prev"));
        flxAddressSection3Prev.setDefaultUnit(kony.flex.DP);
        var flxAddressCountryPrev = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCountryPrev",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCountryPrev"), extendConfig({}, controller.args[1], "flxAddressCountryPrev"), extendConfig({}, controller.args[2], "flxAddressCountryPrev"));
        flxAddressCountryPrev.setDefaultUnit(kony.flex.DP);
        var lblAddressPrev = new kony.ui.Label(extendConfig({
            "id": "lblAddressPrev",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Country\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressPrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressPrev"), extendConfig({}, controller.args[2], "lblAddressPrev"));
        var txtCountryValuePrev = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtCountryValuePrev",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtCountryValuePrev"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCountryValuePrev"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtCountryValuePrev"));
        var currentCountryErrorPrevMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "currentCountryErrorPrevMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentCountryErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentCountryErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentCountryErrorPrevMsg"));
        flxAddressCountryPrev.add(lblAddressPrev, txtCountryValuePrev, currentCountryErrorPrevMsg);
        var flxAddressStatePrev = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressStatePrev",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressStatePrev"), extendConfig({}, controller.args[1], "flxAddressStatePrev"), extendConfig({}, controller.args[2], "flxAddressStatePrev"));
        flxAddressStatePrev.setDefaultUnit(kony.flex.DP);
        var lblAddressStatePrev = new kony.ui.Label(extendConfig({
            "id": "lblAddressStatePrev",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.State\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressStatePrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressStatePrev"), extendConfig({}, controller.args[2], "lblAddressStatePrev"));
        var txtStateValuePrev = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtStateValuePrev",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtStateValuePrev"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtStateValuePrev"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtStateValuePrev"));
        var currentStateErrorPrevMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "currentStateErrorPrevMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentStateErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentStateErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentStateErrorPrevMsg"));
        flxAddressStatePrev.add(lblAddressStatePrev, txtStateValuePrev, currentStateErrorPrevMsg);
        var flxAddressCityPrev = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCityPrev",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCityPrev"), extendConfig({}, controller.args[1], "flxAddressCityPrev"), extendConfig({}, controller.args[2], "flxAddressCityPrev"));
        flxAddressCityPrev.setDefaultUnit(kony.flex.DP);
        var lblAddressCityPrev = new kony.ui.Label(extendConfig({
            "id": "lblAddressCityPrev",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.City\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressCityPrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressCityPrev"), extendConfig({}, controller.args[2], "lblAddressCityPrev"));
        var txtCityValuePrev = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtCityValuePrev",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCityUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "txtCityValuePrev"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCityValuePrev"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtCityValuePrev"));
        var currentCityErrorPrevMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "currentCityErrorPrevMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentCityErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentCityErrorPrevMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentCityErrorPrevMsg"));
        flxAddressCityPrev.add(lblAddressCityPrev, txtCityValuePrev, currentCityErrorPrevMsg);
        flxAddressSection3Prev.add(flxAddressCountryPrev, flxAddressStatePrev, flxAddressCityPrev);
        var flxAddressSection4Prev = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressSection4Prev",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAddressSection4Prev"), extendConfig({}, controller.args[1], "flxAddressSection4Prev"), extendConfig({}, controller.args[2], "flxAddressSection4Prev"));
        flxAddressSection4Prev.setDefaultUnit(kony.flex.DP);
        var flxAddressZipCodePrev = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressZipCodePrev",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressZipCodePrev"), extendConfig({}, controller.args[1], "flxAddressZipCodePrev"), extendConfig({}, controller.args[2], "flxAddressZipCodePrev"));
        flxAddressZipCodePrev.setDefaultUnit(kony.flex.DP);
        var lblAddressZipCodePrev = new kony.ui.Label(extendConfig({
            "id": "lblAddressZipCodePrev",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.zip\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressZipCodePrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressZipCodePrev"), extendConfig({}, controller.args[2], "lblAddressZipCodePrev"));
        var txtZipCodeValuePrev = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtZipCodeValuePrev",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblPostalCodeUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtZipCodeValuePrev"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtZipCodeValuePrev"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtZipCodeValuePrev"));
        var errorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "errorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "errorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "errorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "errorMsg"));
        flxAddressZipCodePrev.add(lblAddressZipCodePrev, txtZipCodeValuePrev, errorMsg);
        flxAddressSection4Prev.add(flxAddressZipCodePrev);
        var flxAddressValidationPrev = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAddressValidationPrev",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxf9f9f9NoBorder",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAddressValidationPrev"), extendConfig({}, controller.args[1], "flxAddressValidationPrev"), extendConfig({}, controller.args[2], "flxAddressValidationPrev"));
        flxAddressValidationPrev.setDefaultUnit(kony.flex.DP);
        var flxErrorIconConatinerPrev = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxErrorIconConatinerPrev",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "44%"
        }, controller.args[0], "flxErrorIconConatinerPrev"), extendConfig({}, controller.args[1], "flxErrorIconConatinerPrev"), extendConfig({}, controller.args[2], "flxErrorIconConatinerPrev"));
        flxErrorIconConatinerPrev.setDefaultUnit(kony.flex.DP);
        var lblUspsRecommendationErrorIconPrev = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblUspsRecommendationErrorIconPrev",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "11dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblUspsRecommendationErrorIconPrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUspsRecommendationErrorIconPrev"), extendConfig({}, controller.args[2], "lblUspsRecommendationErrorIconPrev"));
        flxErrorIconConatinerPrev.add(lblUspsRecommendationErrorIconPrev);
        var lblUspsRecommendationPrev = new kony.ui.Label(extendConfig({
            "id": "lblUspsRecommendationPrev",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818A13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.USPSRecommendation\")",
            "top": "11px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUspsRecommendationPrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUspsRecommendationPrev"), extendConfig({}, controller.args[2], "lblUspsRecommendationPrev"));
        var flxValidatePrev = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxValidatePrev",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "11dp",
            "width": "50dp"
        }, controller.args[0], "flxValidatePrev"), extendConfig({}, controller.args[1], "flxValidatePrev"), extendConfig({}, controller.args[2], "flxValidatePrev"));
        flxValidatePrev.setDefaultUnit(kony.flex.DP);
        var lblVallidatePrev = new kony.ui.Label(extendConfig({
            "id": "lblVallidatePrev",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblHyperLink14PX0a9c098be02ca43",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Validate\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblVallidatePrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVallidatePrev"), extendConfig({}, controller.args[2], "lblVallidatePrev"));
        flxValidatePrev.add(lblVallidatePrev);
        flxAddressValidationPrev.add(flxErrorIconConatinerPrev, lblUspsRecommendationPrev, flxValidatePrev);
        flxPreviousEmployerDtls.add(flxPrevRow1, flxAddressLine1Prev, flxAddressLine2Prev, flxAddressSection3Prev, flxAddressSection4Prev, flxAddressValidationPrev);
        flxPreviousEmployer.add(loansSectionHeader2, flxPreviousEmployerDtls);
        flxSection.add(loansSectionHeader, flxEmploymentStatus, flxPresentEmployer, flxPreviousEmployer);
        editEmployementInfo.add(flxSection);
        return editEmployementInfo;
    }
})