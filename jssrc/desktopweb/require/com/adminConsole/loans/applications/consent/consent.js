define(function() {
    return function(controller) {
        var consent = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "consent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBg000000Op50",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "consent"), extendConfig({}, controller.args[1], "consent"), extendConfig({}, controller.args[2], "consent"));
        consent.setDefaultUnit(kony.flex.DP);
        var flxPopUp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": false,
            "id": "flxPopUp",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox0j9f841cc563e4e",
            "top": "150px",
            "width": "600px",
            "zIndex": 10
        }, controller.args[0], "flxPopUp"), extendConfig({}, controller.args[1], "flxPopUp"), extendConfig({}, controller.args[2], "flxPopUp"));
        flxPopUp.setDefaultUnit(kony.flex.DP);
        var flxPopUpTopColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxPopUpTopColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflx24ace8",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpTopColor"), extendConfig({}, controller.args[1], "flxPopUpTopColor"), extendConfig({}, controller.args[2], "flxPopUpTopColor"));
        flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
        flxPopUpTopColor.add();
        var flxPopupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "300px",
            "id": "flxPopupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0d9c3974835234d",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopupHeader"), extendConfig({}, controller.args[1], "flxPopupHeader"), extendConfig({}, controller.args[2], "flxPopupHeader"));
        flxPopupHeader.setDefaultUnit(kony.flex.DP);
        var flxPopUpClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxPopUpClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "slFbox",
            "top": "15dp",
            "width": "25px",
            "zIndex": 20
        }, controller.args[0], "flxPopUpClose"), extendConfig({}, controller.args[1], "flxPopUpClose"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxPopUpClose"));
        flxPopUpClose.setDefaultUnit(kony.flex.DP);
        var lblPopupClose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "lblPopupClose",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon18pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblPopupClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopupClose"), extendConfig({}, controller.args[2], "lblPopupClose"));
        flxPopUpClose.add(lblPopupClose);
        var lblPopUpMainMessage = new kony.ui.Label(extendConfig({
            "id": "lblPopUpMainMessage",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f23px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.ConsentSection\")",
            "top": "0px",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblPopUpMainMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpMainMessage"), extendConfig({}, controller.args[2], "lblPopUpMainMessage"));
        var flxConsentText = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "220px",
            "horizontalScrollIndicator": true,
            "id": "flxConsentText",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "15dp",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxConsentText"), extendConfig({}, controller.args[1], "flxConsentText"), extendConfig({}, controller.args[2], "flxConsentText"));
        flxConsentText.setDefaultUnit(kony.flex.DP);
        var rtxPopUpDisclaimer = new kony.ui.RichText(extendConfig({
            "bottom": "20px",
            "centerX": "50%",
            "id": "rtxPopUpDisclaimer",
            "isVisible": true,
            "right": 20,
            "skin": "sknrtxLato0df8337c414274d",
            "text": "<b>Agree to All Terms & Conditions and Submit Application</b>\n\nYou authorize, XYZ Bank, to: \n1.\tPull a credit report on you and co-applicant (if any) in order to make a credit decision.\n\n2.\tUse the information on this application and any other information we or our affiliates have about you to determine your ability to pay, as required by federal law.\n\n3.\tVerify, at our option and for your benefit, your application information.\n\n4.\tContact you or receive a detailed message or messages at the telephone, email or direct mail contact data you provided above for purposes of fulfilling this inquiry about automobile loan financing even if you have previously registered on a Do No Call registry or requested XYZ Bank not to send marketing information to you by email and/or direct  mail.\n\n<b>TERMS & CONDITIONS<br>By giving us your phone number and submitting this application, you acknowledge that: </b><br>\n1.\tYou’ve reviewed any special disclosure that applies in your state: ME, CA, NY, RI, OH, VT and WI residents \n2.\tAll information you’ve submitted is true and correct and is given to obtain credit from us\n3.\tCo-applicant (if any) has authorized you to submit this application on his or her behalf.\n4.\tYou and co-applicant (if any) are of legal contracting age.\n5.\tCredit approval, Annual Percentage Rate (APR), and credit terms are based on our review of each applicant’s application information and credit report.\n6.\tYou understand we collect your individual income information to determine your individual ability to pay. Examples may include income earned from your employment (salary, overtime, bonus), retirement/pension benefits, and rental properties. Student loans are not acceptable sources of income. Alimony, child support, or separate maintenance income need not be revealed if you don’t wish it to be considered as a basis for repayment.<br>\n<b>And you agree that XYZ Bank, N.A. may:</b><br>1.\tUse automatic telephone dialing systems and prerecorded voice messages in connection with calls or texts made to any telephone number you provide even if the telephone number is assigned to a cellular/mobile telephone service or other service for which the called party is charged according to their normal text messaging rates.<br>\n<b>Privacy</b><br>All financial institutions are required by federal law to obtain, verify and record information that identifies each customer before opening an account with us. When you open an account with us, we will ask you for your name, address and other information that will allow us to identify you, such as Social Security number and date of birth. In addition, if the account application is approved, do we have your permission to open your account prior to sending you a copy of our XYZ Bank US Consumer Privacy Notice, which will be sent with your customary account disclosures\t<br><br><b>Electronic Consumer Information consent</b><br>To receive the Consumer Information electronically, scroll to read the complete disclosure and then select I consent on this page. As part of your online loan application, XYZ bank., is required by law to provide you with certain Consumer Information. You have the right to receive the Consumer Information on paper. With your consent, we may provide the Consumer Information to you electronically instead. If you prefer to receive the Consumer Information on paper, you’ll need to visit any XYZ banking center to apply in person. Your consent only applies to this loan application and the documents listed here. You aren't required to consent to receive information electronically to complete this application. If you’d like to withdraw your consent after consenting to receive information electronically, or if after receiving electronic documents you would like paper copies, you may do so in the Application Status Center or contact us at 1.888.266.1034 and we will provide you with paper copies at no charge. You can also update your contact information with us by calling that same telephone number. Consent withdrawal won’t apply to any actions already taken or initiated in reliance on your consent. <br>",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "rtxPopUpDisclaimer"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxPopUpDisclaimer"), extendConfig({}, controller.args[2], "rtxPopUpDisclaimer"));
        flxConsentText.add(rtxPopUpDisclaimer);
        flxPopupHeader.add(flxPopUpClose, lblPopUpMainMessage, flxConsentText);
        var flxPopUpButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxPopUpButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox0dc900c4572aa40",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpButtons"), extendConfig({}, controller.args[1], "flxPopUpButtons"), extendConfig({}, controller.args[2], "flxPopUpButtons"));
        flxPopUpButtons.setDefaultUnit(kony.flex.DP);
        var btnPopUpCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
            "height": "40px",
            "id": "btnPopUpCancel",
            "isVisible": true,
            "right": "130px",
            "skin": "sknBtnLatoRegulara5abc413pxKA",
            "text": "DISAGREE",
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
        }, controller.args[2], "btnPopUpCancel"));
        var btnPopUpDelete = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
            "height": "40px",
            "id": "btnPopUpDelete",
            "isVisible": true,
            "minWidth": "100px",
            "right": "20px",
            "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
            "text": "AGREE",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpDelete"), extendConfig({
            "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
        }, controller.args[2], "btnPopUpDelete"));
        flxPopUpButtons.add(btnPopUpCancel, btnPopUpDelete);
        flxPopUp.add(flxPopUpTopColor, flxPopupHeader, flxPopUpButtons);
        consent.add(flxPopUp);
        return consent;
    }
})