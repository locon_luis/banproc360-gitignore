define(function() {
    return function(controller) {
        var editExpenditureInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "editExpenditureInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "editExpenditureInfo"), extendConfig({}, controller.args[1], "editExpenditureInfo"), extendConfig({}, controller.args[2], "editExpenditureInfo"));
        editExpenditureInfo.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Expenditure\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxSection"), extendConfig({}, controller.args[1], "flxSection"), extendConfig({}, controller.args[2], "flxSection"));
        flxSection.setDefaultUnit(kony.flex.DP);
        var flxExpenditureInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxExpenditureInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxExpenditureInfo"), extendConfig({}, controller.args[1], "flxExpenditureInfo"), extendConfig({}, controller.args[2], "flxExpenditureInfo"));
        flxExpenditureInfo.setDefaultUnit(kony.flex.DP);
        var flxRent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxRent"), extendConfig({}, controller.args[1], "flxRent"), extendConfig({}, controller.args[2], "flxRent"));
        flxRent.setDefaultUnit(kony.flex.DP);
        var lblRent = new kony.ui.Label(extendConfig({
            "id": "lblRent",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.MonthlyHousingRentOptional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRent"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRent"), extendConfig({}, controller.args[2], "lblRent"));
        var flxRentAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxRentAmount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRentAmount"), extendConfig({}, controller.args[1], "flxRentAmount"), extendConfig({}, controller.args[2], "flxRentAmount"));
        flxRentAmount.setDefaultUnit(kony.flex.DP);
        var lblCurrency = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblCurrency",
            "isVisible": true,
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DollarSymbol\")",
            "width": "20%",
            "zIndex": 10
        }, controller.args[0], "lblCurrency"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrency"), extendConfig({}, controller.args[2], "lblCurrency"));
        var txtRentValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtRentValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "txtRentValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtRentValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtRentValue"));
        flxRentAmount.add(lblCurrency, txtRentValue);
        var rentValueErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "rentValueErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "rentValueErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "rentValueErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "rentValueErrorMsg"));
        flxRent.add(lblRent, flxRentAmount, rentValueErrorMsg);
        var flxMortgage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMortgage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxMortgage"), extendConfig({}, controller.args[1], "flxMortgage"), extendConfig({}, controller.args[2], "flxMortgage"));
        flxMortgage.setDefaultUnit(kony.flex.DP);
        var lblMortgage = new kony.ui.Label(extendConfig({
            "id": "lblMortgage",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.MonthlyHousingMortgageOptional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMortgage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMortgage"), extendConfig({}, controller.args[2], "lblMortgage"));
        var flxMortgageAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxMortgageAmount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxMortgageAmount"), extendConfig({}, controller.args[1], "flxMortgageAmount"), extendConfig({}, controller.args[2], "flxMortgageAmount"));
        flxMortgageAmount.setDefaultUnit(kony.flex.DP);
        var lblMortgageCurrency = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblMortgageCurrency",
            "isVisible": true,
            "left": "0",
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DollarSymbol\")",
            "top": "0",
            "width": "20%",
            "zIndex": 10
        }, controller.args[0], "lblMortgageCurrency"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMortgageCurrency"), extendConfig({}, controller.args[2], "lblMortgageCurrency"));
        var txtMortgageValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtMortgageValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "txtMortgageValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtMortgageValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtMortgageValue"));
        flxMortgageAmount.add(lblMortgageCurrency, txtMortgageValue);
        var mortgageValueErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "mortgageValueErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "mortgageValueErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "mortgageValueErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "mortgageValueErrorMsg"));
        flxMortgage.add(lblMortgage, flxMortgageAmount, mortgageValueErrorMsg);
        flxExpenditureInfo.add(flxRent, flxMortgage);
        flxSection.add(flxExpenditureInfo);
        editExpenditureInfo.add(loansSectionHeader, flxSection);
        return editExpenditureInfo;
    }
})