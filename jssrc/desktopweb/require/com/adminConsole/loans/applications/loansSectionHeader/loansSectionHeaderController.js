define("com/adminConsole/loans/applications/loansSectionHeader/userloansSectionHeaderController", function() {
    return {};
});
define("com/adminConsole/loans/applications/loansSectionHeader/loansSectionHeaderControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnEdit **/
    AS_Button_e1685424d4c6426b95ee8300041e29ac: function AS_Button_e1685424d4c6426b95ee8300041e29ac(eventobject) {
        var self = this;
    }
});
define("com/adminConsole/loans/applications/loansSectionHeader/loansSectionHeaderController", ["com/adminConsole/loans/applications/loansSectionHeader/userloansSectionHeaderController", "com/adminConsole/loans/applications/loansSectionHeader/loansSectionHeaderControllerActions"], function() {
    var controller = require("com/adminConsole/loans/applications/loansSectionHeader/userloansSectionHeaderController");
    var actions = require("com/adminConsole/loans/applications/loansSectionHeader/loansSectionHeaderControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
