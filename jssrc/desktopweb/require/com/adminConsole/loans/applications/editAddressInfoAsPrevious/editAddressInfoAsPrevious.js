define(function() {
    return function(controller) {
        var editAddressInfoAsPrevious = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "editAddressInfoAsPrevious",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "editAddressInfoAsPrevious"), extendConfig({}, controller.args[1], "editAddressInfoAsPrevious"), extendConfig({}, controller.args[2], "editAddressInfoAsPrevious"));
        editAddressInfoAsPrevious.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PresentAddress\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxAddressDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxAddressDetails"), extendConfig({}, controller.args[1], "flxAddressDetails"), extendConfig({}, controller.args[2], "flxAddressDetails"));
        flxAddressDetails.setDefaultUnit(kony.flex.DP);
        var flxConsent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxConsent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxConsent"), extendConfig({}, controller.args[1], "flxConsent"), extendConfig({}, controller.args[2], "flxConsent"));
        flxConsent.setDefaultUnit(kony.flex.DP);
        var flxAcceptanceIcon = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxAcceptanceIcon",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 2,
            "width": "15dp"
        }, controller.args[0], "flxAcceptanceIcon"), extendConfig({}, controller.args[1], "flxAcceptanceIcon"), extendConfig({}, controller.args[2], "flxAcceptanceIcon"));
        flxAcceptanceIcon.setDefaultUnit(kony.flex.DP);
        var lblAcceptanceIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAcceptanceIcon",
            "isVisible": true,
            "left": "0",
            "skin": "sknIcon13pxWhite",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAcceptanceIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAcceptanceIcon"), extendConfig({}, controller.args[2], "lblAcceptanceIcon"));
        flxAcceptanceIcon.add(lblAcceptanceIcon);
        var lblConsentMsg = new kony.ui.Label(extendConfig({
            "id": "lblConsentMsg",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "I John Williams, Consent*",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblConsentMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConsentMsg"), extendConfig({}, controller.args[2], "lblConsentMsg"));
        flxConsent.add(flxAcceptanceIcon, lblConsentMsg);
        var flxAddressInfoSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressInfoSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxAddressInfoSection"), extendConfig({}, controller.args[1], "flxAddressInfoSection"), extendConfig({}, controller.args[2], "flxAddressInfoSection"));
        flxAddressInfoSection.setDefaultUnit(kony.flex.DP);
        var flxAddressInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxAddressInfo"), extendConfig({}, controller.args[1], "flxAddressInfo"), extendConfig({}, controller.args[2], "flxAddressInfo"));
        flxAddressInfo.setDefaultUnit(kony.flex.DP);
        var flxAddressLine1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxAddressLine1"), extendConfig({}, controller.args[1], "flxAddressLine1"), extendConfig({}, controller.args[2], "flxAddressLine1"));
        flxAddressLine1.setDefaultUnit(kony.flex.DP);
        var flxAddressLine1Labels = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine1Labels",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddressLine1Labels"), extendConfig({}, controller.args[1], "flxAddressLine1Labels"), extendConfig({}, controller.args[2], "flxAddressLine1Labels"));
        flxAddressLine1Labels.setDefaultUnit(kony.flex.DP);
        var lblAddressLine1 = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AddressLine1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressLine1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine1"), extendConfig({}, controller.args[2], "lblAddressLine1"));
        var lblAddrLine1Size = new kony.ui.Label(extendConfig({
            "id": "lblAddrLine1Size",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/40",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblAddrLine1Size"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddrLine1Size"), extendConfig({}, controller.args[2], "lblAddrLine1Size"));
        flxAddressLine1Labels.add(lblAddressLine1, lblAddrLine1Size);
        var txtAddressLine1Value = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtAddressLine1Value",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 40,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine1User\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAddressLine1Value"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtAddressLine1Value"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtAddressLine1Value"));
        var currentAddressErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "currentAddressErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentAddressErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentAddressErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentAddressErrorMsg"));
        flxAddressLine1.add(flxAddressLine1Labels, txtAddressLine1Value, currentAddressErrorMsg);
        var flxAddressLine2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressLine2"), extendConfig({}, controller.args[1], "flxAddressLine2"), extendConfig({}, controller.args[2], "flxAddressLine2"));
        flxAddressLine2.setDefaultUnit(kony.flex.DP);
        var flxAddressLine2Label = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine2Label",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddressLine2Label"), extendConfig({}, controller.args[1], "flxAddressLine2Label"), extendConfig({}, controller.args[2], "flxAddressLine2Label"));
        flxAddressLine2Label.setDefaultUnit(kony.flex.DP);
        var lblAddressLine2 = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine2",
            "isVisible": true,
            "left": 0,
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AddressLine2\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressLine2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine2"), extendConfig({}, controller.args[2], "lblAddressLine2"));
        var lblAddreLine2Size = new kony.ui.Label(extendConfig({
            "id": "lblAddreLine2Size",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/40",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblAddreLine2Size"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddreLine2Size"), extendConfig({}, controller.args[2], "lblAddreLine2Size"));
        flxAddressLine2Label.add(lblAddressLine2, lblAddreLine2Size);
        var txtAddressLine2Value = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtAddressLine2Value",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 40,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine2User\")",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAddressLine2Value"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtAddressLine2Value"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtAddressLine2Value"));
        var addressLine2ErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "addressLine2ErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "addressLine2ErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "addressLine2ErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "addressLine2ErrorMsg"));
        flxAddressLine2.add(flxAddressLine2Label, txtAddressLine2Value, addressLine2ErrorMsg);
        var flxAddressSection3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressSection3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressSection3"), extendConfig({}, controller.args[1], "flxAddressSection3"), extendConfig({}, controller.args[2], "flxAddressSection3"));
        flxAddressSection3.setDefaultUnit(kony.flex.DP);
        var flxAddressCountry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCountry",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCountry"), extendConfig({}, controller.args[1], "flxAddressCountry"), extendConfig({}, controller.args[2], "flxAddressCountry"));
        flxAddressCountry.setDefaultUnit(kony.flex.DP);
        var lblAddress1Country = new kony.ui.Label(extendConfig({
            "id": "lblAddress1Country",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Country\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddress1Country"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddress1Country"), extendConfig({}, controller.args[2], "lblAddress1Country"));
        var txtCountryValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtCountryValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtCountryValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCountryValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtCountryValue"));
        var currentCountryErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "currentCountryErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentCountryErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentCountryErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentCountryErrorMsg"));
        flxAddressCountry.add(lblAddress1Country, txtCountryValue, currentCountryErrorMsg);
        var flxAddressState = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressState",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressState"), extendConfig({}, controller.args[1], "flxAddressState"), extendConfig({}, controller.args[2], "flxAddressState"));
        flxAddressState.setDefaultUnit(kony.flex.DP);
        var lblAddressState = new kony.ui.Label(extendConfig({
            "id": "lblAddressState",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.State\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressState"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressState"), extendConfig({}, controller.args[2], "lblAddressState"));
        var txtStateValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtStateValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtStateValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtStateValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtStateValue"));
        var currentStateErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "currentStateErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentStateErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentStateErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentStateErrorMsg"));
        flxAddressState.add(lblAddressState, txtStateValue, currentStateErrorMsg);
        var flxAddressCity = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCity",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCity"), extendConfig({}, controller.args[1], "flxAddressCity"), extendConfig({}, controller.args[2], "flxAddressCity"));
        flxAddressCity.setDefaultUnit(kony.flex.DP);
        var lblAddressCity = new kony.ui.Label(extendConfig({
            "id": "lblAddressCity",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.City\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressCity"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressCity"), extendConfig({}, controller.args[2], "lblAddressCity"));
        var txtCityValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtCityValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCityUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "txtCityValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCityValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtCityValue"));
        var currentCityErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "currentCityErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentCityErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentCityErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentCityErrorMsg"));
        flxAddressCity.add(lblAddressCity, txtCityValue, currentCityErrorMsg);
        flxAddressSection3.add(flxAddressCountry, flxAddressState, flxAddressCity);
        var flxAddressSection4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressSection4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAddressSection4"), extendConfig({}, controller.args[1], "flxAddressSection4"), extendConfig({}, controller.args[2], "flxAddressSection4"));
        flxAddressSection4.setDefaultUnit(kony.flex.DP);
        var flxAddressZipCode = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressZipCode",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressZipCode"), extendConfig({}, controller.args[1], "flxAddressZipCode"), extendConfig({}, controller.args[2], "flxAddressZipCode"));
        flxAddressZipCode.setDefaultUnit(kony.flex.DP);
        var lblAddressZipCode = new kony.ui.Label(extendConfig({
            "id": "lblAddressZipCode",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.zip\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressZipCode"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressZipCode"), extendConfig({}, controller.args[2], "lblAddressZipCode"));
        var txtZipCodeValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtZipCodeValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblPostalCodeUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtZipCodeValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtZipCodeValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtZipCodeValue"));
        var currentZipCodeErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "currentZipCodeErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentZipCodeErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentZipCodeErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentZipCodeErrorMsg"));
        flxAddressZipCode.add(lblAddressZipCode, txtZipCodeValue, currentZipCodeErrorMsg);
        flxAddressSection4.add(flxAddressZipCode);
        var flxAddressValidation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAddressValidation",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxf9f9f9NoBorder",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAddressValidation"), extendConfig({}, controller.args[1], "flxAddressValidation"), extendConfig({}, controller.args[2], "flxAddressValidation"));
        flxAddressValidation.setDefaultUnit(kony.flex.DP);
        var flxErrorIconConatiner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxErrorIconConatiner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "44%"
        }, controller.args[0], "flxErrorIconConatiner"), extendConfig({}, controller.args[1], "flxErrorIconConatiner"), extendConfig({}, controller.args[2], "flxErrorIconConatiner"));
        flxErrorIconConatiner.setDefaultUnit(kony.flex.DP);
        var lblUspsRecommendationErrorIcon = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblUspsRecommendationErrorIcon",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "11dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblUspsRecommendationErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUspsRecommendationErrorIcon"), extendConfig({}, controller.args[2], "lblUspsRecommendationErrorIcon"));
        flxErrorIconConatiner.add(lblUspsRecommendationErrorIcon);
        var lblUspsRecommendation = new kony.ui.Label(extendConfig({
            "id": "lblUspsRecommendation",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818A13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.USPSRecommendation\")",
            "top": "11px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUspsRecommendation"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUspsRecommendation"), extendConfig({}, controller.args[2], "lblUspsRecommendation"));
        var flxValidate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxValidate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "11dp",
            "width": "50dp"
        }, controller.args[0], "flxValidate"), extendConfig({}, controller.args[1], "flxValidate"), extendConfig({}, controller.args[2], "flxValidate"));
        flxValidate.setDefaultUnit(kony.flex.DP);
        var lblValidate = new kony.ui.Label(extendConfig({
            "id": "lblValidate",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblHyperLink14PX0a9c098be02ca43",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Validate\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValidate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValidate"), extendConfig({}, controller.args[2], "lblValidate"));
        flxValidate.add(lblValidate);
        flxAddressValidation.add(flxErrorIconConatiner, lblUspsRecommendation, flxValidate);
        flxAddressInfo.add(flxAddressLine1, flxAddressLine2, flxAddressSection3, flxAddressSection4, flxAddressValidation);
        flxAddressInfoSection.add(flxAddressInfo);
        flxAddressDetails.add(flxConsent, flxAddressInfoSection);
        editAddressInfoAsPrevious.add(loansSectionHeader, flxAddressDetails);
        return editAddressInfoAsPrevious;
    }
})