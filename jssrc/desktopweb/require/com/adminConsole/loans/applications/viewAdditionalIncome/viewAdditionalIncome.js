define(function() {
    return function(controller) {
        var viewAdditionalIncome = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewAdditionalIncome",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "viewAdditionalIncome"), extendConfig({}, controller.args[1], "viewAdditionalIncome"), extendConfig({}, controller.args[2], "viewAdditionalIncome"));
        viewAdditionalIncome.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AdditionalIncomeSource\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxIncome = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncome",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "42dp",
            "width": "100%"
        }, controller.args[0], "flxIncome"), extendConfig({}, controller.args[1], "flxIncome"), extendConfig({}, controller.args[2], "flxIncome"));
        flxIncome.setDefaultUnit(kony.flex.DP);
        var flxIncomeSource = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncomeSource",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": 30,
            "zIndex": 1
        }, controller.args[0], "flxIncomeSource"), extendConfig({}, controller.args[1], "flxIncomeSource"), extendConfig({}, controller.args[2], "flxIncomeSource"));
        flxIncomeSource.setDefaultUnit(kony.flex.DP);
        var lblAdditionalIncome = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalIncome",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AddtionalSourceOfIncomeCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalIncome"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalIncome"), extendConfig({}, controller.args[2], "lblAdditionalIncome"));
        var lblAdditionalIncomeValue = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalIncomeValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Yes",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalIncomeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalIncomeValue"), extendConfig({}, controller.args[2], "lblAdditionalIncomeValue"));
        flxIncomeSource.add(lblAdditionalIncome, lblAdditionalIncomeValue);
        var flxIncomeInfoSource = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncomeInfoSource",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxIncomeInfoSource"), extendConfig({}, controller.args[1], "flxIncomeInfoSource"), extendConfig({}, controller.args[2], "flxIncomeInfoSource"));
        flxIncomeInfoSource.setDefaultUnit(kony.flex.DP);
        flxIncomeInfoSource.add();
        flxIncome.add(flxIncomeSource, flxIncomeInfoSource);
        viewAdditionalIncome.add(loansSectionHeader, flxIncome);
        return viewAdditionalIncome;
    }
})