define(function() {
    return function(controller) {
        var viewAddressInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewAddressInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "viewAddressInfo"), extendConfig({}, controller.args[1], "viewAddressInfo"), extendConfig({}, controller.args[2], "viewAddressInfo"));
        viewAddressInfo.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PresentAddress\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxAddressSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxAddressSection"), extendConfig({}, controller.args[1], "flxAddressSection"), extendConfig({}, controller.args[2], "flxAddressSection"));
        flxAddressSection.setDefaultUnit(kony.flex.DP);
        var flxAddressInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxAddressInfo"), extendConfig({}, controller.args[1], "flxAddressInfo"), extendConfig({}, controller.args[2], "flxAddressInfo"));
        flxAddressInfo.setDefaultUnit(kony.flex.DP);
        var flxAddressLine1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressLine1"), extendConfig({}, controller.args[1], "flxAddressLine1"), extendConfig({}, controller.args[2], "flxAddressLine1"));
        flxAddressLine1.setDefaultUnit(kony.flex.DP);
        var lblAddressLine1 = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.AddressLine1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressLine1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine1"), extendConfig({}, controller.args[2], "lblAddressLine1"));
        var lblAddressLine1Value = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine1Value",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Address Line 1",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblAddressLine1Value"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine1Value"), extendConfig({}, controller.args[2], "lblAddressLine1Value"));
        flxAddressLine1.add(lblAddressLine1, lblAddressLine1Value);
        var flxAddressLine2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressLine2"), extendConfig({}, controller.args[1], "flxAddressLine2"), extendConfig({}, controller.args[2], "flxAddressLine2"));
        flxAddressLine2.setDefaultUnit(kony.flex.DP);
        var lblAddressLine2 = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.AddressLine2CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressLine2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine2"), extendConfig({}, controller.args[2], "lblAddressLine2"));
        var lblAddressLine2Value = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine2Value",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Address Line 2",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblAddressLine2Value"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine2Value"), extendConfig({}, controller.args[2], "lblAddressLine2Value"));
        flxAddressLine2.add(lblAddressLine2, lblAddressLine2Value);
        var flxAddressCountryDtls = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCountryDtls",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCountryDtls"), extendConfig({}, controller.args[1], "flxAddressCountryDtls"), extendConfig({}, controller.args[2], "flxAddressCountryDtls"));
        flxAddressCountryDtls.setDefaultUnit(kony.flex.DP);
        var flxAddressCountry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCountry",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCountry"), extendConfig({}, controller.args[1], "flxAddressCountry"), extendConfig({}, controller.args[2], "flxAddressCountry"));
        flxAddressCountry.setDefaultUnit(kony.flex.DP);
        var lblAddress1Country = new kony.ui.Label(extendConfig({
            "id": "lblAddress1Country",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.Country\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddress1Country"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddress1Country"), extendConfig({}, controller.args[2], "lblAddress1Country"));
        var lblCountryValue = new kony.ui.Label(extendConfig({
            "id": "lblCountryValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Contry",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblCountryValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCountryValue"), extendConfig({}, controller.args[2], "lblCountryValue"));
        flxAddressCountry.add(lblAddress1Country, lblCountryValue);
        var flxAddressState = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxAddressState",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressState"), extendConfig({}, controller.args[1], "flxAddressState"), extendConfig({}, controller.args[2], "flxAddressState"));
        flxAddressState.setDefaultUnit(kony.flex.DP);
        var lblAddressState = new kony.ui.Label(extendConfig({
            "id": "lblAddressState",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.State\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressState"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressState"), extendConfig({}, controller.args[2], "lblAddressState"));
        var lblStateValue = new kony.ui.Label(extendConfig({
            "id": "lblStateValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "State",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblStateValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStateValue"), extendConfig({}, controller.args[2], "lblStateValue"));
        flxAddressState.add(lblAddressState, lblStateValue);
        var flxAddressCity = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCity",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCity"), extendConfig({}, controller.args[1], "flxAddressCity"), extendConfig({}, controller.args[2], "flxAddressCity"));
        flxAddressCity.setDefaultUnit(kony.flex.DP);
        var lblAddressCity = new kony.ui.Label(extendConfig({
            "id": "lblAddressCity",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.City\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressCity"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressCity"), extendConfig({}, controller.args[2], "lblAddressCity"));
        var lblCityValue = new kony.ui.Label(extendConfig({
            "id": "lblCityValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "City",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblCityValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCityValue"), extendConfig({}, controller.args[2], "lblCityValue"));
        flxAddressCity.add(lblAddressCity, lblCityValue);
        flxAddressCountryDtls.add(flxAddressCountry, flxAddressState, flxAddressCity);
        var flxAddressZipOwnership = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressZipOwnership",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAddressZipOwnership"), extendConfig({}, controller.args[1], "flxAddressZipOwnership"), extendConfig({}, controller.args[2], "flxAddressZipOwnership"));
        flxAddressZipOwnership.setDefaultUnit(kony.flex.DP);
        var flxAddressZipCode = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressZipCode",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressZipCode"), extendConfig({}, controller.args[1], "flxAddressZipCode"), extendConfig({}, controller.args[2], "flxAddressZipCode"));
        flxAddressZipCode.setDefaultUnit(kony.flex.DP);
        var lblAddressZipCode = new kony.ui.Label(extendConfig({
            "id": "lblAddressZipCode",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.ZipCode\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressZipCode"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressZipCode"), extendConfig({}, controller.args[2], "lblAddressZipCode"));
        var lblZipCodeValue = new kony.ui.Label(extendConfig({
            "id": "lblZipCodeValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "zip code",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblZipCodeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblZipCodeValue"), extendConfig({}, controller.args[2], "lblZipCodeValue"));
        flxAddressZipCode.add(lblAddressZipCode, lblZipCodeValue);
        var flxOwnerShip = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOwnerShip",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxOwnerShip"), extendConfig({}, controller.args[1], "flxOwnerShip"), extendConfig({}, controller.args[2], "flxOwnerShip"));
        flxOwnerShip.setDefaultUnit(kony.flex.DP);
        var lblOwnership = new kony.ui.Label(extendConfig({
            "id": "lblOwnership",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.HomeOwnershipCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOwnership"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOwnership"), extendConfig({}, controller.args[2], "lblOwnership"));
        var lblOwnershipValue = new kony.ui.Label(extendConfig({
            "id": "lblOwnershipValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Own and Mortgage Free",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblOwnershipValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOwnershipValue"), extendConfig({}, controller.args[2], "lblOwnershipValue"));
        flxOwnerShip.add(lblOwnership, lblOwnershipValue);
        flxAddressZipOwnership.add(flxAddressZipCode, flxOwnerShip);
        flxAddressInfo.add(flxAddressLine1, flxAddressLine2, flxAddressCountryDtls, flxAddressZipOwnership);
        var flxDuration = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDuration",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxDuration"), extendConfig({}, controller.args[1], "flxDuration"), extendConfig({}, controller.args[2], "flxDuration"));
        flxDuration.setDefaultUnit(kony.flex.DP);
        var flxDurationStay = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDurationStay",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxDurationStay"), extendConfig({}, controller.args[1], "flxDurationStay"), extendConfig({}, controller.args[2], "flxDurationStay"));
        flxDurationStay.setDefaultUnit(kony.flex.DP);
        var lblDuration = new kony.ui.Label(extendConfig({
            "id": "lblDuration",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.DurationOfStayCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDuration"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDuration"), extendConfig({}, controller.args[2], "lblDuration"));
        var lblDurationValue = new kony.ui.Label(extendConfig({
            "id": "lblDurationValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Duration Of Stay",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblDurationValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDurationValue"), extendConfig({}, controller.args[2], "lblDurationValue"));
        flxDurationStay.add(lblDuration, lblDurationValue);
        flxDuration.add(flxDurationStay);
        var flxPreviousAddress = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPreviousAddress",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPreviousAddress"), extendConfig({}, controller.args[1], "flxPreviousAddress"), extendConfig({}, controller.args[2], "flxPreviousAddress"));
        flxPreviousAddress.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader1 = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "loansSectionHeader1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "right": "0dp"
                },
                "flxSectionHeader": {
                    "left": "0dp",
                    "right": "0dp"
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PreviousAddress\")",
                    "left": "0dp",
                    "top": "0dp"
                },
                "lblSeparator": {
                    "left": "0dp",
                    "right": "0dp"
                },
                "loansSectionHeader": {
                    "left": "0dp"
                }
            }
        }, controller.args[0], "loansSectionHeader1"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader1"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader1"));
        var flxPrevAddressInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressInfo"), extendConfig({}, controller.args[1], "flxPrevAddressInfo"), extendConfig({}, controller.args[2], "flxPrevAddressInfo"));
        flxPrevAddressInfo.setDefaultUnit(kony.flex.DP);
        var flxPrevAddressLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressLine"), extendConfig({}, controller.args[1], "flxPrevAddressLine"), extendConfig({}, controller.args[2], "flxPrevAddressLine"));
        flxPrevAddressLine.setDefaultUnit(kony.flex.DP);
        var lblPervAddressLine1 = new kony.ui.Label(extendConfig({
            "id": "lblPervAddressLine1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.AddressLine1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPervAddressLine1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPervAddressLine1"), extendConfig({}, controller.args[2], "lblPervAddressLine1"));
        var lblPervAddressLine1Value = new kony.ui.Label(extendConfig({
            "id": "lblPervAddressLine1Value",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Address Line 1",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPervAddressLine1Value"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPervAddressLine1Value"), extendConfig({}, controller.args[2], "lblPervAddressLine1Value"));
        flxPrevAddressLine.add(lblPervAddressLine1, lblPervAddressLine1Value);
        var flxPrevAddressLine2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressLine2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressLine2"), extendConfig({}, controller.args[1], "flxPrevAddressLine2"), extendConfig({}, controller.args[2], "flxPrevAddressLine2"));
        flxPrevAddressLine2.setDefaultUnit(kony.flex.DP);
        var lblPervAddressLine2 = new kony.ui.Label(extendConfig({
            "id": "lblPervAddressLine2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.AddressLine2CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPervAddressLine2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPervAddressLine2"), extendConfig({}, controller.args[2], "lblPervAddressLine2"));
        var lblPervAddressLine2Value = new kony.ui.Label(extendConfig({
            "id": "lblPervAddressLine2Value",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Address Line 2",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPervAddressLine2Value"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPervAddressLine2Value"), extendConfig({}, controller.args[2], "lblPervAddressLine2Value"));
        flxPrevAddressLine2.add(lblPervAddressLine2, lblPervAddressLine2Value);
        var flxPrevAddressCountryDtls = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressCountryDtls",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressCountryDtls"), extendConfig({}, controller.args[1], "flxPrevAddressCountryDtls"), extendConfig({}, controller.args[2], "flxPrevAddressCountryDtls"));
        flxPrevAddressCountryDtls.setDefaultUnit(kony.flex.DP);
        var flxPrevAddressCountry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressCountry",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressCountry"), extendConfig({}, controller.args[1], "flxPrevAddressCountry"), extendConfig({}, controller.args[2], "flxPrevAddressCountry"));
        flxPrevAddressCountry.setDefaultUnit(kony.flex.DP);
        var lblPrevAddress = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddress",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.Country\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddress"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddress"), extendConfig({}, controller.args[2], "lblPrevAddress"));
        var lblPrevCountryValue = new kony.ui.Label(extendConfig({
            "id": "lblPrevCountryValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Contry",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrevCountryValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevCountryValue"), extendConfig({}, controller.args[2], "lblPrevCountryValue"));
        flxPrevAddressCountry.add(lblPrevAddress, lblPrevCountryValue);
        var flxPrevAddressState = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxPrevAddressState",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressState"), extendConfig({}, controller.args[1], "flxPrevAddressState"), extendConfig({}, controller.args[2], "flxPrevAddressState"));
        flxPrevAddressState.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressState = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressState",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.State\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressState"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressState"), extendConfig({}, controller.args[2], "lblPrevAddressState"));
        var lblPrevAddressStateValue = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressStateValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "State",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressStateValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressStateValue"), extendConfig({}, controller.args[2], "lblPrevAddressStateValue"));
        flxPrevAddressState.add(lblPrevAddressState, lblPrevAddressStateValue);
        var flxPrevAddressCity = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressCity",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressCity"), extendConfig({}, controller.args[1], "flxPrevAddressCity"), extendConfig({}, controller.args[2], "flxPrevAddressCity"));
        flxPrevAddressCity.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressCity = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressCity",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.City\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressCity"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressCity"), extendConfig({}, controller.args[2], "lblPrevAddressCity"));
        var lblPrevAddressCityValue = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressCityValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "City",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressCityValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressCityValue"), extendConfig({}, controller.args[2], "lblPrevAddressCityValue"));
        flxPrevAddressCity.add(lblPrevAddressCity, lblPrevAddressCityValue);
        flxPrevAddressCountryDtls.add(flxPrevAddressCountry, flxPrevAddressState, flxPrevAddressCity);
        var flxPrevAddressZipOwnership = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressZipOwnership",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxPrevAddressZipOwnership"), extendConfig({}, controller.args[1], "flxPrevAddressZipOwnership"), extendConfig({}, controller.args[2], "flxPrevAddressZipOwnership"));
        flxPrevAddressZipOwnership.setDefaultUnit(kony.flex.DP);
        var flxPrevAddressZipCode = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressZipCode",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressZipCode"), extendConfig({}, controller.args[1], "flxPrevAddressZipCode"), extendConfig({}, controller.args[2], "flxPrevAddressZipCode"));
        flxPrevAddressZipCode.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressZipCode = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressZipCode",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.ZipCode\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressZipCode"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressZipCode"), extendConfig({}, controller.args[2], "lblPrevAddressZipCode"));
        var lblPrevAddressZipCodeValue = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressZipCodeValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "zip code",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressZipCodeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressZipCodeValue"), extendConfig({}, controller.args[2], "lblPrevAddressZipCodeValue"));
        flxPrevAddressZipCode.add(lblPrevAddressZipCode, lblPrevAddressZipCodeValue);
        flxPrevAddressZipOwnership.add(flxPrevAddressZipCode);
        flxPrevAddressInfo.add(flxPrevAddressLine, flxPrevAddressLine2, flxPrevAddressCountryDtls, flxPrevAddressZipOwnership);
        flxPreviousAddress.add(loansSectionHeader1, flxPrevAddressInfo);
        flxAddressSection.add(flxAddressInfo, flxDuration, flxPreviousAddress);
        viewAddressInfo.add(loansSectionHeader, flxAddressSection);
        return viewAddressInfo;
    }
})