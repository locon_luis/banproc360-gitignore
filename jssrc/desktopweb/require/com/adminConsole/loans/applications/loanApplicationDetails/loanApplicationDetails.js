define(function() {
    return function(controller) {
        var loanApplicationDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "loanApplicationDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "loanApplicationDetails"), extendConfig({}, controller.args[1], "loanApplicationDetails"), extendConfig({}, controller.args[2], "loanApplicationDetails"));
        loanApplicationDetails.setDefaultUnit(kony.flex.DP);
        var flxHeaderAndBackNav = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxHeaderAndBackNav",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeaderAndBackNav"), extendConfig({}, controller.args[1], "flxHeaderAndBackNav"), extendConfig({}, controller.args[2], "flxHeaderAndBackNav"));
        flxHeaderAndBackNav.setDefaultUnit(kony.flex.DP);
        var lblTrackApplicationHeader = new kony.ui.Label(extendConfig({
            "id": "lblTrackApplicationHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg33333315px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.trackApplication\")",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 10
        }, controller.args[0], "lblTrackApplicationHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTrackApplicationHeader"), extendConfig({}, controller.args[2], "lblTrackApplicationHeader"));
        var flxReturnToLoans = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxReturnToLoans",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "105dp"
        }, controller.args[0], "flxReturnToLoans"), extendConfig({}, controller.args[1], "flxReturnToLoans"), extendConfig({}, controller.args[2], "flxReturnToLoans"));
        flxReturnToLoans.setDefaultUnit(kony.flex.DP);
        var lblReturnToLoansIcon = new kony.ui.Label(extendConfig({
            "id": "lblReturnToLoansIcon",
            "isVisible": true,
            "skin": "sknBackIcon3ebaed16PX",
            "text": "",
            "top": "5dp",
            "width": "20dp",
            "zIndex": 10
        }, controller.args[0], "lblReturnToLoansIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblReturnToLoansIcon"), extendConfig({}, controller.args[2], "lblReturnToLoansIcon"));
        var lblReturnToLoans = new kony.ui.Label(extendConfig({
            "id": "lblReturnToLoans",
            "isVisible": true,
            "skin": "sknLbl117eb013pxHov",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.BackToLoans\")",
            "top": "5dp",
            "width": "85dp",
            "zIndex": 1
        }, controller.args[0], "lblReturnToLoans"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblReturnToLoans"), extendConfig({}, controller.args[2], "lblReturnToLoans"));
        flxReturnToLoans.add(lblReturnToLoansIcon, lblReturnToLoans);
        flxHeaderAndBackNav.add(lblTrackApplicationHeader, flxReturnToLoans);
        var flxLoansApplicationHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxLoansApplicationHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "5dp",
            "zIndex": 10
        }, controller.args[0], "flxLoansApplicationHeader"), extendConfig({}, controller.args[1], "flxLoansApplicationHeader"), extendConfig({}, controller.args[2], "flxLoansApplicationHeader"));
        flxLoansApplicationHeader.setDefaultUnit(kony.flex.DP);
        var lblApplicationIdStatic = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblApplicationIdStatic",
            "isVisible": true,
            "left": "0dp",
            "skin": "skn11px9CA9BA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.ApplicationId\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "lblApplicationIdStatic"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApplicationIdStatic"), extendConfig({}, controller.args[2], "lblApplicationIdStatic"));
        var lblApplicationId = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblApplicationId",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlbl485c7514px",
            "text": "NA",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "lblApplicationId"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApplicationId"), extendConfig({}, controller.args[2], "lblApplicationId"));
        var lblSeparator = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlbl485c75Border",
            "top": "33dp",
            "width": "1dp",
            "zIndex": 10
        }, controller.args[0], "lblSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator"), extendConfig({}, controller.args[2], "lblSeparator"));
        var lblCreatedDate = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCreatedDate",
            "isVisible": true,
            "left": "10dp",
            "skin": "skn11px9CA9BA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.SubmittedOn\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "lblCreatedDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreatedDate"), extendConfig({}, controller.args[2], "lblCreatedDate"));
        var lblCreatedDateValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCreatedDateValue",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlbl485c7514px",
            "text": "NA",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "lblCreatedDateValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreatedDateValue"), extendConfig({}, controller.args[2], "lblCreatedDateValue"));
        var lblSeparator2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "lblSeparator2",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlbl485c75Border",
            "top": "33dp",
            "width": "1dp",
            "zIndex": 10
        }, controller.args[0], "lblSeparator2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator2"), extendConfig({}, controller.args[2], "lblSeparator2"));
        var lblModDate = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblModDate",
            "isVisible": true,
            "left": "10dp",
            "skin": "skn11px9CA9BA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.ModifiedOn\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "lblModDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblModDate"), extendConfig({}, controller.args[2], "lblModDate"));
        var lblModDateValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblModDateValue",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlbl485c7514px",
            "text": "NA",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "lblModDateValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblModDateValue"), extendConfig({}, controller.args[2], "lblModDateValue"));
        flxLoansApplicationHeader.add(lblApplicationIdStatic, lblApplicationId, lblSeparator, lblCreatedDate, lblCreatedDateValue, lblSeparator2, lblModDate, lblModDateValue);
        loanApplicationDetails.add(flxHeaderAndBackNav, flxLoansApplicationHeader);
        return loanApplicationDetails;
    }
})