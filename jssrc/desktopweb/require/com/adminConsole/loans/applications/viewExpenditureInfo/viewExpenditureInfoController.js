define("com/adminConsole/loans/applications/viewExpenditureInfo/userviewExpenditureInfoController", function() {
    return {};
});
define("com/adminConsole/loans/applications/viewExpenditureInfo/viewExpenditureInfoControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnEdit **/
    AS_Button_b4630188e8cb4e1788612fb028603a0f: function AS_Button_b4630188e8cb4e1788612fb028603a0f(eventobject) {
        var self = this;
        var self = this;
        var parentId = eventobject.parent.parent.parent.id;
    }
});
define("com/adminConsole/loans/applications/viewExpenditureInfo/viewExpenditureInfoController", ["com/adminConsole/loans/applications/viewExpenditureInfo/userviewExpenditureInfoController", "com/adminConsole/loans/applications/viewExpenditureInfo/viewExpenditureInfoControllerActions"], function() {
    var controller = require("com/adminConsole/loans/applications/viewExpenditureInfo/userviewExpenditureInfoController");
    var actions = require("com/adminConsole/loans/applications/viewExpenditureInfo/viewExpenditureInfoControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
