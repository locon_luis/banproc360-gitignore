define(function() {
    return function(controller) {
        var viewLoaninformation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewLoaninformation",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "viewLoaninformation"), extendConfig({}, controller.args[1], "viewLoaninformation"), extendConfig({}, controller.args[2], "viewLoaninformation"));
        viewLoaninformation.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.LoanInfo\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxMainDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMainDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxMainDetails"), extendConfig({}, controller.args[1], "flxMainDetails"), extendConfig({}, controller.args[2], "flxMainDetails"));
        flxMainDetails.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAmount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxAmount"), extendConfig({}, controller.args[1], "flxAmount"), extendConfig({}, controller.args[2], "flxAmount"));
        flxAmount.setDefaultUnit(kony.flex.DP);
        var lblAmountRequested = new kony.ui.Label(extendConfig({
            "id": "lblAmountRequested",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Summary.AmountRequested\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAmountRequested"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmountRequested"), extendConfig({}, controller.args[2], "lblAmountRequested"));
        var lblAmountRequestedValue = new kony.ui.Label(extendConfig({
            "id": "lblAmountRequestedValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "$2000",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblAmountRequestedValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmountRequestedValue"), extendConfig({}, controller.args[2], "lblAmountRequestedValue"));
        flxAmount.add(lblAmountRequested, lblAmountRequestedValue);
        var flxCardType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCardType",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxCardType"), extendConfig({}, controller.args[1], "flxCardType"), extendConfig({}, controller.args[2], "flxCardType"));
        flxCardType.setDefaultUnit(kony.flex.DP);
        var lblCardType = new kony.ui.Label(extendConfig({
            "id": "lblCardType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "text": "CARD TYPE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCardType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCardType"), extendConfig({}, controller.args[2], "lblCardType"));
        var lblCardTypeValue = new kony.ui.Label(extendConfig({
            "id": "lblCardTypeValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "DBX Cash Rewards Card",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblCardTypeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCardTypeValue"), extendConfig({}, controller.args[2], "lblCardTypeValue"));
        flxCardType.add(lblCardType, lblCardTypeValue);
        var flxLoanTerm = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLoanTerm",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "30%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxLoanTerm"), extendConfig({}, controller.args[1], "flxLoanTerm"), extendConfig({}, controller.args[2], "flxLoanTerm"));
        flxLoanTerm.setDefaultUnit(kony.flex.DP);
        var lblLoanTerm = new kony.ui.Label(extendConfig({
            "id": "lblLoanTerm",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.LoanTermCAPS\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanTerm"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanTerm"), extendConfig({}, controller.args[2], "lblLoanTerm"));
        var lblLoanTermValue = new kony.ui.Label(extendConfig({
            "id": "lblLoanTermValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "12 Months",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblLoanTermValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanTermValue"), extendConfig({}, controller.args[2], "lblLoanTermValue"));
        flxLoanTerm.add(lblLoanTerm, lblLoanTermValue);
        var flxCreditLimit = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCreditLimit",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "30%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxCreditLimit"), extendConfig({}, controller.args[1], "flxCreditLimit"), extendConfig({}, controller.args[2], "flxCreditLimit"));
        flxCreditLimit.setDefaultUnit(kony.flex.DP);
        var lblCreditLimit = new kony.ui.Label(extendConfig({
            "id": "lblCreditLimit",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "text": "CREDIT LIMIT",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCreditLimit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreditLimit"), extendConfig({}, controller.args[2], "lblCreditLimit"));
        var lblCreditLimitValue = new kony.ui.Label(extendConfig({
            "id": "lblCreditLimitValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "12 Months",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblCreditLimitValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreditLimitValue"), extendConfig({}, controller.args[2], "lblCreditLimitValue"));
        flxCreditLimit.add(lblCreditLimit, lblCreditLimitValue);
        var flxPurposeCollateral = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPurposeCollateral",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "60%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxPurposeCollateral"), extendConfig({}, controller.args[1], "flxPurposeCollateral"), extendConfig({}, controller.args[2], "flxPurposeCollateral"));
        flxPurposeCollateral.setDefaultUnit(kony.flex.DP);
        var lblPurposeCollateral = new kony.ui.Label(extendConfig({
            "id": "lblPurposeCollateral",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.PurposeCollateralCAPS\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPurposeCollateral"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPurposeCollateral"), extendConfig({}, controller.args[2], "lblPurposeCollateral"));
        var lblLoanPurposeValue = new kony.ui.Label(extendConfig({
            "id": "lblLoanPurposeValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Home Improvement",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblLoanPurposeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanPurposeValue"), extendConfig({}, controller.args[2], "lblLoanPurposeValue"));
        flxPurposeCollateral.add(lblPurposeCollateral, lblLoanPurposeValue);
        var flxVehicleIdentificationNumber = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxVehicleIdentificationNumber",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "30%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "38%",
            "zIndex": 1
        }, controller.args[0], "flxVehicleIdentificationNumber"), extendConfig({}, controller.args[1], "flxVehicleIdentificationNumber"), extendConfig({}, controller.args[2], "flxVehicleIdentificationNumber"));
        flxVehicleIdentificationNumber.setDefaultUnit(kony.flex.DP);
        var lblVehicleIdentificationNumber = new kony.ui.Label(extendConfig({
            "id": "lblVehicleIdentificationNumber",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "text": "DO YOU HAVE A VEHICLE IDENTIFICATION  NUMBER",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblVehicleIdentificationNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleIdentificationNumber"), extendConfig({}, controller.args[2], "lblVehicleIdentificationNumber"));
        var lblVehicleIdentificationNumberValue = new kony.ui.Label(extendConfig({
            "id": "lblVehicleIdentificationNumberValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "YES",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblVehicleIdentificationNumberValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleIdentificationNumberValue"), extendConfig({}, controller.args[2], "lblVehicleIdentificationNumberValue"));
        flxVehicleIdentificationNumber.add(lblVehicleIdentificationNumber, lblVehicleIdentificationNumberValue);
        flxRow1.add(flxAmount, flxCardType, flxLoanTerm, flxCreditLimit, flxPurposeCollateral, flxVehicleIdentificationNumber);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxVehicleType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxVehicleType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxVehicleType"), extendConfig({}, controller.args[1], "flxVehicleType"), extendConfig({}, controller.args[2], "flxVehicleType"));
        flxVehicleType.setDefaultUnit(kony.flex.DP);
        var lblVehicleType = new kony.ui.Label(extendConfig({
            "id": "lblVehicleType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "text": "VEHICLE TYPE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblVehicleType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleType"), extendConfig({}, controller.args[2], "lblVehicleType"));
        var lblVehicleTypeValue = new kony.ui.Label(extendConfig({
            "id": "lblVehicleTypeValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Recreational Vehicle",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblVehicleTypeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleTypeValue"), extendConfig({}, controller.args[2], "lblVehicleTypeValue"));
        flxVehicleType.add(lblVehicleType, lblVehicleTypeValue);
        var flxVehicleReleaseYear = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxVehicleReleaseYear",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxVehicleReleaseYear"), extendConfig({}, controller.args[1], "flxVehicleReleaseYear"), extendConfig({}, controller.args[2], "flxVehicleReleaseYear"));
        flxVehicleReleaseYear.setDefaultUnit(kony.flex.DP);
        var lblVehicleReleaseYear = new kony.ui.Label(extendConfig({
            "id": "lblVehicleReleaseYear",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "text": "VEHICLE RELEASE YEAR",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblVehicleReleaseYear"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleReleaseYear"), extendConfig({}, controller.args[2], "lblVehicleReleaseYear"));
        var lblVehicleReleaseYearValue = new kony.ui.Label(extendConfig({
            "id": "lblVehicleReleaseYearValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "2015",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblVehicleReleaseYearValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleReleaseYearValue"), extendConfig({}, controller.args[2], "lblVehicleReleaseYearValue"));
        flxVehicleReleaseYear.add(lblVehicleReleaseYear, lblVehicleReleaseYearValue);
        var flxMakeYear = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMakeYear",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxMakeYear"), extendConfig({}, controller.args[1], "flxMakeYear"), extendConfig({}, controller.args[2], "flxMakeYear"));
        flxMakeYear.setDefaultUnit(kony.flex.DP);
        var lblMakeYear = new kony.ui.Label(extendConfig({
            "id": "lblMakeYear",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "text": "MAKE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMakeYear"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMakeYear"), extendConfig({}, controller.args[2], "lblMakeYear"));
        var lblMakeYearValue = new kony.ui.Label(extendConfig({
            "id": "lblMakeYearValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "2009",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblMakeYearValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMakeYearValue"), extendConfig({}, controller.args[2], "lblMakeYearValue"));
        flxMakeYear.add(lblMakeYear, lblMakeYearValue);
        flxRow2.add(flxVehicleType, flxVehicleReleaseYear, flxMakeYear);
        var flxRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow3"), extendConfig({}, controller.args[1], "flxRow3"), extendConfig({}, controller.args[2], "flxRow3"));
        flxRow3.setDefaultUnit(kony.flex.DP);
        var flxModel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxModel",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxModel"), extendConfig({}, controller.args[1], "flxModel"), extendConfig({}, controller.args[2], "flxModel"));
        flxModel.setDefaultUnit(kony.flex.DP);
        var lblModel = new kony.ui.Label(extendConfig({
            "id": "lblModel",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "text": "MODEL",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblModel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblModel"), extendConfig({}, controller.args[2], "lblModel"));
        var lblModelValue = new kony.ui.Label(extendConfig({
            "id": "lblModelValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Recreational Vehicle",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblModelValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblModelValue"), extendConfig({}, controller.args[2], "lblModelValue"));
        flxModel.add(lblModel, lblModelValue);
        var flxTrim = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTrim",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxTrim"), extendConfig({}, controller.args[1], "flxTrim"), extendConfig({}, controller.args[2], "flxTrim"));
        flxTrim.setDefaultUnit(kony.flex.DP);
        var lblTrim = new kony.ui.Label(extendConfig({
            "id": "lblTrim",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "text": "TRIM(OPTIONAL)",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTrim"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTrim"), extendConfig({}, controller.args[2], "lblTrim"));
        var lblTrimValue = new kony.ui.Label(extendConfig({
            "id": "lblTrimValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "XL",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblTrimValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTrimValue"), extendConfig({}, controller.args[2], "lblTrimValue"));
        flxTrim.add(lblTrim, lblTrimValue);
        var flxVehicleMileage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxVehicleMileage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxVehicleMileage"), extendConfig({}, controller.args[1], "flxVehicleMileage"), extendConfig({}, controller.args[2], "flxVehicleMileage"));
        flxVehicleMileage.setDefaultUnit(kony.flex.DP);
        var lblVehicleMileage = new kony.ui.Label(extendConfig({
            "id": "lblVehicleMileage",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "text": "VEHICLE MILEAGE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblVehicleMileage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleMileage"), extendConfig({}, controller.args[2], "lblVehicleMileage"));
        var lblVehicleMileageValue = new kony.ui.Label(extendConfig({
            "id": "lblVehicleMileageValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "150000 Miles",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblVehicleMileageValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleMileageValue"), extendConfig({}, controller.args[2], "lblVehicleMileageValue"));
        flxVehicleMileage.add(lblVehicleMileage, lblVehicleMileageValue);
        flxRow3.add(flxModel, flxTrim, flxVehicleMileage);
        var flxRow4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow4"), extendConfig({}, controller.args[1], "flxRow4"), extendConfig({}, controller.args[2], "flxRow4"));
        flxRow4.setDefaultUnit(kony.flex.DP);
        var flxVehicleAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxVehicleAmount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxVehicleAmount"), extendConfig({}, controller.args[1], "flxVehicleAmount"), extendConfig({}, controller.args[2], "flxVehicleAmount"));
        flxVehicleAmount.setDefaultUnit(kony.flex.DP);
        var lblVehicleAmountRequested = new kony.ui.Label(extendConfig({
            "id": "lblVehicleAmountRequested",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Summary.AmountRequested\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblVehicleAmountRequested"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleAmountRequested"), extendConfig({}, controller.args[2], "lblVehicleAmountRequested"));
        var lblVehicleAmountRequestedValue = new kony.ui.Label(extendConfig({
            "id": "lblVehicleAmountRequestedValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "$2000",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblVehicleAmountRequestedValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleAmountRequestedValue"), extendConfig({}, controller.args[2], "lblVehicleAmountRequestedValue"));
        flxVehicleAmount.add(lblVehicleAmountRequested, lblVehicleAmountRequestedValue);
        var flxVehicleTerm = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxVehicleTerm",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxVehicleTerm"), extendConfig({}, controller.args[1], "flxVehicleTerm"), extendConfig({}, controller.args[2], "flxVehicleTerm"));
        flxVehicleTerm.setDefaultUnit(kony.flex.DP);
        var lblVehicleTerm = new kony.ui.Label(extendConfig({
            "id": "lblVehicleTerm",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.LoanTermCAPS\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblVehicleTerm"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleTerm"), extendConfig({}, controller.args[2], "lblVehicleTerm"));
        var lblVehicleTermValue = new kony.ui.Label(extendConfig({
            "id": "lblVehicleTermValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "12 Months",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblVehicleTermValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleTermValue"), extendConfig({}, controller.args[2], "lblVehicleTermValue"));
        flxVehicleTerm.add(lblVehicleTerm, lblVehicleTermValue);
        var flxApplicationType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxApplicationType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxApplicationType"), extendConfig({}, controller.args[1], "flxApplicationType"), extendConfig({}, controller.args[2], "flxApplicationType"));
        flxApplicationType.setDefaultUnit(kony.flex.DP);
        var lblApplicationType = new kony.ui.Label(extendConfig({
            "id": "lblApplicationType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "text": "APPLICATION TYPE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblApplicationType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApplicationType"), extendConfig({}, controller.args[2], "lblApplicationType"));
        var lblApplicationTypeValue = new kony.ui.Label(extendConfig({
            "id": "lblApplicationTypeValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Individual with Co-Applicant",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblApplicationTypeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApplicationTypeValue"), extendConfig({}, controller.args[2], "lblApplicationTypeValue"));
        flxApplicationType.add(lblApplicationType, lblApplicationTypeValue);
        flxRow4.add(flxVehicleAmount, flxVehicleTerm, flxApplicationType);
        flxMainDetails.add(flxRow1, flxRow2, flxRow3, flxRow4);
        viewLoaninformation.add(loansSectionHeader, flxMainDetails);
        return viewLoaninformation;
    }
})