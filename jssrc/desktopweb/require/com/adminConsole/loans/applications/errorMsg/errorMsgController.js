define("com/adminConsole/loans/applications/errorMsg/usererrorMsgController", function() {
    return {};
});
define("com/adminConsole/loans/applications/errorMsg/errorMsgControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/loans/applications/errorMsg/errorMsgController", ["com/adminConsole/loans/applications/errorMsg/usererrorMsgController", "com/adminConsole/loans/applications/errorMsg/errorMsgControllerActions"], function() {
    var controller = require("com/adminConsole/loans/applications/errorMsg/usererrorMsgController");
    var actions = require("com/adminConsole/loans/applications/errorMsg/errorMsgControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
