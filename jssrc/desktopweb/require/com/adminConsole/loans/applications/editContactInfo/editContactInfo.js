define(function() {
    return function(controller) {
        var editContactInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "editContactInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "editContactInfo"), extendConfig({}, controller.args[1], "editContactInfo"), extendConfig({}, controller.args[2], "editContactInfo"));
        editContactInfo.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.ContactDetails\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxSection"), extendConfig({}, controller.args[1], "flxSection"), extendConfig({}, controller.args[2], "flxSection"));
        flxSection.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 30,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxHomePhone = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHomePhone",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "49%",
            "zIndex": 1
        }, controller.args[0], "flxHomePhone"), extendConfig({}, controller.args[1], "flxHomePhone"), extendConfig({}, controller.args[2], "flxHomePhone"));
        flxHomePhone.setDefaultUnit(kony.flex.DP);
        var flxHomePhoneLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHomePhoneLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHomePhoneLabel"), extendConfig({}, controller.args[1], "flxHomePhoneLabel"), extendConfig({}, controller.args[2], "flxHomePhoneLabel"));
        flxHomePhoneLabel.setDefaultUnit(kony.flex.DP);
        var lblHomePhone = new kony.ui.Label(extendConfig({
            "id": "lblHomePhone",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.HomePhoneOptional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHomePhone"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHomePhone"), extendConfig({}, controller.args[2], "lblHomePhone"));
        var lblHomePhoneSize = new kony.ui.Label(extendConfig({
            "id": "lblHomePhoneSize",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/10",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblHomePhoneSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHomePhoneSize"), extendConfig({}, controller.args[2], "lblHomePhoneSize"));
        flxHomePhoneLabel.add(lblHomePhone, lblHomePhoneSize);
        var txtHomePhoneValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtHomePhoneValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0px",
            "maxTextLength": 10,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.HomePhone\")",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtHomePhoneValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtHomePhoneValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtHomePhoneValue"));
        var homePhoneErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "homePhoneErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "homePhoneErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "homePhoneErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "homePhoneErrorMsg"));
        flxHomePhone.add(flxHomePhoneLabel, txtHomePhoneValue, homePhoneErrorMsg);
        var flxCellphone = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCellphone",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "49%",
            "zIndex": 1
        }, controller.args[0], "flxCellphone"), extendConfig({}, controller.args[1], "flxCellphone"), extendConfig({}, controller.args[2], "flxCellphone"));
        flxCellphone.setDefaultUnit(kony.flex.DP);
        var flxCellphoneLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCellphoneLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxCellphoneLabel"), extendConfig({}, controller.args[1], "flxCellphoneLabel"), extendConfig({}, controller.args[2], "flxCellphoneLabel"));
        flxCellphoneLabel.setDefaultUnit(kony.flex.DP);
        var lblCellPhone = new kony.ui.Label(extendConfig({
            "id": "lblCellPhone",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CellPhone\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCellPhone"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCellPhone"), extendConfig({}, controller.args[2], "lblCellPhone"));
        var lblCellPhoneSize = new kony.ui.Label(extendConfig({
            "id": "lblCellPhoneSize",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/10",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblCellPhoneSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCellPhoneSize"), extendConfig({}, controller.args[2], "lblCellPhoneSize"));
        flxCellphoneLabel.add(lblCellPhone, lblCellPhoneSize);
        var txtCellPhoneValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtCellPhoneValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0px",
            "maxTextLength": 10,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CellPhone\")",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtCellPhoneValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCellPhoneValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtCellPhoneValue"));
        var cellPhoneErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "cellPhoneErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "cellPhoneErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "cellPhoneErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "cellPhoneErrorMsg"));
        flxCellphone.add(flxCellphoneLabel, txtCellPhoneValue, cellPhoneErrorMsg);
        flxRow1.add(flxHomePhone, flxCellphone);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxBusinessPhone = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBusinessPhone",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "49%",
            "zIndex": 1
        }, controller.args[0], "flxBusinessPhone"), extendConfig({}, controller.args[1], "flxBusinessPhone"), extendConfig({}, controller.args[2], "flxBusinessPhone"));
        flxBusinessPhone.setDefaultUnit(kony.flex.DP);
        var flxBusinessPhoneLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBusinessPhoneLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxBusinessPhoneLabel"), extendConfig({}, controller.args[1], "flxBusinessPhoneLabel"), extendConfig({}, controller.args[2], "flxBusinessPhoneLabel"));
        flxBusinessPhoneLabel.setDefaultUnit(kony.flex.DP);
        var lblBusinessPhone = new kony.ui.Label(extendConfig({
            "id": "lblBusinessPhone",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.BusinessPhone\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBusinessPhone"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBusinessPhone"), extendConfig({}, controller.args[2], "lblBusinessPhone"));
        var lblBusinessPhoneSize = new kony.ui.Label(extendConfig({
            "id": "lblBusinessPhoneSize",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/10",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblBusinessPhoneSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBusinessPhoneSize"), extendConfig({}, controller.args[2], "lblBusinessPhoneSize"));
        flxBusinessPhoneLabel.add(lblBusinessPhone, lblBusinessPhoneSize);
        var txtBusinessPhoneValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtBusinessPhoneValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0px",
            "maxTextLength": 10,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.OfficePhone\")",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtBusinessPhoneValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtBusinessPhoneValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtBusinessPhoneValue"));
        var businessPhoneErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "businessPhoneErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "businessPhoneErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "businessPhoneErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "businessPhoneErrorMsg"));
        flxBusinessPhone.add(flxBusinessPhoneLabel, txtBusinessPhoneValue, businessPhoneErrorMsg);
        var flxPrimaryContactMethod = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrimaryContactMethod",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "49%",
            "zIndex": 1
        }, controller.args[0], "flxPrimaryContactMethod"), extendConfig({}, controller.args[1], "flxPrimaryContactMethod"), extendConfig({}, controller.args[2], "flxPrimaryContactMethod"));
        flxPrimaryContactMethod.setDefaultUnit(kony.flex.DP);
        var lblPrimaryContact = new kony.ui.Label(extendConfig({
            "id": "lblPrimaryContact",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.PrimaryContactMethod\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrimaryContact"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrimaryContact"), extendConfig({}, controller.args[2], "lblPrimaryContact"));
        var lstPrimaryContactValue = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstPrimaryContactValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstPrimaryContactValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstPrimaryContactValue"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstPrimaryContactValue"));
        var primaryContactErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "primaryContactErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "primaryContactErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "primaryContactErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "primaryContactErrorMsg"));
        flxPrimaryContactMethod.add(lblPrimaryContact, lstPrimaryContactValue, primaryContactErrorMsg);
        flxRow2.add(flxBusinessPhone, flxPrimaryContactMethod);
        flxSection.add(flxRow1, flxRow2);
        editContactInfo.add(loansSectionHeader, flxSection);
        return editContactInfo;
    }
})