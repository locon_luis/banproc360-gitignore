define("com/adminConsole/loans/applications/viewEmployerDetails/userviewEmployerDetailsController", function() {
    return {
        showEmployementDetails: function(employmentType, hasPreviousEmployment, payperiod) {
            var scopeObj = this;
            switch (employmentType) {
                case kony.i18n.getLocalizedString("i18n.frmTrackApplication.SelfEmployed"):
                    scopeObj.view.loansSectionHeader.lblSectionHeader.text = kony.i18n.getLocalizedString("i18n.frmTrackApplication.SelfEmploymentDetails");
                    scopeObj.view.lblEmployerName.text = kony.i18n.getLocalizedString("i18n.frmTrackApplication.BusinessNameCAPS");
                    scopeObj.view.flxRowMilitary.setVisibility(false);
                    scopeObj.view.flxOtherEmp.setVisibility(false);
                    scopeObj.view.flxDesignation.setVisibility(false);
                    scopeObj.view.flxPresentEmployment.setVisibility(true);
                    scopeObj.view.flxEmployerDtls.setVisibility(true);
                    scopeObj.view.flxPreviousEmpDtls.setVisibility(false);
                    scopeObj.view.flxPreviousEmployer.setVisibility(false);
                    break;
                case kony.i18n.getLocalizedString("i18n.frmTrackApplication.Military"):
                    scopeObj.view.loansSectionHeader.lblSectionHeader.text = kony.i18n.getLocalizedString("i18n.frmTrackApplication.MilitaryEmploymentDetails");
                    scopeObj.view.flxRowMilitary.setVisibility(true);
                    scopeObj.view.flxOtherEmp.setVisibility(false);
                    scopeObj.view.flxEmployerDtls.setVisibility(true);
                    scopeObj.view.flxPresentEmployment.setVisibility(false);
                    scopeObj.view.flxPreviousEmployer.setVisibility(false);
                    scopeObj.view.flxPreviousEmpDtls.setVisibility(false);
                    break;
                case kony.i18n.getLocalizedString("i18n.frmTrackApplication.Unemployed"):
                    scopeObj.view.flxEmployerDtls.setVisibility(false);
                    scopeObj.view.flxPreviousEmpDtls.setVisibility(false);
                    scopeObj.view.flxPreviousEmployer.setVisibility(false);
                    break;
                case kony.i18n.getLocalizedString("i18n.frmTrackApplication.Other"):
                    scopeObj.view.loansSectionHeader.lblSectionHeader.text = kony.i18n.getLocalizedString("i18n.frmTrackApplication.OtherEmployment");
                    scopeObj.view.flxRowMilitary.setVisibility(false);
                    scopeObj.view.flxOtherEmp.setVisibility(true);
                    scopeObj.view.flxEmployerDtls.setVisibility(true);
                    scopeObj.view.flxPreviousEmpDtls.setVisibility(false);
                    scopeObj.view.flxPresentEmployment.setVisibility(false);
                    scopeObj.view.flxPreviousEmployer.setVisibility(false);
                    break;
                default:
                    scopeObj.view.loansSectionHeader.lblSectionHeader.text = kony.i18n.getLocalizedString("i18n.frmTrackApplication.PresentEmployerDetails");
                    scopeObj.view.lblEmployerName.text = kony.i18n.getLocalizedString("i18n.frmTrackApplication.EmployerNameCAPS");
                    scopeObj.view.flxRowMilitary.setVisibility(false);
                    scopeObj.view.flxOtherEmp.setVisibility(false);
                    scopeObj.view.flxDesignation.setVisibility(true);
                    scopeObj.view.flxPresentEmployment.setVisibility(true);
                    scopeObj.view.flxEmployerDtls.setVisibility(true);
                    scopeObj.view.flxPreviousEmpDtls.setVisibility(true);
                    scopeObj.view.flxPreviousEmployer.setVisibility(hasPreviousEmployment);
                    break;
            }
            scopeObj.view.flxTotalWorkingHrs.setVisibility(payperiod !== undefined && payperiod.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmTrackApplication.Hourly").toUpperCase());
        }
    };
});
define("com/adminConsole/loans/applications/viewEmployerDetails/viewEmployerDetailsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnEdit **/
    AS_Button_b4630188e8cb4e1788612fb028603a0f: function AS_Button_b4630188e8cb4e1788612fb028603a0f(eventobject) {
        var self = this;
        var self = this;
        var parentId = eventobject.parent.parent.parent.id;
    }
});
define("com/adminConsole/loans/applications/viewEmployerDetails/viewEmployerDetailsController", ["com/adminConsole/loans/applications/viewEmployerDetails/userviewEmployerDetailsController", "com/adminConsole/loans/applications/viewEmployerDetails/viewEmployerDetailsControllerActions"], function() {
    var controller = require("com/adminConsole/loans/applications/viewEmployerDetails/userviewEmployerDetailsController");
    var actions = require("com/adminConsole/loans/applications/viewEmployerDetails/viewEmployerDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
