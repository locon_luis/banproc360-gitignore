define(function() {
    return function(controller) {
        var viewApplicantDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewApplicantDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "viewApplicantDetails"), extendConfig({}, controller.args[1], "viewApplicantDetails"), extendConfig({}, controller.args[2], "viewApplicantDetails"));
        viewApplicantDetails.setDefaultUnit(kony.flex.DP);
        var lblApplicantHeader = new kony.ui.Label(extendConfig({
            "id": "lblApplicantHeader",
            "isVisible": true,
            "left": "20dp",
            "skin": "LblLatoRegular485c7516px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Summary.LoanInfo.ApplicantDetails\")",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "lblApplicantHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApplicantHeader"), extendConfig({}, controller.args[2], "lblApplicantHeader"));
        var viewPersonalInfo = new com.adminConsole.loans.applications.viewPersonalInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "viewPersonalInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "viewPersonalInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "viewPersonalInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "viewPersonalInfo"));
        var viewContactInfo = new com.adminConsole.loans.applications.viewContactInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "viewContactInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "viewContactInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "viewContactInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "viewContactInfo"));
        var viewAddressInfo = new com.adminConsole.loans.applications.viewAddressInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "viewAddressInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "viewAddressInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "viewAddressInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "viewAddressInfo"));
        var viewIncomeSource = new com.adminConsole.loans.applications.viewIncomeSource(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "viewIncomeSource",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "viewIncomeSource"), extendConfig({
            "overrides": {}
        }, controller.args[1], "viewIncomeSource"), extendConfig({
            "overrides": {}
        }, controller.args[2], "viewIncomeSource"));
        var viewEmployerDetails = new com.adminConsole.loans.applications.viewEmployerDetails(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "viewEmployerDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "viewEmployerDetails"), extendConfig({
            "overrides": {}
        }, controller.args[1], "viewEmployerDetails"), extendConfig({
            "overrides": {}
        }, controller.args[2], "viewEmployerDetails"));
        var viewAdditionalIncome = new com.adminConsole.loans.applications.viewAdditionalIncome(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "viewAdditionalIncome",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "viewAdditionalIncome"), extendConfig({
            "overrides": {}
        }, controller.args[1], "viewAdditionalIncome"), extendConfig({
            "overrides": {}
        }, controller.args[2], "viewAdditionalIncome"));
        var viewExpenditureInfo = new com.adminConsole.loans.applications.viewExpenditureInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "viewExpenditureInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "viewExpenditureInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "viewExpenditureInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "viewExpenditureInfo"));
        viewApplicantDetails.add(lblApplicantHeader, viewPersonalInfo, viewContactInfo, viewAddressInfo, viewIncomeSource, viewEmployerDetails, viewAdditionalIncome, viewExpenditureInfo);
        return viewApplicantDetails;
    }
})