define("com/adminConsole/loans/applications/viewAUDetails/userviewAUDetailsController", function() {
    return {};
});
define("com/adminConsole/loans/applications/viewAUDetails/viewAUDetailsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/loans/applications/viewAUDetails/viewAUDetailsController", ["com/adminConsole/loans/applications/viewAUDetails/userviewAUDetailsController", "com/adminConsole/loans/applications/viewAUDetails/viewAUDetailsControllerActions"], function() {
    var controller = require("com/adminConsole/loans/applications/viewAUDetails/userviewAUDetailsController");
    var actions = require("com/adminConsole/loans/applications/viewAUDetails/viewAUDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
