define(function() {
    return function(controller) {
        var editAUDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "editAUDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "editAUDetails"), extendConfig({}, controller.args[1], "editAUDetails"), extendConfig({}, controller.args[2], "editAUDetails"));
        editAUDetails.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "lblSectionHeader": {
                    "text": "Authorized User"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxAddAU = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddAU",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxAddAU"), extendConfig({}, controller.args[1], "flxAddAU"), extendConfig({}, controller.args[2], "flxAddAU"));
        flxAddAU.setDefaultUnit(kony.flex.DP);
        var flxAddAUQuestion = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddAUQuestion",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddAUQuestion"), extendConfig({}, controller.args[1], "flxAddAUQuestion"), extendConfig({}, controller.args[2], "flxAddAUQuestion"));
        flxAddAUQuestion.setDefaultUnit(kony.flex.DP);
        var lblAddAU = new kony.ui.Label(extendConfig({
            "id": "lblAddAU",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "text": "WOULD YOU LIKE TO ADD AUTHORIZED USER",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddAU"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddAU"), extendConfig({}, controller.args[2], "lblAddAU"));
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({}, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var flxRadioButton1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRadioButton1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "55dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton1"), extendConfig({}, controller.args[1], "flxRadioButton1"), extendConfig({}, controller.args[2], "flxRadioButton1"));
        flxRadioButton1.setDefaultUnit(kony.flex.DP);
        var imgRadioButton1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadioButton1",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton1"), extendConfig({}, controller.args[2], "imgRadioButton1"));
        var lblRadioButtonValue1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRadioButtonValue1",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Yes",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue1"), extendConfig({}, controller.args[2], "lblRadioButtonValue1"));
        flxRadioButton1.add(imgRadioButton1, lblRadioButtonValue1);
        var flxRadioButton2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRadioButton2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton2"), extendConfig({}, controller.args[1], "flxRadioButton2"), extendConfig({}, controller.args[2], "flxRadioButton2"));
        flxRadioButton2.setDefaultUnit(kony.flex.DP);
        var imgRadioButton2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadioButton2",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton2"), extendConfig({}, controller.args[2], "imgRadioButton2"));
        var lblRadioButtonValue2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRadioButtonValue2",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue2"), extendConfig({}, controller.args[2], "lblRadioButtonValue2"));
        flxRadioButton2.add(imgRadioButton2, lblRadioButtonValue2);
        flxOptions.add(flxRadioButton1, flxRadioButton2);
        flxAddAUQuestion.add(lblAddAU, flxOptions);
        flxAddAU.add(flxAddAUQuestion);
        var editAUPersonalInfo = new com.adminConsole.loans.applications.editPersonalInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "editAUPersonalInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {}
        }, controller.args[0], "editAUPersonalInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editAUPersonalInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editAUPersonalInfo"));
        var editAUContactInfo = new com.adminConsole.loans.applications.editContactInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "editAUContactInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {}
        }, controller.args[0], "editAUContactInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editAUContactInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editAUContactInfo"));
        var editAddressInfoAsPrevious = new com.adminConsole.loans.applications.editAddressInfoAsPrevious(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "editAddressInfoAsPrevious",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {}
        }, controller.args[0], "editAddressInfoAsPrevious"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editAddressInfoAsPrevious"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editAddressInfoAsPrevious"));
        editAUDetails.add(loansSectionHeader, flxAddAU, editAUPersonalInfo, editAUContactInfo, editAddressInfoAsPrevious);
        return editAUDetails;
    }
})