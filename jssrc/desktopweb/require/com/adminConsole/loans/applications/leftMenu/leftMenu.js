define(function() {
    return function(controller) {
        var leftMenu = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "leftMenu",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "leftMenu"), extendConfig({}, controller.args[1], "leftMenu"), extendConfig({}, controller.args[2], "leftMenu"));
        leftMenu.setDefaultUnit(kony.flex.DP);
        var flxLoanDocumentImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLoanDocumentImage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknf8f9faNoBorder",
            "top": "0dp",
            "width": "20%"
        }, controller.args[0], "flxLoanDocumentImage"), extendConfig({}, controller.args[1], "flxLoanDocumentImage"), extendConfig({}, controller.args[2], "flxLoanDocumentImage"));
        flxLoanDocumentImage.setDefaultUnit(kony.flex.DP);
        var lblLoanDoc = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblLoanDoc",
            "isVisible": true,
            "skin": "sknLblIcomoon28px485c75",
            "text": "",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanDoc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanDoc"), extendConfig({}, controller.args[2], "lblLoanDoc"));
        flxLoanDocumentImage.add(lblLoanDoc);
        var flxApplicationSections = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxApplicationSections",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknf8f9faBorder100op",
            "top": "0dp",
            "width": "80%",
            "zIndex": 10
        }, controller.args[0], "flxApplicationSections"), extendConfig({}, controller.args[1], "flxApplicationSections"), extendConfig({}, controller.args[2], "flxApplicationSections"));
        flxApplicationSections.setDefaultUnit(kony.flex.DP);
        var flxLoanTypeDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLoanTypeDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLoanTypeDetails"), extendConfig({}, controller.args[1], "flxLoanTypeDetails"), extendConfig({}, controller.args[2], "flxLoanTypeDetails"));
        flxLoanTypeDetails.setDefaultUnit(kony.flex.DP);
        var lblLoanType = new kony.ui.Label(extendConfig({
            "bottom": "10px",
            "id": "lblLoanType",
            "isVisible": true,
            "skin": "sknlblLatoReg485c7512px",
            "text": "PERSONAL LOAN",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanType"), extendConfig({}, controller.args[2], "lblLoanType"));
        var lblLoanStatus = new kony.ui.Label(extendConfig({
            "bottom": "10px",
            "id": "lblLoanStatus",
            "isVisible": true,
            "skin": "sknlLatoRegularbl6B6B6B12",
            "text": "Loan Status",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanStatus"), extendConfig({}, controller.args[2], "lblLoanStatus"));
        var flxLoanStatusSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxLoanStatusSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknflxe1e5edop100",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxLoanStatusSeperator"), extendConfig({}, controller.args[1], "flxLoanStatusSeperator"), extendConfig({}, controller.args[2], "flxLoanStatusSeperator"));
        flxLoanStatusSeperator.setDefaultUnit(kony.flex.DP);
        flxLoanStatusSeperator.add();
        flxLoanTypeDetails.add(lblLoanType, lblLoanStatus, flxLoanStatusSeperator);
        var flxAppplicantHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxAppplicantHeading",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": 0,
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAppplicantHeading"), extendConfig({}, controller.args[1], "flxAppplicantHeading"), extendConfig({}, controller.args[2], "flxAppplicantHeading"));
        flxAppplicantHeading.setDefaultUnit(kony.flex.DP);
        var lblApplicationDetails = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblApplicationDetails",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbl485c7511px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Applicant\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblApplicationDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApplicationDetails"), extendConfig({}, controller.args[2], "lblApplicationDetails"));
        var flxApplicationDetailsSeparator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxApplicationDetailsSeparator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknflxe1e5edop100",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxApplicationDetailsSeparator"), extendConfig({}, controller.args[1], "flxApplicationDetailsSeparator"), extendConfig({}, controller.args[2], "flxApplicationDetailsSeparator"));
        flxApplicationDetailsSeparator.setDefaultUnit(kony.flex.DP);
        flxApplicationDetailsSeparator.add();
        var lblApplicantArrow = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblApplicantArrow",
            "isVisible": false,
            "right": 30,
            "skin": "sknLblIIcoMoon485c7514px",
            "text": "",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "lblApplicantArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApplicantArrow"), extendConfig({}, controller.args[2], "lblApplicantArrow"));
        flxAppplicantHeading.add(lblApplicationDetails, flxApplicationDetailsSeparator, lblApplicantArrow);
        var flxCoApplicantHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxCoApplicantHeading",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": 0,
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCoApplicantHeading"), extendConfig({}, controller.args[1], "flxCoApplicantHeading"), extendConfig({}, controller.args[2], "flxCoApplicantHeading"));
        flxCoApplicantHeading.setDefaultUnit(kony.flex.DP);
        var lblCoApplicantHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCoApplicantHeader",
            "isVisible": true,
            "left": 0,
            "skin": "sknlbl485c7511px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CoApplicant\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCoApplicantHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCoApplicantHeader"), extendConfig({}, controller.args[2], "lblCoApplicantHeader"));
        var lblCoApplicantHeadingSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "lblCoApplicantHeadingSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknflxe1e5edop100",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCoApplicantHeadingSeperator"), extendConfig({}, controller.args[1], "lblCoApplicantHeadingSeperator"), extendConfig({}, controller.args[2], "lblCoApplicantHeadingSeperator"));
        lblCoApplicantHeadingSeperator.setDefaultUnit(kony.flex.DP);
        lblCoApplicantHeadingSeperator.add();
        var lblCoApplicantArrow = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCoApplicantArrow",
            "isVisible": false,
            "right": 30,
            "skin": "sknLblIIcoMoon485c7514px",
            "text": "",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "lblCoApplicantArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCoApplicantArrow"), extendConfig({}, controller.args[2], "lblCoApplicantArrow"));
        flxCoApplicantHeading.add(lblCoApplicantHeader, lblCoApplicantHeadingSeperator, lblCoApplicantArrow);
        flxApplicationSections.add(flxLoanTypeDetails, flxAppplicantHeading, flxCoApplicantHeading);
        leftMenu.add(flxLoanDocumentImage, flxApplicationSections);
        return leftMenu;
    }
})