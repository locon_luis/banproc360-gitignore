define("com/adminConsole/loans/applications/viewConsent/userviewConsentController", function() {
    return {};
});
define("com/adminConsole/loans/applications/viewConsent/viewConsentControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnEdit **/
    AS_Button_b4630188e8cb4e1788612fb028603a0f: function AS_Button_b4630188e8cb4e1788612fb028603a0f(eventobject) {
        var self = this;
        var self = this;
        var parentId = eventobject.parent.parent.parent.id;
    }
});
define("com/adminConsole/loans/applications/viewConsent/viewConsentController", ["com/adminConsole/loans/applications/viewConsent/userviewConsentController", "com/adminConsole/loans/applications/viewConsent/viewConsentControllerActions"], function() {
    var controller = require("com/adminConsole/loans/applications/viewConsent/userviewConsentController");
    var actions = require("com/adminConsole/loans/applications/viewConsent/viewConsentControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
