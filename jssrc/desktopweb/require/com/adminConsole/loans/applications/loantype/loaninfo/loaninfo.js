define(function() {
    return function(controller) {
        var loaninfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "centerX": "50%",
            "clipBounds": true,
            "isMaster": true,
            "height": "93dp",
            "id": "loaninfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknFlxBordere1e5eedCustomRadius5px",
            "top": "10dp",
            "width": "286dp"
        }, controller.args[0], "loaninfo"), extendConfig({}, controller.args[1], "loaninfo"), extendConfig({
            "onHover": controller.AS_FlexContainer_f060a06b2a454e61a0577b070e30e355
        }, controller.args[2], "loaninfo"));
        loaninfo.setDefaultUnit(kony.flex.DP);
        var flxLoanType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLoanType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxLoanType"), extendConfig({}, controller.args[1], "flxLoanType"), extendConfig({}, controller.args[2], "flxLoanType"));
        flxLoanType.setDefaultUnit(kony.flex.DP);
        var lblNewLoanType = new kony.ui.Label(extendConfig({
            "id": "lblNewLoanType",
            "isVisible": true,
            "left": "27%",
            "skin": "sknLblLatoBold16px485c75KA",
            "text": "Personal Loan",
            "top": "30%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNewLoanType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNewLoanType"), extendConfig({}, controller.args[2], "lblNewLoanType"));
        var flxLoanTypeIcon = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60%",
            "id": "flxLoanTypeIcon",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "6%",
            "isModalContainer": false,
            "skin": "sknflxbackgroundBlue",
            "width": "18%",
            "zIndex": 100
        }, controller.args[0], "flxLoanTypeIcon"), extendConfig({}, controller.args[1], "flxLoanTypeIcon"), extendConfig({}, controller.args[2], "flxLoanTypeIcon"));
        flxLoanTypeIcon.setDefaultUnit(kony.flex.DP);
        var lblLoanTypeIcon = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblLoanTypeIcon",
            "isVisible": true,
            "skin": "sknIcon32pxWhiteNormal",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanTypeIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanTypeIcon"), extendConfig({}, controller.args[2], "lblLoanTypeIcon"));
        flxLoanTypeIcon.add(lblLoanTypeIcon);
        var flxLoanAprDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "42%",
            "id": "flxLoanAprDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "3%",
            "isModalContainer": false,
            "skin": "sknFlxFull0a9163354047140",
            "top": "60%",
            "width": "98%",
            "zIndex": 50
        }, controller.args[0], "flxLoanAprDetails"), extendConfig({}, controller.args[1], "flxLoanAprDetails"), extendConfig({}, controller.args[2], "flxLoanAprDetails"));
        flxLoanAprDetails.setDefaultUnit(kony.flex.DP);
        var lblLoanApr = new kony.ui.Label(extendConfig({
            "id": "lblLoanApr",
            "isVisible": true,
            "left": "24.50%",
            "skin": "sknLbl485C75Font12pxKA",
            "text": "APR as low as 9.49%",
            "top": "8%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanApr"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanApr"), extendConfig({}, controller.args[2], "lblLoanApr"));
        flxLoanAprDetails.add(lblLoanApr);
        var flxApplyLoan = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "42%",
            "id": "flxApplyLoan",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "3%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60%",
            "width": "97%",
            "zIndex": 1
        }, controller.args[0], "flxApplyLoan"), extendConfig({}, controller.args[1], "flxApplyLoan"), extendConfig({}, controller.args[2], "flxApplyLoan"));
        flxApplyLoan.setDefaultUnit(kony.flex.DP);
        var flxLearnMore = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40%",
            "id": "flxLearnMore",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "24.50%",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "8%",
            "width": "70dp"
        }, controller.args[0], "flxLearnMore"), extendConfig({}, controller.args[1], "flxLearnMore"), extendConfig({}, controller.args[2], "flxLearnMore"));
        flxLearnMore.setDefaultUnit(kony.flex.DP);
        var lblLearnMore = new kony.ui.Label(extendConfig({
            "id": "lblLearnMore",
            "isVisible": true,
            "skin": "sknAlertBlue",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.LearnMore\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblLearnMore"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLearnMore"), extendConfig({}, controller.args[2], "lblLearnMore"));
        flxLearnMore.add(lblLearnMore);
        var btnApplyLoan = new kony.ui.Button(extendConfig({
            "id": "btnApplyLoan",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Apply\")",
            "top": "8%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnApplyLoan"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [5, 1, 5, 1],
            "paddingInPixel": false
        }, controller.args[1], "btnApplyLoan"), extendConfig({}, controller.args[2], "btnApplyLoan"));
        var lblNewLoanTypeId = new kony.ui.Label(extendConfig({
            "id": "lblNewLoanTypeId",
            "isVisible": false,
            "left": "27%",
            "skin": "sknLblLatoBold16px485c75KA",
            "text": "Personal Loan",
            "top": "30%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNewLoanTypeId"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNewLoanTypeId"), extendConfig({}, controller.args[2], "lblNewLoanTypeId"));
        flxApplyLoan.add(flxLearnMore, btnApplyLoan, lblNewLoanTypeId);
        flxLoanType.add(lblNewLoanType, flxLoanTypeIcon, flxLoanAprDetails, flxApplyLoan);
        loaninfo.add(flxLoanType);
        return loaninfo;
    }
})