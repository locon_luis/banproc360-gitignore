define("com/adminConsole/loans/applications/editApplicantDetails/usereditApplicantDetailsController", function() {
    return {};
});
define("com/adminConsole/loans/applications/editApplicantDetails/editApplicantDetailsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onKeyUp defined for txtDescription **/
    AS_TextArea_d1068c1c380a4eb5a5faa76f3caea26c: function AS_TextArea_d1068c1c380a4eb5a5faa76f3caea26c(eventobject) {
        var self = this;
        return self.onDescriptionKeyUp.call(this, null);
    }
});
define("com/adminConsole/loans/applications/editApplicantDetails/editApplicantDetailsController", ["com/adminConsole/loans/applications/editApplicantDetails/usereditApplicantDetailsController", "com/adminConsole/loans/applications/editApplicantDetails/editApplicantDetailsControllerActions"], function() {
    var controller = require("com/adminConsole/loans/applications/editApplicantDetails/usereditApplicantDetailsController");
    var actions = require("com/adminConsole/loans/applications/editApplicantDetails/editApplicantDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
