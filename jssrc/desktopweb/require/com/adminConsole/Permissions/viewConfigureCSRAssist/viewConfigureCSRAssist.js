define(function() {
    return function(controller) {
        var viewConfigureCSRAssist = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewConfigureCSRAssist",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_fd4b6994bf604ee78bff55ba90fcb858(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "viewConfigureCSRAssist"), extendConfig({}, controller.args[1], "viewConfigureCSRAssist"), extendConfig({}, controller.args[2], "viewConfigureCSRAssist"));
        viewConfigureCSRAssist.setDefaultUnit(kony.flex.DP);
        var flxBackToPermmissionBtn = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxBackToPermmissionBtn",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBackToPermmissionBtn"), extendConfig({}, controller.args[1], "flxBackToPermmissionBtn"), extendConfig({}, controller.args[2], "flxBackToPermmissionBtn"));
        flxBackToPermmissionBtn.setDefaultUnit(kony.flex.DP);
        var backToPageHeader = new com.adminConsole.customerMang.backToPageHeader(extendConfig({
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "backToPageHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "backToPageHeader": {
                    "centerY": "50%"
                },
                "btnBack": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.ViewConfigureCSR.Back_To_Permissions\")"
                }
            }
        }, controller.args[0], "backToPageHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "backToPageHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "backToPageHeader"));
        flxBackToPermmissionBtn.add(backToPageHeader);
        var flxHeadingSegment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeadingSegment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "40dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeadingSegment"), extendConfig({}, controller.args[1], "flxHeadingSegment"), extendConfig({}, controller.args[2], "flxHeadingSegment"));
        flxHeadingSegment.setDefaultUnit(kony.flex.DP);
        var flxHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxHeading",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeading"), extendConfig({}, controller.args[1], "flxHeading"), extendConfig({}, controller.args[2], "flxHeading"));
        flxHeading.setDefaultUnit(kony.flex.DP);
        var lblHeading = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeading",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknlblLatoreg485c7515px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.ViewConfigureCSR.Configure_CSR_Assist\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        flxHeading.add(lblHeading);
        var flxCSRSegContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCSRSegContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "55dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCSRSegContainer"), extendConfig({}, controller.args[1], "flxCSRSegContainer"), extendConfig({}, controller.args[2], "flxCSRSegContainer"));
        flxCSRSegContainer.setDefaultUnit(kony.flex.DP);
        var lblTopLine = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblTopLine",
            "isVisible": false,
            "left": "35dp",
            "right": "35dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "zIndex": 3
        }, controller.args[0], "lblTopLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTopLine"), extendConfig({}, controller.args[2], "lblTopLine"));
        var segViewConfigureCSR = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "data": [{
                "lblEnable": "Enable",
                "lblEnabled": "Enabled",
                "lblHeadingDesc": "DESCRIPTION",
                "lblIconArrow": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fontIconBreadcrumbsRight"),
                "lblIconGreenTick": "",
                "lblLine": "Label",
                "lblLine2": "Label",
                "lblName": "Account Transfers",
                "rtxDescription": "RichText",
                "switchToggle": {
                    "selectedIndex": 1
                }
            }, {
                "lblEnable": "Enable",
                "lblEnabled": "Enabled",
                "lblHeadingDesc": "DESCRIPTION",
                "lblIconArrow": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fontIconBreadcrumbsRight"),
                "lblIconGreenTick": "",
                "lblLine": "Label",
                "lblLine2": "Label",
                "lblName": "Account Transfers",
                "rtxDescription": "RichText",
                "switchToggle": {
                    "selectedIndex": 1
                }
            }, {
                "lblEnable": "Enable",
                "lblEnabled": "Enabled",
                "lblHeadingDesc": "DESCRIPTION",
                "lblIconArrow": kony.i18n.getLocalizedString("i18n.userwidgetmodel.fontIconBreadcrumbsRight"),
                "lblIconGreenTick": "",
                "lblLine": "Label",
                "lblLine2": "Label",
                "lblName": "Account Transfers",
                "rtxDescription": "RichText",
                "switchToggle": {
                    "selectedIndex": 1
                }
            }],
            "groupCells": false,
            "id": "segViewConfigureCSR",
            "isVisible": true,
            "left": "35dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "35dp",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxViewConfigureCSR",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "f0eff400",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxEnableTick": "flxEnableTick",
                "flxEnableToggle": "flxEnableToggle",
                "flxExpandArrow": "flxExpandArrow",
                "flxToggleSwitch": "flxToggleSwitch",
                "flxViewConfigureCSR": "flxViewConfigureCSR",
                "flxViewConfigureDesc": "flxViewConfigureDesc",
                "flxViewConfigureRowCont": "flxViewConfigureRowCont",
                "lblEnable": "lblEnable",
                "lblEnabled": "lblEnabled",
                "lblHeadingDesc": "lblHeadingDesc",
                "lblIconArrow": "lblIconArrow",
                "lblIconGreenTick": "lblIconGreenTick",
                "lblLine": "lblLine",
                "lblLine2": "lblLine2",
                "lblName": "lblName",
                "rtxDescription": "rtxDescription",
                "switchToggle": "switchToggle"
            },
            "zIndex": 1
        }, controller.args[0], "segViewConfigureCSR"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segViewConfigureCSR"), extendConfig({}, controller.args[2], "segViewConfigureCSR"));
        flxCSRSegContainer.add(lblTopLine, segViewConfigureCSR);
        flxHeadingSegment.add(flxHeading, flxCSRSegContainer);
        viewConfigureCSRAssist.add(flxBackToPermmissionBtn, flxHeadingSegment);
        return viewConfigureCSRAssist;
    }
})