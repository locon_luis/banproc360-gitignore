define(function() {
    return function(controller) {
        var searchCustomer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "searchCustomer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0hfd18814fd664dCM",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "searchCustomer"), extendConfig({}, controller.args[1], "searchCustomer"), extendConfig({}, controller.args[2], "searchCustomer"));
        searchCustomer.setDefaultUnit(kony.flex.DP);
        var lblDefaultSearchHeader = new kony.ui.Label(extendConfig({
            "id": "lblDefaultSearchHeader",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDefaultSearchHeader\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDefaultSearchHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDefaultSearchHeader"), extendConfig({}, controller.args[2], "lblDefaultSearchHeader"));
        var lblSearchError = new kony.ui.Label(extendConfig({
            "id": "lblSearchError",
            "isVisible": false,
            "left": "200px",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchError\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchError"), extendConfig({}, controller.args[2], "lblSearchError"));
        var imgSearchError = new kony.ui.Image2(extendConfig({
            "height": "12dp",
            "id": "imgSearchError",
            "isVisible": false,
            "left": "184dp",
            "skin": "slImage",
            "src": "error_2x.png",
            "top": "2dp",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "imgSearchError"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSearchError"), extendConfig({}, controller.args[2], "imgSearchError"));
        var flxDefaultSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDefaultSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "16px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDefaultSearch"), extendConfig({}, controller.args[1], "flxDefaultSearch"), extendConfig({}, controller.args[2], "flxDefaultSearch"));
        flxDefaultSearch.setDefaultUnit(kony.flex.DP);
        var flxFirstRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "68dp",
            "id": "flxFirstRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "20dp",
            "zIndex": 1
        }, controller.args[0], "flxFirstRow"), extendConfig({}, controller.args[1], "flxFirstRow"), extendConfig({}, controller.args[2], "flxFirstRow"));
        flxFirstRow.setDefaultUnit(kony.flex.DP);
        var flxSearchCriteria1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "68dp",
            "id": "flxSearchCriteria1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxSearchCriteria1"), extendConfig({}, controller.args[1], "flxSearchCriteria1"), extendConfig({}, controller.args[2], "flxSearchCriteria1"));
        flxSearchCriteria1.setDefaultUnit(kony.flex.DP);
        var lblSearchParam1 = new kony.ui.Label(extendConfig({
            "id": "lblSearchParam1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertMainName\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchParam1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchParam1"), extendConfig({}, controller.args[2], "lblSearchParam1"));
        var txtSearchParam1 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtSearchParam1",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "placeholder": "Customer Name",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25px",
            "zIndex": 1
        }, controller.args[0], "txtSearchParam1"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearchParam1"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSearchParam1"));
        flxSearchCriteria1.add(lblSearchParam1, txtSearchParam1);
        var flxSearchCriteria2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "68dp",
            "id": "flxSearchCriteria2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxSearchCriteria2"), extendConfig({}, controller.args[1], "flxSearchCriteria2"), extendConfig({}, controller.args[2], "flxSearchCriteria2"));
        flxSearchCriteria2.setDefaultUnit(kony.flex.DP);
        var lblSearchParam2 = new kony.ui.Label(extendConfig({
            "id": "lblSearchParam2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.User_ID\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchParam2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchParam2"), extendConfig({}, controller.args[2], "lblSearchParam2"));
        var txtSearchParam2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtSearchParam2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "placeholder": "User ID",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25px",
            "zIndex": 1
        }, controller.args[0], "txtSearchParam2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearchParam2"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSearchParam2"));
        flxSearchCriteria2.add(lblSearchParam2, txtSearchParam2);
        var flxSearchCriteria3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "68dp",
            "id": "flxSearchCriteria3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxSearchCriteria3"), extendConfig({}, controller.args[1], "flxSearchCriteria3"), extendConfig({}, controller.args[2], "flxSearchCriteria3"));
        flxSearchCriteria3.setDefaultUnit(kony.flex.DP);
        var lblSearchParam3 = new kony.ui.Label(extendConfig({
            "id": "lblSearchParam3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchParam3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchParam3"), extendConfig({}, controller.args[2], "lblSearchParam3"));
        var txtSearchParam3 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtSearchParam3",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "placeholder": "Username",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtSearchParam3"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearchParam3"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSearchParam3"));
        flxSearchCriteria3.add(lblSearchParam3, txtSearchParam3);
        flxFirstRow.add(flxSearchCriteria1, flxSearchCriteria2, flxSearchCriteria3);
        flxDefaultSearch.add(flxFirstRow);
        searchCustomer.add(lblDefaultSearchHeader, lblSearchError, imgSearchError, flxDefaultSearch);
        return searchCustomer;
    }
})