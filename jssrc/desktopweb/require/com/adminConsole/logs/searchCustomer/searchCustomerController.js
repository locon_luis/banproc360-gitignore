define("com/adminConsole/logs/searchCustomer/usersearchCustomerController", function() {
    return {};
});
define("com/adminConsole/logs/searchCustomer/searchCustomerControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/logs/searchCustomer/searchCustomerController", ["com/adminConsole/logs/searchCustomer/usersearchCustomerController", "com/adminConsole/logs/searchCustomer/searchCustomerControllerActions"], function() {
    var controller = require("com/adminConsole/logs/searchCustomer/usersearchCustomerController");
    var actions = require("com/adminConsole/logs/searchCustomer/searchCustomerControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
