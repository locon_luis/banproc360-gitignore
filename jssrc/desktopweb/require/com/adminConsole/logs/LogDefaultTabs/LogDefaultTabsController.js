define("com/adminConsole/logs/LogDefaultTabs/userLogDefaultTabsController", function() {
    return {};
});
define("com/adminConsole/logs/LogDefaultTabs/LogDefaultTabsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/logs/LogDefaultTabs/LogDefaultTabsController", ["com/adminConsole/logs/LogDefaultTabs/userLogDefaultTabsController", "com/adminConsole/logs/LogDefaultTabs/LogDefaultTabsControllerActions"], function() {
    var controller = require("com/adminConsole/logs/LogDefaultTabs/userLogDefaultTabsController");
    var actions = require("com/adminConsole/logs/LogDefaultTabs/LogDefaultTabsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
