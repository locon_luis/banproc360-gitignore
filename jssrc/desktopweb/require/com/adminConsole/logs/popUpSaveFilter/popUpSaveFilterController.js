define("com/adminConsole/logs/popUpSaveFilter/userpopUpSaveFilterController", function() {
    return {
        setPreShow: function() {
            var scopeObj = this;
            this.view.txtfldName.onKeyUp = function() {
                scopeObj.view.lblNoNameError.skin = "sknErrorTransparent";
                scopeObj.view.txtfldName.skin = "skntxtbxDetails0bbf1235271384a";
                if (scopeObj.view.txtfldName.text.trim().length === 0) {
                    scopeObj.view.lblNameSize.setVisibility(false);
                } else {
                    scopeObj.view.lblNameSize.setVisibility(true);
                    scopeObj.view.lblNameSize.text = scopeObj.view.txtfldName.text.trim().length + "/60";
                }
                scopeObj.view.forceLayout();
            };
            this.view.txtareaDescription.onKeyUp = function() {
                if (scopeObj.view.txtareaDescription.text.trim().length === 0) {
                    scopeObj.view.lbldescriptionSize.setVisibility(false);
                } else {
                    scopeObj.view.lbldescriptionSize.setVisibility(true);
                    scopeObj.view.lbldescriptionSize.text = scopeObj.view.txtareaDescription.text.trim().length + "/70";
                }
                scopeObj.view.forceLayout();
            };
        },
    };
});
define("com/adminConsole/logs/popUpSaveFilter/popUpSaveFilterControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for popUpSaveFilter **/
    AS_FlexContainer_f861d67e4d0e4f109d85004bd1deb82d: function AS_FlexContainer_f861d67e4d0e4f109d85004bd1deb82d(eventobject) {
        var self = this;
        this.setPreShow();
    }
});
define("com/adminConsole/logs/popUpSaveFilter/popUpSaveFilterController", ["com/adminConsole/logs/popUpSaveFilter/userpopUpSaveFilterController", "com/adminConsole/logs/popUpSaveFilter/popUpSaveFilterControllerActions"], function() {
    var controller = require("com/adminConsole/logs/popUpSaveFilter/userpopUpSaveFilterController");
    var actions = require("com/adminConsole/logs/popUpSaveFilter/popUpSaveFilterControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
