define("com/adminConsole/logs/selectAmount/userselectAmountController", function() {
    return {};
});
define("com/adminConsole/logs/selectAmount/selectAmountControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for selectAmount **/
    AS_FlexContainer_hddf6ece62b0438fa74c1cd81a02568f: function AS_FlexContainer_hddf6ece62b0438fa74c1cd81a02568f(eventobject) {
        var self = this;
        this.setPreShow();
    }
});
define("com/adminConsole/logs/selectAmount/selectAmountController", ["com/adminConsole/logs/selectAmount/userselectAmountController", "com/adminConsole/logs/selectAmount/selectAmountControllerActions"], function() {
    var controller = require("com/adminConsole/logs/selectAmount/userselectAmountController");
    var actions = require("com/adminConsole/logs/selectAmount/selectAmountControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
