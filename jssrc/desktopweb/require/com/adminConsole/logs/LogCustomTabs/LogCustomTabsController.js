define("com/adminConsole/logs/LogCustomTabs/userLogCustomTabsController", function() {
    return {};
});
define("com/adminConsole/logs/LogCustomTabs/LogCustomTabsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/logs/LogCustomTabs/LogCustomTabsController", ["com/adminConsole/logs/LogCustomTabs/userLogCustomTabsController", "com/adminConsole/logs/LogCustomTabs/LogCustomTabsControllerActions"], function() {
    var controller = require("com/adminConsole/logs/LogCustomTabs/userLogCustomTabsController");
    var actions = require("com/adminConsole/logs/LogCustomTabs/LogCustomTabsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
