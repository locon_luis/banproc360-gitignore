define("com/adminConsole/view/details/userdetailsController", function() {
    return {};
});
define("com/adminConsole/view/details/detailsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/view/details/detailsController", ["com/adminConsole/view/details/userdetailsController", "com/adminConsole/view/details/detailsControllerActions"], function() {
    var controller = require("com/adminConsole/view/details/userdetailsController");
    var actions = require("com/adminConsole/view/details/detailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
