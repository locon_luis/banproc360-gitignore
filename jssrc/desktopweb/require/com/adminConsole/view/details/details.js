define(function() {
    return function(controller) {
        var details = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "40px",
            "id": "details",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "details"), extendConfig({}, controller.args[1], "details"), extendConfig({}, controller.args[2], "details"));
        details.setDefaultUnit(kony.flex.DP);
        var flxColumn1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxColumn1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxColumn1"), extendConfig({}, controller.args[1], "flxColumn1"), extendConfig({}, controller.args[2], "flxColumn1"));
        flxColumn1.setDefaultUnit(kony.flex.DP);
        var lblHeading1 = new kony.ui.Label(extendConfig({
            "id": "lblHeading1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading1"), extendConfig({}, controller.args[2], "lblHeading1"));
        var flxDataAndImage1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataAndImage1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataAndImage1"), extendConfig({}, controller.args[1], "flxDataAndImage1"), extendConfig({}, controller.args[2], "flxDataAndImage1"));
        flxDataAndImage1.setDefaultUnit(kony.flex.DP);
        var imgData1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12px",
            "id": "imgData1",
            "isVisible": false,
            "left": "0dp",
            "right": "5px",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "imgData1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgData1"), extendConfig({}, controller.args[2], "imgData1"));
        var lblData1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData1"), extendConfig({}, controller.args[2], "lblData1"));
        var btnLink1 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnLink1",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknBtnLato11ABEB11Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Enroll\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnLink1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnLink1"), extendConfig({}, controller.args[2], "btnLink1"));
        flxDataAndImage1.add(imgData1, lblData1, btnLink1);
        flxColumn1.add(lblHeading1, flxDataAndImage1);
        var flxColumn2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxColumn2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxColumn2"), extendConfig({}, controller.args[1], "flxColumn2"), extendConfig({}, controller.args[2], "flxColumn2"));
        flxColumn2.setDefaultUnit(kony.flex.DP);
        var lblHeading2 = new kony.ui.Label(extendConfig({
            "id": "lblHeading2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading2"), extendConfig({}, controller.args[2], "lblHeading2"));
        var flxDataAndImage2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataAndImage2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataAndImage2"), extendConfig({}, controller.args[1], "flxDataAndImage2"), extendConfig({}, controller.args[2], "flxDataAndImage2"));
        flxDataAndImage2.setDefaultUnit(kony.flex.DP);
        var imgData2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12px",
            "id": "imgData2",
            "isVisible": false,
            "left": "0dp",
            "right": "5px",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "imgData2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgData2"), extendConfig({}, controller.args[2], "imgData2"));
        var lblSignData2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSignData2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSignData2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSignData2"), extendConfig({}, controller.args[2], "lblSignData2"));
        var lblIconData2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconData2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcomoonCurrencySymbol",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconData2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconData2"), extendConfig({}, controller.args[2], "lblIconData2"));
        var lblData2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData2"), extendConfig({}, controller.args[2], "lblData2"));
        var btnLink2 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnLink2",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknBtnLato11ABEB11Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Enroll\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnLink2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnLink2"), extendConfig({}, controller.args[2], "btnLink2"));
        flxDataAndImage2.add(imgData2, lblSignData2, lblIconData2, lblData2, btnLink2);
        flxColumn2.add(lblHeading2, flxDataAndImage2);
        var flxColumn3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxColumn3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "72%",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxColumn3"), extendConfig({}, controller.args[1], "flxColumn3"), extendConfig({}, controller.args[2], "flxColumn3"));
        flxColumn3.setDefaultUnit(kony.flex.DP);
        var lblHeading3 = new kony.ui.Label(extendConfig({
            "id": "lblHeading3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading3"), extendConfig({}, controller.args[2], "lblHeading3"));
        var flxDataAndImage3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataAndImage3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataAndImage3"), extendConfig({}, controller.args[1], "flxDataAndImage3"), extendConfig({}, controller.args[2], "flxDataAndImage3"));
        flxDataAndImage3.setDefaultUnit(kony.flex.DP);
        var imgData3 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12px",
            "id": "imgData3",
            "isVisible": false,
            "left": "0dp",
            "right": "5px",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "imgData3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgData3"), extendConfig({}, controller.args[2], "imgData3"));
        var lblData3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData3",
            "isVisible": true,
            "left": "0px",
            "right": "7px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData3"), extendConfig({}, controller.args[2], "lblData3"));
        var btnLink3 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnLink3",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknBtnLato11ABEB11Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Enroll\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnLink3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnLink3"), extendConfig({}, controller.args[2], "btnLink3"));
        flxDataAndImage3.add(imgData3, lblData3, btnLink3);
        flxColumn3.add(lblHeading3, flxDataAndImage3);
        details.add(flxColumn1, flxColumn2, flxColumn3);
        return details;
    }
})