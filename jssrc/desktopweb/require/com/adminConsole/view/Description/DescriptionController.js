define("com/adminConsole/view/Description/userDescriptionController", function() {
    return {
        setFlowActions: function() {
            var scopeObj = this;
            this.view.flxToggleDescription.onClick = function() {
                scopeObj.toggleDescription();
            };
        },
        toggleDescription: function() {
            if (this.view.rtxDescription.isVisible) {
                this.view.imgToggleDescription.src = "img_desc_arrow.png";
                this.view.rtxDescription.setVisibility(false);
            } else {
                this.view.imgToggleDescription.src = "img_down_arrow.png";
                this.view.rtxDescription.setVisibility(true);
            }
        }
    };
});
define("com/adminConsole/view/Description/DescriptionControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for Description **/
    AS_FlexContainer_d679d108c5a349f8b1a851b0cd6a09ef: function AS_FlexContainer_d679d108c5a349f8b1a851b0cd6a09ef(eventobject) {
        var self = this;
        this.setFlowActions();
    }
});
define("com/adminConsole/view/Description/DescriptionController", ["com/adminConsole/view/Description/userDescriptionController", "com/adminConsole/view/Description/DescriptionControllerActions"], function() {
    var controller = require("com/adminConsole/view/Description/userDescriptionController");
    var actions = require("com/adminConsole/view/Description/DescriptionControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
