define("com/adminConsole/view/Description1/userDescription1Controller", function() {
    return {
        setFlowActions: function() {
            var scopeObj = this;
            this.view.flxToggleDescription.onClick = function() {
                scopeObj.toggleDescription();
            };
            this.view.lblDescription.onClick = function() {
                scopeObj.toggleDescription();
            };
        },
        toggleDescription: function() {
            if (this.view.rtxDescription.isVisible) {
                this.view.lblToggleDescription.text = "\ue922";
                this.view.rtxDescription.setVisibility(false);
            } else {
                this.view.lblToggleDescription.text = "\ue915";
                this.view.rtxDescription.setVisibility(true);
            }
        }
    };
});
define("com/adminConsole/view/Description1/Description1ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for Description1 **/
    AS_FlexContainer_c6b61fe1c4c9441d8694eec976dedf80: function AS_FlexContainer_c6b61fe1c4c9441d8694eec976dedf80(eventobject) {
        var self = this;
        this.setFlowActions();
    }
});
define("com/adminConsole/view/Description1/Description1Controller", ["com/adminConsole/view/Description1/userDescription1Controller", "com/adminConsole/view/Description1/Description1ControllerActions"], function() {
    var controller = require("com/adminConsole/view/Description1/userDescription1Controller");
    var actions = require("com/adminConsole/view/Description1/Description1ControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
