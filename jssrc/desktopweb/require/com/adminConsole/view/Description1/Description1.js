define(function() {
    return function(controller) {
        var Description1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "Description1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_c6b61fe1c4c9441d8694eec976dedf80(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "Description1"), extendConfig({}, controller.args[1], "Description1"), extendConfig({}, controller.args[2], "Description1"));
        Description1.setDefaultUnit(kony.flex.DP);
        var flxViewDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxViewDescription",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxViewDescription"), extendConfig({}, controller.args[1], "flxViewDescription"), extendConfig({}, controller.args[2], "flxViewDescription"));
        flxViewDescription.setDefaultUnit(kony.flex.DP);
        var lblDescription = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDescription",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSubHeader0b73b6af9628b4e",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({
            "hoverSkin": "sknlbl485c7513pxHoverCursor"
        }, controller.args[2], "lblDescription"));
        var flxToggleDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxToggleDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "5px",
            "skin": "slFbox",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxToggleDescription"), extendConfig({}, controller.args[1], "flxToggleDescription"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxToggleDescription"));
        flxToggleDescription.setDefaultUnit(kony.flex.DP);
        var imgToggleDescription = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "10dp",
            "id": "imgToggleDescription",
            "isVisible": false,
            "skin": "slImage",
            "src": "img_down_arrow.png",
            "width": "10dp",
            "zIndex": 1
        }, controller.args[0], "imgToggleDescription"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgToggleDescription"), extendConfig({}, controller.args[2], "imgToggleDescription"));
        var lblToggleDescription = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "lblToggleDescription",
            "isVisible": true,
            "skin": "sknIcon13pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "lblToggleDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblToggleDescription"), extendConfig({}, controller.args[2], "lblToggleDescription"));
        flxToggleDescription.add(imgToggleDescription, lblToggleDescription);
        flxViewDescription.add(lblDescription, flxToggleDescription);
        var rtxDescription = new kony.ui.RichText(extendConfig({
            "id": "rtxDescription",
            "isVisible": true,
            "left": "35px",
            "right": "35px",
            "skin": "sknrtxLato485c7514px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxDescription\")",
            "top": "40px",
            "zIndex": 1
        }, controller.args[0], "rtxDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxDescription"), extendConfig({}, controller.args[2], "rtxDescription"));
        Description1.add(flxViewDescription, rtxDescription);
        return Description1;
    }
})