define("com/adminConsole/view/detailsWithLink/userdetailsWithLinkController", function() {
    return {};
});
define("com/adminConsole/view/detailsWithLink/detailsWithLinkControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/view/detailsWithLink/detailsWithLinkController", ["com/adminConsole/view/detailsWithLink/userdetailsWithLinkController", "com/adminConsole/view/detailsWithLink/detailsWithLinkControllerActions"], function() {
    var controller = require("com/adminConsole/view/detailsWithLink/userdetailsWithLinkController");
    var actions = require("com/adminConsole/view/detailsWithLink/detailsWithLinkControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
