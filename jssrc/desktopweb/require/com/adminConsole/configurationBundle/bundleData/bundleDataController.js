define("com/adminConsole/configurationBundle/bundleData/userbundleDataController", function() {
    return {};
});
define("com/adminConsole/configurationBundle/bundleData/bundleDataControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/configurationBundle/bundleData/bundleDataController", ["com/adminConsole/configurationBundle/bundleData/userbundleDataController", "com/adminConsole/configurationBundle/bundleData/bundleDataControllerActions"], function() {
    var controller = require("com/adminConsole/configurationBundle/bundleData/userbundleDataController");
    var actions = require("com/adminConsole/configurationBundle/bundleData/bundleDataControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
