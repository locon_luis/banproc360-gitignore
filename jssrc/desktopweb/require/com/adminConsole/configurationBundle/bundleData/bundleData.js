define(function() {
    return function(controller) {
        var bundleData = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "80px",
            "id": "bundleData",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxConfigurationBundlesWithBorder",
            "top": "0px",
            "width": "48%"
        }, controller.args[0], "bundleData"), extendConfig({}, controller.args[1], "bundleData"), extendConfig({
            "hoverSkin": "tabsHoverSkin"
        }, controller.args[2], "bundleData"));
        bundleData.setDefaultUnit(kony.flex.DP);
        var flxBundleLogo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxBundleLogo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "80px",
            "zIndex": 1
        }, controller.args[0], "flxBundleLogo"), extendConfig({}, controller.args[1], "flxBundleLogo"), extendConfig({}, controller.args[2], "flxBundleLogo"));
        flxBundleLogo.setDefaultUnit(kony.flex.DP);
        var flxBundleLogoInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "50px",
            "id": "flxBundleLogoInner",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknLblBundleLogo",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "flxBundleLogoInner"), extendConfig({}, controller.args[1], "flxBundleLogoInner"), extendConfig({}, controller.args[2], "flxBundleLogoInner"));
        flxBundleLogoInner.setDefaultUnit(kony.flex.DP);
        var lblBundleLogoApp = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblBundleLogoApp",
            "isVisible": true,
            "skin": "sknlblConfigurationBundlesLogo",
            "text": "APP",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBundleLogoApp"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBundleLogoApp"), extendConfig({}, controller.args[2], "lblBundleLogoApp"));
        flxBundleLogoInner.add(lblBundleLogoApp);
        var lblBundleLogo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "80px",
            "id": "lblBundleLogo",
            "isVisible": true,
            "left": "6px",
            "skin": "sknlblIcomoonApp",
            "text": "",
            "width": "80px",
            "zIndex": 1
        }, controller.args[0], "lblBundleLogo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBundleLogo"), extendConfig({}, controller.args[2], "lblBundleLogo"));
        flxBundleLogo.add(flxBundleLogoInner, lblBundleLogo);
        var flxBundleDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxBundleDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "80px",
            "isModalContainer": false,
            "right": "80px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxBundleDetails"), extendConfig({}, controller.args[1], "flxBundleDetails"), extendConfig({}, controller.args[2], "flxBundleDetails"));
        flxBundleDetails.setDefaultUnit(kony.flex.DP);
        var flxBundleName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxBundleName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "55%",
            "zIndex": 1
        }, controller.args[0], "flxBundleName"), extendConfig({}, controller.args[1], "flxBundleName"), extendConfig({}, controller.args[2], "flxBundleName"));
        flxBundleName.setDefaultUnit(kony.flex.DP);
        var lblBundleNameHeader = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblBundleNameHeader",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblConfigBundleHeader",
            "text": "NAME",
            "top": "19px",
            "width": "93%",
            "zIndex": 1
        }, controller.args[0], "lblBundleNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBundleNameHeader"), extendConfig({}, controller.args[2], "lblBundleNameHeader"));
        var lblBundleName = new kony.ui.Label(extendConfig({
            "bottom": "19px",
            "height": "15px",
            "id": "lblBundleName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblConfigBundleName",
            "text": "Retail Banking",
            "width": "93%",
            "zIndex": 1
        }, controller.args[0], "lblBundleName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBundleName"), extendConfig({}, controller.args[2], "lblBundleName"));
        flxBundleName.add(lblBundleNameHeader, lblBundleName);
        var flxAppId = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxAppId",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "width": "45%",
            "zIndex": 1
        }, controller.args[0], "flxAppId"), extendConfig({}, controller.args[1], "flxAppId"), extendConfig({}, controller.args[2], "flxAppId"));
        flxAppId.setDefaultUnit(kony.flex.DP);
        var lblAppIdHeader = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblAppIdHeader",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblConfigBundleHeader",
            "text": "APP ID",
            "top": "19px",
            "width": "93%",
            "zIndex": 1
        }, controller.args[0], "lblAppIdHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAppIdHeader"), extendConfig({}, controller.args[2], "lblAppIdHeader"));
        var lblAppId = new kony.ui.Label(extendConfig({
            "bottom": "19px",
            "height": "15px",
            "id": "lblAppId",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblConfigBundleName",
            "text": "Kony DBP",
            "width": "93%",
            "zIndex": 1
        }, controller.args[0], "lblAppId"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAppId"), extendConfig({}, controller.args[2], "lblAppId"));
        flxAppId.add(lblAppIdHeader, lblAppId);
        flxBundleDetails.add(flxBundleName, flxAppId);
        var flxBundleButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxBundleButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "width": "80px",
            "zIndex": 1
        }, controller.args[0], "flxBundleButtons"), extendConfig({}, controller.args[1], "flxBundleButtons"), extendConfig({}, controller.args[2], "flxBundleButtons"));
        flxBundleButtons.setDefaultUnit(kony.flex.DP);
        var flxBundleEdit = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxBundleEdit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "50px",
            "skin": "CopyslFbox0efc73ba91cad4b",
            "top": "15px",
            "width": "20px"
        }, controller.args[0], "flxBundleEdit"), extendConfig({}, controller.args[1], "flxBundleEdit"), extendConfig({
            "hoverSkin": "tabsHoverSkin"
        }, controller.args[2], "flxBundleEdit"));
        flxBundleEdit.setDefaultUnit(kony.flex.DP);
        var lblBundleEdit = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20px",
            "id": "lblBundleEdit",
            "isVisible": true,
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblBundleEdit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBundleEdit"), extendConfig({}, controller.args[2], "lblBundleEdit"));
        flxBundleEdit.add(lblBundleEdit);
        var flxBundleDelete = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxBundleDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "CopyslFbox0efc73ba91cad4b",
            "top": "15px",
            "width": "20px"
        }, controller.args[0], "flxBundleDelete"), extendConfig({}, controller.args[1], "flxBundleDelete"), extendConfig({
            "hoverSkin": "tabsHoverSkin"
        }, controller.args[2], "flxBundleDelete"));
        flxBundleDelete.setDefaultUnit(kony.flex.DP);
        var lblBundleDelete = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20px",
            "id": "lblBundleDelete",
            "isVisible": true,
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblBundleDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBundleDelete"), extendConfig({}, controller.args[2], "lblBundleDelete"));
        flxBundleDelete.add(lblBundleDelete);
        flxBundleButtons.add(flxBundleEdit, flxBundleDelete);
        bundleData.add(flxBundleLogo, flxBundleDetails, flxBundleButtons);
        return bundleData;
    }
})