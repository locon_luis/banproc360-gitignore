define("com/adminConsole/configurationBundle/configurationData/userconfigurationDataController", function() {
    return {};
});
define("com/adminConsole/configurationBundle/configurationData/configurationDataControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/configurationBundle/configurationData/configurationDataController", ["com/adminConsole/configurationBundle/configurationData/userconfigurationDataController", "com/adminConsole/configurationBundle/configurationData/configurationDataControllerActions"], function() {
    var controller = require("com/adminConsole/configurationBundle/configurationData/userconfigurationDataController");
    var actions = require("com/adminConsole/configurationBundle/configurationData/configurationDataControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
