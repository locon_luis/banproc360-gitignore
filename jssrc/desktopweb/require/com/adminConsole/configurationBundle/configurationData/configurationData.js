define(function() {
    return function(controller) {
        var configurationData = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "300px",
            "id": "configurationData",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "configurationData"), extendConfig({}, controller.args[1], "configurationData"), extendConfig({}, controller.args[2], "configurationData"));
        configurationData.setDefaultUnit(kony.flex.DP);
        var flxHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeaders"), extendConfig({}, controller.args[1], "flxHeaders"), extendConfig({}, controller.args[2], "flxHeaders"));
        flxHeaders.setDefaultUnit(kony.flex.DP);
        var flxConfigurationArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxConfigurationArrow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "5%",
            "zIndex": 1
        }, controller.args[0], "flxConfigurationArrow"), extendConfig({}, controller.args[1], "flxConfigurationArrow"), extendConfig({}, controller.args[2], "flxConfigurationArrow"));
        flxConfigurationArrow.setDefaultUnit(kony.flex.DP);
        flxConfigurationArrow.add();
        var flxConfigurationKey = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxConfigurationKey",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "18%",
            "zIndex": 1
        }, controller.args[0], "flxConfigurationKey"), extendConfig({}, controller.args[1], "flxConfigurationKey"), extendConfig({}, controller.args[2], "flxConfigurationKey"));
        flxConfigurationKey.setDefaultUnit(kony.flex.DP);
        var lblConfigurationKey = new kony.ui.Label(extendConfig({
            "id": "lblConfigurationKey",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.keyInCaps\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblConfigurationKey"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConfigurationKey"), extendConfig({}, controller.args[2], "lblConfigurationKey"));
        var flxConfigurationKeySort = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxConfigurationKeySort",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0px",
            "width": "20px"
        }, controller.args[0], "flxConfigurationKeySort"), extendConfig({}, controller.args[1], "flxConfigurationKeySort"), extendConfig({}, controller.args[2], "flxConfigurationKeySort"));
        flxConfigurationKeySort.setDefaultUnit(kony.flex.DP);
        var fonticonConfigurationKeySort = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "fonticonConfigurationKeySort",
            "isVisible": true,
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonConfigurationKeySort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonConfigurationKeySort"), extendConfig({}, controller.args[2], "fonticonConfigurationKeySort"));
        flxConfigurationKeySort.add(fonticonConfigurationKeySort);
        flxConfigurationKey.add(lblConfigurationKey, flxConfigurationKeySort);
        var flxConfigurationValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxConfigurationValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "23%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "27%",
            "zIndex": 1
        }, controller.args[0], "flxConfigurationValue"), extendConfig({}, controller.args[1], "flxConfigurationValue"), extendConfig({}, controller.args[2], "flxConfigurationValue"));
        flxConfigurationValue.setDefaultUnit(kony.flex.DP);
        var lblConfigurationValue = new kony.ui.Label(extendConfig({
            "id": "lblConfigurationValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.valueInCaps\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblConfigurationValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConfigurationValue"), extendConfig({}, controller.args[2], "lblConfigurationValue"));
        var flxConfigurationValueSort = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxConfigurationValueSort",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0px",
            "width": "20px"
        }, controller.args[0], "flxConfigurationValueSort"), extendConfig({}, controller.args[1], "flxConfigurationValueSort"), extendConfig({}, controller.args[2], "flxConfigurationValueSort"));
        flxConfigurationValueSort.setDefaultUnit(kony.flex.DP);
        var fonticonConfigurationValueSort = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "fonticonConfigurationValueSort",
            "isVisible": true,
            "right": "10px",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonConfigurationValueSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonConfigurationValueSort"), extendConfig({}, controller.args[2], "fonticonConfigurationValueSort"));
        flxConfigurationValueSort.add(fonticonConfigurationValueSort);
        flxConfigurationValue.add(lblConfigurationValue, flxConfigurationValueSort);
        var flxConfigurationType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxConfigurationType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "flxConfigurationType"), extendConfig({}, controller.args[1], "flxConfigurationType"), extendConfig({}, controller.args[2], "flxConfigurationType"));
        flxConfigurationType.setDefaultUnit(kony.flex.DP);
        var lblConfigurationType = new kony.ui.Label(extendConfig({
            "id": "lblConfigurationType",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.typeInCaps\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblConfigurationType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConfigurationType"), extendConfig({}, controller.args[2], "lblConfigurationType"));
        var flxConfigurationTypeFilter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12px",
            "id": "flxConfigurationTypeFilter",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0px",
            "width": "12px"
        }, controller.args[0], "flxConfigurationTypeFilter"), extendConfig({}, controller.args[1], "flxConfigurationTypeFilter"), extendConfig({}, controller.args[2], "flxConfigurationTypeFilter"));
        flxConfigurationTypeFilter.setDefaultUnit(kony.flex.DP);
        var lblConfigurationTypeFilter = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblConfigurationTypeFilter",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblConfigurationTypeFilter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConfigurationTypeFilter"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "lblConfigurationTypeFilter"));
        flxConfigurationTypeFilter.add(lblConfigurationTypeFilter);
        flxConfigurationType.add(lblConfigurationType, flxConfigurationTypeFilter);
        var flxConfigurationTarget = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxConfigurationTarget",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "70%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "24%",
            "zIndex": 1
        }, controller.args[0], "flxConfigurationTarget"), extendConfig({}, controller.args[1], "flxConfigurationTarget"), extendConfig({}, controller.args[2], "flxConfigurationTarget"));
        flxConfigurationTarget.setDefaultUnit(kony.flex.DP);
        var lblConfigurationTarget = new kony.ui.Label(extendConfig({
            "id": "lblConfigurationTarget",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.targetInCaps\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblConfigurationTarget"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConfigurationTarget"), extendConfig({}, controller.args[2], "lblConfigurationTarget"));
        var flxConfigurationTargetFilter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12px",
            "id": "flxConfigurationTargetFilter",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0px",
            "width": "12px"
        }, controller.args[0], "flxConfigurationTargetFilter"), extendConfig({}, controller.args[1], "flxConfigurationTargetFilter"), extendConfig({}, controller.args[2], "flxConfigurationTargetFilter"));
        flxConfigurationTargetFilter.setDefaultUnit(kony.flex.DP);
        var lblConfigurationTargetFilter = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblConfigurationTargetFilter",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblConfigurationTargetFilter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConfigurationTargetFilter"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "lblConfigurationTargetFilter"));
        flxConfigurationTargetFilter.add(lblConfigurationTargetFilter);
        flxConfigurationTarget.add(lblConfigurationTarget, flxConfigurationTargetFilter);
        var flxConfigurationContextualMenu = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxConfigurationContextualMenu",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "94%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "6%",
            "zIndex": 1
        }, controller.args[0], "flxConfigurationContextualMenu"), extendConfig({}, controller.args[1], "flxConfigurationContextualMenu"), extendConfig({}, controller.args[2], "flxConfigurationContextualMenu"));
        flxConfigurationContextualMenu.setDefaultUnit(kony.flex.DP);
        flxConfigurationContextualMenu.add();
        var lblHeaderSeparator = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1dp",
            "id": "lblHeaderSeparator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknConfigurationBundleSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblHeaderSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderSeparator"), extendConfig({}, controller.args[2], "lblHeaderSeparator"));
        flxHeaders.add(flxConfigurationArrow, flxConfigurationKey, flxConfigurationValue, flxConfigurationType, flxConfigurationTarget, flxConfigurationContextualMenu, lblHeaderSeparator);
        var flxTableView = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTableView",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "40px",
            "zIndex": 1
        }, controller.args[0], "flxTableView"), extendConfig({}, controller.args[1], "flxTableView"), extendConfig({}, controller.args[2], "flxTableView"));
        flxTableView.setDefaultUnit(kony.flex.DP);
        var segConfiguration = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "fonticonArrow": "",
                "lblConfigSeparator": kony.i18n.getLocalizedString("i18n.userwidgetmodel.lblHeaderSeparator"),
                "lblConfigurationKey": "Configuration Key",
                "lblConfigurationTarget": "CLIENT",
                "lblConfigurationType": "PREFERENCE",
                "lblConfigurationValue": "Configuration Value",
                "lblIconOptions": ""
            }, {
                "fonticonArrow": "",
                "lblConfigSeparator": kony.i18n.getLocalizedString("i18n.userwidgetmodel.lblHeaderSeparator"),
                "lblConfigurationKey": "Configuration Key",
                "lblConfigurationTarget": "CLIENT",
                "lblConfigurationType": "PREFERENCE",
                "lblConfigurationValue": "Configuration Value",
                "lblIconOptions": ""
            }, {
                "fonticonArrow": "",
                "lblConfigSeparator": kony.i18n.getLocalizedString("i18n.userwidgetmodel.lblHeaderSeparator"),
                "lblConfigurationKey": "Configuration Key",
                "lblConfigurationTarget": "CLIENT",
                "lblConfigurationType": "PREFERENCE",
                "lblConfigurationValue": "Configuration Value",
                "lblIconOptions": ""
            }],
            "groupCells": false,
            "height": "200dp",
            "id": "segConfiguration",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxConfiguration",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkbox.png"
            },
            "separatorRequired": false,
            "showScrollbars": true,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxArrow": "flxArrow",
                "flxConfiguration": "flxConfiguration",
                "flxConfigurationArrow": "flxConfigurationArrow",
                "flxConfigurationContextualMenu": "flxConfigurationContextualMenu",
                "flxConfigurationKey": "flxConfigurationKey",
                "flxConfigurationTarget": "flxConfigurationTarget",
                "flxConfigurationType": "flxConfigurationType",
                "flxConfigurationValue": "flxConfigurationValue",
                "flxOptions": "flxOptions",
                "fonticonArrow": "fonticonArrow",
                "lblConfigSeparator": "lblConfigSeparator",
                "lblConfigurationKey": "lblConfigurationKey",
                "lblConfigurationTarget": "lblConfigurationTarget",
                "lblConfigurationType": "lblConfigurationType",
                "lblConfigurationValue": "lblConfigurationValue",
                "lblIconOptions": "lblIconOptions"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segConfiguration"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segConfiguration"), extendConfig({}, controller.args[2], "segConfiguration"));
        var flxPagination = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 20,
            "clipBounds": false,
            "height": "30px",
            "id": "flxPagination",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox0d9c3974835234d",
            "top": "15dp",
            "width": "222px",
            "zIndex": 1
        }, controller.args[0], "flxPagination"), extendConfig({}, controller.args[1], "flxPagination"), extendConfig({}, controller.args[2], "flxPagination"));
        flxPagination.setDefaultUnit(kony.flex.DP);
        var lbxPagination = new kony.ui.ListBox(extendConfig({
            "height": "30dp",
            "id": "lbxPagination",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["lb1", "Page 1 of 20"],
                ["lb2", "Page 2 of 20"],
                ["lb3", "Page 3 of 20"],
                ["lb4", "Page 4 of 20"]
            ],
            "selectedKey": "lb1",
            "selectedKeyValue": ["lb1", "Page 1 of 20"],
            "skin": "sknlbxNobgNoBorderPagination",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lbxPagination"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbxPagination"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lbxPagination"));
        var flxPaginationSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxPaginationSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "2dp",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxPaginationSeperator"), extendConfig({}, controller.args[1], "flxPaginationSeperator"), extendConfig({}, controller.args[2], "flxPaginationSeperator"));
        flxPaginationSeperator.setDefaultUnit(kony.flex.DP);
        flxPaginationSeperator.add();
        var flxPrevious = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxPrevious",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "sknflxRadius40px",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxPrevious"), extendConfig({}, controller.args[1], "flxPrevious"), extendConfig({}, controller.args[2], "flxPrevious"));
        flxPrevious.setDefaultUnit(kony.flex.DP);
        var fontIconPrevious = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconPrevious",
            "isVisible": true,
            "left": "0px",
            "skin": "sknFontIconPrevNextPage",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconPrevious\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconPrevious"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconPrevious"), extendConfig({}, controller.args[2], "fontIconPrevious"));
        flxPrevious.add(fontIconPrevious);
        var flxNext = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxNext",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "3dp",
            "isModalContainer": false,
            "skin": "sknflxRadius40px",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxNext"), extendConfig({}, controller.args[1], "flxNext"), extendConfig({}, controller.args[2], "flxNext"));
        flxNext.setDefaultUnit(kony.flex.DP);
        var fontIconNext = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconNext",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconPrevNextPage",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconNext\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconNext"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconNext"), extendConfig({}, controller.args[2], "fontIconNext"));
        flxNext.add(fontIconNext);
        var imgPreviousArrow = new kony.ui.Image2(extendConfig({
            "height": "30px",
            "id": "imgPreviousArrow",
            "isVisible": false,
            "left": "15px",
            "skin": "slImage",
            "src": "rightarrow_circel2x.png",
            "top": "0px",
            "width": "30dp",
            "zIndex": 1
        }, controller.args[0], "imgPreviousArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgPreviousArrow"), extendConfig({}, controller.args[2], "imgPreviousArrow"));
        var imgNextArrow = new kony.ui.Image2(extendConfig({
            "height": "30dp",
            "id": "imgNextArrow",
            "isVisible": false,
            "left": "10dp",
            "skin": "slImage",
            "src": "left_cricle.png",
            "top": "0dp",
            "width": "30dp",
            "zIndex": 1
        }, controller.args[0], "imgNextArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgNextArrow"), extendConfig({}, controller.args[2], "imgNextArrow"));
        flxPagination.add(lbxPagination, flxPaginationSeperator, flxPrevious, flxNext, imgPreviousArrow, imgNextArrow);
        flxTableView.add(segConfiguration, flxPagination);
        configurationData.add(flxHeaders, flxTableView);
        return configurationData;
    }
})