define(function() {
    return function(controller) {
        var productTypeFilterMenu = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "productTypeFilterMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 5
        }, controller.args[0], "productTypeFilterMenu"), extendConfig({}, controller.args[1], "productTypeFilterMenu"), extendConfig({}, controller.args[2], "productTypeFilterMenu"));
        productTypeFilterMenu.setDefaultUnit(kony.flex.DP);
        var flxArrowImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12dp",
            "id": "flxArrowImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxArrowImage"), extendConfig({}, controller.args[1], "flxArrowImage"), extendConfig({}, controller.args[2], "flxArrowImage"));
        flxArrowImage.setDefaultUnit(kony.flex.DP);
        var imgUpArrow = new kony.ui.Image2(extendConfig({
            "height": "12dp",
            "id": "imgUpArrow",
            "isVisible": true,
            "right": "15dp",
            "skin": "slImage",
            "src": "uparrow_2x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgUpArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgUpArrow"), extendConfig({}, controller.args[2], "imgUpArrow"));
        flxArrowImage.add(imgUpArrow);
        var flxChechboxOuter = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "120px",
            "horizontalScrollIndicator": true,
            "id": "flxChechboxOuter",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox0id84829d89c84d",
            "top": "11dp",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxChechboxOuter"), extendConfig({}, controller.args[1], "flxChechboxOuter"), extendConfig({}, controller.args[2], "flxChechboxOuter"));
        flxChechboxOuter.setDefaultUnit(kony.flex.DP);
        var segStatusFilterDropdown = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "imgCheckBox": "checkbox.png",
                "lblDescription": "Active"
            }, {
                "imgCheckBox": "checkbox.png",
                "lblDescription": "Inactive"
            }, {
                "imgCheckBox": "checkbox.png",
                "lblDescription": "Suspended"
            }],
            "groupCells": false,
            "id": "segStatusFilterDropdown",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxSearchDropDown",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkbox.png"
            },
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 0,
            "showScrollbars": false,
            "top": "5dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCheckBox": "flxCheckBox",
                "flxSearchDropDown": "flxSearchDropDown",
                "imgCheckBox": "imgCheckBox",
                "lblDescription": "lblDescription"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segStatusFilterDropdown"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segStatusFilterDropdown"), extendConfig({}, controller.args[2], "segStatusFilterDropdown"));
        var lblSeperator = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblTableHeaderLine",
            "text": "-",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        var flxFilterSort = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5px",
            "clipBounds": true,
            "id": "flxFilterSort",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxFilterSort"), extendConfig({}, controller.args[1], "flxFilterSort"), extendConfig({}, controller.args[2], "flxFilterSort"));
        flxFilterSort.setDefaultUnit(kony.flex.DP);
        var lblSortByAlphabet = new kony.ui.Label(extendConfig({
            "bottom": "5dp",
            "id": "lblSortByAlphabet",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblLato5d6c7f12px",
            "text": "SORT BY ALPHABET",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSortByAlphabet"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSortByAlphabet"), extendConfig({}, controller.args[2], "lblSortByAlphabet"));
        var fontIconSortByAlpha = new kony.ui.Label(extendConfig({
            "bottom": "5dp",
            "id": "fontIconSortByAlpha",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortByAlpha"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortByAlpha"), extendConfig({}, controller.args[2], "fontIconSortByAlpha"));
        flxFilterSort.add(lblSortByAlphabet, fontIconSortByAlpha);
        flxChechboxOuter.add(segStatusFilterDropdown, lblSeperator, flxFilterSort);
        productTypeFilterMenu.add(flxArrowImage, flxChechboxOuter);
        return productTypeFilterMenu;
    }
})