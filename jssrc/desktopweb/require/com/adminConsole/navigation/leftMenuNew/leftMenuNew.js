define(function() {
    return function(controller) {
        var leftMenuNew = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "leftMenuNew",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknLeftMenuBackground",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "leftMenuNew"), extendConfig({}, controller.args[1], "leftMenuNew"), extendConfig({}, controller.args[2], "leftMenuNew"));
        leftMenuNew.setDefaultUnit(kony.flex.DP);
        var flxBrandLogo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80dp",
            "id": "flxBrandLogo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyCopyslFbox4",
            "top": 0,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBrandLogo"), extendConfig({}, controller.args[1], "flxBrandLogo"), extendConfig({}, controller.args[2], "flxBrandLogo"));
        flxBrandLogo.setDefaultUnit(kony.flex.DP);
        var brandLogo = new kony.ui.Image2(extendConfig({
            "bottom": "20dp",
            "id": "brandLogo",
            "isVisible": true,
            "left": "15dp",
            "right": "20dp",
            "skin": "CopyCopyslImage",
            "src": "konydbxlogobignverted.png",
            "top": "20dp",
            "width": "140dp",
            "zIndex": 1
        }, controller.args[0], "brandLogo"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "brandLogo"), extendConfig({}, controller.args[2], "brandLogo"));
        flxBrandLogo.add(brandLogo);
        var flxScrollMenu = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "40dp",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxScrollMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "80dp",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxScrollMenu"), extendConfig({}, controller.args[1], "flxScrollMenu"), extendConfig({}, controller.args[2], "flxScrollMenu"));
        flxScrollMenu.setDefaultUnit(kony.flex.DP);
        var segMenu = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "groupCells": false,
            "id": "segMenu",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {},
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segMenu"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segMenu"), extendConfig({}, controller.args[2], "segMenu"));
        flxScrollMenu.add(segMenu);
        var flxBottomTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxBottomTitle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "sknflx3",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBottomTitle"), extendConfig({}, controller.args[1], "flxBottomTitle"), extendConfig({}, controller.args[2], "flxBottomTitle"));
        flxBottomTitle.setDefaultUnit(kony.flex.DP);
        var flxBottomLogo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "37dp",
            "id": "flxBottomLogo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxBottomLogo"), extendConfig({}, controller.args[1], "flxBottomLogo"), extendConfig({}, controller.args[2], "flxBottomLogo"));
        flxBottomLogo.setDefaultUnit(kony.flex.DP);
        var lblBottomTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblBottomTitle",
            "isVisible": true,
            "left": "0dp",
            "skin": "slLabel08e64a51cad174e",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblBottomTitle\")",
            "width": "70dp",
            "zIndex": 1
        }, controller.args[0], "lblBottomTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBottomTitle"), extendConfig({}, controller.args[2], "lblBottomTitle"));
        var imgBottomLogo = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "imgBottomLogo",
            "isVisible": true,
            "left": "70dp",
            "skin": "slImage",
            "src": "konydbxinverted.png",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "imgBottomLogo"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgBottomLogo"), extendConfig({}, controller.args[2], "imgBottomLogo"));
        flxBottomLogo.add(lblBottomTitle, imgBottomLogo);
        flxBottomTitle.add(flxBottomLogo);
        leftMenuNew.add(flxBrandLogo, flxScrollMenu, flxBottomTitle);
        return leftMenuNew;
    }
})