define("com/adminConsole/StaticContentManagement/faq/tableView/usertableViewController", function() {
    return {
        toggleContextualMenu: function() {
            // alert("infunction");
            kony.application.getCurrentForm().flxSelectOptionsHeader.setVisibility(false);
            kony.application.getCurrentForm().tableView.imgHeaderCheckBox.src = "checkbox.png"
            var index = kony.application.getCurrentForm().tableView.segServicesAndFaq.selectedRowIndex;
            var sectionIndex = index[0];
            var rowIndex = index[1];
            if (kony.application.getCurrentForm().flxSelectOptions.isVisible === false) {
                kony.application.getCurrentForm().tableView.segServicesAndFaq.selectedRowIndices = [
                    [0, [rowIndex]]
                ];
            } else {
                kony.application.getCurrentForm().tableView.segServicesAndFaq.selectedRowIndices = null;
            }
            var mainSegmentRowIndex;
            kony.store.setItem(mainSegmentRowIndex, rowIndex);
            var templateHeight;
            var data = kony.application.getCurrentForm().tableView.segServicesAndFaq.data;
            if (data[0].template === "flxOutage") templateHeight = 80;
            else if (data[0].template === "flxServicesAndFaq") templateHeight = 65;
            else templateHeight = 50;
            var height;
            if (data[0].template === "flxServicesAndFaq")
                if (data.length === rowIndex + 1) {
                    var menu_height = kony.application.getCurrentForm().flxSelectOptions.height;
                    height = (80 + ((rowIndex) * templateHeight)) - 105;
                } else {
                    height = 80 + ((rowIndex + 1) * templateHeight);
                }
            else height = 45 + ((rowIndex + 1) * templateHeight);
            //this.updateContextualMenu();
            if (data[rowIndex].lblServiceStatus.text === "Active") {
                kony.application.getCurrentForm().flxSelectOptions.flxDeactivate.lblOption2.text = "Deactivate";
                kony.application.getCurrentForm().flxSelectOptions.flxDeactivate.fonticonDeactive.text = "";
                if (data[0].template === "flxOutage") {
                    kony.application.getCurrentForm().flxSelectOptions.flxDelete.setVisibility(true);
                }
            } else {
                kony.application.getCurrentForm().flxSelectOptions.flxDeactivate.lblOption2.text = "Activate";
                kony.application.getCurrentForm().flxSelectOptions.flxDeactivate.fonticonDeactive.text = "";
                if (data[0].template === "flxOutage") {
                    kony.application.getCurrentForm().flxSelectOptions.flxDelete.setVisibility(false);
                }
            }
            kony.application.getCurrentForm().flxSelectOptions.top = (this.ScreenY - 148) + "px";
            if (kony.application.getCurrentForm().flxSelectOptions.isVisible === false) {
                kony.application.getCurrentForm().flxSelectOptions.setVisibility(true);
                kony.application.getCurrentForm().flxSelectOptions.setFocus(true);
                kony.application.getCurrentForm().forceLayout();
                this.fixContextualMenu(this.ScreenY - 148);
            } else {
                kony.application.getCurrentForm().flxSelectOptions.setVisibility(false);
            }
            kony.print("toggleVisibility TableView called");
        },
        fixContextualMenu: function(heightVal) {
            if (((kony.application.getCurrentForm().flxSelectOptions.frame.height + heightVal) > (kony.application.getCurrentForm().tableView.segServicesAndFaq.frame.height + 50)) && kony.application.getCurrentForm().flxSelectOptions.frame.height < kony.application.getCurrentForm().tableView.segServicesAndFaq.frame.height) {
                kony.application.getCurrentForm().flxSelectOptions.top = ((heightVal - kony.application.getCurrentForm().flxSelectOptions.frame.height) - 60) + "px";
            } else {
                kony.application.getCurrentForm().flxSelectOptions.top = heightVal + "px";
            }
            this.view.forceLayout();
        },
        fixScreenY: function(screenY) {
            this.ScreenY = screenY;
        }
    };
});
define("com/adminConsole/StaticContentManagement/faq/tableView/tableViewControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/StaticContentManagement/faq/tableView/tableViewController", ["com/adminConsole/StaticContentManagement/faq/tableView/usertableViewController", "com/adminConsole/StaticContentManagement/faq/tableView/tableViewControllerActions"], function() {
    var controller = require("com/adminConsole/StaticContentManagement/faq/tableView/usertableViewController");
    var actions = require("com/adminConsole/StaticContentManagement/faq/tableView/tableViewControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
