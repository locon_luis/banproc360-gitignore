define(function() {
    return function(controller) {
        var tableView = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "tableView",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "tableView"), extendConfig({}, controller.args[1], "tableView"), extendConfig({}, controller.args[2], "tableView"));
        tableView.setDefaultUnit(kony.flex.DP);
        var flxTableView = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTableView",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTableView"), extendConfig({}, controller.args[1], "flxTableView"), extendConfig({}, controller.args[2], "flxTableView"));
        flxTableView.setDefaultUnit(kony.flex.DP);
        var flxHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeaders"), extendConfig({}, controller.args[1], "flxHeaders"), extendConfig({}, controller.args[2], "flxHeaders"));
        flxHeaders.setDefaultUnit(kony.flex.DP);
        var flxHeaderLeft = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxHeaderLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 2
        }, controller.args[0], "flxHeaderLeft"), extendConfig({}, controller.args[1], "flxHeaderLeft"), extendConfig({}, controller.args[2], "flxHeaderLeft"));
        flxHeaderLeft.setDefaultUnit(kony.flex.DP);
        var flxHeaderCheckbox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30px",
            "id": "flxHeaderCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_d77d74d95a5b43fc83a09b335071bd85,
            "skin": "slFbox",
            "width": "30px",
            "zIndex": 2
        }, controller.args[0], "flxHeaderCheckbox"), extendConfig({}, controller.args[1], "flxHeaderCheckbox"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxHeaderCheckbox"));
        flxHeaderCheckbox.setDefaultUnit(kony.flex.DP);
        var imgHeaderCheckBox = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "imgHeaderCheckBox",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "12px",
            "zIndex": 2
        }, controller.args[0], "imgHeaderCheckBox"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgHeaderCheckBox"), extendConfig({}, controller.args[2], "imgHeaderCheckBox"));
        flxHeaderCheckbox.add(imgHeaderCheckBox);
        var flxHeaderServiceName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60px",
            "id": "flxHeaderServiceName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "55px",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "130px",
            "zIndex": 1
        }, controller.args[0], "flxHeaderServiceName"), extendConfig({}, controller.args[1], "flxHeaderServiceName"), extendConfig({}, controller.args[2], "flxHeaderServiceName"));
        flxHeaderServiceName.setDefaultUnit(kony.flex.DP);
        var lblHeaderServiceName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderServiceName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "lblHeaderServiceName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderServiceName"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblHeaderServiceName"));
        var lblFontIconSortName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFontIconSortName",
            "isVisible": true,
            "right": "10px",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFontIconSortName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconSortName"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "lblFontIconSortName"));
        flxHeaderServiceName.add(lblHeaderServiceName, lblFontIconSortName);
        flxHeaderLeft.add(flxHeaderCheckbox, flxHeaderServiceName);
        var lblHeaderDescription = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderDescription",
            "isVisible": true,
            "left": "30.20%",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Services.Supportedchannels\")",
            "width": "34%",
            "zIndex": 1
        }, controller.args[0], "lblHeaderDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderDescription"), extendConfig({}, controller.args[2], "lblHeaderDescription"));
        var flxHeaderStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60px",
            "id": "flxHeaderStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "70px",
            "skin": "slFbox",
            "width": "130px",
            "zIndex": 1
        }, controller.args[0], "flxHeaderStatus"), extendConfig({}, controller.args[1], "flxHeaderStatus"), extendConfig({}, controller.args[2], "flxHeaderStatus"));
        flxHeaderStatus.setDefaultUnit(kony.flex.DP);
        var lblHeaderServiceStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderServiceStatus",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
            "width": "60dp",
            "zIndex": 1
        }, controller.args[0], "lblHeaderServiceStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderServiceStatus"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblHeaderServiceStatus"));
        var lblFontIconSortStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFontIconSortStatus",
            "isVisible": true,
            "right": "10px",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFontIconSortStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconSortStatus"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "lblFontIconSortStatus"));
        flxHeaderStatus.add(lblHeaderServiceStatus, lblFontIconSortStatus);
        var lblHeaderSeparator = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1dp",
            "id": "lblHeaderSeparator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblTableHeaderLine",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
            "width": "96%",
            "zIndex": 2
        }, controller.args[0], "lblHeaderSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderSeparator"), extendConfig({}, controller.args[2], "lblHeaderSeparator"));
        flxHeaders.add(flxHeaderLeft, lblHeaderDescription, flxHeaderStatus, lblHeaderSeparator);
        var segServicesAndFaq = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "ImgArrow": "img_desc_arrow.png",
                "imgCheckBox": "checkbox.png",
                "imgOptions": "dots3x.png",
                "imgServiceStatus": "active_circle2x.png",
                "lblDescription": "Mobile App",
                "lblSeparator": "Label",
                "lblServiceName": "Jompay",
                "lblServiceStatus": "Active"
            }, {
                "ImgArrow": "img_desc_arrow.png",
                "imgCheckBox": "checkbox.png",
                "imgOptions": "dots3x.png",
                "imgServiceStatus": "active_circle2x.png",
                "lblDescription": "Mobile App",
                "lblSeparator": "Label",
                "lblServiceName": "Jompay",
                "lblServiceStatus": "Active"
            }, {
                "ImgArrow": "img_desc_arrow.png",
                "imgCheckBox": "checkbox.png",
                "imgOptions": "dots3x.png",
                "imgServiceStatus": "active_circle2x.png",
                "lblDescription": "Mobile App",
                "lblSeparator": "Label",
                "lblServiceName": "Jompay",
                "lblServiceStatus": "Active"
            }],
            "groupCells": false,
            "id": "segServicesAndFaq",
            "isVisible": true,
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxServicesAndFaq",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkbox.png"
            },
            "separatorRequired": false,
            "showScrollbars": true,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "ImgArrow": "ImgArrow",
                "flxCheckbox": "flxCheckbox",
                "flxDropdown": "flxDropdown",
                "flxOptions": "flxOptions",
                "flxServicesAndFaq": "flxServicesAndFaq",
                "flxServicesAndFaqLeft": "flxServicesAndFaqLeft",
                "flxStatus": "flxStatus",
                "imgCheckBox": "imgCheckBox",
                "imgOptions": "imgOptions",
                "imgServiceStatus": "imgServiceStatus",
                "lblDescription": "lblDescription",
                "lblSeparator": "lblSeparator",
                "lblServiceName": "lblServiceName",
                "lblServiceStatus": "lblServiceStatus"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segServicesAndFaq"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segServicesAndFaq"), extendConfig({}, controller.args[2], "segServicesAndFaq"));
        flxTableView.add(flxHeaders, segServicesAndFaq);
        var flxNoRecordsFound = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200dp",
            "id": "flxNoRecordsFound",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoRecordsFound"), extendConfig({}, controller.args[1], "flxNoRecordsFound"), extendConfig({}, controller.args[2], "flxNoRecordsFound"));
        flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
        var rtxNoRecords = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxNoRecords",
            "isVisible": true,
            "skin": "sknRtxLato84939e12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxNoRecords"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxNoRecords"), extendConfig({}, controller.args[2], "rtxNoRecords"));
        flxNoRecordsFound.add(rtxNoRecords);
        tableView.add(flxTableView, flxNoRecordsFound);
        return tableView;
    }
})