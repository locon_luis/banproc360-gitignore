define("com/adminConsole/Customers/ToolTip/userToolTipController", function() {
    return {};
});
define("com/adminConsole/Customers/ToolTip/ToolTipControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/Customers/ToolTip/ToolTipController", ["com/adminConsole/Customers/ToolTip/userToolTipController", "com/adminConsole/Customers/ToolTip/ToolTipControllerActions"], function() {
    var controller = require("com/adminConsole/Customers/ToolTip/userToolTipController");
    var actions = require("com/adminConsole/Customers/ToolTip/ToolTipControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
