define(function() {
    return function(controller) {
        var ToolTip = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "ToolTip",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "230px"
        }, controller.args[0], "ToolTip"), extendConfig({}, controller.args[1], "ToolTip"), extendConfig({}, controller.args[2], "ToolTip"));
        ToolTip.setDefaultUnit(kony.flex.DP);
        var lblarrow = new kony.ui.Label(extendConfig({
            "centerX": "80%",
            "height": "10dp",
            "id": "lblarrow",
            "isVisible": true,
            "left": "0px",
            "skin": "sknfonticonCustomersTooltip",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconUp\")",
            "top": "0px",
            "width": "17px",
            "zIndex": 20
        }, controller.args[0], "lblarrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblarrow"), extendConfig({}, controller.args[2], "lblarrow"));
        var flxToolTipMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "60px",
            "id": "flxToolTipMessage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxTooltip",
            "top": "9px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxToolTipMessage"), extendConfig({}, controller.args[1], "flxToolTipMessage"), extendConfig({}, controller.args[2], "flxToolTipMessage"));
        flxToolTipMessage.setDefaultUnit(kony.flex.DP);
        var lblNoConcentToolTip = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "45%",
            "id": "lblNoConcentToolTip",
            "isVisible": true,
            "skin": "sknlblCSRAssist",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CSRAssistDisabled\")",
            "width": "93%",
            "zIndex": 1
        }, controller.args[0], "lblNoConcentToolTip"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoConcentToolTip"), extendConfig({}, controller.args[2], "lblNoConcentToolTip"));
        flxToolTipMessage.add(lblNoConcentToolTip);
        ToolTip.add(lblarrow, flxToolTipMessage);
        return ToolTip;
    }
})