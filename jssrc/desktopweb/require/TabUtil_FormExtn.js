define(function() {
    /* for header tabs*/
    var tabUtilButtonFunction = function(widgetArray, selectedTab) {
        for (var i = 0; i < widgetArray.length; i++) {
            if (widgetArray[i] === selectedTab) {
                widgetArray[i].skin = "sknBtnUtilActiveUnderline485b7512pxBold"; //selected skin
                widgetArray[i].hoverSkin = "sknBtnUtilActiveUnderline485b7512pxBold";
            } else {
                widgetArray[i].skin = "sknBtnUtilRest73767812pxReg"; //normal skin sknBtnUtilRest73767812pxReg
                widgetArray[i].hoverSkin = "sknBtnUtilHover30353612pxReg"; //hover skin
            }
        }
    };
    var tabUtilLabelFunction = function(widgetArray, selectedTab) {
        for (var i = 0; i < widgetArray.length; i++) {
            if (widgetArray[i] === selectedTab) {
                widgetArray[i].skin = "sknLblUtilActive485b7512pxBold"; //selected skin
                widgetArray[i].hoverSkin = "sknLblUtilActive485b7512pxBold";
            } else {
                widgetArray[i].skin = "sknLblUtilRest73767812pxReg"; //normal skin
                widgetArray[i].hoverSkin = "sknLblUtilHover30353612pxReg"; //hover skin
            }
        }
    };
    /* for vertical tabs*/
    var tabUtilVerticleButtonFunction = function(widgetArray, selectedTab) {
        for (var i = 0; i < widgetArray.length; i++) {
            if (widgetArray[i] === selectedTab) {
                widgetArray[i].skin = "sknBtnUtilActive485b7512pxBold"; //selected skin
                widgetArray[i].hoverSkin = "sknBtnUtilActive485b7512pxBold";
            } else {
                widgetArray[i].skin = "sknBtnUtilRest73767812pxReg"; //normal skin sknBtnUtilRest73767812pxReg
                widgetArray[i].hoverSkin = "sknBtnUtilHover30353612pxReg"; //hover skin
            }
        }
    };
    var tabUtilVerticleArrowVisibilityFunction = function(widgetArray, selectedTab) {
        for (var i = 0; i < widgetArray.length; i++) {
            if (widgetArray[i] === selectedTab) {
                widgetArray[i].setVisibility(true);
            } else {
                widgetArray[i].setVisibility(false);
            }
        }
    };
    //customer profile/loans tabs
    var subTabsButtonUtilFunction = function(widgetArray, selectedTab) {
        for (var i = 0; i < widgetArray.length; i++) {
            if (widgetArray[i] === selectedTab) {
                widgetArray[i].skin = "sknBord7d9e0Rounded3px485c75"; //selected skin
            } else {
                widgetArray[i].skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px"; //normal skin
            }
        }
    };
    return {
        tabUtilButtonFunction: tabUtilButtonFunction,
        tabUtilLabelFunction: tabUtilLabelFunction,
        tabUtilVerticleButtonFunction: tabUtilVerticleButtonFunction,
        tabUtilVerticleArrowVisibilityFunction: tabUtilVerticleArrowVisibilityFunction,
        subTabsButtonUtilFunction: subTabsButtonUtilFunction
    };
});