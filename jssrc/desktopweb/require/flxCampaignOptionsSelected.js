define("flxCampaignOptionsSelected", function() {
    return function(controller) {
        var flxCampaignOptionsSelected = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCampaignOptionsSelected",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {});
        flxCampaignOptionsSelected.setDefaultUnit(kony.flex.DP);
        var flxAddOptionWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddOptionWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": 25,
            "skin": "sknflxffffffop0e",
            "top": "10dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknFlxSegRowHover11abeb"
        });
        flxAddOptionWrapper.setDefaultUnit(kony.flex.DP);
        var lblOption = new kony.ui.Label({
            "bottom": 15,
            "id": "lblOption",
            "isVisible": true,
            "left": "15dp",
            "right": "45px",
            "skin": "sknlbl485c7514px",
            "text": "John Doe",
            "top": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxClose = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "5px",
            "skin": "slFbox",
            "top": "10dp",
            "width": "30px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxClose.setDefaultUnit(kony.flex.DP);
        var fontIconClose = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "fontIconClose",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconCross14px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxClose.add(fontIconClose);
        flxAddOptionWrapper.add(lblOption, flxClose);
        flxCampaignOptionsSelected.add(flxAddOptionWrapper);
        return flxCampaignOptionsSelected;
    }
})