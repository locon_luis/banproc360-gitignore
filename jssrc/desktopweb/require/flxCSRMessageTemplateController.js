define("userflxCSRMessageTemplateController", {
    hide: function() {
        this.executeOnParent("toggleVisibility");
    }
});
define("flxCSRMessageTemplateControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("flxCSRMessageTemplateController", ["userflxCSRMessageTemplateController", "flxCSRMessageTemplateControllerActions"], function() {
    var controller = require("userflxCSRMessageTemplateController");
    var controllerActions = ["flxCSRMessageTemplateControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
