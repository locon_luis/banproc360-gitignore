define("flxListOfApplications", function() {
    return function(controller) {
        var flxListOfApplications = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxListOfApplications",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknFlxf9fbfd1000"
        });
        flxListOfApplications.setDefaultUnit(kony.flex.DP);
        var lblApplicationId = new kony.ui.Label({
            "id": "lblApplicationId",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLoanType = new kony.ui.Label({
            "id": "lblLoanType",
            "isVisible": true,
            "left": "19%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAmount = new kony.ui.Label({
            "id": "lblAmount",
            "isVisible": true,
            "left": "32%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblApplicant = new kony.ui.Label({
            "id": "lblApplicant",
            "isVisible": true,
            "left": "48%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblModifyOn = new kony.ui.Label({
            "id": "lblModifyOn",
            "isVisible": true,
            "left": "64%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCreateOn = new kony.ui.Label({
            "id": "lblCreateOn",
            "isVisible": true,
            "left": "80%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblResume = new kony.ui.Label({
            "id": "lblResume",
            "isVisible": true,
            "left": "94%",
            "skin": "sknLbladminfonticon",
            "text": "",
            "top": "20dp",
            "width": "5%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbladminfonticonHover"
        });
        var lblSeparatorListOfApplications = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeparatorListOfApplications",
            "isVisible": true,
            "left": "22dp",
            "right": "22dp",
            "skin": "sknLblD5D9DD1000",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxListOfApplications.add(lblApplicationId, lblLoanType, lblAmount, lblApplicant, lblModifyOn, lblCreateOn, lblResume, lblSeparatorListOfApplications);
        return flxListOfApplications;
    }
})