define("flxsegLocations", function() {
    return function(controller) {
        var flxsegLocations = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxsegLocations",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxsegLocations.setDefaultUnit(kony.flex.DP);
        var lblBranchName = new kony.ui.Label({
            "bottom": "18dp",
            "id": "lblBranchName",
            "isVisible": true,
            "left": "55px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Special Branch",
            "top": "18dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCode = new kony.ui.Label({
            "bottom": "18dp",
            "id": "lblCode",
            "isVisible": true,
            "left": "21.98%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "TD123340",
            "top": "18dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "bottom": "18dp",
            "id": "lblDescription",
            "isVisible": true,
            "left": "32.96%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Pensions - Houston Branch",
            "top": "18dp",
            "width": "28%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPhone = new kony.ui.Label({
            "bottom": "18dp",
            "id": "lblPhone",
            "isVisible": true,
            "left": "61.87%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "707-839-1154",
            "top": "18dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "74.73%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "9%",
            "zIndex": 1
        }, {}, {});
        flxType.setDefaultUnit(kony.flex.DP);
        var lblType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblType",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Branch",
            "width": "70px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxType.add(lblType, lblIconType);
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "84.06%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "8%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "70px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblStatus, lblIconStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b841079c89634eada06d07f145523502,
            "right": "43px",
            "skin": "sknFlxBorffffff1pxRound",
            "top": "11dp",
            "width": "25px",
            "zIndex": 10
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblOptions = new kony.ui.Label({
            "bottom": "0dp",
            "centerX": "46%",
            "centerY": "47%",
            "id": "lblOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblOptions);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "35px",
            "right": "35px",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxsegLocations.add(lblBranchName, lblCode, lblDescription, lblPhone, flxType, flxStatus, flxOptions, lblSeperator);
        return flxsegLocations;
    }
})