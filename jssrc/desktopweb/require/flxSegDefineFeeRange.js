define("flxSegDefineFeeRange", function() {
    return function(controller) {
        var flxSegDefineFeeRange = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxSegDefineFeeRange",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "CopyslFbox0a74799b0b0034a"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxSegDefineFeeRange.setDefaultUnit(kony.flex.DP);
        var flxMinimumAmountDollar = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxMinimumAmountDollar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0h3f0e7512b874a",
            "top": "20dp",
            "width": "40dp",
            "zIndex": 1
        }, {}, {});
        flxMinimumAmountDollar.setDefaultUnit(kony.flex.DP);
        var lblMinimumAmountDollar = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblMinimumAmountDollar",
            "isVisible": true,
            "left": 0,
            "skin": "CopyslLabel0fc0562b7640a4c",
            "text": "$",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMinimumAmountDollar.add(lblMinimumAmountDollar);
        var tbxMinimumAmount = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "40dp",
            "id": "tbxMinimumAmount",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "70dp",
            "secureTextEntry": false,
            "skin": "CopyslTextBox0b4bc21adeecd47",
            "text": "0.00",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "width": "150dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        var flxMaximumAmountDollar = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxMaximumAmountDollar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "300dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0h3f0e7512b874a",
            "top": "20dp",
            "width": "40dp",
            "zIndex": 1
        }, {}, {});
        flxMaximumAmountDollar.setDefaultUnit(kony.flex.DP);
        var lblMaximumAmountDollar = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblMaximumAmountDollar",
            "isVisible": true,
            "left": 0,
            "skin": "CopyslLabel0fc0562b7640a4c",
            "text": "$",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMaximumAmountDollar.add(lblMaximumAmountDollar);
        var tbxMaximumAmount = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "40dp",
            "id": "tbxMaximumAmount",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "340dp",
            "secureTextEntry": false,
            "skin": "CopyslTextBox0b4bc21adeecd47",
            "text": "0.00",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "width": "150dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        var flxFeeDollar = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxFeeDollar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "570dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0h3f0e7512b874a",
            "top": "20dp",
            "width": "40dp",
            "zIndex": 1
        }, {}, {});
        flxFeeDollar.setDefaultUnit(kony.flex.DP);
        var lblFeeDollar = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblFeeDollar",
            "isVisible": true,
            "left": 0,
            "skin": "CopyslLabel0fc0562b7640a4c",
            "text": "$",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeeDollar.add(lblFeeDollar);
        var tbxFee = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "40dp",
            "id": "tbxFee",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "610dp",
            "secureTextEntry": false,
            "skin": "CopyslTextBox0b4bc21adeecd47",
            "text": "0.00",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "width": "150dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        flxSegDefineFeeRange.add(flxMinimumAmountDollar, tbxMinimumAmount, flxMaximumAmountDollar, tbxMaximumAmount, flxFeeDollar, tbxFee);
        return flxSegDefineFeeRange;
    }
})