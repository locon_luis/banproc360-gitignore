define("flxCustMangActivityHistory", function() {
    return function(controller) {
        var flxCustMangActivityHistory = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustMangActivityHistory",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "skin": "slFbox"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustMangActivityHistory.setDefaultUnit(kony.flex.DP);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "35px",
            "skin": "sknlblSeperator",
            "text": ".",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxCloumns = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "70px",
            "id": "flxCloumns",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {});
        flxCloumns.setDefaultUnit(kony.flex.DP);
        var flxSession = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "69px",
            "id": "flxSession",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFboxBGF9F9F9",
            "top": "1px",
            "width": "20%"
        }, {}, {});
        flxSession.setDefaultUnit(kony.flex.DP);
        var flxFirstRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50%",
            "id": "flxFirstRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxFirstRow.setDefaultUnit(kony.flex.DP);
        var lblSessionStart = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblSessionStart",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "10/21/2016 - 6:46 am",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFirstRow.add(lblSessionStart);
        var flxSecondRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50%",
            "id": "flxSecondRow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "150px"
        }, {}, {});
        flxSecondRow.setDefaultUnit(kony.flex.DP);
        var lblDuration = new kony.ui.Label({
            "bottom": "18px",
            "centerY": "50%",
            "id": "lblDuration",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f212px",
            "text": "20 Mins.",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblHorizontalSeperator = new kony.ui.Label({
            "centerY": "50%",
            "height": "13px",
            "id": "lblHorizontalSeperator",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblSeperator2",
            "text": ".",
            "width": "1px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblNumberOfActivities = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblNumberOfActivities",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f212px",
            "text": "10 Activities",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSecondRow.add(lblDuration, lblHorizontalSeperator, lblNumberOfActivities);
        flxSession.add(flxFirstRow, flxSecondRow);
        var lblDevice = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDevice",
            "isVisible": true,
            "left": "8%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Desktop / Laptop",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblChannel = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblChannel",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "text": "Web",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOS = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOS",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "text": "Windows 10",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIpaddress = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIpaddress",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Chrome 36.01",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCloumns.add(flxSession, lblDevice, lblChannel, lblOS, lblIpaddress);
        flxCustMangActivityHistory.add(lblSeperator, flxCloumns);
        return flxCustMangActivityHistory;
    }
})