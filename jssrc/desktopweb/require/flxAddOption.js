define("flxAddOption", function() {
    return function(controller) {
        var flxAddOption = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "95dp",
            "id": "flxAddOption",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {});
        flxAddOption.setDefaultUnit(kony.flex.DP);
        var flxAdd = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "85px",
            "id": "flxAdd",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": 20,
            "skin": "sknflxffffffOp100Border5e90cbRadius3Px",
            "top": "10dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "blocksHover"
        });
        flxAdd.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "id": "lblName",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "IBFT",
            "top": "15px",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnAdd = new kony.ui.Button({
            "height": "20px",
            "id": "btnAdd",
            "isVisible": true,
            "onClick": controller.AS_Button_d5a06f2ade8047b1894bb6670b26a421,
            "right": 20,
            "skin": "sknBtnLatoRegular11abeb12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknBtnLatoRegular1682b212px"
        });
        var rtxDescription = new kony.ui.RichText({
            "height": "47px",
            "id": "rtxDescription",
            "isVisible": true,
            "left": "15px",
            "right": 40,
            "skin": "sknrtxLato485c7514px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do incididunt ut labore et dolore magna aliqua.",
            "top": "37px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAdd.add(lblName, btnAdd, rtxDescription);
        flxAddOption.add(flxAdd);
        return flxAddOption;
    }
})