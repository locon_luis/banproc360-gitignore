define("userflxCustomerMangNotesController", {
    //Type your controller code here 
    toggleCheckboxSelected: function() {
        var index = kony.application.getCurrentForm().tableView.segServicesAndFaq.selectedIndex;
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().tableView.segServicesAndFaq.data;
        if (data[rowIndex].imgCheckBox.src === "checkbox.png") data[rowIndex].imgCheckBox.src = "checkboxselected.png";
        else data[rowIndex].imgCheckBox.src = "checkbox.png";
        kony.application.getCurrentForm().tableView.segServicesAndFaq.setDataAt(data[rowIndex], rowIndex);
    },
    showSelectedRow: function() {
        var index = kony.application.getCurrentForm().tableView.segServicesAndFaq.selectedIndex;
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().tableView.segServicesAndFaq.data;
        kony.print("index:" + index);
        data[rowIndex].ImgArrow = "img_desc_arrow.png";
        data[rowIndex].template = "flxServicesAndFaq";
        kony.application.getCurrentForm().tableView.segServicesAndFaq.setData(data);
    }
});
define("flxCustomerMangNotesControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("flxCustomerMangNotesController", ["userflxCustomerMangNotesController", "flxCustomerMangNotesControllerActions"], function() {
    var controller = require("userflxCustomerMangNotesController");
    var controllerActions = ["flxCustomerMangNotesControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
