define("flxCSRMsgList", function() {
    return function(controller) {
        var flxCSRMsgList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCSRMsgList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknbackGroundffffff100",
            "top": "0",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCSRMsgList.setDefaultUnit(kony.flex.DP);
        var flxSegMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknCursor",
            "top": "0dp",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxSegMain.setDefaultUnit(kony.flex.DP);
        var flxMsgSegMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMsgSegMain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxMsgSegMain.setDefaultUnit(kony.flex.DP);
        var flxReqId = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "18px",
            "clipBounds": true,
            "id": "flxReqId",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "18px",
            "width": "15%"
        }, {}, {});
        flxReqId.setDefaultUnit(kony.flex.DP);
        var lblReply = new kony.ui.Label({
            "id": "lblReply",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRequestID = new kony.ui.Label({
            "id": "lblRequestID",
            "isVisible": true,
            "left": "2px",
            "skin": "sknlblLatoBold35475f14px",
            "text": " RI333456",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxReqId.add(lblReply, lblRequestID);
        var flxUserName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxUserName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "1%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a3ab7b9f4fbb49bfb050ba644d5f88b4,
            "skin": "slFbox",
            "top": "18px",
            "width": "16%",
            "zIndex": 2
        }, {}, {});
        flxUserName.setDefaultUnit(kony.flex.DP);
        var lblCustomerName = new kony.ui.Label({
            "id": "lblCustomerName",
            "isVisible": true,
            "left": "2px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "John Doe(9), ",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDraft = new kony.ui.Label({
            "id": "lblDraft",
            "isVisible": true,
            "left": "2px",
            "skin": "sknlblLatoRegularee6565",
            "text": "Draft",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUserName.add(lblCustomerName, lblDraft);
        var lblSubject = new kony.ui.Label({
            "bottom": "18px",
            "id": "lblSubject",
            "isVisible": true,
            "left": "1%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Lorem ipsum dolor sitconsectetu.. (9).orem ipsum dolor sitconsectetu.. (orem ipsum dolor sitconsectetu.. (9).",
            "top": "18px",
            "width": "23%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCategory = new kony.ui.Label({
            "id": "lblCategory",
            "isVisible": true,
            "left": "1%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "General banking",
            "top": "18px",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDate = new kony.ui.Label({
            "id": "lblDate",
            "isVisible": true,
            "left": "1%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "12.10.2017",
            "top": "18px",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAssignto = new kony.ui.Label({
            "id": "lblAssignto",
            "isVisible": true,
            "left": "1%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Edward Shwartz",
            "top": "18px",
            "width": "11%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "24dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_af3d0880b0214a8ea28743df4bd54088,
            "skin": "slFbox",
            "top": "18px",
            "width": "24px",
            "zIndex": 200
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconOptions = new kony.ui.Label({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconOptions",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconOptions);
        flxMsgSegMain.add(flxReqId, flxUserName, lblSubject, lblCategory, lblDate, lblAssignto, flxOptions);
        flxSegMain.add(flxMsgSegMain);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblSeperator",
            "text": "'",
            "width": "100%",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCSRMsgList.add(flxSegMain, lblSeparator);
        return flxCSRMsgList;
    }
})