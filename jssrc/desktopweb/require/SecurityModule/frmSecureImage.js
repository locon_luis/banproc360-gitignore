define("SecurityModule/frmSecureImage", function() {
    return function(controller) {
        function addWidgetsfrmSecureImage() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMainKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMainKA",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0bf008b77431a48",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainKA.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0d5aa5d4c1cb048",
                "top": "0%",
                "width": "305dp",
                "zIndex": 1
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox0c19f24e28e164c",
                "zIndex": 1
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0c19f24e28e164c",
                "top": "0%",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.SecureImage\")"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxUploadImageKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "340dp",
                "id": "flxUploadImageKA",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": 35,
                "skin": "sknflxUploadImage0e65d5a93fe0f45",
                "top": "115dp",
                "zIndex": 1
            }, {}, {});
            flxUploadImageKA.setDefaultUnit(kony.flex.DP);
            var imgUploadImage = new kony.ui.Image2({
                "height": "100%",
                "id": "imgUploadImage",
                "isVisible": false,
                "left": "0dp",
                "skin": "d",
                "src": "uploadimagebg.png",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxBrowseOrDrag = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBrowseOrDrag",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0c19f24e28e164c",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxBrowseOrDrag.setDefaultUnit(kony.flex.DP);
            var lblDragDropImage = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblDragDropImage",
                "isVisible": true,
                "skin": "sknlblLatoLight0a41fe520e1334c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.DragDropImage\")",
                "top": "74px",
                "width": "320dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblORKA = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblORKA",
                "isVisible": true,
                "skin": "sknlblLatobold0j8d5e0139ddf42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.OR\")",
                "top": "120dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnBrowseKA = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknBtnBrowseFcs0i83e079e708746",
                "height": "42px",
                "id": "btnBrowseKA",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknBtnBrowseNorm0f0cb6612256c4d",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.BROWSE\")",
                "top": "152px",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnBrowseFcs"
            });
            var lblFileFormaKA = new kony.ui.Label({
                "centerX": "50%",
                "height": "15px",
                "id": "lblFileFormaKA",
                "isVisible": true,
                "skin": "sknlbllatoRegular0f715a58455b840",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.FileFormat\")",
                "top": "233px",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSizeLimitKA = new kony.ui.Label({
                "centerX": "50%",
                "height": "15px",
                "id": "lblSizeLimitKA",
                "isVisible": true,
                "skin": "sknlbllatoRegular0f715a58455b840",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.SizeLimit\")",
                "top": "250px",
                "width": "160dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBrowseOrDrag.add(lblDragDropImage, lblORKA, btnBrowseKA, lblFileFormaKA, lblSizeLimitKA);
            var flxUploadingIndiacatorKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUploadingIndiacatorKA",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0c19f24e28e164c",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxUploadingIndiacatorKA.setDefaultUnit(kony.flex.DP);
            var lblUploading = new kony.ui.Label({
                "centerX": "50%",
                "height": "15px",
                "id": "lblUploading",
                "isVisible": true,
                "skin": "sknlbllatoRegular0bc8f2143d6204e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Uploading\")",
                "top": "118px",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUploadingStatusBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxUploadingStatusBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflx0c0989fa7ba8d45",
                "top": "145px",
                "width": "302px",
                "zIndex": 1
            }, {}, {});
            flxUploadingStatusBar.setDefaultUnit(kony.flex.DP);
            flxUploadingStatusBar.add();
            var btnCancelKA = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknCancelFcs0jaa1f1c04f884a",
                "height": "44px",
                "id": "btnCancelKA",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknBtnCancelNorm0h58a67c690e642",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.CANCEL\")",
                "top": "195px",
                "width": "110px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknCancelFcs"
            });
            flxUploadingIndiacatorKA.add(lblUploading, flxUploadingStatusBar, btnCancelKA);
            flxUploadImageKA.add(imgUploadImage, flxBrowseOrDrag, flxUploadingIndiacatorKA);
            var flxShowAddedImagesStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxShowAddedImagesStatus",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknMainContentTrans0j937d745c2a749",
                "top": "106px",
                "zIndex": 1
            }, {}, {});
            flxShowAddedImagesStatus.setDefaultUnit(kony.flex.DP);
            var flxUploadImage2KA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxUploadImage2KA",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "sknflxUploadImage0e65d5a93fe0f45",
                "top": "60dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxUploadImage2KA.setDefaultUnit(kony.flex.DP);
            var flxBrowseOrDrag2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBrowseOrDrag2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0c19f24e28e164c",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxBrowseOrDrag2.setDefaultUnit(kony.flex.DP);
            var lblDragDropImage2 = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblDragDropImage2",
                "isVisible": true,
                "skin": "sknlblLatoLight0a41fe520e1334c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.DragDropImage\")",
                "top": "140px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblORKA2 = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblORKA2",
                "isVisible": true,
                "skin": "sknlblLatobold0j8d5e0139ddf42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.OR\")",
                "top": "187dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnBrowseKA2 = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknBtnBrowseFcs0i83e079e708746",
                "height": "42px",
                "id": "btnBrowseKA2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknBtnBrowseNorm0f0cb6612256c4d",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.BROWSE\")",
                "top": "225px",
                "width": "115px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnBrowseFcs"
            });
            var lblFileForma2KA = new kony.ui.Label({
                "centerX": "50%",
                "height": "15px",
                "id": "lblFileForma2KA",
                "isVisible": true,
                "skin": "sknlbllatoRegular0e6a8af5fad5a44",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.FileFormat\")",
                "top": "306px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSizeLimit2KA = new kony.ui.Label({
                "centerX": "50%",
                "height": "15px",
                "id": "lblSizeLimit2KA",
                "isVisible": true,
                "skin": "sknlbllatoRegular0f715a58455b840",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.SizeLimit\")",
                "top": "324px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBrowseOrDrag2.add(lblDragDropImage2, lblORKA2, btnBrowseKA2, lblFileForma2KA, lblSizeLimit2KA);
            var flxUploadingIndiacator2KA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUploadingIndiacator2KA",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0c19f24e28e164c",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxUploadingIndiacator2KA.setDefaultUnit(kony.flex.DP);
            var lblUploading2 = new kony.ui.Label({
                "centerX": "50%",
                "height": "15px",
                "id": "lblUploading2",
                "isVisible": true,
                "skin": "sknlbllatoRegular0bc8f2143d6204e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Uploading\")",
                "top": "190px",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUploadingStatusBar2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxUploadingStatusBar2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflx0c0989fa7ba8d45",
                "top": "215px",
                "width": "250px",
                "zIndex": 1
            }, {}, {});
            flxUploadingStatusBar2.setDefaultUnit(kony.flex.DP);
            flxUploadingStatusBar2.add();
            var btnCancel2KA = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknCancelFcs0jaa1f1c04f884a",
                "height": "44px",
                "id": "btnCancel2KA",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknBtnCancelNorm0h58a67c690e642",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.CANCEL\")",
                "top": "260px",
                "width": "110px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknCancelFcs"
            });
            flxUploadingIndiacator2KA.add(lblUploading2, flxUploadingStatusBar2, btnCancel2KA);
            var imgUploadImage2 = new kony.ui.Image2({
                "height": "100%",
                "id": "imgUploadImage2",
                "isVisible": false,
                "left": "0dp",
                "skin": "d",
                "src": "uploadimage2.png",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUploadImage2KA.add(flxBrowseOrDrag2, flxUploadingIndiacator2KA, imgUploadImage2);
            var flxuploadedImageList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxuploadedImageList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "400px",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxImageListWhiteBorder0i0515e215b1449",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxuploadedImageList.setDefaultUnit(kony.flex.DP);
            var flxScrollImageUploadList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxScrollImageUploadList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox0b626bfbafced4f",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScrollImageUploadList.setDefaultUnit(kony.flex.DP);
            var segUploadedImageListKA = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "data": [
                    [{
                            "fontIconFilterStatus": "",
                            "imgSortName": "sorting3x.png",
                            "imgSortStatus": "sorting3x.png",
                            "lblHeaderSeperator": ".",
                            "lblName": "IMAGE",
                            "lblStatus": "IMAGE STATUS"
                        },
                        [{
                            "imgUpload": "imgplaceholder.png",
                            "lblIconStatus": "Label",
                            "lblImageStatus": "Active",
                            "lblOptions": "Label",
                            "lblSeperator": "."
                        }, {
                            "imgUpload": "imgplaceholder.png",
                            "lblIconStatus": "Label",
                            "lblImageStatus": "Active",
                            "lblOptions": "Label",
                            "lblSeperator": "."
                        }, {
                            "imgUpload": "imgplaceholder.png",
                            "lblIconStatus": "Label",
                            "lblImageStatus": "Active",
                            "lblOptions": "Label",
                            "lblSeperator": "."
                        }, {
                            "imgUpload": "imgplaceholder.png",
                            "lblIconStatus": "Label",
                            "lblImageStatus": "Active",
                            "lblOptions": "Label",
                            "lblSeperator": "."
                        }, {
                            "imgUpload": "imgplaceholder.png",
                            "lblIconStatus": "Label",
                            "lblImageStatus": "Active",
                            "lblOptions": "Label",
                            "lblSeperator": "."
                        }, {
                            "imgUpload": "imgplaceholder.png",
                            "lblIconStatus": "Label",
                            "lblImageStatus": "Active",
                            "lblOptions": "Label",
                            "lblSeperator": "."
                        }, {
                            "imgUpload": "imgplaceholder.png",
                            "lblIconStatus": "Label",
                            "lblImageStatus": "Active",
                            "lblOptions": "Label",
                            "lblSeperator": "."
                        }, {
                            "imgUpload": "imgplaceholder.png",
                            "lblIconStatus": "Label",
                            "lblImageStatus": "Active",
                            "lblOptions": "Label",
                            "lblSeperator": "."
                        }, {
                            "imgUpload": "imgplaceholder.png",
                            "lblIconStatus": "Label",
                            "lblImageStatus": "Active",
                            "lblOptions": "Label",
                            "lblSeperator": "."
                        }, {
                            "imgUpload": "",
                            "lblIconStatus": "Label",
                            "lblImageStatus": " ",
                            "lblOptions": "Label",
                            "lblSeperator": " "
                        }]
                    ]
                ],
                "groupCells": false,
                "id": "segUploadedImageListKA",
                "isVisible": true,
                "left": "20px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "10px",
                "rowTemplate": "segUploadedImageListKA",
                "sectionHeaderTemplate": "flxImageUploadListHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "64646464",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": true,
                "top": "0%",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxHeaderStatus": "flxHeaderStatus",
                    "flxImageUploadListHeader": "flxImageUploadListHeader",
                    "flxName": "flxName",
                    "flxOptions": "flxOptions",
                    "flxStatus": "flxStatus",
                    "fontIconFilterStatus": "fontIconFilterStatus",
                    "imgSortName": "imgSortName",
                    "imgSortStatus": "imgSortStatus",
                    "imgUpload": "imgUpload",
                    "lblHeaderSeperator": "lblHeaderSeperator",
                    "lblIconStatus": "lblIconStatus",
                    "lblImageStatus": "lblImageStatus",
                    "lblName": "lblName",
                    "lblOptions": "lblOptions",
                    "lblSeperator": "lblSeperator",
                    "lblStatus": "lblStatus",
                    "segUploadedImageListKA": "segUploadedImageListKA"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStatusFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "105px",
                "skin": "slFbox",
                "top": "40dp",
                "width": "150px",
                "zIndex": 15
            }, {}, {});
            flxStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 15,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "zIndex": 15
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilter.add(statusFilterMenu);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "70px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "145px",
                "width": "200px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var flxSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeperator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeperator.setDefaultUnit(kony.flex.DP);
            flxSeperator.add();
            var flxOption1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxOption1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f9afe4921bd742",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption1.setDefaultUnit(kony.flex.DP);
            var imgOption1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption1",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption1.add(imgOption1, lblOption1);
            var flxOption2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxOption2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ie7c78af12d244",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption2.setDefaultUnit(kony.flex.DP);
            var lblIconOption2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblIconOption2",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDeactive\")",
                "top": "12dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Deactivate\")",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption2.add(lblIconOption2, lblOption2);
            var flxOption3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxOption3",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ca5b7d20dbb749",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption3.setDefaultUnit(kony.flex.DP);
            var imgOption3 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption3",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption3",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Suspend\")",
                "top": "4dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption3.add(imgOption3, lblOption3);
            var flxOption4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxOption4",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0dec27c942d5140",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption4.setDefaultUnit(kony.flex.DP);
            var lblIconOption4 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblIconOption4",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "top": "12dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption4 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption4",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption4.add(lblIconOption4, lblOption4);
            flxSelectOptions.add(flxSeperator, flxOption1, flxOption2, flxOption3, flxOption4);
            var rtxNoResultsFound = new kony.ui.RichText({
                "bottom": "150px",
                "centerX": "50%",
                "id": "rtxNoResultsFound",
                "isVisible": false,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "150px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxScrollImageUploadList.add(segUploadedImageListKA, flxStatusFilter, flxSelectOptions, rtxNoResultsFound);
            flxuploadedImageList.add(flxScrollImageUploadList);
            var flxPhraseStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxPhraseStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox0h43cfab67a134d",
                "top": "9dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxPhraseStatus.setDefaultUnit(kony.flex.DP);
            var lblPhraseStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPhraseStatus",
                "isVisible": true,
                "right": "50px",
                "skin": "sknlblLato0c4e52819ae0b47",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.PHRASESTATUS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var SwitchToggleStatus = new kony.ui.Switch({
                "centerY": "50%",
                "height": "25px",
                "id": "SwitchToggleStatus",
                "isVisible": true,
                "leftSideText": " ",
                "right": "0px",
                "rightSideText": " ",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "width": "38px",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPhraseStatus.add(lblPhraseStatus, SwitchToggleStatus);
            flxShowAddedImagesStatus.add(flxUploadImage2KA, flxuploadedImageList, flxPhraseStatus);
            flxRightPannel.add(flxMainHeader, flxHeaderDropdown, flxUploadImageKA, flxShowAddedImagesStatus);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "60px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "60px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxMainKA.add(flxLeftPannel, flxRightPannel, flxLoading);
            var flxPopUpCancelImageUpload = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpCancelImageUpload",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0db4ac791cac04d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpCancelImageUpload.setDefaultUnit(kony.flex.DP);
            var flxPoupDeactivePermission = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxPoupDeactivePermission",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0e35d4d07b2054b",
                "top": "250px",
                "width": "600px",
                "zIndex": 1
            }, {}, {});
            flxPoupDeactivePermission.setDefaultUnit(kony.flex.DP);
            var flxTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxebb0ed11ac582a494f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopColor.setDefaultUnit(kony.flex.DP);
            flxTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "182px",
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0cd4d17dca3e64b",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var lblDeactivePermission = new kony.ui.Label({
                "id": "lblDeactivePermission",
                "isVisible": true,
                "left": "3%",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.CancelImageUpload\")",
                "top": "35px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var RichTextDisclaimer = new kony.ui.RichText({
                "id": "RichTextDisclaimer",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmSecureImage.RichTextDisclaimer\")",
                "top": "75px",
                "width": "560px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPopUpCancelClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpCancelClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxPopUpCancelClose.setDefaultUnit(kony.flex.DP);
            var fontIconImgCancel = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCancel",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpCancelClose.add(fontIconImgCancel);
            flxPopupHeader.add(lblDeactivePermission, RichTextDisclaimer, flxPopUpCancelClose);
            var flxDeactivateButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxDeactivateButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0gf3cede0eb1c4f",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDeactivateButtons.setDefaultUnit(kony.flex.DP);
            var btnLeaveAsItIs = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "btnLeaveAsIsFcs0ad59d12ae03342",
                "height": "60%",
                "id": "btnLeaveAsItIs",
                "isVisible": true,
                "right": "170px",
                "skin": "btnLeaveAsIsNorm0gb9d0e67deae48",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                "top": "0%",
                "width": "140px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "btnLeaveAsIsFcs"
            });
            var btnDeactivate = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "btnYesCancelFcs0b913ba61217a49",
                "height": "60%",
                "id": "btnDeactivate",
                "isVisible": true,
                "right": "15px",
                "skin": "btnYesCancelNorm0h1aa5218b4cd42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.YesCancel\")",
                "top": "0%",
                "width": "140px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "btnYesCancelFcs"
            });
            flxDeactivateButtons.add(btnLeaveAsItIs, btnDeactivate);
            flxPoupDeactivePermission.add(flxTopColor, flxPopupHeader, flxDeactivateButtons);
            flxPopUpCancelImageUpload.add(flxPoupDeactivePermission);
            var flxPopUpDeleteImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpDeleteImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0db4ac791cac04d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpDeleteImage.setDefaultUnit(kony.flex.DP);
            var flxPoupDelete = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxPoupDelete",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0e35d4d07b2054b",
                "top": "250px",
                "width": "600px",
                "zIndex": 1
            }, {}, {});
            flxPoupDelete.setDefaultUnit(kony.flex.DP);
            var flxTopColor1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopColor1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxebb0ed11ac582a494f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopColor1.setDefaultUnit(kony.flex.DP);
            flxTopColor1.add();
            var flxPopupHeader1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "182px",
                "id": "flxPopupHeader1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0cd4d17dca3e64b",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader1.setDefaultUnit(kony.flex.DP);
            var lblDeleteImage = new kony.ui.Label({
                "id": "lblDeleteImage",
                "isVisible": true,
                "left": "3%",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmSecureImageController.Delete_Image\")",
                "top": "35px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var RichTextDisclaimer1 = new kony.ui.RichText({
                "id": "RichTextDisclaimer1",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmSecureImage.RichTextDisclaimer1\")",
                "top": "75px",
                "width": "560px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(fontIconImgCLose);
            flxPopupHeader1.add(lblDeleteImage, RichTextDisclaimer1, flxPopUpClose);
            var flxDeactivateButtons1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxDeactivateButtons1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0gf3cede0eb1c4f",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDeactivateButtons1.setDefaultUnit(kony.flex.DP);
            var btnLeaveAsItIs1 = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "btnLeaveAsIsFcs0ad59d12ae03342",
                "height": "60%",
                "id": "btnLeaveAsItIs1",
                "isVisible": true,
                "right": "170px",
                "skin": "btnLeaveAsIsNorm0gb9d0e67deae48",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                "top": "0%",
                "width": "140px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "btnLeaveAsIsFcs"
            });
            var btnYesDelete = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "btnYesCancelFcs0b913ba61217a49",
                "height": "60%",
                "id": "btnYesDelete",
                "isVisible": true,
                "right": "15px",
                "skin": "btnYesCancelNorm0h1aa5218b4cd42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDelete\")",
                "top": "0%",
                "width": "140px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "btnYesCancelFcs"
            });
            flxDeactivateButtons1.add(btnLeaveAsItIs1, btnYesDelete);
            flxPoupDelete.add(flxTopColor1, flxPopupHeader1, flxDeactivateButtons1);
            flxPopUpDeleteImage.add(flxPoupDelete);
            var flxPopUpWarning = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpWarning",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0db4ac791cac04d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpWarning.setDefaultUnit(kony.flex.DP);
            var flxPoupDeactiveImage2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxPoupDeactiveImage2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0e35d4d07b2054b",
                "top": "250px",
                "width": "600px",
                "zIndex": 1
            }, {}, {});
            flxPoupDeactiveImage2.setDefaultUnit(kony.flex.DP);
            var flxTopColor2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopColor2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxee6565Op100NoBorder",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopColor2.setDefaultUnit(kony.flex.DP);
            flxTopColor2.add();
            var flxPopupHeader2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "182px",
                "id": "flxPopupHeader2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0cd4d17dca3e64b",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader2.setDefaultUnit(kony.flex.DP);
            var lblCanntDelete = new kony.ui.Label({
                "id": "lblCanntDelete",
                "isVisible": true,
                "left": "3%",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmSecureImage.lblCanntDelete\")",
                "top": "35px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var RichTextDisclaimer2 = new kony.ui.RichText({
                "id": "RichTextDisclaimer2",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmSecureImage.RichTextDisclaimer2\")",
                "top": "75px",
                "width": "560px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPopUpWarningClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpWarningClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxPopUpWarningClose.setDefaultUnit(kony.flex.DP);
            var fontIconImgWarning = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgWarning",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpWarningClose.add(fontIconImgWarning);
            flxPopupHeader2.add(lblCanntDelete, RichTextDisclaimer2, flxPopUpWarningClose);
            var flxDeactivateButtons2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxDeactivateButtons2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0gf3cede0eb1c4f",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDeactivateButtons2.setDefaultUnit(kony.flex.DP);
            var btnLeaveAsIs2 = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "btnLeaveAsIsFcs0ad59d12ae03342",
                "height": "60%",
                "id": "btnLeaveAsIs2",
                "isVisible": false,
                "right": "170px",
                "skin": "btnLeaveAsIsNorm0gb9d0e67deae48",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Noleaveasis\")",
                "top": "0%",
                "width": "140px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "btnLeaveAsIsFcs"
            });
            var btnOk = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLato12Pxee6565Radius25Px",
                "height": "40px",
                "id": "btnOk",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.OK\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxDeactivateButtons2.add(btnLeaveAsIs2, btnOk);
            flxPoupDeactiveImage2.add(flxTopColor2, flxPopupHeader2, flxDeactivateButtons2);
            flxPopUpWarning.add(flxPoupDeactiveImage2);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "lbltoastMessage": {
                        "text": "Deleted security image successfully."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            this.add(flxMainKA, flxPopUpCancelImageUpload, flxPopUpDeleteImage, flxPopUpWarning, flxToastMessage);
        };
        return [{
            "addWidgets": addWidgetsfrmSecureImage,
            "enabledForIdleTimeout": true,
            "id": "frmSecureImage",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_b94cffe1734f45a1bf2ae051c534c7e1(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_b9951920b45f41b39ad72cc3d6bbf166,
            "retainScrollPosition": false
        }]
    }
});