define("flxCompanies", function() {
    return function(controller) {
        var flxCompanies = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanies",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCompanies.setDefaultUnit(kony.flex.DP);
        var lblCompanyId = new kony.ui.Label({
            "bottom": "18px",
            "id": "lblCompanyId",
            "isVisible": true,
            "left": "55dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "TRF63736",
            "top": "18px",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCompanyName = new kony.ui.Label({
            "bottom": "18px",
            "id": "lblCompanyName",
            "isVisible": true,
            "left": "23%",
            "right": "78%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "View Permissions",
            "top": "18px",
            "width": "19%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblType = new kony.ui.Label({
            "id": "lblType",
            "isVisible": true,
            "left": "44%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "20",
            "top": "18px",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTIN = new kony.ui.Label({
            "bottom": "18px",
            "id": "lblTIN",
            "isVisible": true,
            "left": "59%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "TRF63736",
            "top": "18px",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEmail = new kony.ui.Label({
            "id": "lblEmail",
            "isVisible": true,
            "left": "74%",
            "right": "35dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "25",
            "top": "18px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "35dp",
            "right": "35dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCompanies.add(lblCompanyId, lblCompanyName, lblType, lblTIN, lblEmail, lblSeperator);
        return flxCompanies;
    }
})