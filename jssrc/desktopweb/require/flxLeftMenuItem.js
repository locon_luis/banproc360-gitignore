define("flxLeftMenuItem", function() {
    return function(controller) {
        var flxLeftMenuItem = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLeftMenuItem",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxLeftMenuItem.setDefaultUnit(kony.flex.DP);
        var lblMenuItem = new kony.ui.Label({
            "id": "lblMenuItem",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLeftMenuItemLabel",
            "text": "Main Item",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLeftMenuItemLabelHover"
        });
        var imgSelectionArrow = new kony.ui.Image2({
            "height": "10dp",
            "id": "imgSelectionArrow",
            "isVisible": false,
            "right": "30dp",
            "skin": "slImage",
            "src": "leftmenu_arrow.png",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconimgSelectionArrow = new kony.ui.Label({
            "height": "10dp",
            "id": "fontIconimgSelectionArrow",
            "isVisible": true,
            "right": "30px",
            "skin": "sknIcon11pxWhite",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeftMenuItem.add(lblMenuItem, imgSelectionArrow, fontIconimgSelectionArrow);
        return flxLeftMenuItem;
    }
})