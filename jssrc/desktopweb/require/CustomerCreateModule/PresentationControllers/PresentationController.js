define(['ErrorInterceptor'], function(ErrorInterceptor) {
    /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
    function PresentationController() {
        this.customerCreateModel = {
            companyDetails: null,
            accounts: null,
            roles: null,
            rolePermission: null,
            isUsernameAvailable: null,
            OFACVerification: null,
            editInputs: null,
            customerLimits: null,
            context: null,
            customerAccounts: null,
            rolePermissionEdit: null,
        };
        kony.mvc.Presentation.BasePresenter.call(this);
    }
    inheritsFrom(PresentationController, kony.mvc.Presentation.BasePresenter);
    /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
    PresentationController.prototype.initializePresentationController = function() {};
    /*
     * dispaly create screen when navigate
     *@param: context
     */
    PresentationController.prototype.showCreatecompanyScreen = function(context) {
        var self = this;
        self.clearToDefaults();
        var type = context.type;
        var comapnyId = context.companyID;
        self.customerCreateModel.companyDetails = context;
        self.presentCreateScreen(self.customerCreateModel);
        self.getRoles({
            "typeId": type
        });
        self.getAccounts({
            "Organization_id": comapnyId
        });
        self.getUsernameRulesAndPolicy();
    };
    /*
     * common function for presentUserInterface action
     * @param: viewmodel of context
     */
    PresentationController.prototype.presentCreateScreen = function(viewModel) {
        var self = this;
        self.presentUserInterface('frmCustomerCreate', viewModel);
    };
    /*
     * hide create customer form and navigate to company form
     */
    PresentationController.prototype.hideCreatecompanyScreen = function(context, isLoading, toast) {
        var self = this;
        self.showLoadingScreen();
        self.navigateTo('CompaniesModule', 'showResultFromCustomerScreen', [context, isLoading, toast]);
    };
    /*
     * navigate back to customer profile screen
     * @param: customer id
     */
    PresentationController.prototype.navigateToCustomerPersonal = function(param, formDetails) {
        var self = this;
        var context = [param.Customer_id, formDetails];
        this.navigateTo('CustomerManagementModule', 'diplayCustomerProfileFromOtherModule', context);
    };
    /*
     * display loading screen
     */
    PresentationController.prototype.showLoadingScreen = function() {
        var self = this;
        self.presentCreateScreen({
            "LoadingScreen": {
                "focus": true
            }
        });
    };
    /*
     * hide loading screen
     */
    PresentationController.prototype.hideLoadingScreen = function() {
        var self = this;
        self.presentCreateScreen({
            "LoadingScreen": {
                "focus": false
            }
        });
    };
    /*
     * display  toast message
     * @param: status,toast message
     */
    PresentationController.prototype.showToastMessage = function(message, status) {
        var self = this;
        self.presentCreateScreen({
            "toastMessage": {
                "status": status,
                "message": message
            }
        });
    };
    /*
     * @name getCompanyAccounts
     * @member CustomerCreateModule.presentationController 
     */
    PresentationController.prototype.getAccounts = function(context) {
        var self = this;

        function completionFetchAccountsCallback(response) {
            self.clearToDefaults();
            self.customerCreateModel.accounts = response.OgranizationAccounts;
            self.hideLoadingScreen();
            self.presentCreateScreen(self.customerCreateModel);
        }

        function onError(error) {
            self.hideLoadingScreen();
            self.showToastMessage(ErrorInterceptor.errorMessage(error), kony.i18n.getLocalizedString("i18n.frmGroupsController.error"));
        }
        self.showLoadingScreen();
        self.businessController.getCompanyAccounts(context, completionFetchAccountsCallback, onError);
    };
    /*
     * @name getRoles
     * @member CustomerCreateModule.presentationController
     */
    PresentationController.prototype.getRoles = function(context) {
        var self = this;
        self.showLoadingScreen();

        function completionFetchRolesCallback(response) {
            self.clearToDefaults();
            self.customerCreateModel.roles = response;
            self.hideLoadingScreen();
            self.presentCreateScreen(self.customerCreateModel);
        }

        function onError(error) {
            self.hideLoadingScreen();
            self.showToastMessage(ErrorInterceptor.errorMessage(error), kony.i18n.getLocalizedString("i18n.frmGroupsController.error"));
        }
        this.businessController.getRoles(context, completionFetchRolesCallback, onError);
    };
    /*
     * @name getPermissionsOfRole - fetch permissions for a particular role selected
     * @member CustomerCreateModule.presentationController
     * @param: role id
     */
    PresentationController.prototype.getPermissionsOfRole = function(context, editData) {
        var self = this;
        self.showLoadingScreen();

        function completionFetchGroupEntitlementsCallback(response) {
            self.customerCreateModel.rolePermission = [];
            self.clearToDefaults();
            if (editData) {
                self.customerCreateModel.customerLimits = editData;
                self.customerCreateModel.rolePermissionEdit = response;
            } else {
                self.customerCreateModel.rolePermission = [];
                self.customerCreateModel.rolePermission = response;
            }
            self.presentCreateScreen(self.customerCreateModel);
            self.hideLoadingScreen();
        }

        function onError(error) {
            self.hideLoadingScreen();
            self.showToastMessage(ErrorInterceptor.errorMessage(error), kony.i18n.getLocalizedString("i18n.frmGroupsController.error"));
        }
        this.businessController.getGroupEntitlements(context, completionFetchGroupEntitlementsCallback, onError);
    };
    /*
     * @name createCustomer
     * @member CustomerCreateModule.presentationController
     */
    PresentationController.prototype.createCustomer = function(context, orgId) {
        var self = this;
        self.showLoadingScreen();
        var inputParam = {
            "action": "createCustomer",
            "id": orgId
        };

        function completionCreateCustomer(response) {
            self.hideCreatecompanyScreen(inputParam, true, {
                "toastMessage": {
                    "status": kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"),
                    "message": kony.i18n.getLocalizedString("i18n.frmCompanies.Customer_created_successfully")
                }
            });
        }

        function onError(error) {
            self.hideCreatecompanyScreen(inputParam, true, {
                "toastMessage": {
                    "status": kony.i18n.getLocalizedString("i18n.frmGroupsController.error"),
                    "message": ErrorInterceptor.errorMessage(error)
                }
            });
        }
        self.businessController.createCustomer(context, completionCreateCustomer, onError);
    };
    /*
     * @name verifyUsername and call OFAC verification
     * @member CustomerCreateModule.presentationController
     */
    PresentationController.prototype.verifyUsername = function(context, OFACReqParam) {
        var self = this;
        var userAvailable = false;
        self.showLoadingScreen();

        function completionVerifyUsername(response) {
            var res = response.msg;
            self.clearToDefaults();
            self.hideLoadingScreen();
            if (response.msg === "false") {
                userAvailable = true;
                if (OFACReqParam) {
                    self.OFACverification(OFACReqParam);
                }
            } else {
                userAvailable = false;
            }
            self.customerCreateModel.isUsernameAvailable = {
                "userAvailable": userAvailable
            };
            self.presentCreateScreen(self.customerCreateModel);
        }

        function onError(error) {
            self.hideLoadingScreen();
            self.showToastMessage(ErrorInterceptor.errorMessage(error), kony.i18n.getLocalizedString("i18n.frmGroupsController.error"));
        }
        self.businessController.verifyUsername(context, completionVerifyUsername, onError);
    };
    /*
     * @name OFACverification
     * @member CustomerCreateModule.presentationController
     */
    PresentationController.prototype.OFACverification = function(context) {
        var self = this;
        self.showLoadingScreen();

        function completionOFACVerify(response) {
            var result;
            self.clearToDefaults();
            self.hideLoadingScreen();
            if ((response.msg.indexOf("Successful") >= 0) && response.Status === "true") {
                result = {
                    "canProceed": true
                };
            } else {
                result = {
                    "canProceed": false
                };
            }
            self.customerCreateModel.OFACVerification = result;
            self.presentCreateScreen(self.customerCreateModel);
        }

        function onError(error) {
            self.hideLoadingScreen();
            self.showToastMessage(ErrorInterceptor.errorMessage(error), kony.i18n.getLocalizedString("i18n.frmGroupsController.error"));
        }
        self.businessController.verifyOFACandCIP(context, completionOFACVerify, onError);
    };
    /*
   * @name showEditCustomerScreen
   * @member CustomerCreateModule.presentationController
   * @param: {"id": "","DateOfBirth": "","DrivingLicenseNumber": "","FirstName": "","LastName": "","Lastlogintime": "","MiddleName":"","Ssn": "","UserName": "","createdts": "","role_name": "","status": "","Email": "","Phone": "",
	          "company": {"UserName": "","id": "","TypeId": "TYPE_ID_SMALL_BUSINESS"}}
   */
    PresentationController.prototype.showEditCustomerScreen = function(context) {
        var self = this;
        self.showLoadingScreen();
        self.clearToDefaults();
        self.customerCreateModel.editInputs = context;
        self.callsForEditCustomer(self.customerCreateModel.editInputs);
        self.presentCreateScreen(self.customerCreateModel);
    };
    PresentationController.prototype.callsForEditCustomer = function(context) {
        var self = this;
        var companyType = context.company.TypeId;
        var companyId = context.company.id;
        self.getAccounts({
            "Organization_id": companyId
        });
        self.getCustomerAccounts({
            "Customer_id": context.id
        });
        self.getRoles({
            "typeId": companyType
        });
    };
    /*
     * @name getCustomerRoleLimit
     * @member CustomerCreateModule.presentationController
     */
    PresentationController.prototype.getCustomerRoleLimit = function(context) {
        var self = this;
        self.showLoadingScreen();

        function completionGetCustomerLimits(response) {
            self.showLoadingScreen();
            self.clearToDefaults();
            var group_Id = response.Group_id;
            self.getPermissionsOfRole({
                "Group_id": group_Id
            }, response);
        }

        function onError(error) {
            self.hideLoadingScreen();
            self.showToastMessage(ErrorInterceptor.errorMessage(error), kony.i18n.getLocalizedString("i18n.frmGroupsController.error"));
        }
        self.businessController.getBBCustomerServiceLimit(context, completionGetCustomerLimits, onError);
    };
    /*
     * @name editCustomer
     * @member CustomerCreateModule.presentationController
     */
    PresentationController.prototype.editCustomer = function(context, orgId) {
        var self = this;
        self.showLoadingScreen();
        var inputParam = {
            "action": "createCustomer",
            "id": orgId
        };

        function completionEditCustomer(response) {
            self.hideLoadingScreen();
            self.hideCreatecompanyScreen(inputParam, true, {
                "toastMessage": {
                    "status": kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"),
                    "message": kony.i18n.getLocalizedString("i18n.frmCompanies.Customer_updated_successfully")
                }
            });
        }

        function onError(error) {
            self.hideLoadingScreen();
            self.hideCreatecompanyScreen(inputParam, true, {
                "toastMessage": {
                    "status": kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"),
                    "message": ErrorInterceptor.errorMessage(error)
                }
            });
        }
        self.businessController.editCustomer(context, completionEditCustomer, onError);
    };
    /*
     * @name getCustomerAccounts
     * @member CustomerCreateModule.presentationController
     */
    PresentationController.prototype.getCustomerAccounts = function(context) {
        var self = this;

        function completionCustomerAcc(response) {
            self.clearToDefaults();
            self.customerCreateModel.customerAccounts = response.Accounts;
            self.hideLoadingScreen();
            self.presentCreateScreen(self.customerCreateModel);
        }

        function onError(error) {
            self.hideLoadingScreen();
            self.showToastMessage(ErrorInterceptor.errorMessage(error), kony.i18n.getLocalizedString("i18n.frmGroupsController.error"));
        }
        self.showLoadingScreen();
        self.businessController.getCustomerAccounts(context, completionCustomerAcc, onError);
    };
    /*
     * clear viewmodel to default values
     */
    PresentationController.prototype.clearToDefaults = function() {
        var self = this;
        self.customerCreateModel.accounts = null;
        self.customerCreateModel.roles = null;
        self.customerCreateModel.rolePermission = null;
        self.customerCreateModel.companyDetails = null;
        self.customerCreateModel.isUsernameAvailable = null;
        self.customerCreateModel.OFACVerification = null;
        self.customerCreateModel.context = null;
        self.customerCreateModel.editInputs = null;
        self.customerCreateModel.customerLimits = null;
        self.customerCreateModel.customerAccounts = null;
        self.customerCreateModel.rolePermissionEdit = null;
    };
    PresentationController.prototype.getUsernameRulesAndPolicy = function() {
        var self = this;
        self.showLoadingScreen();

        function successCallback(response) {
            var context = {
                "usernameRulesAndPolicy": {
                    "usernamerules": response.usernamerules,
                    "usernamepolicy": response.usernamepolicy
                }
            };
            self.presentCreateScreen(context);
            self.hideLoadingScreen();
        }

        function failureCallback(error) {
            self.showToastMessage(ErrorInterceptor.errorMessage(error), kony.i18n.getLocalizedString("i18n.frmGroupsController.error"));
            self.hideLoadingScreen();
        }
        self.businessController.getUsernameRulesAndPolicy({}, successCallback, failureCallback);
    };
    return PresentationController;
});