define("CustomerCreateModule/frmCustomerCreate", function() {
    return function(controller) {
        function addWidgetsfrmCustomerCreate() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "305dp",
                "zIndex": 1
            }, {}, {});
            flxLeftPanel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "konydbxlogobignverted.png"
                    },
                    "imgBottomLogo": {
                        "src": "konydbxinverted.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPanel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CreateCompany\")",
                        "isVisible": false,
                        "right": "0dp"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")",
                        "isVisible": false
                    },
                    "flxButtons": {
                        "right": "35dp",
                        "width": "400dp"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.companies\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadcrumb",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadcrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "isVisible": true
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ViewCompanies\")"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CREATE_CUSTOMER\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumb.add(breadcrumbs);
            var flxMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "88%",
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxCustomerContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerContainer.setDefaultUnit(kony.flex.DP);
            var flxCustomerCreate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "60dp",
                "clipBounds": true,
                "id": "flxCustomerCreate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerCreate.setDefaultUnit(kony.flex.DP);
            var flxCustomerVerticalTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerVerticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "225dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerVerticalTabs.setDefaultUnit(kony.flex.DP);
            var verticalTabsCustomer = new com.adminConsole.common.verticalTabs1({
                "clipBounds": true,
                "height": "100%",
                "id": "verticalTabsCustomer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CUSTOMER_DETAILS\")"
                    },
                    "btnOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNT_ACCESS\")"
                    },
                    "btnOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ROLES_LIMTS\")"
                    },
                    "flxOption3": {
                        "isVisible": true
                    },
                    "flxOption4": {
                        "isVisible": false
                    },
                    "imgSelected1": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected2": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected3": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected4": {
                        "src": "right_arrow2x.png"
                    },
                    "lblOptional2": {
                        "isVisible": false
                    },
                    "lblOptional3": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxVerticalTabsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxVerticalTabsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxVerticalTabsSeperator.setDefaultUnit(kony.flex.DP);
            flxVerticalTabsSeperator.add();
            flxCustomerVerticalTabs.add(verticalTabsCustomer, flxVerticalTabsSeperator);
            var flxCustomerDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerDetails.setDefaultUnit(kony.flex.DP);
            var flxCustomerDetailsContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxCustomerDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxCustomerDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxOFACError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxOFACError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxOFACError.setDefaultUnit(kony.flex.DP);
            var alertMessage = new com.adminConsole.customerMang.alertMessage({
                "clipBounds": true,
                "height": "40px",
                "id": "alertMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "alertMessage": {
                        "height": "40px",
                        "width": "100%"
                    },
                    "flxMessage": {
                        "left": "0px",
                        "right": "viz.val_cleared",
                        "width": "91%"
                    },
                    "flxRow1": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "height": "100%",
                        "left": "viz.val_cleared",
                        "top": "15%",
                        "width": "100%"
                    },
                    "flxRow2": {
                        "isVisible": false
                    },
                    "fonticonBullet1": {
                        "isVisible": false,
                        "left": "0px"
                    },
                    "lblData": {
                        "left": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOFACError.add(alertMessage);
            var flxSalutation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxSalutation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSalutation.setDefaultUnit(kony.flex.DP);
            var flxDetails1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetails1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "29.80%",
                "zIndex": 2
            }, {}, {});
            flxDetails1.setDefaultUnit(kony.flex.DP);
            var lblSalutation = new kony.ui.Label({
                "id": "lblSalutation",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblContactDetails1\")",
                "top": "5px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBxSalutation = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40dp",
                "id": "lstBxSalutation",
                "isVisible": true,
                "left": "0px",
                "masterData": [
                    ["k1", "Select Salutation"],
                    ["k2", "Mrs."],
                    ["k3", "Dr."],
                    ["k4", "Ms."]
                ],
                "selectedKey": "k1",
                "selectedKeyValue": ["k1", "Select Salutation"],
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "30dp",
                "width": "100%",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlstbxcursor",
                "multiSelect": false
            });
            var lblSalutationError = new kony.ui.Label({
                "bottom": "0px",
                "id": "lblSalutationError",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.selectSalutaion\")",
                "top": 75,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetails1.add(lblSalutation, lstBxSalutation, lblSalutationError);
            flxSalutation.add(flxDetails1);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var flxColumn11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%"
            }, {}, {});
            flxColumn11.setDefaultUnit(kony.flex.DP);
            var textBoxEntry11 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxEntry11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxInlineError": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "right": "viz.val_cleared"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstName\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn11.add(textBoxEntry11);
            var flxColumn12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%",
                "zIndex": 1
            }, {}, {});
            flxColumn12.setDefaultUnit(kony.flex.DP);
            var textBoxEntry12 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxEntry12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": true,
                        "left": "100dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn12.add(textBoxEntry12);
            var flxColumn13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.45%",
                "zIndex": 1
            }, {}, {});
            flxColumn13.setDefaultUnit(kony.flex.DP);
            var textBoxEntry13 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxEntry13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn13.add(textBoxEntry13);
            flxRow1.add(flxColumn11, flxColumn12, flxColumn13);
            var flxRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRow2.setDefaultUnit(kony.flex.DP);
            var flxColumn21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%"
            }, {}, {});
            flxColumn21.setDefaultUnit(kony.flex.DP);
            var textBoxEntry21 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxEntry21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxBtnCheck": {
                        "isVisible": true
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")",
                        "right": "70dp"
                    },
                    "textBoxEntry": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblUsernameCheck = new kony.ui.Label({
                "id": "lblUsernameCheck",
                "isVisible": false,
                "left": "20dp",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.Username_Available\")",
                "top": "67dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRulesLabel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxRulesLabel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "30px",
                "zIndex": 10
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRulesLabel.setDefaultUnit(kony.flex.DP);
            var lblRules = new kony.ui.RichText({
                "id": "lblRules",
                "isVisible": true,
                "onClick": controller.AS_RichText_bd35a9814af54de48c78a578bdfcf027,
                "right": "0px",
                "skin": "slRichText0f825e4c814004c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Rulestxt\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRulesLabel.add(lblRules);
            flxColumn21.add(textBoxEntry21, lblUsernameCheck, flxRulesLabel);
            var flxColumn22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%",
                "zIndex": 1
            }, {}, {});
            flxColumn22.setDefaultUnit(kony.flex.DP);
            var textBoxEntry22 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxEntry22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxInlineError": {
                        "isVisible": false,
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn22.add(textBoxEntry22);
            var flxColumn23 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn23",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.45%",
                "zIndex": 1
            }, {}, {});
            flxColumn23.setDefaultUnit(kony.flex.DP);
            var lblHeading = new kony.ui.Label({
                "id": "lblHeading",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchParam4\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var textBoxEntry23 = new com.adminConsole.common.contactNumber({
                "clipBounds": true,
                "height": "70.56%",
                "id": "textBoxEntry23",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "1dp",
                "skin": "slFbox",
                "top": "26dp",
                "overrides": {
                    "contactNumber": {
                        "height": "70.56%",
                        "left": "10dp",
                        "right": "1dp",
                        "top": "26dp",
                        "width": "viz.val_cleared"
                    },
                    "flxContactNumberWrapper": {
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "flxError": {
                        "height": "33dp",
                        "isVisible": false,
                        "left": "0%",
                        "right": "0dp",
                        "top": "44dp",
                        "width": "viz.val_cleared"
                    },
                    "lblDash": {
                        "isVisible": true,
                        "left": "32.50%",
                        "width": "5dp"
                    },
                    "lblErrorText": {
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "txtContactNumber": {
                        "left": "36.50%",
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "txtISDCode": {
                        "width": "32%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn23.add(lblHeading, textBoxEntry23);
            flxRow2.add(flxColumn21, flxColumn22, flxColumn23);
            var flxRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxRow3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRow3.setDefaultUnit(kony.flex.DP);
            var flxColumn31 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn31",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%"
            }, {}, {});
            flxColumn31.setDefaultUnit(kony.flex.DP);
            var textBoxEntry31 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxEntry31",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.DOB\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.DOB\")",
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxCalendarDOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCalendarDOB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "25dp",
                "width": "95.20%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxCalendarDOB.setDefaultUnit(kony.flex.DP);
            var customCalCustomerDOB = new kony.ui.CustomWidget({
                "id": "customCalCustomerDOB",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "maxDate": "true",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "single",
                "value": ""
            });
            flxCalendarDOB.add(customCalCustomerDOB);
            flxColumn31.add(textBoxEntry31, flxCalendarDOB);
            var flxColumn32 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn32",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%",
                "zIndex": 1
            }, {}, {});
            flxColumn32.setDefaultUnit(kony.flex.DP);
            var textBoxEntry32 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxEntry32",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN\")",
                        "maxTextLength": null,
                        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn32.add(textBoxEntry32);
            var flxColumn33 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn33",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.45%",
                "zIndex": 1
            }, {}, {});
            flxColumn33.setDefaultUnit(kony.flex.DP);
            var textBoxEntry33 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxEntry33",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Drivers_License\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": true,
                        "left": "115dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Drivers_License\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn33.add(textBoxEntry33);
            flxRow3.add(flxColumn31, flxColumn32, flxColumn33);
            var flxRow4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxRow4",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRow4.setDefaultUnit(kony.flex.DP);
            var flxDetailsRisk = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsRisk",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "62%"
            }, {}, {});
            flxDetailsRisk.setDefaultUnit(kony.flex.DP);
            var lblRisksHeader = new kony.ui.Label({
                "id": "lblRisksHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails8\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSubDetailsRisk = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "36px",
                "id": "flxSubDetailsRisk",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxOuterBorder",
                "top": "10px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxSubDetailsRisk.setDefaultUnit(kony.flex.DP);
            var flxRiskStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "36dp",
                "id": "flxRiskStatus",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxfdf6f1bg100px",
                "top": "0dp",
                "width": "100dp",
                "zIndex": 111
            }, {}, {});
            flxRiskStatus.setDefaultUnit(kony.flex.DP);
            var lblDetails8 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblDetails8",
                "isVisible": true,
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails8\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRiskStatus.add(lblDetails8);
            var flxFlagSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "36dp",
                "id": "flxFlagSeperator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxOuterBorder",
                "top": "0dp",
                "width": "1px",
                "zIndex": 111
            }, {}, {});
            flxFlagSeperator.setDefaultUnit(kony.flex.DP);
            flxFlagSeperator.add();
            var flxSelectFlags = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "36px",
                "id": "flxSelectFlags",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 111
            }, {}, {});
            flxSelectFlags.setDefaultUnit(kony.flex.DP);
            var flxFlag3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "13dp",
                "id": "flxFlag3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "13px",
                "zIndex": 1
            }, {}, {});
            flxFlag3.setDefaultUnit(kony.flex.DP);
            var imgFlag3 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgFlag3",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Delete"
            });
            flxFlag3.add(imgFlag3);
            var lblFlag3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFlag3",
                "isVisible": true,
                "left": "6px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFlag3\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFlag2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "13dp",
                "id": "flxFlag2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "13px",
                "zIndex": 1
            }, {}, {});
            flxFlag2.setDefaultUnit(kony.flex.DP);
            var imgFlag2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgFlag2",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Deactive"
            });
            flxFlag2.add(imgFlag2);
            var lblFlag2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFlag2",
                "isVisible": true,
                "left": "6px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Defaulter\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFlag1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "13dp",
                "id": "flxFlag1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": 15,
                "width": "13px",
                "zIndex": 2
            }, {}, {});
            flxFlag1.setDefaultUnit(kony.flex.DP);
            var imgFlag1 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgFlag1",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Active"
            });
            flxFlag1.add(imgFlag1);
            var lblFlag1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFlag1",
                "isVisible": true,
                "left": "6px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.High_Risk\")",
                "top": "15px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectFlags.add(flxFlag3, lblFlag3, flxFlag2, lblFlag2, flxFlag1, lblFlag1);
            flxSubDetailsRisk.add(flxRiskStatus, flxFlagSeperator, flxSelectFlags);
            flxDetailsRisk.add(lblRisksHeader, flxSubDetailsRisk);
            var flxEagreementDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEagreementDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 15,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "29%",
                "zIndex": 2
            }, {}, {});
            flxEagreementDetails.setDefaultUnit(kony.flex.DP);
            var lblEagreementHeader = new kony.ui.Label({
                "id": "lblEagreementHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Edit_eagreementStatus\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchEagreement = new kony.ui.Switch({
                "height": "25dp",
                "id": "switchEagreement",
                "isVisible": true,
                "left": "0dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "17px",
                "width": "38dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEagreementDetails.add(lblEagreementHeader, switchEagreement);
            flxRow4.add(flxDetailsRisk, flxEagreementDetails);
            flxCustomerDetailsContainer.add(flxOFACError, flxSalutation, flxRow1, flxRow2, flxRow3, flxRow4);
            var flxDetailsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxDetailsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsDetails = new com.adminConsole.common.commonButtons({
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "20dp"
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                        "isVisible": true,
                        "right": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDetailsButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxDetailsButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxDetailsButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxDetailsButtonsSeperator.add();
            flxDetailsButtons.add(commonButtonsDetails, flxDetailsButtonsSeperator);
            var flxUsernameRules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUsernameRules",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox0a30851fc80b24b",
                "top": "150px",
                "width": "370px",
                "zIndex": 10
            }, {}, {});
            flxUsernameRules.setDefaultUnit(kony.flex.DP);
            var lblUsernameRules = new kony.ui.Label({
                "height": "15px",
                "id": "lblUsernameRules",
                "isVisible": true,
                "left": "10px",
                "skin": "slLabel0ff9cfa99545646",
                "text": "Username rules:",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUsernameRulesInner = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "10px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "120px",
                "horizontalScrollIndicator": true,
                "id": "flxUsernameRulesInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10px",
                "pagingEnabled": false,
                "right": "10px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "40px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxUsernameRulesInner.setDefaultUnit(kony.flex.DP);
            var rtxUsernameRules = new kony.ui.RichText({
                "bottom": "10px",
                "height": "100%",
                "id": "rtxUsernameRules",
                "isVisible": true,
                "left": "0px",
                "linkSkin": "defRichTextLink",
                "skin": "sknrtxLato485c7514px",
                "text": "<ul><li>Minimum Length- 8, Maximum Length- 24</li> <li>Can be alpha-numeric (a-z, A-Z, 0-9)</li> <li>No spaces or special characters allowed except period '.'</li> <li>Should Start with an alphabet or digit.</li> <li>Username is not case sensitive.</li> <li>Period '.' cannot be used more than once.</li></ul>",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [5, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUsernameRulesInner.add(rtxUsernameRules);
            flxUsernameRules.add(lblUsernameRules, flxUsernameRulesInner);
            flxCustomerDetails.add(flxCustomerDetailsContainer, flxDetailsButtons, flxUsernameRules);
            var flxCustomerAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerAccounts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerAccounts.setDefaultUnit(kony.flex.DP);
            var flxHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeading.setDefaultUnit(kony.flex.DP);
            var flxMembership = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMembership",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "290dp",
                "zIndex": 1
            }, {}, {});
            flxMembership.setDefaultUnit(kony.flex.DP);
            var lblMembershipHeading = new kony.ui.Label({
                "id": "lblMembershipHeading",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.MEMBERSHIP_ID\")",
                "top": "15dp",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMembershipValue = new kony.ui.Label({
                "id": "lblMembershipValue",
                "isVisible": true,
                "left": "125dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "234163546786453216",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMembership.add(lblMembershipHeading, lblMembershipValue);
            var flxTin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "350dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxTin.setDefaultUnit(kony.flex.DP);
            var lblTinHeading = new kony.ui.Label({
                "id": "lblTinHeading",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.TAXNUMBER\")",
                "top": "15dp",
                "width": "90dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTinValue = new kony.ui.Label({
                "id": "lblTinValue",
                "isVisible": true,
                "left": "95dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "234163546",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTin.add(lblTinHeading, lblTinValue);
            var flxLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxLine.setDefaultUnit(kony.flex.DP);
            flxLine.add();
            flxHeading.add(flxMembership, flxTin, flxLine);
            var flxAccountsAddAndRemove = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxAccountsAddAndRemove",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccountsAddAndRemove.setDefaultUnit(kony.flex.DP);
            var addAndRemoveAccounts = new com.adminConsole.common.addAndRemoveOptions1({
                "bottom": "0dp",
                "clipBounds": true,
                "id": "addAndRemoveAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "addAndRemoveOptions1": {
                        "bottom": "0dp",
                        "height": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "width": "100%"
                    },
                    "btnRemoveAll": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")"
                    },
                    "btnSelectAll": {
                        "isVisible": false
                    },
                    "flxAvailableOptions": {
                        "height": "100%",
                        "left": "20px"
                    },
                    "flxError": {
                        "left": "30%"
                    },
                    "flxFilteredSearch": {
                        "isVisible": false
                    },
                    "flxSegSearchType": {
                        "isVisible": false
                    },
                    "flxSelectedOptions": {
                        "right": "20px"
                    },
                    "imgLoadingAccountSearch": {
                        "src": "loadingscreenimage.gif"
                    },
                    "lblAvailableOptionsHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Available_Accounts\")"
                    },
                    "lblSelectedOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Selected_Accounts\")"
                    },
                    "rtxAvailableOptionsMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Search_to_see_available_accounts\")"
                    },
                    "rtxSelectedOptionsMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Select_atleast_one_account_to_proceed\")"
                    },
                    "segAddOptions": {
                        "data": [{
                            "btnAdd": "",
                            "lblFullName": "",
                            "lblUserIdValue": "",
                            "lblUsername": ""
                        }]
                    },
                    "segSelectedOptions": {
                        "data": [{
                            "fontIconClose": "",
                            "lblOption": ""
                        }]
                    },
                    "tbxSearchBox": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Search_by_AccountNo_TIN_MembershipID\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAccountsAddAndRemove.add(addAndRemoveAccounts);
            var flxAccountsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxAccountsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccountsButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsAccounts = new com.adminConsole.common.commonButtons({
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "20dp"
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                        "isVisible": true,
                        "right": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAccountsButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAccountsButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAccountsButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxAccountsButtonsSeperator.add();
            flxAccountsButtons.add(commonButtonsAccounts, flxAccountsButtonsSeperator);
            flxCustomerAccounts.add(flxHeading, flxAccountsAddAndRemove, flxAccountsButtons);
            var flxCustomerRolesLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerRolesLimits",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerRolesLimits.setDefaultUnit(kony.flex.DP);
            var flxRolesLimitsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxRolesLimitsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRolesLimitsContainer.setDefaultUnit(kony.flex.DP);
            var lblRoleHeader = new kony.ui.Label({
                "id": "lblRoleHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.btnRoles\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxSelectRole = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxSelectRole",
                "isVisible": true,
                "left": "20dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "45dp",
                "width": "250dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxInlineError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxInlineError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "85dp",
                "width": "50%",
                "zIndex": 2
            }, {}, {});
            flxInlineError.setDefaultUnit(kony.flex.DP);
            var lblInlineErrorIcon = new kony.ui.Label({
                "centerX": "38%",
                "centerY": "52%",
                "id": "lblInlineErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblInlineError = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblInlineError",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLabelRed",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Please_select_a_role\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInlineError.add(lblInlineErrorIcon, lblInlineError);
            var flxLimitsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxLimitsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "100dp",
                "zIndex": 1
            }, {}, {});
            flxLimitsContainer.setDefaultUnit(kony.flex.DP);
            var lblSetLimitsHeader = new kony.ui.Label({
                "id": "lblSetLimitsHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Set_Limits\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSegLimitsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "id": "flxSegLimitsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "35dp",
                "zIndex": 1
            }, {}, {});
            flxSegLimitsContainer.setDefaultUnit(kony.flex.DP);
            var flxSegLimitsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxSegLimitsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegLimitsHeader.setDefaultUnit(kony.flex.DP);
            var flxPermissionName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPermissionName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "37%",
                "zIndex": 1
            }, {}, {});
            flxPermissionName.setDefaultUnit(kony.flex.DP);
            var lblPermissionName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPermissionName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "text": "PERMISSIONS",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPermissionName.add(lblPermissionName);
            var flxMaxDailyLimit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMaxDailyLimit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxMaxDailyLimit.setDefaultUnit(kony.flex.DP);
            var lblDailyLimit = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDailyLimit",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "text": "MAX DAILY LIMIT",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxDailyLimit.add(lblDailyLimit);
            var flxMaxTransactionLimit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMaxTransactionLimit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxMaxTransactionLimit.setDefaultUnit(kony.flex.DP);
            var lblTransactionLimit = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionLimit",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "text": "MAX TRANSACTION LIMIT",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxTransactionLimit.add(lblTransactionLimit);
            var flxSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknTableHeaderLine",
                "zIndex": 1
            }, {}, {});
            flxSeperator.setDefaultUnit(kony.flex.DP);
            flxSeperator.add();
            flxSegLimitsHeader.add(flxPermissionName, flxMaxDailyLimit, flxMaxTransactionLimit, flxSeperator);
            var flxSegLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "id": "flxSegLimits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "50dp",
                "zIndex": 1
            }, {}, {});
            flxSegLimits.setDefaultUnit(kony.flex.DP);
            var segLimits = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "data": [{
                    "lblCurrencyValue": "",
                    "lblDailyLimit": "",
                    "lblErrorIconDaily": "",
                    "lblErrorIconTransaction": "",
                    "lblErrorTextDaily": "",
                    "lblErrorTextTransaction": "",
                    "lblPermissionName": "",
                    "lblTransactionCurrency": "",
                    "lblTransactionLimit": "",
                    "tbxMaxDailyLimit": {
                        "placeholder": "",
                        "text": ""
                    },
                    "tbxTransactionLimit": {
                        "placeholder": "",
                        "text": ""
                    }
                }],
                "groupCells": false,
                "id": "segLimits",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxLimitsCustomer",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "e1e5ed00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCurrency": "flxCurrency",
                    "flxCurrencyTransactional": "flxCurrencyTransactional",
                    "flxDailyLimit": "flxDailyLimit",
                    "flxErrorDailyLimit": "flxErrorDailyLimit",
                    "flxErrorTransaction": "flxErrorTransaction",
                    "flxLimitsCustomer": "flxLimitsCustomer",
                    "flxLine": "flxLine",
                    "flxLineVertical": "flxLineVertical",
                    "flxTransactionLimit": "flxTransactionLimit",
                    "lblCurrencyValue": "lblCurrencyValue",
                    "lblDailyLimit": "lblDailyLimit",
                    "lblErrorIconDaily": "lblErrorIconDaily",
                    "lblErrorIconTransaction": "lblErrorIconTransaction",
                    "lblErrorTextDaily": "lblErrorTextDaily",
                    "lblErrorTextTransaction": "lblErrorTextTransaction",
                    "lblPermissionName": "lblPermissionName",
                    "lblTransactionCurrency": "lblTransactionCurrency",
                    "lblTransactionLimit": "lblTransactionLimit",
                    "tbxMaxDailyLimit": "tbxMaxDailyLimit",
                    "tbxTransactionLimit": "tbxTransactionLimit"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegLimits.add(segLimits);
            flxSegLimitsContainer.add(flxSegLimitsHeader, flxSegLimits);
            var flxNoResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "50dp",
                "clipBounds": true,
                "id": "flxNoResults",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxf9f9f9NoBorder",
                "top": "40dp",
                "zIndex": 1
            }, {}, {});
            flxNoResults.setDefaultUnit(kony.flex.DP);
            var rtxNoResults = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "height": "40dp",
                "id": "rtxNoResults",
                "isVisible": true,
                "linkSkin": "defRichTextLink",
                "skin": "sknrtxLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.No_limits_found_Select_role_to_view_limits\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResults.add(rtxNoResults);
            flxLimitsContainer.add(lblSetLimitsHeader, flxSegLimitsContainer, flxNoResults);
            flxRolesLimitsContainer.add(lblRoleHeader, lstBoxSelectRole, flxInlineError, flxLimitsContainer);
            var flxRolesLimitButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxRolesLimitButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRolesLimitButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsRolesLimit = new com.adminConsole.common.commonButtons({
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsRolesLimit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "20dp"
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.CREATE\")",
                        "right": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxLimitButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLimitButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxLimitButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxLimitButtonsSeperator.add();
            flxRolesLimitButtons.add(commonButtonsRolesLimit, flxLimitButtonsSeperator);
            flxCustomerRolesLimits.add(flxRolesLimitsContainer, flxRolesLimitButtons);
            flxCustomerCreate.add(flxCustomerVerticalTabs, flxCustomerDetails, flxCustomerAccounts, flxCustomerRolesLimits);
            var flxInlineErrorAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxInlineErrorAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInlineErrorAccount.setDefaultUnit(kony.flex.DP);
            var lblIconError = new kony.ui.Label({
                "centerX": "41%",
                "centerY": "52%",
                "id": "lblIconError",
                "isVisible": true,
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorText = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblErrorText",
                "isVisible": true,
                "skin": "sknLabelRed",
                "text": "Please select an account",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInlineErrorAccount.add(lblIconError, lblErrorText);
            flxCustomerContainer.add(flxCustomerCreate, flxInlineErrorAccount);
            flxMainContainer.add(flxCustomerContainer);
            flxRightPanel.add(flxMainHeader, flxBreadcrumb, flxMainContainer);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopup.setDefaultUnit(kony.flex.DP);
            var popUpRole = new com.adminConsole.common.popUp1({
                "clipBounds": true,
                "height": "100%",
                "id": "popUpRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.CANCEL\")",
                        "right": "130px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.UPDATE\")"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Update Role"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30dp",
                        "right": "viz.val_cleared",
                        "text": "Are you sure to update  role Admin?\n\nThe Admin role has been assigned tothis customer.Changing this may result in losing the data permanently.",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopup.add(popUpRole);
            flxMain.add(flxLeftPanel, flxRightPanel, flxHeaderDropdown, flxToastMessage, flxLoading, flxPopup);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerCreate,
            "enabledForIdleTimeout": true,
            "id": "frmCustomerCreate",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_e2ab347545e548ea99359704703a939f(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});