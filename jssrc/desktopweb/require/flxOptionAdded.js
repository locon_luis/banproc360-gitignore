define("flxOptionAdded", function() {
    return function(controller) {
        var flxOptionAdded = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxOptionAdded",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {});
        flxOptionAdded.setDefaultUnit(kony.flex.DP);
        var flxAddOptionWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxAddOptionWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": 25,
            "skin": "sknflxffffffop0e",
            "top": "10dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknFlxSegRowHover11abeb"
        });
        flxAddOptionWrapper.setDefaultUnit(kony.flex.DP);
        var lblOption = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOption",
            "isVisible": true,
            "left": "20px",
            "right": "45px",
            "skin": "sknlbl485c7514px",
            "text": "John Doe",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxClose = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_d01abe772a6a4c09b140ae1057a7a0f2,
            "right": "5px",
            "skin": "slFbox",
            "width": "30px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxClose.setDefaultUnit(kony.flex.DP);
        var fontIconClose = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "fontIconClose",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconCross14px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxClose.add(fontIconClose);
        flxAddOptionWrapper.add(lblOption, flxClose);
        flxOptionAdded.add(flxAddOptionWrapper);
        return flxOptionAdded;
    }
})