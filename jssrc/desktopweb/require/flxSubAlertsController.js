define("userflxSubAlertsController", {
    //Type your controller code here 
});
define("flxSubAlertsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxOptions **/
    AS_FlexContainer_gb05fbb7e77e41eda2711661bab68811: function AS_FlexContainer_gb05fbb7e77e41eda2711661bab68811(eventobject, context) {
        this.executeOnParent("onClickOptions");
    }
});
define("flxSubAlertsController", ["userflxSubAlertsController", "flxSubAlertsControllerActions"], function() {
    var controller = require("userflxSubAlertsController");
    var controllerActions = ["flxSubAlertsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
