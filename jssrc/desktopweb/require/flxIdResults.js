define("flxIdResults", function() {
    return function(controller) {
        var flxIdResults = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIdResults",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknbackGroundffffff100",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxIdResults.setDefaultUnit(kony.flex.DP);
        var lblIdName = new kony.ui.Label({
            "bottom": "10dp",
            "id": "lblIdName",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlbl485c7514px",
            "text": "Granville Street, Vancover, BC, Canada",
            "top": "10dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxIdResults.add(lblIdName);
        return flxIdResults;
    }
})