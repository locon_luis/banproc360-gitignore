define("ServicesManagementModule/frmServiceManagement", function() {
    return function(controller) {
        function addWidgetsfrmServiceManagement() {
            this.setDefaultUnit(kony.flex.DP);
            var flxServiceManagement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "624dp",
                "id": "flxServiceManagement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxServiceManagement.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "segMenu": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "CopyslFbox0c18001ae52ef46",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "right": "150dp"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DOWNLOADLIST\")"
                    },
                    "flxButtons": {
                        "right": "0dp",
                        "width": "284dp"
                    },
                    "flxHeaderSeperator": {
                        "bottom": "0dp",
                        "height": "1dp",
                        "right": "0dp"
                    },
                    "flxHeaderUserOptions": {
                        "right": "0px"
                    },
                    "flxMainHeader": {
                        "left": "0dp"
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.Services\")",
                        "left": "35dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": 0,
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "105dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "bottom": 0,
                        "isVisible": false,
                        "top": "105dp"
                    },
                    "btnBackToMain": {
                        "text": "SERVICES"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "text": "ADD SERVICES"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader, breadcrumbs);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": false,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "Copysknscrollflxf0ja093a1ef62648",
                "top": "120dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxAddService = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "340dp",
                "id": "flxAddService",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxUploadImage0e65d5a93fe0f45",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAddService.setDefaultUnit(kony.flex.DP);
            var flxAddServiceMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddServiceMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddServiceMain.setDefaultUnit(kony.flex.DP);
            var lblNoServiceAdded = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoServiceAdded",
                "isVisible": true,
                "skin": "sknLblLato",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceAdded\")",
                "top": "100dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblClickOnAddService = new kony.ui.Label({
                "id": "lblClickOnAddService",
                "isVisible": true,
                "left": 0,
                "skin": "sknlbllatoRegular0f715a58455b840",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblClickOnAddService\")",
                "top": "140dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddService = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40dp",
                "id": "btnAddService",
                "isVisible": true,
                "left": 0,
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.btnAddService\")",
                "top": "180dp",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxAddServiceMain.add(lblNoServiceAdded, lblClickOnAddService, btnAddService);
            flxAddService.add(flxAddServiceMain);
            var flxViewServices = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewServices",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxViewServices.setDefaultUnit(kony.flex.DP);
            var flxSearchBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75dp",
                "id": "flxSearchBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "5%",
                "skin": "slFbox",
                "top": "0dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxSearchBar.setDefaultUnit(kony.flex.DP);
            var search = new com.adminConsole.header.search({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "search",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxSearch": {
                        "centerX": "viz.val_cleared",
                        "centerY": "50%",
                        "height": "100%",
                        "left": "viz.val_cleared",
                        "right": "0%",
                        "width": "35%"
                    },
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": 36,
                        "left": "0dp",
                        "right": "0px",
                        "top": "viz.val_cleared",
                        "width": 350
                    },
                    "flxSubHeader": {
                        "left": "viz.val_cleared",
                        "right": "0px"
                    },
                    "imgSearchCancel": {
                        "src": "close_blue.png"
                    },
                    "imgSearchIcon": {
                        "src": "search_1x.png"
                    },
                    "search": {
                        "height": "100%",
                        "left": "viz.val_cleared",
                        "right": "35px",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchBar.add(search);
            var flxServicesList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxServicesList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "75dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxServicesList.setDefaultUnit(kony.flex.DP);
            var flxServicesListHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxServicesListHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "5%",
                "skin": "slFbox",
                "top": "0dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxServicesListHeader.setDefaultUnit(kony.flex.DP);
            var lblServicesName = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblServicesName",
                "isVisible": true,
                "left": "30dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": "20dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortServicesName = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortServicesName",
                "isVisible": true,
                "left": "80dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblServicesCode = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblServicesCode",
                "isVisible": true,
                "left": "200dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CODE\")",
                "top": "20dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortServicesCode = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortServicesCode",
                "isVisible": true,
                "left": "250dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblServicesType = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblServicesType",
                "isVisible": true,
                "left": "330dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TYPE\")",
                "top": "20dp",
                "width": "35dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortServicesType = new kony.ui.Image2({
                "height": "13dp",
                "id": "imgSortServicesType",
                "isVisible": true,
                "left": "370dp",
                "skin": "slImage",
                "src": "filter.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblServicesCategory = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblServicesCategory",
                "isVisible": true,
                "left": "450dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CATEGORY\")",
                "top": "20dp",
                "width": "70dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortServicesCategory = new kony.ui.Image2({
                "height": "13dp",
                "id": "imgSortServicesCategory",
                "isVisible": true,
                "left": "525dp",
                "skin": "slImage",
                "src": "filter.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblServicesSupportedChannels = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblServicesSupportedChannels",
                "isVisible": true,
                "left": "630dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Services.Supportedchannels\")",
                "top": "20dp",
                "width": "140dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortServicesSupportedChannels = new kony.ui.Image2({
                "height": "13dp",
                "id": "imgSortServicesSupportedChannels",
                "isVisible": true,
                "left": "780dp",
                "skin": "slImage",
                "src": "filter.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblServicesStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblServicesStatus",
                "isVisible": true,
                "left": "860dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": "20dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeparatorServicesViewTableHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorServicesViewTableHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorServicesViewTableHeader.setDefaultUnit(kony.flex.DP);
            flxSeparatorServicesViewTableHeader.add();
            flxServicesListHeader.add(lblServicesName, imgSortServicesName, lblServicesCode, imgSortServicesCode, lblServicesType, imgSortServicesType, lblServicesCategory, imgSortServicesCategory, lblServicesSupportedChannels, imgSortServicesSupportedChannels, lblServicesStatus, flxSeparatorServicesViewTableHeader);
            var flxServicesListContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "500dp",
                "id": "flxServicesListContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "5%",
                "skin": "slFbox",
                "top": "50dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxServicesListContent.setDefaultUnit(kony.flex.DP);
            var flxServicesListContentScroll = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "500dp",
                "id": "flxServicesListContentScroll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxServicesListContentScroll.setDefaultUnit(kony.flex.DP);
            var segServicesListContent = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblIconStatus": "Label",
                    "lblOptions": "Label",
                    "lblSeperator": ".",
                    "lblServicesCategory": "Category",
                    "lblServicesCode": "Code",
                    "lblServicesName": "Name",
                    "lblServicesStatus": "Active",
                    "lblServicesSupportedChannels": "Supported Channels",
                    "lblServicesType": "Type"
                }, {
                    "lblIconStatus": "Label",
                    "lblOptions": "Label",
                    "lblSeperator": ".",
                    "lblServicesCategory": "Category",
                    "lblServicesCode": "Code",
                    "lblServicesName": "Name",
                    "lblServicesStatus": "Active",
                    "lblServicesSupportedChannels": "Supported Channels",
                    "lblServicesType": "Type"
                }, {
                    "lblIconStatus": "Label",
                    "lblOptions": "Label",
                    "lblSeperator": ".",
                    "lblServicesCategory": "Category",
                    "lblServicesCode": "Code",
                    "lblServicesName": "Name",
                    "lblServicesStatus": "Active",
                    "lblServicesSupportedChannels": "Supported Channels",
                    "lblServicesType": "Type"
                }],
                "groupCells": false,
                "height": "300px",
                "id": "segServicesListContent",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxServices2",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxOptions": "flxOptions",
                    "flxServices2": "flxServices2",
                    "flxStatus": "flxStatus",
                    "lblIconStatus": "lblIconStatus",
                    "lblOptions": "lblOptions",
                    "lblSeperator": "lblSeperator",
                    "lblServicesCategory": "lblServicesCategory",
                    "lblServicesCode": "lblServicesCode",
                    "lblServicesName": "lblServicesName",
                    "lblServicesStatus": "lblServicesStatus",
                    "lblServicesSupportedChannels": "lblServicesSupportedChannels",
                    "lblServicesType": "lblServicesType"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServicesListContentScroll.add(segServicesListContent);
            flxServicesListContent.add(flxServicesListContentScroll);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "5%",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100px",
                "width": "23.68%",
                "zIndex": 100
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var flxEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEdit.setDefaultUnit(kony.flex.DP);
            var imgEdit = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgEdit",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "configure1x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEdit = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEdit",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEdit.add(imgEdit, lblEdit);
            var flxActivateDeactivate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxActivateDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActivateDeactivate.setDefaultUnit(kony.flex.DP);
            var imgActivateDeactivate = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgActivateDeactivate",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "deactive_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActivateDeactivate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActivateDeactivate",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActivateDeactivate.add(imgActivateDeactivate, lblActivateDeactivate);
            flxSelectOptions.add(flxEdit, flxActivateDeactivate);
            flxServicesList.add(flxServicesListHeader, flxServicesListContent, flxSelectOptions);
            flxViewServices.add(flxSearchBar, flxServicesList);
            var flxScrollViewServices = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": false,
                "id": "flxScrollViewServices",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "0dp",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxScrollViewServices.setDefaultUnit(kony.flex.DP);
            var flxListingPage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxListingPage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxListingPage.setDefaultUnit(kony.flex.DP);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "-7dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxClearSearchImage": {
                        "isVisible": false
                    },
                    "flxMenu": {
                        "isVisible": false,
                        "left": "0dp"
                    },
                    "flxSearch": {
                        "right": "35px"
                    },
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "top": "5dp"
                    },
                    "subHeader": {
                        "height": "60dp"
                    },
                    "tbxSearchBox": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.Search_by_Service_Name\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxViewServicesWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxViewServicesWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxViewServicesWrapper.setDefaultUnit(kony.flex.DP);
            var flxSegmentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSegmentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var flxHeaderAndSegmentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": false,
                "id": "flxHeaderAndSegmentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderAndSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var flxViewServicesHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxViewServicesHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0"
            }, {}, {});
            flxViewServicesHeader.setDefaultUnit(kony.flex.DP);
            var flxViewServicesHeaderName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewServicesHeaderName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "55px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxViewServicesHeaderName.setDefaultUnit(kony.flex.DP);
            var lblHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderName",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": "38px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServicesHeaderName.add(lblHeaderName, lblSortName);
            var flxViewServicesHeaderCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewServicesHeaderCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "21.98%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxViewServicesHeaderCode.setDefaultUnit(kony.flex.DP);
            var lblViewServicesHeaderCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewServicesHeaderCode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CODE\")",
                "top": 0,
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortCode = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortCode",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServicesHeaderCode.add(lblViewServicesHeaderCode, lblSortCode);
            var flxViewServicesHeaderType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewServicesHeaderType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewServicesHeaderType.setDefaultUnit(kony.flex.DP);
            var lblViewServicesHeaderType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewServicesHeaderType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TYPE\")",
                "top": 0,
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServicesHeaderType.add(lblViewServicesHeaderType);
            var flxViewServicesHeaderCategory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewServicesHeaderCategory",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "47%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "90px",
                "zIndex": 1
            }, {}, {});
            flxViewServicesHeaderCategory.setDefaultUnit(kony.flex.DP);
            var lblViewServicesHeaderCategory = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewServicesHeaderCategory",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CATEGORY\")",
                "top": 0,
                "width": "65px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortCategory = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortCategory",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServicesHeaderCategory.add(lblViewServicesHeaderCategory, lblSortCategory);
            var flxLocationsHeaderSC = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLocationsHeaderSC",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "61.73%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "165px",
                "zIndex": 1
            }, {}, {});
            flxLocationsHeaderSC.setDefaultUnit(kony.flex.DP);
            var lblViewServicesHeaderSC = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewServicesHeaderSC",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Services.Supportedchannels\")",
                "top": 0,
                "width": "140px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblFilterSC = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblFilterSC",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLocationsHeaderSC.add(lblViewServicesHeaderSC, lblFilterSC);
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "84.06%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "65px",
                "zIndex": 1
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortStatus",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxStatus.add(lblStatus, lblSortStatus);
            var lblViewServicesHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblViewServicesHeaderSeperator",
                "isVisible": true,
                "left": "35px",
                "right": "35px",
                "skin": "sknLblTableHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServicesHeader.add(flxViewServicesHeaderName, flxViewServicesHeaderCode, flxViewServicesHeaderType, flxViewServicesHeaderCategory, flxLocationsHeaderSC, flxStatus, lblViewServicesHeaderSeperator);
            var flxViewServicesSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "600px",
                "id": "flxViewServicesSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewServicesSegment.setDefaultUnit(kony.flex.DP);
            var listingSegmentClient = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "listingSegmentClient",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "flxListingSegmentWrapper": {
                        "height": "100%"
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "segListing": {
                        "height": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewServicesSegment.add(listingSegmentClient);
            var flxServiceStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxServiceStatusFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "810px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40px",
                "width": "130dp",
                "zIndex": 5
            }, {}, {});
            flxServiceStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxChechboxOuter": {
                        "zIndex": 1
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "top": "5dp",
                        "zIndex": 1
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxServiceStatusFilter.add(statusFilterMenu);
            flxHeaderAndSegmentWrapper.add(flxViewServicesHeader, flxViewServicesSegment, flxServiceStatusFilter);
            flxSegmentWrapper.add(flxHeaderAndSegmentWrapper);
            flxViewServicesWrapper.add(flxSegmentWrapper);
            flxListingPage.add(flxMainSubHeader, flxViewServicesWrapper);
            flxScrollViewServices.add(flxListingPage);
            var flxViewServiceData = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxViewServiceData",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "pagingEnabled": false,
                "right": "35px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0gd265e68164b42",
                "top": "20dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxViewServiceData.setDefaultUnit(kony.flex.DP);
            var flxViewServiceData1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewServiceData1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "CopyslFbox0dedbeed865924a",
                "top": "0",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewServiceData1.setDefaultUnit(kony.flex.DP);
            var flxViewServiceDataHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxViewServiceDataHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxViewServiceDataHeader.setDefaultUnit(kony.flex.DP);
            var lblSpecialBranch = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSpecialBranch",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFlxViewServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblServiceName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconViewKey = new kony.ui.Label({
                "centerY": "52%",
                "height": "15dp",
                "id": "lblIconViewKey",
                "isVisible": true,
                "left": "41dp",
                "skin": "sknIcon13pxGreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServiceDataHeader.add(lblSpecialBranch, lblIconViewKey, lblViewValue);
            var flxViewEditButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxViewEditButton",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffop100Border424242Radius100px",
                "top": "15px",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxViewEditButton.setDefaultUnit(kony.flex.DP);
            var lblViewEditButton = new kony.ui.Label({
                "centerX": "48%",
                "centerY": "47%",
                "id": "lblViewEditButton",
                "isVisible": true,
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgViewEditButton = new kony.ui.Image2({
                "centerY": "47%",
                "height": "15dp",
                "id": "imgViewEditButton",
                "isVisible": false,
                "left": "10dp",
                "skin": "slImage",
                "src": "img_edit_btn.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewEditButton.add(lblViewEditButton, imgViewEditButton);
            var flxViewServiceDataContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxViewServiceDataContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewServiceDataContent.setDefaultUnit(kony.flex.DP);
            var flxDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetails.setDefaultUnit(kony.flex.DP);
            var flxDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "8%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxDetailsHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDetailsHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmRolesController.DETAILS\")",
                "width": "60dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxDropdownDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 122
            }, {}, {});
            flxDropdownDetails.setDefaultUnit(kony.flex.DP);
            var lblArrowDetails = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblArrowDetails",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownDetails.add(lblArrowDetails);
            flxDetailsHeader.add(lblDetailsHeader, flxDropdownDetails);
            var flxDetailsContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsContent.setDefaultUnit(kony.flex.DP);
            var flxDetailsContent1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDetailsContent1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsContent1.setDefaultUnit(kony.flex.DP);
            var flxDetailsServiceCodeHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceCodeHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceCodeHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceCodeHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceCodeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CODE\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceCodeHeader.add(lblDetailsServiceCodeHeader);
            var flxDetailsServiceCodeContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceCodeContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceCodeContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceCodeContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceCodeContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblFullName\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceCodeContent.add(lblDetailsServiceCodeContent);
            var flxDetailsServiceDisplayNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceDisplayNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceDisplayNameHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceDisplayNameHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceDisplayNameHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DISPLAYNAME\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceDisplayNameHeader.add(lblDetailsServiceDisplayNameHeader);
            var flxDetailsServiceDisplayNameContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceDisplayNameContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceDisplayNameContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceDisplayNameContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceDisplayNameContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblDisplayName\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceDisplayNameContent.add(lblDetailsServiceDisplayNameContent);
            var flxDetailsServiceCategoryHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceCategoryHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceCategoryHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceCategoryHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceCategoryHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CATEGORY\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceCategoryHeader.add(lblDetailsServiceCategoryHeader);
            var flxDetailsServiceCategoryContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceCategoryContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceCategoryContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceCategoryContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceCategoryContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblCategory\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceCategoryContent.add(lblDetailsServiceCategoryContent);
            flxDetailsContent1.add(flxDetailsServiceCodeHeader, flxDetailsServiceCodeContent, flxDetailsServiceDisplayNameHeader, flxDetailsServiceDisplayNameContent, flxDetailsServiceCategoryHeader, flxDetailsServiceCategoryContent);
            var flxDetailsContent2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDetailsContent2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsContent2.setDefaultUnit(kony.flex.DP);
            var flxDetailsServiceChannelHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceChannelHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceChannelHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceChannelHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceChannelHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewChannelHeader\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceChannelHeader.add(lblDetailsServiceChannelHeader);
            var flxDetailsServiceChannelContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceChannelContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceChannelContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceChannelContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceChannelContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Channel\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceChannelContent.add(lblDetailsServiceChannelContent);
            var flxDetailsServiceSmsAlertHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceSmsAlertHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceSmsAlertHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceSmsAlertHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceSmsAlertHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SMSALERT\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceSmsAlertHeader.add(lblDetailsServiceSmsAlertHeader);
            var flxDetailsServiceSmsAlertContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceSmsAlertContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceSmsAlertContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceSmsAlertContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceSmsAlertContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Yes\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceSmsAlertContent.add(lblDetailsServiceSmsAlertContent);
            var flxDetailsServiceBeneficiarySmsAlertHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceBeneficiarySmsAlertHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceBeneficiarySmsAlertHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceCodeHeader0e2effa01bc3145 = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceCodeHeader0e2effa01bc3145",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.BENEFICIARYSMSALERT\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceBeneficiarySmsAlertHeader.add(lblDetailsServiceCodeHeader0e2effa01bc3145);
            var flxDetailsServiceBeneficiarySmsAlertContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceBeneficiarySmsAlertContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceBeneficiarySmsAlertContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceBeneficiarySmsAlertContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceBeneficiarySmsAlertContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Yes\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceBeneficiarySmsAlertContent.add(lblDetailsServiceBeneficiarySmsAlertContent);
            flxDetailsContent2.add(flxDetailsServiceChannelHeader, flxDetailsServiceChannelContent, flxDetailsServiceSmsAlertHeader, flxDetailsServiceSmsAlertContent, flxDetailsServiceBeneficiarySmsAlertHeader, flxDetailsServiceBeneficiarySmsAlertContent);
            var flxDetailsContent3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDetailsContent3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsContent3.setDefaultUnit(kony.flex.DP);
            var flxDetailsServiceTypeHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceTypeHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceTypeHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceTypeHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceTypeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TYPE\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceTypeHeader.add(lblDetailsServiceTypeHeader);
            var flxDetailsServiceTypeContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceTypeContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceTypeContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceTypeContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceTypeContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblType\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceTypeContent.add(lblDetailsServiceTypeContent);
            var flxDetailsServiceSmsChargeHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceSmsChargeHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceSmsChargeHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceSmsChargeHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceSmsChargeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDetailsServiceSmsChargeHeader\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceSmsChargeHeader.add(lblDetailsServiceSmsChargeHeader);
            var flxDetailsServiceSmsChargeContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceSmsChargeContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceSmsChargeContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceSmsChargeSymbol = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceSmsChargeSymbol",
                "isVisible": true,
                "left": "-7dp",
                "skin": "sknIcomoonCurrencySymbol",
                "text": "",
                "top": "0dp",
                "width": "18px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDetailsServiceSmsChargeContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceSmsChargeContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDetailsServiceBSCContent\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceSmsChargeContent.add(lblDetailsServiceSmsChargeSymbol, lblDetailsServiceSmsChargeContent);
            var flxDetailsServiceBSCHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceBSCHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceBSCHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceBSCHeader2 = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceBSCHeader2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDetailsServiceBSCHeader2\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceBSCHeader.add(lblDetailsServiceBSCHeader2);
            var flxDetailsServiceBSCContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceBSCContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "60%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceBSCContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceBSCSymbol = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceBSCSymbol",
                "isVisible": true,
                "left": "-7dp",
                "skin": "sknIcomoonCurrencySymbol",
                "text": "",
                "top": "0dp",
                "width": "18px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDetailsServiceBSCContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceBSCContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDetailsServiceBSCContent\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceBSCContent.add(lblDetailsServiceBSCSymbol, lblDetailsServiceBSCContent);
            flxDetailsContent3.add(flxDetailsServiceTypeHeader, flxDetailsServiceTypeContent, flxDetailsServiceSmsChargeHeader, flxDetailsServiceSmsChargeContent, flxDetailsServiceBSCHeader, flxDetailsServiceBSCContent);
            var flxDetailsContent4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDetailsContent4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "150dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsContent4.setDefaultUnit(kony.flex.DP);
            var flxDetailsServiceFTFHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceFTFHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceFTFHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceFTFHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceFTFHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDetailsServiceFTFHeader\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceFTFHeader.add(lblDetailsServiceFTFHeader);
            var flxDetailsServiceFTFContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceFTFContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceFTFContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceFTFContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceFTFContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Yes\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceFTFContent.add(lblDetailsServiceFTFContent);
            var flxDetailsServiceOutageMsgHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceOutageMsgHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceOutageMsgHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceOutageMsgHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceOutageMsgHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDetailsServiceOutageMsgHeader\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceOutageMsgHeader.add(lblDetailsServiceOutageMsgHeader);
            var flxDetailsServiceOutageMsgContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceOutageMsgContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceOutageMsgContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceOutageMsgContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceOutageMsgContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Yes\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceOutageMsgContent.add(lblDetailsServiceOutageMsgContent);
            var flxDetailsServiceTACRequiredHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceTACRequiredHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceTACRequiredHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceTACRequiredHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceTACRequiredHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDetailsServiceTACRequiredHeader\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceTACRequiredHeader.add(lblDetailsServiceTACRequiredHeader);
            var flxDetailsServiceTACRequiredContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceTACRequiredContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceTACRequiredContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceTACRequiredContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceTACRequiredContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Yes\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceTACRequiredContent.add(lblDetailsServiceTACRequiredContent);
            flxDetailsContent4.add(flxDetailsServiceFTFHeader, flxDetailsServiceFTFContent, flxDetailsServiceOutageMsgHeader, flxDetailsServiceOutageMsgContent, flxDetailsServiceTACRequiredHeader, flxDetailsServiceTACRequiredContent);
            var flxDetailsContent5 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDetailsContent5",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "200dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsContent5.setDefaultUnit(kony.flex.DP);
            var flxDetailsServiceTACHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceTACHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceTACHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceTACHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceTACHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDetailsServiceTACHeader\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceTACHeader.add(lblDetailsServiceTACHeader);
            var flxDetailsServiceTACContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceTACContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceTACContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceTACContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceTACContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Yes\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceTACContent.add(lblDetailsServiceTACContent);
            var flxDetailsServiceAgreementHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceAgreementHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceAgreementHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceAgreementHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceAgreementHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDetailsServiceAgreementHeader\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceAgreementHeader.add(lblDetailsServiceAgreementHeader);
            var flxDetailsServiceAgreementContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceAgreementContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceAgreementContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceAgreementContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceAgreementContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Yes\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceAgreementContent.add(lblDetailsServiceAgreementContent);
            var flxDetailsServiceSchdeduleHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceSchdeduleHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceSchdeduleHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceScheduleHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceScheduleHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDetailsServiceScheduleHeader\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceSchdeduleHeader.add(lblDetailsServiceScheduleHeader);
            var flxDetailsServiceScheduleContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsServiceScheduleContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDetailsServiceScheduleContent.setDefaultUnit(kony.flex.DP);
            var lblDetailsServiceScheduleContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblDetailsServiceScheduleContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDetailsServiceScheduleContent\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsServiceScheduleContent.add(lblDetailsServiceScheduleContent);
            flxDetailsContent5.add(flxDetailsServiceTACHeader, flxDetailsServiceTACContent, flxDetailsServiceAgreementHeader, flxDetailsServiceAgreementContent, flxDetailsServiceSchdeduleHeader, flxDetailsServiceScheduleContent);
            flxDetailsContent.add(flxDetailsContent1, flxDetailsContent2, flxDetailsContent3, flxDetailsContent4, flxDetailsContent5);
            flxDetails.add(flxDetailsHeader, flxDetailsContent);
            var flxServiceDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxServiceDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxServiceDescription.setDefaultUnit(kony.flex.DP);
            var flxServiceDescriptionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxServiceDescriptionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "165dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxServiceDescriptionHeader.setDefaultUnit(kony.flex.DP);
            var lblServiceDescriptionHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblServiceDescriptionHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SERVICE_DESCRIPTION\")",
                "width": "140dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownServiceDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxDropdownServiceDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "140dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 122
            }, {}, {});
            flxDropdownServiceDescription.setDefaultUnit(kony.flex.DP);
            var lblIconArrowDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconArrowDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownServiceDescription.add(lblIconArrowDescription);
            flxServiceDescriptionHeader.add(lblServiceDescriptionHeader, flxDropdownServiceDescription);
            var flxServiceDescriptionContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxServiceDescriptionContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "15dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxServiceDescriptionContent.setDefaultUnit(kony.flex.DP);
            var lblServiceDescriptionContent = new kony.ui.Label({
                "id": "lblServiceDescriptionContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDisplayDescriptionContent\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServiceDescriptionContent.add(lblServiceDescriptionContent);
            flxServiceDescription.add(flxServiceDescriptionHeader, flxServiceDescriptionContent);
            var flxDisplayDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDisplayDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 15,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDisplayDescription.setDefaultUnit(kony.flex.DP);
            var flxDisplayDescriptionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDisplayDescriptionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "165dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxDisplayDescriptionHeader.setDefaultUnit(kony.flex.DP);
            var lblDisplayDescriptionHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDisplayDescriptionHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.DISPLAY_DESCRIPTION\")",
                "width": "140dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownDisplayDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxDropdownDisplayDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "140dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 122
            }, {}, {});
            flxDropdownDisplayDescription.setDefaultUnit(kony.flex.DP);
            var lblArrowDisplayDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblArrowDisplayDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownDisplayDescription.add(lblArrowDisplayDescription);
            flxDisplayDescriptionHeader.add(lblDisplayDescriptionHeader, flxDropdownDisplayDescription);
            var flxDisplayDescriptionContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDisplayDescriptionContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "15dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxDisplayDescriptionContent.setDefaultUnit(kony.flex.DP);
            var lblDisplayDescriptionContent = new kony.ui.Label({
                "id": "lblDisplayDescriptionContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDisplayDescriptionContent\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDisplayDescriptionContent.add(lblDisplayDescriptionContent);
            flxDisplayDescription.add(flxDisplayDescriptionHeader, flxDisplayDescriptionContent);
            flxViewServiceDataContent.add(flxDetails, flxServiceDescription, flxDisplayDescription);
            flxViewServiceData1.add(flxViewServiceDataHeader, flxViewEditButton, flxViewServiceDataContent);
            var flxViewServiceData2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "280dp",
                "id": "flxViewServiceData2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "CopyslFbox0bd495a9d89a247",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewServiceData2.setDefaultUnit(kony.flex.DP);
            var flxViewServiceData2Header = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewServiceData2Header",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewServiceData2Header.setDefaultUnit(kony.flex.DP);
            var flxServiceData2Limits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxServiceData2Limits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {}, {});
            flxServiceData2Limits.setDefaultUnit(kony.flex.DP);
            var lblServiceData2Limits = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblServiceData2Limits",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Limits\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServiceData2Limits.add(lblServiceData2Limits);
            var flxServiceData2TransactionFee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxServiceData2TransactionFee",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {}, {});
            flxServiceData2TransactionFee.setDefaultUnit(kony.flex.DP);
            var lblServiceData2TransactionFee = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblServiceData2TransactionFee",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TRANSACTIONFEE\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServiceData2TransactionFee.add(lblServiceData2TransactionFee);
            var flxSeparatorServiceData2Header = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorServiceData2Header",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorServiceData2Header.setDefaultUnit(kony.flex.DP);
            flxSeparatorServiceData2Header.add();
            flxViewServiceData2Header.add(flxServiceData2Limits, flxServiceData2TransactionFee, flxSeparatorServiceData2Header);
            var flxViewSD2Limits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxViewSD2Limits",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSD2Limits.setDefaultUnit(kony.flex.DP);
            var flxViewSD2LimitsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewSD2LimitsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSD2LimitsHeader.setDefaultUnit(kony.flex.DP);
            var lblPeriod2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblPeriod2",
                "isVisible": true,
                "left": "30dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDefineLimitsPeriod\")",
                "top": "20dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortPeriod2 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortPeriod2",
                "isVisible": true,
                "left": "90dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLimit2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblLimit2",
                "isVisible": true,
                "left": "300dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDefineLimitsLimit\")",
                "top": "20dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortLimit2 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortLimit2",
                "isVisible": true,
                "left": "350dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeparatorLimits2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorLimits2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLimits2.setDefaultUnit(kony.flex.DP);
            flxSeparatorLimits2.add();
            flxViewSD2LimitsHeader.add(lblPeriod2, imgSortPeriod2, lblLimit2, imgSortLimit2, flxSeparatorLimits2);
            var flxViewSD2LimitsContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "150dp",
                "id": "flxViewSD2LimitsContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSD2LimitsContent.setDefaultUnit(kony.flex.DP);
            var flxViewSD2LimitsContent1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewSD2LimitsContent1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSD2LimitsContent1.setDefaultUnit(kony.flex.DP);
            var lblDaily = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblDaily",
                "isVisible": true,
                "left": "30dp",
                "skin": "CopyslLabel0cbde6b59015e49",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDaily\")",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLimit21 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblLimit21",
                "isVisible": true,
                "left": "300dp",
                "skin": "CopyslLabel0cbde6b59015e49",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblLimit21\")",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewSD2LimitsContent1.add(lblDaily, lblLimit21);
            var flxViewSD2LimitsContent2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewSD2LimitsContent2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSD2LimitsContent2.setDefaultUnit(kony.flex.DP);
            var lblWeekly = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblWeekly",
                "isVisible": true,
                "left": "30dp",
                "skin": "CopyslLabel0cbde6b59015e49",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblWeekly\")",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLimit22 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblLimit22",
                "isVisible": true,
                "left": "300dp",
                "skin": "CopyslLabel0cbde6b59015e49",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblLimit22\")",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewSD2LimitsContent2.add(lblWeekly, lblLimit22);
            var flxViewSD2LimitsContent3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewSD2LimitsContent3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSD2LimitsContent3.setDefaultUnit(kony.flex.DP);
            var lblMonthly = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblMonthly",
                "isVisible": true,
                "left": "30dp",
                "skin": "CopyslLabel0cbde6b59015e49",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblMonthly\")",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLimit23 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblLimit23",
                "isVisible": true,
                "left": "300dp",
                "skin": "CopyslLabel0cbde6b59015e49",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblLimit23\")",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewSD2LimitsContent3.add(lblMonthly, lblLimit23);
            flxViewSD2LimitsContent.add(flxViewSD2LimitsContent1, flxViewSD2LimitsContent2, flxViewSD2LimitsContent3);
            flxViewSD2Limits.add(flxViewSD2LimitsHeader, flxViewSD2LimitsContent);
            var flxViewSD2TF = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "200dp",
                "horizontalScrollIndicator": true,
                "id": "flxViewSD2TF",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "50dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSD2TF.setDefaultUnit(kony.flex.DP);
            var flxViewSD2TFHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewSD2TFHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSD2TFHeader.setDefaultUnit(kony.flex.DP);
            var lblMinimumAmount2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblMinimumAmount2",
                "isVisible": true,
                "left": "30dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblMinimumAmount\")",
                "top": "20dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortMinimumAmount2 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortMinimumAmount2",
                "isVisible": true,
                "left": "160dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaximumAmount2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblMaximumAmount2",
                "isVisible": true,
                "left": "300dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblMaximumAmount\")",
                "top": "20dp",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortMaximumAmount2 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortMaximumAmount2",
                "isVisible": true,
                "left": "435dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFee2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblFee2",
                "isVisible": true,
                "left": "570dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblFee\")",
                "top": "20dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortFee2 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortFee2",
                "isVisible": true,
                "left": "605dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeparatorTransactionFee2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorTransactionFee2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorTransactionFee2.setDefaultUnit(kony.flex.DP);
            flxSeparatorTransactionFee2.add();
            flxViewSD2TFHeader.add(lblMinimumAmount2, imgSortMinimumAmount2, lblMaximumAmount2, imgSortMaximumAmount2, lblFee2, imgSortFee2, flxSeparatorTransactionFee2);
            var flxViewSD2TFContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewSD2TFContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSD2TFContent.setDefaultUnit(kony.flex.DP);
            var segViewServiceData2TF = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblViewServiceDetailsTF1": "$1",
                    "lblViewServiceDetailsTF2": "$50000",
                    "lblViewServiceDetailsTF3": "$5"
                }, {
                    "lblViewServiceDetailsTF1": "$1",
                    "lblViewServiceDetailsTF2": "$50000",
                    "lblViewServiceDetailsTF3": "$5"
                }, {
                    "lblViewServiceDetailsTF1": "$1",
                    "lblViewServiceDetailsTF2": "$50000",
                    "lblViewServiceDetailsTF3": "$5"
                }],
                "groupCells": false,
                "id": "segViewServiceData2TF",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxViewServiceDetailsTF",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxViewServiceDetailsTF": "flxViewServiceDetailsTF",
                    "flxViewServiceDetailsTF1": "flxViewServiceDetailsTF1",
                    "flxViewServiceDetailsTF2": "flxViewServiceDetailsTF2",
                    "flxViewServiceDetailsTF3": "flxViewServiceDetailsTF3",
                    "lblViewServiceDetailsTF1": "lblViewServiceDetailsTF1",
                    "lblViewServiceDetailsTF2": "lblViewServiceDetailsTF2",
                    "lblViewServiceDetailsTF3": "lblViewServiceDetailsTF3"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewSD2TFContent.add(segViewServiceData2TF);
            flxViewSD2TF.add(flxViewSD2TFHeader, flxViewSD2TFContent);
            flxViewServiceData2.add(flxViewServiceData2Header, flxViewSD2Limits, flxViewSD2TF);
            flxViewServiceData.add(flxViewServiceData1, flxViewServiceData2);
            flxMainContent.add(flxAddService, flxViewServices, flxScrollViewServices, flxViewServiceData);
            var flxMainContent2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": false,
                "id": "flxMainContent2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "Copysknscrollflxf0ja093a1ef62648",
                "top": "120dp",
                "zIndex": 1
            }, {}, {});
            flxMainContent2.setDefaultUnit(kony.flex.DP);
            var flxVerticalTab = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxVerticalTab",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i23c43f3dedb4f",
                "width": "230dp",
                "zIndex": 1
            }, {}, {});
            flxVerticalTab.setDefaultUnit(kony.flex.DP);
            var verticalTabs = new com.adminConsole.common.verticalTabs1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "verticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgSelected1": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected2": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected3": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected4": {
                        "src": "right_arrow2x.png"
                    },
                    "verticalTabs1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxVerticalTab.add(verticalTabs);
            var flxService = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxService",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknbackGroundffffff100",
                "zIndex": 1
            }, {}, {});
            flxService.setDefaultUnit(kony.flex.DP);
            var flxServiceDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85%",
                "id": "flxServiceDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100WithoutLeftBorder",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxServiceDetails.setDefaultUnit(kony.flex.DP);
            var flxServiceDetailsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxServiceDetailsInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "20px"
            }, {}, {});
            flxServiceDetailsInner.setDefaultUnit(kony.flex.DP);
            var flxServiceDetailsContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxServiceDetailsContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0px",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxServiceDetailsContent.setDefaultUnit(kony.flex.DP);
            var flxServiceNaming = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxServiceNaming",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxServiceNaming.setDefaultUnit(kony.flex.DP);
            var flxServiceNamingHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxServiceNamingHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "130dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxServiceNamingHeader.setDefaultUnit(kony.flex.DP);
            var lblServiceNaming = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblServiceNaming",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblServiceNaming\")",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownServiceNaming = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxDropdownServiceNaming",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "110dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 122
            }, {}, {});
            flxDropdownServiceNaming.setDefaultUnit(kony.flex.DP);
            var lblArrowServiceNaming = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblArrowServiceNaming",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownServiceNaming.add(lblArrowServiceNaming);
            flxServiceNamingHeader.add(lblServiceNaming, flxDropdownServiceNaming);
            var flxSeparatorServiceNaming = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorServiceNaming",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "10dp",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorServiceNaming.setDefaultUnit(kony.flex.DP);
            flxSeparatorServiceNaming.add();
            var flxServiceNamingContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxServiceNamingContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxServiceNamingContent.setDefaultUnit(kony.flex.DP);
            var flxServiceType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxServiceType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxServiceType.setDefaultUnit(kony.flex.DP);
            var lblServiceType = new kony.ui.Label({
                "height": "20dp",
                "id": "lblServiceType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblServiceType\")",
                "top": "0dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rbServiceType = new kony.ui.RadioButtonGroup({
                "height": "25dp",
                "id": "rbServiceType",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["SER_TYPE_TRNS", "Transactional"],
                    ["SER_TYPE_NONTRANS", "Non-Transactional"]
                ],
                "selectedKey": "SER_TYPE_NONTRANS",
                "selectedKeyValue": ["SER_TYPE_NONTRANS", "Non-Transactional"],
                "skin": "sknRadioGrp484b5213px",
                "top": "5dp",
                "width": "400dp",
                "zIndex": 1
            }, {
                "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoServiceTypeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoServiceTypeError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxNoServiceTypeError.setDefaultUnit(kony.flex.DP);
            var lblNoServiceTypeErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoServiceTypeErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoServiceTypeError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoServiceTypeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoServiceTypeError.add(lblNoServiceTypeErrorIcon, lblNoServiceTypeError);
            flxServiceType.add(lblServiceType, rbServiceType, flxNoServiceTypeError);
            var flxNameCodeGrp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxNameCodeGrp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxNameCodeGrp.setDefaultUnit(kony.flex.DP);
            var lblServiceCode = new kony.ui.Label({
                "height": "20dp",
                "id": "lblServiceCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblServiceCode\")",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxServiceCode = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "tbxServiceCode",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "placeholder": "Service Code",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "195dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoServiceCodeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoServiceCodeError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxNoServiceCodeError.setDefaultUnit(kony.flex.DP);
            var lblNoServiceCodeErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoServiceCodeErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoServiceCodeError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoServiceCodeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceCodeError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoServiceCodeError.add(lblNoServiceCodeErrorIcon, lblNoServiceCodeError);
            var lblServiceName = new kony.ui.Label({
                "height": "20dp",
                "id": "lblServiceName",
                "isVisible": true,
                "left": "270dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblServiceName\")",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxServiceName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "tbxServiceName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "270dp",
                "maxTextLength": 60,
                "placeholder": "Service Name",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "195dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoServiceNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoServiceNameError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "270dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxNoServiceNameError.setDefaultUnit(kony.flex.DP);
            var lblNoServiceNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoServiceNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoServiceNameError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoServiceNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Service_Name_cannot_be_empty\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoServiceNameError.add(lblNoServiceNameErrorIcon, lblNoServiceNameError);
            var lblServiceNameCount = new kony.ui.Label({
                "id": "lblServiceNameCount",
                "isVisible": false,
                "left": "434px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblServiceDisplayNameCount\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNameCodeGrp.add(lblServiceCode, tbxServiceCode, flxNoServiceCodeError, lblServiceName, tbxServiceName, flxNoServiceNameError, lblServiceNameCount);
            var flxServiceDesc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "130dp",
                "id": "flxServiceDesc",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%"
            }, {}, {});
            flxServiceDesc.setDefaultUnit(kony.flex.DP);
            var lblServiceDescription = new kony.ui.Label({
                "height": "20dp",
                "id": "lblServiceDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertDescription\")",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtareaServiceDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "85dp",
                "id": "txtareaServiceDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 100,
                "numberOfVisibleLines": 3,
                "placeholder": "Description",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoServiceDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoServiceDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "110dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxNoServiceDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoServiceDescriptionErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoServiceDescriptionErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoServiceDescriptionError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoServiceDescriptionError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceDescriptionError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoServiceDescriptionError.add(lblNoServiceDescriptionErrorIcon, lblNoServiceDescriptionError);
            var lblDescriptionCount = new kony.ui.Label({
                "id": "lblDescriptionCount",
                "isVisible": false,
                "right": "81px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblCountDisplayName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServiceDesc.add(lblServiceDescription, txtareaServiceDescription, flxNoServiceDescriptionError, lblDescriptionCount);
            var flxServDisplayName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxServDisplayName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%"
            }, {}, {});
            flxServDisplayName.setDefaultUnit(kony.flex.DP);
            var lblDisplayName = new kony.ui.Label({
                "height": "20dp",
                "id": "lblDisplayName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblDisplayName\")",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxDisplayName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "tbxDisplayName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 60,
                "placeholder": "Display Name",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "25%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoDisplayNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoDisplayNameError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxNoDisplayNameError.setDefaultUnit(kony.flex.DP);
            var lblNoDisplayNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoDisplayNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoDisplayNameError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoDisplayNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoDisplayNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoDisplayNameError.add(lblNoDisplayNameErrorIcon, lblNoDisplayNameError);
            var lblServiceDisplayNameCount = new kony.ui.Label({
                "id": "lblServiceDisplayNameCount",
                "isVisible": false,
                "left": "20%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblServiceDisplayNameCount\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServDisplayName.add(lblDisplayName, tbxDisplayName, flxNoDisplayNameError, lblServiceDisplayNameCount);
            var flxDisplayDesc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "130dp",
                "id": "flxDisplayDesc",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDisplayDesc.setDefaultUnit(kony.flex.DP);
            var lblDisplayDescription = new kony.ui.Label({
                "height": "20dp",
                "id": "lblDisplayDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDisplayDescription\")",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtareaDisplayDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "85dp",
                "id": "txtareaDisplayDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 100,
                "numberOfVisibleLines": 3,
                "placeholder": "Display Description",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoDisplayDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoDisplayDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "110dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxNoDisplayDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoDisplayDescriptionErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoDisplayDescriptionErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoDisplayDescriptionError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoDisplayDescriptionError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoDisplayDescriptionError\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoDisplayDescriptionError.add(lblNoDisplayDescriptionErrorIcon, lblNoDisplayDescriptionError);
            var lblDisplayDescriptionCount = new kony.ui.Label({
                "id": "lblDisplayDescriptionCount",
                "isVisible": false,
                "right": "81px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblCountDisplayName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDisplayDesc.add(lblDisplayDescription, txtareaDisplayDescription, flxNoDisplayDescriptionError, lblDisplayDescriptionCount);
            flxServiceNamingContent.add(flxServiceType, flxNameCodeGrp, flxServiceDesc, flxServDisplayName, flxDisplayDesc);
            flxServiceNaming.add(flxServiceNamingHeader, flxSeparatorServiceNaming, flxServiceNamingContent);
            var flxSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSettings",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSettings.setDefaultUnit(kony.flex.DP);
            var flxSettingsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSettingsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "90dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxSettingsHeader.setDefaultUnit(kony.flex.DP);
            var lblSettings = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSettings",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SETTINGS\")",
                "width": "70dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxDropdownSettings",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "70dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 122
            }, {}, {});
            flxDropdownSettings.setDefaultUnit(kony.flex.DP);
            var imageArrowSettings = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imageArrowSettings",
                "isVisible": false,
                "skin": "slImage",
                "src": "arrowdown_1x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblArrowSeetings = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblArrowSeetings",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownSettings.add(imageArrowSettings, lblArrowSeetings);
            flxSettingsHeader.add(lblSettings, flxDropdownSettings);
            var flxSeparatorSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorSettings",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "31dp",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorSettings.setDefaultUnit(kony.flex.DP);
            flxSeparatorSettings.add();
            var flxSettingsContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "260dp",
                "id": "flxSettingsContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSettingsContent.setDefaultUnit(kony.flex.DP);
            var lblCategory = new kony.ui.Label({
                "height": "20dp",
                "id": "lblCategory",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblCategory\")",
                "top": "20dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rbCategory = new kony.ui.RadioButtonGroup({
                "height": "25dp",
                "id": "rbCategory",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["categoryKey1", "Payment"],
                    ["categoryKey2", "Transfer"],
                    ["categoryKey3", "Deposit"]
                ],
                "selectedKey": "categoryKey1",
                "selectedKeyValue": ["categoryKey1", "Payment"],
                "skin": "sknRadioGrp484b5213px",
                "top": "45dp",
                "width": "400dp",
                "zIndex": 1
            }, {
                "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCategoryError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCategoryError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxNoCategoryError.setDefaultUnit(kony.flex.DP);
            var lblNoCategoryErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCategoryErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCategoryError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCategoryError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoCategoryError\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCategoryError.add(lblNoCategoryErrorIcon, lblNoCategoryError);
            var lblServiceStatus = new kony.ui.Label({
                "height": "20dp",
                "id": "lblServiceStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblServiceStatus\")",
                "top": "95dp",
                "width": "140dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxServiceStatusSwitch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxServiceStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "135dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "92dp",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxServiceStatusSwitch.setDefaultUnit(kony.flex.DP);
            var customServiceStatusSwitch = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "customServiceStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "customSwitch": {
                        "height": "100%",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxServiceStatusSwitch.add(customServiceStatusSwitch);
            var lblServiceChannel = new kony.ui.Label({
                "id": "lblServiceChannel",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.ServiceChannel\")",
                "top": "130dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxServiceChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxServiceChannel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "150dp",
                "zIndex": 1
            }, {}, {});
            flxServiceChannel.setDefaultUnit(kony.flex.DP);
            var checkBoxServiceChannel = new kony.ui.CheckBoxGroup({
                "centerX": "50%",
                "height": "100%",
                "id": "checkBoxServiceChannel",
                "isVisible": true,
                "masterData": [
                    ["cbsc1", "Internet Banking"],
                    ["cbsc2", "Mobile Banking"]
                ],
                "skin": "skncheckBox484513px",
                "width": "100%",
                "zIndex": 1
            }, {
                "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_HORIZONTAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServiceChannel.add(checkBoxServiceChannel);
            var flxNoServiceChannelError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoServiceChannelError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "180dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxNoServiceChannelError.setDefaultUnit(kony.flex.DP);
            var lblNoServiceChannelErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoServiceChannelErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoServiceChannelError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoServiceChannelError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceChannelError\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoServiceChannelError.add(lblNoServiceChannelErrorIcon, lblNoServiceChannelError);
            var lblSchedule = new kony.ui.Label({
                "height": "20dp",
                "id": "lblSchedule",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Schedule\")",
                "top": "205dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxScheduleClick = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxScheduleClick",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "230dp",
                "width": "180dp",
                "zIndex": 1
            }, {}, {});
            flxScheduleClick.setDefaultUnit(kony.flex.DP);
            var lblScheduleClick = new kony.ui.Label({
                "height": "100%",
                "id": "lblScheduleClick",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblScheduleClick",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnSelectScheduler\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxScheduleClick.add(lblScheduleClick);
            flxSettingsContent.add(lblCategory, rbCategory, flxNoCategoryError, lblServiceStatus, flxServiceStatusSwitch, lblServiceChannel, flxServiceChannel, flxNoServiceChannelError, lblSchedule, flxScheduleClick);
            flxSettings.add(flxSettingsHeader, flxSeparatorSettings, flxSettingsContent);
            var flxSmsAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSmsAlert.setDefaultUnit(kony.flex.DP);
            var flxSmsAlertHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxSmsAlertHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSmsAlertHeader.setDefaultUnit(kony.flex.DP);
            var flxHeadingSmsAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxHeadingSmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100dp",
                "zIndex": 111
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxHeadingSmsAlert.setDefaultUnit(kony.flex.DP);
            var lblSmsAlert = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSmsAlert",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SMSALERT\")",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownSmsAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxDropdownSmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 111
            }, {}, {});
            flxDropdownSmsAlert.setDefaultUnit(kony.flex.DP);
            var imageArrowSmsAlert = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imageArrowSmsAlert",
                "isVisible": false,
                "skin": "slImage",
                "src": "arrowdown_1x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblArrowSmsAlert = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblArrowSmsAlert",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownSmsAlert.add(imageArrowSmsAlert, lblArrowSmsAlert);
            flxHeadingSmsAlert.add(lblSmsAlert, flxDropdownSmsAlert);
            var flxSwitchSmsAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxSwitchSmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "25dp",
                "skin": "slFbox",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxSwitchSmsAlert.setDefaultUnit(kony.flex.DP);
            var customSwitchSmsAlert = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "customSwitchSmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSwitchSmsAlert.add(customSwitchSmsAlert);
            var flxSeparatorSmsAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorSmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorSmsAlert.setDefaultUnit(kony.flex.DP);
            flxSeparatorSmsAlert.add();
            flxSmsAlertHeader.add(flxHeadingSmsAlert, flxSwitchSmsAlert, flxSeparatorSmsAlert);
            var flxSmsAlertContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxSmsAlertContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSmsAlertContent.setDefaultUnit(kony.flex.DP);
            var lblSmsAlertCharge = new kony.ui.Label({
                "height": "20dp",
                "id": "lblSmsAlertCharge",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Charge\")",
                "top": "20dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxChargeTextbxContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxChargeTextbxContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "45dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxChargeTextbxContainer.setDefaultUnit(kony.flex.DP);
            var flxSmsAlertDollar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSmsAlertDollar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxSmsAlertDollar.setDefaultUnit(kony.flex.DP);
            var lblSmsAlertDollar = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblSmsAlertDollar",
                "isVisible": true,
                "left": "0px",
                "skin": "sknIcomoonCurrencySymbol",
                "text": "",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSmsAlertDollar.add(lblSmsAlertDollar);
            var tbxSmsAlertCharge = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "100%",
                "id": "tbxSmsAlertCharge",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "40dp",
                "placeholder": "1.00",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0dp",
                "width": "158dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxChargeTextbxContainer.add(flxSmsAlertDollar, tbxSmsAlertCharge);
            var flxNoSmsAlertChargeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoSmsAlertChargeError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "85dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxNoSmsAlertChargeError.setDefaultUnit(kony.flex.DP);
            var lblNoSmsAlertChargeErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSmsAlertChargeErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoSmsAlertChargeError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSmsAlertChargeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoSmsAlertChargelError\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoSmsAlertChargeError.add(lblNoSmsAlertChargeErrorIcon, lblNoSmsAlertChargeError);
            flxSmsAlertContent.add(lblSmsAlertCharge, flxChargeTextbxContainer, flxNoSmsAlertChargeError);
            flxSmsAlert.add(flxSmsAlertHeader, flxSmsAlertContent);
            var flxBeneficiarySmsAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBeneficiarySmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBeneficiarySmsAlert.setDefaultUnit(kony.flex.DP);
            var flxBeneficiarySmsAlertHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxBeneficiarySmsAlertHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBeneficiarySmsAlertHeader.setDefaultUnit(kony.flex.DP);
            var flxHeadingBenificiaryAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxHeadingBenificiaryAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "170dp",
                "zIndex": 111
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxHeadingBenificiaryAlert.setDefaultUnit(kony.flex.DP);
            var lblBeneficiarySmsAlert = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBeneficiarySmsAlert",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.BENEFICIARYSMSALERT\")",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownBeneficiarySmsAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxDropdownBeneficiarySmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "150dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25px",
                "zIndex": 111
            }, {}, {});
            flxDropdownBeneficiarySmsAlert.setDefaultUnit(kony.flex.DP);
            var imageArrowBeneficiarySmsAlert = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15px",
                "id": "imageArrowBeneficiarySmsAlert",
                "isVisible": false,
                "skin": "slImage",
                "src": "arrowdown_1x.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblArrowBeneficiarySmsAlert = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblArrowBeneficiarySmsAlert",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownBeneficiarySmsAlert.add(imageArrowBeneficiarySmsAlert, lblArrowBeneficiarySmsAlert);
            flxHeadingBenificiaryAlert.add(lblBeneficiarySmsAlert, flxDropdownBeneficiarySmsAlert);
            var flxSwitchBeneficiarySmsAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxSwitchBeneficiarySmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "25dp",
                "skin": "slFbox",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxSwitchBeneficiarySmsAlert.setDefaultUnit(kony.flex.DP);
            var customSwitchBeneficiarySmsAlert = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "customSwitchBeneficiarySmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSwitchBeneficiarySmsAlert.add(customSwitchBeneficiarySmsAlert);
            var flxSeparatorBeneficiarySmsAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorBeneficiarySmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorBeneficiarySmsAlert.setDefaultUnit(kony.flex.DP);
            flxSeparatorBeneficiarySmsAlert.add();
            flxBeneficiarySmsAlertHeader.add(flxHeadingBenificiaryAlert, flxSwitchBeneficiarySmsAlert, flxSeparatorBeneficiarySmsAlert);
            var flxBeneficiarySmsAlertContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxBeneficiarySmsAlertContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBeneficiarySmsAlertContent.setDefaultUnit(kony.flex.DP);
            var lblBeneficiarySmsAlertCharge = new kony.ui.Label({
                "id": "lblBeneficiarySmsAlertCharge",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Charge\")",
                "top": "20dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxBenfChargeTextbxContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxBenfChargeTextbxContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "45dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxBenfChargeTextbxContainer.setDefaultUnit(kony.flex.DP);
            var flxBeneficiarySmsAlertDollar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxBeneficiarySmsAlertDollar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxBeneficiarySmsAlertDollar.setDefaultUnit(kony.flex.DP);
            var lblBeneficiarySmsAlertDollar = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblBeneficiarySmsAlertDollar",
                "isVisible": true,
                "left": "0px",
                "skin": "sknIcomoonCurrencySymbol",
                "text": "",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBeneficiarySmsAlertDollar.add(lblBeneficiarySmsAlertDollar);
            var tbxBeneficiarySmsAlertCharge = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "tbxBeneficiarySmsAlertCharge",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "40dp",
                "placeholder": "1.00",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0dp",
                "width": "158dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxBenfChargeTextbxContainer.add(flxBeneficiarySmsAlertDollar, tbxBeneficiarySmsAlertCharge);
            var flxNoBeneficiarySmsAlertChargeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoBeneficiarySmsAlertChargeError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "85dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxNoBeneficiarySmsAlertChargeError.setDefaultUnit(kony.flex.DP);
            var lblNoBeneficiarySmsAlertChargeErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBeneficiarySmsAlertChargeErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoBeneficiarySmsAlertChargeError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBeneficiarySmsAlertChargeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoBeneficiarySmsAlertCharge\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBeneficiarySmsAlertChargeError.add(lblNoBeneficiarySmsAlertChargeErrorIcon, lblNoBeneficiarySmsAlertChargeError);
            flxBeneficiarySmsAlertContent.add(lblBeneficiarySmsAlertCharge, flxBenfChargeTextbxContainer, flxNoBeneficiarySmsAlertChargeError);
            flxBeneficiarySmsAlert.add(flxBeneficiarySmsAlertHeader, flxBeneficiarySmsAlertContent);
            var flxOperations = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOperations",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOperations.setDefaultUnit(kony.flex.DP);
            var flxOperationsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxOperationsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOperationsHeader.setDefaultUnit(kony.flex.DP);
            var flxHeadingOperation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxHeadingOperation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100dp",
                "zIndex": 111
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxHeadingOperation.setDefaultUnit(kony.flex.DP);
            var lblOperations = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOperations",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.OPERATIONS\")",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownOperations = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxDropdownOperations",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25px",
                "zIndex": 111
            }, {}, {});
            flxDropdownOperations.setDefaultUnit(kony.flex.DP);
            var imageArrowOperations = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15px",
                "id": "imageArrowOperations",
                "isVisible": false,
                "left": 0,
                "skin": "slImage",
                "src": "arrowdown_1x.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblArrowOperations = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblArrowOperations",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownOperations.add(imageArrowOperations, lblArrowOperations);
            flxHeadingOperation.add(lblOperations, flxDropdownOperations);
            var flxSwitchOperations = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxSwitchOperations",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "25dp",
                "skin": "slFbox",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxSwitchOperations.setDefaultUnit(kony.flex.DP);
            var customSwitchOperations = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "customSwitchOperations",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "switchToggle": {
                        "right": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSwitchOperations.add(customSwitchOperations);
            var flxSeparatorOperations = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorOperations",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorOperations.setDefaultUnit(kony.flex.DP);
            flxSeparatorOperations.add();
            flxOperationsHeader.add(flxHeadingOperation, flxSwitchOperations, flxSeparatorOperations);
            var flxOperationsContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "102dp",
                "id": "flxOperationsContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOperationsContent.setDefaultUnit(kony.flex.DP);
            var lblFutureTransactionFlag = new kony.ui.Label({
                "id": "lblFutureTransactionFlag",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.FutureTransactionFlag\")",
                "top": "20dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchFutureTransactionFlag = new kony.ui.Switch({
                "height": "25px",
                "id": "switchFutureTransactionFlag",
                "isVisible": true,
                "left": "150dp",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "17dp",
                "width": "38px",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOutageMessage = new kony.ui.Label({
                "id": "lblOutageMessage",
                "isVisible": true,
                "left": "260dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.OutageMessage\")",
                "top": "20dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchOutageMessage = new kony.ui.Switch({
                "height": "25px",
                "id": "switchOutageMessage",
                "isVisible": true,
                "left": "380dp",
                "selectedIndex": 1,
                "skin": "sknSwitchServiceManagement",
                "top": "17dp",
                "width": "38px",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTacRequired = new kony.ui.Label({
                "id": "lblTacRequired",
                "isVisible": true,
                "left": "490dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TACRequired\")",
                "top": "20dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchTacRequired = new kony.ui.Switch({
                "height": "25px",
                "id": "switchTacRequired",
                "isVisible": true,
                "left": "600dp",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "17dp",
                "width": "38px",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTermsAndConditions = new kony.ui.Label({
                "id": "lblTermsAndConditions",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TermsAndConditions\")",
                "top": "70dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchTermsAndConditions = new kony.ui.Switch({
                "height": "25px",
                "id": "switchTermsAndConditions",
                "isVisible": true,
                "left": "150dp",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "67dp",
                "width": "38px",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAgreement = new kony.ui.Label({
                "id": "lblAgreement",
                "isVisible": true,
                "left": "260dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Agreement\")",
                "top": "70dp",
                "width": "70dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchAgreement = new kony.ui.Switch({
                "height": "25px",
                "id": "switchAgreement",
                "isVisible": true,
                "left": "380dp",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "67dp",
                "width": "38px",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOperationsContent.add(lblFutureTransactionFlag, switchFutureTransactionFlag, lblOutageMessage, switchOutageMessage, lblTacRequired, switchTacRequired, lblTermsAndConditions, switchTermsAndConditions, lblAgreement, switchAgreement);
            flxOperations.add(flxOperationsHeader, flxOperationsContent);
            flxServiceDetailsContent.add(flxServiceNaming, flxSettings, flxSmsAlert, flxBeneficiarySmsAlert, flxOperations);
            flxServiceDetailsInner.add(flxServiceDetailsContent);
            flxServiceDetails.add(flxServiceDetailsInner);
            var flxLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85%",
                "id": "flxLimits",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b62626873b8d4e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLimits.setDefaultUnit(kony.flex.DP);
            var flxLimitsContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "80px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxLimitsContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "20px",
                "verticalScrollIndicator": true,
                "width": "93.90%",
                "zIndex": 1
            }, {}, {});
            flxLimitsContent.setDefaultUnit(kony.flex.DP);
            var flxDefineLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDefineLimits",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDefineLimits.setDefaultUnit(kony.flex.DP);
            var flxDefineLimitsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDefineLimitsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDefineLimitsHeader.setDefaultUnit(kony.flex.DP);
            var lblDefineLimits = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblDefineLimits",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDefineLimits\")",
                "top": "20dp",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnDefineLimitsAdd = new kony.ui.Button({
                "centerY": "50%",
                "height": "20dp",
                "id": "btnDefineLimitsAdd",
                "isVisible": true,
                "left": "700dp",
                "skin": "sknBtnLimitsAddDisabled",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
                "top": "20dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeparatorDefineLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorDefineLimits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorDefineLimits.setDefaultUnit(kony.flex.DP);
            flxSeparatorDefineLimits.add();
            flxDefineLimitsHeader.add(lblDefineLimits, btnDefineLimitsAdd, flxSeparatorDefineLimits);
            var flxDefineLimitsHeader2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDefineLimitsHeader2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDefineLimitsHeader2.setDefaultUnit(kony.flex.DP);
            var lblDefineLimitsPeriod = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblDefineLimitsPeriod",
                "isVisible": true,
                "left": "30dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDefineLimitsPeriod\")",
                "top": "20dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortPeriod = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortPeriod",
                "isVisible": false,
                "left": "90dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconSortPeriod = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblIconSortPeriod",
                "isVisible": true,
                "left": "80dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDefineLimitsLimit = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblDefineLimitsLimit",
                "isVisible": true,
                "left": "300dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDefineLimitsLimit\")",
                "top": "20dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortLimit = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortLimit",
                "isVisible": false,
                "left": "350dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeparatorDefineLimits2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorDefineLimits2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorDefineLimits2.setDefaultUnit(kony.flex.DP);
            flxSeparatorDefineLimits2.add();
            var lblIconSortLimit = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconSortLimit",
                "isVisible": true,
                "left": "337dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDefineLimitsHeader2.add(lblDefineLimitsPeriod, imgSortPeriod, lblIconSortPeriod, lblDefineLimitsLimit, imgSortLimit, flxSeparatorDefineLimits2, lblIconSortLimit);
            var flxDefineLimitsContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDefineLimitsContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDefineLimitsContent.setDefaultUnit(kony.flex.DP);
            var flxDefineLimitsContent1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDefineLimitsContent1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDefineLimitsContent1.setDefaultUnit(kony.flex.DP);
            var lbxPeriod1 = new kony.ui.ListBox({
                "centerY": "50.00%",
                "height": "40dp",
                "id": "lbxPeriod1",
                "isVisible": true,
                "left": "30dp",
                "masterData": [
                    ["Select Period", "Select Period"],
                    ["Monthly", "Monthly"]
                ],
                "skin": "CopyslListBox0i91c872410a74f",
                "top": "20px",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxLimitDollar1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLimitDollar1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "300dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0h3f0e7512b874a",
                "top": "20dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxLimitDollar1.setDefaultUnit(kony.flex.DP);
            var lblLimitDollar1 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblLimitDollar1",
                "isVisible": true,
                "left": 0,
                "skin": "CopyslLabel0fc0562b7640a4c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblDollar\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLimitDollar1.add(lblLimitDollar1);
            var tbxLimit1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "40dp",
                "id": "tbxLimit1",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
                "left": "340dp",
                "secureTextEntry": false,
                "skin": "CopyslTextBox0b4bc21adeecd47",
                "text": "0.00",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "width": "150dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxDefineLimitsContent1.add(lbxPeriod1, flxLimitDollar1, tbxLimit1);
            var lblNoLimitDefineError1 = new kony.ui.Label({
                "id": "lblNoLimitDefineError1",
                "isVisible": false,
                "left": "30dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoLimitDefineError1\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDefineLimitsContent2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDefineLimitsContent2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDefineLimitsContent2.setDefaultUnit(kony.flex.DP);
            var lbxPeriod2 = new kony.ui.ListBox({
                "centerY": "50%",
                "height": "40dp",
                "id": "lbxPeriod2",
                "isVisible": true,
                "left": "30dp",
                "masterData": [
                    ["Select Period", "Select Period"],
                    ["Weekly", "Weekly"],
                    ["Monthly", "Monthly"]
                ],
                "skin": "CopyslListBox0i91c872410a74f",
                "top": "20dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxLimitDollar2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLimitDollar2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "300dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0h3f0e7512b874a",
                "top": "20dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxLimitDollar2.setDefaultUnit(kony.flex.DP);
            var lblLimitDollar2 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblLimitDollar2",
                "isVisible": true,
                "left": 0,
                "skin": "CopyslLabel0fc0562b7640a4c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblDollar\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLimitDollar2.add(lblLimitDollar2);
            var tbxLimit2 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "40dp",
                "id": "tbxLimit2",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
                "left": "340dp",
                "secureTextEntry": false,
                "skin": "CopyslTextBox0b4bc21adeecd47",
                "text": "0.00",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "width": "150dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxDeleteLimits2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxDeleteLimits2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 1
            }, {}, {});
            flxDeleteLimits2.setDefaultUnit(kony.flex.DP);
            var imgDeleteLimits2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgDeleteLimits2",
                "isVisible": false,
                "skin": "slImage",
                "src": "delete_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconDeleteLimits2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblIconDeleteLimits2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeleteLimits2.add(imgDeleteLimits2, lblIconDeleteLimits2);
            flxDefineLimitsContent2.add(lbxPeriod2, flxLimitDollar2, tbxLimit2, flxDeleteLimits2);
            var lblNoLimitDefineError2 = new kony.ui.Label({
                "id": "lblNoLimitDefineError2",
                "isVisible": false,
                "left": "30dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoLimitDefineError1\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDefineLimitsContent3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDefineLimitsContent3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDefineLimitsContent3.setDefaultUnit(kony.flex.DP);
            var lbxPeriod3 = new kony.ui.ListBox({
                "centerY": "50%",
                "height": "40dp",
                "id": "lbxPeriod3",
                "isVisible": true,
                "left": "30dp",
                "masterData": [
                    ["Select Period", "Select Period"],
                    ["Daily", "Daily"],
                    ["Weekly", "Weekly"],
                    ["Monthly", "Monthly"]
                ],
                "skin": "CopyslListBox0i91c872410a74f",
                "top": "20dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxLimitDollar3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLimitDollar3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "300dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0h3f0e7512b874a",
                "top": "20dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxLimitDollar3.setDefaultUnit(kony.flex.DP);
            var lblLimitDollar3 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblLimitDollar3",
                "isVisible": true,
                "left": 0,
                "skin": "CopyslLabel0fc0562b7640a4c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblDollar\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLimitDollar3.add(lblLimitDollar3);
            var tbxLimit3 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "40dp",
                "id": "tbxLimit3",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
                "left": "340dp",
                "secureTextEntry": false,
                "skin": "CopyslTextBox0b4bc21adeecd47",
                "text": "0.00",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "width": "150dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxDeleteLimits3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxDeleteLimits3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 1
            }, {}, {});
            flxDeleteLimits3.setDefaultUnit(kony.flex.DP);
            var imgDeleteLimits3 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgDeleteLimits3",
                "isVisible": false,
                "skin": "slImage",
                "src": "delete_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconDeleteLimit3 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblIconDeleteLimit3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeleteLimits3.add(imgDeleteLimits3, lblIconDeleteLimit3);
            flxDefineLimitsContent3.add(lbxPeriod3, flxLimitDollar3, tbxLimit3, flxDeleteLimits3);
            var lblNoLimitDefineError3 = new kony.ui.Label({
                "id": "lblNoLimitDefineError3",
                "isVisible": false,
                "left": "30dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoLimitDefineError1\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDefineLimitsContent.add(flxDefineLimitsContent1, lblNoLimitDefineError1, flxDefineLimitsContent2, lblNoLimitDefineError2, flxDefineLimitsContent3, lblNoLimitDefineError3);
            flxDefineLimits.add(flxDefineLimitsHeader, flxDefineLimitsHeader2, flxDefineLimitsContent);
            flxLimitsContent.add(flxDefineLimits);
            flxLimits.add(flxLimitsContent);
            var flxTransactionFee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85%",
                "id": "flxTransactionFee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "CopyslFbox0a19652871f6d42",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactionFee.setDefaultUnit(kony.flex.DP);
            var flxTransactionFeeContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "80px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxTransactionFeeContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "20px",
                "verticalScrollIndicator": true,
                "width": "93.90%",
                "zIndex": 1
            }, {}, {});
            flxTransactionFeeContent.setDefaultUnit(kony.flex.DP);
            var flxDefineFeeRange = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDefineFeeRange",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDefineFeeRange.setDefaultUnit(kony.flex.DP);
            var flxDefineFeeRangeHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDefineFeeRangeHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDefineFeeRangeHeader.setDefaultUnit(kony.flex.DP);
            var lblDefineFeeRange = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblDefineFeeRange",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDefineFeeRange\")",
                "top": "20dp",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnDefineFeeRangeAdd = new kony.ui.Button({
                "centerY": "50%",
                "height": "20dp",
                "id": "btnDefineFeeRangeAdd",
                "isVisible": true,
                "left": "688dp",
                "skin": "CopyslButtonGlossBlue0d933dcec26fd45",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
                "top": "20dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeparatorDefineFeeRange = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorDefineFeeRange",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorDefineFeeRange.setDefaultUnit(kony.flex.DP);
            flxSeparatorDefineFeeRange.add();
            flxDefineFeeRangeHeader.add(lblDefineFeeRange, btnDefineFeeRangeAdd, flxSeparatorDefineFeeRange);
            var flxDefineFeeRangeHeader2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDefineFeeRangeHeader2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDefineFeeRangeHeader2.setDefaultUnit(kony.flex.DP);
            var lblMinimumAmount = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblMinimumAmount",
                "isVisible": true,
                "left": "30dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblMinimumAmount\")",
                "top": "20dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortMinimumAmount = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortMinimumAmount",
                "isVisible": false,
                "left": "160dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconSortMinimumAmount = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblIconSortMinimumAmount",
                "isVisible": true,
                "left": "154dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaximumAmount = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblMaximumAmount",
                "isVisible": true,
                "left": "300dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblMaximumAmount\")",
                "top": "20dp",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconSortMaximumAmount = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblIconSortMaximumAmount",
                "isVisible": true,
                "left": "425dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortMaximumAmount = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortMaximumAmount",
                "isVisible": false,
                "left": "435dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFee = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblFee",
                "isVisible": true,
                "left": "570dp",
                "skin": "sknlblSubHeaderNew0i261daa6d62d4b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblFee\")",
                "top": "20dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconSortFee = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblIconSortFee",
                "isVisible": true,
                "left": "595dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortFee = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortFee",
                "isVisible": false,
                "left": "605dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "20dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeparatorDefineFeeRange2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorDefineFeeRange2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorDefineFeeRange2.setDefaultUnit(kony.flex.DP);
            flxSeparatorDefineFeeRange2.add();
            flxDefineFeeRangeHeader2.add(lblMinimumAmount, imgSortMinimumAmount, lblIconSortMinimumAmount, lblMaximumAmount, lblIconSortMaximumAmount, imgSortMaximumAmount, lblFee, lblIconSortFee, imgSortFee, flxSeparatorDefineFeeRange2);
            var flxDefineFeeRangeContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDefineFeeRangeContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDefineFeeRangeContent.setDefaultUnit(kony.flex.DP);
            var segDefineFeeRangeContent = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblFeeDollar": "$",
                    "lblMaximumAmountDollar": "$",
                    "lblMinimumAmountDollar": "$",
                    "tbxFee": {
                        "placeholder": "",
                        "text": "0.00"
                    },
                    "tbxMaximumAmount": {
                        "placeholder": "",
                        "text": "0.00"
                    },
                    "tbxMinimumAmount": {
                        "placeholder": "",
                        "text": "0.00"
                    }
                }, {
                    "lblFeeDollar": "$",
                    "lblMaximumAmountDollar": "$",
                    "lblMinimumAmountDollar": "$",
                    "tbxFee": {
                        "placeholder": "",
                        "text": "0.00"
                    },
                    "tbxMaximumAmount": {
                        "placeholder": "",
                        "text": "0.00"
                    },
                    "tbxMinimumAmount": {
                        "placeholder": "",
                        "text": "0.00"
                    }
                }, {
                    "lblFeeDollar": "$",
                    "lblMaximumAmountDollar": "$",
                    "lblMinimumAmountDollar": "$",
                    "tbxFee": {
                        "placeholder": "",
                        "text": "0.00"
                    },
                    "tbxMaximumAmount": {
                        "placeholder": "",
                        "text": "0.00"
                    },
                    "tbxMinimumAmount": {
                        "placeholder": "",
                        "text": "0.00"
                    }
                }],
                "groupCells": false,
                "id": "segDefineFeeRangeContent",
                "isVisible": false,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxSegDefineFeeRange",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "20px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxFeeDollar": "flxFeeDollar",
                    "flxMaximumAmountDollar": "flxMaximumAmountDollar",
                    "flxMinimumAmountDollar": "flxMinimumAmountDollar",
                    "flxSegDefineFeeRange": "flxSegDefineFeeRange",
                    "lblFeeDollar": "lblFeeDollar",
                    "lblMaximumAmountDollar": "lblMaximumAmountDollar",
                    "lblMinimumAmountDollar": "lblMinimumAmountDollar",
                    "tbxFee": "tbxFee",
                    "tbxMaximumAmount": "tbxMaximumAmount",
                    "tbxMinimumAmount": "tbxMinimumAmount"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDefineFeeRangeContent.add(segDefineFeeRangeContent);
            flxDefineFeeRange.add(flxDefineFeeRangeHeader, flxDefineFeeRangeHeader2, flxDefineFeeRangeContent);
            flxTransactionFeeContent.add(flxDefineFeeRange);
            flxTransactionFee.add(flxTransactionFeeContent);
            var flxCommonButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxCommonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxCommonButtons.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCommonButtons.add(commonButtons);
            flxService.add(flxServiceDetails, flxLimits, flxTransactionFee, flxCommonButtons);
            flxMainContent2.add(flxVerticalTab, flxService);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxMainContent, flxMainContent2);
            var flxScheduleMaster = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxScheduleMaster",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "CopyslFbox0ie07b6077b734c",
                "top": 0,
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxScheduleMaster.setDefaultUnit(kony.flex.DP);
            var flxServicesScheduleMaster = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "500dp",
                "id": "flxServicesScheduleMaster",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "CopyslFbox0a55815af731d49",
                "top": 0,
                "width": "700dp",
                "zIndex": 10
            }, {}, {});
            flxServicesScheduleMaster.setDefaultUnit(kony.flex.DP);
            var flxServicesScheduleMasterTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10dp",
                "id": "flxServicesScheduleMasterTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f849449508464e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxServicesScheduleMasterTopColor.setDefaultUnit(kony.flex.DP);
            flxServicesScheduleMasterTopColor.add();
            var lblServicesScheduleMasterHeader = new kony.ui.Label({
                "id": "lblServicesScheduleMasterHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "CopyslLabel0c372c3f92cd746",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SelectScheduleMaster\")",
                "top": "35dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxScheduleMasterClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxScheduleMasterClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "slFbox",
                "top": "35dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxScheduleMasterClose.setDefaultUnit(kony.flex.DP);
            var imgScheduleMasterClose = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgScheduleMasterClose",
                "isVisible": true,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxScheduleMasterClose.add(imgScheduleMasterClose);
            var flxServicesScheduleMasterContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "300dp",
                "id": "flxServicesScheduleMasterContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "CopyslFbox0gad664d41d864b",
                "top": "100dp",
                "width": "600dp",
                "zIndex": 10
            }, {}, {});
            flxServicesScheduleMasterContent.setDefaultUnit(kony.flex.DP);
            var flxServicesScheduleMasterContentLeft = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxServicesScheduleMasterContentLeft",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a90b1307cef64a",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxServicesScheduleMasterContentLeft.setDefaultUnit(kony.flex.DP);
            var lblServicesMasterList = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblServicesMasterList",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyslLabel0g3d9aed2592e4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.MasterList\")",
                "top": "20dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxServicesLeftSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "0.50%",
                "id": "flxServicesLeftSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0h4a7dc57e1ff49",
                "top": "40dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxServicesLeftSeparator.setDefaultUnit(kony.flex.DP);
            flxServicesLeftSeparator.add();
            var flxServicesScheduleMasterList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bounces": true,
                "centerX": "50%",
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxServicesScheduleMasterList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "50dp",
                "verticalScrollIndicator": true,
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxServicesScheduleMasterList.setDefaultUnit(kony.flex.DP);
            var segMasterListContent = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "imgCheckbox": "radionormal_1x.png",
                    "lblMasterData": "Master Data"
                }, {
                    "imgCheckbox": "radionormal_1x.png",
                    "lblMasterData": "Master Data"
                }, {
                    "imgCheckbox": "radionormal_1x.png",
                    "lblMasterData": "Master Data"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segMasterListContent",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxServicesScheduleMaster",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "64646400",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxServicesScheduleMaster": "flxServicesScheduleMaster",
                    "flxServicesScheduleMasterContent": "flxServicesScheduleMasterContent",
                    "imgCheckbox": "imgCheckbox",
                    "lblMasterData": "lblMasterData"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServicesScheduleMasterList.add(segMasterListContent);
            flxServicesScheduleMasterContentLeft.add(lblServicesMasterList, flxServicesLeftSeparator, flxServicesScheduleMasterList);
            var flxServicesScheduleMasterContentRight = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxServicesScheduleMasterContentRight",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxServicesScheduleMasterContentRight.setDefaultUnit(kony.flex.DP);
            var lblServicesRightHeader = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblServicesRightHeader",
                "isVisible": true,
                "skin": "CopyslLabel0g3d9aed2592e4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblServicesRightHeader\")",
                "top": "20dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxServicesRightSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "0.50%",
                "id": "flxServicesRightSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0h4a7dc57e1ff49",
                "top": "40dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxServicesRightSeparator.setDefaultUnit(kony.flex.DP);
            flxServicesRightSeparator.add();
            var flxServicesScheduleMasterListContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxServicesScheduleMasterListContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxServicesScheduleMasterListContent.setDefaultUnit(kony.flex.DP);
            var lblServicesScheduleMasterListContent = new kony.ui.Label({
                "id": "lblServicesScheduleMasterListContent",
                "isVisible": true,
                "left": 0,
                "skin": "CopyslLabel0ad5a8e07387a4c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblServicesScheduleMasterListContent\")",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServicesScheduleMasterListContent.add(lblServicesScheduleMasterListContent);
            flxServicesScheduleMasterContentRight.add(lblServicesRightHeader, flxServicesRightSeparator, flxServicesScheduleMasterListContent);
            flxServicesScheduleMasterContent.add(flxServicesScheduleMasterContentLeft, flxServicesScheduleMasterContentRight);
            var flxServicesScheduleMasterButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxServicesScheduleMasterButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "CopyslFbox0j364db196ed44d",
                "top": "420dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxServicesScheduleMasterButtons.setDefaultUnit(kony.flex.DP);
            var btnScheduleMasterAdd = new kony.ui.Button({
                "bottom": "20dp",
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40dp",
                "id": "btnScheduleMasterAdd",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            var btnScheduleMasterCancel = new kony.ui.Button({
                "bottom": "20dp",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnScheduleMasterCancel",
                "isVisible": true,
                "right": "150dp",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.Button.Cancel\")",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            flxServicesScheduleMasterButtons.add(btnScheduleMasterAdd, btnScheduleMasterCancel);
            flxServicesScheduleMaster.add(flxServicesScheduleMasterTopColor, lblServicesScheduleMasterHeader, flxScheduleMasterClose, flxServicesScheduleMasterContent, flxServicesScheduleMasterButtons);
            flxScheduleMaster.add(flxServicesScheduleMaster);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxDeactivateServiceManagement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeactivateServiceManagement",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDeactivateServiceManagement.setDefaultUnit(kony.flex.DP);
            var popUpDeactivate = new com.adminConsole.common.popUp1({
                "clipBounds": true,
                "height": "100%",
                "id": "popUpDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "180px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Deactivate_Service\")"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30dp",
                        "right": "viz.val_cleared",
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ...Even though using \"lorem ipsum\" often arouses curiosity because of its resemblance to classical Latin.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. ...Even though using \"lorem ipsum\" often arouses curiosity because of its resemblance to \n",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeactivateServiceManagement.add(popUpDeactivate);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "60px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "60px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxServiceManagement.add(flxLeftPannel, flxRightPanel, flxScheduleMaster, flxToastMessage, flxDeactivateServiceManagement, flxLoading);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxServiceManagement, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmServiceManagement,
            "enabledForIdleTimeout": true,
            "id": "frmServiceManagement",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_g197182751ef45b7ab5605a6f65d849d(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_d95f5c87fabe4c4d9a1bb01a1661012d,
            "retainScrollPosition": false
        }]
    }
});