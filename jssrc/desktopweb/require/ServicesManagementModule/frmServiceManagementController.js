define("ServicesManagementModule/userfrmServiceManagementController", {
    statusConfig: {
        active: "SID_ACTIVE",
        inactive: "SID_INACTIVE"
    },
    createServiceReqParam: null,
    editServiceReqParam: null,
    globalSelectedItem: 0,
    globalSelectedIndex: 0,
    servicesRecList: null,
    firstLoad: false,
    preShowActions: function() {
        this.setFlowActions();
        this.preShowData();
        this.setScrollHeight();
    },
    preShowData: function() {
        this.view.flxServiceManagement.height = kony.os.deviceInfo().screenHeight + "px";
        this.view.flxMainContent.setVisibility(true);
        this.view.mainHeader.lblHeading.centerY = "80";
        this.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.leftmenu.Services");
        // VERTICAL TAB
        this.view.verticalTabs.btnOption1.text = kony.i18n.getLocalizedString("i18n.frmServiceManagementController.SERVICE_DETAILS");
        this.view.verticalTabs.lblSelected1.setVisibility(true);
        this.view.verticalTabs.btnOption2.text = kony.i18n.getLocalizedString("i18n.Group.Limits");
        this.view.btnDefineLimitsAdd.skin = "CopyslButtonGlossBlue0d933dcec26fd45";
        this.view.verticalTabs.lblSelected2.setVisibility(true);
        this.view.verticalTabs.btnOption3.text = kony.i18n.getLocalizedString("i18n.Group.TRANSACTIONFEE");
        this.view.verticalTabs.lblSelected3.setVisibility(true);
        this.view.verticalTabs.flxOption4.setVisibility(false);
        this.view.mainHeader.btnAddNewOption.text = kony.i18n.getLocalizedString("i18n.DragBox.ADD");
        this.view.mainHeader.btnDropdownList.text = kony.i18n.getLocalizedString("i18n.mainHeader.DOWNLOADLIST");
        this.view.flxServiceNamingContent.setVisibility(true);
        this.view.flxSettingsContent.setVisibility(true);
        this.view.flxSmsAlertContent.setVisibility(true);
        this.view.flxBeneficiarySmsAlertContent.setVisibility(true);
        this.view.flxOperationsContent.setVisibility(true);
        this.view.flxServiceStatusFilter.setVisibility(false);
        this.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
        this.view.subHeader.tbxSearchBox.text = "";
        this.view.subHeader.tbxSearchBox.placeholder = kony.i18n.getLocalizedString("i18n.frmServiceManagement.Search_by_Service_Name");
        this.view.subHeader.flxClearSearchImage.setVisibility(false);
        this.view.listingSegmentClient.contextualMenu.lblHeader.setVisibility(false);
        this.view.listingSegmentClient.contextualMenu.btnLink1.setVisibility(false);
        this.view.listingSegmentClient.contextualMenu.btnLink2.setVisibility(false);
        this.view.listingSegmentClient.contextualMenu.flxOptionsSeperator.setVisibility(false);
        this.view.listingSegmentClient.contextualMenu.flxOption4.setVisibility(false);
        this.view.listingSegmentClient.contextualMenu.flxOption3.setVisibility(false);
        this.view.listingSegmentClient.contextualMenu.lblOption1.text = kony.i18n.getLocalizedString("i18n.roles.Edit");
        this.view.listingSegmentClient.contextualMenu.lblOption2.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate");
        this.view.lblServiceNameCount.setVisibility(false);
        this.view.lblServiceDisplayNameCount.setVisibility(false);
        this.view.lblDescriptionCount.setVisibility(false);
        this.view.lblDisplayDescriptionCount.setVisibility(false);
        this.view.flxToastMessage.setVisibility(false);
        this.getMasterDataservices();
        this.view.forceLayout();
    },
    willUpdateUI: function(serviceModel) {
        this.updateLeftMenu(serviceModel);
        var scopeObj = this;
        if (serviceModel.servicesList) {
            if (serviceModel.servicesList.length === 0) {
                scopeObj.view.listingSegmentClient.segListing.setData([]);
                scopeObj.showAddService();
            } else {
                scopeObj.servicesRecList = serviceModel.servicesList;
                scopeObj.setStatusFilterData();
                scopeObj.firstLoad = true;
                scopeObj.sortBy = scopeObj.getObjectSorter("Name");
                scopeObj.resetSortImages();
                scopeObj.view.subHeader.tbxSearchBox.text = "";
                scopeObj.loadPageData = function() {
                    var searchResult = scopeObj.servicesRecList.filter(scopeObj.searchFilter).sort(scopeObj.sortBy.sortData);
                    scopeObj.records = scopeObj.servicesRecList.filter(scopeObj.searchFilter).length;
                    var searchFiltered = scopeObj.filterFurtherBasedOnStatus(searchResult);
                    scopeObj.setServicesSegmentData(searchFiltered);
                };
                scopeObj.loadPageData();
            }
        } else if (serviceModel.masterDataServices) {
            if (serviceModel.masterDataServices.serviceChannel || serviceModel.masterDataServices.serviceType || serviceModel.masterDataServices.serviceCategory) {
                scopeObj.addServicesMasterDataMappings(serviceModel.masterDataServices);
            }
        } else if (serviceModel.toast) {
            if (serviceModel.toast.status === kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success")) {
                this.view.toastMessage.showToastMessage(serviceModel.toast.message, this);
            } else {
                this.view.toastMessage.showErrorToastMessage(serviceModel.toast.message, this);
            }
        } else if (serviceModel.LoadingScreen) {
            if (serviceModel.LoadingScreen.show) {
                kony.adminConsole.utils.showProgressBar(this.view);
            } else {
                kony.adminConsole.utils.hideProgressBar(this.view);
            }
        }
        this.view.forceLayout();
    },
    setScrollHeight: function() {
        var screenHeight = kony.os.deviceInfo().screenHeight;
        var scrollHeight = screenHeight - 136;
        this.view.flxMainContent.height = scrollHeight + "px";
        this.view.flxMainContent2.height = scrollHeight + "px";
        this.view.flxViewServicesSegment.height = screenHeight - 260 + "px";
        this.view.flxScrollViewServices.height = screenHeight - 115 + "px";
        this.view.flxViewServicesWrapper.height = screenHeight - 165 + "px";
        this.view.flxSegmentWrapper.height = screenHeight - 165 + "px";
        this.view.flxViewServicesSegment.height = screenHeight - 248 + "px";
    },
    setFlowActions: function() {
        var scopeObj = this;
        this.view.listingSegmentClient.flxContextualMenu.onHover = scopeObj.onHoverEventCallback;
        this.view.flxServiceStatusFilter.onHover = scopeObj.onHoverEventCallback;
        this.view.tbxServiceName.onEndEditing = function() {
            if (scopeObj.view.lblServiceNameCount.isVisible === true) {
                scopeObj.view.lblServiceNameCount.setVisibility(false);
            }
            scopeObj.view.forceLayout();
        };
        this.view.txtareaServiceDescription.onEndEditing = function() {
            if (scopeObj.view.lblDescriptionCount.isVisible === true) {
                scopeObj.view.lblDescriptionCount.setVisibility(false);
            }
            scopeObj.view.forceLayout();
        };
        this.view.tbxDisplayName.onEndEditing = function() {
            if (scopeObj.view.lblServiceDisplayNameCount.isVisible === true) {
                scopeObj.view.lblServiceDisplayNameCount.setVisibility(false);
            }
            scopeObj.view.forceLayout();
        };
        this.view.txtareaDisplayDescription.onEndEditing = function() {
            if (scopeObj.view.lblDisplayDescriptionCount.isVisible === true) {
                scopeObj.view.lblDisplayDescriptionCount.setVisibility(false);
            }
            scopeObj.view.forceLayout();
        };
        this.view.rbServiceType.onSelection = function() {
            scopeObj.view.flxNoServiceTypeError.isVisible = false;
        };
        this.view.rbCategory.onSelection = function() {
            scopeObj.view.flxNoCategoryError.isVisible = false;
        };
        this.view.checkBoxServiceChannel.onSelection = function() {
            scopeObj.view.flxNoServiceChannelError.isVisible = false;
        };
        this.view.tbxServiceCode.onKeyUp = function() {
            scopeObj.view.tbxServiceCode.skin = "skntbxLato35475f14px";
            scopeObj.view.flxNoServiceCodeError.isVisible = false;
        };
        this.view.tbxServiceName.onKeyUp = function() {
            scopeObj.view.tbxServiceName.skin = "skntbxLato35475f14px";
            scopeObj.view.flxNoServiceNameError.isVisible = false;
            if (scopeObj.view.tbxServiceName.text.trim().length === 0) {
                scopeObj.view.lblServiceNameCount.setVisibility(false);
            } else {
                scopeObj.view.lblServiceNameCount.setVisibility(true);
                scopeObj.view.lblServiceNameCount.text = scopeObj.view.tbxServiceName.text.trim().length + "/60";
            }
            scopeObj.view.forceLayout();
        };
        this.view.txtareaServiceDescription.onKeyUp = function() {
            scopeObj.view.txtareaServiceDescription.skin = "skntxtAreaLato35475f14Px";
            scopeObj.view.flxNoServiceDescriptionError.isVisible = false;
            if (scopeObj.view.txtareaServiceDescription.text.trim().length === 0) {
                scopeObj.view.lblDescriptionCount.setVisibility(false);
            } else {
                scopeObj.view.lblDescriptionCount.setVisibility(true);
                scopeObj.view.lblDescriptionCount.text = scopeObj.view.txtareaServiceDescription.text.trim().length + "/100";
            }
            scopeObj.view.forceLayout();
        };
        this.view.tbxDisplayName.onKeyUp = function() {
            scopeObj.view.tbxDisplayName.skin = "skntbxLato35475f14px";
            scopeObj.view.flxNoDisplayNameError.isVisible = false;
            if (scopeObj.view.tbxDisplayName.text.trim().length === 0) {
                scopeObj.view.lblServiceDisplayNameCount.setVisibility(false);
            } else {
                scopeObj.view.lblServiceDisplayNameCount.setVisibility(true);
                scopeObj.view.lblServiceDisplayNameCount.text = scopeObj.view.tbxDisplayName.text.trim().length + "/60";
            }
            scopeObj.view.forceLayout();
        };
        this.view.txtareaDisplayDescription.onKeyUp = function() {
            scopeObj.view.txtareaDisplayDescription.skin = "skntxtAreaLato35475f14Px";
            scopeObj.view.flxNoDisplayDescriptionError.isVisible = false;
            if (scopeObj.view.txtareaDisplayDescription.text.trim().length === 0) {
                scopeObj.view.lblDisplayDescriptionCount.setVisibility(false);
            } else {
                scopeObj.view.lblDisplayDescriptionCount.setVisibility(true);
                scopeObj.view.lblDisplayDescriptionCount.text = scopeObj.view.txtareaDisplayDescription.text.trim().length + "/100";
            }
            scopeObj.view.forceLayout();
        };
        this.view.tbxBeneficiarySmsAlertCharge.onKeyUp = function() {
            scopeObj.view.flxNoBeneficiarySmsAlertChargeError.isVisible = false;
            scopeObj.view.flxBenfChargeTextbxContainer.skin = "sknFlxSegRowHover11abeb";
        };
        this.view.tbxBeneficiarySmsAlertCharge.onEndEditing = function() {
            scopeObj.view.flxBenfChargeTextbxContainer.skin = "sknflxffffffoptemplateop3px";
            var txtValue = scopeObj.view.tbxBeneficiarySmsAlertCharge.text;
            if (/^\d+$/.test(scopeObj.view.tbxBeneficiarySmsAlertCharge.text)) {
                scopeObj.view.tbxBeneficiarySmsAlertCharge.text = txtValue + ".00";
            }
        };
        this.view.tbxSmsAlertCharge.onKeyUp = function() {
            scopeObj.view.flxNoSmsAlertChargeError.isVisible = false;
            scopeObj.view.flxChargeTextbxContainer.skin = "sknFlxSegRowHover11abeb";
        };
        this.view.tbxSmsAlertCharge.onEndEditing = function() {
            scopeObj.view.flxChargeTextbxContainer.skin = "sknflxffffffoptemplateop3px";
            var txtValue = scopeObj.view.tbxSmsAlertCharge.text;
            if (/^\d+$/.test(scopeObj.view.tbxSmsAlertCharge.text)) {
                scopeObj.view.tbxSmsAlertCharge.text = txtValue + ".00";
            }
        };
        this.view.tbxLimit1.onKeyUp = function() {
            scopeObj.view.tbxLimit1.skin = "CopyslTextBox0b4bc21adeecd47";
            scopeObj.view.lblNoLimitDefineError1.isVisible = false;
        };
        this.view.tbxLimit2.onKeyUp = function() {
            scopeObj.view.tbxLimit2.skin = "CopyslTextBox0b4bc21adeecd47";
            scopeObj.view.lblNoLimitDefineError2.isVisible = false;
        };
        this.view.tbxLimit3.onKeyUp = function() {
            scopeObj.view.tbxLimit3.skin = "CopyslTextBox0b4bc21adeecd47";
            scopeObj.view.lblNoLimitDefineError3.isVisible = false;
        };
        this.view.btnAddService.onClick = function() {
            scopeObj.createServiceReqParam = null;
            scopeObj.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Add_Service");
            scopeObj.view.listingSegmentClient.segListing.selectedIndex = null;
            scopeObj.view.tbxBeneficiarySmsAlertCharge.setEnabled(false);
            scopeObj.view.tbxSmsAlertCharge.setEnabled(false);
            scopeObj.clearData();
            scopeObj.showService();
        };
        this.view.mainHeader.btnAddNewOption.onClick = function() {
            scopeObj.view.btnAddService.onClick();
        };
        this.view.mainHeader.btnDropdownList.onClick = function() {
            if (scopeObj.view.listingSegmentClient.rtxNoResultsFound.isVisible === false) {
                scopeObj.downloadServicesCSV();
            }
        };
        this.view.commonButtons.btnNext.onClick = function() {
            if (scopeObj.view.flxServiceDetails.isVisible === true) {
                scopeObj.showLimits();
            } else if (scopeObj.view.flxLimits.isVisible === true) {
                scopeObj.showTransactionFee();
            }
        };
        this.view.commonButtons.btnSave.onClick = function() {
            var index = scopeObj.view.listingSegmentClient.segListing.selectedIndex;
            if (index === null) { //create service
                scopeObj.setServiceDetails();
                scopeObj.saveService(scopeObj.createServiceReqParam);
            } else { //edit service
                var rowInd = index[1];
                var rowData = scopeObj.view.listingSegmentClient.segListing.data[rowInd];
                scopeObj.setEditServiceDetails(rowData.id);
                scopeObj.updateServiceDetails(scopeObj.editServiceReqParam);
            }
        };
        this.view.listingSegmentClient.segListing.onRowClick = function() {
            scopeObj.showViewServiceData();
            scopeObj.setViewServiceData();
        };
        this.view.commonButtons.btnCancel.onClick = function() {
            scopeObj.hideService();
            scopeObj.viewServices();
        };
        this.view.verticalTabs.flxOption1.onClick = function() {
            scopeObj.showServiceDetails();
        };
        this.view.verticalTabs.flxOption2.onClick = function() {
            scopeObj.showLimits();
        };
        this.view.verticalTabs.flxOption3.onClick = function() {
            scopeObj.showTransactionFee();
        };
        this.view.verticalTabs.btnOption1.onClick = function() {
            scopeObj.showServiceDetails();
        };
        this.view.verticalTabs.btnOption2.onClick = function() {
            scopeObj.showLimits();
        };
        this.view.verticalTabs.btnOption3.onClick = function() {
            scopeObj.showTransactionFee();
        };
        this.view.breadcrumbs.btnBackToMain.onClick = function() {
            scopeObj.loadPageData();
            scopeObj.hideService();
        };
        this.view.flxScheduleClick.onClick = function() {
            var masterList = {
                "records": [{
                    "masterData": kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Master_Data_ABC")
                }, {
                    "masterData": kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Master_Data_XYZ")
                }]
            };
            scopeObj.setServicesScheduleMasterSegmentData(masterList);
        };
        this.view.flxScheduleMasterClose.onClick = function() {
            scopeObj.view.flxScheduleMaster.setVisibility(false);
        };
        this.view.btnScheduleMasterAdd.onClick = function() {
            scopeObj.view.flxScheduleMaster.setVisibility(false);
        };
        this.view.btnScheduleMasterCancel.onClick = function() {
            scopeObj.view.flxScheduleMaster.setVisibility(false);
        };
        this.view.listingSegmentClient.contextualMenu.flxOption2.onClick = function() {
            if (scopeObj.view.listingSegmentClient.contextualMenu.lblOption2.text === kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate")) {
                scopeObj.showDeactivatePopup();
            } else {
                scopeObj.deactivateService();
                scopeObj.view.flxDeactivateServiceManagement.setVisibility(false);
            }
        };
        this.view.flxViewEditButton.onClick = function() {
            scopeObj.editServiceReqParam = null;
            scopeObj.clearInlineErrors();
            scopeObj.showService();
            scopeObj.prefillServiceDataForEdit();
        };
        this.view.listingSegmentClient.contextualMenu.flxOption1.onClick = function() {
            scopeObj.editServiceReqParam = null;
            scopeObj.clearInlineErrors();
            scopeObj.showService();
            scopeObj.prefillServiceDataForEdit();
        };
        this.view.popUpDeactivate.btnPopUpDelete.onClick = function() {
            scopeObj.deactivateService();
            scopeObj.view.flxDeactivateServiceManagement.setVisibility(false);
        };
        this.view.popUpDeactivate.btnPopUpCancel.onClick = function() {
            scopeObj.view.flxDeactivateServiceManagement.setVisibility(false);
            scopeObj.cancelDeactivateServicePopup();
        };
        this.view.popUpDeactivate.flxPopUpClose.onClick = function() {
            scopeObj.view.flxDeactivateServiceManagement.setVisibility(false);
        };
        this.view.btnDefineLimitsAdd.onClick = function() {
            if (scopeObj.view.btnDefineLimitsAdd.skin === "CopyslButtonGlossBlue0d933dcec26fd45" && scopeObj.view.flxDefineLimitsContent2.isVisible === false) {
                scopeObj.view.flxDefineLimitsContent2.setVisibility(true);
            } else if (scopeObj.view.btnDefineLimitsAdd.skin === "CopyslButtonGlossBlue0d933dcec26fd45" && scopeObj.view.flxDefineLimitsContent3.isVisible === false) {
                scopeObj.view.btnDefineLimitsAdd.skin = "sknBtnLimitsAddDisabled";
                scopeObj.view.flxDefineLimitsContent3.setVisibility(true);
            }
        };
        this.view.flxDeleteLimits2.onClick = function() {
            scopeObj.view.btnDefineLimitsAdd.skin = "CopyslButtonGlossBlue0d933dcec26fd45";
            scopeObj.view.flxDefineLimitsContent2.setVisibility(false);
        };
        this.view.flxDeleteLimits3.onClick = function() {
            scopeObj.view.btnDefineLimitsAdd.skin = "CopyslButtonGlossBlue0d933dcec26fd45";
            scopeObj.view.flxDefineLimitsContent3.setVisibility(false);
        };
        this.view.flxServiceNamingHeader.onClick = function() {
            if (scopeObj.view.flxServiceNamingContent.isVisible === true) {
                scopeObj.view.lblArrowServiceNaming.text = "\ue922";
                scopeObj.view.lblArrowServiceNaming.skin = "sknfontIconDescRightArrow14px";
                scopeObj.view.flxServiceNamingContent.setVisibility(false);
            } else {
                scopeObj.view.lblArrowServiceNaming.text = "\ue915";
                scopeObj.view.lblArrowServiceNaming.skin = "sknfontIconDescDownArrow12px";
                scopeObj.view.flxServiceNamingContent.setVisibility(true);
            }
        };
        this.view.flxSettingsHeader.onClick = function() {
            if (scopeObj.view.flxSettingsContent.isVisible === true) {
                scopeObj.view.lblArrowSeetings.text = "\ue922";
                scopeObj.view.lblArrowSeetings.skin = "sknfontIconDescRightArrow14px";
                scopeObj.view.flxSettingsContent.setVisibility(false);
            } else {
                scopeObj.view.lblArrowSeetings.text = "\ue915";
                scopeObj.view.lblArrowSeetings.skin = "sknfontIconDescDownArrow12px";
                scopeObj.view.flxSettingsContent.setVisibility(true);
            }
        };
        this.view.flxHeadingSmsAlert.onClick = function() {
            if (scopeObj.view.flxSmsAlertContent.isVisible === true) {
                scopeObj.view.lblArrowSmsAlert.text = "\ue922";
                scopeObj.view.lblArrowSmsAlert.skin = "sknfontIconDescRightArrow14px";
                scopeObj.view.flxSmsAlertContent.setVisibility(false);
            } else {
                scopeObj.view.lblArrowSmsAlert.text = "\ue915";
                scopeObj.view.lblArrowSmsAlert.skin = "sknfontIconDescDownArrow12px";
                scopeObj.view.flxSmsAlertContent.setVisibility(true);
            }
        };
        this.view.flxHeadingBenificiaryAlert.onClick = function() {
            if (scopeObj.view.flxBeneficiarySmsAlertContent.isVisible === true) {
                scopeObj.view.lblArrowBeneficiarySmsAlert.text = "\ue922";
                scopeObj.view.lblArrowBeneficiarySmsAlert.skin = "sknfontIconDescRightArrow14px";
                scopeObj.view.flxBeneficiarySmsAlertContent.setVisibility(false);
            } else {
                scopeObj.view.lblArrowBeneficiarySmsAlert.text = "\ue915";
                scopeObj.view.lblArrowBeneficiarySmsAlert.skin = "sknfontIconDescDownArrow12px";
                scopeObj.view.flxBeneficiarySmsAlertContent.setVisibility(true);
            }
        };
        this.view.flxHeadingOperation.onClick = function() {
            if (scopeObj.view.flxOperationsContent.isVisible === true) {
                scopeObj.view.lblArrowOperations.text = "\ue922";
                scopeObj.view.lblArrowOperations.skin = "sknfontIconDescRightArrow14px";
                scopeObj.view.flxOperationsContent.setVisibility(false);
            } else {
                scopeObj.view.lblArrowOperations.text = "\ue915";
                scopeObj.view.lblArrowOperations.skin = "sknfontIconDescDownArrow12px";
                scopeObj.view.flxOperationsContent.setVisibility(true);
            }
        };
        this.view.flxViewServicesHeaderName.onClick = function() {
            scopeObj.sortData("Name");
        };
        this.view.flxViewServicesHeaderCode.onClick = function() {
            scopeObj.sortData("Code");
        };
        this.view.flxViewServicesHeaderCategory.onClick = function() {
            scopeObj.sortData("Category_Name");
        };
        this.view.flxLocationsHeaderSC.onClick = function() {
            scopeObj.sortData("Channel");
        };
        this.view.flxStatus.onClick = function() {
            if (scopeObj.firstLoad) {
                scopeObj.setStatusFilterData();
                scopeObj.firstLoad = false;
            }
            var flxLeft = scopeObj.view.flxStatus.frame.x;
            scopeObj.view.flxServiceStatusFilter.left = (flxLeft - 50) + "px";
            scopeObj.view.flxServiceStatusFilter.setVisibility(!scopeObj.view.flxServiceStatusFilter.isVisible);
            if (scopeObj.view.listingSegmentClient.flxContextualMenu.isVisible) {
                scopeObj.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
            }
        };
        // VIEW SERVICE DATA
        this.view.flxDetailsHeader.onClick = function() {
            if (scopeObj.view.flxDetailsContent.isVisible === true) {
                scopeObj.view.lblArrowDetails.text = "\ue922";
                scopeObj.view.lblArrowDetails.skin = "sknfontIconDescRightArrow14px";
                scopeObj.view.flxDetailsContent.setVisibility(false);
            } else {
                scopeObj.view.lblArrowDetails.text = "\ue915";
                scopeObj.view.lblArrowDetails.skin = "sknfontIconDescDownArrow12px";
                scopeObj.view.flxDetailsContent.setVisibility(true);
            }
        };
        this.view.flxServiceDescriptionHeader.onClick = function() {
            if (scopeObj.view.flxServiceDescriptionContent.isVisible === true) {
                scopeObj.view.lblIconArrowDescription.text = "\ue922";
                scopeObj.view.lblIconArrowDescription.skin = "sknfontIconDescRightArrow14px";
                scopeObj.view.flxServiceDescriptionContent.setVisibility(false);
            } else {
                scopeObj.view.lblIconArrowDescription.text = "\ue915";
                scopeObj.view.lblIconArrowDescription.skin = "sknfontIconDescDownArrow12px";
                scopeObj.view.flxServiceDescriptionContent.setVisibility(true);
            }
        };
        this.view.flxDisplayDescriptionHeader.onClick = function() {
            if (scopeObj.view.flxDisplayDescriptionContent.isVisible === true) {
                scopeObj.view.lblArrowDisplayDescription.text = "\ue922";
                scopeObj.view.lblArrowDisplayDescription.skin = "sknfontIconDescRightArrow14px";
                scopeObj.view.flxDisplayDescriptionContent.setVisibility(false);
            } else {
                scopeObj.view.lblArrowDisplayDescription.text = "\ue915";
                scopeObj.view.lblArrowDisplayDescription.skin = "sknfontIconDescDownArrow12px";
                scopeObj.view.flxDisplayDescriptionContent.setVisibility(true);
            }
        };
        this.view.flxServiceData2Limits.onClick = function() {
            scopeObj.view.flxViewSD2TF.setVisibility(false);
            scopeObj.view.flxViewSD2Limits.setVisibility(true);
        };
        this.view.flxServiceData2TransactionFee.onClick = function() {
            scopeObj.view.flxViewSD2Limits.setVisibility(false);
            scopeObj.view.flxViewSD2TF.setVisibility(true);
        };
        this.view.subHeader.tbxSearchBox.onKeyUp = function() {
            if (scopeObj.view.subHeader.tbxSearchBox.text === "") {
                scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
            } else {
                scopeObj.view.subHeader.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
                scopeObj.view.subHeader.flxClearSearchImage.setVisibility(true);
            }
            scopeObj.loadPageData();
            if (scopeObj.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices === null) {
                scopeObj.view.listingSegmentClient.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
                scopeObj.view.flxViewServicesHeader.setVisibility(true);
                scopeObj.view.listingSegmentClient.segListing.setData([]);
                scopeObj.view.forceLayout();
            }
        };
        this.view.subHeader.flxClearSearchImage.onClick = function() {
            scopeObj.view.subHeader.tbxSearchBox.text = "";
            scopeObj.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100";
            scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
            scopeObj.loadPageData();
            if (scopeObj.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices === null) {
                scopeObj.view.listingSegmentClient.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
                scopeObj.view.flxViewServicesHeader.setVisibility(true);
                scopeObj.view.listingSegmentClient.segListing.setData([]);
                scopeObj.view.forceLayout();
            }
        };
        this.view.customSwitchSmsAlert.switchToggle.onSlide = function() {
            if (scopeObj.view.customSwitchSmsAlert.switchToggle.selectedIndex === 0) {
                scopeObj.view.tbxSmsAlertCharge.setEnabled(true);
            } else {
                scopeObj.view.tbxSmsAlertCharge.setEnabled(false);
            }
        };
        this.view.customSwitchBeneficiarySmsAlert.switchToggle.onSlide = function() {
            if (scopeObj.view.customSwitchBeneficiarySmsAlert.switchToggle.selectedIndex === 0) {
                scopeObj.view.tbxBeneficiarySmsAlertCharge.setEnabled(true);
            } else {
                scopeObj.view.tbxBeneficiarySmsAlertCharge.setEnabled(false);
            }
        };
        this.view.statusFilterMenu.segStatusFilterDropdown.onRowClick = function() {
            scopeObj.performStatusFilter();
        };
    },
    showDeactivatePopup: function() {
        this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Deactivate_Service");
        this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmServiceManagementController.deactivate_Service_popup") + "" + kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Service_deactive_messageDesc");
        this.view.popUpDeactivate.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS");
        this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesDeactivate");
        this.view.flxDeactivateServiceManagement.setVisibility(true);
        this.view.forceLayout();
    },
    deactivateService: function() {
        var self = this;
        var index = self.view.listingSegmentClient.segListing.selectedIndex;
        var rowInd = index[1];
        var rowData = self.view.listingSegmentClient.segListing.data[rowInd];
        var statusId = "";
        if (rowData !== null) {
            if (rowData.Status_id === self.statusConfig.active) {
                statusId = self.statusConfig.inactive;
            } else {
                statusId = self.statusConfig.active;
            }
            self.changeStatusOfService(rowData, statusId);
        }
        this.view.forceLayout();
    },
    cancelDeactivateServicePopup: function() {
        this.view.cancelDeactivateServicePopup.setVisibility(false);
        this.view.forceLayout();
    },
    //show add service screen,when no services exists
    showAddService: function() {
        this.view.breadcrumbs.setVisibility(false);
        this.view.mainHeader.btnAddNewOption.setVisibility(false);
        this.view.mainHeader.btnDropdownList.setVisibility(false);
        this.view.mainHeader.flxButtons.setVisibility(false);
        this.view.mainHeader.flxHeaderSeperator.setVisibility(false);
        this.view.flxServiceDetails.setVisibility(false);
        this.view.flxLimits.setVisibility(false);
        this.view.flxTransactionFee.setVisibility(false);
        this.view.flxMainContent2.setVisibility(false);
        // this.view.flxViewServices.setVisibility(false);
        this.view.flxScrollViewServices.setVisibility(false);
        this.view.flxMainContent.setVisibility(true);
        this.view.flxAddService.setVisibility(true);
        this.view.lblServiceNameCount.setVisibility(false);
        this.view.lblServiceDisplayNameCount.setVisibility(false);
        this.view.lblDescriptionCount.setVisibility(false);
        this.view.lblDisplayDescriptionCount.setVisibility(false);
        this.view.forceLayout();
    },
    //show edit/add screen
    showService: function() {
        this.view.breadcrumbs.setVisibility(true);
        this.view.flxAddService.setVisibility(false);
        this.view.flxMainContent.setVisibility(false);
        this.view.flxMainContent2.setVisibility(true);
        this.view.breadcrumbs.setVisibility(false);
        this.showServiceDetails();
    },
    //hide edit/add screen
    hideService: function() {
        this.view.breadcrumbs.setVisibility(false);
        this.view.flxAddService.setVisibility(false);
        this.view.flxMainContent.setVisibility(true);
        this.view.flxMainContent2.setVisibility(false);
        this.view.mainHeader.btnAddNewOption.setVisibility(true);
        this.view.mainHeader.btnDropdownList.setVisibility(true);
        this.view.mainHeader.flxButtons.setVisibility(true);
        this.view.mainHeader.flxHeaderSeperator.setVisibility(true);
        this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.leftmenu.Services");
        this.view.flxViewServiceData.setVisibility(false);
        this.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
    },
    //show  view service details screen on row click
    showViewServiceData: function() {
        this.view.breadcrumbs.setVisibility(true);
        this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmServiceManagementController.SERVICES");
        this.view.breadcrumbs.lblCurrentScreen.setVisibility(true);
        this.view.mainHeader.btnAddNewOption.setVisibility(false);
        this.view.mainHeader.btnDropdownList.setVisibility(false);
        this.view.mainHeader.flxHeaderSeperator.setVisibility(false);
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.leftmenu.Services");
        this.view.flxAddService.setVisibility(false);
        this.view.flxScrollViewServices.setVisibility(false);
        this.view.flxViewServiceData.setVisibility(true);
        this.view.forceLayout();
    },
    //show service tab in edit/add screen
    showServiceDetails: function() {
        this.view.commonButtons.btnNext.setVisibility(true);
        this.view.flxLimits.setVisibility(false);
        this.view.flxTransactionFee.setVisibility(false);
        this.view.flxServiceDetails.setVisibility(true);
        this.view.mainHeader.flxButtons.setVisibility(false);
        this.view.mainHeader.flxHeaderSeperator.setVisibility(false);
        this.view.breadcrumbs.setVisibility(false);
        this.view.verticalTabs.lblSelected1.setVisibility(true);
        this.view.verticalTabs.lblSelected2.setVisibility(false);
        this.view.verticalTabs.lblSelected3.setVisibility(false);
        this.view.verticalTabs.btnOption1.skin = "Btn000000font14px";
        this.view.verticalTabs.btnOption2.skin = "Btn84939efont14px";
        this.view.verticalTabs.btnOption3.skin = "Btn84939efont14px";
        this.view.lblSmsAlertDollar.text = this.defaultCurrencyCode();
        this.view.lblBeneficiarySmsAlertDollar.text = this.defaultCurrencyCode();
        this.view.forceLayout();
    },
    //show limits tab
    showLimits: function() {
        this.view.commonButtons.btnNext.setVisibility(true);
        this.view.flxLimits.setVisibility(true);
        this.view.verticalTabs.lblSelected1.setVisibility(false);
        this.view.verticalTabs.lblSelected2.setVisibility(true);
        this.view.verticalTabs.lblSelected3.setVisibility(false);
        this.view.flxServiceDetails.setVisibility(false);
        this.view.flxTransactionFee.setVisibility(false);
        this.view.verticalTabs.btnOption2.skin = "Btn000000font14px";
        this.view.verticalTabs.btnOption1.skin = "Btn84939efont14px";
        this.view.verticalTabs.btnOption3.skin = "Btn84939efont14px";
        this.view.forceLayout();
    },
    showTransactionFee: function() {
        this.view.commonButtons.btnNext.setVisibility(false);
        this.view.verticalTabs.lblSelected1.setVisibility(false);
        this.view.verticalTabs.lblSelected2.setVisibility(false);
        this.view.verticalTabs.lblSelected3.setVisibility(true);
        this.view.flxServiceDetails.setVisibility(false);
        this.view.flxLimits.setVisibility(false);
        this.view.flxTransactionFee.setVisibility(true);
        this.view.verticalTabs.btnOption3.skin = "Btn000000font14px";
        this.view.verticalTabs.btnOption2.skin = "Btn84939efont14px";
        this.view.verticalTabs.btnOption1.skin = "Btn84939efont14px";
        this.view.forceLayout();
    },
    /*
     * function called on click of save from create service to call presentation controller
     */
    saveService: function() {
        var self = this;
        var condition = self.validation();
        if (condition) {
            self.presenter.createService(self.createServiceReqParam);
            self.viewServices();
            self.hideService();
        }
    },
    //show services segment list
    viewServices: function() {
        this.view.breadcrumbs.setVisibility(false);
        this.view.mainHeader.btnAddNewOption.setVisibility(true);
        this.view.mainHeader.btnDropdownList.setVisibility(true);
        this.view.mainHeader.flxButtons.setVisibility(true);
        this.view.mainHeader.flxHeaderSeperator.setVisibility(true);
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.leftmenu.Services");
        this.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
        this.view.flxServiceDetails.setVisibility(false);
        this.view.flxLimits.setVisibility(false);
        this.view.flxTransactionFee.setVisibility(false);
        this.view.flxMainContent2.setVisibility(false);
        this.view.flxViewServiceData.setVisibility(false);
        this.view.flxAddService.setVisibility(false);
        this.view.flxServicesList.setVisibility(true);
        this.view.flxMainContent.setVisibility(true);
        this.view.flxScrollViewServices.setVisibility(true);
        this.view.forceLayout();
    },
    setServicesSegmentData: function(servicesList, isFilter) {
        var scopeObj = this;
        this.gblServicesData = [
            [
                this.gblServicesList
            ]
        ];
        if (servicesList === undefined) {
            scopeObj.view.listingSegmentClient.segListing.setData([]);
            scopeObj.showAddService();
        } else {
            var records = servicesList;
            if (records.length === 0) {
                scopeObj.showNoResultsFound();
            } else {
                var dataMap = {
                    //required fields mappings
                    "id": "id",
                    "Description": "Description",
                    "Category_Id": "Category_Id",
                    "Channel_Id": "Channel_Id",
                    "Type_id": "Type_id",
                    "Status_id": "Status_id",
                    "DisplayName": "DisplayName",
                    "DisplayDesc": "DisplayDesc",
                    "WorkSchedule_id": "WorkSchedule_id",
                    "TransactionLimit_id": "TransactionLimit_id",
                    "TransactionFee_id": "TransactionFee_id",
                    "IsSMSAlertActivated": "IsSMSAlertActivated",
                    "IsBeneficiarySMSAlertActivated": "IsBeneficiarySMSAlertActivated",
                    "SMSCharges": "SMSCharges",
                    "BeneficiarySMSCharge": "BeneficiarySMSCharge",
                    "IsOutageMessageActive": "IsOutageMessageActive",
                    "IsFutureTransaction": "IsFutureTransaction",
                    "IsTCActive": "IsTCActive",
                    "IsAgreementActive": "IsAgreementActive",
                    "WorkSchedule_Desc": "WorkSchedule_Desc",
                    //widget mappings
                    "lblServicesName": "lblServicesName",
                    "lblServicesCode": "lblServicesCode",
                    "lblServicesType": "lblServicesType",
                    "lblServicesCategory": "lblServicesCategory",
                    "lblServicesSupportedChannels": "lblServicesSupportedChannels",
                    "lblIconStatus": "lblIconStatus",
                    "lblServicesStatus": "lblServicesStatus",
                    "flxStatus": "flxStatus",
                    "lblOptions": "lblOptions",
                    "flxOptions": "flxOptions",
                    "lblSeperator": "lblSeperator",
                    "flxServices2": "flxServices2"
                };
                scopeObj.hideNoResultsFound();
                var data = servicesList.map(scopeObj.toViewServiceSegment.bind(scopeObj));
                scopeObj.view.listingSegmentClient.segListing.widgetDataMap = dataMap;
                scopeObj.view.listingSegmentClient.segListing.setData(data);
                document.getElementById("frmServiceManagement_listingSegmentClient_segListing").onscroll = this.contextualMenuOff;
                scopeObj.viewServices();
            }
        }
    },
    contextualMenuOff: function(context) {
        this.view.listingSegmentClient.flxContextualMenu.isVisible = false;
    },
    setStatusFilterData: function() {
        var self = this;
        var statusList = [];
        for (var i = 0; i < self.servicesRecList.length; i++) {
            if (!statusList.contains(self.servicesRecList[i].Status)) statusList.push(self.servicesRecList[i].Status);
        }
        var widgetMap = {
            "flxSearchDropDown": "flxSearchDropDown",
            "flxCheckBox": "flxCheckBox",
            "imgCheckBox": "imgCheckBox",
            "lblDescription": "lblDescription"
        };
        var data = statusList.map(function(segData) {
            return {
                "flxSearchDropDown": "flxSearchDropDown",
                "flxCheckBox": "flxCheckBox",
                "lblDescription": segData,
                "imgCheckBox": {
                    "src": "checkbox.png"
                }
            };
        });
        self.view.statusFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
        self.view.statusFilterMenu.segStatusFilterDropdown.setData(data);
        var selInd = [
            [0, []]
        ];
        for (var j = 0; j < statusList.length; j++) {
            selInd[0][1].push(j);
        }
        self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices = selInd;
        self.view.forceLayout();
    },
    toViewServiceSegment: function(service) {
        var scopeObj = this;
        return {
            "id": service.id,
            "Description": service.Description,
            "Category_Id": service.Category_Id,
            "Channel_Id": service.Channel_id,
            "DisplayName": service.DisplayName,
            "DisplayDesc": service.DisplayDescription,
            "Type_id": service.Type_id,
            "Status_id": service.Status_id,
            //to be replaced with org values
            "MaxTransferLimit": service.MaxTransferLimit || '',
            "MinTransferLimit": service.MinTransferLimit || '',
            "WorkSchedule_id": service.WorkSchedule_id || '',
            "TransactionLimit_id": service.TransactionLimit_id || '',
            "TransactionFee_id": service.TransactionFee_id || '',
            "IsSMSAlertActivated": service.IsSMSAlertActivated || 'false',
            "IsBeneficiarySMSAlertActivated": service.IsBeneficiarySMSAlertActivated || 'false',
            "SMSCharges": service.SMSCharges || '',
            "BeneficiarySMSCharge": service.BeneficiarySMSCharge || '',
            "IsOutageMessageActive": service.IsOutageMessageActive,
            "IsFutureTransaction": service.IsFutureTransaction,
            "IsTCActive": service.IsTCActive,
            "IsAgreementActive": service.IsAgreementActive,
            "WorkSchedule_Desc": service.WorkSchedule_Desc || '',
            "lblServicesName": {
                "text": service.Name
            },
            "lblServicesCode": {
                "text": scopeObj.AdminConsoleCommonUtils.getTruncatedString(service.Code, 11, 9),
                "info": {
                    "value": service.Code || "-"
                },
                "tooltip": service.Code || ""
            },
            "lblServicesType": {
                "text": service.Type_Name || '-'
            },
            "lblServicesCategory": {
                "text": service.Category_Name || '-'
            },
            "lblServicesSupportedChannels": {
                "text": service.Channel ? service.Channel.replace(/,/g, ", ") : '-'
            },
            "lblIconStatus": {
                "text": "\ue921",
                "skin": scopeObj.statusConfig.active === service.Status_id ? "sknIcon13pxGreen" : "sknIcon13pxGray"
            },
            "lblServicesStatus": {
                "text": scopeObj.statusConfig.active === service.Status_id ? kony.i18n.getLocalizedString("i18n.secureimage.Active") : kony.i18n.getLocalizedString("i18n.secureimage.Inactive"),
                "skin": scopeObj.statusConfig.active === service.Status_id ? "sknlblLato5bc06cBold14px" : "sknlblLatocacacaBold12px"
            },
            "flxOptions": {
                "onClick": function() {
                    scopeObj.toggleContextualMenu(50);
                }
            },
            "lblOptions": {
                "text": "\ue91f",
                "skin": "sknFontIconOptionMenu"
            },
            "lblSeperator": {
                "text": "."
            },
            "template": "flxServices2",
            "Name": service.Name,
            "Code": service.Code,
            "Category_Name": service.Category_Name || '-',
            "Channel": service.Channel ? service.Channel.replace(/,/g, ", ") : '-'
        };
    },
    toggleContextualMenu: function(rowHeight) {
        kony.print("Inside toggleContextualMenu()");
        if (this.view.listingSegmentClient.flxContextualMenu.isVisible === false) {
            var index = this.view.listingSegmentClient.segListing.selectedIndex;
            this.rowIndex = index[1];
            var templateArray = this.view.listingSegmentClient.segListing.clonedTemplates;
            //to caluclate top from preffered row heights
            var finalHeight = 0;
            for (var i = 0; i < this.rowIndex; i++) {
                finalHeight = finalHeight + templateArray[i].flxServices2.frame.height;
            }
            var flexLeft = this.view.listingSegmentClient.segListing.clonedTemplates[this.rowIndex].flxOptions.frame.x;
            this.view.listingSegmentClient.flxContextualMenu.left = ((flexLeft + 25) - 200) + "px";
            this.updateContextualMenu();
            this.view.listingSegmentClient.flxContextualMenu.setVisibility(true);
            this.view.forceLayout();
            var segmentWidget = this.view.listingSegmentClient.segListing;
            var contextualWidget = this.view.listingSegmentClient.contextualMenu;
            finalHeight = ((finalHeight + 45) - segmentWidget.contentOffsetMeasured.y);
            if (finalHeight + contextualWidget.frame.height > segmentWidget.frame.height) {
                finalHeight = finalHeight - contextualWidget.frame.height - 45;
            }
            this.view.listingSegmentClient.flxContextualMenu.top = finalHeight + "px";
        } else {
            this.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
        }
        if (this.view.flxServiceStatusFilter.isVisible) {
            this.view.flxServiceStatusFilter.setVisibility(false);
        }
    },
    updateContextualMenu: function() {
        kony.print("Inside updateContextualMenu()");
        var data = this.view.listingSegmentClient.segListing.data;
        if (data[this.rowIndex].lblServicesStatus.text === kony.i18n.getLocalizedString("i18n.secureimage.Active")) {
            this.view.listingSegmentClient.contextualMenu.lblOption2.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate");
            this.view.listingSegmentClient.contextualMenu.lblIconOption2.text = "\ue91c";
            this.view.listingSegmentClient.contextualMenu.lblIconOption2.skin = "sknIcon20px";
        } else {
            this.view.listingSegmentClient.contextualMenu.lblOption2.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Activate");
            this.view.listingSegmentClient.contextualMenu.lblIconOption2.text = "\ue931";
            this.view.listingSegmentClient.contextualMenu.lblIconOption2.skin = "sknIcon20px";
        }
    },
    showNoResultsFound: function() {
        this.view.listingSegmentClient.segListing.setVisibility(false);
        this.view.listingSegmentClient.rtxNoResultsFound.setVisibility(true);
        this.view.listingSegmentClient.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found") + "\"" + this.view.subHeader.tbxSearchBox.text + "\"" + kony.i18n.getLocalizedString("i18n.frmPoliciesController.Try_with_another_keyword");
        this.view.flxViewServicesHeader.setVisibility(false);
        this.view.listingSegmentClient.pagination.setVisibility(false);
        this.view.forceLayout();
    },
    hideNoResultsFound: function() {
        this.view.listingSegmentClient.segListing.setVisibility(true);
        this.view.listingSegmentClient.rtxNoResultsFound.setVisibility(false);
        this.view.flxViewServicesHeader.setVisibility(true);
        this.view.listingSegmentClient.pagination.setVisibility(true);
        this.view.forceLayout();
    },
    optionsMenu: function() {
        kony.print("Inside optionsMenu() of frmServiceManagementController");
        var scopeObj = this;
        var selectedItem = this.view.segServicesListContent.selectedItems[0];
        this.globalSelectedItem = selectedItem;
        var selectedIndex = this.view.segServicesListContent.selectedIndex[1];
        this.globalSelectedIndex = selectedIndex;
        var selectedRowIndex = this.view.segServicesListContent.selectedRowIndex[1];
        kony.print("selectedItem: " + selectedItem);
        kony.print("selectedIndex: " + selectedIndex);
        kony.print("selectedRowIndex: " + selectedRowIndex);
        if (this.view.flxSelectOptions.isVisible === true) {
            scopeObj.view.flxSelectOptions.setVisibility(false);
        } else {
            if (selectedItem.lblServicesStatus.text === kony.i18n.getLocalizedString("i18n.secureimage.Active")) {
                scopeObj.view.flxSelectOptions.top = ((selectedRowIndex * 80) + 130) + "px";
                scopeObj.view.lblActivateDeactivate.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate");
            } else {
                scopeObj.view.flxSelectOptions.top = ((selectedRowIndex * 80) + 130) + "px";
                scopeObj.view.lblActivateDeactivate.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Activate");
            }
            scopeObj.view.flxSelectOptions.setVisibility(true);
        }
    },
    searchFilter: function(serviceData) {
        var searchText = this.view.subHeader.tbxSearchBox.text;
        if (typeof searchText === "string" && searchText.length > 0) {
            return (serviceData.Name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
        } else {
            return true;
        }
    },
    // ** SCHEDULE KASTER SEGMENT DATA **
    setServicesScheduleMasterSegmentData: function(masterList) {
        kony.print("Inside setServicesScheduleMasterSegmentData() of frmServiceManagementController");
        var scopeObj = this;
        var dataMap = {
            "imgCheckbox": "imgCheckbox",
            "lblMasterData": "lblMasterData"
        };
        this.view.segMasterListContent.widgetDataMap = dataMap;
        this.view.segMasterListContent.data = [];
        var segMasterList = this.view.segMasterListContent.data;
        var records = masterList.records;
        kony.print("records.length: " + records.length);
        for (var i = 0; i < records.length; ++i) {
            var toAdd = {
                "imgCheckbox": {
                    "src": "radionormal_1x.png"
                },
                "lblMasterData": {
                    "text": records[i].masterData
                },
                "flxServicesScheduleMasterContent": {
                    "onClick": scopeObj.masterDataClick()
                }
            };
            segMasterList.push(toAdd);
        }
        this.view.segMasterListContent.setData(segMasterList);
        this.view.forceLayout();
    },
    masterDataClick: function() {
        kony.print("Inside masterDataClick() of frmServiceManagementController");
        var scopeObj = this;
        var selectedItem = this.view.segMasterListContent.selectedItems[0];
        var selectedIndex = this.view.segMasterListContent.selectedIndex[1];
        var selectedRowIndex = this.view.segMasterListContent.selectedRowIndex[1];
        kony.print("selectedItem: " + selectedItem);
        kony.print("selectedIndex: " + selectedIndex);
        kony.print("selectedRowIndex: " + selectedRowIndex);
    },
    // ** SCHEDULE KASTER SEGMENT DATA **
    /*
     * function to clear form data for add service
     */
    clearData: function() {
        var self = this;
        self.view.tbxServiceCode.text = "";
        self.view.tbxDisplayName.text = "";
        self.view.txtareaDisplayDescription.text = "";
        self.view.tbxServiceName.text = "";
        self.view.txtareaServiceDescription.text = "";
        self.view.tbxSmsAlertCharge.text = "";
        self.view.tbxSmsAlertCharge.placeholder = "0.00";
        self.view.tbxBeneficiarySmsAlertCharge.text = "";
        self.view.tbxBeneficiarySmsAlertCharge.placeholder = "0.00";
        self.view.rbServiceType.selectedKey = null;
        self.view.checkBoxServiceChannel.selectedKeys = null;
        self.view.rbCategory.selectedKey = null;
        self.view.switchFutureTransactionFlag.selectedIndex = null;
        self.view.switchOutageMessage.selectedIndex = null;
        self.view.switchTermsAndConditions.selectedIndex = null;
        self.view.switchAgreement.selectedIndex = null;
        self.view.switchTacRequired.selectedIndex = null;
        self.view.customSwitchSmsAlert.switchToggle.selectedIndex = null;
        self.view.customSwitchBeneficiarySmsAlert.switchToggle.selectedIndex = null;
        self.clearInlineErrors();
    },
    /*
     *function to clear inline  errors
     */
    clearInlineErrors: function() {
        var self = this;
        self.view.tbxServiceCode.skin = "skntbxLato35475f14px";
        self.view.flxNoServiceCodeError.isVisible = false;
        self.view.tbxServiceName.skin = "skntbxLato35475f14px";
        self.view.flxNoServiceNameError.isVisible = false;
        self.view.txtareaServiceDescription.skin = "skntxtAreaLato35475f14Px";
        self.view.flxNoServiceDescriptionError.isVisible = false;
        self.view.tbxDisplayName.skin = "skntbxLato35475f14px";
        self.view.flxNoDisplayNameError.isVisible = false;
        self.view.txtareaDisplayDescription.skin = "skntxtAreaLato35475f14Px";
        self.view.flxNoDisplayDescriptionError.isVisible = false;
        self.view.flxBenfChargeTextbxContainer.skin = "sknflxffffffoptemplateop3px";
        self.view.flxNoBeneficiarySmsAlertChargeError.isVisible = false;
        self.view.flxChargeTextbxContainer.skin = "sknflxffffffoptemplateop3px";
        self.view.flxNoSmsAlertChargeError.isVisible = false;
        self.view.tbxLimit1.skin = "CopyslTextBox0b4bc21adeecd47";
        self.view.lblNoLimitDefineError1.isVisible = false;
        self.view.tbxLimit2.skin = "CopyslTextBox0b4bc21adeecd47";
        self.view.lblNoLimitDefineError2.isVisible = false;
        self.view.tbxLimit3.skin = "CopyslTextBox0b4bc21adeecd47";
        self.view.lblNoLimitDefineError3.isVisible = false;
        self.view.flxNoServiceTypeError.isVisible = false;
        self.view.flxNoCategoryError.isVisible = false;
        self.view.flxNoServiceChannelError.isVisible = false;
    },
    /*
     * function to call presentation controller to get all master data
     */
    getMasterDataservices: function() {
        var self = this;
        self.presenter.getAllServiceMasterData();
    },
    /*
     * function to set all the master data for required widgets in add service form
     */
    addServicesMasterDataMappings: function(serviceMasterData) {
        var self = this;
        var serviceChannelData = [
            []
        ];
        var serviceTypeData = [
            []
        ];
        var categoryData = [
            []
        ];
        if (serviceMasterData.serviceChannel) {
            for (var i = 0; i < serviceMasterData.serviceChannel.length; i++) {
                if (serviceMasterData.serviceChannel[i].id == "CH_ID_INT" || serviceMasterData.serviceChannel[i].id == "CH_ID_MOB" || serviceMasterData.serviceChannel[i].id == "CH_ID_MOB_INT") serviceChannelData[i] = [serviceMasterData.serviceChannel[i].id, serviceMasterData.serviceChannel[i].Description];
            }
            self.view.checkBoxServiceChannel.masterData = serviceChannelData;
        }
        if (serviceMasterData.serviceType) {
            for (var j = 0; j < serviceMasterData.serviceType.length; j++) serviceTypeData[j] = [serviceMasterData.serviceType[j].id, serviceMasterData.serviceType[j].Description];
            self.view.rbServiceType.masterData = serviceTypeData;
        }
        if (serviceMasterData.serviceCategory) {
            for (var k = 0; k < serviceMasterData.serviceCategory.length; k++) categoryData[k] = [serviceMasterData.serviceCategory[k].id, serviceMasterData.serviceCategory[k].Name];
            //     var categoryData = [["CAT_TYPE_PAYMENT", kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Payment")], ["CAT_TYPE_TRANSFER", kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Transfer")], ["CAT_TYPE_DEPOSIT", kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Deposit")]];
            self.view.rbCategory.masterData = categoryData;
        }
    },
    /*
     * function to store service details entered
     */
    setServiceDetails: function() {
        var self = this;
        var statusId = "";
        if (self.view.customServiceStatusSwitch.switchToggle.selectedIndex === 0) {
            statusId = self.statusConfig.active;
        } else {
            statusId = self.statusConfig.inactive;
        }
        var channelId, temp1;
        var temp = [];
        if (self.view.checkBoxServiceChannel.selectedKeys !== null) {
            for (var i = 0; i < self.view.checkBoxServiceChannel.selectedKeys.length; i++) {
                temp.push(self.view.checkBoxServiceChannel.selectedKeys[i]);
            }
            temp1 = JSON.stringify(temp);
            channelId = temp1.replace(/"/g, "'");
        }
        self.createServiceReqParam = {
            "User_id": kony.mvc.MDAApplication.getSharedInstance().appContext.userID,
            "Service": [{
                "Type_id": self.view.rbServiceType.selectedKey,
                "Channel_id": channelId,
                "Code": (self.view.tbxServiceCode.text === "") ? "1" : self.view.tbxServiceCode.text,
                "Category_Id": self.view.rbCategory.selectedKey,
                "DisplayName": (self.view.tbxDisplayName.text === "") ? kony.i18n.getLocalizedString("i18n.frmServiceManagementController.display_name") : self.view.tbxDisplayName.text,
                "DisplayDescription": (self.view.txtareaDisplayDescription.text === "") ? kony.i18n.getLocalizedString("i18n.frmServiceManagementController.display_desc") : self.view.txtareaDisplayDescription.text,
                "Name": self.view.tbxServiceName.text,
                "Description": self.view.txtareaServiceDescription.text,
                "Status_id": statusId,
                "MaxTransferLimit": "5000",
                "MinTransferLimit": "1",
                "TransferDenominations": "100",
                "IsFutureTransaction": self.getToggleSelectedValueToString(self.view.switchFutureTransactionFlag.selectedIndex),
                "TransactionCharges": "20",
                "IsAuthorizationRequired": "0",
                "IsSMSAlertActivated": self.getToggleSelectedValueToString(self.view.customSwitchSmsAlert.switchToggle.selectedIndex),
                "SMSCharges": (self.view.tbxSmsAlertCharge.text === "") ? "0" : self.view.tbxSmsAlertCharge.text,
                "IsBeneficiarySMSAlertActivated": self.getToggleSelectedValueToString(self.view.customSwitchBeneficiarySmsAlert.selectedIndex),
                "BeneficiarySMSCharge": (self.view.tbxBeneficiarySmsAlertCharge.text === "") ? "0" : self.view.tbxBeneficiarySmsAlertCharge.text,
                "HasWeekendOperation": "0",
                "IsOutageMessageActive": self.getToggleSelectedValueToString(self.view.switchOutageMessage.selectedIndex),
                "IsAlertActive": "0",
                "IsTCActive": (self.view.switchTermsAndConditions.selectedIndex === null) ? "0" : self.getToggleSelectedValueToString(self.view.switchTermsAndConditions.selectedIndex),
                "IsAgreementActive": self.getToggleSelectedValueToString(self.view.switchAgreement.selectedIndex),
                "IsCampaignActive": "1",
                "WorkSchedule_id": "WORK_SCH_ID1",
                "TransactionLimit_id": "TID1"
            }],
            "transactionFeeName": kony.i18n.getLocalizedString("i18n.frmServiceManagementController.IMPS"),
            "transactionFeeDescription": "desc",
            "transferFeeSlabs": [],
            "periodLimits": []
        };
    },
    /*
     * function to call presentation controller to fetch all services
     */
    getAllServices: function() {
        var self = this;
        self.presenter.fetchAllServices();
    },
    /*
     * function to call update function of presentation controller
     * @ param : edited data request param
     */
    updateServiceDetails: function(editReqParam) {
        var self = this;
        var condition = self.validation();
        if (condition) {
            self.presenter.updateService(editReqParam);
            self.hideService();
        }
    },
    /*
     * function to prefill service details form
     */
    prefillServiceDataForEdit: function() {
        var self = this;
        var index = self.view.listingSegmentClient.segListing.selectedIndex;
        var rowInd = index[1];
        var rowData = self.view.listingSegmentClient.segListing.data[rowInd];
        self.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Edit_Service");
        self.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmServiceManagementController.SERVICES");
        self.view.breadcrumbs.lblCurrentScreen.text = (rowData.lblServicesName.text).toUpperCase();
        self.view.rbServiceType.selectedKey = rowData.Type_id;
        self.view.tbxServiceCode.text = self.checkForNull(rowData.lblServicesCode.info.value, "");
        self.view.tbxServiceName.text = rowData.lblServicesName.text;
        self.view.txtareaServiceDescription.text = rowData.Description;
        self.view.tbxDisplayName.text = self.checkForNull(rowData.DisplayName, "");
        self.view.lblServiceNameCount.setVisibility(false);
        self.view.lblServiceDisplayNameCount.setVisibility(false);
        self.view.lblDescriptionCount.setVisibility(false);
        self.view.lblDisplayDescriptionCount.setVisibility(false);
        self.view.txtareaDisplayDescription.text = self.checkForNull(rowData.DisplayDesc, "");
        self.view.rbCategory.selectedKey = self.checkForNull(rowData.Category_Id, null);
        self.view.customServiceStatusSwitch.switchToggle.selectedIndex = rowData.Status_id === self.statusConfig.active ? 0 : 1;
        self.view.checkBoxServiceChannel.selectedKeys = rowData.Channel_Id.split(",");
        self.view.customSwitchSmsAlert.switchToggle.selectedIndex = (rowData.IsSMSAlertActivated === "true") ? 0 : 1;
        self.view.tbxSmsAlertCharge.text = self.checkForNull(rowData.SMSCharges, "");
        self.view.customSwitchBeneficiarySmsAlert.switchToggle.selectedIndex = (rowData.IsBeneficiarySMSAlertActivated === "true") ? 0 : 1;
        self.view.tbxBeneficiarySmsAlertCharge.text = self.checkForNull(rowData.BeneficiarySMSCharge, "");
        self.view.switchFutureTransactionFlag.selectedIndex = (rowData.IsFutureTransaction === "true") ? 0 : 1;
        self.view.switchOutageMessage.selectedIndex = (rowData.IsOutageMessageActive === "true") ? 0 : 1;
        self.view.switchTermsAndConditions.selectedIndex = (rowData.IsTCActive === "true") ? 0 : 1;
        self.view.switchAgreement.selectedIndex = (rowData.IsAgreementActive === "true") ? 0 : 1;
        if (rowData.IsBeneficiarySMSAlertActivated === "true") {
            self.view.tbxBeneficiarySmsAlertCharge.setEnabled(true);
        } else {
            self.view.tbxBeneficiarySmsAlertCharge.setEnabled(false);
        }
        if (rowData.IsSMSAlertActivated === "true") {
            self.view.tbxSmsAlertCharge.setEnabled(true);
        } else {
            self.view.tbxSmsAlertCharge.setEnabled(false);
        }
    },
    /*
     *null check function
     *@param: value to compare, default value to return
     *@return
     */
    checkForNull: function(value, defaultReturnValue) {
        if (value === null || value === undefined || value === "N/A" || value === "-" || value === "") return defaultReturnValue;
        else return value;
    },
    /*
     * function to set data for edit request param
     * @param : service_id
     */
    setEditServiceDetails: function(serviceId) {
        var self = this;
        var statusId = "";
        if (self.view.customServiceStatusSwitch.switchToggle.selectedIndex === 0) {
            statusId = self.statusConfig.active;
        } else {
            statusId = self.statusConfig.inactive;
        }
        var channelId, temp1;
        var temp = [];
        if (self.view.checkBoxServiceChannel.selectedKeys !== null) {
            for (var i = 0; i < self.view.checkBoxServiceChannel.selectedKeys.length; i++) {
                temp.push(self.view.checkBoxServiceChannel.selectedKeys[i]);
            }
            temp1 = JSON.stringify(temp);
            channelId = temp1.replace(/"/g, "'");
        }
        var ind = self.view.listingSegmentClient.segListing.selectedIndex;
        var rowInd = ind[1];
        var rowData = self.view.listingSegmentClient.segListing.data[rowInd];
        if (serviceId !== null) {
            self.editServiceReqParam = {
                "User_id": kony.mvc.MDAApplication.getSharedInstance().appContext.userID,
                "editedEntitlement": [{
                    "Service_id": serviceId,
                    "Type_id": self.view.rbServiceType.selectedKey,
                    "Channel_id": channelId,
                    "Code": (self.view.tbxServiceCode.text === "") ? "1" : self.view.tbxServiceCode.text,
                    "Category_Id": self.view.rbCategory.selectedKey,
                    "DisplayName": (self.view.tbxDisplayName.text === "") ? "DisplayName" : self.view.tbxDisplayName.text,
                    "DisplayDescription": (self.view.txtareaDisplayDescription.text === "") ? "name" : self.view.txtareaDisplayDescription.text,
                    "Name": self.view.tbxServiceName.text,
                    "Description": self.view.txtareaServiceDescription.text,
                    "Status_id": statusId,
                    "MaxTransferLimit": rowData.MaxTransferLimit || "5000",
                    "MinTransferLimit": rowData.MinTransferLimit || "1",
                    "TransferDenominations": "100",
                    "IsFutureTransaction": self.getToggleSelectedValueToString(self.view.switchFutureTransactionFlag.selectedIndex),
                    "TransactionCharges": "20",
                    "IsAuthorizationRequired": "0",
                    "IsSMSAlertActivated": self.getToggleSelectedValueToString(self.view.customSwitchSmsAlert.switchToggle.selectedIndex),
                    "SMSCharges": (self.view.tbxSmsAlertCharge.text === "") ? "0" : self.view.tbxSmsAlertCharge.text,
                    "IsBeneficiarySMSAlertActivated": self.getToggleSelectedValueToString(self.view.customSwitchBeneficiarySmsAlert.selectedIndex),
                    "BeneficiarySMSCharge": (self.view.tbxBeneficiarySmsAlertCharge.text === "") ? "0" : self.view.tbxBeneficiarySmsAlertCharge.text,
                    "HasWeekendOperation": "0",
                    "IsOutageMessageActive": self.getToggleSelectedValueToString(self.view.switchOutageMessage.selectedIndex),
                    "IsAlertActive": "1",
                    "IsTCActive": self.getToggleSelectedValueToString(self.view.switchTermsAndConditions.selectedIndex),
                    "IsAgreementActive": self.getToggleSelectedValueToString(self.view.switchAgreement.selectedIndex),
                    "IsCampaignActive": "0",
                    "WorkSchedule_id": rowData.WorkSchedule_id,
                    "TransactionFee_id": rowData.TransactionFee_id,
                    "TransactionLimit_id": rowData.TransactionLimit_id
                }],
                "periodLimits": [],
                "transferFeeSlabs": []
            };
        }
    },
    /*
     * function called on click of download csv
     */
    downloadServicesCSV: function() {
        kony.print("Inside downloadServicesCSV() of frmServiceManagementController");
        var scopeObj = this;
        var authToken = KNYMobileFabric.currentClaimToken;
        var mfURL = KNYMobileFabric.mainRef.config.reportingsvc.session.split("/services")[0];
        var downloadURL = mfURL + "/services/data/v1/StaticContentObjService/operations/service/downloadServicesList?authToken=" + authToken;
        if (scopeObj.view.subHeader.tbxSearchBox.text !== "") {
            downloadURL = downloadURL + "&searchText=" + scopeObj.view.subHeader.tbxSearchBox.text;
        }
        var downloadServicesFilterJSON = scopeObj.view.mainHeader.btnDropdownList.info;
        if (downloadServicesFilterJSON !== undefined && downloadServicesFilterJSON.selectedStatusList !== undefined) {
            var status = "&status=" + downloadServicesFilterJSON.selectedStatusList;
            downloadURL = downloadURL + status;
        }
        var encodedURI = encodeURI(downloadURL);
        var downloadLink = document.createElement("a");
        downloadLink.href = encodedURI;
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    },
    /*
     * function to set data to view service details screen, called on click of seg row
     */
    setViewServiceData: function() {
        var index = this.view.listingSegmentClient.segListing.selectedIndex;
        var rowIndex = index[1];
        var rowData = this.view.listingSegmentClient.segListing.data[rowIndex];
        this.view.breadcrumbs.lblCurrentScreen.text = (rowData.lblServicesName.text).toUpperCase();
        this.view.lblSpecialBranch.text = this.checkForNull(rowData.lblServicesName.text, "N/A");
        this.view.lblViewValue.text = this.checkForNull(rowData.lblServicesStatus.text, "N/A");
        this.view.lblViewValue.skin = rowData.lblServicesStatus.skin;
        this.view.lblIconViewKey.text = "\ue921";
        this.view.lblIconViewKey.skin = rowData.lblIconStatus.skin;
        var detailsServicetext = this.checkForNull(rowData.lblServicesCode.info.value, "N/A");
        this.view.lblDetailsServiceCodeContent.text = this.AdminConsoleCommonUtils.getTruncatedString(detailsServicetext, 30, 28);
        this.view.lblDetailsServiceCodeContent.toolTip = detailsServicetext.length > 30 ? detailsServicetext : "";
        detailsServicetext = this.checkForNull(rowData.DisplayName, "N/A");
        this.view.lblDetailsServiceDisplayNameContent.text = this.AdminConsoleCommonUtils.getTruncatedString(detailsServicetext, 30, 28);
        this.view.lblDetailsServiceDisplayNameContent.toolTip = detailsServicetext.length > 30 ? detailsServicetext : "";
        this.view.lblDetailsServiceCategoryContent.text = this.checkForNull(rowData.lblServicesCategory.text, "N/A");
        this.view.lblDetailsServiceChannelContent.text = this.checkForNull(rowData.lblServicesSupportedChannels.text, "N/A");
        this.view.lblDetailsServiceSmsAlertContent.text = (rowData.IsSMSAlertActivated === "true") ? kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Yes") : "No";
        this.view.lblDetailsServiceBeneficiarySmsAlertContent.text = (rowData.IsBeneficiarySMSAlertActivated === "true") ? kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Yes") : "No";
        this.view.lblDetailsServiceTypeContent.text = this.checkForNull(rowData.lblServicesType.text, "N/A");
        this.view.lblDetailsServiceSmsChargeContent.text = this.checkForNull(this.getCurrencyFormat(rowData.SMSCharges), "N/A");
        this.view.lblDetailsServiceSmsChargeSymbol.text = (this.view.lblDetailsServiceSmsChargeContent.text === "N/A") ? "" : this.defaultCurrencyCode();
        this.view.lblDetailsServiceBSCContent.text = this.checkForNull(this.getCurrencyFormat(rowData.BeneficiarySMSCharge), "N/A");
        this.view.lblDetailsServiceBSCSymbol.text = (this.view.lblDetailsServiceBSCContent.text === "N/A") ? "" : this.defaultCurrencyCode();
        this.view.lblDetailsServiceFTFContent.text = (rowData.IsFutureTransaction === "true") ? kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Yes") : "No";
        this.view.lblDetailsServiceOutageMsgContent.text = (rowData.IsOutageMessageActive === "true") ? kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Yes") : "No";
        this.view.lblDetailsServiceTACContent.text = (rowData.IsTCActive === "true") ? kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Yes") : "No";
        this.view.lblDetailsServiceTACRequiredContent.text = "N/A";
        this.view.lblDetailsServiceAgreementContent.text = (rowData.IsAgreementActive === "true") ? kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Yes") : "No";
        this.view.lblDetailsServiceScheduleContent.text = this.checkForNull(rowData.WorkSchedule_Desc, "N/A");
        this.view.lblServiceDescriptionContent.text = this.checkForNull(rowData.Description, "N/A");
        this.view.lblDisplayDescriptionContent.text = this.checkForNull(rowData.DisplayDesc, "N/A");
        //setting visibility false as not implementing now
        this.view.flxViewServiceData2.setVisibility(false);
    },
    /*
     * function called on status change to active/deactive
     * @param : row data , updated new status id
     */
    changeStatusOfService: function(rowData, updatedStatus) {
        var self = this;
        var editStatusReqParam = {
            "User_id": kony.mvc.MDAApplication.getSharedInstance().appContext.userID,
            "Service_id": rowData.id,
            "Status_id": updatedStatus,
        };
        self.callUpdateStatus(editStatusReqParam);
    },
    /*
     * function to call presentation controller to change status of service
     */
    callUpdateStatus: function(param) {
        var self = this;
        self.presenter.updateStatusOfService(param);
    },
    sortIconFor: function(column) {
        return this.determineSortIcon(this.sortBy, column);
    },
    /*
     * function to reset all the sort images in groups list page
     */
    resetSortImages: function() {
        var self = this;
        self.determineSortFontIcon(self.sortBy, 'Name', this.view.lblSortName);
        self.determineSortFontIcon(self.sortBy, 'Code', this.view.lblSortCode);
        self.determineSortFontIcon(self.sortBy, 'Category_Name', this.view.lblSortCategory);
        self.determineSortFontIcon(self.sortBy, 'Channel', this.view.lblFilterSC);
    },
    validation: function() {
        var returnValue = true;
        if (this.view.rbServiceType.selectedKey === null) {
            this.view.flxNoServiceTypeError.isVisible = true;
            returnValue = false;
        }
        if (this.view.tbxServiceCode.text === "") {
            this.view.tbxServiceCode.skin = "skinredbg";
            this.view.flxNoServiceCodeError.isVisible = true;
            returnValue = false;
        }
        if (this.view.tbxServiceName.text === "") {
            this.view.tbxServiceName.skin = "skinredbg";
            this.view.flxNoServiceNameError.isVisible = true;
            returnValue = false;
        }
        if (this.view.txtareaServiceDescription.text === "") {
            this.view.txtareaServiceDescription.skin = "skinredbg";
            this.view.flxNoServiceDescriptionError.isVisible = true;
            returnValue = false;
        }
        if (this.view.tbxDisplayName.text === "") {
            this.view.tbxDisplayName.skin = "skinredbg";
            this.view.flxNoDisplayNameError.isVisible = true;
            returnValue = false;
        }
        if (this.view.txtareaDisplayDescription.text === "") {
            this.view.txtareaDisplayDescription.skin = "skinredbg";
            this.view.flxNoDisplayDescriptionError.isVisible = true;
            returnValue = false;
        }
        if (this.view.rbCategory.selectedKey === null) {
            this.view.flxNoCategoryError.isVisible = true;
            returnValue = false;
        }
        if (this.view.checkBoxServiceChannel.selectedKeys === null) {
            this.view.flxNoServiceChannelError.isVisible = true;
            returnValue = false;
        }
        if (this.view.customSwitchBeneficiarySmsAlert.switchToggle.selectedIndex === 0) {
            if (this.view.tbxBeneficiarySmsAlertCharge.text === "") {
                this.view.flxBenfChargeTextbxContainer.skin = "sknFlxCalendarError";
                this.view.lblNoBeneficiarySmsAlertChargeError.text = kony.i18n.getLocalizedString("i18n.frmServiceManagementController.lblNoBeneficiarySmsAlertCharge");
                this.view.flxNoBeneficiarySmsAlertChargeError.isVisible = true;
                returnValue = false;
            } else if (/^[-]\d+(\.\d{1,2})?$/.test(this.view.tbxBeneficiarySmsAlertCharge.text) === true) {
                this.view.flxBenfChargeTextbxContainer.skin = "sknFlxCalendarError";
                this.view.lblNoBeneficiarySmsAlertChargeError.text = kony.i18n.getLocalizedString("i18n.frmServiceManagement.Charge_cannot_have_negative_value");
                this.view.flxNoBeneficiarySmsAlertChargeError.isVisible = true;
                returnValue = false;
            } else if (/^\d+(\.\d{1,2})?$/.test(this.view.tbxBeneficiarySmsAlertCharge.text) === false) {
                this.view.flxBenfChargeTextbxContainer.skin = "sknFlxCalendarError";
                this.view.lblNoBeneficiarySmsAlertChargeError.text = kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Charge_cannot_exceed_decimals");
                this.view.flxNoBeneficiarySmsAlertChargeError.isVisible = true;
                returnValue = false;
            }
        }
        if (this.view.customSwitchSmsAlert.switchToggle.selectedIndex === 0) {
            if (this.view.tbxSmsAlertCharge.text === "") {
                this.view.flxChargeTextbxContainer.skin = "sknFlxCalendarError";
                this.view.lblNoSmsAlertChargeError.text = kony.i18n.getLocalizedString("i18n.frmServiceManagementController.SmsAlert_cannot_be_empty");
                this.view.flxNoSmsAlertChargeError.isVisible = true;
                returnValue = false;
            } else if (/^[-]\d+(\.\d{1,2})?$/.test(this.view.tbxSmsAlertCharge.text) === true) {
                this.view.flxChargeTextbxContainer.skin = "sknFlxCalendarError";
                this.view.lblNoSmsAlertChargeError.text = kony.i18n.getLocalizedString("i18n.frmServiceManagement.Charge_cannot_have_negative_value");
                this.view.flxNoSmsAlertChargeError.isVisible = true;
                returnValue = false;
            } else if (/^\d+(\.\d{1,2})?$/.test(this.view.tbxSmsAlertCharge.text) === false) {
                this.view.flxChargeTextbxContainer.skin = "sknFlxCalendarError";
                this.view.lblNoSmsAlertChargeError.text = kony.i18n.getLocalizedString("i18n.frmServiceManagementController.Charge_cannot_exceed_decimals");
                this.view.flxNoSmsAlertChargeError.isVisible = true;
                returnValue = false;
            }
        }
        if (this.view.tbxLimit1.text === "") {
            this.view.tbxLimit1.skin = "skinredbg";
            this.view.lblNoLimitDefineError1.isVisible = true;
            returnValue = false;
        }
        if (this.view.tbxLimit2.text === "") {
            this.view.tbxLimit2.skin = "skinredbg";
            this.view.lblNoLimitDefineError2.isVisible = true;
            returnValue = false;
        }
        if (this.view.tbxLimit3.text === "") {
            this.view.tbxLimit3.skin = "skinredbg";
            this.view.lblNoLimitDefineError3.isVisible = true;
            returnValue = false;
        }
        return returnValue;
    },
    performStatusFilter: function() {
        var self = this;
        var selStatus = [];
        var selInd;
        var dataToShow = [];
        var allData = self.servicesRecList;
        var segStatusData = self.view.statusFilterMenu.segStatusFilterDropdown.data;
        var indices = self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices;
        if (indices !== null) { //show selected indices data
            selInd = indices[0][1];
            var statuses = "";
            for (var i = 0; i < selInd.length; i++) {
                selStatus.push(self.view.statusFilterMenu.segStatusFilterDropdown.data[selInd[i]].lblDescription);
                statuses = statuses + self.view.statusFilterMenu.segStatusFilterDropdown.data[selInd[i]].lblDescription + "_";
            }
            if (self.view.mainHeader.btnDropdownList.info === undefined) {
                self.view.mainHeader.btnDropdownList.info = {};
            }
            self.view.mainHeader.btnDropdownList.info.selectedStatusList = statuses.substring(0, statuses.length - 1);
            self.view.listingSegmentClient.rtxNoResultsFound.setVisibility(false);
            self.view.listingSegmentClient.flxPagination.setVisibility(true);
            if (selInd.length === segStatusData.length) { //all are selected
                self.setServicesSegmentData(self.servicesRecList, true);
            } else {
                dataToShow = allData.filter(function(rec) {
                    if (selStatus.indexOf(rec.Status) >= 0) {
                        return rec;
                    }
                });
                if (dataToShow.length > 0) {
                    self.view.listingSegmentClient.segListing.isVisible = true;
                    self.setServicesSegmentData(dataToShow, true);
                } else {
                    self.view.listingSegmentClient.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
                    self.view.listingSegmentClient.rtxNoResultsFound.setVisibility(true);
                    self.view.listingSegmentClient.flxPagination.setVisibility(false);
                    self.view.listingSegmentClient.segListing.setData([]);
                }
            }
        } else {
            self.view.listingSegmentClient.segListing.isVisible = false;
            self.view.listingSegmentClient.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
            self.view.listingSegmentClient.rtxNoResultsFound.setVisibility(true);
            self.view.listingSegmentClient.flxPagination.setVisibility(false);
            self.view.listingSegmentClient.segListing.setData([]);
        }
    },
    /*
     *on hover event for  hiding dropdown visibility
     */
    onHoverEventCallback: function(widget, context) {
        var scopeObj = this;
        var widGetId = widget.id;
        if (widGetId === "flxServiceStatusFilter") { //for filter dropdown
            if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
                scopeObj.view.flxServiceStatusFilter.setVisibility(true);
            } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                scopeObj.view.flxServiceStatusFilter.setVisibility(false);
            }
        } else if (widGetId === "flxContextualMenu") {
            if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
                scopeObj.view.listingSegmentClient.flxContextualMenu.setVisibility(true);
            } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                scopeObj.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
            }
        }
    },
    filterFurtherBasedOnStatus: function(serviceData) {
        var self = this;
        var selStatus = [];
        var selInd;
        var dataToShow = [];
        var indices = self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices;
        if (indices !== null) { //show selected indices data
            selInd = indices[0][1];
            for (var i = 0; i < selInd.length; i++) {
                selStatus.push(self.view.statusFilterMenu.segStatusFilterDropdown.data[selInd[i]].lblDescription);
            }
            dataToShow = serviceData.filter(function(rec) {
                if (selStatus.indexOf(rec.Status) >= 0) {
                    return rec;
                }
            });
            if (dataToShow.length === 0) {
                dataToShow = [];
            }
        } else {
            dataToShow = [];
        }
        return dataToShow;
    },
    /*
     * get string value for backend, based on selected switch value
     */
    getToggleSelectedValueToString: function(index) {
        if (index !== null && index === 0) {
            return "1";
        } else {
            return "0";
        }
    },
    sortData: function(sortColumn) {
        var scopeObj = this;
        var sortOrder = (scopeObj.sortBy && sortColumn === scopeObj.sortBy.columnName) ? !scopeObj.sortBy.inAscendingOrder : true;
        scopeObj.sortBy = scopeObj.getObjectSorter(sortColumn);
        scopeObj.sortBy.inAscendingOrder = sortOrder;
        scopeObj.getObjectSorter().column(sortColumn);
        var data = scopeObj.view.listingSegmentClient.segListing.data.sort(scopeObj.sortBy.sortData);
        scopeObj.view.listingSegmentClient.segListing.setData(data);
        scopeObj.resetSortImages();
    }
});
define("ServicesManagementModule/frmServiceManagementControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmServiceManagement **/
    AS_Form_g197182751ef45b7ab5605a6f65d849d: function AS_Form_g197182751ef45b7ab5605a6f65d849d(eventobject) {
        var self = this;
        this.preShowActions();
    },
    /** onDeviceBack defined for frmServiceManagement **/
    AS_Form_d95f5c87fabe4c4d9a1bb01a1661012d: function AS_Form_d95f5c87fabe4c4d9a1bb01a1661012d(eventobject) {
        var self = this;
        //registered
        this.onBrowserBack();
    }
});
define("ServicesManagementModule/frmServiceManagementController", ["ServicesManagementModule/userfrmServiceManagementController", "ServicesManagementModule/frmServiceManagementControllerActions"], function() {
    var controller = require("ServicesManagementModule/userfrmServiceManagementController");
    var controllerActions = ["ServicesManagementModule/frmServiceManagementControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
