define([], function() {
    function ServicesManagementModule_BusinessController() {
        kony.mvc.Business.Delegator.call(this);
    }
    inheritsFrom(ServicesManagementModule_BusinessController, kony.mvc.Business.Delegator);
    ServicesManagementModule_BusinessController.prototype.initializeBusinessController = function() {};
    /**
     * @name getServices
     * @member ServicesManagementModule.businessController
     * @param {} payload
     * @param (response:{Services : [{Description : object, DisplayDescription : object, WorkSchedule_Desc : object, IsSMSAlertActivated : object, IsAgreementActive : object, BeneficiarySMSCharge : object, TransactionFee_id : object, Channel_id : object, IsFutureTransaction : object, Name : object, IsOutageMessageActive : object, IsBeneficiarySMSAlertActivated : object, MinTransferLimit : object, MaxTransferLimit : object, createdby : object, DisplayName : object, HasWeekendOperation : object, id : object, TransactionCharges : object, TransactionFees : object, Status : object, Type_Name : object, Status_id : object, Channel : object, IsAuthorizationRequired : object, SMSCharges : object, TransferDenominations : object, Category_Name : object, IsCampaignActive : object, Code : object, WorkSchedule_id : object, Category_Id : object, IsTCActive : object, IsAlertActive : object, TransactionLimit_id : object, Type_id : object, PeriodicLimits : object}], opstatus : number, httpStatusCode : number, httpresponse : {headers : string, url : string, responsecode : number}})=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    ServicesManagementModule_BusinessController.prototype.getServices = function(payload, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StaticContentManager").businessController.getServices(payload, onSuccess, onError);
    };
    ServicesManagementModule_BusinessController.prototype.createService = function(createData, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StaticContentManager").businessController.createService(createData, onSuccess, onError);
    };
    ServicesManagementModule_BusinessController.prototype.updateService = function(editedParamReq, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StaticContentManager").businessController.updateService(editedParamReq, onSuccess, onError);
    };
    ServicesManagementModule_BusinessController.prototype.updateServiceStatus = function(editedParamReq, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StaticContentManager").businessController.updateServiceStatus(editedParamReq, onSuccess, onError);
    };
    /**
     * @name getServiceTypes
     * @member ServicesManagementModule.businessController
     * @param {} payload
     * @param (response:{opstatus : number, serviceTypes : [{Description : object, id : object}], httpStatusCode : number, httpresponse : {headers : string, url : string, responsecode : number}})=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    ServicesManagementModule_BusinessController.prototype.getServiceTypes = function(payload, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StaticContentManager").businessController.getServiceTypes(payload, onSuccess, onError);
    };
    /**
     * @name getServiceChannels
     * @member ServicesManagementModule.businessController
     * @param {} payload
     * @param (response:{ServiceChannels : [{Description : object, id : object}], opstatus : number, httpStatusCode : number, httpresponse : {headers : string, url : string, responsecode : number}})=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    ServicesManagementModule_BusinessController.prototype.getServiceChannels = function(payload, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StaticContentManager").businessController.getServiceChannels(payload, onSuccess, onError);
    };
    /**
     * @name getServiceCategories
     * @member ServicesManagementModule.businessController
     * @param {} payload
     * @param (response:{Category : [{id : object, Name : object}], opstatus : number, httpStatusCode : number, httpresponse : {headers : string, url : string, responsecode : number}})=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    ServicesManagementModule_BusinessController.prototype.getServiceCategories = function(payload, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StaticContentManager").businessController.getServiceCategories(payload, onSuccess, onError);
    };
    ServicesManagementModule_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return ServicesManagementModule_BusinessController;
});