define(['ErrorInterceptor', 'ErrorIsNetworkDown'], function(ErrorInterceptor, isNetworkDown) {
    function ServicesManagement_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }
    inheritsFrom(ServicesManagement_PresentationController, kony.mvc.Presentation.BasePresenter);
    ServicesManagement_PresentationController.prototype.initializePresentationController = function() {
        var self = this;
        ErrorInterceptor.wrap(this, 'businessController').match(function(on) {
            return [
                on(isNetworkDown).do(function() {
                    self.presentUserInterface("frmServiceManagement", {
                        NetworkDownMessage: {}
                    });
                })
            ];
        });
    };
    /**
     * @name showServiceManagement
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.showServiceManagement = function() {
        var self = this;
        //self.presentUserInterface("frmServiceManagement");
        self.fetchAllServices();
    };
    /**
     * @name fetchAllServices
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.fetchAllServices = function() {
        var self = this;

        function successCallback(response) {
            self.showServiceMangScreen({
                servicesList: response.Services
            });
            self.hideLoadingScreen();
        }

        function failureCallback(error) {
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
            self.hideLoadingScreen();
        }
        self.showLoadingScreen();
        self.businessController.getServices({}, successCallback, failureCallback);
    };
    /*
     * function to call command handler to create new service
     * @param : create service request
     */
    ServicesManagement_PresentationController.prototype.createService = function(createData) {
        var self = this;

        function successCallback(response) {
            self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.frmServiceManagement.Service_created_msg"), "success");
            self.fetchAllServices();
        }

        function failureCallback(response) {
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(response), "error");
            self.hideLoadingScreen();
        }
        self.showLoadingScreen();
        self.businessController.createService(createData, successCallback, failureCallback);
    };
    /*
     * function to call command handler to update a service
     * @param : edited request
     */
    ServicesManagement_PresentationController.prototype.updateService = function(editedParamReq) {
        var self = this;

        function successCallback(response) {
            self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.frmServiceManagement.Service_edited_msg"), "success");
            self.fetchAllServices();
        }

        function failureCallback(response) {
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(response), "error");
            self.hideLoadingScreen();
        }
        self.showLoadingScreen();
        self.businessController.updateService(editedParamReq, successCallback, failureCallback);
    };
    /*
     * function to call command handler to update a service
     * @param : edited request
     */
    ServicesManagement_PresentationController.prototype.updateStatusOfService = function(editedParamReq) {
        var self = this;

        function successCallback(response) {
            self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.frmServiceManagement.Service_updated_msg"), "success");
            self.fetchAllServices();
        }

        function failureCallback(response) {
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(response), "error");
            self.hideLoadingScreen();
        }
        self.showLoadingScreen();
        self.businessController.updateServiceStatus(editedParamReq, successCallback, failureCallback);
    };
    /**
     * @name getAllServiceMasterData
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.getAllServiceMasterData = function() {
        var self = this;

        function successCallback(response) {
            self.showServiceMangScreen({
                masterDataServices: {
                    serviceType: response.serviceTypes
                }
            });
        }

        function failureCallback(response) {}
        self.businessController.getServiceTypes({}, successCallback, failureCallback);
        self.getAllServiceChannelData();
    };
    /**
     * @name getAllServiceChannelData
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.getAllServiceChannelData = function() {
        var self = this;

        function successCallback(response) {
            self.showServiceMangScreen({
                masterDataServices: {
                    serviceChannel: response.ServiceChannels
                }
            });
        }

        function failureCallback(response) {
            kony.print("Error fetching service channels", response);
        }
        self.businessController.getServiceChannels({}, successCallback, failureCallback);
        self.getAllServiceCatData();
    };
    /**
     * @name getAllServiceCatData
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.getAllServiceCatData = function() {
        var self = this;

        function successCallback(response) {
            self.showServiceMangScreen({
                masterDataServices: {
                    serviceCategory: response.category
                }
            });
        }

        function failureCallback(response) {}
        self.businessController.getServiceCategories({}, successCallback, failureCallback);
    };
    /**
     * @name showServiceMangScreen
     * @member ServicesManagementModule.presentationController
     * @param {servicesList : [{Description : object, DisplayDescription : object, WorkSchedule_Desc : object, IsSMSAlertActivated : object, IsAgreementActive : object, BeneficiarySMSCharge : object, TransactionFee_id : object, Channel_id : object, IsFutureTransaction : object, Name : object, IsOutageMessageActive : object, IsBeneficiarySMSAlertActivated : object, MinTransferLimit : object, MaxTransferLimit : object, createdby : object, DisplayName : object, HasWeekendOperation : object, id : object, TransactionCharges : object, TransactionFees : object, Status : object, Type_Name : object, Status_id : object, Channel : object, IsAuthorizationRequired : object, SMSCharges : object, TransferDenominations : object, Category_Name : object, IsCampaignActive : object, Code : object, WorkSchedule_id : object, Category_Id : object, IsTCActive : object, IsAlertActive : object, TransactionLimit_id : object, Type_id : object, PeriodicLimits : object}]} viewModel
     */
    ServicesManagement_PresentationController.prototype.showServiceMangScreen = function(viewModel) {
        var self = this;
        if (viewModel) {
            self.presentUserInterface("frmServiceManagement", viewModel);
        } else {
            self.presentUserInterface("frmServiceManagement");
        }
    };
    /**
     * @name showLoadingScreen
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.showLoadingScreen = function() {
        this.showServiceMangScreen({
            LoadingScreen: {
                show: true
            }
        });
    };
    /**
     * @name hideLoadingScreen
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.hideLoadingScreen = function() {
        this.showServiceMangScreen({
            LoadingScreen: {
                show: false
            }
        });
    };
    /*
     * common function to present user interface with Toast message
     */
    ServicesManagement_PresentationController.prototype.showToastMessageFlex = function(msg, status) {
        var self = this;
        self.presentUserInterface("frmServiceManagement", {
            toast: {
                message: msg,
                status: status
            }
        });
    };
    return ServicesManagement_PresentationController;
});