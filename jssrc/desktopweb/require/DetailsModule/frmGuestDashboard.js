define("DetailsModule/frmGuestDashboard", function() {
    return function(controller) {
        function addWidgetsfrmGuestDashboard() {
            this.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305dp",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxMain = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxScrlf5f6f8OP100",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 5
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false,
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "top": "58px"
                    },
                    "imgLogout": {
                        "isVisible": true
                    },
                    "lblHeading": {
                        "text": "Customer Management"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            mainHeader.imgLogout.onTouchStart = controller.AS_Image_a336b3eaad6b4580b9a8679a66806fac;
            var btnNotes = new kony.ui.Button({
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "30px",
                "id": "btnNotes",
                "isVisible": true,
                "right": "35px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnNotes\")",
                "top": "58dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxMainHeader.add(mainHeader, btnNotes);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SearchCustomers\")",
                        "left": "35px"
                    },
                    "lblCurrentScreen": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.Breadcrumbs.NewCustomer\")",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            breadcrumbs.btnBackToMain.onClick = controller.AS_Button_f5330200c5bf4bcdad11d8d2a31158f7;
            flxBreadCrumbs.add(breadcrumbs);
            var flxAlertMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAlertMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": 35,
                "skin": "sknFlxFFFFFF1000",
                "top": "130dp",
                "zIndex": 10
            }, {}, {});
            flxAlertMessage.setDefaultUnit(kony.flex.DP);
            var alertMessage = new com.adminConsole.customerMang.alertMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "alertMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "alertMessage": {
                        "height": "100%",
                        "isVisible": true,
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "btnDataLink1": {
                        "isVisible": false
                    },
                    "btnDataLink2": {
                        "isVisible": false
                    },
                    "lblData1": {
                        "centerY": "50%",
                        "left": "20px",
                        "text": "No customer Found",
                        "top": "15px"
                    },
                    "lblData2": {
                        "centerY": "50%",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.requestsAndNotificationsCountSufix\")",
                        "isVisible": false,
                        "left": "2px",
                        "top": "15px"
                    },
                    "lblData3": {
                        "centerY": "50%",
                        "left": "1px",
                        "top": "15px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertMessage.add(alertMessage);
            var flxHeaderAndMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "683dp",
                "id": "flxHeaderAndMainContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxBordere4e6ecCustomRadius5px",
                "top": "190dp",
                "zIndex": 10
            }, {}, {});
            flxHeaderAndMainContainer.setDefaultUnit(kony.flex.DP);
            var HeaderGuest = new com.adminConsole.common.HeaderGuest({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "HeaderGuest",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "3%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "40%",
                "overrides": {
                    "HeaderGuest": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "30dp",
                        "isVisible": true,
                        "left": "3%",
                        "right": "viz.val_cleared",
                        "top": "35dp",
                        "width": "40%"
                    },
                    "flxUserType": {
                        "width": "120dp"
                    },
                    "lblUserName": {
                        "top": "0dp"
                    },
                    "lblUserType": {
                        "text": "New Customer"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxGeneralInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxGeneralInfo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "3%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "71dp",
                "width": "25%",
                "zIndex": 100
            }, {}, {});
            flxGeneralInfo.setDefaultUnit(kony.flex.DP);
            var lblGenInfoStatic = new kony.ui.Label({
                "id": "lblGenInfoStatic",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblFont485C75100O",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.GenInfo\")",
                "top": "12dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxAddInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_e9b811ef795f473497a87fa8527d7891,
                "skin": "slFbox",
                "top": "12dp",
                "width": "31.25%",
                "zIndex": 100
            }, {}, {});
            flxAddInfo.setDefaultUnit(kony.flex.DP);
            var lblAddInfoStatic = new kony.ui.Label({
                "id": "lblAddInfoStatic",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl006CCA12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.EditInfo\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblHover006CCA12pxKA"
            });
            flxAddInfo.add(lblAddInfoStatic);
            flxGeneralInfo.add(lblGenInfoStatic, flxAddInfo);
            var flxSelectLoanType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxSelectLoanType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "48%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "71dp",
                "width": "23.67%",
                "zIndex": 100
            }, {}, {});
            flxSelectLoanType.setDefaultUnit(kony.flex.DP);
            var lblSelectLoanType = new kony.ui.Label({
                "id": "lblSelectLoanType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblFont485C75100O",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.SimulateOrApply\")",
                "top": "12dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectLoanType.add(lblSelectLoanType);
            var flxGuestDashboard = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGuestDashboard",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "120dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxGuestDashboard.setDefaultUnit(kony.flex.DP);
            var flxNoInformation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120dp",
                "id": "flxNoInformation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "3%",
                "isModalContainer": false,
                "skin": "sknFlxBordere1e5eedCustomRadius5px",
                "top": "0dp",
                "width": "40%",
                "zIndex": 100
            }, {}, {});
            flxNoInformation.setDefaultUnit(kony.flex.DP);
            var lblNoInfoAvailable = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "40%",
                "id": "lblNoInfoAvailable",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblFont485C75100O",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.NoInfoAvailable\")",
                "top": "12dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddingInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "60%",
                "clipBounds": true,
                "height": "15dp",
                "id": "flxAddingInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0ffad8531c1406e8870973ff83f5920,
                "skin": "slFbox",
                "top": "12dp",
                "width": "31.25%",
                "zIndex": 100
            }, {}, {});
            flxAddingInfo.setDefaultUnit(kony.flex.DP);
            var lblAddingInfo = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblAddingInfo",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl006CCAsize14pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.AddInfo\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblHover006CCAsize14pxKA"
            });
            flxAddingInfo.add(lblAddingInfo);
            flxNoInformation.add(lblNoInfoAvailable, flxAddingInfo);
            var flxContactInformation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120dp",
                "id": "flxContactInformation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "3%",
                "isModalContainer": false,
                "skin": "sknFlxBordere1e5eedCustomRadius5px",
                "top": "0dp",
                "width": "40%",
                "zIndex": 100
            }, {}, {});
            flxContactInformation.setDefaultUnit(kony.flex.DP);
            var flxName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxName.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "id": "lblName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl78818A100O",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNameValue = new kony.ui.Label({
                "id": "lblNameValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblFont485C75100O",
                "top": "32dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxName.add(lblName, lblNameValue);
            var flxPhone = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxPhone",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "200px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxPhone.setDefaultUnit(kony.flex.DP);
            var lblPhone = new kony.ui.Label({
                "id": "lblPhone",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl78818A100O",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblLocationsHeaderPhone\")",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPhoneValue = new kony.ui.Label({
                "id": "lblPhoneValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblFont485C75100O",
                "top": "32dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPhone.add(lblPhone, lblPhoneValue);
            var flxEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxEmail",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "74px",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxEmail.setDefaultUnit(kony.flex.DP);
            var lblEmail = new kony.ui.Label({
                "id": "lblEmail",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl78818A100O",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.EMAIL\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailValue = new kony.ui.Label({
                "id": "lblEmailValue",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknLblFont485C75100O",
                "top": "14dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmail.add(lblEmail, lblEmailValue);
            var flxMembership = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxMembership",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "200px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "74px",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxMembership.setDefaultUnit(kony.flex.DP);
            var lblMembershipStatic = new kony.ui.Label({
                "id": "lblMembershipStatic",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl78818A100O",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.MembershipKey\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMembershipValue = new kony.ui.Label({
                "id": "lblMembershipValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl006CCA12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.MembershipValue\")",
                "top": "14dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblHover006CCA12pxKA"
            });
            flxMembership.add(lblMembershipStatic, lblMembershipValue);
            flxContactInformation.add(flxName, flxPhone, flxEmail, flxMembership);
            var flxSeparatorHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120dp",
                "id": "flxSeparatorHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2.50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "0.10%",
                "zIndex": 500
            }, {}, {});
            flxSeparatorHeader.setDefaultUnit(kony.flex.DP);
            var lblSeparatorHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblSeparatorHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSeparatorHeader.add(lblSeparatorHeader);
            var flxSimulateOrApply = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "115dp",
                "id": "flxSimulateOrApply",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2.50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "450dp",
                "zIndex": 1
            }, {}, {});
            flxSimulateOrApply.setDefaultUnit(kony.flex.DP);
            var ListBoxLoanTypes = new kony.ui.ListBox({
                "focusSkin": "sknlstbxFocusBor2288f0KA",
                "height": "40dp",
                "id": "ListBoxLoanTypes",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Personal loan"],
                    ["lb2", "Home loan"],
                    ["lb3", "Mortgage"],
                    ["lb4", "car loan"],
                    ["lb5", "Select Loan type"]
                ],
                "onSelection": controller.AS_ListBox_d349e717955c4d72969ecf1a1c3bc550,
                "right": "15dp",
                "skin": "sknlstbx485c7513px",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbx000000OP40",
                "multiSelect": false
            });
            var lblViewInfo = new kony.ui.Label({
                "id": "lblViewInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl006CCAsize14pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.LoanInfo\")",
                "top": "65dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknHoverLbl006CCAsize14pxKA"
            });
            var lblLoanTypeError = new kony.ui.Label({
                "id": "lblLoanTypeError",
                "isVisible": false,
                "left": "0dp",
                "right": "20dp",
                "skin": "sknlblError",
                "text": "Select atleast one Loan type",
                "top": 45,
                "width": "240dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSimulate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSimulate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_h44f59d4f5964018a43ebc2d9a41961f,
                "right": "110dp",
                "top": "60dp",
                "width": "79dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknHoverFlxFFFFFF"
            });
            flxSimulate.setDefaultUnit(kony.flex.DP);
            var lblSimulate = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblSimulate",
                "isVisible": true,
                "skin": "sknLabel4F555D13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Simulate\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLabel4F555D13px"
            });
            flxSimulate.add(lblSimulate);
            var flxApply = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "focusSkin": "sknFlx6fb3cfBG100KA",
                "height": "30dp",
                "id": "flxApply",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_h61129942df04779be2c71ed0f092bc4,
                "right": "15dp",
                "skin": "sknFlx006CCA",
                "top": "60dp",
                "width": "79dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknHoverFlx004F93"
            });
            flxApply.setDefaultUnit(kony.flex.DP);
            var lblApply = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblApply",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLbl0OFontFFFFFF100O",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplyStaticMessage\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxApply.add(lblApply);
            flxSimulateOrApply.add(ListBoxLoanTypes, lblViewInfo, lblLoanTypeError, flxSimulate, flxApply);
            flxGuestDashboard.add(flxNoInformation, flxContactInformation, flxSeparatorHeader, flxSimulateOrApply);
            var flxLoansGuestSimulation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLoansGuestSimulation",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "3%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "93%",
                "zIndex": 2
            }, {}, {});
            flxLoansGuestSimulation.setDefaultUnit(kony.flex.DP);
            var flxLoansGuestSimulationContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "570dp",
                "id": "flxLoansGuestSimulationContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBordere4e6ecCustomRadius5px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLoansGuestSimulationContainer.setDefaultUnit(kony.flex.DP);
            var flxGuestSimulateResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "570dp",
                "id": "flxGuestSimulateResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknfbfcfc",
                "top": "0dp",
                "width": "340dp",
                "zIndex": 2
            }, {}, {
                "onHover": controller.AS_FlexContainer_g4404d619b314c208891429cb3427583
            });
            flxGuestSimulateResults.setDefaultUnit(kony.flex.DP);
            var lblGuestSimulationResults = new kony.ui.Label({
                "id": "lblGuestSimulationResults",
                "isVisible": true,
                "left": "38dp",
                "skin": "sknLbl274060100PerKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SimulationResults\")",
                "top": "33dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segSimulationResultsGuestSimulation = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAPR": "Label",
                    "lblAPRValue": "Label",
                    "lblApply": kony.i18n.getLocalizedString("i18n.frmLoansDashboard.ApplyStaticMessage"),
                    "lblLoanType": "Label",
                    "lblRate": "Label",
                    "lblRateValue": "Label",
                    "lblSeperator": "",
                    "lblTerms": "Label",
                    "lblTermsValue": "Label"
                }, {
                    "lblAPR": "Label",
                    "lblAPRValue": "Label",
                    "lblApply": kony.i18n.getLocalizedString("i18n.frmLoansDashboard.ApplyStaticMessage"),
                    "lblLoanType": "Label",
                    "lblRate": "Label",
                    "lblRateValue": "Label",
                    "lblSeperator": "",
                    "lblTerms": "Label",
                    "lblTermsValue": "Label"
                }, {
                    "lblAPR": "Label",
                    "lblAPRValue": "Label",
                    "lblApply": kony.i18n.getLocalizedString("i18n.frmLoansDashboard.ApplyStaticMessage"),
                    "lblLoanType": "Label",
                    "lblRate": "Label",
                    "lblRateValue": "Label",
                    "lblSeperator": "",
                    "lblTerms": "Label",
                    "lblTermsValue": "Label"
                }],
                "groupCells": false,
                "id": "segSimulationResultsGuestSimulation",
                "isVisible": false,
                "left": "20dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_da264392af274531b895a0515837e5c6,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "sknsegffffffOp100Bordere1e5ed",
                "rowSkin": "sknsegffffffOp100Bordere1e5ed",
                "rowTemplate": "flxGuestSimulationResults",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "70dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxGuestSimulationResults": "flxGuestSimulationResults",
                    "flxHeaderSimulationResults": "flxHeaderSimulationResults",
                    "flxRecommendedLoans": "flxRecommendedLoans",
                    "lblAPR": "lblAPR",
                    "lblAPRValue": "lblAPRValue",
                    "lblApply": "lblApply",
                    "lblLoanType": "lblLoanType",
                    "lblRate": "lblRate",
                    "lblRateValue": "lblRateValue",
                    "lblSeperator": "lblSeperator",
                    "lblTerms": "lblTerms",
                    "lblTermsValue": "lblTermsValue"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblVerticalSeperatorGuestSimulation = new kony.ui.Label({
                "height": "570dp",
                "id": "lblVerticalSeperatorGuestSimulation",
                "isVisible": true,
                "right": "339dp",
                "skin": "sknlblSeperatorBgD5D9DD",
                "text": "Label",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGuestSimulateResults.add(lblGuestSimulationResults, segSimulationResultsGuestSimulation, lblVerticalSeperatorGuestSimulation);
            var flxGuestSimulationBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxGuestSimulationBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_de8bb540898d425c83095fb257783f2d,
                "right": "375dp",
                "skin": "slFbox",
                "top": "30dp",
                "width": "135dp",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "hoverhandSkin"
            });
            flxGuestSimulationBack.setDefaultUnit(kony.flex.DP);
            var lblGuestSimulationBackIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGuestSimulationBackIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknBackIcon3ebaed",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBackToLoansGuestSimulation = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBackToLoansGuestSimulation",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLbl3EBAED108PerKA",
                "text": "Back to Dashboard",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGuestSimulationBack.add(lblGuestSimulationBackIcon, lblBackToLoansGuestSimulation);
            var lblLoanTypeGuestSimulation = new kony.ui.Label({
                "id": "lblLoanTypeGuestSimulation",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLatoReg485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.LoanType\")",
                "top": "80dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxLoanTypeGuestSimulation = new kony.ui.ListBox({
                "focusSkin": "sknlstbxFocusBor2288f0KA",
                "height": "35dp",
                "id": "lstBoxLoanTypeGuestSimulation",
                "isVisible": true,
                "left": "35dp",
                "masterData": [
                    ["Personal loan", "Personal loan"],
                    ["Home loan", "Home loan"],
                    ["Mortgage", "Mortgage"],
                    ["Car loan", "Car loan"]
                ],
                "onSelection": controller.AS_ListBox_ddd49aa9f7e741f497c50a84f892e442,
                "selectedKey": "Personal loan",
                "selectedKeyValue": ["Personal loan", "Personal loan"],
                "skin": "sknlstbx485c7513px",
                "top": "105dp",
                "width": "245dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbx000000OP40",
                "multiSelect": false
            });
            var lblSeperatorGuestSimulation = new kony.ui.Label({
                "height": "1dp",
                "id": "lblSeperatorGuestSimulation",
                "isVisible": true,
                "left": "35dp",
                "right": "375dp",
                "skin": "sknlblSeperatorBgD5D9DD",
                "text": "Label",
                "top": "175dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblGuestSimulationType = new kony.ui.Label({
                "id": "lblGuestSimulationType",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLatoReg485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.PersonalLoanSimulation\")",
                "top": "200dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLoanAmountGuestSimulation = new kony.ui.Label({
                "id": "lblLoanAmountGuestSimulation",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLatoReg485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.LoanAmount\")",
                "top": "235dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtLoanAmountGuestSimulation = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknTxtFFFFFFBor3ebaed100perKA",
                "height": "40dp",
                "id": "txtLoanAmountGuestSimulation",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
                "left": "35dp",
                "placeholder": "Amount range $1000-50000",
                "secureTextEntry": false,
                "skin": "sknTbxFFFFFFBorDEDEDE13pxKA",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "265dp",
                "width": "245dp",
                "zIndex": 2
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "onKeyUp": controller.AS_TextField_gf4d08da5a0749d591a0c3a28a9b8fc0,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var lblDollarGuestSimulation = new kony.ui.Label({
                "centerY": "285dp",
                "id": "lblDollarGuestSimulation",
                "isVisible": true,
                "left": "45dp",
                "skin": "sknLblLatoReg485c75110Per",
                "text": "$",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxEmploymentStatusGuestSimulation = new kony.ui.ListBox({
                "focusSkin": "sknlstbxFocusBor2288f0KA",
                "height": "40dp",
                "id": "lstBoxEmploymentStatusGuestSimulation",
                "isVisible": true,
                "left": "310dp",
                "masterData": [
                    ["Full Time", "Full Time"],
                    ["Part Time", "Part Time"],
                    ["Self Employed", "Self Employed"],
                    ["Unemployed", "Unemployed"],
                    ["Other", "Other"],
                    ["Select", "Select"]
                ],
                "onSelection": controller.AS_ListBox_b735d13044624f8fbb4bc312274e59c4,
                "selectedKey": "Select",
                "selectedKeyValue": ["Select", "Select"],
                "skin": "sknlstbx485c7513px",
                "top": "265dp",
                "width": "245dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbx000000OP40",
                "multiSelect": false
            });
            var lblEmploymentStatusGuestSimulation = new kony.ui.Label({
                "id": "lblEmploymentStatusGuestSimulation",
                "isVisible": true,
                "left": "310dp",
                "skin": "sknlblLatoReg485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.EmploymentStatus\")",
                "top": "235dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCreditScoreGuestSimulation = new kony.ui.Label({
                "id": "lblCreditScoreGuestSimulation",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLatoReg485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CreditScore\")",
                "top": "340dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBottomSeperatorGuestSimulation = new kony.ui.Label({
                "height": "1dp",
                "id": "lblBottomSeperatorGuestSimulation",
                "isVisible": true,
                "left": "35dp",
                "right": "375dp",
                "skin": "sknlblSeperatorBgD5D9DD",
                "top": "470dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSimulateGuestSimulation = new kony.ui.Button({
                "focusSkin": "sknBtn6fb3cfLatoBoldffffff12px",
                "height": "40dp",
                "id": "btnSimulateGuestSimulation",
                "isVisible": true,
                "onClick": controller.AS_Button_g8a5fbbeb3b44b32a3a47ccf0575b7be,
                "right": 375,
                "skin": "sknBtn11abebLatoBoldffffff12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Simulate\")",
                "top": "495dp",
                "width": "100dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn6fb3cfLatoBoldffffff12px"
            });
            var flxCreditRatingMeterGuestSimulation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10dp",
                "id": "flxCreditRatingMeterGuestSimulation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "410dp",
                "width": "450dp",
                "zIndex": 2
            }, {}, {});
            flxCreditRatingMeterGuestSimulation.setDefaultUnit(kony.flex.DP);
            flxCreditRatingMeterGuestSimulation.add();
            var flxRangesGuestSimulation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRangesGuestSimulation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "380dp",
                "width": "450dp",
                "zIndex": 2
            }, {}, {});
            flxRangesGuestSimulation.setDefaultUnit(kony.flex.DP);
            flxRangesGuestSimulation.add();
            var flxRatingNamesGuestSimulation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRatingNamesGuestSimulation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "430dp",
                "width": "450dp",
                "zIndex": 2
            }, {}, {});
            flxRatingNamesGuestSimulation.setDefaultUnit(kony.flex.DP);
            flxRatingNamesGuestSimulation.add();
            var flxCreditScorePointerGuestSimulation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "242dp",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCreditScorePointerGuestSimulation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "400dp",
                "width": "20dp",
                "zIndex": 2
            }, {}, {});
            flxCreditScorePointerGuestSimulation.setDefaultUnit(kony.flex.DP);
            var lblUpperCreditScorePointerGuestSimulation = new kony.ui.Label({
                "centerX": "50%",
                "height": "30dp",
                "id": "lblUpperCreditScorePointerGuestSimulation",
                "isVisible": true,
                "skin": "sknIcommonPointerUpper",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCreditScorePointerGuestSimulation.add(lblUpperCreditScorePointerGuestSimulation);
            var lblGuestSimulationLoanAmountError = new kony.ui.Label({
                "id": "lblGuestSimulationLoanAmountError",
                "isVisible": false,
                "left": "35dp",
                "right": "20dp",
                "skin": "sknlblError",
                "text": "Loan amount cannot be empty",
                "top": 310,
                "width": "240dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblGuestSimulationEmploymentStatusError = new kony.ui.Label({
                "id": "lblGuestSimulationEmploymentStatusError",
                "isVisible": false,
                "left": "310dp",
                "right": "20dp",
                "skin": "sknlblError",
                "text": "Employment status cannot be empty",
                "top": 310,
                "width": "240dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblGuestSimulationDetails = new kony.ui.Label({
                "id": "lblGuestSimulationDetails",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknLbl274060100PerKA",
                "text": "SIMULATION DETAILS",
                "top": "33dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblGuestSimulationLoanTypeError = new kony.ui.Label({
                "id": "lblGuestSimulationLoanTypeError",
                "isVisible": false,
                "left": "35dp",
                "right": "20dp",
                "skin": "sknlblError",
                "text": "Select atleast one Loan type",
                "top": 150,
                "width": "240dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLoansGuestSimulationContainer.add(flxGuestSimulateResults, flxGuestSimulationBack, lblLoanTypeGuestSimulation, lstBoxLoanTypeGuestSimulation, lblSeperatorGuestSimulation, lblGuestSimulationType, lblLoanAmountGuestSimulation, txtLoanAmountGuestSimulation, lblDollarGuestSimulation, lstBoxEmploymentStatusGuestSimulation, lblEmploymentStatusGuestSimulation, lblCreditScoreGuestSimulation, lblBottomSeperatorGuestSimulation, btnSimulateGuestSimulation, flxCreditRatingMeterGuestSimulation, flxRangesGuestSimulation, flxRatingNamesGuestSimulation, flxCreditScorePointerGuestSimulation, lblGuestSimulationLoanAmountError, lblGuestSimulationEmploymentStatusError, lblGuestSimulationDetails, lblGuestSimulationLoanTypeError);
            var flxGuestSimulationContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "620dp",
                "id": "flxGuestSimulationContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxGuestSimulationContainer.setDefaultUnit(kony.flex.DP);
            var simulationTemplate = new com.adminConsole.loans.applications.simulationTemplate.simulationTemplate({
                "clipBounds": true,
                "height": "100%",
                "id": "simulationTemplate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxFFFFFFKA",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDollar": {
                        "zIndex": 150
                    },
                    "lblCreditScoreError": {
                        "isVisible": false
                    },
                    "lblDollar": {
                        "text": "$",
                        "zIndex": 150
                    },
                    "lblEmploymentStatusError": {
                        "isVisible": false
                    },
                    "lblSimulateToGetRes": {
                        "zIndex": 1
                    },
                    "segSimulationResults": {
                        "zIndex": 1
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            simulationTemplate.btnSimulateSimulation.onClick = controller.AS_Button_a29f008d666c490e96fa90b74beaa8f8;
            simulationTemplate.flxSimulationBack.onClick = controller.AS_FlexContainer_acb477fe399247b7a1d371d1105b1bc2;
            simulationTemplate.segSimulationResults.onRowClick = controller.AS_Segment_b1482cdbba954a6ca2f872076a5b8fb4;
            flxGuestSimulationContainer.add(simulationTemplate);
            flxLoansGuestSimulation.add(flxLoansGuestSimulationContainer, flxGuestSimulationContainer);
            var flxSpaceGuestSimulationDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSpaceGuestSimulationDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "230dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSpaceGuestSimulationDetails.setDefaultUnit(kony.flex.DP);
            flxSpaceGuestSimulationDetails.add();
            var flxRegistrationLinkOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxRegistrationLinkOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "750dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17.56%",
                "zIndex": 1
            }, {}, {});
            flxRegistrationLinkOptions.setDefaultUnit(kony.flex.DP);
            var lblSendRegLinkTo = new kony.ui.Label({
                "id": "lblSendRegLinkTo",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLbl78818A100O",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.SendRegistrationLinkTo\")",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContainingRegistrationCheckboxes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxContainingRegistrationCheckboxes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "2dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxContainingRegistrationCheckboxes.setDefaultUnit(kony.flex.DP);
            flxContainingRegistrationCheckboxes.add();
            flxRegistrationLinkOptions.add(lblSendRegLinkTo, flxContainingRegistrationCheckboxes);
            var flxRegistrationWrapperKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRegistrationWrapperKA",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "5dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "17%",
                "zIndex": 51
            }, {}, {});
            flxRegistrationWrapperKA.setDefaultUnit(kony.flex.DP);
            var flxRegistrationLinkKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRegistrationLinkKA",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 52
            }, {}, {});
            flxRegistrationLinkKA.setDefaultUnit(kony.flex.DP);
            var lblRegistrationLinkKA = new kony.ui.Label({
                "id": "lblRegistrationLinkKA",
                "isVisible": true,
                "left": "0dp",
                "onTouchEnd": controller.AS_Label_b5befc3b01724c89a3ada56766fe60f6,
                "skin": "sknLbl006CCA13pxKA",
                "text": "Send Registration Email",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblHover006CCA13pxKA"
            });
            flxRegistrationLinkKA.add(lblRegistrationLinkKA);
            var flxRegistrationLinkOptionsKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRegistrationLinkOptionsKA",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBordere1e5eedCustomRadius5px",
                "top": "45dp",
                "width": "95%",
                "zIndex": 11
            }, {}, {});
            flxRegistrationLinkOptionsKA.setDefaultUnit(kony.flex.DP);
            var lblSendLinkKA = new kony.ui.Label({
                "id": "lblSendLinkKA",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato5d6c7f12px",
                "text": "SEND LINK VIA",
                "top": "7dp",
                "width": "80%",
                "zIndex": 11
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailoptionKA = new kony.ui.Label({
                "id": "lblEmailoptionKA",
                "isVisible": true,
                "left": "5dp",
                "onTouchEnd": controller.AS_Label_f6731768bc634e689788af8e78456393,
                "skin": "sknLblFont485C75100O",
                "text": "Email",
                "top": "10dp",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl11abeb100O"
            });
            var lblSMSOptionKA = new kony.ui.Label({
                "id": "lblSMSOptionKA",
                "isVisible": false,
                "left": "5dp",
                "skin": "sknLblFont485C75100O",
                "text": "SMS",
                "top": "5dp",
                "width": "80%",
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl11abeb100O"
            });
            var lblEmailAndSMSOptionKA = new kony.ui.Label({
                "bottom": "5dp",
                "id": "lblEmailAndSMSOptionKA",
                "isVisible": false,
                "left": "5dp",
                "skin": "sknLblFont485C75100O",
                "text": "Both email and SMS",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl11abeb100O"
            });
            flxRegistrationLinkOptionsKA.add(lblSendLinkKA, lblEmailoptionKA, lblSMSOptionKA, lblEmailAndSMSOptionKA);
            flxRegistrationWrapperKA.add(flxRegistrationLinkKA, flxRegistrationLinkOptionsKA);
            flxHeaderAndMainContainer.add(HeaderGuest, flxGeneralInfo, flxSelectLoanType, flxGuestDashboard, flxLoansGuestSimulation, flxSpaceGuestSimulationDetails, flxRegistrationLinkOptions, flxRegistrationWrapperKA);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "centerX": "50%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            flxMain.add(flxMainHeader, flxHeaderDropdown, flxBreadCrumbs, flxAlertMessage, flxHeaderAndMainContainer, flxToastMessage);
            var flxDisabledBackground = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxDisabledBackground",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopysknFlx0fa33a6e8590344",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxDisabledBackground.setDefaultUnit(kony.flex.DP);
            var flxAddInformationPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "62.10%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "420dp",
                "id": "flxAddInformationPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "340dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFFKA",
                "top": "150dp",
                "width": "830dp",
                "zIndex": 10
            }, {}, {});
            flxAddInformationPopup.setDefaultUnit(kony.flex.DP);
            var flxGenInfoContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "63dp",
                "id": "flxGenInfoContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "500dp",
                "zIndex": 5
            }, {}, {});
            flxGenInfoContainer.setDefaultUnit(kony.flex.DP);
            var lblGeneralInfo = new kony.ui.Label({
                "centerY": 50,
                "id": "lblGeneralInfo",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblLatoBold16px485c75KA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.GenInfo\")",
                "top": "40dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGenInfoContainer.add(lblGeneralInfo);
            var flxCloseIconAddInfoPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxCloseIconAddInfoPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {}, {});
            flxCloseIconAddInfoPopup.setDefaultUnit(kony.flex.DP);
            var lblCloseAddInfoPopupIcon = new kony.ui.Label({
                "id": "lblCloseAddInfoPopupIcon",
                "isVisible": true,
                "onTouchStart": controller.AS_Label_ea41663423704d7abbeeda75b18fb15a,
                "right": "20dp",
                "skin": "sknFontIconCross14px",
                "text": "",
                "top": "22dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseIconAddInfoPopup.add(lblCloseAddInfoPopupIcon);
            var flxAddInfoFirstRowContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "105dp",
                "id": "flxAddInfoFirstRowContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "750dp",
                "zIndex": 5
            }, {}, {});
            flxAddInfoFirstRowContainer.setDefaultUnit(kony.flex.DP);
            var flxFirstName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "105dp",
                "id": "flxFirstName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "238dp",
                "zIndex": 1
            }, {}, {});
            flxFirstName.setDefaultUnit(kony.flex.DP);
            var lblFirstName = new kony.ui.Label({
                "id": "lblFirstName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstName\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "placeholder": "Customer Name",
                "right": "5dp",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxFirstNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFirstNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFirstNameError.setDefaultUnit(kony.flex.DP);
            var lblFirstNameErrorIcon = new kony.ui.Label({
                "id": "lblFirstNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblErrorIcon14px",
                "text": "",
                "top": "3dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFirstNameError = new kony.ui.Label({
                "id": "lblFirstNameError",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.frmGuestDashboard.FirstNameMissing\")",
                "top": "2dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFirstNameError.add(lblFirstNameErrorIcon, lblFirstNameError);
            flxFirstName.add(lblFirstName, txtName, flxFirstNameError);
            var flxMiddleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "105dp",
                "id": "flxMiddleName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "253dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "238dp",
                "zIndex": 1
            }, {}, {});
            flxMiddleName.setDefaultUnit(kony.flex.DP);
            var lblMiddleNameStatic = new kony.ui.Label({
                "id": "lblMiddleNameStatic",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.MiddleName\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtMiddleName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtMiddleName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")",
                "right": "5dp",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxMiddleName.add(lblMiddleNameStatic, txtMiddleName);
            var flxLastName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "105dp",
                "id": "flxLastName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "506dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "238dp",
                "zIndex": 1
            }, {}, {});
            flxLastName.setDefaultUnit(kony.flex.DP);
            var lbllastName = new kony.ui.Label({
                "id": "lbllastName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtLastName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtLastName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")",
                "right": "5dp",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "15px",
                "width": "98%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxLastNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLastNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLastNameError.setDefaultUnit(kony.flex.DP);
            var lblLastNameErrorIcon = new kony.ui.Label({
                "id": "lblLastNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblErrorIcon14px",
                "text": "",
                "top": "3dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLastNameError = new kony.ui.Label({
                "id": "lblLastNameError",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.frmGuestDashboard.LastNameMissing\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLastNameError.add(lblLastNameErrorIcon, lblLastNameError);
            flxLastName.add(lbllastName, txtLastName, flxLastNameError);
            flxAddInfoFirstRowContainer.add(flxFirstName, flxMiddleName, flxLastName);
            var flxAddInfoSecondRowContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "105dp",
                "id": "flxAddInfoSecondRowContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "205dp",
                "width": "750dp",
                "zIndex": 5
            }, {}, {});
            flxAddInfoSecondRowContainer.setDefaultUnit(kony.flex.DP);
            var flxPrimaryPhoneNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "105dp",
                "id": "flxPrimaryPhoneNumber",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "238dp",
                "zIndex": 1
            }, {}, {});
            flxPrimaryPhoneNumber.setDefaultUnit(kony.flex.DP);
            var lblPrimaryPhoneNumber = new kony.ui.Label({
                "id": "lblPrimaryPhoneNumber",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.PrimaryPhoneNumber\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtPrimaryPhoneNumber = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtPrimaryPhoneNumber",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Phone_Number\")",
                "right": "5dp",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxPhoneNumberError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPhoneNumberError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPhoneNumberError.setDefaultUnit(kony.flex.DP);
            var lblPhoneNumberErrorIcon = new kony.ui.Label({
                "id": "lblPhoneNumberErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblErrorIcon14px",
                "text": "",
                "top": "3dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPhoneNumberError = new kony.ui.Label({
                "id": "lblPhoneNumberError",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.frmGuestDashboard.PhoneNumberMissing\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPhoneNumberError.add(lblPhoneNumberErrorIcon, lblPhoneNumberError);
            flxPrimaryPhoneNumber.add(lblPrimaryPhoneNumber, txtPrimaryPhoneNumber, flxPhoneNumberError);
            var flxPrimaryEmailAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "105dp",
                "id": "flxPrimaryEmailAddress",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "258dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "238dp",
                "zIndex": 1
            }, {}, {});
            flxPrimaryEmailAddress.setDefaultUnit(kony.flex.DP);
            var lblPrimaryEmailAddress = new kony.ui.Label({
                "id": "lblPrimaryEmailAddress",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.PrimaryEmailId\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtEmailAddress = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtEmailAddress",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")",
                "right": "5dp",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxEmailError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailError.setDefaultUnit(kony.flex.DP);
            var lblEmailErrorIcon = new kony.ui.Label({
                "id": "lblEmailErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblErrorIcon14px",
                "text": "",
                "top": "3dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailError = new kony.ui.Label({
                "id": "lblEmailError",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.frmGuestDashboard.EmailMissing\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailError.add(lblEmailErrorIcon, lblEmailError);
            flxPrimaryEmailAddress.add(lblPrimaryEmailAddress, txtEmailAddress, flxEmailError);
            flxAddInfoSecondRowContainer.add(flxPrimaryPhoneNumber, flxPrimaryEmailAddress);
            var flxCancelAndSave = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxCancelAndSave",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "sknflxeceff3KA",
                "top": "340dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCancelAndSave.setDefaultUnit(kony.flex.DP);
            var btnCancel = new kony.ui.Button({
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnCancel",
                "isVisible": true,
                "onClick": controller.AS_Button_f7910f38910d4a7c8b01cf1cb79c5818,
                "right": "135dp",
                "skin": "sknbtnffffffLatoBold4F555D13pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "20dp",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBold4F555D13pxKA"
            });
            var btnSave = new kony.ui.Button({
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40dp",
                "id": "btnSave",
                "isVisible": true,
                "onClick": controller.AS_Button_dc9275c66a444d6189cbe4a809b09fb3,
                "right": "20dp",
                "skin": "sknBtnFFFFFFLatoBold006CCA13pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "top": "20dp",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnFFFFFFLatoBold004F9313pxKA"
            });
            flxCancelAndSave.add(btnCancel, btnSave);
            var flxBanner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10dp",
                "id": "flxBanner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBanner006CCA",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBanner.setDefaultUnit(kony.flex.DP);
            flxBanner.add();
            flxAddInformationPopup.add(flxGenInfoContainer, flxCloseIconAddInfoPopup, flxAddInfoFirstRowContainer, flxAddInfoSecondRowContainer, flxCancelAndSave, flxBanner);
            flxDisabledBackground.add(flxAddInformationPopup);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoadingBlur",
                "top": "0px",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxEligibilityCheck = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "62.10%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "440dp",
                "id": "flxEligibilityCheck",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxFFFFFFKA",
                "width": "835dp",
                "zIndex": 5
            }, {}, {});
            flxEligibilityCheck.setDefaultUnit(kony.flex.DP);
            var flxHeaderMEC = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "87dp",
                "id": "flxHeaderMEC",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMEC.setDefaultUnit(kony.flex.DP);
            var flxBannerMEC = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10dp",
                "id": "flxBannerMEC",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBanner006CCA",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBannerMEC.setDefaultUnit(kony.flex.DP);
            flxBannerMEC.add();
            var lblHeadingMEC = new kony.ui.Label({
                "height": "16dp",
                "id": "lblHeadingMEC",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblLatoBold16px485c75KA",
                "text": "MEMBERSHIP ELIGIBILITY CHECK",
                "top": "40dp",
                "width": "50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCancelMEC = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgCancelMEC",
                "isVisible": true,
                "onTouchEnd": controller.AS_Image_c5cf132c9c494e09ac65e7b529afb165,
                "right": "20dp",
                "skin": "slImage",
                "src": "close_blue.png",
                "top": "22dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderMEC.add(flxBannerMEC, lblHeadingMEC, imgCancelMEC);
            var flxQuestionsMEC = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "225dp",
                "id": "flxQuestionsMEC",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "88dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxQuestionsMEC.setDefaultUnit(kony.flex.DP);
            var lblTitleTextMEC = new kony.ui.Label({
                "height": "24dp",
                "id": "lblTitleTextMEC",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoRegular485C7513pxKA",
                "text": "Customer should meet at least one of the criteria  listed below.",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblQuestion1MEC = new kony.ui.Label({
                "height": "24dp",
                "id": "lblQuestion1MEC",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoRegular485C7513pxKA",
                "text": "• An employee of a the company or one of its subsidiaries.",
                "top": "40dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblQuestionTwoMEC = new kony.ui.Label({
                "height": "24dp",
                "id": "lblQuestionTwoMEC",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoRegular485C7513pxKA",
                "text": "• A retiree of the company or one of its subsidiaries.",
                "top": "65dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblQuestionThreeMEC = new kony.ui.Label({
                "height": "24dp",
                "id": "lblQuestionThreeMEC",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoRegular485C7513pxKA",
                "text": "• An immediate family member or roommate of one of the above.",
                "top": "90dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblQuestion4MEC = new kony.ui.Label({
                "height": "24dp",
                "id": "lblQuestion4MEC",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoRegular485C7513pxKA",
                "text": "Are you eligible as a member?",
                "top": "130dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rbgResponse = new kony.ui.RadioButtonGroup({
                "height": "24dp",
                "id": "rbgResponse",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["YES", "Yes   "],
                    ["NO", "No"]
                ],
                "onSelection": controller.AS_RadioButtonGroup_h9845aa69c4841bf940e438d693d7f19,
                "selectedKey": "NO",
                "selectedKeyValue": ["NO", "No"],
                "skin": "sknRbgCoApplicantKA",
                "top": "160dp",
                "width": "130dp",
                "zIndex": 1
            }, {
                "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxQuestionsMEC.add(lblTitleTextMEC, lblQuestion1MEC, lblQuestionTwoMEC, lblQuestionThreeMEC, lblQuestion4MEC, rbgResponse);
            var flxFooter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxFooter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxeceff3KA",
                "top": "360dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFooter.setDefaultUnit(kony.flex.DP);
            var btSubmitMEC = new kony.ui.Button({
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40dp",
                "id": "btSubmitMEC",
                "isVisible": true,
                "onClick": controller.AS_Button_cf9965396b6542e4b40d1d81a7956b4b,
                "right": "20dp",
                "skin": "sknBtnFFFFFFLatoBold006CCA13pxKA",
                "text": "SUBMIT",
                "top": "20dp",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnFFFFFFLatoBold004F9313pxKA"
            });
            var btCancelMEC = new kony.ui.Button({
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btCancelMEC",
                "isVisible": true,
                "onClick": controller.AS_Button_cca8c9081dc1443e8639c55587bed34b,
                "right": "135dp",
                "skin": "sknbtnffffffLatoBold4F555D13pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "20dp",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBold4F555D13pxKA"
            });
            flxFooter.add(btSubmitMEC, btCancelMEC);
            var FlxResponseMsg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "17dp",
                "id": "FlxResponseMsg",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknFlx000000KA",
                "top": "313dp",
                "width": "670dp",
                "zIndex": 1
            }, {}, {});
            FlxResponseMsg.setDefaultUnit(kony.flex.DP);
            var lblCorrectResponse = new kony.ui.Label({
                "height": "15dp",
                "id": "lblCorrectResponse",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblIcomoon52cc76KA",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblResponseMsgMEC = new kony.ui.Label({
                "height": "16dp",
                "id": "lblResponseMsgMEC",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoRegular52cc76Size12pxKA",
                "text": "Customer is eligible for membership.  Go ahead and submit",
                "top": "0dp",
                "width": "356dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlxResponseMsg.add(lblCorrectResponse, lblResponseMsgMEC);
            var flxErrorMsgMEC = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "17dp",
                "id": "flxErrorMsgMEC",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "313dp",
                "width": "670dp",
                "zIndex": 1
            }, {}, {});
            flxErrorMsgMEC.setDefaultUnit(kony.flex.DP);
            var lblErrorMsgIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorMsgIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgMEC = new kony.ui.Label({
                "height": "16dp",
                "id": "lblErrorMsgMEC",
                "isVisible": true,
                "skin": "sknLblLatoRegularff3000size12pxKA",
                "text": "Customer is not eligible for membership..",
                "top": "0dp",
                "width": "356dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorMsgMEC.add(lblErrorMsgIcon, lblErrorMsgMEC);
            flxEligibilityCheck.add(flxHeaderMEC, flxQuestionsMEC, flxFooter, FlxResponseMsg, flxErrorMsgMEC);
            this.add(flxLeftPannel, flxMain, flxDisabledBackground, flxLoading, flxEligibilityCheck);
        };
        return [{
            "addWidgets": addWidgetsfrmGuestDashboard,
            "enabledForIdleTimeout": true,
            "id": "frmGuestDashboard",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_ie05e80f37a8465c916e3d7e4ccad97d(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});