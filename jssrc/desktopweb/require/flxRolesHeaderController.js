define("userflxRolesHeaderController", {
    //Type your controller code here 
});
define("flxRolesHeaderControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxRoleHeaderName **/
    AS_FlexContainer_b38a27ee386d4c37b766c0efa1f60e79: function AS_FlexContainer_b38a27ee386d4c37b766c0efa1f60e79(eventobject, context) {
        var self = this;
        //sffs
    },
    /** onClick defined for flxRoleHeaderDescription **/
    AS_FlexContainer_cf3f3c408ddc4ae4bf29ab1c34d05ab2: function AS_FlexContainer_cf3f3c408ddc4ae4bf29ab1c34d05ab2(eventobject, context) {
        var self = this;
        //sg
    },
    /** onClick defined for flxRoleHeaderValidTill **/
    AS_FlexContainer_g4688ec253184083986ccc70f4693a11: function AS_FlexContainer_g4688ec253184083986ccc70f4693a11(eventobject, context) {
        var self = this;
        //text
    },
    /** onClick defined for flxRoleHeaderUsers **/
    AS_FlexContainer_d8f18db98c474121bcbaf5d4837bed47: function AS_FlexContainer_d8f18db98c474121bcbaf5d4837bed47(eventobject, context) {
        var self = this;
        //text
    },
    /** onClick defined for flxRoleHeaderPermissions **/
    AS_FlexContainer_jb0ea3031a2549f584c7552a6d5e6158: function AS_FlexContainer_jb0ea3031a2549f584c7552a6d5e6158(eventobject, context) {
        var self = this;
        //text
    },
    /** onClick defined for flxRoleHeaderStatus **/
    AS_FlexContainer_ecdcf65e7b694394b7e864fedc016f61: function AS_FlexContainer_ecdcf65e7b694394b7e864fedc016f61(eventobject, context) {
        var self = this;
        //text
    }
});
define("flxRolesHeaderController", ["userflxRolesHeaderController", "flxRolesHeaderControllerActions"], function() {
    var controller = require("userflxRolesHeaderController");
    var controllerActions = ["flxRolesHeaderControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
