define(['ModelManager'], function(ModelManager) {
    /**
     * User defined business controller
     * @constructor
     * @extends kony.mvc.Business.Controller
     */
    function BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(BusinessController, kony.mvc.Business.Controller);
    /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller gets initialized
     * @method
     */
    BusinessController.prototype.initializeBusinessController = function() {};
    /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller is told to execute a command
     * @method
     * @param {Object} kony.mvc.Business.Command Object
     */
    BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    BusinessController.prototype.createCustomer = function(createRequest, onSuccess, onError) {
        ModelManager.invoke('customer', 'createCustomer', createRequest, onSuccess, onError);
    };
    BusinessController.prototype.verifyUsername = function(context, onSuccess, onError) {
        ModelManager.invoke('customer', 'verifyUsername', context, onSuccess, onError);
    };
    BusinessController.prototype.getCompanyAccounts = function(context, onSuccess, onError) {
        ModelManager.invoke('company', 'getCompanyAccounts', context, onSuccess, onError);
    };
    BusinessController.prototype.getCompanyDetails = function(context, onSuccess, onError) {
        ModelManager.invoke('company', 'getCompanyDetails', context, onSuccess, onError);
    };
    BusinessController.prototype.editCompany = function(context, onSuccess, onError) {
        ModelManager.invoke('company', 'editCompany', context, onSuccess, onError);
    };
    BusinessController.prototype.validateTIN = function(context, onSuccess, onError) {
        ModelManager.invoke('company', 'validateTIN', {}, onSuccess, onError);
    };
    BusinessController.prototype.createCompany = function(context, onSuccess, onError) {
        ModelManager.invoke('company', 'createCompany', context, onSuccess, onError);
    };
    BusinessController.prototype.validateTIN = function(context, onSuccess, onError) {
        ModelManager.invoke('company', 'validateTIN', context, onSuccess, onError);
    };
    BusinessController.prototype.getCompanyCustomers = function(context, onSuccess, onError) {
        ModelManager.invoke('company', 'getCompanyCustomers', context, onSuccess, onError);
    };
    BusinessController.prototype.getAllAccounts = function(context, onSuccess, onError) {
        ModelManager.invoke('company', 'getAllAccounts', context, onSuccess, onError);
    };
    BusinessController.prototype.verifyOFACandCIP = function(context, onSuccess, onError) {
        ModelManager.invoke('company', 'verifyOFACandCIP', context, onSuccess, onError);
    };
    BusinessController.prototype.getBBCustomerServiceLimit = function(context, onSuccess, onError) {
        ModelManager.invoke('bbcustomerservicelimit', 'getBBCustomerServiceLimit', context, onSuccess, onError);
    };
    BusinessController.prototype.unlinkAccounts = function(context, onSuccess, onError) {
        ModelManager.invoke('company', 'unlinkAccounts', context, onSuccess, onError);
    };
    BusinessController.prototype.editCustomer = function(context, onSuccess, onError) {
        ModelManager.invoke('customer', 'editCustomer', context, onSuccess, onError);
    };
    BusinessController.prototype.getCustomerAccounts = function(context, onSuccess, onError) {
        ModelManager.invoke('customer', 'getCustomerAccounts', context, onSuccess, onError);
    };
    BusinessController.prototype.upgradeUser = function(context, onSuccess, onError) {
        ModelManager.invoke('customer', 'upgradeUser', context, onSuccess, onError);
    };
    return BusinessController;
});