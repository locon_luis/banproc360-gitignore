define("CustomerGroupsModule/frmGroups", function() {
    return function(controller) {
        function addWidgetsfrmGroups() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "konydbxlogobignverted.png"
                    },
                    "imgBottomLogo": {
                        "src": "konydbxinverted.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.ADD_NEW_GROUP\")",
                        "right": "170dp"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": true
                    },
                    "imgLogout": {
                        "right": "0px",
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Groups.Heading\")"
                    },
                    "lblUserName": {
                        "right": "25px",
                        "text": "Preetish",
                        "top": "viz.val_cleared"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxBreadCrumbs",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "105px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "20dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "centerY": "50%",
                        "top": "20dp"
                    },
                    "btnBackToMain": {
                        "text": "CUSTOMER ROLES"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-70px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 99
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lbltoastMessage": {
                        "text": "Branch deactivated successfully"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxNoGroups = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoGroups",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "106dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoGroups.setDefaultUnit(kony.flex.DP);
            var noStaticData = new com.adminConsole.staticContent.noStaticData({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "id": "noStaticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "btnAddStaticContent": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                        "width": "70dp"
                    },
                    "lblNoStaticContentCreated": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Groups.noGroupAdded\")"
                    },
                    "lblNoStaticContentMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Groups.AddGroup\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoGroups.add(noStaticData);
            var flxAddGroups = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "15px",
                "clipBounds": true,
                "id": "flxAddGroups",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "145dp",
                "zIndex": 1
            }, {}, {});
            flxAddGroups.setDefaultUnit(kony.flex.DP);
            var flxVerticalTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxVerticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "225px",
                "zIndex": 1
            }, {}, {});
            flxVerticalTabs.setDefaultUnit(kony.flex.DP);
            var verticalTabs = new com.adminConsole.common.verticalTabs1({
                "clipBounds": true,
                "height": "100%",
                "id": "verticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.GroupDetails\")",
                        "left": "30px",
                        "right": "40dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.ASSIGNENTITLEMENTS\")",
                        "left": "30px",
                        "right": "40dp",
                        "top": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.ASSIGNCUSTOMERS\")",
                        "left": "30px",
                        "right": "40dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption4": {
                        "left": "30px",
                        "right": 40,
                        "width": "viz.val_cleared"
                    },
                    "flxImgArrow1": {
                        "centerY": "50%",
                        "right": "25px"
                    },
                    "flxImgArrow2": {
                        "right": "25px",
                        "top": "22px"
                    },
                    "flxImgArrow3": {
                        "right": "25px"
                    },
                    "flxImgArrow4": {
                        "right": "25px"
                    },
                    "imgSelected1": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected2": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected3": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected4": {
                        "src": "right_arrow2x.png"
                    },
                    "lblOptional2": {
                        "left": "30px",
                        "top": "50px"
                    },
                    "lblOptional3": {
                        "left": "30px"
                    },
                    "lblOptional4": {
                        "left": "30px"
                    },
                    "lblSelected2": {
                        "centerY": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxVerticalTabs.add(verticalTabs);
            var flxGroupDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxGroupDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxGroupDetails.setDefaultUnit(kony.flex.DP);
            var flxAddGroupDetails = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "90dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAddGroupDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": 0,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "30dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxAddGroupDetails.setDefaultUnit(kony.flex.DP);
            var flxName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "41.74%",
                "zIndex": 1
            }, {}, {});
            flxName.setDefaultUnit(kony.flex.DP);
            var lblGroupNameKey = new kony.ui.Label({
                "id": "lblGroupNameKey",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.NameoftheGroup\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblGroupNameCount = new kony.ui.Label({
                "id": "lblGroupNameCount",
                "isVisible": false,
                "right": "2px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupNameCount\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxGroupNameValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "tbxGroupNameValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "placeholder": "Enter Customer Role Name",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoGroupNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxNoGroupNameError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%"
            }, {}, {});
            flxNoGroupNameError.setDefaultUnit(kony.flex.DP);
            var lblNoGroupNameError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblNoGroupNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblNoGroupNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoGroupNameErrorIcon = new kony.ui.Label({
                "centerY": "52%",
                "height": "15dp",
                "id": "lblNoGroupNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoGroupNameError.add(lblNoGroupNameError, lblNoGroupNameErrorIcon);
            var imgMandatoryName = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryName",
                "isVisible": false,
                "left": "110px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxName.add(lblGroupNameKey, lblGroupNameCount, tbxGroupNameValue, flxNoGroupNameError, imgMandatoryName);
            var flxValidity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75px",
                "id": "flxValidity",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxValidity.setDefaultUnit(kony.flex.DP);
            var lblValidity = new kony.ui.Label({
                "id": "lblValidity",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblValidity\")",
                "top": 0,
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblValidity0d9c3f33b4acc44 = new kony.ui.Label({
                "id": "CopylblValidity0d9c3f33b4acc44",
                "isVisible": true,
                "left": "53%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.CopylblValidity0d9c3f33b4acc44\")",
                "top": 0,
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var calValidStartDate = new kony.ui.Calendar({
                "calendarIcon": "calicon.png",
                "dateFormat": "dd/MM/yyyy",
                "height": "40dp",
                "id": "calValidStartDate",
                "isVisible": true,
                "left": "0dp",
                "placeholder": "  DD/MM/YYYY",
                "skin": "sknCal35475f14Px",
                "top": "30dp",
                "viewConfig": {
                    "gridConfig": {
                        "allowWeekendSelectable": true
                    }
                },
                "viewType": constants.CALENDAR_VIEW_TYPE_GRID_POPUP,
                "width": "47%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "noOfMonths": 1
            });
            var calValidEndDate = new kony.ui.Calendar({
                "calendarIcon": "calicon.png",
                "dateFormat": "dd/MM/yyyy",
                "height": "40dp",
                "id": "calValidEndDate",
                "isVisible": true,
                "placeholder": "  DD/MM/YYYY",
                "right": 2,
                "skin": "sknCal35475f14Px",
                "top": "30dp",
                "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
                "width": "47%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "noOfMonths": 1
            });
            var Label0b4617ed1f1af41 = new kony.ui.Label({
                "id": "Label0b4617ed1f1af41",
                "isVisible": true,
                "left": "406dp",
                "skin": "slLabel0a3041ed9d2454e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "51dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0a005cb85bbe644 = new kony.ui.Label({
                "id": "Label0a005cb85bbe644",
                "isVisible": true,
                "left": "67dp",
                "skin": "slLabel0d9a2a20de91c43",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "-13dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxValidity.add(lblValidity, CopylblValidity0d9c3f33b4acc44, calValidStartDate, calValidEndDate, Label0b4617ed1f1af41, Label0a005cb85bbe644);
            var flxType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "48%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxType.setDefaultUnit(kony.flex.DP);
            var lblTypeHeader = new kony.ui.Label({
                "id": "lblTypeHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblType\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxType = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxType",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "30dp",
                "width": "250dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "multiSelect": false
            });
            var flxTypeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxTypeError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%"
            }, {}, {});
            flxTypeError.setDefaultUnit(kony.flex.DP);
            var lblIconTypeError = new kony.ui.Label({
                "centerY": "52%",
                "height": "15dp",
                "id": "lblIconTypeError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTypeError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblTypeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.Please_select_a_role_type\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTypeError.add(lblIconTypeError, lblTypeError);
            flxType.add(lblTypeHeader, lstBoxType, flxTypeError);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "right": "30dp",
                "skin": "slFbox",
                "top": "100dp",
                "zIndex": 1
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblGroupDescription = new kony.ui.Label({
                "id": "lblGroupDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Description\")",
                "top": 0,
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblGroupDescriptionCount = new kony.ui.Label({
                "id": "lblGroupDescriptionCount",
                "isVisible": true,
                "right": "2px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupDescriptionCount\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtGroupDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "150dp",
                "id": "txtGroupDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 250,
                "numberOfVisibleLines": 3,
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoGroupDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoGroupDescriptionError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "180dp",
                "width": "100%"
            }, {}, {});
            flxNoGroupDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoGroupDescriptionError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblNoGroupDescriptionError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblNoGroupDescriptionError\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoGroupDescriptionErrorIcon = new kony.ui.Label({
                "centerY": "52%",
                "height": "15dp",
                "id": "lblNoGroupDescriptionErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoGroupDescriptionError.add(lblNoGroupDescriptionError, lblNoGroupDescriptionErrorIcon);
            flxDescription.add(lblGroupDescription, lblGroupDescriptionCount, txtGroupDescription, flxNoGroupDescriptionError);
            var flxGroupStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxGroupStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "310dp",
                "width": "110px",
                "zIndex": 1
            }, {}, {});
            flxGroupStatus.setDefaultUnit(kony.flex.DP);
            var lblSwitchActivate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSwitchActivate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato546e7a12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Active\")",
                "top": 0,
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchStatus = new kony.ui.Switch({
                "centerY": "50%",
                "height": "25dp",
                "id": "switchStatus",
                "isVisible": true,
                "left": "0dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupStatus.add(lblSwitchActivate, switchStatus);
            var flxEAgreement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEAgreement",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "150dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "310dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxEAgreement.setDefaultUnit(kony.flex.DP);
            var lblEAgreementStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEAgreementStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato546e7a12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.EAgreement_Required\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchEAgreement = new kony.ui.Switch({
                "centerY": "50%",
                "height": "25dp",
                "id": "switchEAgreement",
                "isVisible": true,
                "left": "20dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEAgreement.add(lblEAgreementStatus, switchEAgreement);
            flxAddGroupDetails.add(flxName, flxValidity, flxType, flxDescription, flxGroupStatus, flxEAgreement);
            var flxSeparator11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 91,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator11.setDefaultUnit(kony.flex.DP);
            flxSeparator11.add();
            var flxGroupDetailsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": true,
                "height": "50px",
                "id": "flxGroupDetailsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupDetailsButtons.setDefaultUnit(kony.flex.DP);
            var btnAddGroupCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnAddGroupCancel",
                "isVisible": true,
                "left": "35px",
                "right": "29.50%",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var flxAddGroupRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddGroupRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "width": "220px",
                "zIndex": 1
            }, {}, {});
            flxAddGroupRightButtons.setDefaultUnit(kony.flex.DP);
            var btnAddGroupNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px",
                "height": "40dp",
                "id": "btnAddGroupNext",
                "isVisible": true,
                "left": 0,
                "skin": "bt6fb3cf",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "Btn0eb251a7a12014cHover"
            });
            var btnAddGroupSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "slButtonGlossBlue0f445e8af7f3c49Hover",
                "height": "40dp",
                "id": "btnAddGroupSave",
                "isVisible": true,
                "right": "0px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxAddGroupRightButtons.add(btnAddGroupNext, btnAddGroupSave);
            flxGroupDetailsButtons.add(btnAddGroupCancel, flxAddGroupRightButtons);
            flxGroupDetails.add(flxAddGroupDetails, flxSeparator11, flxGroupDetailsButtons);
            var flxAssignEntitlements = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAssignEntitlements",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAssignEntitlements.setDefaultUnit(kony.flex.DP);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnNext": {
                        "width": "100px"
                    },
                    "commonButtons": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "width": "100%"
                    },
                    "flxRightButtons": {
                        "width": "210px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxButtons.add(commonButtons);
            var flxMain22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxMain22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain22.setDefaultUnit(kony.flex.DP);
            var addAndRemoveOptions = new com.adminConsole.common.addAndRemoveOptions1({
                "clipBounds": true,
                "height": "100%",
                "id": "addAndRemoveOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnRemoveAll": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")"
                    },
                    "flxAddOptionsSegment": {
                        "bottom": "15px"
                    },
                    "flxSegSelectedOptions": {
                        "bottom": "15px"
                    },
                    "imgLoadingAccountSearch": {
                        "src": "loadingscreenimage.gif"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMain22.add(addAndRemoveOptions);
            var flxSeparator12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 81,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator12.setDefaultUnit(kony.flex.DP);
            flxSeparator12.add();
            flxAssignEntitlements.add(flxButtons, flxMain22, flxSeparator12);
            var flxAssignCustomers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAssignCustomers",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAssignCustomers.setDefaultUnit(kony.flex.DP);
            var flxButtonsCustomers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxButtonsCustomers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxButtonsCustomers.setDefaultUnit(kony.flex.DP);
            var commonButtonsCustomers = new com.adminConsole.common.commonButtons({
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsCustomers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "commonButtons": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxButtonsCustomers.add(commonButtonsCustomers);
            var flxAssignMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "81dp",
                "clipBounds": true,
                "id": "flxAssignMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAssignMain.setDefaultUnit(kony.flex.DP);
            var addAndRemoveOptionsCustomers = new com.adminConsole.common.addAndRemoveOptions({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "addAndRemoveOptionsCustomers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "addAndRemoveOptions": {
                        "bottom": "0dp",
                        "height": "viz.val_cleared"
                    },
                    "imgClearSearch": {
                        "src": "close_blue.png"
                    },
                    "imgSearchIcon": {
                        "src": "search_1x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAssignMain.add(addAndRemoveOptionsCustomers);
            var flxSeparator13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 81,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator13.setDefaultUnit(kony.flex.DP);
            flxSeparator13.add();
            var flxSearchOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "180dp",
                "id": "flxSearchOptions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Border1px4pxRadius",
                "top": "100dp",
                "width": "50%",
                "zIndex": 100
            }, {}, {});
            flxSearchOptions.setDefaultUnit(kony.flex.DP);
            var flxSearchbyFullName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSearchbyFullName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchbyFullName.setDefaultUnit(kony.flex.DP);
            var lblFullName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFullName",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlbl9CA9BALato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.FullNameSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFullNameValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFullNameValue",
                "isVisible": true,
                "left": "150dp",
                "skin": "sknlbl333333Lato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblFullNameValue\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchbyFullName.add(lblFullName, lblFullNameValue);
            var flxSearchbyUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSearchbyUserName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchbyUserName.setDefaultUnit(kony.flex.DP);
            var lblUserName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUserName",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlbl9CA9BALato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.UserNameSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUserNameValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUserNameValue",
                "isVisible": true,
                "left": "150dp",
                "skin": "sknlbl333333Lato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblUserNameValue\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchbyUserName.add(lblUserName, lblUserNameValue);
            var flxSearchbyEmailId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSearchbyEmailId",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchbyEmailId.setDefaultUnit(kony.flex.DP);
            var lblEmailId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailId",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlbl9CA9BALato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.EmailIdSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailIdValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailIdValue",
                "isVisible": true,
                "left": "138dp",
                "skin": "sknlbl333333Lato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblEmailIdValue\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchbyEmailId.add(lblEmailId, lblEmailIdValue);
            var flxSearchbyContactNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSearchbyContactNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchbyContactNumber.setDefaultUnit(kony.flex.DP);
            var lblContactNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContactNumber",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlbl9CA9BALato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.ContactNumberSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContactNumberValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContactNumberValue",
                "isVisible": true,
                "left": "180dp",
                "skin": "sknlbl333333Lato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblContactNumberValue\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchbyContactNumber.add(lblContactNumber, lblContactNumberValue);
            var flxSeparatorSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "130dp",
                "width": "90%",
                "zIndex": 10
            }, {}, {});
            flxSeparatorSearch.setDefaultUnit(kony.flex.DP);
            flxSeparatorSearch.add();
            var btnAdvanceSearch = new kony.ui.Button({
                "height": "20dp",
                "id": "btnAdvanceSearch",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknBtnLato11ABEB11Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.AdvancedSearch\")",
                "top": "143dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchOptions.add(flxSearchbyFullName, flxSearchbyUserName, flxSearchbyEmailId, flxSearchbyContactNumber, flxSeparatorSearch, btnAdvanceSearch);
            flxAssignCustomers.add(flxButtonsCustomers, flxAssignMain, flxSeparator13, flxSearchOptions);
            var flxNoCustomerAssign = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxNoCustomerAssign",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxNoCustomerAssign.setDefaultUnit(kony.flex.DP);
            var flxTopSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxTopSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "60px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopSeparator.setDefaultUnit(kony.flex.DP);
            flxTopSeparator.add();
            var flxAssignCustomerMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "81dp",
                "clipBounds": true,
                "id": "flxAssignCustomerMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAssignCustomerMain.setDefaultUnit(kony.flex.DP);
            flxAssignCustomerMain.add();
            var lblCustAssignHeader = new kony.ui.Label({
                "id": "lblCustAssignHeader",
                "isVisible": true,
                "left": "10px",
                "skin": "sknLblLatoBold485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblCustAssignHeader\")",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCustAssignMessage = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "45%",
                "height": "30dp",
                "id": "lblNoCustAssignMessage",
                "isVisible": true,
                "skin": "sknLblLatoBold485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblNoCustAssignMessage\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddImportButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "53%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxAddImportButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "120dp"
            }, {}, {});
            flxAddImportButtons.setDefaultUnit(kony.flex.DP);
            var btnAddCustomer = new kony.ui.Button({
                "bottom": "0px",
                "centerY": "50%",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "btnAddCustomer",
                "isVisible": true,
                "left": "5dp",
                "right": "0px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnAddCustomer\")",
                "width": "110px",
                "zIndex": 15
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            var btnImportNewCust = new kony.ui.Button({
                "bottom": "0px",
                "centerY": "50%",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "btnImportNewCust",
                "isVisible": false,
                "left": "15dp",
                "right": "0px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.ImportCustomer_LC\")",
                "width": "120px",
                "zIndex": 15
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            flxAddImportButtons.add(btnAddCustomer, btnImportNewCust);
            var flxBottomSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 81,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxBottomSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBottomSeparator.setDefaultUnit(kony.flex.DP);
            flxBottomSeparator.add();
            var flxButtonsCustomer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxButtonsCustomer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxButtonsCustomer.setDefaultUnit(kony.flex.DP);
            var commonButtonsAddCustomers = new com.adminConsole.common.commonButtons({
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsAddCustomers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "commonButtons": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxButtonsCustomer.add(commonButtonsAddCustomers);
            flxNoCustomerAssign.add(flxTopSeparator, flxAssignCustomerMain, lblCustAssignHeader, lblNoCustAssignMessage, flxAddImportButtons, flxBottomSeparator, flxButtonsCustomer);
            var flxCustomerAssigned = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerAssigned",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerAssigned.setDefaultUnit(kony.flex.DP);
            var flxTopRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxTopRow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxTopRow.setDefaultUnit(kony.flex.DP);
            var flxAssignCustHeaderContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAssignCustHeaderContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAssignCustHeaderContainer.setDefaultUnit(kony.flex.DP);
            var lblCustAssignedHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustAssignedHeader",
                "isVisible": true,
                "left": "13px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblCustAssignedHeader\")",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 200
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddCustomerButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddCustomerButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 2
            }, {}, {});
            flxAddCustomerButtons.setDefaultUnit(kony.flex.DP);
            var btnAddMore = new kony.ui.Button({
                "bottom": "0px",
                "centerY": "50%",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "btnAddMore",
                "isVisible": true,
                "left": "0px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTermsAndConditionsController.Add\")",
                "top": "20px",
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            var btnImportCust = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "btnImportCust",
                "isVisible": true,
                "left": "65px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.Import_LC\")",
                "top": "0px",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            flxAddCustomerButtons.add(btnAddMore, btnImportCust);
            var flxRemoveCustButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRemoveCustButtons",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "zIndex": 2
            }, {}, {});
            flxRemoveCustButtons.setDefaultUnit(kony.flex.DP);
            var btnRemove = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "btnRemove",
                "isVisible": true,
                "left": "0dp",
                "right": "0px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.Remove_Selected\")",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            var btnRemoveAll = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "btnRemoveAll",
                "isVisible": true,
                "left": "130dp",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SelectedOptions.RemoveAll\")",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            flxRemoveCustButtons.add(btnRemove, btnRemoveAll);
            flxAssignCustHeaderContainer.add(lblCustAssignedHeader, flxAddCustomerButtons, flxRemoveCustButtons);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "focusSkin": "sknflxBgffffffBorder1293ccRadius30Px",
                "height": "36px",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 30,
                "skin": "sknflxBgffffffBorderc1c9ceRadius30px",
                "top": "10px",
                "width": "240px",
                "zIndex": 210
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearchimg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchimg",
                "isVisible": true,
                "left": "12dp",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconSearchimg\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 200
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_8869ed77fd9d4cc3bb4d7faea4e0afdb,
                "placeholder": "Search by Username",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 200
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearSearchImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearSearchImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "35px",
                "zIndex": 200
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearchImage.setDefaultUnit(kony.flex.DP);
            var fontIconCrossSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCrossSearch",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 200
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearchImage.add(fontIconCrossSearch);
            flxSearchContainer.add(fontIconSearchimg, tbxSearchBox, flxClearSearchImage);
            var flxTopSeparator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxTopSeparator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxTopSeparator1.setDefaultUnit(kony.flex.DP);
            flxTopSeparator1.add();
            flxTopRow.add(flxAssignCustHeaderContainer, flxSearchContainer, flxTopSeparator1);
            var flxAssignedCustHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55px",
                "id": "flxAssignedCustHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "60px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxAssignedCustHeader.setDefaultUnit(kony.flex.DP);
            var flxCustomerHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50px",
                "id": "flxCustomerHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxffffffop100",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxCustomerHeader.setDefaultUnit(kony.flex.DP);
            var flxAssinedCheckbox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxAssinedCheckbox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11px",
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "15px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAssinedCheckbox.setDefaultUnit(kony.flex.DP);
            var imgCustHeaderCheckBox = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "12px",
                "id": "imgCustHeaderCheckBox",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "12px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAssinedCheckbox.add(imgCustHeaderCheckBox);
            var flxCustomerHederName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustomerHederName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%",
                "zIndex": 1
            }, {}, {});
            flxCustomerHederName.setDefaultUnit(kony.flex.DP);
            var lblCustHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustHeaderName",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconAsgCustSortName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconAsgCustSortName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCustomerHederName.add(lblCustHeaderName, fontIconAsgCustSortName);
            var flxCustomerUsername = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustomerUsername",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "23%",
                "zIndex": 1
            }, {}, {});
            flxCustomerUsername.setDefaultUnit(kony.flex.DP);
            var lblCustHeaderUsername = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustHeaderUsername",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.USERNAME\")",
                "top": 0,
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconAsgCustSortUsername = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconAsgCustSortUsername",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCustomerUsername.add(lblCustHeaderUsername, fontIconAsgCustSortUsername);
            var flxCustomerID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustomerID",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "23%",
                "zIndex": 1
            }, {}, {});
            flxCustomerID.setDefaultUnit(kony.flex.DP);
            var lblCustHeaderID = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustHeaderID",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CUSTOMER_ID\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconAsgCustIdSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconAsgCustIdSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCustomerID.add(lblCustHeaderID, fontIconAsgCustIdSort);
            var flxCustomerHeaderStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustomerHeaderStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxCustomerHeaderStatus.setDefaultUnit(kony.flex.DP);
            var lblCustHeaderStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustHeaderStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": "45px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconAsgCustSortStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconAsgCustSortStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCustomerHeaderStatus.add(lblCustHeaderStatus, fontIconAsgCustSortStatus);
            flxCustomerHeader.add(flxAssinedCheckbox, flxCustomerHederName, flxCustomerUsername, flxCustomerID, flxCustomerHeaderStatus);
            var lblCustHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblCustHeaderSeperator",
                "isVisible": true,
                "left": "0px",
                "right": "20px",
                "skin": "sknLblTableHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": 51,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAssignedCustHeader.add(flxCustomerHeader, lblCustHeaderSeperator);
            var flxAssignCustomerAdded = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "81dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAssignCustomerAdded",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "113dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAssignCustomerAdded.setDefaultUnit(kony.flex.DP);
            var segAssignedCustomers = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "data": [{
                    "fontIconCustomerStatus": "",
                    "fontIconDelete": "",
                    "imgCheckBox": "",
                    "lblCustID": "",
                    "lblName": "",
                    "lblSeperator": "",
                    "lblServicesStatus": "",
                    "lblUserName": ""
                }],
                "groupCells": false,
                "id": "segAssignedCustomers",
                "isVisible": true,
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxGroupsAssignedCustomers",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
                "selectionBehaviorConfig": {
                    "imageIdentifier": "imgCheckBox",
                    "selectedStateImage": "checkboxselected.png",
                    "unselectedStateImage": "checkbox.png"
                },
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAssignedCustomers": "flxAssignedCustomers",
                    "flxCheckbox": "flxCheckbox",
                    "flxDelete": "flxDelete",
                    "flxGroupsAssignedCustomers": "flxGroupsAssignedCustomers",
                    "flxStatus": "flxStatus",
                    "fontIconCustomerStatus": "fontIconCustomerStatus",
                    "fontIconDelete": "fontIconDelete",
                    "imgCheckBox": "imgCheckBox",
                    "lblCustID": "lblCustID",
                    "lblName": "lblName",
                    "lblSeperator": "lblSeperator",
                    "lblServicesStatus": "lblServicesStatus",
                    "lblUserName": "lblUserName"
                },
                "width": "100%",
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxNoResults = new kony.ui.RichText({
                "centerX": "50.00%",
                "centerY": "50%",
                "id": "rtxNoResults",
                "isVisible": false,
                "left": "145dp",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAssignCustomerAdded.add(segAssignedCustomers, rtxNoResults);
            var flxCustomerStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomerStatusFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "593dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "86dp",
                "width": "120px",
                "zIndex": 12
            }, {}, {});
            flxCustomerStatusFilter.setDefaultUnit(kony.flex.DP);
            var custStatusFilter = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "custStatusFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCustomerStatusFilter.add(custStatusFilter);
            var flxButtonsCustomerAdded = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxButtonsCustomerAdded",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxButtonsCustomerAdded.setDefaultUnit(kony.flex.DP);
            var flxBottomSeparator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxBottomSeparator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 2
            }, {}, {});
            flxBottomSeparator1.setDefaultUnit(kony.flex.DP);
            flxBottomSeparator1.add();
            var commonButtonsAddedCustomers = new com.adminConsole.common.commonButtons({
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsAddedCustomers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "commonButtons": {
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxButtonsCustomerAdded.add(flxBottomSeparator1, commonButtonsAddedCustomers);
            flxCustomerAssigned.add(flxTopRow, flxAssignedCustHeader, flxAssignCustomerAdded, flxCustomerStatusFilter, flxButtonsCustomerAdded);
            var flxSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "224dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 10
            }, {}, {});
            flxSeparator.setDefaultUnit(kony.flex.DP);
            flxSeparator.add();
            flxAddGroups.add(flxVerticalTabs, flxGroupDetails, flxAssignEntitlements, flxAssignCustomers, flxNoCustomerAssign, flxCustomerAssigned, flxSeparator);
            var flxGroupList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxGroupList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "105px",
                "zIndex": 1
            }, {}, {});
            flxGroupList.setDefaultUnit(kony.flex.DP);
            var flxGroupListMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxGroupListMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupListMain.setDefaultUnit(kony.flex.DP);
            var flxGroupSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxGroupSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupSearch.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "centerY": "50%",
                "clipBounds": true,
                "height": "48dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false,
                        "left": "0dp"
                    },
                    "flxSearch": {
                        "right": 0
                    },
                    "subHeader": {
                        "centerY": "50%"
                    },
                    "tbxSearchBox": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmGroups.Search_by_Group_Name\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxGroupSearch.add(subHeader);
            var flxSegGroups = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxSegGroups",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffOp100BorderE1E5Ed",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegGroups.setDefaultUnit(kony.flex.DP);
            var flxGroupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxGroupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100",
                "top": "0"
            }, {}, {});
            flxGroupHeader.setDefaultUnit(kony.flex.DP);
            var flxGroupName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_aa9ff028c98444408c909813010b2efa,
                "skin": "slFbox",
                "top": 0,
                "width": "18.50%",
                "zIndex": 1
            }, {}, {});
            flxGroupName.setDefaultUnit(kony.flex.DP);
            var lblGroupName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroupName",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": "37px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSortName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupName.add(lblGroupName, fontIconSortName);
            var flxGroupDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "18.50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "43.20%",
                "zIndex": 1
            }, {}, {});
            flxGroupDescription.setDefaultUnit(kony.flex.DP);
            var lblHeaderGroupDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderGroupDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "top": 0,
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupDescription.add(lblHeaderGroupDescription);
            var flxGroupValidTill = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupValidTill",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_g6c11e5273c94b1a86ae0747e84ebd44,
                "skin": "slFbox",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxGroupValidTill.setDefaultUnit(kony.flex.DP);
            var lblGroupValidTill = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroupValidTill",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.ValidTill\")",
                "top": 0,
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSortValidTill = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortValidTill",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupValidTill.add(lblGroupValidTill, fontIconSortValidTill);
            var flxGroupCustomers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupCustomers",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "61.70%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxGroupCustomers.setDefaultUnit(kony.flex.DP);
            var lblGroupCustomerss = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroupCustomerss",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Customers\")",
                "top": 0,
                "width": "72px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSortCustomers = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortCustomers",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupCustomers.add(lblGroupCustomerss, fontIconSortCustomers);
            var flxGroupEntitlements = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupEntitlements",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "73.50%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12.70%",
                "zIndex": 1
            }, {}, {});
            flxGroupEntitlements.setDefaultUnit(kony.flex.DP);
            var lblGroupEntitlements = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroupEntitlements",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "ROLE TYPE",
                "top": 0,
                "width": "69px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSortEntitlements = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortEntitlements",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupEntitlements.add(lblGroupEntitlements, fontIconSortEntitlements);
            var flxGroupHeaderStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupHeaderStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d3a97a0f3de34380a91ef7e725e093be,
                "right": "0%",
                "skin": "slFbox",
                "top": "0dp",
                "width": "13.80%",
                "zIndex": 1
            }, {}, {});
            flxGroupHeaderStatus.setDefaultUnit(kony.flex.DP);
            var lblGroupStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroupStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": 0,
                "width": "46px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconGroupStatusFilter = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconGroupStatusFilter",
                "isVisible": true,
                "left": "2.50%",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxGroupHeaderStatus.add(lblGroupStatus, fontIconGroupStatusFilter);
            flxGroupHeader.add(flxGroupName, flxGroupDescription, flxGroupValidTill, flxGroupCustomers, flxGroupEntitlements, flxGroupHeaderStatus);
            var flxGroupSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxGroupSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknTableHeaderLine",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxGroupSeparator.setDefaultUnit(kony.flex.DP);
            flxGroupSeparator.add();
            var flxGroupListContainer = new kony.ui.FlexContainer({
                "bottom": "10px",
                "clipBounds": true,
                "height": "676dp",
                "id": "flxGroupListContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "61dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupListContainer.setDefaultUnit(kony.flex.DP);
            var listingSegmentClient = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "height": "100%",
                "id": "listingSegmentClient",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "contextualMenu.btnLink1": {
                        "bottom": "10dp",
                        "top": "0dp"
                    },
                    "contextualMenu.btnLink2": {
                        "bottom": "10dp",
                        "top": "0dp"
                    },
                    "contextualMenu.flxOption2": {
                        "height": "30dp"
                    },
                    "contextualMenu.flxOption4": {
                        "bottom": "15dp",
                        "height": "30dp"
                    },
                    "contextualMenu.flxOptionsSeperator": {
                        "bottom": "5dp",
                        "top": "5px"
                    },
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "contextualMenu.lblHeader": {
                        "bottom": "15dp",
                        "top": "20dp"
                    },
                    "contextualMenu.lblIconOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")"
                    },
                    "flxContextualMenu": {
                        "left": "viz.val_cleared",
                        "right": "35dp"
                    },
                    "flxListingSegmentWrapper": {
                        "bottom": "viz.val_cleared",
                        "height": "100%"
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "35dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "35dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "pagination": {
                        "isVisible": false
                    },
                    "segListing": {
                        "bottom": "viz.val_cleared",
                        "height": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxGroupListContainer.add(listingSegmentClient);
            var flxStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStatusFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "810px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "37px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilter.add(statusFilterMenu);
            var flxTypeFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTypeFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "730px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "37px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxTypeFilter.setDefaultUnit(kony.flex.DP);
            var typeFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "typeFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTypeFilter.add(typeFilterMenu);
            flxSegGroups.add(flxGroupHeader, flxGroupSeparator, flxGroupListContainer, flxStatusFilter, flxTypeFilter);
            flxGroupListMain.add(flxGroupSearch, flxSegGroups);
            flxGroupList.add(flxGroupListMain);
            var flxGroupview = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "35px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxGroupview",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "right": "35px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "150dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxGroupview.setDefaultUnit(kony.flex.DP);
            var flxGroupViewDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGroupViewDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupViewDetails.setDefaultUnit(kony.flex.DP);
            var flxGroupViewContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGroupViewContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupViewContainer.setDefaultUnit(kony.flex.DP);
            var flxGroupViewName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxGroupViewName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxGroupViewName.setDefaultUnit(kony.flex.DP);
            var fontIconViewName = new kony.ui.Label({
                "id": "fontIconViewName",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewName\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewName = new kony.ui.Label({
                "id": "lblViewName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewNameValue = new kony.ui.Label({
                "id": "lblViewNameValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewNameValue\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupViewName.add(fontIconViewName, lblViewName, lblViewNameValue);
            var flxGroupRoleType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxGroupRoleType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "24%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxGroupRoleType.setDefaultUnit(kony.flex.DP);
            var lblIconGroupType = new kony.ui.Label({
                "id": "lblIconGroupType",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewName\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewGroupType = new kony.ui.Label({
                "id": "lblViewGroupType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.TYPE\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewGroupTypeValue = new kony.ui.Label({
                "id": "lblViewGroupTypeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewNameValue\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupRoleType.add(lblIconGroupType, lblViewGroupType, lblViewGroupTypeValue);
            var flxViewAgreement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewAgreement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "44%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "16%",
                "zIndex": 1
            }, {}, {});
            flxViewAgreement.setDefaultUnit(kony.flex.DP);
            var lblIconViewAgreement = new kony.ui.Label({
                "id": "lblIconViewAgreement",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewName\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewAgreement = new kony.ui.Label({
                "id": "lblViewAgreement",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.E-AGREEMENT\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewAgreementValue = new kony.ui.Label({
                "id": "lblViewAgreementValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewNameValue\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAgreement.add(lblIconViewAgreement, lblViewAgreement, lblViewAgreementValue);
            var flxGroupViewStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxGroupViewStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxGroupViewStatus.setDefaultUnit(kony.flex.DP);
            var fontIconViewStatus = new kony.ui.Label({
                "id": "fontIconViewStatus",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewStatus\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconStatusValue = new kony.ui.Label({
                "id": "fontIconStatusValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStatusView = new kony.ui.Label({
                "id": "lblStatusView",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue2 = new kony.ui.Label({
                "id": "lblViewValue2",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "22px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupViewStatus.add(fontIconViewStatus, fontIconStatusValue, lblStatusView, lblViewValue2);
            var flxGroupViewValidTill = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxGroupViewValidTill",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxGroupViewValidTill.setDefaultUnit(kony.flex.DP);
            var fontIconValidTill = new kony.ui.Label({
                "id": "fontIconValidTill",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconValidTill\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblValidTillView = new kony.ui.Label({
                "id": "lblValidTillView",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.VALIDTILL\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValidTillValue = new kony.ui.Label({
                "id": "lblViewValidTillValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewValidTillValue\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupViewValidTill.add(fontIconValidTill, lblValidTillView, lblViewValidTillValue);
            var Description = new com.adminConsole.view.Description1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "Description",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "overrides": {
                    "Description1": {
                        "bottom": "10dp",
                        "top": "70dp"
                    },
                    "imgToggleDescription": {
                        "src": "img_down_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var btnEdit = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px",
                "height": "25dp",
                "id": "btnEdit",
                "isVisible": true,
                "right": "35dp",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "15dp",
                "width": "60dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px"
            });
            var flxEditDisableSkin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEditDisableSkin",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxbgffffffOp50Per",
                "top": "15dp",
                "width": "60dp",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursorDisabled"
            });
            flxEditDisableSkin.setDefaultUnit(kony.flex.DP);
            flxEditDisableSkin.add();
            flxGroupViewContainer.add(flxGroupViewName, flxGroupRoleType, flxViewAgreement, flxGroupViewStatus, flxGroupViewValidTill, Description, btnEdit, flxEditDisableSkin);
            flxGroupViewDetails.add(flxGroupViewContainer);
            var flxTableView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxTableView",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTableView.setDefaultUnit(kony.flex.DP);
            var flxTableViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxTableViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxTableViewTabs.setDefaultUnit(kony.flex.DP);
            var flxEntitlements = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60%",
                "id": "flxEntitlements",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40%",
                "width": "165px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxEntitlements.setDefaultUnit(kony.flex.DP);
            var lblEntitlements = new kony.ui.Label({
                "height": "100%",
                "id": "lblEntitlements",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Entitlements\")",
                "top": "0dp",
                "width": "150px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            var flxIdentifier = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "3dp",
                "id": "flxIdentifier",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "top": "20dp",
                "width": "140px",
                "zIndex": 1
            }, {}, {});
            flxIdentifier.setDefaultUnit(kony.flex.DP);
            flxIdentifier.add();
            flxEntitlements.add(lblEntitlements, flxIdentifier);
            var flxCustomers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60%",
                "id": "flxCustomers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40%",
                "width": "100px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCustomers.setDefaultUnit(kony.flex.DP);
            var lblCustomersFix = new kony.ui.Label({
                "height": "100%",
                "id": "lblCustomersFix",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Customers\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            var flxIdentifier2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "3dp",
                "id": "flxIdentifier2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "top": "20dp",
                "width": "72px",
                "zIndex": 1
            }, {}, {});
            flxIdentifier2.setDefaultUnit(kony.flex.DP);
            flxIdentifier2.add();
            flxCustomers.add(lblCustomersFix, flxIdentifier2);
            flxTableViewTabs.add(flxEntitlements, flxCustomers);
            var flxViewSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxD5D9DD",
                "top": "60dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxViewSeperator.setDefaultUnit(kony.flex.DP);
            flxViewSeperator.add();
            var flxViewSegmentAndHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxViewSegmentAndHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": 35,
                "skin": "slFbox",
                "top": "62dp",
                "zIndex": 1
            }, {}, {});
            flxViewSegmentAndHeaders.setDefaultUnit(kony.flex.DP);
            var flxEntitlementsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxEntitlementsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxEntitlementsHeader.setDefaultUnit(kony.flex.DP);
            var flxViewEntitlementsName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewEntitlementsName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewEntitlementsName.setDefaultUnit(kony.flex.DP);
            var lblViewEntitlementsName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewEntitlementsName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": "37px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconViewEntmntNameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconViewEntmntNameSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewEntitlementsName.add(lblViewEntitlementsName, fontIconViewEntmntNameSort);
            var flxViewEntitlementsDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewEntitlementsDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewEntitlementsDescription.setDefaultUnit(kony.flex.DP);
            var lblViewEntitlementsDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewEntitlementsDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "top": 0,
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewEntitlementsDescription.add(lblViewEntitlementsDescription);
            var flxViewEntitlementsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewEntitlementsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewEntitlementsSeperator.setDefaultUnit(kony.flex.DP);
            flxViewEntitlementsSeperator.add();
            flxEntitlementsHeader.add(flxViewEntitlementsName, flxViewEntitlementsDescription, flxViewEntitlementsSeperator);
            var flxCustomersHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxCustomersHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxCustomersHeader.setDefaultUnit(kony.flex.DP);
            var flxViewCustomersFullName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewCustomersFullName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewCustomersFullName.setDefaultUnit(kony.flex.DP);
            var lblViewCustomersFullName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewCustomersFullName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.FULLNAME\")",
                "top": 0,
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconViewCustNameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconViewCustNameSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewCustomersFullName.add(lblViewCustomersFullName, fontIconViewCustNameSort);
            var flxViewCustomersUsername = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewCustomersUsername",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "18.93%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewCustomersUsername.setDefaultUnit(kony.flex.DP);
            var lblViewCustomersUsername = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewCustomersUsername",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.USERNAME\")",
                "top": 0,
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconViewCustUsrnameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconViewCustUsrnameSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewCustomersUsername.add(lblViewCustomersUsername, fontIconViewCustUsrnameSort);
            var flxViewCustomersEmailId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewCustomersEmailId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "36.69%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewCustomersEmailId.setDefaultUnit(kony.flex.DP);
            var lblEmailCustomers = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailCustomers",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.EMAILID\")",
                "top": 0,
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconViewCustMailSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconViewCustMailSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewCustomersEmailId.add(lblEmailCustomers, fontIconViewCustMailSort);
            var flxViewCustomersUpdatedBy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewCustomersUpdatedBy",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "60.35%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewCustomersUpdatedBy.setDefaultUnit(kony.flex.DP);
            var lblCustomersUpdatedBy = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustomersUpdatedBy",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.UPDATEDBY\")",
                "top": 0,
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconCustUpdatedBySort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCustUpdatedBySort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewCustomersUpdatedBy.add(lblCustomersUpdatedBy, fontIconCustUpdatedBySort);
            var flxViewCustomersUpdatedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewCustomersUpdatedOn",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "77.28%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewCustomersUpdatedOn.setDefaultUnit(kony.flex.DP);
            var lblCustomersUpdatedOn = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustomersUpdatedOn",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.UPDATEDON\")",
                "top": 0,
                "width": "82px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconCustUpdatedOnSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCustUpdatedOnSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewCustomersUpdatedOn.add(lblCustomersUpdatedOn, fontIconCustUpdatedOnSort);
            var flxViewCustomersSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewCustomersSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewCustomersSeperator.setDefaultUnit(kony.flex.DP);
            flxViewCustomersSeperator.add();
            flxCustomersHeader.add(flxViewCustomersFullName, flxViewCustomersUsername, flxViewCustomersEmailId, flxViewCustomersUpdatedBy, flxViewCustomersUpdatedOn, flxViewCustomersSeperator);
            var flxViewSegment = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "270dp",
                "horizontalScrollIndicator": true,
                "id": "flxViewSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "61dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSegment.setDefaultUnit(kony.flex.DP);
            var segViewSegment = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "imgConfigure": "",
                    "lblEntitlementsDescription": "",
                    "lblEntitlementsName": "",
                    "lblEntitlementsSeperator": ""
                }],
                "groupCells": false,
                "id": "segViewSegment",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxViewEntitlements",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxEntitlementsConfigure": "flxEntitlementsConfigure",
                    "flxViewEntitlements": "flxViewEntitlements",
                    "imgConfigure": "imgConfigure",
                    "lblEntitlementsDescription": "lblEntitlementsDescription",
                    "lblEntitlementsName": "lblEntitlementsName",
                    "lblEntitlementsSeperator": "lblEntitlementsSeperator"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoResultMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxNoResultMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoResultMessage.setDefaultUnit(kony.flex.DP);
            var rtxNoRecordsAvailable = new kony.ui.RichText({
                "centerY": "50%",
                "id": "rtxNoRecordsAvailable",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.rtxNoRecordsAvailable\")",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultMessage.add(rtxNoRecordsAvailable);
            flxViewSegment.add(segViewSegment, flxNoResultMessage);
            flxViewSegmentAndHeaders.add(flxEntitlementsHeader, flxCustomersHeader, flxViewSegment);
            flxTableView.add(flxTableViewTabs, flxViewSeperator, flxViewSegmentAndHeaders);
            flxGroupview.add(flxGroupViewDetails, flxTableView);
            var flxEntitlementDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxEntitlementDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffOp100BorderE1E5Ed",
                "top": "126dp",
                "zIndex": 1
            }, {}, {});
            flxEntitlementDetails.setDefaultUnit(kony.flex.DP);
            var flxVerticalTabsEntitlement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxVerticalTabsEntitlement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "225px",
                "zIndex": 1
            }, {}, {});
            flxVerticalTabsEntitlement.setDefaultUnit(kony.flex.DP);
            var verticalTabsEntitlement = new com.adminConsole.common.verticalTabs({
                "clipBounds": true,
                "height": "100%",
                "id": "verticalTabsEntitlement",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.EntitlementDetails\")"
                    },
                    "btnOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Limits\")"
                    },
                    "btnOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TRANSACTIONFEE\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxVerticalTabsEntitlement.add(verticalTabsEntitlement);
            var flxSeparator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSeparator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "224dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 10
            }, {}, {});
            flxSeparator2.setDefaultUnit(kony.flex.DP);
            flxSeparator2.add();
            var flxRightEntitlementDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxRightEntitlementDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightEntitlementDetails.setDefaultUnit(kony.flex.DP);
            var flxEntitlementDetailsMain = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "81px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxEntitlementDetailsMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "-15dp",
                "pagingEnabled": false,
                "right": "10dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxEntitlementDetailsMain.setDefaultUnit(kony.flex.DP);
            var flxGeneralInformation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxGeneralInformation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGeneralInformation.setDefaultUnit(kony.flex.DP);
            var lblGeneralInformation = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGeneralInformation",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlbl9ba9baLato11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblGenaralInfoHeader\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownGeneralInformation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDropdownGeneralInformation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "165dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {}, {});
            flxDropdownGeneralInformation.setDefaultUnit(kony.flex.DP);
            var imgDropdownGeneralInfo = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgDropdownGeneralInfo",
                "isVisible": true,
                "left": "7dp",
                "skin": "slImage",
                "src": "downrow.png",
                "top": "8dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownGeneralInformation.add(imgDropdownGeneralInfo);
            var flxSeparator3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "52%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "49dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSeparator3.setDefaultUnit(kony.flex.DP);
            flxSeparator3.add();
            flxGeneralInformation.add(lblGeneralInformation, flxDropdownGeneralInformation, flxSeparator3);
            var flxGeneralInformationDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGeneralInformationDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGeneralInformationDetails.setDefaultUnit(kony.flex.DP);
            var details = new com.adminConsole.view.details({
                "clipBounds": true,
                "height": "40px",
                "id": "details",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "details": {
                        "left": "35dp",
                        "top": "10dp"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var details1 = new com.adminConsole.view.details({
                "clipBounds": true,
                "height": "40px",
                "id": "details1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "details": {
                        "left": "35dp",
                        "right": "35px",
                        "top": "10dp"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var Description1 = new com.adminConsole.view.Description({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "Description1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "Description": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0dp",
                        "top": "10dp"
                    },
                    "imgToggleDescription": {
                        "src": "img_down_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var Description2 = new com.adminConsole.view.Description({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "15px",
                "clipBounds": true,
                "id": "Description2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "Description": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "15px",
                        "top": "10dp"
                    },
                    "imgToggleDescription": {
                        "src": "img_down_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxGeneralInformationDetails.add(details, details1, Description1, Description2);
            var flxSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxSettings",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSettings.setDefaultUnit(kony.flex.DP);
            var lblSettings = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSettings",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlbl9ba9baLato11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SETTINGS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDropdownSettings",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "90dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {}, {});
            flxDropdownSettings.setDefaultUnit(kony.flex.DP);
            var imgSettings = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgSettings",
                "isVisible": true,
                "left": "7dp",
                "skin": "slImage",
                "src": "downrow.png",
                "top": "8dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownSettings.add(imgSettings);
            var flxSeparator4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "52%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "49dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSeparator4.setDefaultUnit(kony.flex.DP);
            flxSeparator4.add();
            flxSettings.add(lblSettings, flxDropdownSettings, flxSeparator4);
            var flxSettingsDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSettingsDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSettingsDetails.setDefaultUnit(kony.flex.DP);
            var details2 = new com.adminConsole.view.details({
                "clipBounds": true,
                "height": "40px",
                "id": "details2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxServiceChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxServiceChannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxServiceChannel.setDefaultUnit(kony.flex.DP);
            var lblServiceChannel = new kony.ui.Label({
                "id": "lblServiceChannel",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.ServiceChannel\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCheckbox = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgCheckbox",
                "isVisible": true,
                "left": "35dp",
                "skin": "slImage",
                "src": "checkbox.png",
                "top": "35dp",
                "width": "12dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCheckbox2 = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgCheckbox2",
                "isVisible": true,
                "left": "195dp",
                "skin": "slImage",
                "src": "checkbox.png",
                "top": "35dp",
                "width": "12dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblInternetBanking = new kony.ui.Label({
                "id": "lblInternetBanking",
                "isVisible": true,
                "left": "55dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.18n.internetBanking\")",
                "top": "33dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIMobileBanking = new kony.ui.Label({
                "id": "lblIMobileBanking",
                "isVisible": true,
                "left": "215dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.MobileBanking\")",
                "top": "33dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServiceChannel.add(lblServiceChannel, imgCheckbox, imgCheckbox2, lblInternetBanking, lblIMobileBanking);
            var lblSchedule = new kony.ui.Label({
                "id": "lblSchedule",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Schedule\")",
                "top": "100dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxScheduler = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "30px",
                "clipBounds": true,
                "focusSkin": "sknflx4b6f9aRadius4px",
                "height": "40dp",
                "id": "flxScheduler",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "sknflxBorder1px4b6f9aRadius4px",
                "top": "125dp",
                "width": "150px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknflx4b6f9aRadius4px"
            });
            flxScheduler.setDefaultUnit(kony.flex.DP);
            var lblScheduler = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblScheduler",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.24x7_Scheduler\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "CopysknlblLatoBold0j5781702776443"
            });
            var flxClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxClose.setDefaultUnit(kony.flex.DP);
            var imgClose = new kony.ui.Image2({
                "centerY": "50%",
                "height": "12dp",
                "id": "imgClose",
                "isVisible": true,
                "left": "10dp",
                "skin": "slImage",
                "src": "close_blue.png",
                "top": "10dp",
                "width": "12dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClose.add(imgClose);
            flxScheduler.add(lblScheduler, flxClose);
            var btnSelectScheduler = new kony.ui.Button({
                "height": "15dp",
                "id": "btnSelectScheduler",
                "isVisible": false,
                "left": "35dp",
                "skin": "sknBtnLato11ABEB11Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnSelectScheduler\")",
                "top": "125dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSettingsDetails.add(details2, flxServiceChannel, lblSchedule, flxScheduler, btnSelectScheduler);
            var flxSmsAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxSmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSmsAlert.setDefaultUnit(kony.flex.DP);
            var lbSmsAlert = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbSmsAlert",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlbl9ba9baLato11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SMSALERT\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownSmsAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDropdownSmsAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "150dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {}, {});
            flxDropdownSmsAlert.setDefaultUnit(kony.flex.DP);
            var imgSmsAlert = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgSmsAlert",
                "isVisible": true,
                "left": "7dp",
                "skin": "slImage",
                "src": "downrow.png",
                "top": "8dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownSmsAlert.add(imgSmsAlert);
            var flxSwitch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "110dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "36dp",
                "zIndex": 1
            }, {}, {});
            flxSwitch.setDefaultUnit(kony.flex.DP);
            var switchSMSAlert = new kony.ui.Switch({
                "height": "100%",
                "id": "switchSMSAlert",
                "isVisible": true,
                "left": "0dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSwitch.add(switchSMSAlert);
            var flxSeparato5 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "52%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparato5",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "49dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSeparato5.setDefaultUnit(kony.flex.DP);
            flxSeparato5.add();
            flxSmsAlert.add(lbSmsAlert, flxDropdownSmsAlert, flxSwitch, flxSeparato5);
            var flxAlertDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120dp",
                "id": "flxAlertDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetails.setDefaultUnit(kony.flex.DP);
            var lblCharge = new kony.ui.Label({
                "id": "lblCharge",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Charge\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxtbxChargeSMS = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxtbxChargeSMS",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "sknflxBorder",
                "top": "35dp",
                "width": "215px",
                "zIndex": 1
            }, {}, {});
            flxtbxChargeSMS.setDefaultUnit(kony.flex.DP);
            var flxdollar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxdollar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopysknflxBorder0b74a7dbe63fe4a",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxdollar.setDefaultUnit(kony.flex.DP);
            var lblDollar = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblDollar",
                "isVisible": true,
                "skin": "sknlblLatoBold35475f18px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblDollar\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxdollar.add(lblDollar);
            var FlexContainer0e7ef9804c39246 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexContainer0e7ef9804c39246",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            FlexContainer0e7ef9804c39246.setDefaultUnit(kony.flex.DP);
            FlexContainer0e7ef9804c39246.add();
            var tbxChargeSMS = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "33dp",
                "id": "tbxChargeSMS",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "45dp",
                "placeholder": "0.00",
                "secureTextEntry": false,
                "skin": "skntbxNoborder",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "3dp",
                "width": "165dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxtbxDisablesms = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "33dp",
                "id": "flxtbxDisablesms",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "45dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0abd84d27de704b",
                "top": "3dp",
                "width": "165dp",
                "zIndex": 100
            }, {}, {});
            flxtbxDisablesms.setDefaultUnit(kony.flex.DP);
            flxtbxDisablesms.add();
            flxtbxChargeSMS.add(flxdollar, FlexContainer0e7ef9804c39246, tbxChargeSMS, flxtbxDisablesms);
            flxAlertDetails.add(lblCharge, flxtbxChargeSMS);
            var flxBeneficiarySMSAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxBeneficiarySMSAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBeneficiarySMSAlert.setDefaultUnit(kony.flex.DP);
            var lbBeneficiarySMSAlert = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbBeneficiarySMSAlert",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlbl9ba9baLato11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.BENEFICIARYSMSALERT\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownBeneficiarySMSAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDropdownBeneficiarySMSAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {}, {});
            flxDropdownBeneficiarySMSAlert.setDefaultUnit(kony.flex.DP);
            var imgBeneficiarySMSAlert = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgBeneficiarySMSAlert",
                "isVisible": true,
                "left": "7dp",
                "skin": "slImage",
                "src": "downrow.png",
                "top": "8dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownBeneficiarySMSAlert.add(imgBeneficiarySMSAlert);
            var flxSwitch2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxSwitch2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "180dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "36dp",
                "zIndex": 1
            }, {}, {});
            flxSwitch2.setDefaultUnit(kony.flex.DP);
            var SwitchBeneficiarySMSAlert = new kony.ui.Switch({
                "height": "100%",
                "id": "SwitchBeneficiarySMSAlert",
                "isVisible": true,
                "left": "0dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSwitch2.add(SwitchBeneficiarySMSAlert);
            var flxSeparato6 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "52%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparato6",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "49dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSeparato6.setDefaultUnit(kony.flex.DP);
            flxSeparato6.add();
            flxBeneficiarySMSAlert.add(lbBeneficiarySMSAlert, flxDropdownBeneficiarySMSAlert, flxSwitch2, flxSeparato6);
            var flxBeneficiarySMSAlertDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120dp",
                "id": "flxBeneficiarySMSAlertDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBeneficiarySMSAlertDetails.setDefaultUnit(kony.flex.DP);
            var lblCharge2 = new kony.ui.Label({
                "id": "lblCharge2",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Charge\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxtbxChargeBeneficiarySMSAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxtbxChargeBeneficiarySMSAlert",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "sknflxBorder",
                "top": "35dp",
                "width": "215px",
                "zIndex": 1
            }, {}, {});
            flxtbxChargeBeneficiarySMSAlert.setDefaultUnit(kony.flex.DP);
            var flxdollar2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxdollar2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopysknflxBorder0b74a7dbe63fe4a",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxdollar2.setDefaultUnit(kony.flex.DP);
            var lblDollar2 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblDollar2",
                "isVisible": true,
                "skin": "sknlblLatoBold35475f18px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblDollar\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxdollar2.add(lblDollar2);
            var flxSeparator7 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSeparator7",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxSeparator7.setDefaultUnit(kony.flex.DP);
            flxSeparator7.add();
            var tbxChargeBeneficiarySMSAlert = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "39dp",
                "id": "tbxChargeBeneficiarySMSAlert",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "41dp",
                "placeholder": "0.00",
                "secureTextEntry": false,
                "skin": "skntbxNoborder",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "172dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxtbxDisable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "33dp",
                "id": "flxtbxDisable",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "45dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0abd84d27de704b",
                "top": "3dp",
                "width": "165dp",
                "zIndex": 100
            }, {}, {});
            flxtbxDisable.setDefaultUnit(kony.flex.DP);
            flxtbxDisable.add();
            flxtbxChargeBeneficiarySMSAlert.add(flxdollar2, flxSeparator7, tbxChargeBeneficiarySMSAlert, flxtbxDisable);
            flxBeneficiarySMSAlertDetails.add(lblCharge2, flxtbxChargeBeneficiarySMSAlert);
            var flxOperations = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxOperations",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOperations.setDefaultUnit(kony.flex.DP);
            var lblOperations = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOperations",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlbl9ba9baLato11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.BENEFICIARYSMSALERT\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownOperations = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDropdownOperations",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {}, {});
            flxDropdownOperations.setDefaultUnit(kony.flex.DP);
            var imgOperations = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOperations",
                "isVisible": true,
                "left": "7dp",
                "skin": "slImage",
                "src": "downrow.png",
                "top": "8dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownOperations.add(imgOperations);
            var flxSwitch3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxSwitch3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "180dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "36dp",
                "zIndex": 1
            }, {}, {});
            flxSwitch3.setDefaultUnit(kony.flex.DP);
            var SwitchOperations = new kony.ui.Switch({
                "height": "100%",
                "id": "SwitchOperations",
                "isVisible": true,
                "left": "0dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSwitch3.add(SwitchOperations);
            var flxSeparato8 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "52%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparato8",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "49dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSeparato8.setDefaultUnit(kony.flex.DP);
            flxSeparato8.add();
            flxOperations.add(lblOperations, flxDropdownOperations, flxSwitch3, flxSeparato8);
            var flxOperationsDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "100dp",
                "id": "flxOperationsDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOperationsDetails.setDefaultUnit(kony.flex.DP);
            var flxOptionsDetailsRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxOptionsDetailsRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxOptionsDetailsRow1.setDefaultUnit(kony.flex.DP);
            var lblFutureTransactionFlag = new kony.ui.Label({
                "id": "lblFutureTransactionFlag",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.FutureTransactionFlag\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOutageMessage = new kony.ui.Label({
                "id": "lblOutageMessage",
                "isVisible": true,
                "left": "250dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.OutageMessage\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTACRequired = new kony.ui.Label({
                "id": "lblTACRequired",
                "isVisible": true,
                "left": "450dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TACRequired\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var SwitchFutureTransactionFlag = new kony.ui.Switch({
                "height": "25dp",
                "id": "SwitchFutureTransactionFlag",
                "isVisible": true,
                "left": "135dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "5dp",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchOutageMessage = new kony.ui.Switch({
                "height": "25dp",
                "id": "switchOutageMessage",
                "isVisible": true,
                "left": "350dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "5dp",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var SwitchTACRequired = new kony.ui.Switch({
                "height": "25dp",
                "id": "SwitchTACRequired",
                "isVisible": true,
                "left": "550dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "5dp",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptionsDetailsRow1.add(lblFutureTransactionFlag, lblOutageMessage, lblTACRequired, SwitchFutureTransactionFlag, switchOutageMessage, SwitchTACRequired);
            var flxOptionsDetailsRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxOptionsDetailsRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxOptionsDetailsRow2.setDefaultUnit(kony.flex.DP);
            var lblTnC = new kony.ui.Label({
                "id": "lblTnC",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TermsAndConditions\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAgreement = new kony.ui.Label({
                "id": "lblAgreement",
                "isVisible": true,
                "left": "250dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Agreement\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var SwitchTnC = new kony.ui.Switch({
                "height": "25dp",
                "id": "SwitchTnC",
                "isVisible": true,
                "left": "115dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "5dp",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchAgreement = new kony.ui.Switch({
                "height": "25dp",
                "id": "switchAgreement",
                "isVisible": true,
                "left": "320dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "5dp",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptionsDetailsRow2.add(lblTnC, lblAgreement, SwitchTnC, switchAgreement);
            flxOperationsDetails.add(flxOptionsDetailsRow1, flxOptionsDetailsRow2);
            flxEntitlementDetailsMain.add(flxGeneralInformation, flxGeneralInformationDetails, flxSettings, flxSettingsDetails, flxSmsAlert, flxAlertDetails, flxBeneficiarySMSAlert, flxBeneficiarySMSAlertDetails, flxOperations, flxOperationsDetails);
            var flxSeparator10 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator10",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator10.setDefaultUnit(kony.flex.DP);
            flxSeparator10.add();
            var flxEntitlementsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxEntitlementsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxEntitlementsButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsEntitlements = new com.adminConsole.common.commonButtons({
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsEntitlements",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "commonButtons": {
                        "bottom": "0dp",
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "viz.val_cleared",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEntitlementsButtons.add(commonButtonsEntitlements);
            flxRightEntitlementDetails.add(flxEntitlementDetailsMain, flxSeparator10, flxEntitlementsButtons);
            var flxRightLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxRightLimits",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightLimits.setDefaultUnit(kony.flex.DP);
            var Limits = new com.adminConsole.customerMang.Llimits.Limits({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "Limits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "imgDelete1": {
                        "src": "delete_2x.png"
                    },
                    "imgDelete2": {
                        "src": "delete_2x.png"
                    },
                    "imgDelete3": {
                        "src": "delete_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSeparator14 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 81,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator14",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator14.setDefaultUnit(kony.flex.DP);
            flxSeparator14.add();
            var flxLimitsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxLimitsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxLimitsButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsLimits = new com.adminConsole.common.commonButtons({
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsLimits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "commonButtons": {
                        "bottom": "0dp",
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "viz.val_cleared",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLimitsButtons.add(commonButtonsLimits);
            flxRightLimits.add(Limits, flxSeparator14, flxLimitsButtons);
            var flxRightTransactionFee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxRightTransactionFee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightTransactionFee.setDefaultUnit(kony.flex.DP);
            var deleteRow = new com.adminConsole.customerMang.deleteRow.deleteRow({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "deleteRow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSeparator15 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 81,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator15",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator15.setDefaultUnit(kony.flex.DP);
            flxSeparator15.add();
            var flxTransactionFeeButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxTransactionFeeButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxTransactionFeeButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsTransactionFee = new com.adminConsole.common.commonButtons({
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsTransactionFee",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "commonButtons": {
                        "bottom": "0dp",
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "viz.val_cleared",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTransactionFeeButtons.add(commonButtonsTransactionFee);
            flxRightTransactionFee.add(deleteRow, flxSeparator15, flxTransactionFeeButtons);
            flxEntitlementDetails.add(flxVerticalTabsEntitlement, flxSeparator2, flxRightEntitlementDetails, flxRightLimits, flxRightTransactionFee);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            flxRightPannel.add(flxMainHeader, flxBreadCrumbs, flxToastMessage, flxNoGroups, flxAddGroups, flxGroupList, flxGroupview, flxEntitlementDetails, flxHeaderDropdown);
            var flxSelectSchedulerPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSelectSchedulerPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxSelectSchedulerPopup.setDefaultUnit(kony.flex.DP);
            var SelectScheduler = new com.adminConsole.Groups.SelectScheduler({
                "clipBounds": true,
                "id": "SelectScheduler",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "imgPopUpClose": {
                        "src": "close_blue.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSelectSchedulerPopup.add(SelectScheduler);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxDeactivateGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeactivateGroup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 7
            }, {}, {});
            flxDeactivateGroup.setDefaultUnit(kony.flex.DP);
            var popUpDeactivate = new com.adminConsole.common.popUp1({
                "clipBounds": true,
                "height": "100%",
                "id": "popUpDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "180px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Deactivate Group"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30dp",
                        "right": "viz.val_cleared",
                        "text": "Are you sure to deactivate the Group Auto Loan.\nThe Group has been assigned to few customers. Deactivating this may result in losing certain entitlements.",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeactivateGroup.add(popUpDeactivate);
            var flxAdvanceSearchPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdvanceSearchPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxAdvanceSearchPopUp.setDefaultUnit(kony.flex.DP);
            var flxAdvansedSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "95%",
                "id": "flxAdvansedSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "CopyslFbox0c37f1533930c4d",
                "top": "20px",
                "width": "70%",
                "zIndex": 100
            }, {}, {});
            flxAdvansedSearch.setDefaultUnit(kony.flex.DP);
            var flxSearchResultPage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchResultPage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ddacfb6d79684f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxSearchResultPage.setDefaultUnit(kony.flex.DP);
            var flxSearchHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25%",
                "id": "flxSearchHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchHeading.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abeb",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "25%",
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox0d9c3974835234d",
                "top": "10px",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "70%",
                "clipBounds": true,
                "height": "48px",
                "id": "flxArrow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxArrow.setDefaultUnit(kony.flex.DP);
            var lblPopUpMainMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLatoBold485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblPopUpMainMessage\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 200
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgArrow = new kony.ui.Image2({
                "centerY": "50%",
                "height": "12dp",
                "id": "imgArrow",
                "isVisible": false,
                "left": "10%",
                "skin": "slImage",
                "src": "img_down_arrow.png",
                "width": "12dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconArrow = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconArrow",
                "isVisible": true,
                "left": "10%",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
                "top": "13dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 200
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnModifySearch = new kony.ui.Button({
                "centerY": "50%",
                "id": "btnModifySearch",
                "isVisible": true,
                "left": "10%",
                "onClick": controller.AS_Button_e161b42beeaf43beb1f12a5d0c519d0f,
                "right": "0px",
                "skin": "sknBtnLato11ABEB14Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnModifySearch\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular1682b213pxHover"
            });
            flxArrow.add(lblPopUpMainMessage, imgArrow, lblIconArrow, btnModifySearch);
            var flxRightImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "40%",
                "clipBounds": true,
                "height": "35px",
                "id": "flxRightImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRightImage.setDefaultUnit(kony.flex.DP);
            var fontIconRight = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconRight",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknFontIconSearchCross20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRightImage.add(fontIconRight);
            flxPopupHeader.add(flxArrow, flxRightImage);
            var flxSearchFilter = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "centerX": "50.00%",
                "clipBounds": true,
                "enableScrolling": true,
                "height": "40%",
                "horizontalScrollIndicator": false,
                "id": "flxSearchFilter",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "pagingEnabled": false,
                "right": "20px",
                "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
                "skin": "slFSbox",
                "top": "28.00%",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxSearchFilter.setDefaultUnit(kony.flex.DP);
            var flxFilter1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxFilter1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ica8ce013fdc40",
                "top": "10dp",
                "width": "160px",
                "zIndex": 1
            }, {}, {});
            flxFilter1.setDefaultUnit(kony.flex.DP);
            var lblCsrAssist = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblCsrAssist",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopyslLabel0d2920a8c52fc41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblCsrAssist\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCross = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCross",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxCross.setDefaultUnit(kony.flex.DP);
            var lblIconCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconCross",
                "isVisible": true,
                "right": "7dp",
                "skin": "sknFontIconCross14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCross.add(lblIconCross);
            flxFilter1.add(lblCsrAssist, flxCross);
            var flxFilter2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxFilter2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ica8ce013fdc40",
                "top": "10dp",
                "width": "160px",
                "zIndex": 1
            }, {}, {});
            flxFilter2.setDefaultUnit(kony.flex.DP);
            var lblAdmin = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdmin",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopyslLabel0d2920a8c52fc41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblAdmin\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconCross1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconCross1",
                "isVisible": true,
                "right": "5dp",
                "skin": "sknFontIconCross14px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFilter2.add(lblAdmin, lblIconCross1);
            var flxFilter3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxFilter3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ica8ce013fdc40",
                "top": "10dp",
                "width": "160px",
                "zIndex": 1
            }, {}, {});
            flxFilter3.setDefaultUnit(kony.flex.DP);
            var lblSuperAdmin = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSuperAdmin",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopyslLabel0d2920a8c52fc41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblAdmin\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconCross2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconCross2",
                "isVisible": true,
                "right": "5dp",
                "skin": "sknFontIconCross14px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFilter3.add(lblSuperAdmin, lblIconCross2);
            flxSearchFilter.add(flxFilter1, flxFilter2, flxFilter3);
            flxSearchHeading.add(flxPopUpTopColor, flxPopupHeader, flxSearchFilter);
            var flxCountAddButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "6.50%",
                "id": "flxCountAddButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "18%",
                "zIndex": 1
            }, {}, {});
            flxCountAddButtons.setDefaultUnit(kony.flex.DP);
            var flxResultHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxResultHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxResultHeader.setDefaultUnit(kony.flex.DP);
            var lblCustSearchCount = new kony.ui.Label({
                "centerY": "50%",
                "height": "25px",
                "id": "lblCustSearchCount",
                "isVisible": true,
                "left": "11dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblCustSearchCount\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelected = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerY": "50%",
                "clipBounds": true,
                "height": "35px",
                "id": "flxSelected",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "CopyslFbox",
                "width": "400px",
                "zIndex": 1
            }, {}, {});
            flxSelected.setDefaultUnit(kony.flex.DP);
            var lblUnselect = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUnselect",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyslLabel0d6e029a5d8e647",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblUnselect\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnUnselect = new kony.ui.Button({
                "centerY": "50%",
                "id": "btnUnselect",
                "isVisible": true,
                "left": "0px",
                "onClick": controller.AS_Button_b40aa0ec60c8440d9739e9f8e7a78d8c,
                "right": "0px",
                "skin": "sknBtnLatoRegular11abeb14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnUnselect\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular1682b213pxHover"
            });
            var lblUnselectBracket = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUnselectBracket",
                "isVisible": true,
                "left": "1dp",
                "skin": "CopyslLabel0d6e029a5d8e647",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblUnselectBracket\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddSelected = new kony.ui.Button({
                "bottom": "0px",
                "centerY": "50%",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "btnAddSelected",
                "isVisible": true,
                "left": "20px",
                "right": "0px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnAddSelected\")",
                "width": "100px",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            var btnAddSelectedMore = new kony.ui.Button({
                "bottom": "0px",
                "centerY": "50%",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "btnAddSelectedMore",
                "isVisible": true,
                "left": "20px",
                "right": "0px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnAddSelectedMore\")",
                "width": "140px",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            flxSelected.add(lblUnselect, btnUnselect, lblUnselectBracket, btnAddSelected, btnAddSelectedMore);
            flxResultHeader.add(lblCustSearchCount, flxSelected);
            var flxSeprator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeprator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "CopyslFbox0g9535637f9db4c",
                "top": "95%",
                "zIndex": 1
            }, {}, {});
            flxSeprator1.setDefaultUnit(kony.flex.DP);
            flxSeprator1.add();
            flxCountAddButtons.add(flxResultHeader, flxSeprator1);
            var flxResultSegment = new kony.ui.FlexContainer({
                "clipBounds": false,
                "height": "75%",
                "id": "flxResultSegment",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "25%",
                "zIndex": 1
            }, {}, {});
            flxResultSegment.setDefaultUnit(kony.flex.DP);
            var flxGroupsAdvSearchHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxGroupsAdvSearchHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxffffffop100",
                "top": "0"
            }, {}, {
                "hoverSkin": "sknfbfcfc"
            });
            flxGroupsAdvSearchHeader.setDefaultUnit(kony.flex.DP);
            var flxGroupSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxGroupSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxffffffop100",
                "top": "0px"
            }, {}, {
                "hoverSkin": "sknfbfcfc"
            });
            flxGroupSegHeader.setDefaultUnit(kony.flex.DP);
            var flxCheckbox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxCheckbox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11px",
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "15px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCheckbox.setDefaultUnit(kony.flex.DP);
            var imgHeaderCheckBox = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "12px",
                "id": "imgHeaderCheckBox",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "12px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckbox.add(imgHeaderCheckBox);
            var flxHederName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHederName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17.20%",
                "zIndex": 1
            }, {}, {});
            flxHederName.setDefaultUnit(kony.flex.DP);
            var lblUsersHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsersHeaderName",
                "isVisible": true,
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSearchNameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchNameSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHederName.add(lblUsersHeaderName, fontIconSearchNameSort);
            var flxUsername = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUsername",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxUsername.setDefaultUnit(kony.flex.DP);
            var lblUsersHeaderUsername = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsersHeaderUsername",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.USERNAME\")",
                "top": 0,
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSearchUsernameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchUsernameSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUsername.add(lblUsersHeaderUsername, fontIconSearchUsernameSort);
            var flxCustID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustID",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "14%",
                "zIndex": 1
            }, {}, {});
            flxCustID.setDefaultUnit(kony.flex.DP);
            var lblCustID = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustID",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CUSTOMER_ID\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSearchCustIdSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchCustIdSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustID.add(lblCustID, fontIconSearchCustIdSort);
            var flxBranch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBranch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxBranch.setDefaultUnit(kony.flex.DP);
            var lblBranch = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBranch",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblBranch\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSearchBranchSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchBranchSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBranch.add(lblBranch, fontIconSearchBranchSort);
            var flxCity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCity",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "14%",
                "zIndex": 1
            }, {}, {});
            flxCity.setDefaultUnit(kony.flex.DP);
            var lblCity = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCity",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblCity\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSearchCitySort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchCitySort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCity.add(lblCity, fontIconSearchCitySort);
            var flxUsersHeaderStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUsersHeaderStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "9%",
                "zIndex": 1
            }, {}, {});
            flxUsersHeaderStatus.setDefaultUnit(kony.flex.DP);
            var lblUsersHeaderStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsersHeaderStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": "45px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSearchStatusSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchStatusSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUsersHeaderStatus.add(lblUsersHeaderStatus, fontIconSearchStatusSort);
            flxGroupSegHeader.add(flxCheckbox, flxHederName, flxUsername, flxCustID, flxBranch, flxCity, flxUsersHeaderStatus);
            var lblSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblSeperator",
                "isVisible": true,
                "left": 20,
                "right": "20px",
                "skin": "sknLblTableHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupsAdvSearchHeader.add(flxGroupSegHeader, lblSeperator);
            var flxResultSeg = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "75%",
                "horizontalScrollIndicator": true,
                "id": "flxResultSeg",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxResultSeg.setDefaultUnit(kony.flex.DP);
            var segCustomers = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "btnAdd": "",
                    "fontIconCustomerStatus": "",
                    "imgCheckBox": "",
                    "imgCheckBoxHidden": "",
                    "lblAdded": "",
                    "lblBranch": "",
                    "lblCity": "",
                    "lblCustID": "",
                    "lblName": "",
                    "lblSeperator": "",
                    "lblServicesStatus": "",
                    "lblUserName": ""
                }],
                "groupCells": false,
                "id": "segCustomers",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxGroupsAdvSearch",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
                "selectionBehaviorConfig": {
                    "imageIdentifier": "imgCheckBox",
                    "selectedStateImage": "checkboxselected.png",
                    "unselectedStateImage": "checkbox.png"
                },
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnAdd": "btnAdd",
                    "flxCheckbox": "flxCheckbox",
                    "flxCheckboxHidden": "flxCheckboxHidden",
                    "flxGroupSearch": "flxGroupSearch",
                    "flxGroupsAdvSearch": "flxGroupsAdvSearch",
                    "flxStatus": "flxStatus",
                    "fontIconCustomerStatus": "fontIconCustomerStatus",
                    "imgCheckBox": "imgCheckBox",
                    "imgCheckBoxHidden": "imgCheckBoxHidden",
                    "lblAdded": "lblAdded",
                    "lblBranch": "lblBranch",
                    "lblCity": "lblCity",
                    "lblCustID": "lblCustID",
                    "lblName": "lblName",
                    "lblSeperator": "lblSeperator",
                    "lblServicesStatus": "lblServicesStatus",
                    "lblUserName": "lblUserName"
                },
                "width": "100%",
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResultSeg.add(segCustomers);
            flxResultSegment.add(flxGroupsAdvSearchHeader, flxResultSeg);
            flxSearchResultPage.add(flxSearchHeading, flxCountAddButtons, flxResultSegment);
            var flxSearchNoResultPage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchNoResultPage",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ddacfb6d79684f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxSearchNoResultPage.setDefaultUnit(kony.flex.DP);
            var flxSearchHeadingNoResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10%",
                "id": "flxSearchHeadingNoResult",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchHeadingNoResult.setDefaultUnit(kony.flex.DP);
            var flxPopUpTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abeb",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTop.setDefaultUnit(kony.flex.DP);
            flxPopUpTop.add();
            var flxPopupHeaderNoResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "60%",
                "id": "flxPopupHeaderNoResult",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "20%",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeaderNoResult.setDefaultUnit(kony.flex.DP);
            var flxArrowNoResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "70%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxArrowNoResult",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxArrowNoResult.setDefaultUnit(kony.flex.DP);
            var lblPopUpMainMessageNoResult = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPopUpMainMessageNoResult",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLatoBold485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblPopUpMainMessage\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 200
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgArrowNoResult = new kony.ui.Image2({
                "centerY": "50%",
                "height": "12dp",
                "id": "imgArrowNoResult",
                "isVisible": false,
                "left": "10%",
                "skin": "slImage",
                "src": "img_down_arrow.png",
                "width": "12dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnModifySearchNoResult = new kony.ui.Button({
                "centerY": "50%",
                "id": "btnModifySearchNoResult",
                "isVisible": true,
                "left": "10%",
                "onClick": controller.AS_Button_j73c6a5a112d4eefb808714c05a92535,
                "right": "0px",
                "skin": "sknBtnLato11ABEB14Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnModifySearch\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular1682b213pxHover"
            });
            flxArrowNoResult.add(lblPopUpMainMessageNoResult, imgArrowNoResult, btnModifySearchNoResult);
            var flxRightImageNoResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxRightImageNoResult",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRightImageNoResult.setDefaultUnit(kony.flex.DP);
            var fontIconNoResultCross = new kony.ui.Label({
                "id": "fontIconNoResultCross",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknFontIconSearchCross20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRightImageNoResult.add(fontIconNoResultCross);
            flxPopupHeaderNoResult.add(flxArrowNoResult, flxRightImageNoResult);
            flxSearchHeadingNoResult.add(flxPopUpTop, flxPopupHeaderNoResult);
            var flxNoResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "85%",
                "id": "flxNoResult",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "0px",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxNoResult.setDefaultUnit(kony.flex.DP);
            var lblMainMessageNoResult = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "49%",
                "id": "lblMainMessageNoResult",
                "isVisible": true,
                "skin": "sknLblLatoBold485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblMainMessageNoResult\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnModifySearchNoResult1 = new kony.ui.Button({
                "centerX": "50%",
                "centerY": "52%",
                "id": "btnModifySearchNoResult1",
                "isVisible": true,
                "onClick": controller.AS_Button_f37a1f527eec4a838faa9efd7499c1ea,
                "skin": "sknBtnLato11ABEB14Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnModifySearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular1682b213pxHover"
            });
            flxNoResult.add(lblMainMessageNoResult, btnModifySearchNoResult1);
            flxSearchNoResultPage.add(flxSearchHeadingNoResult, flxNoResult);
            var flxSearchFilters = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchFilters",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "top": "0px",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxSearchFilters.setDefaultUnit(kony.flex.DP);
            var AdvancedSearch = new com.adminConsole.Groups.AdvancedSearch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "AdvancedSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "AdvancedSearch": {
                        "height": "100%",
                        "width": "100%"
                    },
                    "flxPopUp": {
                        "bottom": "5%"
                    },
                    "imgAfter": {
                        "src": "radionormal_2x.png"
                    },
                    "imgBefore": {
                        "src": "radionormal_2x.png"
                    },
                    "imgBetween": {
                        "src": "radionormal_2x.png"
                    },
                    "imgFlag1": {
                        "src": "radionormal_2x.png"
                    },
                    "imgFlag2": {
                        "src": "radionormal_2x.png"
                    },
                    "imgFlag3": {
                        "src": "radionormal_2x.png"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.search_for_customers\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxCalBefore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxCalBefore",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "450dp",
                "width": "14%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxCalBefore.setDefaultUnit(kony.flex.DP);
            var customCalGroupBefore = new kony.ui.CustomWidget({
                "id": "customCalGroupBefore",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "single",
                "value": ""
            });
            flxCalBefore.add(customCalGroupBefore);
            var flxCalAfter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxCalAfter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "17.50%",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "450dp",
                "width": "14%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxCalAfter.setDefaultUnit(kony.flex.DP);
            var customCalGroupAfter = new kony.ui.CustomWidget({
                "id": "customCalGroupAfter",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "single",
                "value": ""
            });
            flxCalAfter.add(customCalGroupAfter);
            var flxCalBetween = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxCalBetween",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35.50%",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "450dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxCalBetween.setDefaultUnit(kony.flex.DP);
            var customCalGroupBetween = new kony.ui.CustomWidget({
                "id": "customCalGroupBetween",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "list",
                "value": ""
            });
            flxCalBetween.add(customCalGroupBetween);
            flxSearchFilters.add(AdvancedSearch, flxCalBefore, flxCalAfter, flxCalBetween);
            var flxToastMessageSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "centerX": "50%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessageSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "60%",
                "zIndex": 110
            }, {}, {});
            flxToastMessageSearch.setDefaultUnit(kony.flex.DP);
            var toastMessageAdvSearch = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessageAdvSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxToastContainer": {
                        "width": "80%"
                    },
                    "lbltoastMessage": {
                        "text": "251 Customer added successfully."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessageSearch.add(toastMessageAdvSearch);
            flxAdvansedSearch.add(flxSearchResultPage, flxSearchNoResultPage, flxSearchFilters, flxToastMessageSearch);
            var flxLoading2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "95%",
                "id": "flxLoading2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknLoadingBlur",
                "top": "20px",
                "width": "70%",
                "zIndex": 150
            }, {}, {});
            flxLoading2.setDefaultUnit(kony.flex.DP);
            var flxImageCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageCont",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageCont.setDefaultUnit(kony.flex.DP);
            var imageLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imageLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageCont.add(imageLoading);
            flxLoading2.add(flxImageCont);
            flxAdvanceSearchPopUp.add(flxAdvansedSearch, flxLoading2);
            flxMain.add(flxLeftPannel, flxRightPannel, flxSelectSchedulerPopup, flxLoading, flxDeactivateGroup, flxAdvanceSearchPopUp);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxMain, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmGroups,
            "enabledForIdleTimeout": true,
            "id": "frmGroups",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_ecc42658ad144373969e9f10f48dc500(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_fee725e711d941f89d7ce01764bf2095,
            "retainScrollPosition": false
        }]
    }
});