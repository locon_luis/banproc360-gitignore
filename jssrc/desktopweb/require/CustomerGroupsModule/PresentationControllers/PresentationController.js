define(['ErrorInterceptor', 'ErrorIsNetworkDown', 'Promisify'], function(ErrorInterceptor, isNetworkDown, Promisify) {
    function CustomerGroups_PresentationController() {
        this.custGroupsModel = {
            context: null,
            custGroupsList: null,
            entitlements: null,
            customers: null,
            entitlementsOfGroup: null,
            customersOfGroup: null,
            searchResult: null,
            cities: null,
            branches: null,
            products: null,
            roleTypes: null,
            importProgressId: null
        };
        //kony.mvc.Presentation.BasePresenter.call(this);
    }
    inheritsFrom(CustomerGroups_PresentationController, kony.mvc.Presentation.BasePresenter);
    CustomerGroups_PresentationController.prototype.initializePresentationController = function() {
        var self = this;
        ErrorInterceptor.wrap(this, 'businessController').match(function(on) {
            return [
                on(isNetworkDown).do(function() {
                    self.presentUserInterface('frmGroups', {
                        NetworkDownMessage: {}
                    });
                })
            ];
        });
    };
    /**
     * @name showGroups
     * @member CustomerGroupsModule.presentationController
     * 
     */
    CustomerGroups_PresentationController.prototype.showGroups = function() {
        var self = this;
        self.clearToDefault();
        this.fetchCustomerGroups();
        this.fetchAllProducts();
        self.fetchAllBranches();
        self.fetchAllCities();
        self.fetchRoleTypes();
    };
    /**
     * @name fetchCustomerGroups
     * @member CustomerGroupsModule.presentationController
     * @param undefined context
     */
    CustomerGroups_PresentationController.prototype.fetchCustomerGroups = function(context) {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });
        var promiseFetchGroups = Promisify(this.businessController, 'getCustomerGroupsView');
        var promiseGetImportStatuId = Promisify(this.businessController, 'getImportStatusIdList');
        Promise.all([
            promiseFetchGroups({}),
            promiseGetImportStatuId({})
        ]).then(function(responses) {
            self.clearToDefault();
            if (context === "search") { //for adv.search
                self.custGroupsModel.context = "search";
                self.custGroupsModel.custGroupsList = responses[0];
                self.showCustomerGroupsScreen(self.custGroupsModel);
            } else { //for list view
                self.custGroupsModel.context = "";
                self.custGroupsModel.custGroupsList = responses[0];
                self.custGroupsModel.importProgressId = responses[1];
                self.showCustomerGroupsScreen(self.custGroupsModel);
            }
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }).catch(function(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
        });
    };
    /**
     * @name createCustomerGroups
     * @member CustomerGroupsModule.presentationController
     * @param {Name : string, Description : string, Status_id : string, User_id : string, Entitlements : [{Service_id : string, TransactionFee_id : string, TransactionLimit_id : string}], Customers : [{Customer_id : string}]} createData
     */
    CustomerGroups_PresentationController.prototype.createCustomerGroups = function(createData, importReq) {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onCompletionCallBack(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.Groups.successCreatingGroups"), "success");
            self.fetchCustomerGroups();
            if (importReq.isImport) {
                importReq.fileVal.roleId = response.Group_id;
                self.importCustomers(importReq);
            }
        }

        function onError(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(response), "error");
            self.fetchCustomerGroups();
        }
        self.businessController.addCustomerGroups(createData, onCompletionCallBack, onError);
    };
    /**
     * @name fetchEntitlements
     * @member CustomerGroupsModule.presentationController
     * @param boolean getForEdit
     * @param string groupId
     * @param number assignFor
     */
    CustomerGroups_PresentationController.prototype.fetchEntitlements = function(getForEdit, param, assignFor) {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onCompletionCallBack(response) {
            var res = response.Services;
            self.clearToDefault();
            if (getForEdit) { //for edit
            } else { //for create
                self.custGroupsModel.entitlements = res;
                self.custGroupsModel.context = "createGroup";
                self.showCustomerGroupsScreen(self.custGroupsModel);
            }
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }

        function onError(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
        }
        self.businessController.getServices({}, onCompletionCallBack, onError);
    };
    /**
     * @name fetchEntitlementsOfGroup
     * @member CustomerGroupsModule.presentationController
     * @param string groupId
     * @param boolean isEdit
     * @param number assignFor
     */
    CustomerGroups_PresentationController.prototype.fetchEntitlementsOfGroup = function(param, isEdit, assignFor) {
        var self = this;
        var reqParam = {
            "Group_id": param
        };
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onCompletionCallBack(response) {
            if (isEdit) { //for edit
            } else { //for group detailed view
                self.clearToDefault();
                self.custGroupsModel.entitlementsOfGroup = response;
                self.custGroupsModel.context = "";
                self.showCustomerGroupsScreen(self.custGroupsModel);
            }
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }

        function onError(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
        }
        self.businessController.getEntitlements(reqParam, onCompletionCallBack, onError);
    };
    /**
     * @name fetchCustomersOfGroup
     * @member CustomerGroupsModule.presentationController
     * @param json param
     * @param boolean isEdit
     * @param number/string assignFor
     */
    CustomerGroups_PresentationController.prototype.fetchCustomersOfGroup = function(param, isEdit, context) {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onCustomersCompletionCallBack(response) {
            self.clearToDefault();
            if (isEdit && context === "loadMore") { //for edit
                self.custGroupsModel.context = context;
                self.showCustomerGroupsScreen({
                    "context": context,
                    "custOfGroupLoadMore": response
                });
            } else { //for group detailed view
                self.custGroupsModel.context = "";
                self.custGroupsModel.customersOfGroup = response;
                self.showCustomerGroupsScreen(self.custGroupsModel);
            }
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }

        function onError(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
        }
        self.businessController.searchCustomers(param, onCustomersCompletionCallBack, onError);
    };
    /**
     * @name updateCustomerGroups
     * @member CustomerGroupsModule.presentationController
     * @param {Group_id : string, Status_id : string, Name : string, Description : string, User_id : string, removedEntitlementIds : [], addedEntitlementIds : [], removedCustomerIds : [], addedCustomerIds : []} editedParamReq
     */
    CustomerGroups_PresentationController.prototype.updateCustomerGroups = function(editedParamReq, importReq) {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onCompletionCallBack(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.Groups.successEditingGroups"), "success");
            self.fetchCustomerGroups();
            if (importReq.isImport) {
                self.importCustomers(importReq);
            }
        }

        function onError(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
            self.fetchCustomerGroups();
        }
        self.businessController.updateCustomerGroups(editedParamReq, onCompletionCallBack, onError);
    };
    /**
     * @name updateStatusOfCustomerGroups
     * @member CustomerGroupsModule.presentationController
     * @param {Group_id : string, Status_id : string, User_id : string} reqParam
     */
    CustomerGroups_PresentationController.prototype.updateStatusOfCustomerGroups = function(reqParam) {
        var self = this;
        var text = "";
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });
        if (reqParam.Status_id === "SID_ACTIVE") {
            text = kony.i18n.getLocalizedString("i18n.Groups.Activation");
        } else {
            text = kony.i18n.getLocalizedString("i18n.Groups.Deactivation");
        }

        function onCompletionCallBack(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.frmCSRController.CustomerRole") + " " + text + " " + kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Successful"), "success");
            self.fetchCustomerGroups();
        }

        function onError(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
            //self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.frmCSRController.CustomerRole") + " " + text + " " + kony.i18n.getLocalizedString("i18n.frmLogsController.failed"), "error");
            self.fetchCustomerGroups();
        }
        self.businessController.updateCustomerGroupStatus(reqParam, onCompletionCallBack, onError);
    };
    /**
     * @name searchCustomers
     * @member CustomerGroupsModule.presentationController
     * @param {_searchType : string, _name : string, _id : string, _username : string, _branchIDS : null, _productIDS : null, _cityIDS : null, _entitlementIDS : null, _groupIDS : null, _customerStatus : null, _before : null, _after : null, _pageOffset : string, _pageSize : number, _sortVariable : string, _sortDirection : string} param
     */
    CustomerGroups_PresentationController.prototype.searchCustomers = function(param) {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onCompletionCallBack(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.clearToDefault();
            self.custGroupsModel.context = "";
            self.custGroupsModel.searchResult = response;
            self.showCustomerGroupsScreen(self.custGroupsModel);
        }

        function onError(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
        }
        self.businessController.searchCustomers(param, onCompletionCallBack, onError);
    };
    /**
     * @name fetchAllCities
     * @member CustomerGroupsModule.presentationController
     * 
     */
    CustomerGroups_PresentationController.prototype.fetchAllCities = function() {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onCompletionCallBack(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.clearToDefault();
            self.custGroupsModel.context = "";
            self.custGroupsModel.cities = response;
            self.showCustomerGroupsScreen(self.custGroupsModel);
        }

        function onError(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            //TODO: show error
        }
        self.businessController.fetchCityList({}, onCompletionCallBack, onError);
    };
    /**
     * @name fetchAllBranches
     * @member CustomerGroupsModule.presentationController
     * 
     */
    CustomerGroups_PresentationController.prototype.fetchAllBranches = function() {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onCompletionCallBack(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.clearToDefault();
            self.custGroupsModel.context = "";
            self.custGroupsModel.branches = response.branch_view;
            self.showCustomerGroupsScreen(self.custGroupsModel);
        }

        function onError() {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            //TODO: show error
        }
        self.businessController.fetchAllBranches({}, onCompletionCallBack, onError);
    };
    /**
     * @name fetchAllProducts
     * @member CustomerGroupsModule.presentationController
     * 
     */
    CustomerGroups_PresentationController.prototype.fetchAllProducts = function() {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onSuccess(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.clearToDefault();
            self.custGroupsModel.context = "";
            self.custGroupsModel.products = response.product;
            self.showCustomerGroupsScreen(self.custGroupsModel);
        }

        function onError(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            //TODO: show error
        }
        self.businessController.getProductsList({}, onSuccess, onError);
    };
    /**
     * @name fetchEntitlementsForSearch
     * @member CustomerGroupsModule.presentationController
     * 
     */
    CustomerGroups_PresentationController.prototype.fetchEntitlementsForSearch = function() {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onSuccess(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.clearToDefault();
            self.custGroupsModel.context = "search";
            self.custGroupsModel.entitlements = response.Services;
            self.showCustomerGroupsScreen(self.custGroupsModel);
        }

        function onError(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
        }
        self.businessController.getServices({}, onSuccess, onError);
    };
    /*
     * fetch all customer role types
     */
    CustomerGroups_PresentationController.prototype.fetchRoleTypes = function() {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onSuccess(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.clearToDefault();
            self.custGroupsModel.roleTypes = response.CustomerTypeRecords;
            self.showCustomerGroupsScreen(self.custGroupsModel);
        }

        function onError(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        self.businessController.getRoleTypes({}, onSuccess, onError);
    };
    /*
     * common function to present user interface with Toast message
     */
    CustomerGroups_PresentationController.prototype.showToastMessageFlex = function(msg, status) {
        var self = this;
        var toast = {
            "toast": {
                message: msg,
                status: status
            }
        };
        self.presentUserInterface("frmGroups", toast);
        toast = {};
    };
    /**
     * @name showCustomerGroupsScreen
     * @member CustomerGroupsModule.presentationController
     * @param {context : string, custGroupsList : null, entitlements : null, customers : null, entitlementsOfGroup : null, customersOfGroup : null, searchResult : {statuscode : number, Status : string, SortVariable : string, SortDirection : string, PageSize : number, records : [object], opstatus : number, TotalResultsFound : number, PageOffset : number, httpStatusCode : number, httpresponse : {headers : object, url : object, responsecode : object}}, cities : null, branches : null, products : null} viewModel
     */
    CustomerGroups_PresentationController.prototype.showCustomerGroupsScreen = function(viewModel) {
        var self = this;
        if (viewModel) {
            self.presentUserInterface("frmGroups", viewModel);
        } else {
            self.presentUserInterface("frmGroups");
        }
    };
    /*
     * function to clear data to defaults
     */
    CustomerGroups_PresentationController.prototype.clearToDefault = function() {
        var self = this;
        self.custGroupsModel.context = null;
        self.custGroupsModel.custGroupsList = null;
        self.custGroupsModel.entitlements = null;
        self.custGroupsModel.customers = null;
        self.custGroupsModel.entitlementsOfGroup = null;
        self.custGroupsModel.customersOfGroup = null;
        self.custGroupsModel.searchResult = null;
        self.custGroupsModel.cities = null;
        self.custGroupsModel.branches = null;
        self.custGroupsModel.products = null;
        self.custGroupsModel.roleTypes = null;
        self.custGroupsModel.importProgressId = null;
    };
    /*
     * get all enttilements and entitlements,customers assigned to group for edit
     * @param: {entitlements:{"Group_id":""},customers:{}}
     * @context: "editGroup"/0/1
     */
    CustomerGroups_PresentationController.prototype.fetchGroupDataForEdit = function(param, context) {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });
        var promiseFetchAllEntitlements = Promisify(this.businessController, 'getServices');
        var promiseGetGroupEntl = Promisify(this.businessController, 'getEntitlements');
        var promiseGetGroupCustomers = Promisify(this.businessController, 'searchCustomers');
        Promise.all([
            promiseFetchAllEntitlements({}),
            promiseGetGroupEntl(param.entitlements),
            promiseGetGroupCustomers(param.customers)
        ]).then(function(responses) {
            self.showCustomerGroupsScreen({
                "context": context,
                "entitlements": responses[0].Services,
                "entitlementsOfGroup": responses[1],
                "customersOfGroup": responses[2],
            });
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }).catch(function(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
        });
    };
    /*
     * import customers csv file
     * 
     */
    CustomerGroups_PresentationController.prototype.importCustomers = function(context) {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onSuccess(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.clearToDefault();
            //self.custGroupsModel.roleTypes = response.CustomerTypeRecords;
            //self.showCustomerGroupsScreen(self.custGroupsModel);
        }

        function onError(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        self.businessController.importCustomersForGroup(context.fileVal, onSuccess, onError);
    };
    /*
     *fetch list of customers who's import is still in progress
     * 
     */
    CustomerGroups_PresentationController.prototype.getImportStatusIdList = function(context) {
        var self = this;
        self.presentUserInterface('frmGroups', {
            "LoadingScreen": {
                focus: true
            }
        });

        function onSuccess(response) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
            //self.clearToDefault();
            self.showCustomerGroupsScreen({
                "importCustStatusId": response
            });
        }

        function onError(error) {
            self.presentUserInterface('frmGroups', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }
        self.businessController.getImportStatusIdList(context, onSuccess, onError);
    };
    return CustomerGroups_PresentationController;
});