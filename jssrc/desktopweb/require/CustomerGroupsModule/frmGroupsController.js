define("CustomerGroupsModule/userfrmGroupsController", {
    removeCount: 0,
    recordsSize: 20,
    statusConfig: {
        active: "SID_ACTIVE",
        inactive: "SID_INACTIVE",
        custActive: "SID_CUS_ACTIVE",
        custLocked: "SID_CUS_LOCKED",
        custSuspend: "SID_CUS_SUSPENDED"
    },
    typeConfig: {
        smallBusiness: "TYPE_ID_SMALL_BUSINESS",
        microBusiness: "TYPE_ID_MICRO_BUSINESS",
        campaign: "TYPE_ID_CAMPAIGN"
    },
    statusFontIcon: {
        "ASCENDING_IMAGE": '\ue92a',
        "ASCENDING_SKIN": "sknIcon12pxBlack",
        "DESCENDING_IMAGE": '\ue920',
        "DESCENDING_SKIN": "sknIcon12pxBlack",
    },
    custToAdd: [],
    createRequestParam: null,
    editRequestParam: null,
    allEntitleData: null,
    assignEntitleData: null,
    orgEntitlementList: [],
    orgAssignedCust: [],
    entitlementDataSearch: [],
    filterTag: null,
    groupsData: null,
    segmentRecordsSize: 40,
    searchResult: [],
    gotEntitle: false,
    loadMoreModel: null,
    getMoreDataModel: null,
    selectedCustomerId: [],
    deletedCustomerId: [],
    limitForPaginationSearch: 20,
    newAssignedEntitle: [],
    customersSegmentColumns: {
        "backendNames": ["name", "Username", "id", "branch_name", "City_name", "customer_status"],
        "frontendIcons": ["fontIconSearchNameSort", "fontIconSearchUsernameSort", "fontIconSearchCustIdSort", "fontIconSearchBranchSort", "fontIconSearchCitySort", "fontIconSearchStatusSort"]
    },
    viewCustomersSegColumns: {
        "backendNames": ["name", "Username", "PrimaryEmail", "modifiedby", "lastmodifiedts"],
        "frontendIcons": ["fontIconViewCustNameSort", "fontIconViewCustUsrnameSort", "fontIconViewCustMailSort", "fontIconCustUpdatedBySort", "fontIconCustUpdatedOnSort", "fontIconSearchStatusSort"]
    },
    assignCustSegColumns: {
        "backendNames": ["name", "Username", "id", ],
        "frontendIcons": ["fontIconAsgCustSortName", "fontIconAsgCustSortUsername", "fontIconAsgCustIdSort"]
    },
    PreShowGroups: function() {
        this.setFlowActions();
        this.setPreshowData();
        this.showGroupUIChanges();
        this.setHeight();
    },
    willUpdateUI: function(groupsModel) {
        this.updateLeftMenu(groupsModel);
        if (groupsModel === undefined) {} else if (groupsModel.LoadingScreen) {
            if (groupsModel.LoadingScreen.focus) kony.adminConsole.utils.showProgressBar(this.view);
            else kony.adminConsole.utils.hideProgressBar(this.view);
        } else if (groupsModel.custGroupsList && groupsModel.context === "") {
            kony.adminConsole.utils.showProgressBar(this.view);
            if (groupsModel.custGroupsList.length > 0) {
                this.view.flxGroupList.setVisibility(true);
                this.sortBy = this.getObjectSorter("Group_Name");
                this.groupsData = groupsModel.custGroupsList;
                this.setListFiltersData();
                this.resetSortImages("grouplist");
                this.listOfProgressGroup = [];
                if (groupsModel.importProgressId.entityIds.length > 0) {
                    this.listOfProgressGroup = JSON.parse(groupsModel.importProgressId.entityIds);
                }
                var progressId = groupsModel.importProgressId;
                this.loadPageData = function() {
                    var groupsList = this.groupsData.filter(this.searchFilter).sort(this.sortBy.sortData);
                    var filteredGroupList = this.filterBasedOnTypeStatus(groupsList);
                    this.showGroupList(filteredGroupList);
                };
                this.loadPageData();
            } else {
                this.groupsData = [];
                this.view.flxNoGroups.setVisibility(true);
            }
            kony.adminConsole.utils.hideProgressBar(this.view);
        } else if (groupsModel.searchResult) {
            kony.adminConsole.utils.showProgressBar(this.view);
            this.setSearchResultSegment(groupsModel.searchResult);
        } else if (groupsModel.context === "createGroup") {
            this.allEntitleData = this.parseEntitlementForAddSeg(groupsModel.entitlements);
            this.assignEntitleData = [];
            this.orgEntitlementList = [];
        } else if (groupsModel.entitlementsOfGroup) {
            this.setDataToVariables = function() {
                this.orgEntitlementList = this.parseEntitlementsForSelectedSeg(groupsModel.entitlementsOfGroup);
                this.assignEntitleData = this.parseEntitlementsForSelectedSeg(groupsModel.entitlementsOfGroup);
                this.allEntitleData = this.parseEntitlementForAddSeg(groupsModel.entitlements);
                this.gotEntitle = true;
                if (groupsModel.customersOfGroup) { //assigning customers
                    this.orgAssignedCust = this.parseCustomersForAssignedSeg(groupsModel.customersOfGroup.records);
                }
            };
            if (groupsModel.context === "editGroup") { //FOR EDIT assigning entitlements
                this.setDataToVariables();
            } else if (groupsModel.context === 0) { //FOR ASSIGN ENT
                this.setDataToVariables();
                if (this.gotEntitle) this.formatEntitlementsForEdit(this.allEntitleData, this.assignEntitleData);
            } else if (groupsModel.context === 1) { //FOR ASSIGN CUST
                this.setDataToVariables();
                this.showAssignCustomers(true);
                this.setAssignedCustomersList(groupsModel.customersOfGroup);
            } else {
                this.sortBy = this.getObjectSorter("Name");
                this.resetSortImages("viewgroup");
                this.loadPageEntitlementData = function() {
                    this.showEntitlementsOfGroup(groupsModel.entitlementsOfGroup.sort(this.sortBy.sortData));
                };
                this.loadPageEntitlementData();
            }
            kony.adminConsole.utils.hideProgressBar(this.view);
        }
        //for assigned customers pagination
        else if (groupsModel.context === "loadMore" && groupsModel.custOfGroupLoadMore) {
            this.orgAssignedCust = this.parseCustomersForAssignedSeg(groupsModel.custOfGroupLoadMore.records);
            this.setAssignedCustomersList(groupsModel.custOfGroupLoadMore);
        } else if (groupsModel.customersOfGroup && groupsModel.context === "") {
            this.sortBy = this.getObjectSorter(kony.i18n.getLocalizedString("i18n.frmGroupsController.FullName"));
            this.resetSortImages("viewgroup");
            this.loadPageCustomerData = function() {
                this.showCustomersOfGroup(groupsModel.customersOfGroup);
            };
            this.loadPageCustomerData();
        } else if (groupsModel.toast) {
            if (groupsModel.toast.status === kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success")) {
                this.view.toastMessage.showToastMessage(groupsModel.toast.message, this);
            } else if (groupsModel.toast.status === kony.i18n.getLocalizedString("i18n.frmGroupsController.error")) {
                this.view.toastMessage.showErrorToastMessage(groupsModel.toast.message, this);
            } else {}
            kony.adminConsole.utils.hideProgressBar(this.view);
        }
        //SEARCH RELATEd
        else if (groupsModel.context === kony.i18n.getLocalizedString("i18n.frmLogsController.search") && groupsModel.entitlements) {
            this.setDataForEntitlementsListBox(groupsModel.entitlements);
        } else if (groupsModel.context === kony.i18n.getLocalizedString("i18n.frmLogsController.search") && groupsModel.custGroupsList) {
            this.setDataForGroupsListBox(groupsModel.custGroupsList);
        } else if (groupsModel.branches) {
            this.setDataForBranchesListBox(groupsModel.branches);
        } else if (groupsModel.products) {
            this.setDataForProductsListBox(groupsModel.products);
        } else if (groupsModel.cities) {
            this.setDataForCitiesListBox(groupsModel.cities);
        } else if (groupsModel.roleTypes) {
            this.setTypesDataToListBox(groupsModel.roleTypes);
        } else if (groupsModel.importCustStatusId) {
            this.checkCustomerImportProgress(groupsModel.importCustStatusId);
        }
    },
    setHeight: function() {
        var screenHeight = kony.os.deviceInfo().screenHeight;
        var scrollHeight;
        scrollHeight = screenHeight - 106;
        this.view.flxGroupListContainer.height = screenHeight - 255 + "px";
        this.view.flxGroupList.height = scrollHeight + "px";
        this.view.flxEntitlementDetails.height = scrollHeight - 30 + "px";
        this.view.SelectScheduler.top = "";
        this.view.flxSelectSchedulerPopup.height = screenHeight;
        this.view.flxGroupListContainer.height = scrollHeight - 130 + "px";
        this.view.flxViewSegment.height = kony.os.deviceInfo().screenHeight - 150 - 160 + "px";
    },
    setFlowActions: function() {
        var scopeObj = this;
        this.view.listingSegmentClient.flxContextualMenu.onHover = scopeObj.onDropdownHoverCallback;
        //Cities search
        this.view.AdvancedSearch.AdvancedSearchDropDown02.tbxSearchBox.onKeyUp = function() {
            var searchParameters = [{
                "searchKey": "lblDescription",
                "searchValue": scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.tbxSearchBox.text
            }];
            if (scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.tbxSearchBox.text === "") {
                scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.flxSearchCancel.setVisibility(false);
            } else {
                scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.flxSearchCancel.setVisibility(true);
            }
            scopeObj.search(scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData, searchParameters, scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.richTexNoResult);
        };
        this.view.AdvancedSearch.AdvancedSearchDropDown02.flxSearchCancel.onClick = function() {
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.flxSearchCancel.setVisibility(false);
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.tbxSearchBox.text = "";
            var searchParameters = [{
                "searchKey": "lblDescription",
                "searchValue": ""
            }];
            scopeObj.search(scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData, searchParameters, scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.richTexNoResult);
        };
        //Product search
        this.view.AdvancedSearch.AdvSearchDropDown11.tbxSearchBox.onKeyUp = function() {
            var searchParameters = [{
                "searchKey": "lblDescription",
                "searchValue": scopeObj.view.AdvancedSearch.AdvSearchDropDown11.tbxSearchBox.text
            }];
            if (scopeObj.view.AdvancedSearch.AdvSearchDropDown11.tbxSearchBox.text === "") {
                scopeObj.view.AdvancedSearch.AdvSearchDropDown11.flxSearchCancel.setVisibility(false);
            } else {
                scopeObj.view.AdvancedSearch.AdvSearchDropDown11.flxSearchCancel.setVisibility(true);
            }
            scopeObj.search(scopeObj.view.AdvancedSearch.AdvSearchDropDown11.sgmentData, searchParameters, scopeObj.view.AdvancedSearch.AdvSearchDropDown11.richTexNoResult);
        };
        this.view.AdvancedSearch.AdvSearchDropDown11.flxSearchCancel.onClick = function() {
            scopeObj.view.AdvancedSearch.AdvSearchDropDown11.flxSearchCancel.setVisibility(false);
            scopeObj.view.AdvancedSearch.AdvSearchDropDown11.tbxSearchBox.text = "";
            var searchParameters = [{
                "searchKey": "lblDescription",
                "searchValue": ""
            }];
            scopeObj.search(scopeObj.view.AdvancedSearch.AdvSearchDropDown11.sgmentData, searchParameters, scopeObj.view.AdvancedSearch.AdvSearchDropDown11.richTexNoResult);
        };
        //Branch search
        this.view.AdvancedSearch.AdvancedSearchDropDown01.tbxSearchBox.onKeyUp = function() {
            var searchParameters = [{
                "searchKey": "lblDescription",
                "searchValue": scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.tbxSearchBox.text
            }];
            if (scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.tbxSearchBox.text === "") {
                scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.flxSearchCancel.setVisibility(false);
            } else {
                scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.flxSearchCancel.setVisibility(true);
            }
            scopeObj.search(scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData, searchParameters, scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.richTexNoResult);
        };
        this.view.AdvancedSearch.AdvancedSearchDropDown01.flxSearchCancel.onClick = function() {
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.flxSearchCancel.setVisibility(false);
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.tbxSearchBox.text = "";
            var searchParameters = [{
                "searchKey": "lblDescription",
                "searchValue": ""
            }];
            scopeObj.search(scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData, searchParameters, scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.richTexNoResult);
        };
        //Entitlement search
        this.view.AdvancedSearch.AdvancedSearchDropDown03.tbxSearchBox.onKeyUp = function() {
            var searchParameters = [{
                "searchKey": "lblDescription",
                "searchValue": scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.tbxSearchBox.text
            }];
            if (scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.tbxSearchBox.text === "") {
                scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.flxSearchCancel.setVisibility(false);
            } else {
                scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.flxSearchCancel.setVisibility(true);
            }
            scopeObj.search(scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData, searchParameters, scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.richTexNoResult);
        };
        this.view.AdvancedSearch.AdvancedSearchDropDown03.flxSearchCancel.onClick = function() {
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.flxSearchCancel.setVisibility(false);
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.tbxSearchBox.text = "";
            var searchParameters = [{
                "searchKey": "lblDescription",
                "searchValue": ""
            }];
            scopeObj.search(scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData, searchParameters, scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.richTexNoResult);
        };
        //Group search
        this.view.AdvancedSearch.AdvancedSearchDropDown12.tbxSearchBox.onKeyUp = function() {
            var searchParameters = [{
                "searchKey": "lblDescription",
                "searchValue": scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.tbxSearchBox.text
            }];
            if (scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.tbxSearchBox.text === "") {
                scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.flxSearchCancel.setVisibility(false);
            } else {
                scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.flxSearchCancel.setVisibility(true);
            }
            scopeObj.search(scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData, searchParameters, scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.richTexNoResult);
        };
        this.view.AdvancedSearch.AdvancedSearchDropDown12.flxSearchCancel.onClick = function() {
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.flxSearchCancel.setVisibility(false);
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.tbxSearchBox.text = "";
            var searchParameters = [{
                "searchKey": "lblDescription",
                "searchValue": ""
            }];
            scopeObj.search(scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData, searchParameters, scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.richTexNoResult);
        };
        this.view.noStaticData.btnAddStaticContent.onClick = function() {
            scopeObj.showAddGroupDetails(1);
        };
        this.view.verticalTabs.btnOption1.onClick = function() {
            var index = scopeObj.view.listingSegmentClient.segListing.selectedIndex;
            if ((index === null) || typeof index === "undefined") { //create
                scopeObj.showAddGroupDetails(1);
                var editData = {
                    "Name": scopeObj.createRequestParam.Name,
                    "Description": scopeObj.createRequestParam.Description,
                    "Status": scopeObj.createRequestParam.Status_id,
                    "Type_id": scopeObj.createRequestParam.Type_id,
                    "isEAgreementActive": scopeObj.createRequestParam.isEAgreementActive,
                };
                scopeObj.fillDataForEdit(editData);
            } else {
                var editData = {
                    "Name": scopeObj.editRequestParam.Name,
                    "Description": scopeObj.editRequestParam.Description,
                    "Status": scopeObj.editRequestParam.Status_id,
                    "Type_id": scopeObj.editRequestParam.Type_id,
                    "isEAgreementActive": scopeObj.editRequestParam.isEAgreementActive,
                };
                var groupData = scopeObj.getGroupSetData();
                scopeObj.showAddGroupDetails(2, groupData.orginalName);
                scopeObj.fillDataForEdit(editData);
            }
        };
        this.view.verticalTabs.btnOption2.onClick = function() {
            var index = scopeObj.view.listingSegmentClient.segListing.selectedIndex;
            var isValid = scopeObj.checkGroupDetailsValidation();
            scopeObj.clearSearchBoxToDefaults();
            if (isValid) {
                kony.adminConsole.utils.showProgressBar(scopeObj.view);
                if ((index === null) || typeof index === "undefined") { //create
                    scopeObj.showAssignEntitlements(scopeObj.allEntitleData, scopeObj.assignEntitleData);
                } else { //edit
                    scopeObj.storeGroupDetailsEdit();
                    if (scopeObj.gotEntitle) scopeObj.formatEntitlementsForEdit(scopeObj.allEntitleData, scopeObj.assignEntitleData);
                }
            }
        };
        this.view.verticalTabs.btnOption3.onClick = function() {
            scopeObj.view.addAndRemoveOptionsCustomers.tbxSearchBox.text = "";
            var index = scopeObj.view.listingSegmentClient.segListing.selectedIndex;
            var getForEdit;
            scopeObj.clearSearchBoxToDefaults();
            var isValid = scopeObj.checkGroupDetailsValidation();
            if (isValid) {
                if ((index === null) || typeof index === "undefined") { //create
                    getForEdit = false;
                    scopeObj.stroreEntitlementForCreate();
                    scopeObj.showAssignCustomers(getForEdit);
                } else { //edit
                    var rowInd = index[1];
                    getForEdit = true;
                    var groupId = scopeObj.getSelectedGroupId();
                    scopeObj.storeGroupDetailsEdit();
                    scopeObj.updateFromEntitlements(groupId);
                    //set data only on first click of button for edit
                    if (scopeObj.view.verticalTabs.btnOption3.info.firstClick) {
                        var searchParam = scopeObj.getDefaultSearchParam();
                        scopeObj.presenter.fetchCustomersOfGroup(searchParam, true, "loadMore");
                    }
                    scopeObj.showAssignCustomers(true, groupId);
                }
                scopeObj.view.verticalTabs.btnOption3.info.firstClick = false;
            }
        };
        this.view.btnAddGroupCancel.onClick = function() {
            scopeObj.showGroupList();
            scopeObj.clearSearchBoxToDefaults();
        };
        this.view.btnAddGroupNext.onClick = function() {
            var index = scopeObj.view.listingSegmentClient.segListing.selectedIndex;
            var isValid = scopeObj.checkGroupDetailsValidation();
            var selectedType = scopeObj.view.lstBoxType.selectedKey;
            scopeObj.clearSearchBoxToDefaults();
            if (isValid && selectedType !== scopeObj.typeConfig.campaign) {
                kony.adminConsole.utils.showProgressBar(scopeObj.view);
                if ((index === null) || typeof index === "undefined") { //create
                    scopeObj.showAssignEntitlements(scopeObj.allEntitleData, scopeObj.assignEntitleData);
                } else { //edit
                    var rowInd = index[1];
                    var groupId = scopeObj.view.listingSegmentClient.segListing.data[rowInd].Group_id;
                    scopeObj.storeGroupDetailsEdit();
                    if (scopeObj.gotEntitle) scopeObj.formatEntitlementsForEdit(scopeObj.allEntitleData, scopeObj.assignEntitleData);
                }
            } else if (isValid && selectedType === scopeObj.typeConfig.campaign) {
                if (index) {
                    scopeObj.storeGroupDetailsEdit();
                }
                scopeObj.view.commonButtons.btnNext.onClick();
            }
        };
        this.view.btnAddGroupSave.onClick = function() {
            var index = scopeObj.view.listingSegmentClient.segListing.selectedIndex;
            var isValid = scopeObj.checkGroupDetailsValidation();
            if (isValid) {
                if ((index === null) || typeof index === "undefined") {
                    scopeObj.saveGroupDetails({
                        "isImport": false
                    });
                } else {
                    scopeObj.storeGroupDetailsEdit();
                    scopeObj.callUpdate(scopeObj.editRequestParam, {
                        "isImport": false
                    });
                }
                scopeObj.view.tbxGroupNameValue.skin = "skntbxLato35475f14px";
                //scopeObj.getAllGroups();
            } else {}
            //scopeObj.showToastMessage();
        };
        this.view.txtGroupDescription.onEndEditing = function() {
            if (scopeObj.view.lblGroupDescriptionCount.isVisible === true) {
                scopeObj.view.lblGroupDescriptionCount.setVisibility(false);
            }
        };
        this.view.txtGroupDescription.onKeyUp = function() {
            scopeObj.view.txtGroupDescription.skin = "skntxtAreaLato35475f14Px";
            scopeObj.view.flxNoGroupDescriptionError.setVisibility(false);
            scopeObj.editRequestParam.Description = scopeObj.view.txtGroupDescription.text;
            scopeObj.createRequestParam.Description = scopeObj.view.txtGroupDescription.text;
            if (scopeObj.view.txtGroupDescription.text.trim().length === 0) {
                scopeObj.view.lblGroupDescriptionCount.setVisibility(false);
            } else {
                scopeObj.view.lblGroupDescriptionCount.setVisibility(true);
                scopeObj.view.lblGroupDescriptionCount.text = scopeObj.view.txtGroupDescription.text.trim().length + "/250";
            }
            scopeObj.view.forceLayout();
        };
        this.view.tbxGroupNameValue.onKeyUp = function() {
            scopeObj.view.tbxGroupNameValue.skin = "skntbxLato35475f14px";
            scopeObj.view.flxNoGroupNameError.setVisibility(false);
            scopeObj.editRequestParam.Name = scopeObj.view.tbxGroupNameValue.text;
            scopeObj.createRequestParam.Name = scopeObj.view.tbxGroupNameValue.text;
            if (scopeObj.view.tbxGroupNameValue.text.trim().length === 0) {
                scopeObj.view.lblGroupNameCount.setVisibility(false);
            } else {
                scopeObj.view.lblGroupNameCount.setVisibility(true);
                scopeObj.view.lblGroupNameCount.text = scopeObj.view.tbxGroupNameValue.text.trim().length + "/50";
            }
            scopeObj.view.forceLayout();
        };
        this.view.tbxGroupNameValue.onEndEditing = function() {
            if (scopeObj.view.lblGroupNameCount.isVisible === true) {
                scopeObj.view.lblGroupNameCount.setVisibility(false);
            }
        };
        this.view.addAndRemoveOptions.btnSelectAll.onClick = function() {
            scopeObj.AddAllEntitlements();
            scopeObj.view.addAndRemoveOptions.btnSelectAll.setVisibility(false);
            scopeObj.view.addAndRemoveOptions.btnRemoveAll.setVisibility(true);
        };
        this.view.addAndRemoveOptions.btnRemoveAll.onClick = function() {
            scopeObj.RemoveAllEntitlements();
            scopeObj.view.addAndRemoveOptions.btnSelectAll.setVisibility(true);
            scopeObj.view.addAndRemoveOptions.btnRemoveAll.setVisibility(false);
        };
        this.view.commonButtons.btnCancel.onClick = function() {
            scopeObj.getAllGroups();
            scopeObj.clearSearchBoxToDefaults();
        };
        this.view.commonButtons.btnNext.onClick = function() {
            var index = scopeObj.view.listingSegmentClient.segListing.selectedIndex;
            var getForEdit;
            scopeObj.clearSearchBoxToDefaults();
            if ((index === null) || typeof index === "undefined") { //create
                getForEdit = false;
                scopeObj.stroreEntitlementForCreate();
                scopeObj.showAssignCustomers(getForEdit);
            } else { //edit
                var rowInd = index[1];
                getForEdit = true;
                var groupId = scopeObj.getSelectedGroupId();
                scopeObj.updateFromEntitlements(groupId);
                //set data only on first click of button for edit
                if (scopeObj.view.verticalTabs.btnOption3.info.firstClick) {
                    scopeObj.view.verticalTabs.btnOption3.info.firstClick = false;
                    var searchParam = scopeObj.getDefaultSearchParam();
                    scopeObj.presenter.fetchCustomersOfGroup(searchParam, true, "loadMore");
                }
                scopeObj.showAssignCustomers(getForEdit, groupId);
            }
        };
        this.view.commonButtons.btnSave.onClick = function() {
            var index = scopeObj.view.listingSegmentClient.segListing.selectedIndex;
            var i = scopeObj.view.listingSegmentClient.segListing.selectedRowIndex;
            scopeObj.clearSearchBoxToDefaults();
            var getForEdit;
            if ((index === null) || typeof index === "undefined") { //create
                scopeObj.clickSaveFromEnt({
                    "isImport": false
                });
                //scopeObj.getAllGroups();
            } else { //edit
                var rowInd = index[1];
                getForEdit = true;
                var groupId = scopeObj.view.listingSegmentClient.segListing.data[rowInd].Group_id;
                scopeObj.storeGroupDetailsEdit();
                scopeObj.updateFromEntitlements(groupId);
                scopeObj.callUpdate(scopeObj.editRequestParam, {
                    "isImport": false
                });
            }
        };
        this.view.commonButtonsAddCustomers.btnCancel.onClick = function() {
            scopeObj.getAllGroups();
            scopeObj.clearSearchBoxToDefaults();
        };
        this.view.commonButtonsAddedCustomers.btnCancel.onClick = function() {
            scopeObj.getAllGroups();
            scopeObj.clearSearchBoxToDefaults();
        };
        this.view.commonButtonsAddCustomers.btnSave.onClick = function(event) {
            var index = scopeObj.view.listingSegmentClient.segListing.selectedIndex;
            var importRequest = (event.id === "btnPopUpDelete") ? {
                "isImport": true,
                "fileVal": scopeObj.view.btnImportCust.info.value
            } : {
                "isImport": false
            };
            scopeObj.clearSearchBoxToDefaults();
            if ((index === null) || typeof index === "undefined") { //create
                scopeObj.createNewGroup(importRequest);
                //scopeObj.getAllGroups();
            } else { //edit
                var rowInd = index[1];
                var groupId = scopeObj.view.listingSegmentClient.segListing.data[rowInd].Group_id;
                scopeObj.updateFromCustomer(groupId);
                scopeObj.callUpdate(scopeObj.editRequestParam, importRequest);
            }
        };
        this.view.commonButtonsAddedCustomers.btnSave.onClick = function(event) {
            var index = scopeObj.view.listingSegmentClient.segListing.selectedIndex;
            var importRequest = (event.id === "btnPopUpDelete") ? {
                "isImport": true,
                "fileVal": scopeObj.view.btnImportCust.info.value
            } : {
                "isImport": false
            };
            scopeObj.clearSearchBoxToDefaults();
            if ((index === null) || typeof index === "undefined") { //create
                scopeObj.createNewGroup(importRequest);
                //scopeObj.getAllGroups();
            } else { //edit
                var rowInd = index[1];
                var groupId = scopeObj.view.listingSegmentClient.segListing.data[rowInd].Group_id;
                scopeObj.updateFromCustomer(groupId);
                scopeObj.callUpdate(scopeObj.editRequestParam, importRequest);
            }
        };
        this.view.mainHeader.btnAddNewOption.onClick = function() {
            scopeObj.preCreateInitialization();
            scopeObj.fetchListBoxDataForAdvSearch();
            scopeObj.createRequestParam = {
                "Name": "",
                "Description": "",
                "Status_id": scopeObj.statusConfig.active,
                "Type_id": "",
                "isEAgreementActive": 0,
            };
            scopeObj.clearSearchBoxToDefaults();
            scopeObj.assignEntitleData = [];
            scopeObj.newAssignedEntitle = [];
            scopeObj.presenter.fetchEntitlements(false);
            scopeObj.showAddGroupDetails(1);
            scopeObj.orgAssignedCust = [];
            scopeObj.custToAdd = [];
            scopeObj.deletedCustomerId = [];
            scopeObj.selectedCustomerId = [];
        };
        this.view.mainHeader.btnDropdownList.onClick = function() {
            if (scopeObj.view.listingSegmentClient.rtxNoResultsFound.isVisible === false) {
                scopeObj.downloadGroupsCSV();
            }
        };
        this.view.listingSegmentClient.contextualMenu.btnLink1.onClick = function() {
            var index = scopeObj.view.listingSegmentClient.segListing.selectedIndex;
            var rowInd = index[1];
            var getForEdit = true;
            var groupId = scopeObj.view.listingSegmentClient.segListing.data[rowInd].Group_id;
            var rowData = scopeObj.view.listingSegmentClient.segListing.data[rowInd];
            scopeObj.editRequestParam = {
                "Name": rowData.lblGroupName.text,
                "Description": rowData.lblHeaderGroupDescription.text,
                "Status_id": rowData.Status_id,
                "Type_id": rowData.Type_id,
                "isEAgreementActive": rowData.isEAgreementActive
            };
            var grData = {
                "Name": scopeObj.editRequestParam.Name,
                "Description": scopeObj.editRequestParam.Description,
                "Status": scopeObj.editRequestParam.Status_id,
                "Type_id": scopeObj.editRequestParam.Type_id,
                "isEAgreementActive": scopeObj.editRequestParam.isEAgreementActive
            };
            scopeObj.fillDataForEdit(grData);
            scopeObj.storeGroupDetailsEdit();
            scopeObj.custToAdd = [];
            scopeObj.deletedCustomerId = [];
            scopeObj.selectedCustomerId = [];
            scopeObj.view.segAssignedCustomers.selectedRowIndices = null;
            document.getElementById("frmGroups_flxAssignCustomerAdded").onscroll = null;
            var custParam = scopeObj.getDefaultSearchParam();
            custParam._groupIDS = groupId;
            var editParam = {
                entitlements: {
                    "Group_id": groupId
                },
                customers: custParam
            };
            scopeObj.getEntitlementsForEdit(editParam, 0);
        };
        this.view.listingSegmentClient.contextualMenu.btnLink2.onClick = function() {
            var index = scopeObj.view.listingSegmentClient.segListing.selectedIndex;
            var rowInd = index[1];
            var getForEdit = true;
            var groupId = scopeObj.view.listingSegmentClient.segListing.data[rowInd].Group_id;
            var rowData = scopeObj.view.listingSegmentClient.segListing.data[rowInd];
            scopeObj.view.verticalTabs.btnOption3.info.firstClick = false;
            scopeObj.editRequestParam = {
                "Name": rowData.lblGroupName.text,
                "Description": rowData.lblHeaderGroupDescription.text,
                "Status_id": rowData.Status_id,
                "Type_id": rowData.Type_id,
                "isEAgreementActive": rowData.isEAgreementActive
            };
            var grData = {
                "Name": scopeObj.editRequestParam.Name,
                "Description": scopeObj.editRequestParam.Description,
                "Status": scopeObj.editRequestParam.Status_id,
                "Type_id": scopeObj.editRequestParam.Type_id,
                "isEAgreementActive": scopeObj.editRequestParam.isEAgreementActive
            };
            scopeObj.fillDataForEdit(grData);
            scopeObj.storeGroupDetailsEdit();
            scopeObj.custToAdd = [];
            scopeObj.deletedCustomerId = [];
            scopeObj.selectedCustomerId = [];
            scopeObj.view.segAssignedCustomers.selectedRowIndices = null;
            document.getElementById("frmGroups_flxAssignCustomerAdded").onscroll = null;
            var custParam = scopeObj.getDefaultSearchParam();
            scopeObj.updateFromEntitlements(groupId);
            custParam._groupIDS = groupId;
            var editParam = {
                entitlements: {
                    "Group_id": groupId
                },
                customers: custParam
            };
            scopeObj.getEntitlementsForEdit(editParam, 1);
        };
        this.view.listingSegmentClient.contextualMenu.flxOption2.onClick = function() {
            scopeObj.editRequestParam = null;
            var groupData = scopeObj.getGroupSetData();
            // kony.adminConsole.utils.showProgressBar(scopeObj.view);
            var custParam = scopeObj.getDefaultSearchParam();
            custParam._groupIDS = groupData.groupId;
            var editParam = {
                entitlements: {
                    "Group_id": groupData.groupId
                },
                customers: custParam
            };
            scopeObj.getEntitlementsForEdit(editParam, "editGroup");
            scopeObj.view.tbxGroupNameValue.skin = "skntbxLato35475f14px";
            scopeObj.view.txtGroupDescription.skin = "skntxtAreaLato35475f14Px";
            scopeObj.view.flxNoGroupNameError.setVisibility(false);
            scopeObj.view.flxNoGroupDescriptionError.setVisibility(false);
            scopeObj.view.lblGroupDescriptionCount.setVisibility(false);
            scopeObj.view.lblGroupNameCount.setVisibility(false);
            scopeObj.view.verticalTabs.btnOption3.info.firstClick = true;
            scopeObj.custToAdd = [];
            scopeObj.deletedCustomerId = [];
            scopeObj.selectedCustomerId = [];
            scopeObj.showAddGroupDetails(2, groupData.orginalName);
            scopeObj.fillDataForEdit(groupData);
            scopeObj.clearSearchBoxToDefaults();
            scopeObj.view.segAssignedCustomers.selectedRowIndices = null;
            scopeObj.createRequestParam = {
                "Name": "",
                "Description": "",
                "Status_id": scopeObj.statusConfig.active,
                "Type_id": "",
                "isEAgreementActive": 0,
            };
        };
        this.view.listingSegmentClient.contextualMenu.flxOption4.onClick = function() {
            if (scopeObj.view.listingSegmentClient.contextualMenu.lblOption4.text === kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate")) {
                scopeObj.showGroupDeactive();
            } else {
                scopeObj.changeStatusOfGroup();
                scopeObj.ToggleActive();
            }
        };
        this.view.popUpDeactivate.flxPopUpClose.onClick = function() {
            scopeObj.view.flxDeactivateGroup.setVisibility(false);
        };
        this.view.popUpDeactivate.btnPopUpCancel.onClick = function() {
            scopeObj.view.flxDeactivateGroup.setVisibility(false);
        };
        this.view.popUpDeactivate.btnPopUpDelete.onClick = function() {
            scopeObj.changeStatusOfGroup();
            scopeObj.ToggleActive();
            var status = scopeObj.view.listingSegmentClient.contextualMenu.lblOption4.text;
        };
        this.view.listingSegmentClient.segListing.onRowClick = function() {
            scopeObj.showGroupViewDetails();
        };
        this.view.flxDropdownGeneralInformation.onClick = function() {
            scopeObj.hideShowDetails(scopeObj.view.imgDropdownGeneralInfo, scopeObj.view.flxGeneralInformationDetails);
        };
        this.view.flxDropdownSettings.onClick = function() {
            scopeObj.hideShowDetails(scopeObj.view.imgSettings, scopeObj.view.flxSettingsDetails);
        };
        this.view.flxDropdownSmsAlert.onClick = function() {
            scopeObj.hideShowDetails(scopeObj.view.imgSmsAlert, scopeObj.view.flxAlertDetails);
        };
        this.view.flxDropdownBeneficiarySMSAlert.onClick = function() {
            scopeObj.hideShowDetails(scopeObj.view.imgBeneficiarySMSAlert, scopeObj.view.flxBeneficiarySMSAlertDetails);
        };
        this.view.flxDropdownOperations.onClick = function() {
            scopeObj.hideShowDetails(scopeObj.view.imgOperations, scopeObj.view.flxOperationsDetails);
        };
        this.view.verticalTabsEntitlement.btnOption1.onClick = function() {
            scopeObj.showEntitlementDetails();
        };
        this.view.verticalTabsEntitlement.btnOption2.onClick = function() {
            scopeObj.showLimits();
        };
        this.view.verticalTabsEntitlement.btnOption3.onClick = function() {
            scopeObj.showTransactionFee();
        };
        this.view.commonButtonsEntitlements.btnCancel.onClick = function() {
            //scopeObj.showGroupViewEntitlements();
        };
        this.view.commonButtonsEntitlements.btnNext.onClick = function() {
            scopeObj.showLimits();
        };
        this.view.commonButtonsEntitlements.btnSave.onClick = function() {
            scopeObj.showGroupViewEntitlements();
        };
        this.view.commonButtonsLimits.btnCancel.onClick = function() {
            //scopeObj.showGroupViewEntitlements();
        };
        this.view.commonButtonsLimits.btnNext.onClick = function() {
            scopeObj.showTransactionFee();
        };
        this.view.commonButtonsLimits.btnSave.onClick = function() {
            //scopeObj.showGroupViewEntitlements();
        };
        this.view.commonButtonsTransactionFee.btnCancel.onClick = function() {
            //scopeObj.showGroupViewEntitlements();
        };
        this.view.commonButtonsTransactionFee.btnSave.onClick = function() {
            //scopeObj.showGroupViewEntitlements();
        };
        this.view.btnEdit.onClick = function() {
            scopeObj.editRequestParam = null;
            var groupData = scopeObj.getGroupSetData();
            kony.adminConsole.utils.showProgressBar(scopeObj.view);
            var custParam = scopeObj.getDefaultSearchParam();
            custParam._groupIDS = groupData.groupId;
            var editParam = {
                entitlements: {
                    "Group_id": groupData.groupId
                },
                customers: custParam
            };
            scopeObj.getEntitlementsForEdit(editParam, "editGroup");
            scopeObj.view.tbxGroupNameValue.skin = "skntbxLato35475f14px";
            scopeObj.view.txtGroupDescription.skin = "skntxtAreaLato35475f14Px";
            scopeObj.view.flxNoGroupNameError.setVisibility(false);
            scopeObj.view.flxNoGroupDescriptionError.setVisibility(false);
            scopeObj.view.lblGroupDescriptionCount.setVisibility(false);
            scopeObj.view.lblGroupNameCount.setVisibility(false);
            scopeObj.view.verticalTabs.btnOption3.info.firstClick = true;
            scopeObj.showAddGroupDetails(2, groupData.orginalName);
            scopeObj.fillDataForEdit(groupData);
            scopeObj.fetchListBoxDataForAdvSearch();
            scopeObj.view.segAssignedCustomers.selectedRowIndices = null;
            scopeObj.custToAdd = [];
            scopeObj.deletedCustomerId = [];
            scopeObj.selectedCustomerId = [];
            scopeObj.createRequestParam = {
                "Name": "",
                "Description": "",
                "Status_id": scopeObj.statusConfig.active,
                "Type_id": "",
                "isEAgreementActive": 0,
            };
        };
        this.view.flxCustomers.onClick = function() {
            scopeObj.tabUtilLabelFunction([scopeObj.view.lblEntitlements,
                scopeObj.view.lblCustomersFix
            ], scopeObj.view.lblCustomersFix);
            scopeObj.showViewCustomers();
        };
        this.view.flxEntitlements.onClick = function() {
            scopeObj.tabUtilLabelFunction([scopeObj.view.lblEntitlements,
                scopeObj.view.lblCustomersFix
            ], scopeObj.view.lblEntitlements);
            scopeObj.showGroupViewEntitlements();
        };
        this.view.breadcrumbs.btnBackToMain.onClick = function() {
            scopeObj.getAllGroups();
        };
        this.view.switchSMSAlert.onSlide = function() {
            scopeObj.toggleSwitch(scopeObj.view.switchSMSAlert, scopeObj.view.flxtbxDisablesms);
        };
        this.view.SwitchBeneficiarySMSAlert.onSlide = function() {
            scopeObj.toggleSwitch(scopeObj.view.SwitchBeneficiarySMSAlert, scopeObj.view.flxtbxDisable);
        };
        this.view.flxClose.onClick = function() {
            scopeObj.view.flxScheduler.setVisibility(false);
            scopeObj.view.btnSelectScheduler.setVisibility(true);
        };
        this.view.SelectScheduler.flxPopUpClose.onClick = function() {
            scopeObj.view.flxSelectSchedulerPopup.setVisibility(false);
        };
        this.view.SelectScheduler.btnPopUpCancel.onClick = function() {
            scopeObj.view.flxSelectSchedulerPopup.setVisibility(false);
        };
        this.view.SelectScheduler.btnPopUpAdd.onClick = function() {
            scopeObj.setScheduler();
        };
        this.view.btnSelectScheduler.onClick = function() {
            scopeObj.view.flxSelectSchedulerPopup.setVisibility(true);
            scopeObj.setMasterListData();
        };
        this.view.breadcrumbs.btnPreviousPage.onClick = function() {
            scopeObj.navigateBreadcrum();
        };
        this.view.subHeader.tbxSearchBox.onKeyUp = function() {
            if (scopeObj.view.subHeader.tbxSearchBox.text === "") {
                scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
            } else {
                scopeObj.view.subHeader.flxClearSearchImage.setVisibility(true);
                scopeObj.view.subHeader.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
            }
            scopeObj.loadPageData();
            if (scopeObj.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices === null || scopeObj.view.typeFilterMenu.segStatusFilterDropdown.selectedIndices === null) {
                scopeObj.view.listingSegmentClient.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
                scopeObj.view.flxGroupHeader.setVisibility(true);
                scopeObj.view.flxGroupSeparator.setVisibility(true);
                scopeObj.view.listingSegmentClient.segListing.setData([]);
                scopeObj.view.forceLayout();
            }
        };
        this.view.subHeader.flxClearSearchImage.onClick = function() {
            scopeObj.view.subHeader.tbxSearchBox.text = "";
            scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
            scopeObj.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100";
            scopeObj.loadPageData();
            if (scopeObj.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices === null && scopeObj.view.typeFilterMenu.segStatusFilterDropdown.selectedIndices === null) {
                scopeObj.view.listingSegmentClient.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
                scopeObj.view.flxGroupHeader.setVisibility(true);
                scopeObj.view.flxGroupSeparator.setVisibility(true);
                scopeObj.view.listingSegmentClient.segListing.setData([]);
                scopeObj.view.forceLayout();
            }
        };
        this.view.addAndRemoveOptions.tbxSearchBox.onTouchStart = function() {
            scopeObj.view.addAndRemoveOptions.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
        };
        this.view.addAndRemoveOptions.tbxSearchBox.onEndEditing = function() {
            scopeObj.view.addAndRemoveOptions.flxSearchContainer.skin = "sknflxd5d9ddop100";
        };
        this.view.addAndRemoveOptions.tbxSearchBox.onKeyUp = function() {
            var searchText = scopeObj.view.addAndRemoveOptions.tbxSearchBox.text;
            if (scopeObj.view.addAndRemoveOptions.tbxSearchBox.text === "") {
                scopeObj.view.addAndRemoveOptions.flxClearSearchImage.setVisibility(false);
            } else {
                scopeObj.view.addAndRemoveOptions.flxClearSearchImage.setVisibility(true);
                scopeObj.view.addAndRemoveOptions.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
            }
            scopeObj.doSearchInEntitlements(searchText);
        };
        this.view.addAndRemoveOptions.flxClearSearchImage.onClick = function() {
            scopeObj.view.addAndRemoveOptions.tbxSearchBox.text = "";
            scopeObj.view.addAndRemoveOptions.flxClearSearchImage.setVisibility(false);
            scopeObj.view.addAndRemoveOptions.flxSearchContainer.skin = "sknflxd5d9ddop100";
            scopeObj.doSearchInEntitlements(scopeObj.view.addAndRemoveOptions.tbxSearchBox.text);
        };
        this.view.btnAdvanceSearch.onClick = function() {
            scopeObj.showAdvanceSeearchOptions();
        };
        this.view.deleteRow.btnAdd.onClick = function() {
            scopeObj.addFeeRange();
        };
        this.view.flxGroupName.onClick = function() {
            var data = scopeObj.sortBy.column("Group_Name");
            scopeObj.loadPageData();
            scopeObj.resetSortImages("grouplist");
        };
        this.view.flxGroupCustomers.onClick = function() {
            var data = scopeObj.sortBy.column("Customers_Count");
            scopeObj.loadPageData();
            scopeObj.resetSortImages("grouplist");
        };
        this.view.flxGroupHeaderStatus.onClick = function() {
            var leftPos = scopeObj.view.fontIconGroupStatusFilter.left;
            if (scopeObj.view.flxStatusFilter.isVisible) {
                scopeObj.view.flxStatusFilter.setVisibility(false);
            } else {
                scopeObj.view.flxStatusFilter.onHover = scopeObj.onDropdownHoverCallback;
                scopeObj.view.flxStatusFilter.setVisibility(true);
            }
            if (scopeObj.view.listingSegmentClient.flxContextualMenu.isVisible) {
                scopeObj.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
            }
            scopeObj.view.flxTypeFilter.setVisibility(false);
            var flxLeft = scopeObj.view.flxGroupHeaderStatus.frame.x;
            scopeObj.view.flxStatusFilter.left = (flxLeft - 5) + "px";
        };
        this.view.flxGroupEntitlements.onClick = function() {
            var leftPos = scopeObj.view.fontIconSortEntitlements.left;
            if (scopeObj.view.flxTypeFilter.isVisible) {
                scopeObj.view.flxTypeFilter.setVisibility(false);
            } else {
                scopeObj.view.flxTypeFilter.onHover = scopeObj.onDropdownHoverCallback;
                scopeObj.view.flxTypeFilter.setVisibility(true);
            }
            if (scopeObj.view.listingSegmentClient.flxContextualMenu.isVisible) {
                scopeObj.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
            }
            scopeObj.view.flxStatusFilter.setVisibility(false);
            var flxLeft = scopeObj.view.flxGroupEntitlements.frame.x;
            scopeObj.view.flxTypeFilter.left = (flxLeft + 15) + "px";
        };
        this.view.statusFilterMenu.segStatusFilterDropdown.onRowClick = function() {
            var data = scopeObj.filterBasedOnTypeStatus();
            scopeObj.showGroupList(data.sort(scopeObj.sortBy.sortData));
            scopeObj.view.flxGroupHeader.setVisibility(true);
            scopeObj.view.flxGroupSeparator.setVisibility(true);
            scopeObj.view.subHeader.flxSearchContainer.setVisibility(data.length > 0);
            scopeObj.view.forceLayout();
        };
        this.view.typeFilterMenu.segStatusFilterDropdown.onRowClick = function() {
            scopeObj.view.statusFilterMenu.segStatusFilterDropdown.onRowClick();
        };
        this.view.AdvancedSearch.commonButtons.btnSave.onClick = function() {
            scopeObj.view.flxSearchResultPage.isVisible = true;
            scopeObj.view.flxSearchFilters.isVisible = false;
            scopeObj.view.imgHeaderCheckBox.src = "checkbox.png";
            scopeObj.view.segCustomers.selectedIndices = null;
            scopeObj.view.flxSelected.setVisibility(false);
            scopeObj.view.lblCustSearchCount.setVisibility(false);
            scopeObj.onClickSaveAdvSearch();
            scopeObj.hideDropDowns("advsearch");
        };
        this.view.flxCheckbox.onClick = function() {
            if (scopeObj.view.imgHeaderCheckBox.src === "checkboxselected.png") { //scopeObj.view.flxSelected.isVisible){
                scopeObj.view.flxSelected.isVisible = false;
                scopeObj.view.imgHeaderCheckBox.src = "checkbox.png";
                if (scopeObj.view.segCustomers.data !== undefined) {
                    scopeObj.view.lblCustSearchCount.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Displaying") + scopeObj.loadMoreModel.RecordsOnPage + " Of " + scopeObj.loadMoreModel.TotalResultsFound + " " + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers");
                }
                scopeObj.unSelectAllRows();
            } else {
                scopeObj.view.flxSelected.isVisible = true;
                scopeObj.view.imgHeaderCheckBox.src = "checkboxselected.png";
                var selInd = scopeObj.view.segCustomers.selectedIndices;
                if (selInd !== null) {
                    scopeObj.view.lblCustSearchCount.text = scopeObj.searchResult.length + " " + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers_Selected");
                }
                scopeObj.selectAllRows();
            }
            scopeObj.view.forceLayout();
        };
        this.view.segCustomers.onRowClick = function() {
            var selInd = scopeObj.view.segCustomers.selectedIndices;
            if (scopeObj.view.segCustomers.data[scopeObj.view.segCustomers.selectedRowIndex[1]].lblAdded.isVisible === false) {
                if (selInd !== null) {
                    if ((selInd[0][1]).length > 0) {
                        scopeObj.view.flxSelected.setVisibility(true);
                        scopeObj.view.lblCustSearchCount.text = selInd[0][1].length + " " + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers_Selected");
                    } else {
                        scopeObj.view.flxSelected.setVisibility(false);
                    }
                    var selIndCount = scopeObj.getSelectedIndicesCount();
                    if ((selInd[0][1]).length === selIndCount) {
                        scopeObj.view.imgHeaderCheckBox.src = "checkboxselected.png";
                    } else {
                        scopeObj.view.imgHeaderCheckBox.src = "checkbox.png";
                    }
                } else {
                    scopeObj.view.flxSelected.setVisibility(false);
                    scopeObj.view.lblCustSearchCount.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Displaying") + scopeObj.loadMoreModel.RecordsOnPage + " Of " + scopeObj.loadMoreModel.TotalResultsFound + " " + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers");
                    scopeObj.view.lblCustSearchCount.setVisibility(true);
                }
            }
            scopeObj.view.forceLayout();
        };
        this.view.btnAddSelected.onClick = function() {
            scopeObj.view.flxAdvanceSearchPopUp.isVisible = false;
            scopeObj.view.flxCustomerAssigned.isVisible = true;
            scopeObj.view.flxAssignCustomerAdded.setVisibility(true);
            scopeObj.view.flxNoCustomerAssign.isVisible = false;
            scopeObj.onClickOfBtnAddClose("addClose");
        };
        this.view.flxRightImage.onClick = function() {
            scopeObj.setAssignedCustomerSegDataMap();
            scopeObj.closeAdvanceSearchPopUp();
            scopeObj.view.segCustomers.setData([]);
        };
        this.view.AdvancedSearch.flxRightImage.onClick = function() {
            scopeObj.setAssignedCustomerSegDataMap();
            scopeObj.closeAdvanceSearchPopUp();
            scopeObj.view.segCustomers.setData([]);
        };
        this.view.flxRightImageNoResult.onClick = function() {
            scopeObj.closeAdvanceSearchPopUp();
        };
        this.view.btnModifySearch.onClick = function() {
            scopeObj.view.flxSearchResultPage.isVisible = false;
            scopeObj.view.flxSearchFilters.isVisible = true;
            scopeObj.view.segCustomers.setData([]);
            scopeObj.removeAllTags();
        };
        this.view.flxFilter3.onClick = function() {
            scopeObj.view.flxSearchNoResultPage.isVisible = true;
        };
        this.view.AdvancedSearch.flxBefore.onClick = function() {
            if (scopeObj.view.AdvancedSearch.imgBefore.src === "radio_notselected.png") {
                scopeObj.view.AdvancedSearch.imgBefore.src = "radio_selected.png";
                scopeObj.view.AdvancedSearch.imgAfter.src = "radio_notselected.png";
                scopeObj.view.AdvancedSearch.imgBetween.src = "radio_notselected.png";
            } else {
                scopeObj.view.AdvancedSearch.imgBefore.src = "radio_notselected.png";
            }
        };
        this.view.AdvancedSearch.flxAfter.onClick = function() {
            if (scopeObj.view.AdvancedSearch.imgAfter.src === "radio_notselected.png") {
                scopeObj.view.AdvancedSearch.imgAfter.src = "radio_selected.png";
                scopeObj.view.AdvancedSearch.imgBefore.src = "radio_notselected.png";
                scopeObj.view.AdvancedSearch.imgBetween.src = "radio_notselected.png";
            } else {
                scopeObj.view.AdvancedSearch.imgAfter.src = "radio_notselected.png";
            }
        };
        this.view.AdvancedSearch.flxBetween.onClick = function() {
            if (scopeObj.view.AdvancedSearch.imgBetween.src === "radio_notselected.png") {
                scopeObj.view.AdvancedSearch.imgBetween.src = "radio_selected.png";
                scopeObj.view.AdvancedSearch.imgAfter.src = "radio_notselected.png";
                scopeObj.view.AdvancedSearch.imgBefore.src = "radio_notselected.png";
            } else {
                scopeObj.view.AdvancedSearch.imgBetween.src = "radio_notselected.png";
            }
        };
        this.view.AdvancedSearch.flxFlag1.onClick = function() {
            if (scopeObj.view.AdvancedSearch.imgFlag1.src === "radio_notselected.png") {
                scopeObj.view.AdvancedSearch.imgFlag1.src = "radio_selected.png";
                scopeObj.view.AdvancedSearch.imgFlag2.src = "radio_notselected.png";
                scopeObj.view.AdvancedSearch.imgFlag3.src = "radio_notselected.png";
            } else {
                scopeObj.view.AdvancedSearch.imgFlag1.src = "radio_notselected.png";
            }
        };
        this.view.AdvancedSearch.flxFlag2.onClick = function() {
            if (scopeObj.view.AdvancedSearch.imgFlag2.src === "radio_notselected.png") {
                scopeObj.view.AdvancedSearch.imgFlag2.src = "radio_selected.png";
                scopeObj.view.AdvancedSearch.imgFlag1.src = "radio_notselected.png";
                scopeObj.view.AdvancedSearch.imgFlag3.src = "radio_notselected.png";
            } else {
                scopeObj.view.AdvancedSearch.imgFlag2.src = "radio_notselected.png";
            }
        };
        this.view.AdvancedSearch.flxFlag3.onClick = function() {
            if (scopeObj.view.AdvancedSearch.imgFlag3.src === "radio_notselected.png") {
                scopeObj.view.AdvancedSearch.imgFlag3.src = "radio_selected.png";
                scopeObj.view.AdvancedSearch.imgFlag2.src = "radio_notselected.png";
                scopeObj.view.AdvancedSearch.imgFlag1.src = "radio_notselected.png";
            } else {
                scopeObj.view.AdvancedSearch.imgFlag3.src = "radio_notselected.png";
            }
        };
        this.view.btnAddCustomer.onClick = function() {
            scopeObj.custToAdd = [];
            scopeObj.fetchListBoxDataForAdvSearch();
            scopeObj.view.flxAdvanceSearchPopUp.isVisible = true;
            scopeObj.view.flxSearchResultPage.isVisible = false;
            scopeObj.view.flxSearchNoResultPage.isVisible = false;
            scopeObj.view.flxSearchFilters.isVisible = true;
            scopeObj.clearAllFilterInAdvSearch();
            scopeObj.custToAdd = [];
            scopeObj.deletedCustomerId = [];
            scopeObj.selectedCustomerId = [];
        };
        this.view.btnModifySearchNoResult1.onClick = function() {
            scopeObj.view.flxSearchFilters.isVisible = true;
            scopeObj.view.flxSearchResultPage.isVisible = false;
            scopeObj.view.flxSearchNoResultPage.isVisible = false;
            scopeObj.view.segCustomers.setData([]);
            scopeObj.removeAllTags();
        };
        this.view.btnAddSelectedMore.onClick = function() {
            scopeObj.view.flxSelected.isVisible = false;
            scopeObj.showAdvSearchLoadingScreen();
            scopeObj.onClickOfBtnAddClose("addMore");
            scopeObj.view.lblCustSearchCount.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Displaying") + scopeObj.loadMoreModel.RecordsOnPage + " Of " + scopeObj.loadMoreModel.TotalResultsFound + " " + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers");
            scopeObj.view.lblCustSearchCount.setVisibility(true);
        };
        this.view.toastMessageAdvSearch.flxRightImage.onClick = function() {
            scopeObj.view.flxToastMessageSearch.isVisible = false;
        };
        this.view.btnAddMore.onClick = function() {
            //Removing advanced search listbox default behaviour
            scopeObj.clearAllFilterInAdvSearch();
            scopeObj.view.flxAdvanceSearchPopUp.isVisible = true;
            scopeObj.view.flxSearchResultPage.isVisible = false;
            scopeObj.view.flxSearchNoResultPage.isVisible = false;
            scopeObj.view.flxSearchFilters.isVisible = true;
            scopeObj.fetchListBoxDataForAdvSearch();
            scopeObj.removeAllTags();
            var type = scopeObj.view.lstBoxType.selectedKey;
            if (type === scopeObj.typeConfig.campaign) {
                scopeObj.view.btnImportCust.setVisibility(false);
            }
        };
        this.view.flxAssinedCheckbox.onClick = function() {
            var dataLen = 0;
            if (scopeObj.view.segAssignedCustomers.data !== null) {
                dataLen = scopeObj.view.segAssignedCustomers.data.length;
            }
            if (scopeObj.view.imgCustHeaderCheckBox.src === "checkbox.png") {
                scopeObj.view.flxAddCustomerButtons.setVisibility(false);
                scopeObj.view.flxRemoveCustButtons.setVisibility(true);
                scopeObj.view.imgCustHeaderCheckBox.src = "checkboxselected.png";
                scopeObj.view.lblCustAssignedHeader.text = dataLen + " " + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers_Selected");
                scopeObj.selectAllCustomer();
            } else {
                scopeObj.view.flxAddCustomerButtons.setVisibility(true);
                scopeObj.view.flxRemoveCustButtons.setVisibility(false);
                scopeObj.view.imgCustHeaderCheckBox.src = "checkbox.png";
                scopeObj.view.lblCustAssignedHeader.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Assigned_Customers") + dataLen + ")";
                scopeObj.unselectAllCustomer();
            }
        };
        this.view.segAssignedCustomers.onRowClick = function() {
            var indices = scopeObj.view.segAssignedCustomers.selectedRowIndices;
            var selLength = 0;
            var data = scopeObj.view.segAssignedCustomers.data;
            if (indices !== null && data !== null) {
                selLength = (indices[0][1]).length;
                if (selLength === data.length) {
                    scopeObj.view.imgCustHeaderCheckBox.src = "checkboxselected.png";
                } else {
                    scopeObj.view.imgCustHeaderCheckBox.src = "checkbox.png";
                }
                scopeObj.view.flxAddCustomerButtons.setVisibility(false);
                scopeObj.view.flxRemoveCustButtons.setVisibility(true);
                scopeObj.view.lblCustAssignedHeader.text = selLength + " " + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers_Selected");
            } else {
                scopeObj.view.imgCustHeaderCheckBox.src = "checkbox.png";
                scopeObj.view.flxAddCustomerButtons.setVisibility(true);
                scopeObj.view.flxRemoveCustButtons.setVisibility(false);
                scopeObj.updateAssignedCustomerCount();
            }
            scopeObj.unselectDeleteRowSelection();
        };
        this.view.btnUnselect.onClick = function() {
            scopeObj.view.flxSelected.isVisible = false;
            scopeObj.view.imgHeaderCheckBox.src = "checkbox.png";
            var seg = scopeObj.view.segCustomers.data;
            if (seg !== null) {
                scopeObj.view.lblCustSearchCount.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Displaying") + scopeObj.loadMoreModel.RecordsOnPage + " Of " + scopeObj.loadMoreModel.TotalResultsFound + " " + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers");
            } else {
                scopeObj.view.lblCustSearchCount.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Displaying_Customers");
            }
            scopeObj.view.lblCustSearchCount.setVisibility(true);
            scopeObj.unSelectAllRows();
        };
        this.view.AdvancedSearch.flxDropDown01.onClick = function() {
            scopeObj.view.AdvancedSearch.showHideSearchOption01();
            scopeObj.showCalendarFlex();
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.flxSearchCancel.onClick();
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.tbxSearchBox.text = "";
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.flxSearchCancel.setVisibility(false);
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.flxSearchContainer.skin = "sknflxffffffBorderd6dbe7Radius4px";
            scopeObj.view.forceLayout();
        };
        this.view.AdvancedSearch.flxDropDown11.onClick = function() {
            scopeObj.view.AdvancedSearch.showHideSearchOption11();
            scopeObj.showCalendarFlex();
            scopeObj.view.AdvancedSearch.AdvSearchDropDown11.flxSearchCancel.onClick();
            scopeObj.view.AdvancedSearch.AdvSearchDropDown11.tbxSearchBox.text = "";
            scopeObj.view.AdvancedSearch.AdvSearchDropDown11.flxSearchCancel.setVisibility(false);
            scopeObj.view.AdvancedSearch.AdvSearchDropDown11.flxSearchContainer.skin = "sknflxffffffBorderd6dbe7Radius4px";
            scopeObj.view.forceLayout();
        };
        this.view.AdvancedSearch.flxDropDown02.onClick = function() {
            scopeObj.view.AdvancedSearch.showHideSearchOption02();
            scopeObj.showCalendarFlex();
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.flxSearchCancel.onClick();
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.tbxSearchBox.text = "";
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.flxSearchCancel.setVisibility(false);
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.flxSearchContainer.skin = "sknflxffffffBorderd6dbe7Radius4px";
            scopeObj.view.forceLayout();
        };
        this.view.AdvancedSearch.flxDropDown03.onClick = function() {
            scopeObj.showCalendarFlex();
            if (!(scopeObj.view.AdvancedSearch.flxDropDownDetail03.isVisible)) {
                scopeObj.view.flxCalBefore.setVisibility(false);
                scopeObj.view.flxCalAfter.setVisibility(false);
            }
            scopeObj.view.AdvancedSearch.showHideSearchOption03();
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.flxSearchCancel.onClick();
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.tbxSearchBox.text = "";
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.flxSearchCancel.setVisibility(false);
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.flxSearchContainer.skin = "sknflxffffffBorderd6dbe7Radius4px";
            scopeObj.view.forceLayout();
        };
        this.view.AdvancedSearch.flxDropDown12.onClick = function() {
            scopeObj.showCalendarFlex();
            if (!(scopeObj.view.AdvancedSearch.flxDropDownDetail12.isVisible)) {
                scopeObj.view.flxCalBetween.setVisibility(false);
            }
            scopeObj.view.AdvancedSearch.showHideSearchOption12();
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.flxSearchCancel.onClick();
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.tbxSearchBox.text = "";
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.flxSearchCancel.setVisibility(false);
            scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.flxSearchContainer.skin = "sknflxffffffBorderd6dbe7Radius4px";
            scopeObj.view.forceLayout();
        };
        this.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.onRowClick = function() {
            var selInd = scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.selectedindices;
            var segment = scopeObj.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData;
            var selectedValue = segment.data[segment.currentIndex[1]];
            if (segment.info !== 'undefined' && typeof segment.info.selectedData !== 'undefined') {
                if (segment.info.selectedData.indexOf(selectedValue) >= 0) {
                    segment.info.selectedData = segment.info.selectedData.filter(function(e) {
                        return e !== selectedValue;
                    });
                } else {
                    segment.info.selectedData.push(selectedValue);
                }
            } else {
                segment.info.selectedData = [selectedValue];
            }
            if (segment.info.selectedData.length === 0) {
                scopeObj.view.AdvancedSearch.lblSelectedRows.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Branches");
                scopeObj.view.AdvancedSearch.lblSelectedRows.skin = "sknlbl0h2ff0d6b13f947AD";
                scopeObj.view.AdvancedSearch.lblSelectedRows.toolTip = scopeObj.view.AdvancedSearch.lblSelectedRows.text;
            } else {
                var count = segment.info.selectedData.length;
                if (count === 1) {
                    scopeObj.view.AdvancedSearch.lblSelectedRows.text = scopeObj.AdminConsoleCommonUtils.getTruncatedString(segment.info.selectedData[0].lblDescription.text, 30, 30);
                    scopeObj.view.AdvancedSearch.lblSelectedRows.skin = "sknlbl0h2ff0d6b13f947AD";
                    scopeObj.view.AdvancedSearch.lblSelectedRows.toolTip = segment.info.selectedData[0].lblDescription.text;
                } else if (count > 1) {
                    scopeObj.view.AdvancedSearch.lblSelectedRows.text = "" + count + kony.i18n.getLocalizedString("i18n.frmGroupsController.Branches_selected");
                    scopeObj.view.AdvancedSearch.lblSelectedRows.skin = "sknlbl0h2ff0d6b13f947AD";
                    scopeObj.view.AdvancedSearch.lblSelectedRows.toolTip = scopeObj.view.AdvancedSearch.lblSelectedRows.text;
                }
            }
            //Row selection
            var previousData = segment.data;
            previousData[segment.currentIndex[1]].imgCheckBox = (previousData[segment.currentIndex[1]].imgCheckBox === "checkboxselected.png" ? "checkbox.png" : "checkboxselected.png");
            segment.setDataAt(previousData[segment.currentIndex[1]], segment.currentIndex[1]);
            scopeObj.view.forceLayout();
        };
        this.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.onRowClick = function() {
            var selInd = scopeObj.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.selectedindices;
            var segment = scopeObj.view.AdvancedSearch.AdvSearchDropDown11.sgmentData;
            var selectedValue = segment.data[segment.currentIndex[1]];
            if (segment.info !== 'undefined' && typeof segment.info.selectedData !== 'undefined') {
                if (segment.info.selectedData.indexOf(selectedValue) >= 0) {
                    segment.info.selectedData = segment.info.selectedData.filter(function(e) {
                        return e !== selectedValue;
                    });
                } else {
                    segment.info.selectedData.push(selectedValue);
                }
            } else {
                segment.info.selectedData = [selectedValue];
            }
            if (segment.info.selectedData.length === 0) {
                scopeObj.view.AdvancedSearch.lblSelectedRows11.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Products");
                scopeObj.view.AdvancedSearch.lblSelectedRows11.skin = "sknlbl0h2ff0d6b13f947AD";
                scopeObj.view.AdvancedSearch.lblSelectedRows11.toolTip = scopeObj.view.AdvancedSearch.lblSelectedRows11.text;
            } else {
                var count = segment.info.selectedData.length;
                if (count === 1) {
                    scopeObj.view.AdvancedSearch.lblSelectedRows11.text = scopeObj.AdminConsoleCommonUtils.getTruncatedString(segment.info.selectedData[0].lblDescription.text, 32, 30);
                    scopeObj.view.AdvancedSearch.lblSelectedRows11.skin = "sknlbl0h2ff0d6b13f947AD";
                    scopeObj.view.AdvancedSearch.lblSelectedRows11.toolTip = segment.info.selectedData[0].lblDescription.text;
                } else if (count > 1) {
                    scopeObj.view.AdvancedSearch.lblSelectedRows11.text = "" + count + kony.i18n.getLocalizedString("i18n.frmGroupsController.Products_selected");
                    scopeObj.view.AdvancedSearch.lblSelectedRows11.skin = "sknlbl0h2ff0d6b13f947AD";
                    scopeObj.view.AdvancedSearch.lblSelectedRows11.toolTip = scopeObj.view.AdvancedSearch.lblSelectedRows11.text;
                }
            }
            //Row selection
            var previousData = segment.data;
            previousData[segment.currentIndex[1]].imgCheckBox = (previousData[segment.currentIndex[1]].imgCheckBox === "checkboxselected.png" ? "checkbox.png" : "checkboxselected.png");
            segment.setDataAt(previousData[segment.currentIndex[1]], segment.currentIndex[1]);
            scopeObj.view.forceLayout();
        };
        this.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.onRowClick = function() {
            var selInd = scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.selectedindices;
            var segment = scopeObj.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData;
            var selectedValue = segment.data[segment.currentIndex[1]];
            if (segment.info !== 'undefined' && typeof segment.info.selectedData !== 'undefined') {
                if (segment.info.selectedData.indexOf(selectedValue) >= 0) {
                    segment.info.selectedData = segment.info.selectedData.filter(function(e) {
                        return e !== selectedValue;
                    });
                } else {
                    segment.info.selectedData.push(selectedValue);
                }
            } else {
                segment.info.selectedData = [selectedValue];
            }
            if (segment.info.selectedData.length === 0) {
                scopeObj.view.AdvancedSearch.lblSelectedRows02.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Cities");
                scopeObj.view.AdvancedSearch.lblSelectedRows02.skin = "sknlbl0h2ff0d6b13f947AD";
                scopeObj.view.AdvancedSearch.lblSelectedRows02.toolTip = scopeObj.view.AdvancedSearch.lblSelectedRows02.text;
            } else {
                var count = segment.info.selectedData.length;
                if (count === 1) {
                    scopeObj.view.AdvancedSearch.lblSelectedRows02.text = scopeObj.AdminConsoleCommonUtils.getTruncatedString(segment.info.selectedData[0].lblDescription.text, 30, 30);
                    scopeObj.view.AdvancedSearch.lblSelectedRows02.skin = "sknlbl0h2ff0d6b13f947AD";
                    scopeObj.view.AdvancedSearch.lblSelectedRows02.toolTip = segment.info.selectedData[0].lblDescription.text;
                } else if (count > 1) {
                    scopeObj.view.AdvancedSearch.lblSelectedRows02.text = "" + count + kony.i18n.getLocalizedString("i18n.frmGroupsController.Cities_selected");
                    scopeObj.view.AdvancedSearch.lblSelectedRows02.skin = "sknlbl0h2ff0d6b13f947AD";
                    scopeObj.view.AdvancedSearch.lblSelectedRows02.toolTip = scopeObj.view.AdvancedSearch.lblSelectedRows02.text;
                }
            }
            //Row selection
            var previousData = segment.data;
            previousData[segment.currentIndex[1]].imgCheckBox = (previousData[segment.currentIndex[1]].imgCheckBox === "checkboxselected.png" ? "checkbox.png" : "checkboxselected.png");
            segment.setDataAt(previousData[segment.currentIndex[1]], segment.currentIndex[1]);
            scopeObj.view.forceLayout();
        };
        this.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.onRowClick = function() {
            var selInd = scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.selectedindices;
            var segment = scopeObj.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData;
            var selectedValue = segment.data[segment.currentIndex[1]];
            if (segment.info !== 'undefined' && typeof segment.info.selectedData !== 'undefined') {
                if (segment.info.selectedData.indexOf(selectedValue) >= 0) {
                    segment.info.selectedData = segment.info.selectedData.filter(function(e) {
                        return e !== selectedValue;
                    });
                } else {
                    segment.info.selectedData.push(selectedValue);
                }
            } else {
                segment.info.selectedData = [selectedValue];
            }
            if (segment.info.selectedData.length === 0) {
                scopeObj.view.AdvancedSearch.lblSelectedRows03.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Entitlements");
                scopeObj.view.AdvancedSearch.lblSelectedRows03.skin = "sknlbl0h2ff0d6b13f947AD";
                scopeObj.view.AdvancedSearch.lblSelectedRows03.toolTip = scopeObj.view.AdvancedSearch.lblSelectedRows03.text;
            } else {
                var count = segment.info.selectedData.length;
                if (count === 1) {
                    scopeObj.view.AdvancedSearch.lblSelectedRows03.text = scopeObj.AdminConsoleCommonUtils.getTruncatedString(segment.info.selectedData[0].lblDescription.text, 35, 32);
                    scopeObj.view.AdvancedSearch.lblSelectedRows03.skin = "sknlbl0h2ff0d6b13f947AD";
                    scopeObj.view.AdvancedSearch.lblSelectedRows03.toolTip = segment.info.selectedData[0].lblDescription.text;
                } else if (count > 1) {
                    scopeObj.view.AdvancedSearch.lblSelectedRows03.text = "" + count + kony.i18n.getLocalizedString("i18n.frmGroupsController.Entitlements_selected");
                    scopeObj.view.AdvancedSearch.lblSelectedRows03.skin = "sknlbl0h2ff0d6b13f947AD";
                    scopeObj.view.AdvancedSearch.lblSelectedRows03.toolTip = scopeObj.view.AdvancedSearch.lblSelectedRows03.text;
                }
            }
            //Row selection
            var previousData = segment.data;
            previousData[segment.currentIndex[1]].imgCheckBox = (previousData[segment.currentIndex[1]].imgCheckBox === "checkboxselected.png" ? "checkbox.png" : "checkboxselected.png");
            segment.setDataAt(previousData[segment.currentIndex[1]], segment.currentIndex[1]);
            scopeObj.view.forceLayout();
        };
        this.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.onRowClick = function() {
            var selInd = scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.selectedindices;
            var segment = scopeObj.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData;
            var selectedValue = segment.data[segment.currentIndex[1]];
            if (segment.info !== 'undefined' && typeof segment.info.selectedData !== 'undefined') {
                if (segment.info.selectedData.indexOf(selectedValue) >= 0) {
                    segment.info.selectedData = segment.info.selectedData.filter(function(e) {
                        return e !== selectedValue;
                    });
                } else {
                    segment.info.selectedData.push(selectedValue);
                }
            } else {
                segment.info.selectedData = [selectedValue];
            }
            if (segment.info.selectedData.length === 0) {
                scopeObj.view.AdvancedSearch.lblSelectedRows12.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Groups");
                scopeObj.view.AdvancedSearch.lblSelectedRows12.skin = "sknlbl0h2ff0d6b13f947AD";
                scopeObj.view.AdvancedSearch.lblSelectedRows12.toolTip = scopeObj.view.AdvancedSearch.lblSelectedRows12.text;
            } else {
                var count = segment.info.selectedData.length;
                if (count === 1) {
                    scopeObj.view.AdvancedSearch.lblSelectedRows12.text = scopeObj.AdminConsoleCommonUtils.getTruncatedString(segment.info.selectedData[0].lblDescription.text, 33, 30);
                    scopeObj.view.AdvancedSearch.lblSelectedRows12.skin = "sknlbl0h2ff0d6b13f947AD";
                    scopeObj.view.AdvancedSearch.lblSelectedRows12.toolTip = segment.info.selectedData[0].lblDescription.text;
                } else if (count > 1) {
                    scopeObj.view.AdvancedSearch.lblSelectedRows12.text = "" + count + kony.i18n.getLocalizedString("i18n.frmGroupsController.Groups_selected");
                    scopeObj.view.AdvancedSearch.lblSelectedRows12.skin = "sknlbl0h2ff0d6b13f947AD";
                    scopeObj.view.AdvancedSearch.lblSelectedRows12.toolTip = scopeObj.view.AdvancedSearch.lblSelectedRows12.text;
                }
            }
            //Row selection
            var previousData = segment.data;
            previousData[segment.currentIndex[1]].imgCheckBox = (previousData[segment.currentIndex[1]].imgCheckBox === "checkboxselected.png" ? "checkbox.png" : "checkboxselected.png");
            segment.setDataAt(previousData[segment.currentIndex[1]], segment.currentIndex[1]);
            scopeObj.view.forceLayout();
        };
        this.view.flxViewEntitlementsName.onClick = function() {
            var data = scopeObj.sortBy.column("Name");
            scopeObj.loadPageEntitlementData();
            scopeObj.resetSortImages("viewgroup");
        };
        this.view.flxViewCustomersFullName.onClick = function() {
            var searchParam = scopeObj.getDefaultSearchParam();
            searchParam._sortVariable = scopeObj.viewCustomersSegColumns.backendNames[0];
            scopeObj.resetSortImages("viewgroup", scopeObj.viewCustomersSegColumns.frontendIcons[0]);
            if (scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[0]].text === scopeObj.statusFontIcon.ASCENDING_IMAGE) {
                searchParam._sortDirection = "DESC";
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[0]].text = scopeObj.statusFontIcon.DESCENDING_IMAGE;
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[0]].skin = scopeObj.statusFontIcon.DESCENDING_SKIN;
            } else {
                searchParam._sortDirection = "ASC";
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[0]].text = scopeObj.statusFontIcon.ASCENDING_IMAGE;
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[0]].skin = scopeObj.statusFontIcon.ASCENDING_SKIN;
            }
            scopeObj.getCustomersOfGroup(searchParam, false);
        };
        this.view.flxViewCustomersUsername.onClick = function() {
            var searchParam = scopeObj.getDefaultSearchParam();
            searchParam._sortVariable = scopeObj.viewCustomersSegColumns.backendNames[1];
            scopeObj.resetSortImages("viewgroup", scopeObj.viewCustomersSegColumns.frontendIcons[1]);
            if (scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[1]].text === scopeObj.statusFontIcon.ASCENDING_IMAGE) {
                searchParam._sortDirection = "DESC";
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[1]].text = scopeObj.statusFontIcon.DESCENDING_IMAGE;
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[1]].skin = scopeObj.statusFontIcon.DESCENDING_SKIN;
            } else {
                searchParam._sortDirection = "ASC";
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[1]].text = scopeObj.statusFontIcon.ASCENDING_IMAGE;
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[1]].skin = scopeObj.statusFontIcon.ASCENDING_SKIN;
            }
            scopeObj.getCustomersOfGroup(searchParam, false);
        };
        this.view.flxViewCustomersEmailId.onClick = function() {
            var searchParam = scopeObj.getDefaultSearchParam();
            searchParam._sortVariable = scopeObj.viewCustomersSegColumns.backendNames[2];
            scopeObj.resetSortImages("viewgroup", scopeObj.viewCustomersSegColumns.frontendIcons[2]);
            if (scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[2]].text === scopeObj.statusFontIcon.ASCENDING_IMAGE) {
                searchParam._sortDirection = "DESC";
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[2]].text = scopeObj.statusFontIcon.DESCENDING_IMAGE;
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[2]].skin = scopeObj.statusFontIcon.DESCENDING_SKIN;
            } else {
                searchParam._sortDirection = "ASC";
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[2]].text = scopeObj.statusFontIcon.ASCENDING_IMAGE;
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[2]].skin = scopeObj.statusFontIcon.ASCENDING_SKIN;
            }
            scopeObj.getCustomersOfGroup(searchParam, false);
        };
        this.view.flxViewCustomersUpdatedBy.onClick = function() {
            var searchParam = scopeObj.getDefaultSearchParam();
            searchParam._sortVariable = scopeObj.viewCustomersSegColumns.backendNames[3];
            scopeObj.resetSortImages("viewgroup", scopeObj.viewCustomersSegColumns.frontendIcons[3]);
            if (scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[3]].text === scopeObj.statusFontIcon.ASCENDING_IMAGE) {
                searchParam._sortDirection = "DESC";
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[3]].text = scopeObj.statusFontIcon.DESCENDING_IMAGE;
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[3]].skin = scopeObj.statusFontIcon.DESCENDING_SKIN;
            } else {
                searchParam._sortDirection = "ASC";
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[3]].text = scopeObj.statusFontIcon.ASCENDING_IMAGE;
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[3]].skin = scopeObj.statusFontIcon.ASCENDING_SKIN;
            }
            scopeObj.getCustomersOfGroup(searchParam, false);
        };
        this.view.flxViewCustomersUpdatedOn.onClick = function() {
            var searchParam = scopeObj.getDefaultSearchParam();
            searchParam._sortVariable = scopeObj.viewCustomersSegColumns.backendNames[4];
            scopeObj.resetSortImages("viewgroup", scopeObj.viewCustomersSegColumns.frontendIcons[4]);
            if (scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[4]].text === scopeObj.statusFontIcon.ASCENDING_IMAGE) {
                searchParam._sortDirection = "DESC";
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[4]].text = scopeObj.statusFontIcon.DESCENDING_IMAGE;
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[4]].skin = scopeObj.statusFontIcon.DESCENDING_SKIN;
            } else {
                searchParam._sortDirection = "ASC";
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[4]].text = scopeObj.statusFontIcon.ASCENDING_IMAGE;
                scopeObj.view["" + scopeObj.viewCustomersSegColumns.frontendIcons[4]].skin = scopeObj.statusFontIcon.ASCENDING_SKIN;
            }
            scopeObj.getCustomersOfGroup(searchParam, false);
        };
        this.view.AdvancedSearch.commonButtons.btnCancel.onClick = function() {
            scopeObj.closeAdvanceSearchPopUp();
            scopeObj.view.segCustomers.setData([]);
        };
        this.view.AdvancedSearch.commonButtons.btnNext.onClick = function() {
            scopeObj.custToAdd = [];
            scopeObj.clearAllFilterInAdvSearch();
        };
        this.view.btnRemove.onClick = function() {
            scopeObj.btnRemoveOnClick();
        };
        this.view.tbxSearchBox.onTouchStart = function() {
            scopeObj.view.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
        };
        this.view.tbxSearchBox.onEndEditing = function() {
            scopeObj.view.flxSearchContainer.skin = "sknflxd5d9ddop100";
        }
        this.view.tbxSearchBox.onKeyUp = function() {
            scopeObj.view.flxClearSearchImage.setVisibility(true);
            if (scopeObj.view.tbxSearchBox.text === "") {
                scopeObj.view.flxClearSearchImage.setVisibility(false);
            } else {
                scopeObj.view.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
                scopeObj.view.flxClearSearchImage.setVisibility(true);
            }
            scopeObj.performSearchOnAssgCust();
        };
        this.view.flxClearSearchImage.onClick = function() {
            scopeObj.view.tbxSearchBox.text = "";
            scopeObj.view.flxSearchContainer.skin = "sknflxd5d9ddop100";
            scopeObj.view.flxClearSearchImage.setVisibility(false);
            scopeObj.performSearchOnAssgCust();
        };
        this.view.flxHederName.onClick = function() {
            scopeObj.showAdvSearchLoadingScreen();
            scopeObj.removeAllTags();
            scopeObj.loadMoreModel.SortVariable = scopeObj.customersSegmentColumns.backendNames[0];
            if (scopeObj.view["" + scopeObj.customersSegmentColumns.frontendIcons[0]].text === kony.adminConsole.utils.fonticons.ASCENDING_IMAGE) {
                scopeObj.loadMoreModel.SortDirection = "DESC";
            } else {
                scopeObj.loadMoreModel.SortDirection = "ASC";
            }
            searchParam = scopeObj.getAdvancedSearchParameters();
            searchParam["_pageOffset"] = "0";
            searchParam["_pageSize"] = scopeObj.loadMoreModel.RecordsOnPage;
            searchParam["_sortVariable"] = scopeObj.loadMoreModel.SortVariable;
            searchParam["_sortDirection"] = scopeObj.loadMoreModel.SortDirection;
            scopeObj.presenter.searchCustomers(searchParam);
        };
        this.view.flxUsername.onClick = function() {
            scopeObj.showAdvSearchLoadingScreen();
            scopeObj.removeAllTags();
            scopeObj.loadMoreModel.SortVariable = scopeObj.customersSegmentColumns.backendNames[1];
            if (scopeObj.view["" + scopeObj.customersSegmentColumns.frontendIcons[1]].text === kony.adminConsole.utils.fonticons.ASCENDING_IMAGE) {
                scopeObj.loadMoreModel.SortDirection = "DESC";
            } else {
                scopeObj.loadMoreModel.SortDirection = "ASC";
            }
            searchParam = scopeObj.getAdvancedSearchParameters();
            searchParam["_pageOffset"] = "0";
            searchParam["_pageSize"] = scopeObj.loadMoreModel.RecordsOnPage;
            searchParam["_sortVariable"] = scopeObj.loadMoreModel.SortVariable;
            searchParam["_sortDirection"] = scopeObj.loadMoreModel.SortDirection;
            scopeObj.presenter.searchCustomers(searchParam);
        };
        this.view.flxCustID.onClick = function() {
            scopeObj.showAdvSearchLoadingScreen();
            scopeObj.removeAllTags();
            scopeObj.loadMoreModel.SortVariable = scopeObj.customersSegmentColumns.backendNames[2];
            if (scopeObj.view["" + scopeObj.customersSegmentColumns.frontendIcons[2]].text === kony.adminConsole.utils.fonticons.ASCENDING_IMAGE) {
                scopeObj.loadMoreModel.SortDirection = "DESC";
            } else {
                scopeObj.loadMoreModel.SortDirection = "ASC";
            }
            searchParam = scopeObj.getAdvancedSearchParameters();
            searchParam["_pageOffset"] = "0";
            searchParam["_pageSize"] = scopeObj.loadMoreModel.RecordsOnPage;
            searchParam["_sortVariable"] = scopeObj.loadMoreModel.SortVariable;
            searchParam["_sortDirection"] = scopeObj.loadMoreModel.SortDirection;
            scopeObj.presenter.searchCustomers(searchParam);
        };
        this.view.flxBranch.onClick = function() {
            scopeObj.showAdvSearchLoadingScreen();
            scopeObj.removeAllTags();
            scopeObj.loadMoreModel.SortVariable = scopeObj.customersSegmentColumns.backendNames[3];
            if (scopeObj.view["" + scopeObj.customersSegmentColumns.frontendIcons[3]].text === kony.adminConsole.utils.fonticons.ASCENDING_IMAGE) {
                scopeObj.loadMoreModel.SortDirection = "DESC";
            } else {
                scopeObj.loadMoreModel.SortDirection = "ASC";
            }
            searchParam = scopeObj.getAdvancedSearchParameters();
            searchParam["_pageOffset"] = "0";
            searchParam["_pageSize"] = scopeObj.loadMoreModel.RecordsOnPage;
            searchParam["_sortVariable"] = scopeObj.loadMoreModel.SortVariable;
            searchParam["_sortDirection"] = scopeObj.loadMoreModel.SortDirection;
            scopeObj.presenter.searchCustomers(searchParam);
        };
        this.view.flxCity.onClick = function() {
            scopeObj.showAdvSearchLoadingScreen();
            scopeObj.removeAllTags();
            scopeObj.loadMoreModel.SortVariable = scopeObj.customersSegmentColumns.backendNames[4];
            if (scopeObj.view["" + scopeObj.customersSegmentColumns.frontendIcons[4]].text === kony.adminConsole.utils.fonticons.ASCENDING_IMAGE) {
                scopeObj.loadMoreModel.SortDirection = "DESC";
            } else {
                scopeObj.loadMoreModel.SortDirection = "ASC";
            }
            searchParam = scopeObj.getAdvancedSearchParameters();
            searchParam["_pageOffset"] = "0";
            searchParam["_pageSize"] = scopeObj.loadMoreModel.RecordsOnPage;
            searchParam["_sortVariable"] = scopeObj.loadMoreModel.SortVariable;
            searchParam["_sortDirection"] = scopeObj.loadMoreModel.SortDirection;
            scopeObj.presenter.searchCustomers(searchParam);
        };
        this.view.flxUsersHeaderStatus.onClick = function() {
            scopeObj.showAdvSearchLoadingScreen();
            scopeObj.removeAllTags();
            scopeObj.loadMoreModel.SortVariable = scopeObj.customersSegmentColumns.backendNames[5];
            if (scopeObj.view["" + scopeObj.customersSegmentColumns.frontendIcons[5]].text === kony.adminConsole.utils.fonticons.ASCENDING_IMAGE) {
                scopeObj.loadMoreModel.SortDirection = "DESC";
            } else {
                scopeObj.loadMoreModel.SortDirection = "ASC";
            }
            searchParam = scopeObj.getAdvancedSearchParameters();
            searchParam["_pageOffset"] = "0";
            searchParam["_pageSize"] = scopeObj.loadMoreModel.RecordsOnPage;
            searchParam["_sortVariable"] = scopeObj.loadMoreModel.SortVariable;
            searchParam["_sortDirection"] = scopeObj.loadMoreModel.SortDirection;
            scopeObj.presenter.searchCustomers(searchParam);
        };
        this.view.AdvancedSearch.flxPopUp.onClick = function() {
            scopeObj.hideDropDowns("advsearch");
        };
        this.view.AdvancedSearch.tbxSearchBox.onTouchStart = function() {
            scopeObj.view.AdvancedSearch.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
        };
        this.view.AdvancedSearch.tbxSearchBox.onEndEditing = function() {
            scopeObj.view.AdvancedSearch.flxSearchContainer.skin = "sknflxd5d9ddop100";
        };
        this.view.AdvancedSearch.tbxSearchBox.onKeyUp = function() {
            if (scopeObj.view.AdvancedSearch.tbxSearchBox.text === "") {
                scopeObj.view.AdvancedSearch.flxClearSearchImage.setVisibility(false);
            } else {
                scopeObj.view.AdvancedSearch.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
                scopeObj.view.AdvancedSearch.flxClearSearchImage.setVisibility(true);
            }
            scopeObj.view.forceLayout();
        };
        this.view.AdvancedSearch.flxClearSearchImage.onClick = function() {
            scopeObj.view.AdvancedSearch.tbxSearchBox.text = "";
            scopeObj.view.AdvancedSearch.flxSearchContainer.skin = "sknflxd5d9ddop100";
            scopeObj.view.AdvancedSearch.flxClearSearchImage.setVisibility(false);
            scopeObj.view.forceLayout();
        };
        this.view.lblIconArrow.onClick = function() {
            if (scopeObj.view.flxSearchFilter.isVisible) {
                scopeObj.view.flxSearchFilter.setVisibility(false);
                scopeObj.view.lblIconArrow.text = "\ue922"; //rightArrow
                scopeObj.view.lblIconArrow.skin = "sknfontIconDescRightArrow14px";
            } else {
                scopeObj.view.lblIconArrow.text = "\ue915"; //downArrow
                scopeObj.view.lblIconArrow.skin = "sknfontIconDescDownArrow12px";
                scopeObj.view.flxSearchFilter.setVisibility(true);
            }
            scopeObj.setAdvancedSearchFlexHeight();
        };
        this.view.flxCustomerHeaderStatus.onClick = function() {
            if (scopeObj.view.flxCustomerStatusFilter.isVisible) {
                scopeObj.view.flxCustomerStatusFilter.setVisibility(false);
            } else {
                scopeObj.view.flxCustomerStatusFilter.setVisibility(true);
            }
            var flxLeft = scopeObj.view.flxCustomerHeaderStatus.frame.x;
            scopeObj.view.flxCustomerStatusFilter.left = (flxLeft - 40) + "px";
        };
        this.view.custStatusFilter.segStatusFilterDropdown.onRowClick = function() {
            scopeObj.performStatusFilterInAssignCustomer();
        };
        this.view.flxCustomerStatusFilter.onHover = scopeObj.onDropdownHoverCallback;
        this.view.flxCustomerHederName.onClick = function() {
            var searchParam = scopeObj.getDefaultSearchParam();
            searchParam._sortVariable = scopeObj.assignCustSegColumns.backendNames[0];
            scopeObj.resetSortImages("assigncust", scopeObj.assignCustSegColumns.frontendIcons[0]);
            if (scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[0]].text === scopeObj.statusFontIcon.ASCENDING_IMAGE) {
                searchParam._sortDirection = "DESC";
                scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[0]].text = scopeObj.statusFontIcon.DESCENDING_IMAGE;
                scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[0]].skin = scopeObj.statusFontIcon.DESCENDING_SKIN;
            } else {
                searchParam._sortDirection = "ASC";
                scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[0]].text = scopeObj.statusFontIcon.ASCENDING_IMAGE;
                scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[0]].skin = scopeObj.statusFontIcon.ASCENDING_SKIN;
            }
            scopeObj.presenter.fetchCustomersOfGroup(searchParam, true, "loadMore");
        };
        this.view.flxCustomerUsername.onClick = function() {
            var searchParam = scopeObj.getDefaultSearchParam();
            searchParam._sortVariable = scopeObj.assignCustSegColumns.backendNames[1];
            scopeObj.resetSortImages("assigncust", scopeObj.assignCustSegColumns.frontendIcons[1]);
            if (scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[1]].text === scopeObj.statusFontIcon.ASCENDING_IMAGE) {
                searchParam._sortDirection = "DESC";
                scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[1]].text = scopeObj.statusFontIcon.DESCENDING_IMAGE;
                scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[1]].skin = scopeObj.statusFontIcon.DESCENDING_SKIN;
            } else {
                searchParam._sortDirection = "ASC";
                scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[1]].text = scopeObj.statusFontIcon.ASCENDING_IMAGE;
                scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[1]].skin = scopeObj.statusFontIcon.ASCENDING_SKIN;
            }
            scopeObj.presenter.fetchCustomersOfGroup(searchParam, true, "loadMore");
        };
        this.view.flxCustomerID.onClick = function() {
            var searchParam = scopeObj.getDefaultSearchParam();
            searchParam._sortVariable = scopeObj.assignCustSegColumns.backendNames[2];
            scopeObj.resetSortImages("assigncust", scopeObj.assignCustSegColumns.frontendIcons[2]);
            if (scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[2]].text === scopeObj.statusFontIcon.ASCENDING_IMAGE) {
                searchParam._sortDirection = "DESC";
                scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[2]].text = scopeObj.statusFontIcon.DESCENDING_IMAGE;
                scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[2]].skin = scopeObj.statusFontIcon.DESCENDING_SKIN;
            } else {
                searchParam._sortDirection = "ASC";
                scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[2]].text = scopeObj.statusFontIcon.ASCENDING_IMAGE;
                scopeObj.view["" + scopeObj.assignCustSegColumns.frontendIcons[2]].skin = scopeObj.statusFontIcon.ASCENDING_SKIN;
            }
            scopeObj.presenter.fetchCustomersOfGroup(searchParam, true, "loadMore");
        };
        this.view.lstBoxType.onSelection = function() {
            scopeObj.view.lstBoxType.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
            scopeObj.view.flxTypeError.setVisibility(false);
            scopeObj.hideUIBasedOnRoleType();
            scopeObj.createRequestParam.Type_id = scopeObj.view.lstBoxType.selectedKey;
        };
        this.view.switchStatus.onSlide = function() {
            scopeObj.createRequestParam.Status_id = scopeObj.view.switchStatus.selectedIndex === 0 ? scopeObj.statusConfig.active : scopeObj.statusConfig.inactive;
            scopeObj.editRequestParam.Status_id = scopeObj.view.switchStatus.selectedIndex === 0 ? scopeObj.statusConfig.active : scopeObj.statusConfig.inactive;
        };
        this.view.switchEAgreement.onSlide = function() {
            //changing values for backend request
            scopeObj.createRequestParam.isEAgreementActive = scopeObj.view.switchEAgreement.selectedIndex === 0 ? 1 : 0;
            scopeObj.editRequestParam.isEAgreementActive = scopeObj.view.switchEAgreement.selectedIndex === 0 ? 1 : 0;
        };
        this.view.btnImportCust.onClick = function() {
            scopeObj.importCustomers();
        };
        this.view.btnImportNewCust.onClick = function() {
            scopeObj.importCustomers();
        };
        this.view.btnRemoveAll.onClick = function() {
            scopeObj.custToAdd = [];
            scopeObj.editRequestParam.IsRemoveAll = true;
            scopeObj.view.segAssignedCustomers.removeAll();
            //after removal
            scopeObj.view.flxAddCustomerButtons.setVisibility(true);
            scopeObj.view.flxRemoveCustButtons.setVisibility(false);
            var segdata = scopeObj.view.segAssignedCustomers.data;
            if (segdata.length === 0) {
                scopeObj.view.flxNoCustomerAssign.setVisibility(true);
                scopeObj.view.flxCustomerAssigned.setVisibility(false);
            }
        };
    },
    showToastMessage: function() {
        this.view.flxToastMessageSearch.isVisible = true;
        kony.timer.schedule("mytimer", this.callBackTimer, 2, false);
    },
    callBackTimer: function() {
        this.view.flxToastMessageSearch.isVisible = false;
    },
    hoverScheduler: function() {
        this.view.lblScheduler.skin = "sknlblffffffLato12px";
        this.view.imgClose.src = "close_small2x.png";
        this.view.forceLayout();
    },
    addFeeRange: function() {
        flag = true;
        var self = this;
        var data = this.view.deleteRow.segFeeRange.data;
        var toAdd = {
            "flxMinAmount": {
                "skin": "skntxtbxNormald7d9e0"
            },
            "flxMaxAmount": {
                "skin": "skntxtbxNormald7d9e0"
            },
            "flxFee": {
                "skin": "skntxtbxNormald7d9e0"
            },
            "txtbxAmount": {
                "placeholder": "0.00"
            },
            "txtbxMaxAmount": {
                "placeholder": "0.00"
            },
            "txtbxFee": {
                "placeholder": "0.00"
            },
            "flxDelete": {
                "isVisible": true,
                "onClick": function() {
                    self.deleteRow();
                }
            },
            "lblSeparator3": ".",
            "lblSeparator2": ".",
            "lblSeparator1": ".",
            "flx1": {
                "skin": "sknflxNoneditable"
            },
            "flxMax1": {
                "skin": "sknflxNoneditable"
            },
            "flxFee1": {
                "skin": "sknflxNoneditable"
            },
            "lblDollar": "$",
            "lblDollar1": "$",
            "lblDollar2": "$",
            "imgDelete": {
                "src": "delete_2x.png",
                "isVisible": true
            },
            "template": "flxFeeRange"
        };
        data.push(toAdd);
        this.view.deleteRow.segFeeRange.setData(data);
        if (data.length > 1) {
            data[0].imgDelete.isVisible = true;
        }
        this.view.forceLayout();
    },
    showAdvanceSeearchOptions: function() {
        this.view.flxSearchOptions.setVisibility(false);
    },
    navigateBreadcrum: function() {
        if (this.view.breadcrumbs.lblCurrentScreen.text === kony.i18n.getLocalizedString("i18n.frmGroupsController.CONFIGURE")) this.showGroupViewEntitlements();
    },
    setMasterListData: function() {
        //setmasterListData
        var dataMap = {
            "flxCheckbox": "flxCheckbox",
            "flxMain": "flxMain",
            "flxMasterList": "flxMasterList",
            "imgCheckbox": "imgCheckbox",
            "lblSchedulerName": "lblSchedulerName",
            "segMasterList": "segMasterList"
        };
        var data = [{
            "imgCheckbox": {
                "src": "radio_notselected.png"
            },
            "lblSchedulerName": kony.i18n.getLocalizedString("i18n.frmGroupsController.24x7_Scheduler"),
            "template": "flxMasterList"
        }, {
            "imgCheckbox": {
                "src": "radio_notselected.png"
            },
            "lblSchedulerName": "Bi-Weekly",
            "template": "flxMasterList"
        }, {
            "imgCheckbox": {
                "src": "radio_notselected.png"
            },
            "lblSchedulerName": kony.i18n.getLocalizedString("i18n.frmGroupsController.Branch_Working_Hours"),
            "template": "flxMasterList"
        }];
        this.view.SelectScheduler.segMasterList.widgetDataMap = dataMap;
        this.view.SelectScheduler.segMasterList.setData(data);
        this.view.SelectScheduler.flxNoDetails.setVisibility(true);
        this.view.SelectScheduler.flxDetails.setVisibility(false);
        this.view.forceLayout();
    },
    setScheduler: function() {
        var data = this.view.SelectScheduler.segMasterList.data;
        var selSchedule = -1;
        for (var i = 0; i < data.length; i++) {
            if (data[i].imgCheckbox.src === "radioselected_2x.png") {
                selSchedule = i;
                break;
            }
        }
        if (selSchedule === -1) {
            this.view.SelectScheduler.flxNoDetails.setVisibility(true);
            this.view.SelectScheduler.flxDetails.setVisibility(false);
            this.view.flxSelectSchedulerPopup.setVisibility(true);
        } else {
            this.view.lblScheduler.text = data[selSchedule].lblSchedulerName;
            this.view.flxSelectSchedulerPopup.setVisibility(false);
            this.view.btnSelectScheduler.setVisibility(false);
            this.view.flxScheduler.setVisibility(true);
        }
    },
    hideAll: function() {
        this.view.verticalTabs.flxOption4.setVisibility(false);
        this.view.flxAdvanceSearchPopUp.setVisibility(false);
        this.view.flxBreadCrumbs.setVisibility(false);
        this.view.flxNoGroups.setVisibility(false);
        this.view.flxAddGroups.setVisibility(false);
        this.view.flxGroupList.setVisibility(false);
        this.view.flxGroupview.setVisibility(false);
        this.view.flxEntitlementDetails.setVisibility(false);
        this.view.flxSelectSchedulerPopup.setVisibility(false);
        this.view.flxDeactivateGroup.setVisibility(false);
        this.view.mainHeader.flxButtons.setVisibility(false);
    },
    setPreshowData: function() {
        this.hideAll();
        this.view.subHeader.tbxSearchBox.text = "";
        this.view.subHeader.flxClearSearchImage.setVisibility(false);
        this.view.lblGroupDescriptionCount.text = "0/250";
        this.view.lblGroupNameCount.text = "0/50";
        this.view.subHeader.lbxPageNumbers.selectedKey = "lbl1";
        this.view.flxMain.height = kony.os.deviceInfo().screenHeight + "px";
        this.view.subHeader.tbxSearchBox.placeholder = kony.i18n.getLocalizedString("i18n.frmGroups.Search_by_Group_Name");
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.Groups.Heading");
        this.view.verticalTabs.btnOption1.text = kony.i18n.getLocalizedString("i18n.Group.GroupDetails");
        this.view.verticalTabs.btnOption2.text = kony.i18n.getLocalizedString("i18n.Group.ASSIGNENTITLEMENTS");
        this.view.verticalTabs.btnOption3.text = kony.i18n.getLocalizedString("i18n.Group.ASSIGNCUSTOMERS");
        this.view.verticalTabs.btnOption3.info = {
            "firstClick": true
        };
        this.view.lblGroupValidTill.setVisibility(false);
        this.view.fontIconSortValidTill.setVisibility(false);
        this.view.mainHeader.flxHeaderSeperator.setVisibility(false);
        this.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
        this.view.flxNoGroups.setVisibility(false);
        this.view.flxNoGroupNameError.setVisibility(false);
        this.view.flxNoGroupDescriptionError.setVisibility(false);
        this.view.flxStatusFilter.setVisibility(false);
        this.view.flxTypeFilter.setVisibility(false);
        this.view.verticalTabs.lblSelected1.setVisibility(false);
        this.view.verticalTabs.lblSelected2.setVisibility(false);
        this.view.verticalTabs.lblSelected3.setVisibility(false);
        this.view.flxToastMessage.setVisibility(false);
        this.tabUtilLabelFunction([this.view.lblEntitlements,
            this.view.lblCustomersFix
        ], this.view.lblEntitlements);
        this.view.forceLayout();
        this.editRequestParam = {};
    },
    showAddGroupDetails: function(opt, grpName) {
        this.hideAll();
        this.view.flxAddGroups.setVisibility(true);
        this.hideShowHeadingSeperator(false);
        this.view.flxBreadCrumbs.setVisibility(false);
        this.view.verticalTabs.flxOption4.setVisibility(false);
        this.view.verticalTabs.lblSelected1.setVisibility(true);
        this.view.verticalTabs.lblSelected2.setVisibility(false);
        this.view.verticalTabs.lblSelected3.setVisibility(false);
        this.view.calValidStartDate.dateEditable = false;
        this.view.calValidEndDate.dateEditable = false;
        this.view.flxGroupDetails.setVisibility(true);
        this.view.flxAssignEntitlements.setVisibility(false);
        this.view.flxAssignCustomers.setVisibility(false);
        this.view.flxCustomerAssigned.setVisibility(false);
        this.view.flxNoCustomerAssign.setVisibility(false);
        this.view.addAndRemoveOptionsCustomers.tbxSearchBox.text = "";
        this.view.addAndRemoveOptionsCustomers.segAddOptions.removeAll();
        this.view.lstBoxType.setEnabled(true);
        this.view.lstBoxType.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
        this.view.lstBoxType.hoverSkin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
        //Hiding visibility as not inplementing for now
        this.view.lblValidity.setVisibility(false);
        this.view.CopylblValidity0d9c3f33b4acc44.setVisibility(false);
        this.view.calValidEndDate.setVisibility(false);
        this.view.calValidStartDate.setVisibility(false);
        var widgetArray = [this.view.verticalTabs.btnOption1, this.view.verticalTabs.btnOption2, this.view.verticalTabs.btnOption3];
        this.tabUtilVerticleButtonFunction(widgetArray, this.view.verticalTabs.btnOption1);
        document.getElementById("frmGroups_flxAssignCustomerAdded").onscroll = null;
        if (opt === 1) {
            this.view.tbxGroupNameValue.text = "";
            this.view.calValidStartDate.date = null;
            this.view.calValidEndDate.date = null;
            this.view.txtGroupDescription.text = "";
            this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.GROUPS");
            this.view.breadcrumbs.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.DragBox.ADD");
            this.view.breadcrumbs.btnPreviousPage.setVisibility(false);
            this.view.breadcrumbs.fontIconBreadcrumbsRight2.setVisibility(false);
            this.view.listingSegmentClient.segListing.selectedIndex = null;
            this.view.addAndRemoveOptions.segSelectedOptions.removeAll();
            this.view.addAndRemoveOptionsCustomers.segSelectedOptions.removeAll();
            this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Add_Group");
            kony.adminConsole.utils.hideProgressBar(this.view);
        } else {
            kony.adminConsole.utils.showProgressBar(this.view);
            this.view.addAndRemoveOptions.segSelectedOptions.removeAll();
            this.view.addAndRemoveOptionsCustomers.segSelectedOptions.removeAll();
            this.view.tbxGroupNameValue.text = "";
            this.view.calValidStartDate.date = "";
            this.view.calValidEndDate.date = "";
            this.view.txtGroupDescription.text = "";
            this.view.lstBoxType.setEnabled(false);
            this.view.lstBoxType.skin = "lstBxBre1e5edR3pxBgf5f6f8Disable";
            this.view.lstBoxType.hoverSkin = "sknLstBxBre1e5edBgf5f6f813pxDisableHover";
            this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.GROUPS");
            this.view.breadcrumbs.fontIconBreadcrumbsRight.setVisibility(true);
            this.view.breadcrumbs.lblCurrentScreen.text = grpName.toUpperCase();
            this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Edit_Group");
            var self = this;
            kony.adminConsole.utils.hideProgressBar(self.view);
        }
    },
    /*
     * function to perform segment data mapping for segAddOption segment
     * @param: list of entitlements not assigned
     * @return : mapped array json data 
     */
    parseEntitlementForAddSeg: function(addSegData) {
        var self = this;
        var data = addSegData.map(function(unAssgEnt) {
            return {
                "id": unAssgEnt.id,
                "Group_id": unAssgEnt.Group_id,
                "Service_id": unAssgEnt.id,
                "TransactionFee_id": self.checkForUndefined(unAssgEnt.TransactionFee_id),
                "TransactionLimit_id": self.checkForUndefined(unAssgEnt.TransactionLimit_id),
                "btnAdd": {
                    "text": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                    "onClick": function() {
                        self.addEntitlement();
                    }
                },
                "template": "flxGrpAddOption",
                "lblName": unAssgEnt.Name,
                "rtxDescription": unAssgEnt.Description
            };
        });
        return data;
    },
    /*
     * function to perform segment data mapping for segSelectedOptions segment
     * @param: list of entitlements assigned
     * @return : mapped array json data 
     */
    parseEntitlementsForSelectedSeg: function(selectedSegData) {
        var self = this;
        var data2 = selectedSegData.map(function(assgEnt) {
            return {
                "flxClose": {
                    "onClick": function() {
                        self.removeEntitlement();
                    }
                },
                "fontIconClose": {
                    "text": "\ue929",
                    "tooltip": kony.i18n.getLocalizedString("i18n.frmGroupsController.Remove_Entitlement")
                },
                "id": assgEnt.Service_id,
                "Group_id": assgEnt.Group_id,
                "Service_id": assgEnt.Service_id,
                "TransactionFee_id": self.checkForUndefined(assgEnt.TransactionFee_id),
                "TransactionLimit_id": self.checkForUndefined(assgEnt.TransactionLimit_id),
                "lblOption": {
                    "text": assgEnt.Name.length >= 38 ? assgEnt.Name.substring(0, 35) + "..." : assgEnt.Name,
                    "tooltip": "" + assgEnt.Name
                },
                "lblOptionText": assgEnt.Name,
                "hiddenDescription": assgEnt.Description,
                "template": "flxOptionAdded"
            };
        });
        return data2;
    },
    showAssignEntitlements: function(unassignedList, assignedList) {
        var self = this;
        this.hideAll();
        this.hideShowHeadingSeperator(false);
        this.view.flxBreadCrumbs.setVisibility(false);
        this.view.flxAddGroups.setVisibility(true);
        this.view.flxGroupDetails.setVisibility(false);
        this.view.flxAssignEntitlements.setVisibility(true);
        this.view.flxCustomerAssigned.setVisibility(false);
        this.view.flxNoCustomerAssign.setVisibility(false);
        this.view.flxAssignCustomers.setVisibility(false);
        this.view.verticalTabs.lblSelected1.setVisibility(false);
        this.view.verticalTabs.lblSelected2.setVisibility(true);
        this.view.verticalTabs.lblSelected3.setVisibility(false);
        var widgetArray = [this.view.verticalTabs.btnOption1, this.view.verticalTabs.btnOption2, this.view.verticalTabs.btnOption3];
        this.tabUtilVerticleButtonFunction(widgetArray, this.view.verticalTabs.btnOption2);
        this.view.addAndRemoveOptions.btnSelectAll.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Add_All");
        this.view.addAndRemoveOptions.btnRemoveAll.text = kony.i18n.getLocalizedString("i18n.Common.button.Reset");
        this.view.addAndRemoveOptions.lblAvailableOptionsHeading.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Available_Entitlements");
        this.view.addAndRemoveOptions.lblSelectedOption.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Selected_Entitlements");
        this.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(false);
        this.view.addAndRemoveOptions.rtxSelectedOptionsMessage.setVisibility(false);
        if (unassignedList === undefined) {
            unassignedList = [];
        }
        if (assignedList === undefined) {
            assignedList = [];
        }
        var dataMap = {
            "id": "id",
            "Group_id": "Group_id",
            "Service_id": "Service_id",
            "TransactionFee_id": "TransactionFee_id",
            "TransactionLimit_id": "TransactionLimit_id",
            "btnAdd": "btnAdd",
            "flxAdd": "flxAdd",
            "flxAddOption": "flxAddOption",
            "lblName": "lblName",
            "rtxDescription": "rtxDescription"
        };
        if (unassignedList.length === 0) {
            self.view.addAndRemoveOptions.btnSelectAll.setVisibility(false);
            self.view.addAndRemoveOptions.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.No_Entitlements_Available");
            self.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(true);
            self.view.addAndRemoveOptions.rtxSelectedOptionsMessage.setVisibility(false);
        } else self.view.addAndRemoveOptions.btnSelectAll.setVisibility(true);
        this.view.addAndRemoveOptions.segAddOptions.widgetDataMap = dataMap;
        this.view.addAndRemoveOptions.segAddOptions.setVisibility(true);
        this.view.addAndRemoveOptions.segAddOptions.setData(unassignedList);
        this.view.addAndRemoveOptions.segAddOptions.info = {
            "segData": this.view.addAndRemoveOptions.segAddOptions.data
        };
        self.entitlementDataSearch = unassignedList;
        this.view.forceLayout();
        if (typeof assignedList !== "undefined") {
            var datamap2 = {
                "id": "id",
                "Group_id": "Group_id",
                "Service_id": "Service_id",
                "TransactionFee_id": "TransactionFee_id",
                "TransactionLimit_id": "TransactionLimit_id",
                "flxAddOptionWrapper": "flxAddOptionWrapper",
                "flxClose": "flxClose",
                "flxOptionAdded": "flxOptionAdded",
                "fontIconClose": "fontIconClose",
                "hiddenDescription": "hiddenDescription",
                "lblOption": "lblOption"
            };
            if (assignedList.length === 0) {
                self.view.addAndRemoveOptions.btnRemoveAll.setVisibility(false);
                self.view.addAndRemoveOptions.rtxSelectedOptionsMessage.setVisibility(true);
            } else self.view.addAndRemoveOptions.btnRemoveAll.setVisibility(true);
            this.view.addAndRemoveOptions.segSelectedOptions.widgetDataMap = datamap2;
            this.view.addAndRemoveOptions.segSelectedOptions.setVisibility(true);
            this.view.addAndRemoveOptions.segSelectedOptions.setData(assignedList);
        }
        kony.adminConsole.utils.hideProgressBar(this.view);
        this.view.forceLayout();
    },
    showAssignCustomers: function(forEdit, groupId) {
        this.hideAll();
        this.hideShowHeadingSeperator(false);
        this.view.flxBreadCrumbs.setVisibility(false);
        this.view.flxAddGroups.setVisibility(true);
        this.view.flxGroupDetails.setVisibility(false);
        this.view.flxAssignEntitlements.setVisibility(false);
        this.view.commonButtonsAddCustomers.btnNext.setVisibility(false);
        this.view.commonButtonsAddedCustomers.btnNext.setVisibility(false);
        this.view.flxNoCustomerAssign.setVisibility(true);
        this.view.verticalTabs.lblSelected1.setVisibility(false);
        this.view.verticalTabs.lblSelected2.setVisibility(false);
        this.view.verticalTabs.lblSelected3.setVisibility(true);
        this.view.addAndRemoveOptionsCustomers.lblAvailableOptionsHeading.text = "Customers";
        this.view.addAndRemoveOptionsCustomers.lblSelectedOption.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Selected_Customers");
        this.view.addAndRemoveOptionsCustomers.btnSelectAll.setVisibility(false);
        this.view.addAndRemoveOptionsCustomers.btnRemoveAll.setVisibility(false);
        this.view.commonButtonsCustomers.btnNext.setVisibility(false);
        this.view.addAndRemoveOptionsCustomers.rtxAvailableOptionsMessage.setVisibility(false);
        this.view.addAndRemoveOptionsCustomers.rtxSelectedOptionsMessage.setVisibility(false);
        this.view.addAndRemoveOptionsCustomers.segAddOptions.setVisibility(false);
        this.view.addAndRemoveOptionsCustomers.rtxAvailableOptionsMessage.setVisibility(true);
        this.view.addAndRemoveOptionsCustomers.segSelectedOptions.setVisibility(false);
        this.view.addAndRemoveOptionsCustomers.rtxSelectedOptionsMessage.setVisibility(true);
        var widgetArray = [this.view.verticalTabs.btnOption1, this.view.verticalTabs.btnOption2, this.view.verticalTabs.btnOption3];
        this.tabUtilVerticleButtonFunction(widgetArray, this.view.verticalTabs.btnOption3);
        this.view.rtxNoResults.setVisibility(false);
        this.view.flxCustomerStatusFilter.setVisibility(false);
        this.view.flxAddCustomerButtons.setVisibility(true);
        this.view.flxRemoveCustButtons.setVisibility(false);
        var hasData = (this.orgAssignedCust.length > 0) || (this.custToAdd.length > 0);
        this.view.flxCustomerAssigned.setVisibility(hasData);
        this.view.flxAssignCustomerAdded.setVisibility(hasData);
        this.view.flxNoCustomerAssign.setVisibility(!hasData);
        this.view.segAssignedCustomers.setVisibility(hasData);
        this.view.imgCustHeaderCheckBox.src = "checkbox.png";
        this.setDataForCustomerStatusFilter();
        if (!forEdit && (this.custToAdd.length === 0)) {
            this.view.segAssignedCustomers.setData([]);
        }
    },
    showGroupUIChanges: function() {
        this.hideAll();
        this.view.flxGroupList.setVisibility(true);
        this.view.mainHeader.setVisibility(true);
        this.view.mainHeader.flxButtons.setVisibility(true);
        this.view.flxBreadCrumbs.setVisibility(false);
        this.view.subHeader.flxMenu.left = "0dp";
        this.view.subHeader.flxSearch.right = "0dp";
        this.view.mainHeader.flxHeaderSeperator.setVisibility(true);
        this.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
        this.view.listingSegmentClient.contextualMenu.flxOption1.setVisibility(false);
        this.view.listingSegmentClient.contextualMenu.flxOption3.setVisibility(false);
    },
    showGroupList: function(response) {
        this.hideAll();
        this.view.flxGroupList.setVisibility(true);
        this.view.mainHeader.setVisibility(true);
        this.view.mainHeader.flxButtons.setVisibility(true);
        this.view.flxBreadCrumbs.setVisibility(false);
        this.view.subHeader.flxMenu.left = "0dp";
        this.view.subHeader.flxSearch.right = "0dp";
        this.view.mainHeader.flxHeaderSeperator.setVisibility(true);
        this.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
        this.view.forceLayout();
        this.view.flxStatusFilter.setVisibility(false);
        this.hideShowHeadingSeperator(true);
        this.view.listingSegmentClient.contextualMenu.flxOption1.setVisibility(false);
        this.view.listingSegmentClient.contextualMenu.flxOption3.setVisibility(false);
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.Groups.Heading");
        var self = this;
        this.groupId = undefined;
        var data;
        //segGroupListData
        var dataMap = {
            "Group_id": "Group_id",
            "Status_id": "Status_id",
            "flxGroupseg": "flxGroupseg",
            "flxGroupsegmain": "flxGroupsegmain",
            "flxOptions": "flxOptions",
            "flxSegMain": "flxSegMain",
            "flxStatus": "flxStatus",
            "fontIconGroupStatus": "fontIconGroupStatus",
            "fontIconOptions": "fontIconOptions",
            "lblGroupCustomers": "lblGroupCustomers",
            "lblGroupEntitlements": "lblGroupEntitlements",
            "lblGroupName": "lblGroupName",
            "lblGroupStatus": "lblGroupStatus",
            "lblGroupValidTill": "lblGroupValidTill",
            "lblHeaderGroupDescription": "lblHeaderGroupDescription",
            "lblSeparator": "lblSeparator",
            "Type_id": "Type_id",
            "isEAgreementActive": "isEAgreementActive",
            "segGroupList": "segGroupList"
        };
        if (typeof response !== 'undefined' || response !== null) {
            if (response.length > 0) {
                data = response.map(function(groupViewData) {
                    var isOptionsVisible = true;
                    if (self.listOfProgressGroup.indexOf(groupViewData.Group_id) >= 0) {
                        isOptionsVisible = false;
                    }
                    return {
                        "Group_id": groupViewData.Group_id,
                        "fontIconGroupStatus": {
                            "skin": (groupViewData.Status_id === self.statusConfig.active) ? "sknFontIconActivate" : "sknfontIconInactive"
                        },
                        "Status_id": groupViewData.Status_id,
                        "fontIconOptions": {
                            "text": "\ue91f"
                        },
                        "lblGroupCustomers": {
                            "text": groupViewData.Customers_Count
                        },
                        "lblGroupEntitlements": {
                            "text": groupViewData.Type_Name
                        },
                        "lblGroupName": {
                            "text": groupViewData.Group_Name
                        },
                        "lblGroupStatus": (groupViewData.Status_id === self.statusConfig.active) ? {
                            "text": kony.i18n.getLocalizedString("i18n.secureimage.Active"),
                            "skin": "sknlblLato5bc06cBold14px"
                        } : {
                            "text": kony.i18n.getLocalizedString("i18n.secureimage.Inactive"),
                            "skin": "sknlblLatocacacaBold12px"
                        },
                        "lblGroupValidTill": {
                            "text": ""
                        },
                        "lblHeaderGroupDescription": {
                            "text": groupViewData.Group_Desc
                        },
                        "lblSeparator": ".",
                        "flxOptions": {
                            "isVisible": isOptionsVisible,
                            "onClick": function() {
                                self.toggleContextualMenu(50);
                            }
                        },
                        "Type_id": groupViewData.Type_id,
                        "isEAgreementActive": groupViewData.isEAgreementActive === "true" ? 1 : 0,
                        "template": "flxGroupseg"
                    };
                });
                self.view.listingSegmentClient.segListing.setVisibility(true);
                self.view.flxGroupHeader.setVisibility(true);
                self.view.flxGroupSeparator.setVisibility(true);
                self.view.listingSegmentClient.rtxNoResultsFound.setVisibility(false);
                self.view.listingSegmentClient.segListing.widgetDataMap = dataMap;
                self.view.listingSegmentClient.segListing.setData(data);
                document.getElementById("frmGroups_listingSegmentClient_segListing").onscroll = this.contextualMenuOff;
                this.view.forceLayout();
            } else {
                if (self.view.subHeader.tbxSearchBox.text !== "") {
                    self.view.listingSegmentClient.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found") + self.view.subHeader.tbxSearchBox.text + kony.i18n.getLocalizedString("i18n.frmCSRController._Try_with_another_keyword");
                } else {
                    self.view.listingSegmentClient.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
                }
                self.view.flxGroupHeader.setVisibility(false);
                self.view.flxGroupSeparator.setVisibility(false);
                self.view.listingSegmentClient.rtxNoResultsFound.setVisibility(true);
                self.view.listingSegmentClient.segListing.setVisibility(false);
                this.view.forceLayout();
            }
        }
        kony.adminConsole.utils.hideProgressBar(this.view);
    },
    contextualMenuOff: function(context) {
        this.view.listingSegmentClient.flxContextualMenu.isVisible = false;
    },
    toggleContextualMenu: function(rowHeight) {
        var index = this.view.listingSegmentClient.segListing.selectedIndex;
        this.sectionIndex = index[0];
        var rowIndex = index[1];
        var templateArray = this.view.listingSegmentClient.segListing.clonedTemplates;
        if (this.view.listingSegmentClient.flxContextualMenu.isVisible === false) {
            this.view.listingSegmentClient.flxContextualMenu.setVisibility(true);
        } else {
            this.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
        }
        this.view.forceLayout();
        //to caluclate top from preffered row heights
        var finalHeight = 0;
        for (var i = 0; i < rowIndex; i++) {
            finalHeight = finalHeight + templateArray[i].flxGroupseg.frame.height;
        }
        var flexLeft = this.view.listingSegmentClient.segListing.clonedTemplates[rowIndex].flxOptions.frame.x;
        this.view.listingSegmentClient.flxContextualMenu.left = ((flexLeft + 25) - 200) + "px";
        this.view.listingSegmentClient.contextualMenu.btnLink1.text = kony.i18n.getLocalizedString("i18n.userwidgetmodel.lblCol03");
        this.view.listingSegmentClient.contextualMenu.btnLink2.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers");
        var data = this.view.listingSegmentClient.segListing.data;
        if (data[rowIndex].lblGroupStatus.text === kony.i18n.getLocalizedString("i18n.secureimage.Active")) {
            this.view.listingSegmentClient.contextualMenu.lblOption4.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate");
            this.view.listingSegmentClient.contextualMenu.lblIconOption4.text = "\ue91c";
        } else {
            this.view.listingSegmentClient.contextualMenu.lblOption4.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Activate");
            this.view.listingSegmentClient.contextualMenu.lblIconOption4.text = "\ue931";
        }
        this.toggleMenuItemsBasedOnType(data[rowIndex].Type_id);
        var segmentWidget = this.view.listingSegmentClient.segListing;
        var contextualWidget = this.view.listingSegmentClient.contextualMenu;
        finalHeight = ((finalHeight + 45) - segmentWidget.contentOffsetMeasured.y);
        if (finalHeight + contextualWidget.frame.height > segmentWidget.frame.height) {
            finalHeight = finalHeight - contextualWidget.frame.height - 45;
        }
        this.view.listingSegmentClient.flxContextualMenu.top = finalHeight + "px";
        if (this.view.flxStatusFilter.isVisible) {
            this.view.flxStatusFilter.setVisibility(false);
        }
        kony.print(kony.i18n.getLocalizedString("i18n.frmGroupsController.called_in_form_controller"));
    },
    updateContextualMenu: function(rowIndex) {
        kony.print(kony.i18n.getLocalizedString("i18n.frmGroupsController.updating_contextual_menu"));
    },
    addEntitlement: function() {
        var self = this;
        var selectedItemData = this.view.addAndRemoveOptions.segAddOptions.selectedItems[0];
        this.view.addAndRemoveOptions.rtxSelectedOptionsMessage.setVisibility(false);
        this.view.addAndRemoveOptions.btnRemoveAll.setVisibility(true);
        var name = "" + selectedItemData.lblName;
        var toAdd = {
            "flxClose": {
                "onClick": function() {
                    self.removeEntitlement();
                }
            },
            "fontIconClose": {
                "text": "\ue929",
                "tooltip": kony.i18n.getLocalizedString("i18n.frmGroupsController.Remove_Entitlement")
            },
            "lblOption": {
                "text": name.length >= 38 ? name.substring(0, 35) + "..." : name,
                "tooltip": name
            },
            "lblOptionText": name,
            "hiddenDescription": "" + selectedItemData.rtxDescription,
            "id": "" + selectedItemData.id,
            "Group_id": "" + selectedItemData.Group_id,
            "Service_id": "" + selectedItemData.Service_id,
            "TransactionFee_id": "" + self.checkForUndefined(selectedItemData.TransactionFee_id),
            "TransactionLimit_id": "" + self.checkForUndefined(selectedItemData.TransactionLimit_id),
            "template": "flxOptionAdded"
        };
        this.assignEntitleData.push(toAdd);
        var isSaved = false;
        for (var i = 0; i < this.orgEntitlementList.length; i++) {
            if (this.orgEntitlementList[i].Service_id === selectedItemData.id) {
                delete this.orgEntitlementList[i].isRemove;
                isSaved = true;
                break;
            }
        }
        if (!isSaved) {
            var newElem = {
                "Service_id": selectedItemData.id,
                "TransactionLimit_id": self.checkForUndefined(selectedItemData.TransactionLimit_id),
                "TransactionFee_id": self.checkForUndefined(selectedItemData.TransactionFee_id)
            };
            this.newAssignedEntitle.push(newElem);
        }
        this.view.addAndRemoveOptions.segAddOptions.info.segData = (this.view.addAndRemoveOptions.segAddOptions.info.segData).filter(function(record) {
            if (record.id !== selectedItemData.id) {
                return record;
            }
        });
        var rowIndex = this.view.addAndRemoveOptions.segAddOptions.selectedIndex[1];
        this.view.addAndRemoveOptions.segAddOptions.removeAt(rowIndex);
        this.view.addAndRemoveOptions.segSelectedOptions.setVisibility(true);
        this.view.addAndRemoveOptions.segSelectedOptions.setData(this.assignEntitleData);
        if (this.view.addAndRemoveOptions.segAddOptions.data.length === 0) {
            this.view.addAndRemoveOptions.btnSelectAll.setVisibility(false);
            this.view.addAndRemoveOptions.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.No_Entitlements_Available");
            this.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(true);
        } else this.view.addAndRemoveOptions.btnSelectAll.setVisibility(true);
        var isNoAvailableOptions = this.view.addAndRemoveOptions.segAddOptions.data.length === 0;
        this.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(isNoAvailableOptions);
        this.view.forceLayout();
    },
    removeEntitlement: function() {
        var self = this;
        var rowIndex = this.view.addAndRemoveOptions.segSelectedOptions.selectedIndex[1];
        //add row to right
        this.view.addAndRemoveOptions.btnSelectAll.setVisibility(true);
        var selectedServicesData = this.view.addAndRemoveOptions.segSelectedOptions.data;
        var availableServicesData = this.view.addAndRemoveOptions.segAddOptions.data;
        var toAddData = {
            "btnAdd": {
                "text": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                "onClick": function() {
                    self.addEntitlement();
                }
            },
            "id": "" + selectedServicesData[rowIndex].id,
            "Serivce_id": "" + selectedServicesData[rowIndex].Service_id,
            "TransactionFee_id": "" + self.checkForUndefined(selectedServicesData[rowIndex].TransactionFee_id),
            "TransactionLimit_id": "" + self.checkForUndefined(selectedServicesData[rowIndex].TransactionLimit_id),
            "template": "flxGrpAddOption",
            "lblName": "" + selectedServicesData[rowIndex].lblOptionText,
            "rtxDescription": "" + selectedServicesData[rowIndex].hiddenDescription
        };
        this.allEntitleData.push(toAddData);
        this.view.addAndRemoveOptions.segAddOptions.setVisibility(true);
        this.view.addAndRemoveOptions.segAddOptions.setData(this.allEntitleData);
        this.view.addAndRemoveOptions.segAddOptions.info.segData = this.allEntitleData;
        //remove row from left
        var deletedData = this.view.addAndRemoveOptions.segSelectedOptions.selectedItems[0];
        var isSaved = false;
        for (var i = 0; i < this.orgEntitlementList.length; i++) {
            if (this.orgEntitlementList[i].Service_id === deletedData.id) {
                this.orgEntitlementList[i].isRemove = true;
                isSaved = true;
                break;
            }
        }
        if (!isSaved) {
            for (var j = 0; j < this.newAssignedEntitle.length; j++) {
                if (this.newAssignedEntitle[j].Service_id === deletedData.id) {
                    this.newAssignedEntitle.splice(j, 1);
                    break;
                }
            }
        }
        this.clearSearchBoxToDefaults();
        //check if right segment is empty and display rtx
        this.view.addAndRemoveOptions.segSelectedOptions.removeAt(rowIndex);
        if (this.view.addAndRemoveOptions.segSelectedOptions.data.length === 0) {
            this.view.addAndRemoveOptions.btnRemoveAll.setVisibility(false);
            this.view.addAndRemoveOptions.rtxSelectedOptionsMessage.setVisibility(true);
        } else this.view.addAndRemoveOptions.btnRemoveAll.setVisibility(true);
        var isNoAvailableOptions = this.view.addAndRemoveOptions.segAddOptions.data.length === 0;
        this.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(isNoAvailableOptions);
        this.view.forceLayout();
    },
    AddAllEntitlements: function() {
        var self = this;
        var availableEntitlemenrs = this.view.addAndRemoveOptions.segAddOptions.data;
        var data = this.view.addAndRemoveOptions.segSelectedOptions.data;
        for (var i = 0; i < availableEntitlemenrs.length; i++) {
            var name = "" + availableEntitlemenrs[i].lblName;
            var toAdd = {
                "flxClose": {
                    "onClick": function() {
                        self.removeEntitlement();
                    }
                },
                "id": "" + availableEntitlemenrs[i].id,
                "Service_id": "" + availableEntitlemenrs[i].Service_id,
                "TransactionFee_id": "" + self.checkForUndefined(availableEntitlemenrs[i].TransactionFee_id),
                "TransactionLimit_id": "" + self.checkForUndefined(availableEntitlemenrs[i].TransactionLimit_id),
                "fontIconClose": {
                    "text": "\ue929",
                    "tooltip": kony.i18n.getLocalizedString("i18n.frmGroupsController.Remove_Entitlement")
                },
                "lblOption": {
                    "text": name.length >= 38 ? name.substring(0, 35) + "..." : name,
                    "tooltip": name
                },
                "lblOptionText": name,
                "hiddenDescription": "" + availableEntitlemenrs[i].rtxDescription,
                "template": "flxOptionAdded"
            };
            this.assignEntitleData.push(toAdd);
        }
        //add on left
        this.view.addAndRemoveOptions.segSelectedOptions.setData(this.assignEntitleData);
        //remove all from right
        var actualEntil = this.view.addAndRemoveOptions.segAddOptions.info.segData;
        if (availableEntitlemenrs.length === actualEntil.length) {
            this.view.addAndRemoveOptions.segAddOptions.removeAll();
            this.allEntitleData = [];
            this.view.addAndRemoveOptions.segAddOptions.info.segData = [];
        } else { //in case search text exsist
            while (availableEntitlemenrs.length > 0) {
                this.updateAvailableEntitlementData(this.view.addAndRemoveOptions.segAddOptions.data[0].id);
                this.view.addAndRemoveOptions.segAddOptions.removeAt(0);
            }
        }
        var isSaved = false;
        for (var j = 0; j < this.orgEntitlementList.length; j++) {
            delete this.orgEntitlementList[j].isRemove;
        }
        if (!isSaved) {
            var newElem = [];
            for (var k = 0; k < availableEntitlemenrs.length; k++) {
                newElem[k] = {
                    "Service_id": availableEntitlemenrs[k].id,
                    "TransactionLimit_id": self.checkForUndefined(availableEntitlemenrs[k].TransactionLimit_id),
                    "TransactionFee_id": self.checkForUndefined(availableEntitlemenrs[k].TransactionFee_id)
                };
                this.newAssignedEntitle.push(newElem[k]);
            }
        }
        this.view.addAndRemoveOptions.rtxSelectedOptionsMessage.setVisibility(false);
        this.view.addAndRemoveOptions.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.No_Entitlements_Available");
        this.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(true);
        this.view.forceLayout();
    },
    //remove the added entitlement and update data variables
    updateAvailableEntitlementData: function(data) {
        var filterData = [];
        var segData = this.view.addAndRemoveOptions.segAddOptions.info.segData;
        for (var i = 0; i < segData.length; i++) {
            if (data === segData[i].id) {
                segData.splice(i, 1);
                break;
            }
        }
    },
    RemoveAllEntitlements: function() {
        var self = this;
        var selEntitlements = this.view.addAndRemoveOptions.segSelectedOptions.data;
        var availableEntitlements = this.view.addAndRemoveOptions.segAddOptions.data;
        for (var i = 0; i < selEntitlements.length; i++) {
            var toAddData = {
                "btnAdd": {
                    "text": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                    "onClick": function() {
                        self.addEntitlement();
                    }
                },
                "id": "" + selEntitlements[i].id,
                "Service_id": "" + selEntitlements[i].Service_id,
                "TransactionFee_id": "" + self.checkForUndefined(selEntitlements[i].TransactionFee_id),
                "TransactionLimit_id": "" + self.checkForUndefined(selEntitlements[i].TransactionLimit_id),
                "template": "flxGrpAddOption",
                "lblName": "" + selEntitlements[i].lblOptionText,
                "rtxDescription": "" + selEntitlements[i].hiddenDescription
            };
            this.allEntitleData.push(toAddData);
        }
        //add data on left
        this.view.addAndRemoveOptions.segAddOptions.setVisibility(true);
        this.view.addAndRemoveOptions.segAddOptions.setData(this.allEntitleData);
        var isSaved = false;
        for (var j = 0; j < this.orgEntitlementList.length; j++) {
            this.orgEntitlementList[j].isRemove = true;
            isSaved = true;
        }
        this.newAssignedEntitle = [];
        this.clearSearchBoxToDefaults();
        //remove data from right
        this.view.addAndRemoveOptions.segSelectedOptions.removeAll();
        this.assignEntitleData = [];
        //hide placeholder text
        this.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(this.allEntitleData.length === 0);
        this.view.addAndRemoveOptions.segAddOptions.info.segData = this.view.addAndRemoveOptions.segAddOptions.data;
        this.view.addAndRemoveOptions.rtxSelectedOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Click_on_ADD");
        this.view.addAndRemoveOptions.rtxSelectedOptionsMessage.setVisibility(true);
    },
    ToggleActive: function() {
        var index = this.view.listingSegmentClient.segListing.selectedIndex;
        var rowIndex = index[1];
        var data = this.view.listingSegmentClient.segListing.data;
        if (data[rowIndex].lblGroupStatus.text === kony.i18n.getLocalizedString("i18n.secureimage.Active")) {
            data[rowIndex].lblGroupStatus.text = kony.i18n.getLocalizedString("i18n.secureimage.Inactive");
            data[rowIndex].lblGroupStatus.skin = "sknlblLatocacacaBold12px";
            data[rowIndex].fontIconGroupStatus.skin = "sknfontIconInactive";
        } else {
            data[rowIndex].lblGroupStatus.text = kony.i18n.getLocalizedString("i18n.secureimage.Active");
            data[rowIndex].lblGroupStatus.skin = "sknlblLato5bc06cBold14px";
            data[rowIndex].fontIconGroupStatus.skin = "sknFontIconActivate";
        }
        this.view.listingSegmentClient.segListing.setDataAt(data[rowIndex], rowIndex);
        this.view.flxDeactivateGroup.setVisibility(false);
        this.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
        this.view.forceLayout();
    },
    showGroupDeactive: function() {
        var self = this;
        var index = [];
        var row = self.view.listingSegmentClient.segListing.selectedIndex;
        var grpName = "";
        if (row !== null) {
            index = row[1];
            grpName = self.view.listingSegmentClient.segListing.data[index].lblGroupName.text;
        } else {
            grpName = "";
        }
        this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Deactivate_Group");
        this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.deactivate_Group_Message") + "\"" + grpName + "\"" + kony.i18n.getLocalizedString("i18n.frmGroupsController.deactivate_Group_MessageContent");
        this.view.popUpDeactivate.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS");
        this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesDeactivate");
        this.view.flxDeactivateGroup.setVisibility(true);
        //btn deactivate on click
        this.view.popUpDeactivate.btnPopUpDelete.onClick = function() {
            self.changeStatusOfGroup();
            self.ToggleActive();
            var status = self.view.listingSegmentClient.contextualMenu.lblOption4.text;
        };
        this.view.listingSegmentClient.flxContextualMenu.setVisibility(false);
        this.view.forceLayout();
    },
    showGroupViewDetails: function() {
        this.hideAll();
        var data = this.view.listingSegmentClient.segListing.data;
        var Index = this.view.listingSegmentClient.segListing.selectedIndex;
        var rowIndex = Index[1];
        var rowData = data[rowIndex];
        var iconSkin, statusSkin;
        if (rowData.Status_id === this.statusConfig.active) {
            iconSkin = "sknFontIconActivate";
            statusSkin = "sknlblLato5bc06cBold14px";
        } else {
            iconSkin = "sknfontIconInactive";
            statusSkin = "sknlblLatocacacaBold12px";
        }
        this.view.Description.lblDescription.skin = "sknlblLato5d6c7f12px";
        this.view.Description.lblDescription.hoverSkin = "sknlbl485c7512pxHoverCursor";
        this.hideShowHeadingSeperator(false);
        this.view.flxGroupview.setVisibility(true);
        this.view.flxBreadCrumbs.setVisibility(true);
        this.view.lblViewGroupTypeValue.text = rowData.lblGroupEntitlements.text || kony.i18n.getLocalizedString("i18n.Applications.NA");
        this.view.breadcrumbs.lblCurrentScreen.text = (rowData.lblGroupName.text).toUpperCase();
        this.view.lblViewNameValue.text = rowData.lblGroupName.text;
        this.view.lblViewGroupTypeValue.text = rowData.lblGroupEntitlements.text || kony.i18n.getLocalizedString("i18n.Applications.NA");
        this.view.lblViewAgreementValue.text = rowData.isEAgreementActive === 1 ? kony.i18n.getLocalizedString("i18n.frmGroups.Required") : kony.i18n.getLocalizedString("i18n.frmGroups.Not_Required");
        this.view.Description.rtxDescription.text = rowData.lblHeaderGroupDescription.text;
        this.view.Description.lblToggleDescription.text = "\ue922";
        this.view.Description.rtxDescription.setVisibility(false);
        this.view.fontIconStatusValue.skin = iconSkin;
        this.view.lblViewValue2.skin = statusSkin;
        this.view.breadcrumbs.lblCurrentScreen.setVisibility(true);
        this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.GROUPS");
        this.view.lblViewValue2.text = rowData.lblGroupStatus.text;
        this.view.flxGroupViewValidTill.setVisibility(false);
        this.toggleViewGroupDetailsScreenTabs(rowData.Type_id);
        if (rowData.Type_id === this.typeConfig.campaign) {
            this.getCustomerInprogressList();
            this.showViewCustomers();
        } else {
            this.showGroupViewEntitlements();
        }
        this.view.forceLayout();
    },
    showGroupViewEntitlements: function() {
        var groupId = this.getSelectedGroupId();
        this.getEntitlementsForGroup(groupId);
        this.view.flxEntitlementsHeader.setVisibility(true);
        this.view.flxCustomersHeader.setVisibility(false);
        this.tabUtilLabelFunction([this.view.lblEntitlements,
            this.view.lblCustomersFix
        ], this.view.lblEntitlements);
        this.view.forceLayout();
    },
    showViewCustomers: function() {
        this.tabUtilLabelFunction([this.view.lblEntitlements,
            this.view.lblCustomersFix
        ], this.view.lblCustomersFix);
        this.view.flxEntitlementsHeader.setVisibility(false);
        this.view.flxCustomersHeader.setVisibility(true);
        this.view.flxSearchOptions.setVisibility(false);
        var groupId = this.getSelectedGroupId();
        var searchParam = this.getDefaultSearchParam();
        searchParam["_groupIDS"] = groupId;
        this.getCustomersOfGroup(searchParam, false);
    },
    showEntitlementDetails: function() {
        this.hideAll();
        this.view.flxEntitlementDetails.setVisibility(true);
        this.view.verticalTabsEntitlement.flxOption4.setVisibility(false);
        this.view.verticalTabsEntitlement.imgSelected1.setVisibility(true);
        this.view.verticalTabsEntitlement.imgSelected2.setVisibility(false);
        this.view.verticalTabsEntitlement.imgSelected3.setVisibility(false);
        this.view.flxRightEntitlementDetails.setVisibility(true);
        this.view.flxRightLimits.setVisibility(false);
        this.view.flxRightTransactionFee.setVisibility(false);
        this.view.breadcrumbs.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.CONFIGURE");
        this.view.flxBreadCrumbs.setVisibility(false);
    },
    toggleSwitch: function(switchWidget, flxDisableWidget) {
        if (switchWidget.selectedindex === 1) {
            //it is off
            flxDisableWidget.setVisibility(true);
        } else {
            flxDisableWidget.setVisibility(false);
        }
    },
    hideShowDetails: function(image, flex) {
        if (image.src === "downrow.png") {
            flex.setVisibility(false);
            image.src = "img_desc_arrow.png";
        } else {
            flex.setVisibility(true);
            image.src = "downrow.png";
        }
    },
    showLimits: function() {
        this.view.verticalTabsEntitlement.imgSelected1.setVisibility(false);
        this.view.verticalTabsEntitlement.imgSelected2.setVisibility(true);
        this.view.verticalTabsEntitlement.imgSelected3.setVisibility(false);
        this.view.flxRightEntitlementDetails.setVisibility(false);
        this.view.flxRightLimits.setVisibility(true);
        this.view.flxRightTransactionFee.setVisibility(false);
    },
    showTransactionFee: function() {
        var self = this;
        this.view.commonButtonsTransactionFee.btnNext.setVisibility(false);
        this.view.verticalTabsEntitlement.imgSelected1.setVisibility(false);
        this.view.verticalTabsEntitlement.imgSelected2.setVisibility(false);
        this.view.verticalTabsEntitlement.imgSelected3.setVisibility(true);
        this.view.flxRightEntitlementDetails.setVisibility(false);
        this.view.flxRightLimits.setVisibility(false);
        this.view.flxRightTransactionFee.setVisibility(true);
        var dataMap = {
            "flx1": "flx1",
            "flxDelete": "flxDelete",
            "flxFee": "flxFee",
            "flxFee1": "flxFee1",
            "flxFeeRange": "flxFeeRange",
            "flxMain": "flxMain",
            "flxMax1": "flxMax1",
            "flxMaxAmount": "flxMaxAmount",
            "flxMinAmount": "flxMinAmount",
            "imgDelete": "imgDelete",
            "lblDollar": "lblDollar",
            "lblDollar1": "lblDollar1",
            "lblDollar2": "lblDollar2",
            "lblFeeError": "lblFeeError",
            "lblMaximumTransactionError": "lblMaximumTransactionError",
            "lblMinimumTransactionError": "lblMinimumTransactionError",
            "lblSeparator": "lblSeparator",
            "lblSeparator2": "lblSeparator2",
            "lblSeparator3": "lblSeparator3",
            "segFeeRange": "segFeeRange",
            "txtbxAmount": "txtbxAmount",
            "txtbxFee": "txtbxFee",
            "txtbxMaxAmount": "txtbxMaxAmount"
        };
        var data = [{
            "flxMinAmount": {
                "skin": "skntxtbxNormald7d9e0"
            },
            "flxMaxAmount": {
                "skin": "skntxtbxNormald7d9e0"
            },
            "flxFee": {
                "skin": "skntxtbxNormald7d9e0"
            },
            "txtbxAmount": {
                "placeholder": "0.00"
            },
            "txtbxMaxAmount": {
                "placeholder": "0.00"
            },
            "txtbxFee": {
                "placeholder": "0.00"
            },
            "flxDelete": {
                "isVisible": true,
                "onClick": function() {
                    self.deleteRow();
                }
            },
            "lblSeparator3": ".",
            "lblSeparator2": ".",
            "lblSeparator1": ".",
            "flx1": {
                "skin": "sknflxNoneditable"
            },
            "flxMax1": {
                "skin": "sknflxNoneditable"
            },
            "flxFee1": {
                "skin": "sknflxNoneditable"
            },
            "lblDollar": "$",
            "lblDollar1": "$",
            "lblDollar2": "$",
            "imgDelete": {
                "src": "delete_2x.png",
                "isVisible": true
            },
            "template": "flxFeeRange"
        }, {
            "flxMinAmount": {
                "skin": "skntxtbxNormald7d9e0"
            },
            "flxMaxAmount": {
                "skin": "skntxtbxNormald7d9e0"
            },
            "flxFee": {
                "skin": "skntxtbxNormald7d9e0"
            },
            "txtbxAmount": {
                "placeholder": "0.00"
            },
            "txtbxMaxAmount": {
                "placeholder": "0.00"
            },
            "txtbxFee": {
                "placeholder": "0.00"
            },
            "flxDelete": {
                "isVisible": true,
                "onClick": function() {
                    self.deleteRow();
                }
            },
            "lblSeparator3": ".",
            "lblSeparator2": ".",
            "lblSeparator1": ".",
            "flx1": {
                "skin": "sknflxNoneditable"
            },
            "flxMax1": {
                "skin": "sknflxNoneditable"
            },
            "flxFee1": {
                "skin": "sknflxNoneditable"
            },
            "lblDollar": "$",
            "lblDollar1": "$",
            "lblDollar2": "$",
            "imgDelete": {
                "src": "delete_2x.png",
                "isVisible": true
            },
            "template": "flxFeeRange"
        }, {
            "flxMinAmount": {
                "skin": "skntxtbxNormald7d9e0"
            },
            "flxMaxAmount": {
                "skin": "skntxtbxNormald7d9e0"
            },
            "flxFee": {
                "skin": "skntxtbxNormald7d9e0"
            },
            "txtbxAmount": {
                "placeholder": "0.00"
            },
            "txtbxMaxAmount": {
                "placeholder": "0.00"
            },
            "txtbxFee": {
                "placeholder": "0.00"
            },
            "flxDelete": {
                "isVisible": true,
                "onClick": function() {
                    self.deleteRow();
                }
            },
            "lblSeparator3": ".",
            "lblSeparator2": ".",
            "lblSeparator1": ".",
            "flx1": {
                "skin": "sknflxNoneditable"
            },
            "flxMax1": {
                "skin": "sknflxNoneditable"
            },
            "flxFee1": {
                "skin": "sknflxNoneditable"
            },
            "lblDollar": "$",
            "lblDollar1": "$",
            "lblDollar2": "$",
            "imgDelete": {
                "src": "delete_2x.png",
                "isVisible": true
            },
            "template": "flxFeeRange"
        }];
        this.view.deleteRow.segFeeRange.widgetDataMap = dataMap;
        this.view.deleteRow.segFeeRange.setData(data);
        this.view.forceLayout();
    },
    deleteRow: function() {
        var data = this.view.deleteRow.segFeeRange.data;
        var Index = this.view.deleteRow.segFeeRange.selectedIndex;
        var rowIndex = Index[1];
        this.view.deleteRow.segFeeRange.removeAt(rowIndex);
        if (data.length === 1) {
            data[0].imgDelete.isVisible = false;
        }
    },
    searchFilter:   function (Group) {
        var  searchText  =  this.view.subHeader.tbxSearchBox.text;
        if (typeof  searchText  ===  'string'  &&  searchText.length  > 0) {
            return  Group.Group_Name.toLowerCase().indexOf(searchText.toLowerCase())  !==  -1;
        } else {
            return  true;
        }
    },
    /*
     * function to fetch the listView data of groups
     */
    getAllGroups: function() {
        var scopeObj = this;
        scopeObj.presenter.fetchCustomerGroups();
    },
    /*
     * function to save group details data for group part
     */
    storeGroupDetailsCreate: function() {
        var self = this;
        var statusInd = self.view.switchStatus.selectedIndex;
        var statusVal;
        if (statusInd === 0) {
            statusVal = self.statusConfig.active;
        } else {
            statusVal = self.statusConfig.inactive;
        }
        return {
            "Name": self.view.tbxGroupNameValue.text.trim(),
            "Description": self.view.txtGroupDescription.text.trim(),
            "Status_id": statusVal,
            "User_id": kony.mvc.MDAApplication.getSharedInstance().appContext.userID,
            "Type_id": self.view.lstBoxType.selectedKey,
            "isEAgreementActive": self.view.flxEAgreement.isVisible ? (self.view.switchEAgreement.selectedIndex === 0 ? 1 : 0) : 0,
            "Entitlements": [],
            "Customers": []
        };
    },
    /*
     * function called on click of save button  for group part
     * @param:isImport - true/false
     */
    saveGroupDetails: function(importReq) {
        var self = this;
        var reqParam = self.storeGroupDetailsCreate();
        self.presenter.createCustomerGroups(reqParam, importReq);
    },
    /*
     * function called to fetch all entitlements
     * @param: getForEdit - true - for edit group function
     *         getForEdit - false - for create group function
     *         assignFor -0-assignEnt:assignFor -1-assignCust:assignFor -3-other
     */
    getEntitlementsForEdit: function(param, context) {
        var self = this;
        self.groupId = param.entitlements.Group_id || "";
        this.assignEntitleData = [];
        this.newAssignedEntitle = [];
        this.gotEntitle = false;
        self.presenter.fetchGroupDataForEdit(param, context);
    },
    /*
     * function called to fetch  entitlements of particular group
     * @param : groupid
     */
    getEntitlementsForGroup: function(groupId) {
        var self = this;
        self.presenter.fetchEntitlementsOfGroup(groupId);
    },
    /*
     * function called to fetch  Customers of particular group
     * @param : groupid
     */
    getCustomersOfGroup: function(groupId, isEdit) {
        var self = this;
        self.presenter.fetchCustomersOfGroup(groupId, isEdit);
    },
    /*
     * function to get difference of two sets.
     */
    updatedIds: function(a1, a2) {
        return a1.filter(function(x) {
            if (a2.indexOf(x) >= 0) return false;
            else return true;
        });
    },
    /*
     * function to format the entitlements to unSelected and Selected segments for edit
     * @param: allEntitle-all the entitlements
     * @param : assignedEntitle - entitlements assgined to that group
     */
    formatEntitlementsForEdit: function(allEntitle, assignedEntitle) {
        var self = this;
        var unSelectedId = [];
        var selectedId = [];
        if (allEntitle) {
            for (var i = 0; i < allEntitle.length; i++) {
                unSelectedId.push(allEntitle[i].id);
            }
        }
        if (assignedEntitle !== null) {
            for (var j = 0; j < assignedEntitle.length; j++) {
                selectedId.push(assignedEntitle[j].Service_id);
            }
        }
        var unSelectedListId = self.updatedIds(unSelectedId, selectedId);
        var unSelectedList = allEntitle.filter(function(ent) {
            for (var i = 0; i < unSelectedListId.length; i++) {
                if (unSelectedListId[i] === ent.id) return ent;
            }
        });
        self.allEntitleData = unSelectedList;
        self.assignEntitleData = assignedEntitle;
        self.showAssignEntitlements(unSelectedList, assignedEntitle);
    },
    /*
     * function called to set data to entitlement segment in group view
     */
    showEntitlementsOfGroup: function(entitlements) {
        var self = this;
        //set Entitlements data
        var dataMap = {
            "flxEntitlementsConfigure": "flxEntitlementsConfigure",
            "flxViewEntitlements": "flxViewEntitlements",
            "Group_id": "Group_id",
            "Service_id": "Service_id",
            "TransactionFee_id": "TransactionFee_id",
            "TransactionLimit_id": "TransactionLimit_id",
            "imgConfigure": "imgConfigure",
            "lblEntitlementsDescription": "lblEntitlementsDescription",
            "lblEntitlementsName": "lblEntitlementsName",
            "lblEntitlementsSeperator": "lblEntitlementsSeperator",
        };
        if (entitlements.length > 0) {
            var data2 = entitlements.map(function(assgEnt) {
                return {
                    "imgConfigure": {
                        "src": "configure2x.png"
                    },
                    "Group_id": assgEnt.Group_id,
                    "Service_id": assgEnt.Service_id,
                    "TransactionFee_id": self.checkForUndefined(assgEnt.TransactionFee_id),
                    "TransactionLimit_id": self.checkForUndefined(assgEnt.TransactionLimit_id),
                    "lblEntitlementsDescription": {
                        "text": assgEnt.Description
                    },
                    "lblEntitlementsName": {
                        "text": assgEnt.Name
                    },
                    "lblEntitlementsSeperator": {
                        "text": "",
                        "isVisible": true
                    },
                    "template": "flxViewEntitlements"
                };
            });
            self.view.flxNoResultMessage.setVisibility(false);
            self.view.flxEntitlementsHeader.setVisibility(true);
            self.view.segViewSegment.setVisibility(true);
            if (data2.length > 0) data2[0].lblEntitlementsSeperator.isVisible = false;
            self.view.segViewSegment.widgetDataMap = dataMap;
            self.view.segViewSegment.setData(data2);
            document.getElementById("frmGroups_segViewSegment").onscroll = null;
            self.view.forceLayout();
        } else {
            self.view.flxEntitlementsHeader.setVisibility(false);
            self.view.segViewSegment.setVisibility(false);
            self.view.rtxNoRecordsAvailable.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.No_Entitlements_Available");
            self.view.flxNoResultMessage.setVisibility(true);
        }
        self.view.forceLayout();
    },
    /*
     * function to set data of customers assigned to group in Group View
     */
    showCustomersOfGroup: function(customerModel) {
        var self = this;
        //set Customers data
        var dataMap = {
            "flxViewUsers": "flxViewUsers",
            "lblViewEmailId": "lblViewEmailId",
            "lblViewFullName": "lblViewFullName",
            "lblViewSeperator": "lblViewSeperator",
            "lblViewUpdatedBy": "lblViewUpdatedBy",
            "lblViewUpdatedDate": "lblViewUpdatedDate",
            "lblViewUpdatedTime": "lblViewUpdatedTime",
            "lblViewUsername": "lblViewUsername",
        };
        var customers = customerModel.records;
        var sortColumnIcon = self.viewCustomersSegColumns.frontendIcons[self.viewCustomersSegColumns.backendNames.indexOf(customerModel.SortVariable)];
        self.resetSortImages("viewgroup", sortColumnIcon);
        if (customerModel.SortDirection === "ASC") {
            self.view["" + sortColumnIcon].text = self.statusFontIcon.ASCENDING_IMAGE;
            self.view["" + sortColumnIcon].skin = self.statusFontIcon.ASCENDING_SKIN;
        } else {
            self.view["" + sortColumnIcon].text = self.statusFontIcon.DESCENDING_IMAGE;
            self.view["" + sortColumnIcon].skin = self.statusFontIcon.DESCENDING_SKIN;
        }
        var data2 = customers.map(function(assgCust) {
            return {
                "lblViewEmailId": assgCust.PrimaryEmail || kony.i18n.getLocalizedString("i18n.Applications.NA"),
                "lblViewFullName": assgCust.name || kony.i18n.getLocalizedString("i18n.Applications.NA"),
                "lblViewSeperator": {
                    "text": "",
                    "isVisible": true
                },
                "lblViewUpdatedBy": assgCust.modifiedby || kony.i18n.getLocalizedString("i18n.Applications.NA"),
                "lblViewUpdatedDate": self.getLocaleDateAndTime(self.getDateInstanceFromDBDateTime(assgCust.lastmodifiedts)) || kony.i18n.getLocalizedString("i18n.Applications.NA"),
                "lblViewUpdatedTime": "",
                "Customer_id": assgCust.id,
                "lblViewUsername": assgCust.Username || kony.i18n.getLocalizedString("i18n.Applications.NA"),
                "template": "flxViewUsers"
            };
        });
        if (customerModel.PageOffset === 0) {
            self.view.flxNoResultMessage.setVisibility(false);
            self.view.flxCustomersHeader.setVisibility(true);
            self.view.segViewSegment.setVisibility(true);
            if (data2.length > 0) data2[0].lblViewSeperator.isVisible = false;
            self.view.segViewSegment.widgetDataMap = dataMap;
            self.view.segViewSegment.setData(data2);
            self.view.flxViewSegment.setContentOffset({
                x: 0,
                y: 3 + "px"
            });
        } else {
            var heigthFlex = self.view.flxViewSegment.frame.height;
            self.view.segViewSegment.setData(data2);
            self.view.flxViewSegment.setContentOffset({
                x: 0,
                y: (self.segmentRecordsSize * 20) + "px"
            });
        }
        self.getMoreDataModel = {
            "PageOffset": customerModel.PageOffset,
            "PageSize": customerModel.PageSize,
            "TotalResultsFound": customerModel.TotalResultsFound,
            "RecordsOnPage": self.view.segViewSegment.data.length,
            "SortVariable": customerModel.SortVariable,
            "SortDirection": customerModel.SortDirection,
        };
        document.getElementById("frmGroups_flxViewSegment").onscroll = self.getResultsOnReachingend;
        if (customerModel.PageOffset === 0 && data2.length <= 0) {
            self.view.rtxNoRecordsAvailable.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.No_Customers_Available");
            self.view.flxNoResultMessage.setVisibility(true);
            self.view.flxCustomersHeader.setVisibility(false);
            self.view.segViewSegment.setVisibility(false);
        } else {
            self.view.flxNoResultMessage.setVisibility(false);
            self.view.flxCustomersHeader.setVisibility(true);
            self.view.segViewSegment.setVisibility(true);
        }
        self.view.forceLayout();
    },
    /*
     * function to get the new entitlements assigned for create group
     */
    stroreEntitlementForCreate: function() {
        var self = this;
        var selectedOptions = [];
        var prevData = self.storeGroupDetailsCreate();
        var data = this.view.addAndRemoveOptions.segSelectedOptions.data;
        for (var i = 0; i < data.length; i++) {
            selectedOptions.push({
                "Service_id": data[i].Service_id,
                "TransactionFee_id": self.checkForUndefined(data[i].TransactionFee_id),
                "TransactionLimit_id": self.checkForUndefined(data[i].TransactionLimit_id)
            });
        }
        prevData.Entitlements = selectedOptions;
        self.createRequestParam = prevData;
    },
    /*
     * function called on click of FINAL SAVE button for create group
     * @param: importReq={isImport:true/false,fileVal:""}
     */
    createNewGroup: function(importReq) {
        var self = this;
        var selectedOptions = [];
        var segdata = self.view.segAssignedCustomers.data;
        for (var i = 0; i < segdata.length; i++) {
            selectedOptions.push({
                "Customer_id": segdata[i].lblCustID.info.text
            });
        }
        self.createRequestParam.Customers = selectedOptions;
        self.presenter.createCustomerGroups(self.createRequestParam, importReq);
    },
    /*
     * function called on click of SAVE from entitlements for create group
     * @param: importReq={isImport:true/false,fileVal:""}
     */
    clickSaveFromEnt: function(importReq) {
        var self = this;
        self.stroreEntitlementForCreate();
        self.presenter.createCustomerGroups(self.createRequestParam, importReq);
    },
    /*
     * function called on click of SAVE from entitlements for update/edit
     */
    updateFromEntitlements: function(groupId) {
        var self = this;
        var orgEntitlements = self.orgEntitlementList;
        var removed = [];
        var added = [];
        if (orgEntitlements !== null) {
            for (var i = 0; i < orgEntitlements.length; i++) {
                var temp = orgEntitlements[i];
                if (temp.isRemove) {
                    var removeElem = {
                        "Group_id": groupId,
                        "Service_id": temp.Service_id
                    };
                    removed.push(removeElem);
                }
            }
        }
        self.editRequestParam.removedEntitlementIds = removed;
        self.editRequestParam.addedEntitlementIds = this.newAssignedEntitle;
    },
    /*
     * function called on click of SAVE from customers for update/edit
     */
    updateFromCustomer: function(groupId) {
        var self = this;
        var addedCustId = [];
        var addedCust = self.custToAdd;
        var deletedCust = self.deletedCustomerId;
        //creating array of id
        for (var i = 0; i < addedCust.length; i++) {
            addedCustId.push(addedCust[i].lblCustID.info.text);
        }
        //to remove any deleted id's from added list
        var addedId = self.updatedIds(addedCustId, deletedCust);
        var removedIdList = deletedCust.map(function(cust) {
            return {
                "Customer_id": cust
            };
        });
        var addedIdList = addedId.map(function(cust) {
            return {
                "Customer_id": cust
            };
        });
        self.editRequestParam.removedCustomerIds = removedIdList;
        self.editRequestParam.addedCustomerIds = addedIdList;
    },
    /*
     * function to store the edited details of group 
     */
    storeGroupDetailsEdit: function() {
        var self = this;
        var index = self.view.listingSegmentClient.segListing.selectedIndex;
        var rowIndex = index[1];
        var rowData = self.view.listingSegmentClient.segListing.data[rowIndex];
        var status = self.statusConfig.active;
        if (self.view.switchStatus.selectedIndex === 0) {
            status = self.statusConfig.active;
        } else {
            status = self.statusConfig.inactive;
        }
        var reqParam = {
            "Group_id": rowData.Group_id,
            "Status_id": status,
            "Name": self.view.tbxGroupNameValue.text.trim(),
            "Description": self.view.txtGroupDescription.text.trim(),
            "Type_id": self.view.lstBoxType.selectedKey,
            "isEAgreementActive": self.view.flxEAgreement.isVisible ? (self.view.switchEAgreement.selectedIndex === 0 ? 1 : 0) : 0,
            "User_id": kony.mvc.MDAApplication.getSharedInstance().appContext.userID,
            "removedEntitlementIds": [],
            "addedEntitlementIds": [],
            "removedCustomerIds": [],
            "addedCustomerIds": [],
            "IsRemoveAll": false
        };
        self.editRequestParam = reqParam;
    },
    /*
     * function to get data that is set
     */
    getGroupSetData: function() {
        var self = this;
        var statusVal;
        var index = self.view.listingSegmentClient.segListing.selectedIndex;
        var rowIndex = index[1];
        var rowData = self.view.listingSegmentClient.segListing.data[rowIndex];
        self.editRequestParam = {
            "Name": rowData.lblGroupName.text,
            "Description": rowData.lblHeaderGroupDescription.text,
            "Status_id": rowData.Status_id,
            "Type_id": rowData.Type_id,
            "isEAgreementActive": rowData.isEAgreementActive,
        };
        var data = {
            "Name": self.editRequestParam.Name,
            "Description": self.editRequestParam.Description,
            "Status": self.editRequestParam.Status_id,
            "groupId": rowData.Group_id,
            "orginalName": rowData.lblGroupName.text,
            "Type_id": self.editRequestParam.Type_id,
            "isEAgreementActive": self.editRequestParam.isEAgreementActive,
        };
        return data;
    },
    /*
     * function to prefill data on click of edit option
     */
    fillDataForEdit: function(data) {
        var self = this;
        var statusVal = 0,
            eAgreement = 0;
        self.view.flxBreadCrumbs.setVisibility(false);
        self.view.breadcrumbs.lblCurrentScreen.setVisibility(true);
        self.view.breadcrumbs.lblCurrentScreen.text = data.Name.toUpperCase();
        self.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.GROUPS");
        self.view.tbxGroupNameValue.text = data.Name;
        self.view.txtGroupDescription.text = data.Description;
        self.view.lblGroupDescriptionCount.text = self.view.txtGroupDescription.text.length + "/250";
        self.view.lblGroupNameCount.text = self.view.tbxGroupNameValue.text.length + "/50";
        self.view.lstBoxType.selectedKey = data.Type_id;
        self.hideUIBasedOnRoleType();
        eAgreement = data.isEAgreementActive;
        statusVal = data.Status;
        if (statusVal === 0 || statusVal === self.statusConfig.active) {
            self.view.switchStatus.selectedIndex = 0;
        } else {
            self.view.switchStatus.selectedIndex = 1;
        }
        if (eAgreement === 0) {
            self.view.switchEAgreement.selectedIndex = 1;
        } else {
            self.view.switchEAgreement.selectedIndex = 0;
        }
    },
    /*
     * function called to activate/deactivate a group
     */
    changeStatusOfGroup: function() {
        var self = this;
        var index = self.view.listingSegmentClient.segListing.selectedIndex;
        var rowIndex = index[1];
        var rowData = self.view.listingSegmentClient.segListing.data[rowIndex];
        var currStatus = rowData.Status_id;
        var status;
        if (currStatus === self.statusConfig.active) {
            status = self.statusConfig.inactive;
        } else {
            status = self.statusConfig.active;
        }
        var reqParam = {
            "Group_id": rowData.Group_id,
            "Status_id": status,
            "User_id": kony.mvc.MDAApplication.getSharedInstance().appContext.userID
        };
        self.presenter.updateStatusOfCustomerGroups(reqParam);
    },
    /*
     * function called on click of download list to download the CSV file
     */
    downloadGroupsCSV: function() {
        kony.print("Inside downloadGroupsCSV() of frmGroupsController");
        var scopeObj = this;
        var authToken = KNYMobileFabric.currentClaimToken;
        var mfURL = KNYMobileFabric.mainRef.config.reportingsvc.session.split("/services")[0];
        var downloadURL = mfURL + "/services/data/v1/CustomerGroupsAndEntitlObjSvc/operations/Group/downloadGroupsList?authToken=" + authToken;
        if (scopeObj.view.subHeader.tbxSearchBox.text !== "") {
            downloadURL = downloadURL + "&searchText=" + scopeObj.view.subHeader.tbxSearchBox.text;
        }
        var downloadServicesFilterJSON = scopeObj.view.mainHeader.btnDropdownList.info;
        if (downloadServicesFilterJSON !== undefined && downloadServicesFilterJSON.selectedStatusList !== undefined) {
            var status = "&status=" + downloadServicesFilterJSON.selectedStatusList;
            downloadURL = downloadURL + status;
        }
        var encodedURI = encodeURI(downloadURL);
        var downloadLink = document.createElement("a");
        downloadLink.href = encodedURI;
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    },
    /*
     * function called on click of save to update edited data
     */
    callUpdate: function(reqParams, importReq) {
        var self = this;
        self.presenter.updateCustomerGroups(reqParams, importReq);
    },
    /*
     * function for internal search in entitlements while create/edit
     */
    doSearchInEntitlements: function(searchText) {
        var self = this;
        var searchRes;
        var orgSegData = this.view.addAndRemoveOptions.segAddOptions.info.segData;
        if (searchText !== "") {
            var requiredData = function(entData) {
                return entData.lblName.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
            };
            searchRes = orgSegData.filter(requiredData);
            this.records = searchRes.length;
            if (this.records === 0) {
                self.view.addAndRemoveOptions.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.No_results_found_with_parameters") + searchText + kony.i18n.getLocalizedString("i18n.frmGroupsController.Try_with_another_keyword");
                self.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(true);
                self.view.addAndRemoveOptions.segAddOptions.setVisibility(false);
                self.view.addAndRemoveOptions.btnSelectAll.setVisibility(false);
            } else {
                self.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(false);
                self.view.addAndRemoveOptions.segAddOptions.setVisibility(true);
                self.view.addAndRemoveOptions.segAddOptions.setData(searchRes);
                self.view.addAndRemoveOptions.btnSelectAll.setVisibility(true);
            }
            self.view.forceLayout();
        } else {
            if (orgSegData.length === 0) {
                self.view.addAndRemoveOptions.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.No_Entitlements_Available");
                self.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(true);
                self.view.addAndRemoveOptions.btnSelectAll.setVisibility(false);
            } else {
                self.view.addAndRemoveOptions.rtxAvailableOptionsMessage.setVisibility(false);
                self.view.addAndRemoveOptions.segAddOptions.setVisibility(true);
                self.view.addAndRemoveOptions.btnSelectAll.setVisibility(true);
                this.view.addAndRemoveOptions.segAddOptions.setData(orgSegData);
            }
        }
    },
    /*
     * function to validate name,description in group details page
     */
    checkGroupDetailsValidation: function() {
        var self = this;
        var isValid = false;
        var groupName = self.view.tbxGroupNameValue.text.trim();
        var groupDesc = self.view.txtGroupDescription.text.trim();
        var selectedItem = self.view.lstBoxType.selectedKey;
        if (groupName === "" || groupDesc === "" || selectedItem === "select") {
            isValid = false;
            if (groupName === "") {
                self.view.tbxGroupNameValue.skin = "skntbxBordereb30173px";
                self.view.flxNoGroupNameError.setVisibility(true);
            }
            if (groupDesc === "") {
                self.view.txtGroupDescription.skin = "skinredbg";
                self.view.flxNoGroupDescriptionError.setVisibility(true);
            }
            if (selectedItem === "select") {
                self.view.lstBoxType.skin = "redListBxSkin";
                self.view.flxTypeError.setVisibility(true);
            }
            return isValid;
        } else {
            isValid = true;
            self.view.tbxGroupNameValue.skin = "skntbxLato35475f14px";
            self.view.txtGroupDescription.skin = "skntxtAreaLato35475f14Px";
            self.view.lstBoxType.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
            self.view.flxNoGroupNameError.setVisibility(false);
            self.view.flxNoGroupDescriptionError.setVisibility(false);
            self.view.flxTypeError.setVisibility(false);
            return isValid;
        }
    },
    /* 
     * function to set the search results data to segment
     * @param : search response
     */
    setSearchResultSegment: function(response) {
        var scopeObj = this;
        var dataMap = {
            "btnAdd": "btnAdd",
            "flxCheckboxHidden": "flxCheckboxHidden",
            "imgCheckBoxHidden": "imgCheckBoxHidden",
            "flxCheckbox": "flxCheckbox",
            "flxGroupsAdvSearch": "flxGroupsAdvSearch",
            "flxGroupSearch": "flxGroupSearch",
            "flxStatus": "flxStatus",
            "imgCheckBox": "imgCheckBox",
            "fontIconCustomerStatus": "fontIconCustomerStatus",
            "lblBranch": "lblBranch",
            "lblCity": "lblCity",
            "lblCustID": "lblCustID",
            "lblName": "lblName",
            "lblSeperator": "lblSeperator",
            "lblServicesStatus": "lblServicesStatus",
            "lblUserName": "lblUserName",
            "lblAdded": "lblAdded"
        };
        this.resetAllSortImagesAdvSearchResults();
        var sortColumnIcon = this.customersSegmentColumns.frontendIcons[this.customersSegmentColumns.backendNames.indexOf(response.SortVariable)];
        if (response.SortDirection === "ASC") {
            this.view["" + sortColumnIcon].text = kony.adminConsole.utils.fonticons.ASCENDING_IMAGE;
        } else {
            this.view["" + sortColumnIcon].text = kony.adminConsole.utils.fonticons.DESCENDING_IMAGE;
        }
        var currGroup = this.getSelectedGroupId();
        var btnAddJSON, checkboxJSON;
        var segDataJSON = "";
        var statusText = "";
        var statusSkin, statusImgSkin, addedCount = 0;
        var data = response.records.map(function(custObj) {
            if (custObj.Status_id === scopeObj.statusConfig.custActive) {
                statusText = kony.i18n.getLocalizedString("i18n.secureimage.Active");
                statusSkin = "sknlblLato5bc06cBold14px";
                statusImgSkin = "sknFontIconActivate";
            } else if (custObj.Status_id === scopeObj.statusConfig.custLocked) {
                statusText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Locked");
                statusSkin = "sknlblLatocacacaBold12px";
                statusImgSkin = "sknfontIconInactive";
            } else {
                statusText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Suspended");
                statusSkin = "sknlblLatoeab55d12px";
                statusImgSkin = "sknFontIconSuspend";
            }
            segDataJSON = {
                "btnAdd": {
                    "text": kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add"),
                    "skin": "sknBtnLatoRegular11abeb14px",
                    "isVisible": true,
                    "onClick": function() {
                        var self = this;
                        var selIndex = scopeObj.view.segCustomers.selectedIndex;
                        if (selIndex === null) {} else {
                            var rowData = scopeObj.view.segCustomers.data[selIndex[1]];
                            rowData.btnAdd.isVisible = false;
                            rowData.flxCheckboxHidden.isVisible = true;
                            rowData.imgCheckBoxHidden.isVisible = true;
                            rowData.flxCheckbox.isVisible = false;
                            rowData.lblAdded.isVisible = true;
                            scopeObj.view.segCustomers.setDataAt(rowData, selIndex[1]);
                            scopeObj.advSearchResultOnClickOfAdd(rowData);
                            var selRowInd = scopeObj.view.segCustomers.selectedRowIndices;
                            if (selRowInd && selRowInd[0][1].length > 0) {
                                //do-nothing
                            } else {
                                //hide add-close buttons
                                scopeObj.view.flxSelected.setVisibility(false);
                                scopeObj.view.imgHeaderCheckBox.src = "checkbox.png";
                                scopeObj.view.btnUnselect.onClick();
                            }
                        }
                    }
                },
                "flxCheckbox": {
                    "isVisible": true
                },
                "flxCheckboxHidden": {
                    "isVisible": false
                },
                "imgCheckBoxHidden": {
                    "isVisible": false
                },
                "imgCheckBox": "checkbox.png",
                "fontIconCustomerStatus": {
                    "skin": statusImgSkin
                },
                "lblBranch": (custObj.branch_name === "" ? kony.i18n.getLocalizedString("i18n.Applications.NA") : custObj.branch_name) || kony.i18n.getLocalizedString("i18n.Applications.NA"),
                "lblCity": (custObj.City_name === "" ? kony.i18n.getLocalizedString("i18n.Applications.NA") : custObj.City_name) || kony.i18n.getLocalizedString("i18n.Applications.NA"),
                "lblCustID": {
                    "tooltip": custObj.id,
                    "info": {
                        "id": custObj.id
                    },
                    "text": (custObj.id.trim() === "" ? kony.i18n.getLocalizedString("i18n.Applications.NA") : scopeObj.AdminConsoleCommonUtils.getTruncatedString(custObj.id, 15, 13)) || kony.i18n.getLocalizedString("i18n.Applications.NA"),
                },
                "lblName": custObj.name.trim() === "" ? kony.i18n.getLocalizedString("i18n.Applications.NA") : custObj.name,
                "lblSeperator": ".",
                "lblServicesStatus": {
                    "text": statusText,
                    "skin": statusSkin
                },
                "lblUserName": {
                    "tooltip": custObj.Username,
                    "info": {
                        "id": custObj.Username
                    },
                    "text": (custObj.Username.trim() === "" ? kony.i18n.getLocalizedString("i18n.Applications.NA") : scopeObj.AdminConsoleCommonUtils.getTruncatedString(custObj.Username, 15, 13)) || kony.i18n.getLocalizedString("i18n.Applications.NA"),
                },
                "lblAdded": {
                    "text": kony.i18n.getLocalizedString("i18n.frmGroupsController.Added"),
                    "isVisible": false
                },
                "template": "flxGroupsAdvSearch"
            };
            if (custObj.assigned_group_ids) {
                var groupArr = custObj.assigned_group_ids.split(",");
                if (groupArr.indexOf(currGroup) >= 0) {
                    segDataJSON.btnAdd.isVisible = false;
                    segDataJSON.flxCheckbox.isVisible = false;
                    segDataJSON.flxCheckboxHidden.isVisible = true;
                    segDataJSON.imgCheckBoxHidden.isVisible = true;
                    segDataJSON.lblAdded.isVisible = true;
                    addedCount = addedCount + 1;
                }
            }
            if (scopeObj.custToAdd.length !== 0) {
                for (var i = 0; i < scopeObj.custToAdd.length; i++) {
                    if (custObj.id === scopeObj.custToAdd[i].lblCustID.info.text) {
                        segDataJSON.btnAdd.isVisible = false;
                        segDataJSON.flxCheckbox.isVisible = false;
                        segDataJSON.flxCheckboxHidden.isVisible = true;
                        segDataJSON.imgCheckBoxHidden.isVisible = true;
                        segDataJSON.lblAdded.isVisible = true;
                        addedCount = addedCount + 1;
                        break;
                    }
                }
            }
            return segDataJSON;
        });
        if (response.PageOffset === 0) {
            if (response.records !== null && response.records.length > 0) {
                scopeObj.view.flxSearchResultPage.setVisibility(true);
                scopeObj.view.flxSearchNoResultPage.setVisibility(false);
                scopeObj.view.btnModifySearchNoResult.setVisibility(true);
                scopeObj.view.flxSelected.setVisibility(false);
                scopeObj.view.flxSearchFilters.isVisible = false;
                scopeObj.searchResult = data;
                scopeObj.view.segCustomers.widgetDataMap = dataMap;
                scopeObj.view.segCustomers.setData(data);
                //tags visibility
                scopeObj.view.lblIconArrow.text = "\ue915"; //downArrow
                scopeObj.view.lblIconArrow.skin = "sknfontIconDescDownArrow12px";
                scopeObj.setAdvancedSearchFlexHeight();
                scopeObj.view.flxSearchFilter.setVisibility(true);
                //adjusting header and segment row alignment based on scrollbar visibility
                if (scopeObj.view.segCustomers.data.length > 5 && scopeObj.view.flxSearchFilter.isVisible === true) {
                    scopeObj.view.flxGroupSegHeader.right = "36px";
                } else if (scopeObj.view.segCustomers.data.length > 6 && scopeObj.view.flxSearchFilter.isVisible === true) {
                    scopeObj.view.flxGroupSegHeader.right = "36px";
                } else {
                    scopeObj.view.flxGroupSegHeader.right = "20px";
                }
                this.loadMoreModel = {
                    "TotalResultsFound": response.TotalResultsFound,
                    "RecordsOnPage": scopeObj.view.segCustomers.data.length,
                    "SortVariable": response.SortVariable,
                    "SortDirection": response.SortDirection
                };
                scopeObj.view.lblCustSearchCount.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Displaying") + scopeObj.loadMoreModel.RecordsOnPage + " Of " + scopeObj.loadMoreModel.TotalResultsFound + " " + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers");
            } else {
                scopeObj.view.flxSearchResultPage.setVisibility(false);
                scopeObj.view.flxSearchNoResultPage.setVisibility(true);
                scopeObj.view.btnModifySearchNoResult.setVisibility(false);
                scopeObj.view.flxSelected.setVisibility(false);
                scopeObj.view.flxSearchFilters.isVisible = false;
            }
        } else {
            for (var i = 0; i < data.length; i++) {
                scopeObj.view.segCustomers.addDataAt(data[i], scopeObj.view.segCustomers.data.length);
            }
            scopeObj.loadMoreModel.RecordsOnPage = scopeObj.view.segCustomers.data.length;
            scopeObj.view.lblCustSearchCount.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Displaying") + scopeObj.loadMoreModel.RecordsOnPage + " Of " + scopeObj.loadMoreModel.TotalResultsFound + " " + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers");
        }
        //set checkbox disable or normal based on records available to select
        if (addedCount === scopeObj.view.segCustomers.data.length) {
            scopeObj.view.imgHeaderCheckBox.src = "checkboxdisable.png";
            scopeObj.view.flxCheckbox.setEnabled(false);
        } else {
            scopeObj.view.imgHeaderCheckBox.src = "checkbox.png";
            scopeObj.view.flxCheckbox.setEnabled(true);
        }
        scopeObj.view.lblCustSearchCount.setVisibility(true);
        scopeObj.hideAdvSearchLoadingScreen();
        kony.adminConsole.utils.hideProgressBar(this.view);
        document.getElementById("frmGroups_flxResultSeg").onscroll = scopeObj.populateResultsOnReachingend;
        this.view.forceLayout();
    },
    resetAllSortImagesAdvSearchResults: function() {
        if (this.view.fontIconSearchNameSort.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fontIconSearchNameSort.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fontIconSearchNameSort.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fontIconSearchUsernameSort.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fontIconSearchUsernameSort.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fontIconSearchUsernameSort.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fontIconSearchCustIdSort.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fontIconSearchCustIdSort.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fontIconSearchCustIdSort.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fontIconSearchBranchSort.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fontIconSearchBranchSort.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fontIconSearchBranchSort.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fontIconSearchCitySort.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fontIconSearchCitySort.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fontIconSearchCitySort.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
        if (this.view.fontIconSearchStatusSort.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
            this.view.fontIconSearchStatusSort.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
            this.view.fontIconSearchStatusSort.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
        }
    },
    populateResultsOnReachingend: function(context) {
        var self = this;
        var segmentWidget = self.view.segCustomers;
        if (Math.ceil(context.currentTarget.offsetHeight) + Math.floor(context.currentTarget.scrollTop) === Math.ceil(context.currentTarget.scrollHeight)) {
            if (self.loadMoreModel.TotalResultsFound > self.loadMoreModel.RecordsOnPage) {
                self.showAdvSearchLoadingScreen();
                self.removeAllTags();
                limit = self.loadMoreModel.TotalResultsFound - self.loadMoreModel.RecordsOnPage > self.limitForPaginationSearch ? self.limitForPaginationSearch : self.loadMoreModel.TotalResultsFound - self.loadMoreModel.RecordsOnPage;
                searchParam = self.getAdvancedSearchParameters();
                searchParam["_pageOffset"] = self.loadMoreModel.RecordsOnPage;
                searchParam["_pageSize"] = limit;
                searchParam["_sortVariable"] = self.loadMoreModel.SortVariable;
                searchParam["_sortDirection"] = self.loadMoreModel.SortDirection;
                self.presenter.searchCustomers(searchParam);
                var selInd = [];
                //to show rows selected for currently adding data on scroll
                if (self.view.imgHeaderCheckBox.src === "checkboxselected.png") {
                    for (var ind = 0; ind < segmentWidget.data.length; ind++) {
                        selInd.push(ind);
                    }
                    segmentWidget.selectedIndices = [
                        [0, selInd]
                    ];
                }
            }
        }
    },
    /*
     * function for widget data map of assigned customers segment
     */
    setAssignedCustomerSegDataMap: function() {
        var scopeObj = this;
        var dataMap = {
            "flxAssignedCustomers": "flxAssignedCustomers",
            "flxCheckbox": "flxCheckbox",
            "flxDelete": "flxDelete",
            "flxGroupsAssignedCustomers": "flxGroupsAssignedCustomers",
            "flxStatus": "flxStatus",
            "imgCheckBox": "imgCheckBox",
            "fontIconDelete": "fontIconDelete",
            "fontIconCustomerStatus": "fontIconCustomerStatus",
            "lblCustID": "lblCustID",
            "lblName": "lblName",
            "lblSeperator": "lblSeperator",
            "lblServicesStatus": "lblServicesStatus",
            "lblUserName": "lblUserName",
            "Status_id": "Status_id",
            "lblRemoved": "lblRemoved"
        };
        this.view.segAssignedCustomers.widgetDataMap = dataMap;
    },
    selectAllCustomer: function() {
        var data = this.view.segAssignedCustomers.data;
        var limit = data.length;
        var indices = [
            [0, []]
        ];
        for (var i = 0; i < limit; i++) {
            indices[0][1].push(i);
        }
        this.view.segAssignedCustomers.selectedRowIndices = indices;
    },
    unselectAllCustomer: function() {
        this.view.segAssignedCustomers.selectedRowIndices = null;
    },
            
    selectAllRows: function() {
        var data = this.view.segCustomers.data;
        var limit = data.length;
        var indices = [
            [0, []]
        ];
        for (var i = 0; i < limit; i++) {
            indices[0][1].push(i);
        }
        this.view.segCustomers.selectedIndices = indices;
        this.view.lblCustSearchCount.text = data.length + " " + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers_Selected");
    },
    unSelectAllRows: function() {
        this.view.segCustomers.selectedIndices = null;
    },
    /*
     * function called on click of save from customers advance search
     */
    onClickSaveAdvSearch: function() {
        var self = this;
        self.showAdvSearchLoadingScreen();
        self.removeAllTags();
        var searchReqParam;
        this.view.flxFilter1.setVisibility(false);
        this.view.flxFilter2.setVisibility(false);
        this.view.flxFilter3.setVisibility(false);
        self.view.imgHeaderCheckBox.src = "checkbox.png";
        self.view.flxCheckbox.setEnabled(true);
        searchReqParam = this.getAdvancedSearchParameters();
        searchReqParam["_pageOffset"] = "0";
        searchReqParam["_pageSize"] = this.limitForPaginationSearch;
        searchReqParam["_sortVariable"] = this.customersSegmentColumns.backendNames[0];
        searchReqParam["_sortDirection"] = "ASC";
        self.view.flxResultSeg.setContentOffset({
            y: 0,
            x: 0
        });
        self.presenter.searchCustomers(searchReqParam);
        self.view.forceLayout();
    },
    getAdvancedSearchParameters: function() {
        var self = this;
        var beforeDate = "",
            afterDate = "";
        var status = "",
            statusText = "";
        var branches = [],
            products = [],
            entitlements = [],
            groups = [],
            cities = [];
        branches = (self.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.info && self.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.info.selectedData) ? self.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.info.selectedData.map(function(record) {
            self.addTag(record.lblDescription.text, "br" + record.id);
            return record.id;
        }) : [];
        products = (self.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.info && self.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.info.selectedData) ? self.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.info.selectedData.map(function(record) {
            self.addTag(record.lblDescription.text, "pr" + record.id);
            return record.id;
        }) : [];
        entitlements = (self.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.info && self.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.info.selectedData) ? self.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.info.selectedData.map(function(record) {
            self.addTag(record.lblDescription.text, "en" + record.id);
            return record.id;
        }) : [];
        groups = (self.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.info && self.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.info.selectedData) ? self.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.info.selectedData.map(function(record) {
            self.addTag(record.lblDescription.text, "gr" + record.id);
            return record.id;
        }) : [];
        cities = (self.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.info && self.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.info.selectedData) ? self.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.info.selectedData.map(function(record) {
            self.addTag(record.lblDescription.text, "ci" + record.id);
            return record.id;
        }) : [];
        //DATES
        if (self.view.AdvancedSearch.imgBefore.src === "radio_selected.png") {
            if (self.view.customCalGroupBefore.value === "") {
                beforeDate = "";
            } else {
                var beforeDateArray = (self.view.customCalGroupBefore.value).split("/");
                beforeDate = beforeDateArray[2] + "-" + beforeDateArray[0] + "-" + beforeDateArray[1];
            }
            afterDate = "";
        } else if (self.view.AdvancedSearch.imgAfter.src === "radio_selected.png") {
            if (self.view.customCalGroupAfter.value === "") {
                afterDate = "";
            } else {
                var afterDateArray = (self.view.customCalGroupAfter.value).split("/");
                afterDate = afterDateArray[2] + "-" + afterDateArray[0] + "-" + afterDateArray[1];
            }
            beforeDate = "";
        } else if (self.view.AdvancedSearch.imgBetween.src === "radio_selected.png") {
            if (self.view.customCalGroupBetween.value === "") {
                beforeDate = "";
                afterDate = "";
            } else {
                var betweenDate = (self.view.customCalGroupBetween.value).split("-");
                var btwBeforeDate = betweenDate[0].trim().split("/");
                var btwAfterDate = betweenDate[1].trim().split("/");
                beforeDate = btwBeforeDate[2] + "-" + btwBeforeDate[0] + "-" + btwBeforeDate[1];
                afterDate = btwAfterDate[2] + "-" + btwAfterDate[0] + "-" + btwAfterDate[1];
            }
        } else {
            beforeDate = "";
            afterDate = "";
        }
        //STATUS
        if (self.view.AdvancedSearch.imgFlag1.src === "radio_selected.png") {
            status = self.statusConfig.custActive;
            statusText = kony.i18n.getLocalizedString("i18n.secureimage.Active");
        } else if (this.view.AdvancedSearch.imgFlag2.src === "radio_selected.png") {
            status = self.statusConfig.custSuspend;
            statusText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Suspended");
        } else if (this.view.AdvancedSearch.imgFlag3.src === "radio_selected.png") {
            status = self.statusConfig.custLocked;
            statusText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Locked");
        } else {
            status = "";
            statusText = "";
        }
        //Is bank staff
        var isBankStaff = self.view.AdvancedSearch.switchToggle.selectedIndex === 1 ? "false" : "true";
        //dates,status
        if (beforeDate !== "") {
            self.addTag(beforeDate, "bd");
        }
        if (afterDate !== "") {
            self.addTag(afterDate, "ad");
        }
        if (status !== "") {
            self.addTag(statusText, "st");
        }
        self.filterTag = {
            "name": self.view.AdvancedSearch.tbxSearchBox.text === "" ? [] : [self.view.AdvancedSearch.tbxSearchBox.text],
            'branch': branches,
            'product': products,
            'city': cities,
            'entitlement': entitlements,
            'group': groups,
            'status': status === "" ? [] : [status],
            'beforedate': beforeDate === "" ? [] : [beforeDate],
            'afterdate': afterDate === "" ? [] : [afterDate]
        };
        return {
            "_searchType": "GROUP_SEARCH",
            "_name": this.view.AdvancedSearch.tbxSearchBox.text === "" ? null : this.view.AdvancedSearch.tbxSearchBox.text,
            "_id": this.view.AdvancedSearch.tbxSearchBox.text === "" ? null : this.view.AdvancedSearch.tbxSearchBox.text,
            "_username": this.view.AdvancedSearch.tbxSearchBox.text === "" ? null : this.view.AdvancedSearch.tbxSearchBox.text,
            "_IsStaffMember": isBankStaff,
            "_branchIDS": branches.length === 0 ? null : branches.join(","), //"LID1",
            "_productIDS": products.length === 0 ? null : products.join(","), //"",
            "_cityIDS": cities.length === 0 ? null : cities.join(","), //"CITY203,CITY204",
            "_entitlementIDS": entitlements.length === 0 ? null : entitlements.join(","), //"",
            "_groupIDS": groups.length === 0 ? null : groups.join(","),
            "_customerStatus": status === "" ? null : status, //""
            "_before": beforeDate === "" ? null : beforeDate,
            "_after": afterDate === "" ? null : afterDate
        };
    },
    sortIconFor: function(column, iconPath) {
        var self = this;
        self.determineSortFontIcon(this.sortBy, column, self.view[iconPath]);
    },
    resetSortImages: function(context, fontIconName) {
        var self = this;
        if (context === "grouplist") {
            self.sortIconFor('Group_Name', 'fontIconSortName');
            self.sortIconFor('Customers_Count', 'fontIconSortCustomers');
        } else if (context === "viewgroup") {
            self.sortIconFor('Name', 'fontIconViewEntmntNameSort');
            if (this.view.fontIconViewCustNameSort.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE && fontIconName !== "fontIconViewCustNameSort") {
                this.view.fontIconViewCustNameSort.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
                this.view.fontIconViewCustNameSort.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
            }
            if (this.view.fontIconViewCustUsrnameSort.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE && fontIconName !== "fontIconViewCustUsrnameSort") {
                this.view.fontIconViewCustUsrnameSort.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
                this.view.fontIconViewCustUsrnameSort.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
            }
            if (this.view.fontIconViewCustMailSort.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE && fontIconName !== "fontIconViewCustMailSort") {
                this.view.fontIconViewCustMailSort.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
                this.view.fontIconViewCustMailSort.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
            }
            if (this.view.fontIconCustUpdatedOnSort.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE && fontIconName !== "fontIconCustUpdatedOnSort") {
                this.view.fontIconCustUpdatedOnSort.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
                this.view.fontIconCustUpdatedOnSort.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
            }
        } else if (context === "assigncust") { //CustAssignedPage
            if (this.view.fontIconAsgCustSortName.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE && fontIconName !== "fontIconAsgCustSortName") {
                this.view.fontIconAsgCustSortName.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
                this.view.fontIconAsgCustSortName.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
            }
            if (this.view.fontIconAsgCustSortUsername.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE && fontIconName !== "fontIconAsgCustSortUsername") {
                this.view.fontIconAsgCustSortUsername.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
                this.view.fontIconAsgCustSortUsername.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
            }
            if (this.view.fontIconAsgCustIdSort.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE && fontIconName !== "fontIconAsgCustIdSort") {
                this.view.fontIconAsgCustIdSort.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
                this.view.fontIconAsgCustIdSort.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
            }
        } else {}
    },
    commonWidgetDataMapForListBox: function() {
        var self = this;
        var dataMap = {
            "id": "id",
            "flxCheckBox": "flxCheckBox",
            "imgCheckBox": "imgCheckBox",
            "lblDescription": "lblDescription"
        };
        return dataMap;
    },
    /*
     * function to populates BRANCHES in listbox in advance search
     */
    setDataForBranchesListBox: function(branchesList) {
        var self = this;
        kony.adminConsole.utils.showProgressBar(this.view);
        var widgetMap = self.commonWidgetDataMapForListBox();
        var data = branchesList.map(function(record) {
            return {
                "id": record.Branch_id,
                "flxCheckBox": "flxCheckBox",
                "imgCheckBox": "checkbox.png",
                "lblDescription": {
                    "text": record.Branch_Code + "-" + record.Branch_Name,
                    "tooltip": record.Branch_Name
                },
                "template": "flxSearchDropDown"
            };
        });
        self.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.widgetDataMap = widgetMap;
        self.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.setData(data);
        self.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.info = {
            "data": data,
            "searchAndSortData": data
        };
        kony.adminConsole.utils.hideProgressBar(this.view);
    },
    /*
     * function to populates PRODUCTS in listbox in advance search
     */
    setDataForProductsListBox: function(productList) {
        var self = this;
        kony.adminConsole.utils.showProgressBar(this.view);
        var widgetMap = self.commonWidgetDataMapForListBox();
        var data = productList.map(function(record) {
            return {
                "id": record.id,
                "flxCheckBox": "flxCheckBox",
                "imgCheckBox": "checkbox.png",
                "lblDescription": {
                    "text": record.Name,
                    "tooltip": record.Name
                },
                "template": "flxSearchDropDown"
            };
        });
        self.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.widgetDataMap = widgetMap;
        self.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.setData(data);
        self.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.info = {
            "data": data,
            "searchAndSortData": data
        };
        kony.adminConsole.utils.hideProgressBar(this.view);
    },
    /*
     * function to populates CITIES in listbox in advance search
     */
    setDataForCitiesListBox: function(citiesList) {
        var self = this;
        kony.adminConsole.utils.showProgressBar(this.view);
        var widgetMap = self.commonWidgetDataMapForListBox();
        var data = citiesList.map(function(record) {
            return {
                "id": record.id,
                "flxCheckBox": "flxCheckBox",
                "imgCheckBox": "checkbox.png",
                "lblDescription": {
                    "text": record.Name,
                    "tooltip": record.Name
                },
                "template": "flxSearchDropDown"
            };
        });
        self.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.widgetDataMap = widgetMap;
        self.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.setData(data);
        self.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.info = {
            "data": data,
            "searchAndSortData": data
        };
        kony.adminConsole.utils.hideProgressBar(this.view);
    },
    /*
     * function to populates ENTITLEMENTS in listbox in advance search
     */
    setDataForEntitlementsListBox: function(entitlementsList) {
        var self = this;
        kony.adminConsole.utils.showProgressBar(this.view);
        var widgetMap = self.commonWidgetDataMapForListBox();
        var data = entitlementsList.map(function(record) {
            return {
                "id": record.id,
                "flxCheckBox": "flxCheckBox",
                "imgCheckBox": "checkbox.png",
                "lblDescription": {
                    "text": record.Name,
                    "tooltip": record.Name
                },
                "template": "flxSearchDropDown"
            };
        });
        self.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.widgetDataMap = widgetMap;
        self.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.setData(data);
        self.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.info = {
            "data": data,
            "searchAndSortData": data
        };
        kony.adminConsole.utils.hideProgressBar(this.view);
    },
    /*
     * function to populates GROUPS in listbox in advance search
     */
    setDataForGroupsListBox: function(groupsList) {
        var self = this;
        var widgetMap = self.commonWidgetDataMapForListBox();
        var data = groupsList.map(function(record) {
            return {
                "id": record.Group_id,
                "flxCheckBox": "flxCheckBox",
                "imgCheckBox": "checkbox.png",
                "lblDescription": {
                    "text": record.Group_Name,
                    "tooltip": record.Group_Name
                },
                "template": "flxSearchDropDown"
            };
        });
        self.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.widgetDataMap = widgetMap;
        self.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.setData(data);
        self.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.info = {
            "data": data,
            "searchAndSortData": data
        };
    },
    /*
     * function to call presentation controller functions to fetch data for search
     */
    fetchListBoxDataForAdvSearch: function() {
        var self = this;
        self.presenter.fetchEntitlementsForSearch();
        self.presenter.fetchCustomerGroups(kony.i18n.getLocalizedString("i18n.frmLogsController.search"));
        self.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.selectionindicator = null;
        self.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.selectionindicator = null;
        self.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.selectionindicator = null;
        self.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.selectionindicator = null;
        self.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.selectionindicator = null;
    },
    /*
     * function to map search results segment data to customers assigned segment
     *@param : search segment single row data
     *@return: mapped data for custAssign segment
     */
    addBtnClickSegmentDataMap: function(rowData) {
        var self = this;
        var status = "";
        if ((rowData.lblServicesStatus.text).toLowerCase().indexOf((kony.i18n.getLocalizedString("i18n.secureimage.Active")).toLowerCase()) > -1) {
            status = self.statusConfig.custActive;
        } else if ((rowData.lblServicesStatus.text).toLowerCase().indexOf((kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Locked")).toLowerCase()) > -1) {
            status = self.statusConfig.custLocked;
        } else {
            status = self.statusConfig.custSuspend;
        }
        var segRowData = {
            "Status_id": status,
            "flxAssignedCustomers": {
                "isVisible": true,
            },
            "flxCheckbox": {
                "isVisible": true
            },
            "flxDelete": {
                "isVisible": true,
                "onClick": function() {
                    self.onClickOfDeleteIcon();
                }
            },
            "flxGroupsAssignedCustomers": {
                "isVisible": true,
                "hoverSkin": "sknfbfcfc"
            },
            "flxStatus": {
                "isVisible": true
            },
            "imgCheckBox": {
                "isVisible": true,
                "src": "checkbox.png"
            },
            "fontIconDelete": {
                "text": "\ue929"
            },
            "fontIconCustomerStatus": {
                "skin": rowData.fontIconCustomerStatus.skin
            },
            "lblCustID": {
                "text": rowData.lblCustID.info.id ? self.AdminConsoleCommonUtils.getTruncatedString(rowData.lblCustID.info.id, 20, 17) : kony.i18n.getLocalizedString("i18n.Applications.NA"),
                "info": {
                    "text": rowData.lblCustID.info.id
                },
                "tooltip": rowData.lblCustID.info.id,
                "skin": "sknlblLatoRegular484b5213px"
            },
            "lblName": {
                "text": rowData.lblName,
                "skin": "sknlblLatoRegular484b5213px"
            },
            "lblSeperator": ".",
            "lblServicesStatus": {
                "text": rowData.lblServicesStatus.text,
                "skin": "sknlblLatoRegular484b5213px"
            },
            "lblUserName": {
                "text": rowData.lblUserName.info.id ? self.AdminConsoleCommonUtils.getTruncatedString(rowData.lblUserName.info.id, 20, 17) : kony.i18n.getLocalizedString("i18n.Applications.NA"),
                "info": {
                    "text": rowData.lblUserName.info.id
                },
                "tooltip": rowData.lblUserName.info.id,
                "skin": "sknlblLatoRegular484b5213px"
            },
            "lblRemoved": {
                "isVisible": false,
                "skin": "sknLbl484b52LatoReg13pxOp40"
            },
        };
        return segRowData;
    },
    /*
     * function called on click of add button in search results
     */
    advSearchResultOnClickOfAdd: function(rowData) {
        var self = this;
        var mappedrowData = self.addBtnClickSegmentDataMap(rowData);
        var segLength = self.view.segAssignedCustomers.data.length;
        self.custToAdd.push(mappedrowData);
        self.setAssignedCustomerSegDataMap();
        self.view.segAssignedCustomers.addDataAt(mappedrowData, segLength);
        self.updateAssignedCustomerCount();
        this.view.forceLayout();
    },
    /*
     * parse data for assigned ustomer segment
     * @param: customer search response
     * @return: mapped data for segment
     */
    parseCustomersForAssignedSeg: function(data) {
        var self = this;
        var status, statusImgSkin;
        var isDeleted = false;
        var mappedData = [];
        mappedData = data.map(function(record) {
            if (self.deletedCustomerId.indexOf(record.id) >= 0) {
                isDeleted = true;
            } else {
                isDeleted = false;
            }
            if (record.Status_id === self.statusConfig.custActive) {
                status = kony.i18n.getLocalizedString("i18n.secureimage.Active");
                statusImgSkin = isDeleted ? "sknFontIconActivateOp40" : "sknFontIconActivate";
            } else if (record.Status_id === self.statusConfig.custLocked) {
                status = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Locked");
                statusImgSkin = isDeleted ? "sknfontIconInactiveOp40" : "sknfontIconInactive";
            } else {
                status = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Suspended");
                statusImgSkin = isDeleted ? "sknFontIconSuspendOp40" : "sknFontIconSuspend";
            }
            return {
                "flxAssignedCustomers": {
                    "isVisible": true
                },
                "flxCheckbox": {
                    "isVisible": true
                },
                "flxDelete": {
                    "isVisible": (isDeleted === true) ? false : true,
                    "onClick": function() {
                        self.onClickOfDeleteIcon();
                    }
                },
                "flxGroupsAssignedCustomers": {
                    "isVisible": true,
                    "hoverSkin": (isDeleted === true) ? "sknFlxDisableCursor" : "sknfbfcfc"
                },
                "flxStatus": {
                    "isVisible": true
                },
                "Status_id": record.Status_id,
                "imgCheckBox": {
                    "isVisible": (isDeleted === true) ? false : true,
                    "src": "checkbox.png"
                },
                "fontIconDelete": {
                    "text": "\ue929"
                },
                "fontIconCustomerStatus": {
                    "skin": statusImgSkin
                },
                "lblCustID": {
                    "text": self.AdminConsoleCommonUtils.getTruncatedString(record.id, 20, 17) || "N/A",
                    "info": {
                        "text": record.id
                    },
                    "tooltip": record.id,
                    "skin": (isDeleted === true) ? "sknLbl484b52LatoReg13pxOp40" : "sknlblLatoRegular484b5213px"
                },
                "lblName": {
                    "text": record.name || "N/A",
                    "skin": (isDeleted === true) ? "sknLbl484b52LatoReg13pxOp40" : "sknlblLatoRegular484b5213px"
                },
                "lblSeperator": ".",
                "lblServicesStatus": {
                    "text": status,
                    "skin": (isDeleted === true) ? "sknLbl484b52LatoReg13pxOp40" : "sknlblLatoRegular484b5213px"
                },
                "lblUserName": {
                    "text": self.AdminConsoleCommonUtils.getTruncatedString(record.Username, 20, 17) || "N/A",
                    "info": {
                        "text": record.Username
                    },
                    "tooltip": record.Username,
                    "skin": (isDeleted === true) ? "sknLbl484b52LatoReg13pxOp40" : "sknlblLatoRegular484b5213px"
                },
                "lblRemoved": {
                    "isVisible": (isDeleted === true) ? true : false,
                    "skin": "sknLbl484b52ItalicSz12px"
                },
                "template": "flxGroupsAssignedCustomers"
            };
        });
        return mappedData;
    },
    /*
     * function to set data to assigned customers segment in edit customers
     * @param: customer's search response
     */
    setAssignedCustomersList: function(custResponse) {
        var self = this;
        var data = [],
            finalData = [],
            addData = [];
        self.setAssignedCustomerSegDataMap();
        if (custResponse !== undefined) {
            var sortColumnIcon = self.assignCustSegColumns.frontendIcons[self.assignCustSegColumns.backendNames.indexOf(custResponse.SortVariable)];
            self.resetSortImages("assigncust", sortColumnIcon);
            if (custResponse.SortDirection === "ASC") {
                self.view["" + sortColumnIcon].text = self.statusFontIcon.ASCENDING_IMAGE;
                self.view["" + sortColumnIcon].skin = self.statusFontIcon.ASCENDING_SKIN;
            } else {
                self.view["" + sortColumnIcon].text = self.statusFontIcon.DESCENDING_IMAGE;
                self.view["" + sortColumnIcon].skin = self.statusFontIcon.DESCENDING_SKIN;
            }
            data = self.parseCustomersForAssignedSeg(custResponse.records);
        }
        if (custResponse.PageOffset === 0) {
            self.view.flxAssignCustomerAdded.setContentOffset({
                x: 0,
                y: 3 + "px"
            });
        } else {
            self.view.flxAssignCustomerAdded.setContentOffset({
                x: 0,
                y: (self.segmentRecordsSize * 20) + "px"
            });
        }
        addData = data.concat(self.custToAdd); //to concat newly added customers if any
        self.view.segAssignedCustomers.setData(addData);
        self.selectPreviouslySelectedCust(addData);
        self.getMoreDataModel = {
            "PageOffset": custResponse.PageOffset,
            "PageSize": custResponse.PageSize,
            "TotalResultsFound": custResponse.TotalResultsFound,
            "RecordsOnPage": self.orgAssignedCust.length,
            "SortVariable": custResponse.SortVariable,
            "SortDirection": custResponse.SortDirection,
        };
        if (addData.length <= 0 && custResponse.PageOffset === 0) {
            self.view.flxCustomerAssigned.setVisibility(false);
            self.view.flxAssignCustomerAdded.setVisibility(false);
            self.view.flxNoCustomerAssign.setVisibility(true);
        } else {
            self.view.flxCustomerAssigned.setVisibility(true);
            self.view.flxAssignCustomerAdded.setVisibility(true);
            self.view.flxNoCustomerAssign.setVisibility(false);
            self.view.segAssignedCustomers.setVisibility(true);
            document.getElementById("frmGroups_flxAssignCustomerAdded").onscroll = self.getResultsOnReachingend;
        }
        self.setDataForCustomerStatusFilter();
        self.updateAssignedCustomerCount();
        this.view.forceLayout();
    },
    /*
     * select the page customer which were selected previously before scroll
     * @param: segment page data
     */
    selectPreviouslySelectedCust: function(data) {
        var self = this;
        var selInd = [];
        for (var i = 0; i < data.length; i++) {
            if ((self.selectedCustomerId).indexOf(data[i].lblCustID.info.text) >= 0) {
                selInd.push(i);
            }
        }
        if (selInd.length > 0) {
            self.view.segAssignedCustomers.selectedRowIndices = [
                [0, selInd]
            ];
        } else {
            self.view.segAssignedCustomers.selectedRowIndices = "";
        }
    },
    /*
     * function called on click of delete icon in the assigned customers segment
     */
    onClickOfDeleteIcon: function() {
        var self = this;
        var updateIndices = self.view.segAssignedCustomers.selectedRowIndices;
        var index = self.view.segAssignedCustomers.selectedRowIndex;
        var rowIndex = index[1];
        var rowData = self.view.segAssignedCustomers.data[rowIndex];
        rowData.flxDelete.isVisible = false;
        rowData.imgCheckBox.isVisible = false;
        rowData.lblRemoved.isVisible = true;
        //changing skins of row's label to reduce opacity on deletion
        rowData.lblCustID.skin = "sknLbl484b52LatoReg13pxOp40";
        rowData.lblName.skin = "sknLbl484b52LatoReg13pxOp40";
        rowData.lblServicesStatus.skin = "sknLbl484b52LatoReg13pxOp40";
        rowData.lblUserName.skin = "sknLbl484b52LatoReg13pxOp40";
        rowData.flxGroupsAssignedCustomers.hoverSkin = "sknFlxDisableCursor";
        if (rowData.fontIconCustomerStatus.skin === "sknFontIconActivate") {
            rowData.fontIconCustomerStatus.skin = "sknFontIconActivateOp40";
        } else if (rowData.fontIconCustomerStatus.skin === "sknfontIconInactive") {
            rowData.fontIconCustomerStatus.skin = "sknfontIconInactiveOp40";
        } else {
            rowData.fontIconCustomerStatus.skin = "sknFontIconSuspendOp40";
        }
        self.deletedCustomerId.push(rowData.lblCustID.info.text);
        self.view.segAssignedCustomers.setDataAt(rowData, index[1]);
        self.updateAssignedCustomerCount();
        self.closeAdvanceSearchPopUp();
    },
    /*
     * function to add filter tags
     * @param : display text
     * @param : new flex id
     */
    addTag: function(textForTag, id) {
        var self = this;
        var newTextTag = this.view.flxFilter1.clone(id);
        var lblname = id + "lblCsrAssist";
        var imgname = id + "flxCross";
        newTextTag[lblname].text = textForTag;
        newTextTag[lblname].tooltip = textForTag;
        newTextTag.isVisible = true;
        self.view.flxSearchFilter.addAt(newTextTag, -1);
        newTextTag[imgname].onTouchStart = function() {
            self.removeTag(newTextTag.id);
        };
        this.view.flxSearchFilter.setVisibility(true);
        this.view.forceLayout();
    },
    /*
     * function to remove a selected tag
     * @param : selectedflex id
     */
    removeTag: function(flexName) {
        var selectedOptions = flexName.slice(0, -10);
        var self = this;
        this.view.flxSearchFilter.remove(this.view[flexName]); 
        this.view.forceLayout();
        var codes = {    
            "br": "branch",
            "pr": "product",
            "ci": kony.i18n.getLocalizedString("i18n.frmGroupsController.city"),
            "en": kony.i18n.getLocalizedString("i18n.frmGroupsController.entitlement"),
            "gr": "group",
            "bd": "beforedate",
            "ad": "afterdate",
            "st": "status"
        };
        var res = codes[selectedOptions.slice(0, 2)];
        this.removeTagsFromSegmentInfo(res, selectedOptions.slice(2));
        this.filterTag[res].splice(this.filterTag[res].indexOf(selectedOptions.slice(2)), 1);
        var searchReqParam = {
            "_name": self.filterTag.name.length === 0 ? "" : self.filterTag.name[0],
            "_searchType": "GROUP_SEARCH",
            "_id": null,
            "_username": self.filterTag.name.length === 0 ? "" : self.filterTag.name[0],
            "_branchIDS": self.filterTag.branch.length === 0 ? null : self.filterTag.branch.join(","), //"LID1",
            "_productIDS": self.filterTag.product.length === 0 ? null : self.filterTag.product.join(","), //"",
            "_cityIDS": self.filterTag.city.length === 0 ? null : self.filterTag.city.join(","), //"CITY203,CITY204",
            "_entitlementIDS": self.filterTag.entitlement.length === 0 ? null : self.filterTag.entitlement.join(","), //"",
            "_groupIDS": self.filterTag.group.length === 0 ? null : self.filterTag.group.join(","),
            "_customerStatus": self.filterTag.status.length === 0 ? null : self.filterTag.status[0], //""
            "_before": self.filterTag.beforedate.length === 0 ? null : self.filterTag.beforedate[0],
            "_after": self.filterTag.afterdate.length === 0 ? null : self.filterTag.afterdate.length[0],
            "_pageOffset": "0",
            "_pageSize": self.limitForPaginationSearch,
            "_sortVariable": self.customersSegmentColumns.backendNames[0],
            "_sortDirection": "ASC"
        };
        self.showAdvSearchLoadingScreen();
        self.presenter.searchCustomers(searchReqParam);
    },
    /*
     * update selection for listbox data and count on removing tags
     * @param: target - selected listbox category,id-removed tag's data id
     */
    removeTagsFromSegmentInfo: function(target, id) {
        var branchSegment = this.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData;
        var productSegment = this.view.AdvancedSearch.AdvSearchDropDown11.sgmentData;
        var citySegment = this.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData;
        var entitlementsSegment = this.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData;
        var groupsSegment = this.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData;
        var count = 0;
        if (target === "status") {
            this.view.AdvancedSearch.imgFlag1.src = "radio_notselected.png";
            this.view.AdvancedSearch.imgFlag2.src = "radio_notselected.png";
            this.view.AdvancedSearch.imgFlag3.src = "radio_notselected.png";
        } else if (target === "branch") {
            if (branchSegment.info && branchSegment.info.selectedData) {
                branchSegment.info.selectedData = branchSegment.info.selectedData.filter(function(item) {
                    if (item.id !== id) return true;
                });
                branchSegment.setData(branchSegment.data.map(function(item) {
                    if (item.id === id) item.imgCheckBox = "checkbox.png";
                    return item;
                }));
                count = branchSegment.info.selectedData.length;
                this.view.AdvancedSearch.lblSelectedRows.text = count === 0 ? kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Groups") : count === 1 ? branchSegment.info.selectedData[0].lblDescription.text : count + kony.i18n.getLocalizedString("i18n.frmGroupsController.Groups_selected");
                this.view.AdvancedSearch.lblSelectedRows.toolTip = this.view.AdvancedSearch.lblSelectedRows.text;
            }
        } else if (target === "product") {
            if (productSegment.info && productSegment.info.selectedData) {
                productSegment.info.selectedData = productSegment.info.selectedData.filter(function(item) {
                    if (item.id !== id) return true;
                });
                productSegment.setData(productSegment.data.map(function(item) {
                    if (item.id === id) item.imgCheckBox = "checkbox.png";
                    return item;
                }));
                count = productSegment.info.selectedData.length;
                this.view.AdvancedSearch.lblSelectedRows11.text = count === 0 ? kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Products") : count === 1 ? productSegment.info.selectedData[0].lblDescription.text : count + kony.i18n.getLocalizedString("i18n.frmGroupsController.Products_selected");
                this.view.AdvancedSearch.lblSelectedRows11.toolTip = this.view.AdvancedSearch.lblSelectedRows11.text;
            }
        } else if (target === "city") {
            if (citySegment.info && citySegment.info.selectedData) {
                citySegment.info.selectedData = citySegment.info.selectedData.filter(function(item) {
                    if (item.id !== id) return true;
                });
                citySegment.setData(citySegment.data.map(function(item) {
                    if (item.id === id) item.imgCheckBox = "checkbox.png";
                    return item;
                }));
                count = citySegment.info.selectedData.length;
                this.view.AdvancedSearch.lblSelectedRows02.text = count === 0 ? kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Cities") : count === 1 ? citySegment.info.selectedData[0].lblDescription.text : count + kony.i18n.getLocalizedString("i18n.frmGroupsController.Cities_selected");
                this.view.AdvancedSearch.lblSelectedRows02.toolTip = this.view.AdvancedSearch.lblSelectedRows02.text;
            }
        } else if (target === "entitlement") {
            if (entitlementsSegment.info && entitlementsSegment.info.selectedData) {
                entitlementsSegment.info.selectedData = entitlementsSegment.info.selectedData.filter(function(item) {
                    if (item.id !== id) return true;
                });
                entitlementsSegment.setData(entitlementsSegment.data.map(function(item) {
                    if (item.id === id) item.imgCheckBox = "checkbox.png";
                    return item;
                }));
                count = entitlementsSegment.info.selectedData.length;
                this.view.AdvancedSearch.lblSelectedRows03.text = count === 0 ? kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Entitlements") : count === 1 ? entitlementsSegment.info.selectedData[0].lblDescription.text : count + kony.i18n.getLocalizedString("i18n.frmGroupsController.Entitlements_selected");
                this.view.AdvancedSearch.lblSelectedRows03.toolTip = this.view.AdvancedSearch.lblSelectedRows03.text;
            }
        } else if (target === "group") {
            if (groupsSegment.info && groupsSegment.info.selectedData) {
                groupsSegment.info.selectedData = groupsSegment.info.selectedData.filter(function(item) {
                    if (item.id !== id) return true;
                });
                groupsSegment.setData(groupsSegment.data.map(function(item) {
                    if (item.id === id) item.imgCheckBox = "checkbox.png";
                    return item;
                }));
                count = groupsSegment.info.selectedData.length;
                this.view.AdvancedSearch.lblSelectedRows12.text = count === 0 ? kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Groups") : count === 1 ? groupsSegment.info.selectedData[0].lblDescription.text : count + kony.i18n.getLocalizedString("i18n.frmGroupsController.Groups_selected");
                this.view.AdvancedSearch.lblSelectedRows12.toolTip = this.view.AdvancedSearch.lblSelectedRows12.text;
            }
        }
    },
    /*
     * function to remove all the filter tags
     */
    removeAllTags: function() {
        var self = this;
        self.view.flxSearchFilter.removeAll();
    },
    /*
     * function to clear all the filters
     */
    clearAllFilterInAdvSearch: function() {
        var self = this;
        self.view.AdvancedSearch.tbxSearchBox.text = "";
        self.view.AdvancedSearch.flxSearchContainer.skin = "sknflxd5d9ddop100";
        self.view.AdvancedSearch.flxClearSearchImage.setVisibility(false);
        self.hideDropDowns("advsearch");
        self.view.lblIconArrow.text = "\ue915"; //downArrow
        self.view.lblIconArrow.skin = "sknfontIconDescDownArrow12px";
        self.view.flxSearchFilter.setVisibility(true);
        self.view.AdvancedSearch.AdvancedSearchDropDown01.tbxSearchBox.text = "";
        self.view.AdvancedSearch.AdvSearchDropDown11.tbxSearchBox.text = "";
        self.view.AdvancedSearch.AdvancedSearchDropDown02.tbxSearchBox.text = "";
        self.view.AdvancedSearch.AdvancedSearchDropDown03.tbxSearchBox.text = "";
        self.view.AdvancedSearch.AdvancedSearchDropDown12.tbxSearchBox.text = "";
        if (self.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.info) {
            self.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.info.selectedData = [];
            self.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.setData(self.view.AdvancedSearch.AdvancedSearchDropDown01.sgmentData.info.data.map(function(record) {
                record.imgCheckBox = "checkbox.png";
                return record;
            }));
        }
        if (self.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.info) {
            self.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.info.selectedData = [];
            self.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.setData(self.view.AdvancedSearch.AdvSearchDropDown11.sgmentData.info.data.map(function(record) {
                record.imgCheckBox = "checkbox.png";
                return record;
            }));
        }
        if (self.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.info) {
            self.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.info.selectedData = [];
            self.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.setData(self.view.AdvancedSearch.AdvancedSearchDropDown02.sgmentData.info.data.map(function(record) {
                record.imgCheckBox = "checkbox.png";
                return record;
            }));
        }
        if (self.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.info) {
            self.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.info.selectedData = [];
            self.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.setData(self.view.AdvancedSearch.AdvancedSearchDropDown03.sgmentData.info.data.map(function(record) {
                record.imgCheckBox = "checkbox.png";
                return record;
            }));
        }
        if (self.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.info) {
            self.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.info.selectedData = [];
            self.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.setData(self.view.AdvancedSearch.AdvancedSearchDropDown12.sgmentData.info.data.map(function(record) {
                record.imgCheckBox = "checkbox.png";
                return record;
            }));
        }
        self.view.AdvancedSearch.imgFlag1.src = "radio_notselected.png";
        self.view.AdvancedSearch.imgFlag2.src = "radio_notselected.png";
        self.view.AdvancedSearch.imgFlag3.src = "radio_notselected.png";
        self.view.AdvancedSearch.imgBefore.src = "radio_notselected.png";
        self.view.AdvancedSearch.imgAfter.src = "radio_notselected.png";
        self.view.AdvancedSearch.imgBetween.src = "radio_notselected.png";
        self.view.AdvancedSearch.switchToggle.selectedIndex = 1;
        self.view.AdvancedSearch.lblSelectedRows.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Branches");
        self.view.AdvancedSearch.lblSelectedRows11.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Products");
        self.view.AdvancedSearch.lblSelectedRows02.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Cities");
        self.view.AdvancedSearch.lblSelectedRows03.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Entitlements");
        self.view.AdvancedSearch.lblSelectedRows12.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Select_the_Groups");
        self.view.customCalGroupBefore.resetData = kony.i18n.getLocalizedString("i18n.frmLogsController.Select_Date");
        self.view.customCalGroupAfter.resetData = kony.i18n.getLocalizedString("i18n.frmLogsController.Select_Date");
        self.view.customCalGroupBetween.resetData = kony.i18n.getLocalizedString("i18n.frmLogsController.Select_Date");
        self.view.AdvancedSearch.lblSelectedRows.toolTip = self.view.AdvancedSearch.lblSelectedRows.text;
        self.view.AdvancedSearch.lblSelectedRows11.toolTip = self.view.AdvancedSearch.lblSelectedRows11.text;
        self.view.AdvancedSearch.lblSelectedRows02.toolTip = self.view.AdvancedSearch.lblSelectedRows02.text;
        self.view.AdvancedSearch.lblSelectedRows03.toolTip = self.view.AdvancedSearch.lblSelectedRows03.text;
        self.view.AdvancedSearch.lblSelectedRows12.toolTip = self.view.AdvancedSearch.lblSelectedRows12.text;
    },
    /*
     * function called to add selected customers
     */
    onClickOfBtnAddClose: function(btnName) {
        var self = this;
        self.showAdvSearchLoadingScreen();
        kony.adminConsole.utils.showProgressBar(self.view);
        var selectedData = [];
        var selInd = self.view.segCustomers.selectedIndices;
        var count = 0;
        if (selInd !== null) {
            selectedData = self.view.segCustomers.selectedItems;
            var row = selInd[0][1];
            //populate assign cust segment
            if (self.view.imgHeaderCheckBox.src === "checkboxselected.png") {
                self.view.segAssignedCustomers.info = {
                    "completeData": self.searchResult,
                    "offset": 0,
                    "context": "selectall"
                };
            } else {
                self.view.segAssignedCustomers.info = {
                    "completeData": self.view.segCustomers.selectedItems,
                    "offset": 0,
                    "context": "multiselect"
                };
            }
            kony.timer.schedule("setSegDataTimer", self.segDataSettingTimer, 2, true);
        }
        kony.adminConsole.utils.hideProgressBar(self.view);
    },
    /*
     * call back function for timer to set data in intervals
     */
    segDataSettingTimer: function() {
        var scopeObj = this;
        var mappedData;
        scopeObj.showAdvSearchLoadingScreen();
        kony.adminConsole.utils.showProgressBar(scopeObj.view);
        var data = scopeObj.view.segAssignedCustomers.info.completeData;
        var pos = scopeObj.view.segAssignedCustomers.info.offset;
        var context = scopeObj.view.segAssignedCustomers.info.context;
        var rowPosition = scopeObj.view.segAssignedCustomers.data.length;
        var dataToAdd = data.slice(pos, pos + 15);
        for (var i = 0; i < dataToAdd.length; i++) {
            if (dataToAdd[i].lblAdded.isVisible && context === "selectall") {
                continue;
            } else {
                mappedData = scopeObj.addBtnClickSegmentDataMap(dataToAdd[i]);
                scopeObj.view.segAssignedCustomers.addDataAt(mappedData, rowPosition++);
                scopeObj.custToAdd.push(mappedData);
            }
        }
        scopeObj.view.segAssignedCustomers.info.offset = pos + 15;
        if (dataToAdd.length === 0) {
            var toatalRecCount = 0;
            kony.timer.cancel("setSegDataTimer");
            if (scopeObj.getMoreDataModel) {
                toatalRecCount = scopeObj.getMoreDataModel.TotalResultsFound;
            }
            scopeObj.updateAssignedCustomerCount();
            kony.adminConsole.utils.hideProgressBar(scopeObj.view);
            scopeObj.hideAdvSearchLoadingScreen();
            scopeObj.updateSearchSegData();
            var segLength = scopeObj.view.segAssignedCustomers.data.length || 0;
            if (segLength > 0) {
                scopeObj.view.flxNoCustomerAssign.setVisibility(false);
                scopeObj.view.flxCustomerAssigned.setVisibility(true);
                scopeObj.view.segAssignedCustomers.setVisibility(true);
                scopeObj.view.flxAssignCustomerAdded.setVisibility(true);
            } else {
                scopeObj.view.flxCustomerAssigned.setVisibility(false);
                scopeObj.view.flxNoCustomerAssign.setVisibility(true);
            }
        }
        scopeObj.view.forceLayout();
    },
    /*
     * function to update the search segment data
     */
    updateSearchSegData: function() {
        var self = this;
        var selectedData = [];
        self.showAdvSearchLoadingScreen();
        kony.adminConsole.utils.showProgressBar(self.view);
        var selInd = self.view.segCustomers.selectedIndices;
        var count = 0,
            cRow = 0;
        if (selInd !== null) {
            selectedData = self.view.segCustomers.selectedItems;
            var row = selInd[0][1];
            for (var i = 0; i < selectedData.length; i++) {
                if (selectedData[i].lblAdded.isVisible) { //restrict already added data
                    count = count + 1;
                } else {
                    selectedData[i].btnAdd.isVisible = false;
                    selectedData[i].flxCheckboxHidden.isVisible = true;
                    selectedData[i].imgCheckBoxHidden.isVisible = true;
                    selectedData[i].flxCheckbox.isVisible = false;
                    selectedData[i].lblAdded.isVisible = true;
                    if (self.view.imgHeaderCheckBox.src === "checkboxselected.png" && selectedData.length === self.view.segCustomers.data.length) { //when all are selected
                        cRow = (selectedData.length > row.length) ? row[0] : row[i]; //incase index of rows added are not present in row array
                        self.view.segCustomers.setDataAt(selectedData[i], cRow);
                        self.view.toastMessageAdvSearch.lbltoastMessage.text = (self.searchResult.length - count) + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers_added_successfully");
                    } else { //when multiple are selected
                        self.view.segCustomers.setDataAt(selectedData[i], row[0]);
                        self.view.toastMessageAdvSearch.lbltoastMessage.text = (selectedData.length - count) + kony.i18n.getLocalizedString("i18n.frmGroupsController.Customers_added_successfully");
                    }
                }
            }
            self.showToastMessage();
            if (self.view.imgHeaderCheckBox.src === "checkboxselected.png") {
                for (var j = 0; j < self.searchResult.length; j++) { //updating remaining unset data
                    self.searchResult[j].btnAdd.isVisible = false;
                    self.searchResult[j].flxCheckboxHidden.isVisible = true;
                    self.searchResult[j].imgCheckBoxHidden.isVisible = true;
                    self.searchResult[j].flxCheckbox.isVisible = false;
                    self.searchResult[j].lblAdded.isVisible = true;
                }
            }
        }
        self.hideAdvSearchLoadingScreen();
        kony.adminConsole.utils.hideProgressBar(self.view);
        self.unSelectAllRows();
        self.enableDisableSearchCheckbox();
    },
    /*
     * set checkbox image to disable or normal
     */
    enableDisableSearchCheckbox: function() {
        var self = this;
        if (self.view.imgHeaderCheckBox.src === "checkboxselected.png") {
            self.view.imgHeaderCheckBox.src = "checkboxdisable.png";
            self.view.flxCheckbox.setEnabled(false);
        } else {
            self.view.imgHeaderCheckBox.src = "checkbox.png";
            self.view.flxCheckbox.setEnabled(true);
        }
    },
    /*
     * function to perform internal search on customers page of groups
     */
    performSearchOnAssgCust: function() {
        var self = this;
        var searchText = self.view.tbxSearchBox.text;
        var orgsegData = self.orgAssignedCust.concat(self.custToAdd);
        var requiredData = function(custObj) {
            return custObj.lblUserName.info.text.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
        };
        var searchRes = orgsegData.filter(requiredData);
        var records = searchRes.length;
        self.view.rtxNoResults.text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found") + self.view.tbxSearchBox.text + kony.i18n.getLocalizedString("i18n.frmPoliciesController.Try_with_another_keyword");
        if (records === 0) {
            self.view.segAssignedCustomers.setVisibility(false);
            self.view.rtxNoResults.setVisibility(true);
        } else {
            self.view.rtxNoResults.setVisibility(false);
            self.view.segAssignedCustomers.setVisibility(true);
            self.view.segAssignedCustomers.setData(searchRes);
        }
        self.view.forceLayout();
    },
    search: function(SegmentWidget, searchParameters, rtxMsg) {
        //var sampleSearch=[{"searchKey":"lblRequestNumber","searchValue":scopeObj.view.RequestsSearch.tbxSearchBox.text},{	"searchKey":"lblCategory","searchValue":scopeObj.view.RequestsSearch.tbxSearchBox.text}];
        var data = SegmentWidget.info.data;
        data = data.filter(searchFilter);
        SegmentWidget.info.searchAndSortData = data;
        if (data.length === 0) {
            rtxMsg.setVisibility(true);
            SegmentWidget.setVisibility(false);
        } else {
            rtxMsg.setVisibility(false);
            SegmentWidget.setVisibility(true);
            SegmentWidget.setData(data.map(function(segmentData) {
                if (SegmentWidget.info.selectedData && SegmentWidget.info.selectedData.indexOf(segmentData) > -1) segmentData.imgCheckBox = 'checkboxselected.png';
                else segmentData.imgCheckBox = 'checkbox.png';
                return segmentData;
            }));
        }
        this.view.forceLayout();

        function searchFilter(searchModel) {
            var flag = false;
            for (var i = 0; i < searchParameters.length; i++) {
                if (flag) break;
                else {
                    if (typeof searchParameters[i].searchValue === 'string' && searchParameters[i].searchValue.length > 0) {
                        flag = flag || (searchModel["" + searchParameters[i].searchKey].text || "").toLowerCase().indexOf(searchParameters[i].searchValue.toLowerCase()) !== -1;
                    } else {
                        return true;
                    }
                }
            }
            return flag;
        }
    },
    /*
     * function to set data to type and status filter segments
     */
    setListFiltersData: function() {
        var self = this;
        var statusList = [],
            typeList = [];
        for (var i = 0; i < self.groupsData.length; i++) {
            if (!statusList.contains(self.groupsData[i].Status_id)) statusList.push(self.groupsData[i].Status_id);
            if (!typeList.contains(self.groupsData[i].Type_Name)) {
                typeList.push(self.groupsData[i].Type_Name);
            }
        }
        var widgetMap = {
            "Status_id": "Status_id",
            "Type_id": "Type_id",
            "flxSearchDropDown": "flxSearchDropDown",
            "flxCheckBox": "flxCheckBox",
            "imgCheckBox": "imgCheckBox",
            "lblDescription": "lblDescription"
        };
        var statusData = statusList.map(function(rec) {
            return {
                "Status_id": rec,
                "flxSearchDropDown": "flxSearchDropDown",
                "flxCheckBox": "flxCheckBox",
                "imgCheckBox": "checkbox.png",
                "lblDescription": (rec === self.statusConfig.active) ? kony.i18n.getLocalizedString("i18n.secureimage.Active") : kony.i18n.getLocalizedString("i18n.secureimage.Inactive")
            };
        });
        var typeData = typeList.map(function(rec) {
            return {
                "Type_id": rec,
                "flxSearchDropDown": "flxSearchDropDown",
                "flxCheckBox": "flxCheckBox",
                "imgCheckBox": "checkbox.png",
                "lblDescription": {
                    "text": self.AdminConsoleCommonUtils.getTruncatedString(rec, 12, 10),
                    "tooltip": rec
                }
            };
        });
        self.view.statusFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
        self.view.typeFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
        self.view.statusFilterMenu.segStatusFilterDropdown.setData(statusData);
        self.view.typeFilterMenu.segStatusFilterDropdown.setData(typeData);
        var selStatusInd = [],
            selTypeInd = [];
        for (var j = 0; j < statusList.length; j++) {
            selStatusInd.push(j);
        }
        for (var k = 0; k < typeList.length; k++) {
            selTypeInd.push(k);
        }
        self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices = [
            [0, selStatusInd]
        ];
        self.view.typeFilterMenu.segStatusFilterDropdown.selectedIndices = [
            [0, selTypeInd]
        ];
        self.view.forceLayout();
    },
    /*
     * filters groups list based on selected type and status
     * @param: groups backend data
     * @return : filtered data
     */
    filterBasedOnTypeStatus: function(data) {
        var self = this;
        var selFilter = [
            []
        ];
        var dataToShow = [];
        var groupsList = data || self.groupsData;
        var statusIndices = self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices;
        var typeIndices = self.view.typeFilterMenu.segStatusFilterDropdown.selectedIndices;
        selFilter[0][1] = [];
        selFilter[0][0] = [];
        var selStatusInd = null;
        var selTypeInd = null;
        //get selected types
        selTypeInd = typeIndices ? typeIndices[0][1] : [];
        for (var i = 0; i < selTypeInd.length; i++) {
            selFilter[0][0].push(self.view.typeFilterMenu.segStatusFilterDropdown.data[selTypeInd[i]].Type_id);
        }
        //get selected status
        selStatusInd = statusIndices ? statusIndices[0][1] : [];
        for (var j = 0; j < selStatusInd.length; j++) {
            selFilter[0][1].push(self.view.statusFilterMenu.segStatusFilterDropdown.data[selStatusInd[j]].Status_id);
        }
        if (selFilter[0][0].length === 0 || selFilter[0][1].length === 0) { //none selected - show no results
            dataToShow = [];
        } else if (selFilter[0][0].length > 0 && selFilter[0][1].length > 0) { //both filters selected
            dataToShow = groupsList.filter(function(rec) {
                if (selFilter[0][1].indexOf(rec.Status_id) >= 0 && selFilter[0][0].indexOf(rec.Type_Name) >= 0) {
                    return rec;
                }
            });
        } else { //single filter selected
        }
        return dataToShow;
    },
    /*
     * function to perform removal of multiple rows in assigned customers segment 
     */
    btnRemoveOnClick: function() {
        var self = this;
        var selInd = [],
            rowData = "";
        var data = self.view.segAssignedCustomers.data;
        var indices = self.view.segAssignedCustomers.selectedRowIndices;
        var updateIndices = [];
        if (indices !== null) {
            selInd = indices[0][1];
            //if only multiple are selected
            while (selInd.length > 0) {
                rowData = self.view.segAssignedCustomers.data[selInd[0]];
                //mark row as deleted
                if (rowData.flxDelete.isVisible === true) {
                    rowData.flxDelete.isVisible = false;
                    rowData.imgCheckBox.isVisible = false;
                    rowData.lblRemoved.isVisible = true;
                    //changin row's label skins to reduce opacity
                    rowData.lblCustID.skin = "sknLbl484b52LatoReg13pxOp40";
                    rowData.lblName.skin = "sknLbl484b52LatoReg13pxOp40";
                    rowData.lblServicesStatus.skin = "sknLbl484b52LatoReg13pxOp40";
                    rowData.lblUserName.skin = "sknLbl484b52LatoReg13pxOp40";
                    rowData.flxGroupsAssignedCustomers.hoverSkin = "sknFlxDisableCursor";
                    if (rowData.fontIconCustomerStatus.skin === "sknFontIconActivate") {
                        rowData.fontIconCustomerStatus.skin = "sknFontIconActivateOp40";
                    } else if (rowData.fontIconCustomerStatus.skin === "sknfontIconInactive") {
                        rowData.fontIconCustomerStatus.skin = "sknfontIconInactiveOp40";
                    } else {
                        rowData.fontIconCustomerStatus.skin = "sknFontIconSuspendOp40";
                    }
                    self.deletedCustomerId.push(rowData.lblCustID.info.text);
                    self.view.segAssignedCustomers.setDataAt(rowData, selInd[0]);
                } else {
                    //incase deleted row is selected again
                    updateIndices = selInd.splice(0, 1);
                    self.view.segAssignedCustomers.selectedRowIndices = [
                        [0, updateIndices]
                    ];
                }
            }
            self.updateAssignedCustomerCount();
        }
        //after removal
        self.view.flxAddCustomerButtons.setVisibility(true);
        self.view.flxRemoveCustButtons.setVisibility(false);
        var segdata = self.view.segAssignedCustomers.data;
        if (segdata.length === 0) {
            self.view.flxNoCustomerAssign.setVisibility(true);
            self.view.flxCustomerAssigned.setVisibility(false);
        }
    },
    /*
     * function to hide the dropdowns visibility when clicked outside it
     * @param:context
     */
    hideDropDowns: function(context) {
        var self = this;
        if (context === "advsearch") {
            self.view.AdvancedSearch.flxDropDownDetail01.setVisibility(false);
            self.view.AdvancedSearch.fontIconSearchDown01.text = "\ue920";
            self.view.AdvancedSearch.flxDropDownDetail11.setVisibility(false);
            self.view.AdvancedSearch.fontIconSearchDown11.text = "\ue920";
            self.view.AdvancedSearch.flxDropDownDetail02.setVisibility(false);
            self.view.AdvancedSearch.fontIconSearchDown02.text = "\ue920";
            self.view.AdvancedSearch.flxDropDownDetail03.setVisibility(false);
            self.view.AdvancedSearch.fontIconSearchDown03.text = "\ue920";
            self.view.AdvancedSearch.flxDropDownDetail12.setVisibility(false);
            self.view.AdvancedSearch.fontIconSearchDown12.text = "\ue920";
            self.showCalendarFlex();
        } else {}
    },
    /*
     * call back function for dropdowns visibility 
     */
    onDropdownHoverCallback: function(widget, context) {
        var scopeObj = this;
        var widGetId = widget.id;
        if (widget) { //for filter dropdown
            if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
                widget.setVisibility(true);
            } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                widget.setVisibility(false);
            }
        }
    },
    /*
     * show-hide loading screen function for advance search popup
     */
    showAdvSearchLoadingScreen: function() {
        var self = this;
        kony.adminConsole.utils.LOADING_TIMEOUT_IN_SEC = 20;
        self.view.flxLoading2.setVisibility(true);
        if (self.view.flxLoading2.timeoutHandle) clearTimeout(self.view.flxLoading2.timeoutHandle);
        self.view.flxLoading2.timeoutHandle = setTimeout(function() {
            kony.adminConsole.utils.hideProgressBar(self.view);
        }, kony.adminConsole.utils.LOADING_TIMEOUT_IN_SEC * 1000);
    },
    hideAdvSearchLoadingScreen: function() {
        var self = this;
        if (self.view.flxLoading2.isVisible) {
            self.view.flxLoading2.setVisibility(false);
            if (self.view.flxLoading2.timeoutHandle) clearTimeout(self.view.flxLoading2.timeoutHandle);
        }
    },
    /*
     * function to clear search box to defaults
     */
    clearSearchBoxToDefaults: function() {
        var scopeObj = this;
        if (scopeObj.view.addAndRemoveOptions.tbxSearchBox.text !== "") {
            scopeObj.view.addAndRemoveOptions.tbxSearchBox.text = "";
            scopeObj.view.addAndRemoveOptions.flxClearSearchImage.setVisibility(false);
            scopeObj.view.addAndRemoveOptions.flxSearchContainer.skin = "sknflxd5d9ddop100";
        }
        if (scopeObj.view.tbxSearchBox.text !== "") {
            scopeObj.view.tbxSearchBox.text = "";
            scopeObj.view.flxSearchContainer.skin = "sknflxBgffffffBorderc1c9ceRadius30px";
            scopeObj.view.flxClearSearchImage.setVisibility(false);
        }
    },
    /*
     * function to close dv search popup and display appropriate screen in assign customers
     */
    closeAdvanceSearchPopUp: function() {
        var scopeObj = this;
        scopeObj.view.flxAdvanceSearchPopUp.isVisible = false;
        var segLength = (scopeObj.view.segAssignedCustomers.data !== null) ? scopeObj.view.segAssignedCustomers.data.length : 0;
        if (segLength > 0) {
            scopeObj.view.flxNoCustomerAssign.setVisibility(false);
            scopeObj.view.flxCustomerAssigned.setVisibility(true);
            scopeObj.view.segAssignedCustomers.setVisibility(true);
            scopeObj.view.flxAssignCustomerAdded.setVisibility(true);
            scopeObj.setDataForCustomerStatusFilter();
        } else {
            scopeObj.view.flxCustomerAssigned.setVisibility(false);
            scopeObj.view.flxNoCustomerAssign.setVisibility(true);
        }
        if (scopeObj.view.btnImportCust.isVisible === false && scopeObj.custToAdd.length === 0) {
            scopeObj.view.btnImportCust.setVisibility(true);
        } else if (scopeObj.view.btnImportCust.isVisible === true && scopeObj.custToAdd.length > 0) {
            scopeObj.view.btnImportCust.setVisibility(false);
        }
        scopeObj.updateAssignedCustomerCount();
        scopeObj.view.forceLayout();
    },
    /*
     * function to show/hide the custom calendar flex when dropdown are active
     */
    showCalendarFlex: function() {
        var self = this;
        this.view.flxCalBefore.setVisibility(true);
        this.view.flxCalAfter.setVisibility(true);
        this.view.flxCalBetween.setVisibility(true);
    },
    hideBetweenCalendar: function() {
        var self = this;
        this.view.flxCalBefore.setVisibility(false);
        this.view.flxCalAfter.setVisibility(false);
        this.view.flxCalBetween.setVisibility(false);
    },
    /*
     * function to get count of rows that are available for selection
     *return-count of rows available for selection
     */
    getSelectedIndicesCount: function() {
        var self = this;
        var count = 0;
        var segData = self.view.segCustomers.data;
        for (var i = 0; i < segData.length; i++) {
            if (segData[i].btnAdd.isVisible) {
                count = count + 1;
            }
        }
        return count;
    },
    /*
     * functiont to set height and top of flex based on 
     * visbility of the filter tags in search results page
     */
    setAdvancedSearchFlexHeight: function() {
        var self = this;
        if (self.view.flxSearchFilter.isVisible) {
            self.view.flxCountAddButtons.top = "18%";
            self.view.flxResultSegment.top = "25%";
            self.view.flxResultSegment.height = "75%";
            self.view.flxResultSeg.height = "75%";
        } else {
            self.view.flxCountAddButtons.top = "10%";
            self.view.flxResultSegment.top = "17%";
            self.view.flxResultSegment.height = "80%";
            self.view.flxResultSeg.height = "80%";
        }
        self.view.forceLayout();
    },
    /*
     * function to populate status filter segment in assign customer screen
     */
    setDataForCustomerStatusFilter: function() {
        var self = this;
        var statusList = [];
        for (var i = 0; i < self.orgAssignedCust.length; i++) {
            if (!statusList.contains(self.orgAssignedCust[i].Status_id)) statusList.push(self.orgAssignedCust[i].Status_id);
        }
        var widgetMap = {
            "Status_id": "Status_id",
            "flxSearchDropDown": "flxSearchDropDown",
            "flxCheckBox": "flxCheckBox",
            "imgCheckBox": "imgCheckBox",
            "lblDescription": "lblDescription"
        };
        var statusData = statusList.map(function(rec) {
            var status;
            if (rec === self.statusConfig.custActive) {
                status = kony.i18n.getLocalizedString("i18n.secureimage.Active");
            } else if (rec === self.statusConfig.custLocked) {
                status = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Locked");
            } else {
                status = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Suspended");
            }
            return {
                "Status_id": rec,
                "flxSearchDropDown": "flxSearchDropDown",
                "flxCheckBox": "flxCheckBox",
                "imgCheckBox": "checkbox.png",
                "lblDescription": status
            };
        });
        self.view.custStatusFilter.segStatusFilterDropdown.widgetDataMap = widgetMap;
        self.view.custStatusFilter.segStatusFilterDropdown.setData(statusData);
        var selInd = [];
        for (var j = 0; j < statusList.length; j++) {
            selInd.push(j);
        }
        self.view.custStatusFilter.segStatusFilterDropdown.selectedIndices = [
            [0, selInd]
        ];
        self.view.forceLayout();
    },
    /*
     * function called filter segment data based on selected status values in assign customer
     */
    performStatusFilterInAssignCustomer: function() {
        var self = this;
        var selStatus = [];
        var selInd;
        var dataToShow = [];
        var allData = self.orgAssignedCust.concat(self.custToAdd);
        var segStatusData = self.view.custStatusFilter.segStatusFilterDropdown.data;
        var indices = self.view.custStatusFilter.segStatusFilterDropdown.selectedIndices;
        if (indices !== null) { //show selected indices data
            selInd = indices[0][1];
            for (var i = 0; i < selInd.length; i++) {
                selStatus.push(self.view.custStatusFilter.segStatusFilterDropdown.data[selInd[i]].Status_id);
            }
            self.view.rtxNoResults.setVisibility(false);
            dataToShow = allData.filter(function(rec) {
                if (selStatus.indexOf(rec.Status_id) >= 0) {
                    return rec;
                }
            });
            if (dataToShow.length > 0) {
                self.view.segAssignedCustomers.setData(dataToShow);
            } else {
                self.view.rtxNoResults.setVisibility(true);
                self.view.rtxNoResults.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
                self.view.segAssignedCustomers.setData([]);
            }
        } else {
            self.view.rtxNoResults.setVisibility(true);
            self.view.rtxNoResults.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
            self.view.segAssignedCustomers.setData([]);
        }
    },
    /*
     * set data to role type listbox in group add/edit screens
     *@param: role types data
     */
    setTypesDataToListBox: function(types) {
        var self = this;
        var listBoxData = [],
            typesData = [];
        listBoxData = types.reduce(function(list, record) {
            return list.concat([
                [record.id, record.Name]
            ]);
        }, [
            ["select", kony.i18n.getLocalizedString("i18n.frmGroups.Select_role_type")]
        ]);
        self.view.lstBoxType.masterData = listBoxData;
        self.view.lstBoxType.selectedKey = "TYPE_ID_RETAIL"; // "select";
        self.view.lstBoxType.info = types;
    },
    /*
     * hide vertical tabs based on type of role
     */
    hideUIBasedOnRoleType: function() {
        var self = this;
        var selectedItem = self.view.lstBoxType.selectedKey;
        if (selectedItem) {
            self.view.btnImportCust.setVisibility(false);
            self.view.btnImportNewCust.setVisibility(false);
            if (selectedItem === self.typeConfig.microBusiness || selectedItem === self.typeConfig.smallBusiness) {
                self.view.flxEAgreement.setVisibility(true);
                self.view.switchEAgreement.selectedIndex = 1;
                self.view.verticalTabs.flxOption3.setVisibility(false);
                self.view.verticalTabs.flxOption2.setVisibility(true);
                self.view.commonButtons.btnNext.setVisibility(false);
                this.view.flxAddImportButtons.width = "120dp";
            } else if (selectedItem === self.typeConfig.campaign) {
                self.view.flxEAgreement.setVisibility(false);
                self.view.verticalTabs.flxOption2.setVisibility(false);
                self.view.verticalTabs.flxOption3.setVisibility(true);
                self.view.commonButtons.btnNext.setVisibility(true);
                //import buttons
                self.view.btnImportCust.setVisibility(true);
                self.view.btnImportNewCust.setVisibility(true);
                this.view.flxAddImportButtons.width = "250dp";
            } else {
                self.view.flxEAgreement.setVisibility(false);
                self.view.verticalTabs.flxOption2.setVisibility(true);
                self.view.verticalTabs.flxOption3.setVisibility(true);
                self.view.commonButtons.btnNext.setVisibility(true);
                this.view.flxAddImportButtons.width = "120dp";
            }
        }
        self.view.forceLayout();
    },
    /*
     * set all UI as needed for create group
     */
    preCreateInitialization: function() {
        var scopeObj = this;
        scopeObj.view.tbxGroupNameValue.skin = "skntbxLato35475f14px";
        scopeObj.view.txtGroupDescription.skin = "skntxtAreaLato35475f14Px";
        scopeObj.view.flxNoGroupNameError.setVisibility(false);
        scopeObj.view.flxNoGroupDescriptionError.setVisibility(false);
        scopeObj.view.lblGroupDescriptionCount.text = "0/250";
        scopeObj.view.lblGroupDescriptionCount.isVisible = false;
        scopeObj.view.lblGroupNameCount.text = "0/50";
        scopeObj.view.lblGroupNameCount.isVisible = false;
        scopeObj.view.lstBoxType.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
        scopeObj.view.flxTypeError.setVisibility(false);
        scopeObj.view.switchStatus.selectedIndex = 0;
        scopeObj.view.lstBoxType.selectedKey = "TYPE_ID_RETAIL"; // "select";
        scopeObj.view.switchEAgreement.selectedIndex = 1;
        scopeObj.view.flxEAgreement.setVisibility(false);
        scopeObj.view.verticalTabs.flxOption3.setVisibility(true);
        scopeObj.view.commonButtons.btnNext.setVisibility(true);
        scopeObj.view.verticalTabs.btnOption3.info.firstClick = true;
        scopeObj.view.forceLayout();
    },
    /**
    Check for empty
    **/
    checkForUndefined: function(value) {
        var self = this;
        var returnValue = "";
        if (value === undefined || value === "undefined") {
            return returnValue;
        } else {
            return value;
        }
    },
    /*
     * toggle visibility contextual menu options based on type
     * @param: role_type_id
     */
    toggleMenuItemsBasedOnType: function(typeId) {
        var self = this;
        if (typeId === self.typeConfig.microBusiness || typeId === self.typeConfig.smallBusiness) {
            self.view.listingSegmentClient.contextualMenu.btnLink1.setVisibility(true);
            self.view.listingSegmentClient.contextualMenu.btnLink2.setVisibility(false);
        } else if (typeId === self.typeConfig.campaign) {
            self.view.listingSegmentClient.contextualMenu.btnLink1.setVisibility(false);
            self.view.listingSegmentClient.contextualMenu.btnLink2.setVisibility(true);
        } else {
            self.view.listingSegmentClient.contextualMenu.btnLink1.setVisibility(true);
            self.view.listingSegmentClient.contextualMenu.btnLink2.setVisibility(true);
        }
        self.view.forceLayout();
    },
    hideShowHeadingSeperator: function(value) {
        if (value) {
            this.view.mainHeader.flxHeaderSeperator.setVisibility(true);
        } else {
            this.view.mainHeader.flxHeaderSeperator.setVisibility(false);
        }
    },
    /*
     * fetch more records when scroll reaches to bottom/top
     */
    getResultsOnReachingend: function(context) {
        var self = this;
        var segmentWidget;
        var searchParam = self.getDefaultSearchParam();
        if (context.currentTarget.id === "frmGroups_flxAssignCustomerAdded") {
            segmentWidget = self.view.segAssignedCustomers;
        } else if (context.currentTarget.id === "frmGroups_flxViewSegment") segmentWidget = self.view.segViewSegment;
        //to record the select customer id's in a page
        if ((Math.ceil(context.currentTarget.offsetHeight) + Math.ceil(context.currentTarget.scrollTop) === Math.ceil(context.currentTarget.scrollHeight))) {
            if (self.getMoreDataModel.TotalResultsFound > (self.getMoreDataModel.PageOffset + self.getMoreDataModel.PageSize)) {
                self.addSelectedCustToList();
                searchParam["_pageOffset"] = self.getMoreDataModel.PageOffset + (self.segmentRecordsSize / 2);
                searchParam["_pageSize"] = self.segmentRecordsSize;
                searchParam["_sortVariable"] = self.getMoreDataModel.SortVariable;
                searchParam["_sortDirection"] = self.getMoreDataModel.SortDirection;
                if (context.currentTarget.id === "frmGroups_flxAssignCustomerAdded") {
                    self.view.segAssignedCustomers.selectedRowIndices = "";
                    self.presenter.fetchCustomersOfGroup(searchParam, true, "loadMore");
                } else if (context.currentTarget.id === "frmGroups_flxViewSegment" && this.view.flxCustomersHeader.isVisible) {
                    self.presenter.fetchCustomersOfGroup(searchParam, false, 3);
                }
            }
        } else if (Math.ceil(context.currentTarget.scrollTop) === 0) {
            if (self.getMoreDataModel.PageOffset > 0) {
                self.addSelectedCustToList();
                searchParam["_pageOffset"] = self.getMoreDataModel.PageOffset - (self.segmentRecordsSize / 2);
                searchParam["_pageSize"] = self.segmentRecordsSize;
                searchParam["_sortVariable"] = self.getMoreDataModel.SortVariable;
                searchParam["_sortDirection"] = self.getMoreDataModel.SortDirection;
                if (context.currentTarget.id === "frmGroups_flxAssignCustomerAdded") {
                    self.view.segAssignedCustomers.selectedRowIndices = "";
                    self.presenter.fetchCustomersOfGroup(searchParam, true, "loadMore");
                } else if (context.currentTarget.id === "frmGroups_flxViewSegment" && this.view.flxCustomersHeader.isVisible) {
                    self.presenter.fetchCustomersOfGroup(searchParam, false, 3);
                }
            }
        }
    },
    /*
     * add current page selected customer id to list
     */
    addSelectedCustToList: function() {
        var self = this;
        var custId = "";
        var selRowData = self.view.segAssignedCustomers.selectedRowItems || null;
        if (selRowData) {
            for (var i = 0; i < selRowData.length; i++) {
                custId = selRowData[i].lblCustID.info.text;
                if (self.selectedCustomerId.indexOf(custId) < 0) {
                    self.selectedCustomerId.push(custId);
                }
            }
        }
    },
    /*
     * remove customer from selected list
     * @param: customerId
     */
    removeUnselectCustFromList: function(custId) {
        var self = this;
        var index = self.selectedCustomerId.indexOf(custId);
        if (index >= 0) {
            self.selectedCustomerId.splice(index, 1);
        }
    },
    /*
     * add customers to deleted list
     * @param: customerId
     */
    addCustomersToDeleteList: function() {
        var self = this;
        var custId = "";
        var selRowData = self.view.segAssignedCustomers.selectedRowItems || null;
        if (selRowData) {
            for (var i = 0; i < selRowData.length; i++) {
                custId = selRowData[i].lblCustID.info.text;
                if (self.deletedCustomerId.indexOf(custId) < 0) {
                    self.deletedCustomerId.push(custId);
                }
            }
        }
    },
    /*
     * remove customer from the deleted list
     * @param: customerId
     */
    removeCustomerFromDeleteList: function(custId) {
        var self = this;
        var index = self.deletedCustomerId.indexOf(custId);
        if (index >= 0) {
            self.deletedCustomerId.splice(index, 1);
        }
    },
    /*
     * create search request param to fetch customers list assigned to a group
     * @return: searchrequest param json
     */
    getDefaultSearchParam: function() {
        var self = this;
        var groupId = self.getSelectedGroupId();
        var searchParam = {
            "_searchType": "GROUP_SEARCH",
            "_groupIDS": groupId,
            "_pageOffset": "0",
            "_pageSize": self.segmentRecordsSize,
            "_sortVariable": "name",
            "_sortDirection": "ASC"
        };
        return searchParam;
    },
    /*
     * get currently selected group id
     * @return: group id
     */
    getSelectedGroupId: function() {
        var self = this;
        var selRow = self.view.listingSegmentClient.segListing.selectedRowIndex;
        var groupId = "";
        if (selRow) groupId = self.view.listingSegmentClient.segListing.data[selRow[1]].Group_id;
        else groupId = null;
        return groupId;
    },
    /*
     * fetch customer id whose customers import is in progress
     */
    getCustomerInprogressList: function() {
        this.presenter.getImportStatusIdList({});
    },
    /*
     * check status of import customer CSV and display status toast message
     */
    checkCustomerImportProgress: function(data) {
        var groupId = this.getSelectedGroupId();
        var status;
        if (data.entityIds.length > 0) {
            status = JSON.parse(data.entityIds);
        } else {
            status = [];
        }
        if (status.indexOf(groupId) >= 0) {
            this.view.flxEditDisableSkin.setVisibility(true);
            this.view.toastMessage.showWarningToastMessage(kony.i18n.getLocalizedString("i18n.frmGroups.ImportInprogressToastMsg"), this, false);
        } else {
            this.view.flxEditDisableSkin.setVisibility(false);
            this.view.toastMessage.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroups.ImportCompleteToastMsg"), this);
        }
    },
    /*
     * update visibilty of entitlements/customer tabs based on type in view group details
     * @param: typeId
     */
    toggleViewGroupDetailsScreenTabs: function(typeId) {
        var self = this;
        if (typeId === self.typeConfig.campaign) {
            self.view.flxEntitlements.setVisibility(false);
            self.view.flxCustomers.setVisibility(true);
            self.view.flxEntitlementsHeader.setVisibility(false);
            self.view.flxCustomersHeader.setVisibility(true);
        } else if (typeId === self.typeConfig.microBusiness || typeId === self.typeConfig.smallBusiness) {
            self.view.flxEntitlements.setVisibility(true);
            self.view.flxCustomers.setVisibility(false);
            self.view.flxEntitlementsHeader.setVisibility(true);
            self.view.flxCustomersHeader.setVisibility(false);
        } else {
            self.view.flxEntitlements.setVisibility(true);
            self.view.flxCustomers.setVisibility(true);
            self.view.flxEntitlementsHeader.setVisibility(true);
            self.view.flxCustomersHeader.setVisibility(true);
        }
        self.view.forceLayout();
    },
    /*
     * import customers from CSV file
     */
    importCustomers: function() {
        var self = this;
        var config = {
            selectMultipleFiles: false,
            filter: ["text/csv"]
        };
        kony.io.FileSystem.browse(config, csvCallback);

        function csvCallback(event, filesList) {
            var fileType = filesList[0].name;
            var fileSize = filesList[0].size;
            fileType = fileType.substring(fileType.lastIndexOf(".") + 1);
            if (fileType === "csv" && parseInt(fileSize, 10) <= 10485760) {
                var payLoad = {
                    file: event.target.files[0],
                    roleId: self.getSelectedGroupId(),
                    User_id: kony.mvc.MDAApplication.getSharedInstance().appContext.userID
                };
                self.view.btnImportCust.info = {
                    "value": payLoad
                };
                self.showImportCustomersPopup();
            } else if (fileType !== "csv") {
                self.view.toastMessage.showErrorToastMessage(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Incorrect_file_format"), self);
            } else if (parseInt(fileSize, 10) > 10485760) {
                self.view.toastMessage.showErrorToastMessage(kony.i18n.getLocalizedString("i18n.frmLocationsController.Incorrect_file_size"), self);
            }
        }
    },
    /*
     * show popup for customers import
     */
    showImportCustomersPopup: function() {
        var self = this;
        this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmGroups.ImportCustomer_LC");
        this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmGroups.ImportCustomerPopupMsg");
        this.view.popUpDeactivate.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS");
        this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesProceed");
        this.view.flxDeactivateGroup.setVisibility(true);
        this.view.popUpDeactivate.btnPopUpDelete.onClick = function(event) {
            self.view.flxDeactivateGroup.setVisibility(false);
            self.deletedCustomerId = [];
            if (self.view.segAssignedCustomers.data.length === 0) {
                self.view.commonButtonsAddCustomers.btnSave.onClick(event);
            } else {
                self.view.commonButtonsAddedCustomers.btnSave.onClick(event);
            }
        }
        this.view.forceLayout();
    },
    /*
     * updates selectedRowIndices when clicked on the deleted row
     */
    unselectDeleteRowSelection: function() {
        var self = this;
        var custId = "",
            rowData = "",
            index;
        var selRowIndex = self.view.segAssignedCustomers.selectedRowIndex;
        var selIndices = self.view.segAssignedCustomers.selectedRowIndices;
        if (selIndices) {
            var rowInd = selRowIndex[1];
            rowData = self.view.segAssignedCustomers.data[rowInd];
            custId = rowData.lblCustID.info.text;
            if (rowData.flxDelete.isVisible === false) {
                self.view.flxAddCustomerButtons.setVisibility(true);
                self.view.flxRemoveCustButtons.setVisibility(false);
                self.updateAssignedCustomerCount();
                //removing row index
                self.removeUnselectCustFromList(custId);
                index = selIndices[0][1].splice(selRowIndex[1], 1);
                self.view.segAssignedCustomers.selectedRowIndices = [
                    [0, index]
                ];
            }
        } else {}
    },
    /*
     *update the count in assign customers tab
     */
    updateAssignedCustomerCount: function() {
        var self = this;
        var totalres = self.getMoreDataModel ? self.getMoreDataModel.TotalResultsFound : 0;
        var custCount = (totalres + self.custToAdd.length) - self.deletedCustomerId.length;
        self.view.lblCustAssignedHeader.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Assigned_Customers") + custCount + ")";
    },
});
define("CustomerGroupsModule/frmGroupsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmGroups **/
    AS_Form_ecc42658ad144373969e9f10f48dc500: function AS_Form_ecc42658ad144373969e9f10f48dc500(eventobject) {
        var self = this;
        this.PreShowGroups();
    },
    /** onDeviceBack defined for frmGroups **/
    AS_Form_fee725e711d941f89d7ce01764bf2095: function AS_Form_fee725e711d941f89d7ce01764bf2095(eventobject) {
        var self = this;
        //registered
        this.onBrowserBack();
    }
});
define("CustomerGroupsModule/frmGroupsController", ["CustomerGroupsModule/userfrmGroupsController", "CustomerGroupsModule/frmGroupsControllerActions"], function() {
    var controller = require("CustomerGroupsModule/userfrmGroupsController");
    var controllerActions = ["CustomerGroupsModule/frmGroupsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
