define("CompaniesModule/frmCompanies", function() {
    return function(controller) {
        function addWidgetsfrmCompanies() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "305dp",
                "zIndex": 1
            }, {}, {});
            flxLeftPanel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "konydbxlogobignverted.png"
                    },
                    "imgBottomLogo": {
                        "src": "konydbxinverted.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPanel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CreateCompany\")",
                        "right": "0dp"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")",
                        "isVisible": false
                    },
                    "flxButtons": {
                        "right": "35dp",
                        "width": "400dp"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.companies\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadcrumb",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadcrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "isVisible": true
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.SearchCompanies\")",
                        "isVisible": true
                    },
                    "btnPreviousPage": {
                        "isVisible": false
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "isVisible": false,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CreateCompany\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumb.add(breadcrumbs);
            var flxMainContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "88.50%",
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxSearchCompanies = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxSearchCompanies",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxSearchCompanies.setDefaultUnit(kony.flex.DP);
            var flxSearchList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxSearchList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxSearchList.setDefaultUnit(kony.flex.DP);
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "99%",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var lblSearchHeader = new kony.ui.Label({
                "id": "lblSearchHeader",
                "isVisible": true,
                "left": "35px",
                "skin": "sknLbl485C75LatoBold11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.SearchCompaniesBy\")",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeader.add(lblSearchHeader);
            var flxSearchInput = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchInput",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchInput.setDefaultUnit(kony.flex.DP);
            var flxInputs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxInputs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInputs.setDefaultUnit(kony.flex.DP);
            var flxSearchName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxSearchName.setDefaultUnit(kony.flex.DP);
            var lblSearchName = new kony.ui.Label({
                "id": "lblSearchName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertMainName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxt11abebFocus",
                "height": "40dp",
                "id": "tbxName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.CompanyName\")",
                "secureTextEntry": false,
                "skin": "sknTbxFFFFFFBorDEDEDE13pxKA",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "15px",
                "width": "98%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxSearchName.add(lblSearchName, tbxName);
            var flxSearchEmailId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchEmailId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "15px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxSearchEmailId.setDefaultUnit(kony.flex.DP);
            var lblSearchEmailId = new kony.ui.Label({
                "id": "lblSearchEmailId",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxEmailId = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxt11abebFocus",
                "height": "40dp",
                "id": "tbxEmailId",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")",
                "secureTextEntry": false,
                "skin": "sknTbxFFFFFFBorDEDEDE13pxKA",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "15px",
                "width": "98%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxSearchEmailId.add(lblSearchEmailId, tbxEmailId);
            var btnSearch = new kony.ui.Button({
                "bottom": "2dp",
                "height": "40dp",
                "id": "btnSearch",
                "isVisible": true,
                "left": "23dp",
                "skin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SEARCH\")",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknffffffHoverBorder485c75"
            });
            flxInputs.add(flxSearchName, flxSearchEmailId, btnSearch);
            var flxError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxError.setDefaultUnit(kony.flex.DP);
            var flxNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNameError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxNameError.setDefaultUnit(kony.flex.DP);
            var lblNameErrorIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNameErrorText = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNameErrorText",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocationsController.Enter_Name\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNameError.add(lblNameErrorIcon, lblNameErrorText);
            var flxEmailIdError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEmailIdError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "250px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxEmailIdError.setDefaultUnit(kony.flex.DP);
            var lblEmailIDErrorIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailIDErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailIdErrorText = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailIdErrorText",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblErrorEmailId\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailIdError.add(lblEmailIDErrorIcon, lblEmailIdErrorText);
            flxError.add(flxNameError, flxEmailIdError);
            flxSearchInput.add(flxInputs, flxError);
            flxSearch.add(flxHeader, flxSearchInput);
            var flxSpace = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSpace",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSpace.setDefaultUnit(kony.flex.DP);
            flxSpace.add();
            var flxSearchResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "590dp",
                "id": "flxSearchResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchResults.setDefaultUnit(kony.flex.DP);
            var flxSearchResultsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxSearchResultsList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxSearchResultsList.setDefaultUnit(kony.flex.DP);
            var flxSearchResultsFor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSearchResultsFor",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxFFFFFF100O",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchResultsFor.setDefaultUnit(kony.flex.DP);
            var lblResultsFor = new kony.ui.Label({
                "id": "lblResultsFor",
                "isVisible": true,
                "left": "40dp",
                "skin": "CopysknlblLatoBold0a387dc0eb1714f",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ShowingResultFor\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEnteredText = new kony.ui.Label({
                "id": "lblEnteredText",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknlblLatobold000000",
                "text": "Label",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchResultsFor.add(lblResultsFor, lblEnteredText);
            var flxHeaderPermissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxHeaderPermissions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxHeaderPermissions.setDefaultUnit(kony.flex.DP);
            var flxCompanyId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCompanyId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "55dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17%",
                "zIndex": 1
            }, {}, {});
            flxCompanyId.setDefaultUnit(kony.flex.DP);
            var lblCompanyId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCompanyId",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.COMPANY_ID\")",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCompanyId.add(lblCompanyId);
            var flxCompanyName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCompanyName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "23%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_fbe01558a0a64e0ca4625eeff7f71077,
                "skin": "slFbox",
                "top": 0,
                "width": "21%",
                "zIndex": 1
            }, {}, {});
            flxCompanyName.setDefaultUnit(kony.flex.DP);
            var lblCompanyName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCompanyName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": "37px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortName",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCompanyName.add(lblCompanyName, fontIconSortName);
            var flxContactHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxContactHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "44%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxContactHeader.setDefaultUnit(kony.flex.DP);
            var lblContactHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContactHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TYPE\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxContactHeader.add(lblContactHeader);
            var flxTIN = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTIN",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "59%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxTIN.setDefaultUnit(kony.flex.DP);
            var lblTIN = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTIN",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "TIN",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTIN.add(lblTIN);
            var flxEmailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEmailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "74%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_bed9873cfdeb4311ba234401c30421f2,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxEmailHeader.setDefaultUnit(kony.flex.DP);
            var lblEmailHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.EMAILID\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortRoles = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortRoles",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxEmailHeader.add(lblEmailHeader, fontIconSortRoles);
            var lblHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblHeaderSeperator",
                "isVisible": true,
                "left": "35dp",
                "right": "35dp",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderPermissions.add(flxCompanyId, flxCompanyName, flxContactHeader, flxTIN, flxEmailHeader, lblHeaderSeperator);
            var segSearchResults = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblCompanyId": "TRF63736",
                    "lblCompanyName": "View Permissions",
                    "lblEmail": "25",
                    "lblSeperator": ".",
                    "lblTIN": "TRF63736",
                    "lblType": "20"
                }, {
                    "lblCompanyId": "TRF63736",
                    "lblCompanyName": "View Permissions",
                    "lblEmail": "25",
                    "lblSeperator": ".",
                    "lblTIN": "TRF63736",
                    "lblType": "20"
                }, {
                    "lblCompanyId": "TRF63736",
                    "lblCompanyName": "View Permissions",
                    "lblEmail": "25",
                    "lblSeperator": ".",
                    "lblTIN": "TRF63736",
                    "lblType": "20"
                }, {
                    "lblCompanyId": "TRF63736",
                    "lblCompanyName": "View Permissions",
                    "lblEmail": "25",
                    "lblSeperator": ".",
                    "lblTIN": "TRF63736",
                    "lblType": "20"
                }, {
                    "lblCompanyId": "TRF63736",
                    "lblCompanyName": "View Permissions",
                    "lblEmail": "25",
                    "lblSeperator": ".",
                    "lblTIN": "TRF63736",
                    "lblType": "20"
                }, {
                    "lblCompanyId": "TRF63736",
                    "lblCompanyName": "View Permissions",
                    "lblEmail": "25",
                    "lblSeperator": ".",
                    "lblTIN": "TRF63736",
                    "lblType": "20"
                }, {
                    "lblCompanyId": "TRF63736",
                    "lblCompanyName": "View Permissions",
                    "lblEmail": "25",
                    "lblSeperator": ".",
                    "lblTIN": "TRF63736",
                    "lblType": "20"
                }, {
                    "lblCompanyId": "TRF63736",
                    "lblCompanyName": "View Permissions",
                    "lblEmail": "25",
                    "lblSeperator": ".",
                    "lblTIN": "TRF63736",
                    "lblType": "20"
                }, {
                    "lblCompanyId": "TRF63736",
                    "lblCompanyName": "View Permissions",
                    "lblEmail": "25",
                    "lblSeperator": ".",
                    "lblTIN": "TRF63736",
                    "lblType": "20"
                }, {
                    "lblCompanyId": "TRF63736",
                    "lblCompanyName": "View Permissions",
                    "lblEmail": "25",
                    "lblSeperator": ".",
                    "lblTIN": "TRF63736",
                    "lblType": "20"
                }],
                "groupCells": false,
                "height": "300dp",
                "id": "segSearchResults",
                "isVisible": false,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxCompanies",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCompanies": "flxCompanies",
                    "lblCompanyId": "lblCompanyId",
                    "lblCompanyName": "lblCompanyName",
                    "lblEmail": "lblEmail",
                    "lblSeperator": "lblSeperator",
                    "lblTIN": "lblTIN",
                    "lblType": "lblType"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchResultsList.add(flxSearchResultsFor, flxHeaderPermissions, segSearchResults);
            var flxNoResultsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxNoResultsFound",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxNoResultsFound.setDefaultUnit(kony.flex.DP);
            var flxNoResultsFoundCenter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxNoResultsFoundCenter",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoResultsFoundCenter.setDefaultUnit(kony.flex.DP);
            var lblNoResults = new kony.ui.Label({
                "centerX": "47%",
                "id": "lblNoResults",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.NoSearchResultsFound\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCreateCompanyLink = new kony.ui.Label({
                "id": "lblCreateCompanyLink",
                "isVisible": true,
                "left": "5px",
                "skin": "sknLblLato13px117eb0",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.createcompanylink\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl117eb013pxHov"
            });
            flxNoResultsFoundCenter.add(lblNoResults, lblCreateCompanyLink);
            flxNoResultsFound.add(flxNoResultsFoundCenter);
            flxSearchResults.add(flxSearchResultsList, flxNoResultsFound);
            flxSearchList.add(flxSearch, flxSpace, flxSearchResults);
            flxSearchCompanies.add(flxSearchList);
            var flxCreateCompany = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCreateCompany",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCreateCompany.setDefaultUnit(kony.flex.DP);
            var flxCreateCompanyContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "60dp",
                "clipBounds": true,
                "id": "flxCreateCompanyContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxCreateCompanyContainer.setDefaultUnit(kony.flex.DP);
            var flxCreateCompanyVerticalTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCreateCompanyVerticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "225dp",
                "zIndex": 1
            }, {}, {});
            flxCreateCompanyVerticalTabs.setDefaultUnit(kony.flex.DP);
            var verticalTabsCompany = new com.adminConsole.common.verticalTabs1({
                "clipBounds": true,
                "height": "100%",
                "id": "verticalTabsCompany",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.COMPANY_DETAILS\")"
                    },
                    "btnOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACCOUNTS\")"
                    },
                    "btnOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmComapnies.owner_details\")"
                    },
                    "flxOption3": {
                        "isVisible": true
                    },
                    "flxOption4": {
                        "isVisible": false
                    },
                    "imgSelected1": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected2": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected3": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected4": {
                        "src": "right_arrow2x.png"
                    },
                    "lblOptional2": {
                        "isVisible": false
                    },
                    "lblOptional3": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxVerticalTabsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxVerticalTabsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxVerticalTabsSeperator.setDefaultUnit(kony.flex.DP);
            flxVerticalTabsSeperator.add();
            flxCreateCompanyVerticalTabs.add(verticalTabsCompany, flxVerticalTabsSeperator);
            var flxCompanyDetailsTab = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCompanyDetailsTab",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCompanyDetailsTab.setDefaultUnit(kony.flex.DP);
            var flxDetailContentContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "86%",
                "horizontalScrollIndicator": true,
                "id": "flxDetailContentContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxDetailContentContainer.setDefaultUnit(kony.flex.DP);
            var flxCompanyDetailsRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "90px",
                "id": "flxCompanyDetailsRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxCompanyDetailsRow1.setDefaultUnit(kony.flex.DP);
            var flxCompanyType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxCompanyType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "46.10%",
                "zIndex": 1
            }, {}, {});
            flxCompanyType.setDefaultUnit(kony.flex.DP);
            var lstBoxSelectRole = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxSelectRole",
                "isVisible": false,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Small Business"],
                    ["lb2", "Micro Business"]
                ],
                "selectedKey": "lb2",
                "selectedKeyValue": ["lb2", "Micro Business"],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var lblCompanyTypeTitle = new kony.ui.Label({
                "id": "lblCompanyTypeTitle",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "text": "Type",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var radioCompanyType = new kony.ui.RadioButtonGroup({
                "height": "40dp",
                "id": "radioCompanyType",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["TYPE_MICRO_BUSINESS", "Micro Business"],
                    ["TYPE_SMALL_BUSINESS", "Small Business"]
                ],
                "selectedKey": "TYPE_MICRO_BUSINESS",
                "selectedKeyValue": ["TYPE_MICRO_BUSINESS", "Micro Business"],
                "skin": "slRadioButtonGroup",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCompanyType.add(lstBoxSelectRole, lblCompanyTypeTitle, radioCompanyType);
            flxCompanyDetailsRow1.add(flxCompanyType);
            var flxCompanyDetailsRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCompanyDetailsRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxCompanyDetailsRow2.setDefaultUnit(kony.flex.DP);
            var flxName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "46.40%",
                "zIndex": 1
            }, {}, {});
            flxName.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "id": "lblName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.CompanyName\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxNameValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxNameValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.CompanyName\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoNameError.setDefaultUnit(kony.flex.DP);
            var lblNoNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoNameError = new kony.ui.Label({
                "id": "lblNoNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoNameError.add(lblNoNameErrorIcon, lblNoNameError);
            flxName.add(lblName, tbxNameValue, flxNoNameError);
            var flxContactNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "105px",
                "id": "flxContactNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "46.40%",
                "zIndex": 1
            }, {}, {});
            flxContactNumber.setDefaultUnit(kony.flex.DP);
            var lblContactNumber = new kony.ui.Label({
                "id": "lblContactNumber",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CompnayContact\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var contactNumber = new com.adminConsole.common.contactNumber({
                "clipBounds": true,
                "height": "100%",
                "id": "contactNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "contactNumber": {
                        "top": "30dp"
                    },
                    "flxContactNumberWrapper": {
                        "right": "viz.val_cleared",
                        "width": "100%"
                    },
                    "flxError": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "0dp",
                        "top": "40dp",
                        "width": "100%"
                    },
                    "lblDash": {
                        "isVisible": true,
                        "left": "33%"
                    },
                    "lblErrorText": {
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "txtContactNumber": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CompnayContact\")",
                        "left": "viz.val_cleared",
                        "right": "0dp",
                        "width": "61%"
                    },
                    "txtISDCode": {
                        "isVisible": true,
                        "width": "30%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContactNumber.add(lblContactNumber, contactNumber);
            flxCompanyDetailsRow2.add(flxName, flxContactNumber);
            var flxCompanyDetailsRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "90px",
                "id": "flxCompanyDetailsRow3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxCompanyDetailsRow3.setDefaultUnit(kony.flex.DP);
            var flxMBMembershipId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxMBMembershipId",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "46.10%",
                "zIndex": 2
            }, {}, {});
            flxMBMembershipId.setDefaultUnit(kony.flex.DP);
            var lblMBMembershipId = new kony.ui.Label({
                "id": "lblMBMembershipId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Membership_ID_Capitalise\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxMBMembershipIdValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxMBMembershipIdValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Membership_ID_Capitalise\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxMBMembershipIdError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxMBMembershipIdError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMBMembershipIdError.setDefaultUnit(kony.flex.DP);
            var lblMBMembershipIdErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblMBMembershipIdErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMBMembershipIdError = new kony.ui.Label({
                "id": "lblMBMembershipIdError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.no_membership_error\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMBMembershipIdError.add(lblMBMembershipIdErrorIcon, lblMBMembershipIdError);
            flxMBMembershipId.add(lblMBMembershipId, tbxMBMembershipIdValue, flxMBMembershipIdError);
            var flxEmailID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxEmailID",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "46.10%",
                "zIndex": 2
            }, {}, {});
            flxEmailID.setDefaultUnit(kony.flex.DP);
            var lblEmailID = new kony.ui.Label({
                "id": "lblEmailID",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CompanyEmailID\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxEmailValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxEmailValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CompanyEmailID\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoEmailError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoEmailError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoEmailError.setDefaultUnit(kony.flex.DP);
            var lblNoEmailErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoEmailErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEmailError = new kony.ui.Label({
                "id": "lblNoEmailError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEmailError.add(lblNoEmailErrorIcon, lblNoEmailError);
            flxEmailID.add(lblEmailID, tbxEmailValue, flxNoEmailError);
            var flxMBTin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxMBTin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 10,
                "skin": "slFbox",
                "top": "0dp",
                "width": "46.40%",
                "zIndex": 1
            }, {}, {});
            flxMBTin.setDefaultUnit(kony.flex.DP);
            var textBoxEntryTin = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxEntryTin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnCheck": {
                        "height": "100%",
                        "left": "0dp",
                        "width": "100%"
                    },
                    "flxBtnCheck": {
                        "isVisible": false,
                        "right": "0dp",
                        "top": "30dp",
                        "width": "26%"
                    },
                    "flxEnterValue": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "30dp",
                        "width": "100%",
                        "zIndex": 2
                    },
                    "flxInlineError": {
                        "isVisible": false,
                        "left": "0dp"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.no_tin_error\")"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.TaxNumberOptional\")",
                        "left": "0dp"
                    },
                    "lblOptional": {
                        "isVisible": false
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.TaxNumber\")",
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "75%"
                    },
                    "textBoxEntry": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblTINErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblTINErrorIcon",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "75dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTinCheck = new kony.ui.Label({
                "id": "lblTinCheck",
                "isVisible": false,
                "left": "20dp",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.tin_valid_success\")",
                "top": "75dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMBTin.add(textBoxEntryTin, lblTINErrorIcon, lblTinCheck);
            flxCompanyDetailsRow3.add(flxMBMembershipId, flxEmailID, flxMBTin);
            var flxCompanyDetailsRow4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCompanyDetailsRow4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxCompanyDetailsRow4.setDefaultUnit(kony.flex.DP);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "350dp",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 20,
                "isModalContainer": false,
                "skin": "sknf8f9fabordere1e5ed",
                "top": "0dp",
                "width": "46.10%",
                "zIndex": 2
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var tbxSearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxSearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var segSearch = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }],
                "groupCells": false,
                "height": "75%",
                "id": "segSearch",
                "isVisible": false,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowTemplate": "flxSearchCompanyMap",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "40dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxSearchCompanyMap": "flxSearchCompanyMap",
                    "lblAddress": "lblAddress",
                    "lblPinIcon": "lblPinIcon"
                },
                "width": "100%",
                "zIndex": 2
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var map1 = new com.konymp.map1({
                "clipBounds": true,
                "height": "90%",
                "id": "map1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%",
                "overrides": {
                    "map1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "90%",
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "40dp"
                    },
                    "mapLocations": {
                        "height": "100%",
                        "top": "0%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchContainer.add(tbxSearch, segSearch, map1);
            var flxAddressContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "500dp",
                "id": "flxAddressContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 10,
                "skin": "slFbox",
                "top": "0dp",
                "width": "46.40%"
            }, {}, {});
            flxAddressContainer.setDefaultUnit(kony.flex.DP);
            var flxStreetName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxStreetName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxStreetName.setDefaultUnit(kony.flex.DP);
            var lblStreetName = new kony.ui.Label({
                "id": "lblStreetName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblStreetName\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxStreetName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxStreetName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblStreetName\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoStreetName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoStreetName",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoStreetName.setDefaultUnit(kony.flex.DP);
            var lblNoStreetNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStreetNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoStreetNameError = new kony.ui.Label({
                "id": "lblNoStreetNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoStreetName.add(lblNoStreetNameErrorIcon, lblNoStreetNameError);
            flxStreetName.add(lblStreetName, tbxStreetName, flxNoStreetName);
            var flxBuildingName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxBuildingName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBuildingName.setDefaultUnit(kony.flex.DP);
            var lblBuildingName = new kony.ui.Label({
                "id": "lblBuildingName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblBuildingNumber\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxBuildingName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxBuildingName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.BuildingName\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoBuildingName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoBuildingName",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoBuildingName.setDefaultUnit(kony.flex.DP);
            var lblNoBuildingNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBuildingNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoBuildingNameError = new kony.ui.Label({
                "id": "lblNoBuildingNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBuildingName.add(lblNoBuildingNameErrorIcon, lblNoBuildingNameError);
            flxBuildingName.add(lblBuildingName, tbxBuildingName, flxNoBuildingName);
            var flxZipCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxZipCode",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "180dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxZipCode.setDefaultUnit(kony.flex.DP);
            var lblZipCode = new kony.ui.Label({
                "id": "lblZipCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblzipCode\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxZipCode = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxZipCode",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblzipCode\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var imgMandatoryZip = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryZip",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoZipCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoZipCode",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoZipCode.setDefaultUnit(kony.flex.DP);
            var lblNoZipErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoZipErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoZipError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoZipError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ErrorZipCode\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoZipCode.add(lblNoZipErrorIcon, lblNoZipError);
            flxZipCode.add(lblZipCode, tbxZipCode, imgMandatoryZip, flxNoZipCode);
            var flxCountry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxCountry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "180dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxCountry.setDefaultUnit(kony.flex.DP);
            var lblCountry = new kony.ui.Label({
                "id": "lblCountry",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCountryTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCountryTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxCountryTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadCountry = new com.adminConsole.common.typeHead({
                "clipBounds": true,
                "height": "170dp",
                "id": "typeHeadCountry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")"
                    },
                    "typeHead": {
                        "height": "170dp",
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCountryTypeHead.add(typeHeadCountry);
            var imgMandatoryCountry = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryCountry",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCountry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCountry",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCountry.setDefaultUnit(kony.flex.DP);
            var lblNoCountryErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountryErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCountryError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountryError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ErrorCountry\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCountry.add(lblNoCountryErrorIcon, lblNoCountryError);
            flxCountry.add(lblCountry, flxCountryTypeHead, imgMandatoryCountry, flxNoCountry);
            var flxState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxState",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "270dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxState.setDefaultUnit(kony.flex.DP);
            var lblState = new kony.ui.Label({
                "id": "lblState",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStateTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStateTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxStateTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadState = new com.adminConsole.common.typeHead({
                "clipBounds": true,
                "height": "170dp",
                "id": "typeHeadState",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")"
                    },
                    "typeHead": {
                        "height": "170dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStateTypeHead.add(typeHeadState);
            var imgMandatoryState = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryState",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoState",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoState.setDefaultUnit(kony.flex.DP);
            var lblNoStateErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStateErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoStateError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStateError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ErrorState\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoState.add(lblNoStateErrorIcon, lblNoStateError);
            flxState.add(lblState, flxStateTypeHead, imgMandatoryState, flxNoState);
            var flxCity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxCity",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "270dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxCity.setDefaultUnit(kony.flex.DP);
            var lblCity = new kony.ui.Label({
                "id": "lblCity",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.city\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCityTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCityTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxCityTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadCity = new com.adminConsole.common.typeHead({
                "clipBounds": true,
                "height": "170dp",
                "id": "typeHeadCity",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.city\")"
                    },
                    "typeHead": {
                        "height": "170dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCityTypeHead.add(typeHeadCity);
            var imgMandatoryCity = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryCity",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCity",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCity.setDefaultUnit(kony.flex.DP);
            var lblNoCityErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCityErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCityError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCityError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ErrorCity\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCity.add(lblNoCityErrorIcon, lblNoCityError);
            flxCity.add(lblCity, flxCityTypeHead, imgMandatoryCity, flxNoCity);
            flxAddressContainer.add(flxStreetName, flxBuildingName, flxZipCode, flxCountry, flxState, flxCity);
            flxCompanyDetailsRow4.add(flxSearchContainer, flxAddressContainer);
            flxDetailContentContainer.add(flxCompanyDetailsRow1, flxCompanyDetailsRow2, flxCompanyDetailsRow3, flxCompanyDetailsRow4);
            var flxDetailsBottomButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxDetailsBottomButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsBottomButtons.setDefaultUnit(kony.flex.DP);
            var flxRightButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxRightButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxRightButtonsSeperator.add();
            var commonButtonsDetails = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "20dp"
                    },
                    "btnNext": {
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "100dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.CommonButtons.Save_AddNew\")",
                        "isVisible": false,
                        "right": "35dp"
                    },
                    "commonButtons": {
                        "bottom": "viz.val_cleared",
                        "centerY": "50%",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDetailsBottomButtons.add(flxRightButtonsSeperator, commonButtonsDetails);
            flxCompanyDetailsTab.add(flxDetailContentContainer, flxDetailsBottomButtons);
            var flxAccountsTab = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAccountsTab",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAccountsTab.setDefaultUnit(kony.flex.DP);
            var flxHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeading.setDefaultUnit(kony.flex.DP);
            var flxMembership = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMembership",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "290dp",
                "zIndex": 1
            }, {}, {});
            flxMembership.setDefaultUnit(kony.flex.DP);
            var lblMembershipHeading = new kony.ui.Label({
                "id": "lblMembershipHeading",
                "isVisible": true,
                "left": "0dp",
                "skin": "Labela1acbaae771e4f46",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.MEMBERSHIP_ID\")",
                "top": "15dp",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMembershipValue = new kony.ui.Label({
                "id": "lblMembershipValue",
                "isVisible": true,
                "left": "125dp",
                "skin": "sknlblLatoRegular00000013px",
                "text": "234163546786453216",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMembership.add(lblMembershipHeading, lblMembershipValue);
            var flxTinHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTinHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "350dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxTinHeader.setDefaultUnit(kony.flex.DP);
            var lblTinHeading = new kony.ui.Label({
                "id": "lblTinHeading",
                "isVisible": true,
                "left": "0dp",
                "skin": "Labela1acbaae771e4f46",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.TAXNUMBER\")",
                "top": "15dp",
                "width": "90dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTinValue = new kony.ui.Label({
                "id": "lblTinValue",
                "isVisible": true,
                "left": "95dp",
                "skin": "sknlblLatoRegular00000013px",
                "text": "234163546",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTinHeader.add(lblTinHeading, lblTinValue);
            var flxSepratorLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSepratorLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxSepratorLine.setDefaultUnit(kony.flex.DP);
            flxSepratorLine.add();
            flxHeading.add(flxMembership, flxTinHeader, flxSepratorLine);
            var flxAccountsAddAndRemove = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxAccountsAddAndRemove",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccountsAddAndRemove.setDefaultUnit(kony.flex.DP);
            var addAndRemoveAccounts = new com.adminConsole.common.addAndRemoveOptions1({
                "clipBounds": true,
                "height": "100%",
                "id": "addAndRemoveAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnRemoveAll": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")"
                    },
                    "btnSelectAll": {
                        "isVisible": false
                    },
                    "flxError": {
                        "left": "35%",
                        "right": "viz.val_cleared",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "imgLoadingAccountSearch": {
                        "src": "loadingscreenimage.gif"
                    },
                    "lblAvailableOptionsHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Available_Accounts\")"
                    },
                    "lblSelectedOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Selected_Accounts\")"
                    },
                    "rtxAvailableOptionsMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Search_to_see_available_accounts\")"
                    },
                    "rtxSelectedOptionsMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Select_atleast_one_account_to_proceed\")"
                    },
                    "segAddOptions": {
                        "data": [{
                            "btnAdd": "",
                            "lblFullName": "",
                            "lblUserIdValue": "",
                            "lblUsername": ""
                        }]
                    },
                    "segSelectedOptions": {
                        "data": [{
                            "fontIconClose": "",
                            "lblOption": ""
                        }]
                    },
                    "tbxSearchBox": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAccountsAddAndRemove.add(addAndRemoveAccounts);
            var flxAccBottomButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxAccBottomButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccBottomButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsAccounts = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "35dp"
                    },
                    "btnNext": {
                        "isVisible": true
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.CREATE\")",
                        "isVisible": false,
                        "right": "35dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAccButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAccButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAccButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxAccButtonsSeperator.add();
            flxAccBottomButtons.add(commonButtonsAccounts, flxAccButtonsSeperator);
            flxAccountsTab.add(flxHeading, flxAccountsAddAndRemove, flxAccBottomButtons);
            var flxOwnerDetailsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxOwnerDetailsTabs",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxOwnerDetailsTabs.setDefaultUnit(kony.flex.DP);
            var flxOwnerDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "height": "86%",
                "id": "flxOwnerDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOwnerDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxOwnerDetailsRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxOwnerDetailsRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOwnerDetailsRow1.setDefaultUnit(kony.flex.DP);
            var flxOwnerDetailsColumn11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOwnerDetailsColumn11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%"
            }, {}, {});
            flxOwnerDetailsColumn11.setDefaultUnit(kony.flex.DP);
            var textBoxOwnerDetailsEntry11 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxOwnerDetailsEntry11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxInlineError": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "right": "viz.val_cleared"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstName\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstName\")",
                        "right": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOwnerDetailsColumn11.add(textBoxOwnerDetailsEntry11);
            var flxOwnerDetailsColumn12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOwnerDetailsColumn12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%",
                "zIndex": 1
            }, {}, {});
            flxOwnerDetailsColumn12.setDefaultUnit(kony.flex.DP);
            var textBoxOwnerDetailsEntry12 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxOwnerDetailsEntry12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": true,
                        "left": "100dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOwnerDetailsColumn12.add(textBoxOwnerDetailsEntry12);
            var flxOwnerDetailsColumn13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOwnerDetailsColumn13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.60%",
                "zIndex": 1
            }, {}, {});
            flxOwnerDetailsColumn13.setDefaultUnit(kony.flex.DP);
            var textBoxOwnerDetailsEntry13 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxOwnerDetailsEntry13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOwnerDetailsColumn13.add(textBoxOwnerDetailsEntry13);
            flxOwnerDetailsRow1.add(flxOwnerDetailsColumn11, flxOwnerDetailsColumn12, flxOwnerDetailsColumn13);
            var flxOwnerDetailsRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxOwnerDetailsRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOwnerDetailsRow2.setDefaultUnit(kony.flex.DP);
            var flxOwnerDetailsColumn21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOwnerDetailsColumn21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%"
            }, {}, {});
            flxOwnerDetailsColumn21.setDefaultUnit(kony.flex.DP);
            var textBoxOwnerDetailsEntry21 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxOwnerDetailsEntry21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.DOB\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.DOB\")",
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxCalendarDOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCalendarDOB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "25dp",
                "width": "95.20%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxCalendarDOB.setDefaultUnit(kony.flex.DP);
            var customCalOwnerDOB = new kony.ui.CustomWidget({
                "id": "customCalOwnerDOB",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "maxDate": "true",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "single",
                "value": ""
            });
            var flxDOBDisabled = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxDOBDisabled",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "42dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxDOBDisabled.setDefaultUnit(kony.flex.DP);
            flxDOBDisabled.add();
            flxCalendarDOB.add(customCalOwnerDOB, flxDOBDisabled);
            flxOwnerDetailsColumn21.add(textBoxOwnerDetailsEntry21, flxCalendarDOB);
            var flxOwnerDetailsColumn22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOwnerDetailsColumn22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%",
                "zIndex": 1
            }, {}, {});
            flxOwnerDetailsColumn22.setDefaultUnit(kony.flex.DP);
            var textBoxOwnerDetailsEntry22 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxOwnerDetailsEntry22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOwnerDetailsColumn22.add(textBoxOwnerDetailsEntry22);
            var flxOwnerDetailsColumn23 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOwnerDetailsColumn23",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.60%",
                "zIndex": 1
            }, {}, {});
            flxOwnerDetailsColumn23.setDefaultUnit(kony.flex.DP);
            var textBoxOwnerDetailsEntry23 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxOwnerDetailsEntry23",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxInlineError": {
                        "isVisible": false,
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOwnerDetailsColumn23.add(textBoxOwnerDetailsEntry23);
            flxOwnerDetailsRow2.add(flxOwnerDetailsColumn21, flxOwnerDetailsColumn22, flxOwnerDetailsColumn23);
            var flxOwnerDetailsRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120dp",
                "id": "flxOwnerDetailsRow3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOwnerDetailsRow3.setDefaultUnit(kony.flex.DP);
            var flxOwnerDetailsColumn31 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOwnerDetailsColumn31",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxOwnerDetailsColumn31.setDefaultUnit(kony.flex.DP);
            var lblHeading = new kony.ui.Label({
                "id": "lblHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchParam4\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var textBoxOwnerDetailsEntry31 = new com.adminConsole.common.contactNumber({
                "clipBounds": true,
                "height": "90%",
                "id": "textBoxOwnerDetailsEntry31",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "26dp",
                "overrides": {
                    "contactNumber": {
                        "height": "90%",
                        "left": "20dp",
                        "right": "0dp",
                        "top": "26dp",
                        "width": "viz.val_cleared"
                    },
                    "flxContactNumberWrapper": {
                        "height": "90dp",
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "flxError": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "0dp",
                        "right": "0dp",
                        "top": "44dp",
                        "width": "viz.val_cleared"
                    },
                    "lblDash": {
                        "left": "70dp"
                    },
                    "lblErrorText": {
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ContactNo_cannot_be_empty\")",
                        "left": "20dp",
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "txtContactNumber": {
                        "left": "80dp",
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "txtISDCode": {
                        "width": "70dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOwnerDetailsColumn31.add(lblHeading, textBoxOwnerDetailsEntry31);
            flxOwnerDetailsRow3.add(flxOwnerDetailsColumn31);
            flxOwnerDetailsContainer.add(flxOwnerDetailsRow1, flxOwnerDetailsRow2, flxOwnerDetailsRow3);
            var flxDetailsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxDetailsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsOwnerDetails = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsOwnerDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "20dp"
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.CREATE\")",
                        "isVisible": true,
                        "right": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDetailsButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxDetailsButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxDetailsButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxDetailsButtonsSeperator.add();
            flxDetailsButtons.add(commonButtonsOwnerDetails, flxDetailsButtonsSeperator);
            flxOwnerDetailsTabs.add(flxOwnerDetailsContainer, flxDetailsButtons);
            flxCreateCompanyContainer.add(flxCreateCompanyVerticalTabs, flxCompanyDetailsTab, flxAccountsTab, flxOwnerDetailsTabs);
            var flxInlineError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxInlineError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInlineError.setDefaultUnit(kony.flex.DP);
            var lblInlineErrorIcon = new kony.ui.Label({
                "centerX": "41%",
                "centerY": "52%",
                "id": "lblInlineErrorIcon",
                "isVisible": true,
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblInlineError = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblInlineError",
                "isVisible": true,
                "skin": "sknLabelRed",
                "text": "Please select an account",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInlineError.add(lblInlineErrorIcon, lblInlineError);
            flxCreateCompany.add(flxCreateCompanyContainer, flxInlineError);
            var flxCompanyDetails = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "30dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxCompanyDetails",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxCompanyDetails.setDefaultUnit(kony.flex.DP);
            var flxMainDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMainDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "20dp",
                "width": "94.50%",
                "zIndex": 1
            }, {}, {});
            flxMainDetails.setDefaultUnit(kony.flex.DP);
            var flxNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75dp",
                "id": "flxNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "15dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxNameHeader.setDefaultUnit(kony.flex.DP);
            var flxTypeFlag = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxTypeFlag",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxTypeFlag.setDefaultUnit(kony.flex.DP);
            var flxFlagBackground = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFlagBackground",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxCustomertagPurple",
                "top": "5dp",
                "width": "100dp",
                "zIndex": 1
            }, {}, {});
            flxFlagBackground.setDefaultUnit(kony.flex.DP);
            var flxDot = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "5dp",
                "id": "flxDot",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFRounded",
                "top": "6dp",
                "width": "5dp",
                "zIndex": 1
            }, {}, {});
            flxDot.setDefaultUnit(kony.flex.DP);
            flxDot.add();
            var lblTypeValue = new kony.ui.Label({
                "centerY": "45%",
                "id": "lblTypeValue",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLblFFFFFLatoRegular12Px",
                "text": "Small Business",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFlagBackground.add(flxDot, lblTypeValue);
            flxTypeFlag.add(flxFlagBackground);
            var lblCompanyDetailName = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblCompanyDetailName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoRegular485c7518Px",
                "text": "Kony",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnCompanyDetailEdit = new kony.ui.Button({
                "height": "20px",
                "id": "btnCompanyDetailEdit",
                "isVisible": true,
                "right": "20px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "20px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            var flxLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "25dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxLine.setDefaultUnit(kony.flex.DP);
            flxLine.add();
            flxNameHeader.add(flxTypeFlag, lblCompanyDetailName, btnCompanyDetailEdit, flxLine);
            var flxSamllBusiness = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSamllBusiness",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75dp",
                "width": "100%"
            }, {}, {});
            flxSamllBusiness.setDefaultUnit(kony.flex.DP);
            var lblCompanyDetailEmail = new kony.ui.Label({
                "id": "lblCompanyDetailEmail",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.COMPANY_EMAIL\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCompanyDetailEmailValue = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblCompanyDetailEmailValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "info@kony.com",
                "top": "35dp",
                "width": "25%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCompanyDetailContact = new kony.ui.Label({
                "id": "lblCompanyDetailContact",
                "isVisible": true,
                "left": "30%",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.COMPANY_CONTACT\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCompanyDetailContactValue = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblCompanyDetailContactValue",
                "isVisible": true,
                "left": "30%",
                "skin": "sknlblLatoBold35475f14px",
                "text": "123-09821721",
                "top": "35dp",
                "width": "25%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCompanyDetailAddress = new kony.ui.Label({
                "id": "lblCompanyDetailAddress",
                "isVisible": true,
                "left": "57%",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.ADDRESS\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCompanyDetailAddressValue = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblCompanyDetailAddressValue",
                "isVisible": true,
                "left": "57%",
                "right": "25dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibends laors proin gravida dolor sit",
                "top": "35dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSamllBusiness.add(lblCompanyDetailEmail, lblCompanyDetailEmailValue, lblCompanyDetailContact, lblCompanyDetailContactValue, lblCompanyDetailAddress, lblCompanyDetailAddressValue);
            var flxMicroBusiness = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMicroBusiness",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75dp",
                "width": "100%"
            }, {}, {});
            flxMicroBusiness.setDefaultUnit(kony.flex.DP);
            var flxMBBasicDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMBBasicDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxMBBasicDetails.setDefaultUnit(kony.flex.DP);
            var lblMBCompanyId = new kony.ui.Label({
                "id": "lblMBCompanyId",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.COMPANY_ID\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMBCompanyIdValue = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblMBCompanyIdValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "1233324242424242",
                "top": "35dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMBTin = new kony.ui.Label({
                "id": "lblMBTin",
                "isVisible": true,
                "left": "35%",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.TAXNUMBER\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMBTinValue = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblMBTinValue",
                "isVisible": true,
                "left": "35%",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "konyLtd@kony.com",
                "top": "35dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMBEmail = new kony.ui.Label({
                "id": "lblMBEmail",
                "isVisible": true,
                "left": "66%",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.COMPANY_EMAIL\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMBEmailValue = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblMBEmailValue",
                "isVisible": true,
                "left": "66%",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "info@kony.com",
                "top": "35dp",
                "width": "21%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMBContact = new kony.ui.Label({
                "id": "lblMBContact",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.COMPANY_CONTACT\")",
                "top": "70dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMBContactValue = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblMBContactValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "123-09821721",
                "top": "95dp",
                "width": "14%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMBAddress = new kony.ui.Label({
                "id": "lblMBAddress",
                "isVisible": true,
                "left": "35%",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.ADDRESS\")",
                "top": "70dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMBAddressValue = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblMBAddressValue",
                "isVisible": true,
                "left": "35%",
                "right": "30dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibends laors proin gravida dolor sit",
                "top": "95dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMBBasicDetails.add(lblMBCompanyId, lblMBCompanyIdValue, lblMBTin, lblMBTinValue, lblMBEmail, lblMBEmailValue, lblMBContact, lblMBContactValue, lblMBAddress, lblMBAddressValue);
            var flxOwnerDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxOwnerDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOwnerDetails.setDefaultUnit(kony.flex.DP);
            var flxOwnershipHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxOwnershipHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "130dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxOwnershipHeader.setDefaultUnit(kony.flex.DP);
            var lblOwnershipHeader = new kony.ui.Label({
                "id": "lblOwnershipHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "text": "Ownership Details",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblArrowIcon = new kony.ui.Label({
                "id": "lblArrowIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknfontIconDescRightArrow14px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOwnershipHeader.add(lblOwnershipHeader, lblArrowIcon);
            var flxMBOwnerDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMBOwnerDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "20dp"
            }, {}, {});
            flxMBOwnerDetails.setDefaultUnit(kony.flex.DP);
            var lblOwnerName = new kony.ui.Label({
                "id": "lblOwnerName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOwnerNameValue = new kony.ui.Label({
                "bottom": "20dp",
                "id": "lblOwnerNameValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "Kony",
                "top": "35dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOwnerEmail = new kony.ui.Label({
                "id": "lblOwnerEmail",
                "isVisible": true,
                "left": "35%",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.EMAILID\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOwnerEmailValue = new kony.ui.Label({
                "bottom": "20dp",
                "id": "lblOwnerEmailValue",
                "isVisible": true,
                "left": "35%",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "comapny@kony.com",
                "top": "35dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOwnerContact = new kony.ui.Label({
                "id": "lblOwnerContact",
                "isVisible": true,
                "left": "66%",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CONTACT\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOwnerContactValue = new kony.ui.Label({
                "bottom": "20dp",
                "id": "lblOwnerContactValue",
                "isVisible": true,
                "left": "66%",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "12-3434353533",
                "top": "35dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOwnerSSN = new kony.ui.Label({
                "id": "lblOwnerSSN",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN_HEADER\")",
                "top": "70dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOwnerSSNValue = new kony.ui.Label({
                "bottom": "20dp",
                "id": "lblOwnerSSNValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "09821721",
                "top": "95dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOwnerDOB = new kony.ui.Label({
                "id": "lblOwnerDOB",
                "isVisible": true,
                "left": "35%",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.DOB\")",
                "top": "70dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOwnerDOBValue = new kony.ui.Label({
                "bottom": "20dp",
                "id": "lblOwnerDOBValue",
                "isVisible": true,
                "left": "35%",
                "right": "25dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "07-12-1994",
                "top": "95dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMBOwnerDetails.add(lblOwnerName, lblOwnerNameValue, lblOwnerEmail, lblOwnerEmailValue, lblOwnerContact, lblOwnerContactValue, lblOwnerSSN, lblOwnerSSNValue, lblOwnerDOB, lblOwnerDOBValue);
            flxOwnerDetails.add(flxOwnershipHeader, flxMBOwnerDetails);
            flxMicroBusiness.add(flxMBBasicDetails, flxOwnerDetails);
            flxMainDetails.add(flxNameHeader, flxSamllBusiness, flxMicroBusiness);
            var flxSubDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "150%",
                "id": "flxSubDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "20dp",
                "width": "94.50%",
                "zIndex": 1
            }, {}, {});
            flxSubDetails.setDefaultUnit(kony.flex.DP);
            var flxViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxViewTabs.setDefaultUnit(kony.flex.DP);
            var flxViewTab1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "70dp",
                "zIndex": 1
            }, {}, {});
            flxViewTab1.setDefaultUnit(kony.flex.DP);
            var lblTabName1 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblTabName1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACCOUNTS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [5, 0, 5, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            flxViewTab1.add(lblTabName1);
            var flxViewTab2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "120dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewTab2.setDefaultUnit(kony.flex.DP);
            var lblTabName2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblTabName2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.BUSINESS_USERS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [5, 0, 5, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            flxViewTab2.add(lblTabName2);
            flxViewTabs.add(flxViewTab1, flxViewTab2);
            var flxViewSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxD5D9DD",
                "top": "65dp",
                "zIndex": 1
            }, {}, {});
            flxViewSeperator.setDefaultUnit(kony.flex.DP);
            flxViewSeperator.add();
            var flxCompanyDetailAccountsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCompanyDetailAccountsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "65dp"
            }, {}, {});
            flxCompanyDetailAccountsContainer.setDefaultUnit(kony.flex.DP);
            var flxAccountSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "17dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxAccountSegment.setDefaultUnit(kony.flex.DP);
            var flxAccountsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxAccountsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccountsHeader.setDefaultUnit(kony.flex.DP);
            var flxAccountType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "55px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18.50%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAccountType.setDefaultUnit(kony.flex.DP);
            var lblAccountType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTTYPE\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblAccountTypeSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountTypeSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountType.add(lblAccountType, lblAccountTypeSortIcon);
            var flxAccountName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "22.50%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAccountName.setDefaultUnit(kony.flex.DP);
            var lblAccountName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNAME\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblAccountNameSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountNameSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountName.add(lblAccountName, lblAccountNameSortIcon);
            var flxAccountNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountNumber",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAccountNumber.setDefaultUnit(kony.flex.DP);
            var lblAccountNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountNumber",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNUMBER\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblAccountNumberSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountNumberSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountNumber.add(lblAccountNumber, lblAccountNumberSortIcon);
            var flxAccountTin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountTin",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17.50%",
                "zIndex": 1
            }, {}, {});
            flxAccountTin.setDefaultUnit(kony.flex.DP);
            var lblAccountTin = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountTin",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.TAXNUMBER\")",
                "top": 0,
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxAccountTin.add(lblAccountTin);
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblStatusSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatusSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStatus.add(lblStatus, lblStatusSortIcon);
            flxAccountsHeader.add(flxAccountType, flxAccountName, flxAccountNumber, flxAccountTin, flxStatus);
            var segCompanyDetailAccount = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "btnAccountOwner": "",
                    "flblUnlink": "",
                    "fontIconStatus": "",
                    "lblAccountNumber": "",
                    "lblAccountOwner": "",
                    "lblProductName": "",
                    "lblProductType": "",
                    "lblSeperator": "",
                    "lblStatus": ""
                }],
                "groupCells": false,
                "height": "86%",
                "id": "segCompanyDetailAccount",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustMangProductInfo",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "65dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnAccountOwner": "btnAccountOwner",
                    "flblUnlink": "flblUnlink",
                    "flxAccountOwner": "flxAccountOwner",
                    "flxCustMangProduct": "flxCustMangProduct",
                    "flxCustMangProductInfo": "flxCustMangProductInfo",
                    "flxProductType": "flxProductType",
                    "flxStatus": "flxStatus",
                    "flxUnlink": "flxUnlink",
                    "fontIconStatus": "fontIconStatus",
                    "lblAccountNumber": "lblAccountNumber",
                    "lblAccountOwner": "lblAccountOwner",
                    "lblProductName": "lblProductName",
                    "lblProductType": "lblProductType",
                    "lblSeperator": "lblSeperator",
                    "lblStatus": "lblStatus"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAccountsHeaderSepartor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxAccountsHeaderSepartor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknTableHeaderLine",
                "top": "65dp",
                "zIndex": 1
            }, {}, {});
            flxAccountsHeaderSepartor.setDefaultUnit(kony.flex.DP);
            flxAccountsHeaderSepartor.add();
            flxAccountSegment.add(flxAccountsHeader, segCompanyDetailAccount, flxAccountsHeaderSepartor);
            var flxCompanyAccountsDetail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCompanyAccountsDetail",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "25dp",
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCompanyAccountsDetail.setDefaultUnit(kony.flex.DP);
            var backToAccounts = new com.adminConsole.customerMang.backToPageHeader({
                "clipBounds": true,
                "height": "20px",
                "id": "backToAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "backToPageHeader": {
                        "top": "10dp"
                    },
                    "btnBack": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACCOUNTS_BTN_BACK\")"
                    },
                    "flxBack": {
                        "left": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var AccountsHeader = new com.adminConsole.common.productHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "AccountsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "blAccountNumber": {
                        "left": "2dp",
                        "right": "viz.val_cleared",
                        "text": "9897765676545678",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxAccountDetailsColumn2": {
                        "width": "30%",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "lblAccountNumberLabel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNUMBERColon\")",
                        "right": "0px"
                    },
                    "lblIBAN": {
                        "centerY": "50%",
                        "isVisible": false
                    },
                    "lblIBANLabel": {
                        "isVisible": false
                    },
                    "lblProductCardName": {
                        "left": "0dp"
                    },
                    "lblSeperator": {
                        "bottom": "2px",
                        "isVisible": false
                    },
                    "productHeader": {
                        "height": "60px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAccountsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAccountsTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccountsTabs.setDefaultUnit(kony.flex.DP);
            var flxAccountsTabsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAccountsTabsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxedefefOp100NoBorderRadius3Px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAccountsTabsWrapper.setDefaultUnit(kony.flex.DP);
            flxAccountsTabsWrapper.add();
            var btnTransactionHistory = new kony.ui.Button({
                "height": "37px",
                "id": "btnTransactionHistory",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnTransactionHistory\")",
                "top": "13px",
                "width": "160dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnProductsDetails = new kony.ui.Button({
                "height": "37px",
                "id": "btnProductsDetails",
                "isVisible": true,
                "left": "180px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnProductDetails\")",
                "top": "13px",
                "width": "130dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountsTabs.add(flxAccountsTabsWrapper, btnTransactionHistory, btnProductsDetails);
            var flxTransactionContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "74.36%",
                "id": "flxTransactionContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactionContainer.setDefaultUnit(kony.flex.DP);
            var flxAccuontsStatusTab = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxAccuontsStatusTab",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccuontsStatusTab.setDefaultUnit(kony.flex.DP);
            var flxRecent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRecent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "55dp",
                "zIndex": 1
            }, {}, {});
            flxRecent.setDefaultUnit(kony.flex.DP);
            var lblRecent = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "lblRecent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RECENT\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [5, 0, 5, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            flxRecent.add(lblRecent);
            var flxPending = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPending",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "85dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxPending.setDefaultUnit(kony.flex.DP);
            var lblPending = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblPending",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PENDING\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [5, 0, 5, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            flxPending.add(lblPending);
            var flxScheduled = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxScheduled",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "165dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxScheduled.setDefaultUnit(kony.flex.DP);
            var lblScheduled = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblScheduled",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SCHEDULED\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [5, 0, 5, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            flxScheduled.add(lblScheduled);
            var flxAccountStatusSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAccountStatusSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxD5D9DD",
                "zIndex": 1
            }, {}, {});
            flxAccountStatusSeprator.setDefaultUnit(kony.flex.DP);
            flxAccountStatusSeprator.add();
            flxAccuontsStatusTab.add(flxRecent, flxPending, flxScheduled, flxAccountStatusSeprator);
            var flxTransactionHistory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "83%",
                "id": "flxTransactionHistory",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactionHistory.setDefaultUnit(kony.flex.DP);
            var flxTransactionHistorySearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "61px",
                "id": "flxTransactionHistorySearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "25dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxTransactionHistorySearch.setDefaultUnit(kony.flex.DP);
            var flxStart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStart",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 225,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 10
            }, {}, {});
            flxStart.setDefaultUnit(kony.flex.DP);
            var calStartDate = new kony.ui.CustomWidget({
                "id": "calStartDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "18px",
                "top": "18px",
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "opens": "right",
                "rangeType": null,
                "resetData": "Start Date",
                "type": "single",
                "uniqueId": "calStartDate",
                "value": ""
            });
            flxStart.add(calStartDate);
            var flxEndDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEndDate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "380px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 10
            }, {}, {});
            flxEndDate.setDefaultUnit(kony.flex.DP);
            var calEndDate = new kony.ui.CustomWidget({
                "id": "calEndDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "18px",
                "top": "18px",
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "opens": "right",
                "rangeType": null,
                "resetData": "End Date",
                "type": "single",
                "uniqueId": "calEndDate",
                "value": ""
            });
            flxEndDate.add(calEndDate);
            var flxCreatedRangePicker = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "33px",
                "id": "flxCreatedRangePicker",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25px",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "width": "200px",
                "zIndex": 10
            }, {}, {});
            flxCreatedRangePicker.setDefaultUnit(kony.flex.DP);
            var customCalCreatedDate = new kony.ui.CustomWidget({
                "id": "customCalCreatedDate",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "2px",
                "top": "5dp",
                "width": "167px",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "list",
                "value": "Last 7 Days"
            });
            var flxCloseCal1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxCloseCal1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal1.setDefaultUnit(kony.flex.DP);
            var lblCloseCal1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCloseCal1",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal1.add(lblCloseCal1);
            flxCreatedRangePicker.add(customCalCreatedDate, flxCloseCal1);
            var btnApply = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "btnApply",
                "isVisible": true,
                "left": "240px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
                "width": "55px",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            var transactionHistorySearch = new com.adminConsole.history.search({
                "clipBounds": true,
                "height": "60px",
                "id": "transactionHistorySearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "overrides": {
                    "flxListboxAndCalendar": {
                        "width": "50%"
                    },
                    "flxSearchAndDownload": {
                        "left": "viz.val_cleared",
                        "right": "0px"
                    },
                    "imgClearSearch": {
                        "src": "close_blue.png"
                    },
                    "imgDownload": {
                        "src": "download_2x.png"
                    },
                    "imgSearchIcon": {
                        "src": "search_1x.png"
                    },
                    "lblShowing": {
                        "isVisible": false
                    },
                    "lbxPageNumbers": {
                        "isVisible": false
                    },
                    "search": {
                        "left": "0dp",
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            transactionHistorySearch.tbxSearchBox.onTouchStart = controller.AS_TextField_gf9a2570adc34581b930bf1d5169b593;
            var flxSeperator3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperator3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "60px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeperator3.setDefaultUnit(kony.flex.DP);
            flxSeperator3.add();
            flxTransactionHistorySearch.add(flxStart, flxEndDate, flxCreatedRangePicker, btnApply, transactionHistorySearch, flxSeperator3);
            var flxTransactionHistorySegmentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxTransactionHistorySegmentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxTransactionHistorySegmentHeader.setDefaultUnit(kony.flex.DP);
            var flxTranasctionRefNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTranasctionRefNo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "25px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "10.92%",
                "zIndex": 1
            }, {}, {});
            flxTranasctionRefNo.setDefaultUnit(kony.flex.DP);
            var lblTranasctionRefNo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTranasctionRefNo",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblTranasctionRefNo\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTranasctionRefNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTranasctionRefNo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTranasctionRefNo.setDefaultUnit(kony.flex.DP);
            var imgSortTranasctionRefNo = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortTranasctionRefNo",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortTranasctionRefNo = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortTranasctionRefNo",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortTranasctionRefNo.add(imgSortTranasctionRefNo, fonticonSortTranasctionRefNo);
            flxTranasctionRefNo.add(lblTranasctionRefNo, flxSortTranasctionRefNo);
            var flxTransactionDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "15%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12.06%",
                "zIndex": 1
            }, {}, {});
            flxTransactionDateAndTime.setDefaultUnit(kony.flex.DP);
            var lblTransactionDateAndTime = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionDateAndTime",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblDateAndTime\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionDateAndTime.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionDateAndTime = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortTransactionDateAndTime",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortTransactionDateAndTime = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortTransactionDateAndTime",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortTransactionDateAndTime.add(imgSortTransactionDateAndTime, fonticonSortTransactionDateAndTime);
            flxTransactionDateAndTime.add(lblTransactionDateAndTime, flxSortTransactionDateAndTime);
            var flxTransactionDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "29.50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "10.91%",
                "zIndex": 1
            }, {}, {});
            flxTransactionDescription.setDefaultUnit(kony.flex.DP);
            var lblTransactionDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "text": "DESCRIPTION",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTransactionDescription.add(lblTransactionDescription);
            var flxTransactionType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "54.5%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "6.50%",
                "zIndex": 1
            }, {}, {});
            flxTransactionType.setDefaultUnit(kony.flex.DP);
            var lblTransactionType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TYPE\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionType.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionType = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortTransactionType",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortTransactionType = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortTransactionType",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortTransactionType.add(imgSortTransactionType, fonticonSortTransactionType);
            flxTransactionType.add(lblTransactionType, flxSortTransactionType);
            var flxTransactionAmountOriginal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionAmountOriginal",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "67.5%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxTransactionAmountOriginal.setDefaultUnit(kony.flex.DP);
            var lblTransactionAmountOriginal = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionAmountOriginal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblTransactionAmountOriginal\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionAmountOriginal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionAmountOriginal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "19px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionAmountOriginal.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionAmountOriginal = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortTransactionAmountOriginal",
                "isVisible": false,
                "right": "0px",
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortTransactionAmountOriginal = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortTransactionAmountOriginal",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortTransactionAmountOriginal.add(imgSortTransactionAmountOriginal, fonticonSortTransactionAmountOriginal);
            flxTransactionAmountOriginal.add(lblTransactionAmountOriginal, flxSortTransactionAmountOriginal);
            var flxTransactionAmountConverted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionAmountConverted",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "82%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12.56%",
                "zIndex": 1
            }, {}, {});
            flxTransactionAmountConverted.setDefaultUnit(kony.flex.DP);
            var lblTransactionAmountConverted = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionAmountConverted",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblTransactionAmountConverted\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionAmountConverted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionAmountConverted",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "19px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionAmountConverted.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionAmountConverted = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortTransactionAmountConverted",
                "isVisible": false,
                "right": "0px",
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fonticonSortTransactionAmountConverted = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortTransactionAmountConverted",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortTransactionAmountConverted.add(imgSortTransactionAmountConverted, fonticonSortTransactionAmountConverted);
            flxTransactionAmountConverted.add(lblTransactionAmountConverted, flxSortTransactionAmountConverted);
            flxTransactionHistorySegmentHeader.add(flxTranasctionRefNo, flxTransactionDateAndTime, flxTransactionDescription, flxTransactionType, flxTransactionAmountOriginal, flxTransactionAmountConverted);
            var flxSeperator4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperator4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxd6dbe7",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxSeperator4.setDefaultUnit(kony.flex.DP);
            flxSeperator4.add();
            var flxScrollTransctionsSegment = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "63%",
                "id": "flxScrollTransctionsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "maxHeight": "250px",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScrollTransctionsSegment.setDefaultUnit(kony.flex.DP);
            var segTransactionHistory = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblAmountConverted": "990,000.00",
                    "lblAmountConvertedSign": "-",
                    "lblAmountConvertedSymbol": "",
                    "lblAmountOriginal": "100,000.00",
                    "lblAmountOriginalSign": "-",
                    "lblAmountOriginalSymbol": "",
                    "lblDateAndTime": "01/01/2019 10:30",
                    "lblRefNo": "RI123456",
                    "lblSeperator": ".",
                    "lblTransctionDescription": "Payment",
                    "lblType": "Fund transfer"
                }, {
                    "lblAmountConverted": "990,000.00",
                    "lblAmountConvertedSign": "+",
                    "lblAmountConvertedSymbol": "",
                    "lblAmountOriginal": "100,000.00",
                    "lblAmountOriginalSign": "-",
                    "lblAmountOriginalSymbol": "",
                    "lblDateAndTime": "01/01/2019 10:30",
                    "lblRefNo": "RI123456",
                    "lblSeperator": ".",
                    "lblTransctionDescription": "Payment",
                    "lblType": "Fund transfer"
                }, {
                    "lblAmountConverted": "990,000.00",
                    "lblAmountConvertedSign": "-",
                    "lblAmountConvertedSymbol": "",
                    "lblAmountOriginal": "100,000.00",
                    "lblAmountOriginalSign": "-",
                    "lblAmountOriginalSymbol": "",
                    "lblDateAndTime": "01/01/2019 10:30",
                    "lblRefNo": "RI123456",
                    "lblSeperator": ".",
                    "lblTransctionDescription": "Payment",
                    "lblType": "Fund transfer"
                }, {
                    "lblAmountConverted": "990,000.00",
                    "lblAmountConvertedSign": "-",
                    "lblAmountConvertedSymbol": "",
                    "lblAmountOriginal": "100,000.00",
                    "lblAmountOriginalSign": "+",
                    "lblAmountOriginalSymbol": "",
                    "lblDateAndTime": "01/01/2019 10:30",
                    "lblRefNo": "RI123456",
                    "lblSeperator": "",
                    "lblTransctionDescription": "Payment",
                    "lblType": "Fund transfer"
                }, {
                    "lblAmountConverted": "100,000.00",
                    "lblAmountConvertedSign": "-",
                    "lblAmountConvertedSymbol": "",
                    "lblAmountOriginal": "100,000.00",
                    "lblAmountOriginalSign": "+",
                    "lblAmountOriginalSymbol": "",
                    "lblDateAndTime": "01/01/2019 10:30",
                    "lblRefNo": "RI123456",
                    "lblSeperator": "",
                    "lblTransctionDescription": "Payment",
                    "lblType": "Fund transfer"
                }, {
                    "lblAmountConverted": "100,000.00100,000.00",
                    "lblAmountConvertedSign": "+",
                    "lblAmountConvertedSymbol": "",
                    "lblAmountOriginal": "100,000.00",
                    "lblAmountOriginalSign": "+",
                    "lblAmountOriginalSymbol": "",
                    "lblDateAndTime": "01/01/2019 10:30",
                    "lblRefNo": "RI123456",
                    "lblSeperator": "",
                    "lblTransctionDescription": "Payment",
                    "lblType": "Fund transfer"
                }, {
                    "lblAmountConverted": "100,000.00",
                    "lblAmountConvertedSign": "+",
                    "lblAmountConvertedSymbol": "",
                    "lblAmountOriginal": "100,000.00",
                    "lblAmountOriginalSign": "-",
                    "lblAmountOriginalSymbol": "",
                    "lblDateAndTime": "01/01/2019 10:30",
                    "lblRefNo": "RI123456",
                    "lblSeperator": "",
                    "lblTransctionDescription": "Payment",
                    "lblType": "Fund transfer"
                }, {
                    "lblAmountConverted": "100,000.00",
                    "lblAmountConvertedSign": "-",
                    "lblAmountConvertedSymbol": "",
                    "lblAmountOriginal": "100,000.00",
                    "lblAmountOriginalSign": "-",
                    "lblAmountOriginalSymbol": "",
                    "lblDateAndTime": "01/01/2019 10:30",
                    "lblRefNo": "RI123456",
                    "lblSeperator": "",
                    "lblTransctionDescription": "Payment",
                    "lblType": "Fund transfer"
                }],
                "groupCells": false,
                "height": "250px",
                "id": "segTransactionHistory",
                "isVisible": true,
                "left": "0dp",
                "maxHeight": "250px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": 0,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustMangTransctionHistory",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAmountConverted": "flxAmountConverted",
                    "flxAmountOriginal": "flxAmountOriginal",
                    "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                    "flxCustMangTransctionHistory": "flxCustMangTransctionHistory",
                    "flxFirstColoum": "flxFirstColoum",
                    "lblAmountConverted": "lblAmountConverted",
                    "lblAmountConvertedSign": "lblAmountConvertedSign",
                    "lblAmountConvertedSymbol": "lblAmountConvertedSymbol",
                    "lblAmountOriginal": "lblAmountOriginal",
                    "lblAmountOriginalSign": "lblAmountOriginalSign",
                    "lblAmountOriginalSymbol": "lblAmountOriginalSymbol",
                    "lblDateAndTime": "lblDateAndTime",
                    "lblRefNo": "lblRefNo",
                    "lblSeperator": "lblSeperator",
                    "lblTransctionDescription": "lblTransctionDescription",
                    "lblType": "lblType"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxMsgTransctions = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxMsgTransctions",
                "isVisible": false,
                "skin": "sknrtxLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.rtxMsgActivityHistory\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxScrollTransctionsSegment.add(segTransactionHistory, rtxMsgTransctions);
            flxTransactionHistory.add(flxTransactionHistorySearch, flxTransactionHistorySegmentHeader, flxSeperator4, flxScrollTransctionsSegment);
            flxTransactionContainer.add(flxAccuontsStatusTab, flxTransactionHistory);
            var flxProductDetailContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "74.36%",
                "id": "flxProductDetailContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductDetailContainer.setDefaultUnit(kony.flex.DP);
            var flxProductInfoWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductInfoWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductInfoWrapper.setDefaultUnit(kony.flex.DP);
            var flxProductDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductDetails.setDefaultUnit(kony.flex.DP);
            var flxProductHeaderAndDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductHeaderAndDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_c56ba97ff3f0490483af8b7728751669,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductHeaderAndDetails.setDefaultUnit(kony.flex.DP);
            var ProductRow1 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details": {
                        "height": "60px"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "lblData1": {
                        "text": "22.10.2014"
                    },
                    "lblData2": {
                        "text": " $100,000.00"
                    },
                    "lblData3": {
                        "text": "$2.00"
                    },
                    "lblHeading1": {
                        "text": "CREATED ON"
                    },
                    "lblHeading2": {
                        "text": "CURRENT BALANCE"
                    },
                    "lblHeading3": {
                        "text": "DIVIDEND RATE"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProductRow2 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details": {
                        "height": "60px"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "lblData1": {
                        "text": "JOINT"
                    },
                    "lblData2": {
                        "text": " $80,000.00"
                    },
                    "lblData3": {
                        "text": "$5000.00"
                    },
                    "lblHeading1": {
                        "text": "OWNERSHIP"
                    },
                    "lblHeading2": {
                        "text": "AVAILABLE BALANCE"
                    },
                    "lblHeading3": {
                        "text": "DIVIDEND PAID (YTD)"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProductRow3 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductRow3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details": {
                        "height": "60px"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "lblData1": {
                        "text": "N.A"
                    },
                    "lblData2": {
                        "text": "123456789"
                    },
                    "lblData3": {
                        "text": "$200.00"
                    },
                    "lblHeading1": {
                        "text": "ACCOUNT HOLDER"
                    },
                    "lblHeading2": {
                        "text": " ROUTING NUMBER"
                    },
                    "lblHeading3": {
                        "text": "LAST DIVIDEND PAID"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProductRow4 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductRow4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details": {
                        "height": "60px"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "lblData1": {
                        "text": "03.11.2017"
                    },
                    "lblData2": {
                        "text": "CTBAAU2S"
                    },
                    "lblData3": {
                        "text": "12.11.2017"
                    },
                    "lblHeading1": {
                        "text": "LAST UPDATE ON"
                    },
                    "lblHeading2": {
                        "text": "SWIFT CODE"
                    },
                    "lblHeading3": {
                        "text": "LAST DIVIDEND PAID ON"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProductRow5 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "ProductRow5",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details": {
                        "height": "60px"
                    },
                    "flxColumn3": {
                        "isVisible": false
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "lblData1": {
                        "text": "03.11.2017"
                    },
                    "lblData2": {
                        "text": "CTBAAU2S"
                    },
                    "lblData3": {
                        "text": "12.11.2017"
                    },
                    "lblHeading1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.MEMBERSHIP_ID\")"
                    },
                    "lblHeading2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.TAXNUMBER\")"
                    },
                    "lblHeading3": {
                        "text": "LAST DIVIDEND PAID ON"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSubscriptionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSubscriptionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSubscriptionsHeader.setDefaultUnit(kony.flex.DP);
            var lblSubscriptions = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "lblSubscriptions",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLatoRegular485c7518Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Subscriptions\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubscriptionsHeader.add(lblSubscriptions);
            var flxSubscriptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxSubscriptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSubscriptions.setDefaultUnit(kony.flex.DP);
            var lblAccountStatement = new kony.ui.Label({
                "height": "15dp",
                "id": "lblAccountStatement",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.AccountStatement\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSubscriptionsRadio = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSubscriptionsRadio",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxSubscriptionsRadio.setDefaultUnit(kony.flex.DP);
            var flxImgRbPaper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "12px",
                "id": "flxImgRbPaper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12px",
                "zIndex": 1
            }, {}, {});
            flxImgRbPaper.setDefaultUnit(kony.flex.DP);
            var imgRbPaper = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgRbPaper",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "radio_selected.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImgRbPaper.add(imgRbPaper);
            var lblPaper = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPaper",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Paper",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImgRbEstatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "12px",
                "id": "flxImgRbEstatement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12px",
                "zIndex": 1
            }, {}, {});
            flxImgRbEstatement.setDefaultUnit(kony.flex.DP);
            var imgRbEstatement = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgRbEstatement",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "radio_notselected.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImgRbEstatement.add(imgRbEstatement);
            var lblEstatement = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEstatement",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "e-Statement",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubscriptionsRadio.add(flxImgRbPaper, lblPaper, flxImgRbEstatement, lblEstatement);
            flxSubscriptions.add(lblAccountStatement, flxSubscriptionsRadio);
            flxProductHeaderAndDetails.add(ProductRow1, ProductRow2, ProductRow3, ProductRow4, ProductRow5, flxSubscriptionsHeader, flxSubscriptions);
            flxProductDetails.add(flxProductHeaderAndDetails);
            flxProductInfoWrapper.add(flxProductDetails);
            flxProductDetailContainer.add(flxProductInfoWrapper);
            flxCompanyAccountsDetail.add(backToAccounts, AccountsHeader, flxAccountsTabs, flxTransactionContainer, flxProductDetailContainer);
            flxCompanyDetailAccountsContainer.add(flxAccountSegment, flxCompanyAccountsDetail);
            var flxCompanyDetailaCustomerContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCompanyDetailaCustomerContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "10dp",
                "skin": "slFbox",
                "top": "65dp"
            }, {}, {});
            flxCompanyDetailaCustomerContainer.setDefaultUnit(kony.flex.DP);
            var btnCreateCustomer = new kony.ui.Button({
                "height": "30px",
                "id": "btnCreateCustomer",
                "isVisible": false,
                "right": "30px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Create_Business_User\")",
                "top": "20px",
                "width": "230px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCustomerHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxCustomerHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "50dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerHeader.setDefaultUnit(kony.flex.DP);
            var flxCustomerName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustomerName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCustomerName.setDefaultUnit(kony.flex.DP);
            var lblCustomerName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustomerName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "NAME",
                "top": 0,
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblCustomerNameSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustomerNameSortIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerName.add(lblCustomerName, lblCustomerNameSortIcon);
            var flxRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRole",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "21%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRole.setDefaultUnit(kony.flex.DP);
            var lblRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "ROLE",
                "top": 0,
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblRoleSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRoleSortIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRole.add(lblRole, lblRoleSortIcon);
            var flxUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUserName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "39%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "13%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxUserName.setDefaultUnit(kony.flex.DP);
            var lblUserName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUserName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "USER NAME ",
                "top": 0,
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblUserNameSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUserNameSortIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUserName.add(lblUserName, lblUserNameSortIcon);
            var flxCustomerStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustomerStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "55%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17%",
                "zIndex": 1
            }, {}, {});
            flxCustomerStatus.setDefaultUnit(kony.flex.DP);
            var lblCustomerStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustomerStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "STATUS",
                "top": 0,
                "width": "130px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxCustomerStatus.add(lblCustomerStatus);
            var flxCustomerEmailID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustomerEmailID",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "72%",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCustomerEmailID.setDefaultUnit(kony.flex.DP);
            var lblCustomerEmailID = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustomerEmailID",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "EMAIL ID",
                "top": 0,
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblCustomerEmailIDSortIocn = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustomerEmailIDSortIocn",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerEmailID.add(lblCustomerEmailID, lblCustomerEmailIDSortIocn);
            var flxCustomerHeaderSepartor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxCustomerHeaderSepartor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknTableHeaderLine",
                "zIndex": 1
            }, {}, {});
            flxCustomerHeaderSepartor.setDefaultUnit(kony.flex.DP);
            flxCustomerHeaderSepartor.add();
            flxCustomerHeader.add(flxCustomerName, flxRole, flxUserName, flxCustomerStatus, flxCustomerEmailID, flxCustomerHeaderSepartor);
            var segCompanyDetailCustomer = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblCustomerStatus": "122-4628761298",
                    "lblEmail": "bryanvictornash@gmail.com",
                    "lblFontIconOptions": "",
                    "lblIconStatus": "Label",
                    "lblName": "Bryan Victor Nash",
                    "lblRole": "bryanvnash123",
                    "lblSepartor": "Label",
                    "lblUsername": "TD123456BVN"
                }, {
                    "lblCustomerStatus": "34-1256721209",
                    "lblEmail": "Lukebryan@gmail.com",
                    "lblFontIconOptions": "",
                    "lblIconStatus": "Label",
                    "lblName": "Luke Bryan",
                    "lblRole": "lukebryan12",
                    "lblSepartor": "Label",
                    "lblUsername": "TDLB12345"
                }, {
                    "lblCustomerStatus": "345-1286745321",
                    "lblEmail": "thomasbronson@gmail.com",
                    "lblFontIconOptions": "",
                    "lblIconStatus": "Label",
                    "lblName": "Bronson Thomas",
                    "lblRole": "thomasbronson345",
                    "lblSepartor": "Label",
                    "lblUsername": "TD9861BT"
                }],
                "groupCells": false,
                "height": "70%",
                "id": "segCompanyDetailCustomer",
                "isVisible": false,
                "left": "35dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "35dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCompanyCustomer",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "115dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCompanyCustomer": "flxCompanyCustomer",
                    "flxContainer": "flxContainer",
                    "flxCustomerStatus": "flxCustomerStatus",
                    "flxOptions": "flxOptions",
                    "lblCustomerStatus": "lblCustomerStatus",
                    "lblEmail": "lblEmail",
                    "lblFontIconOptions": "lblFontIconOptions",
                    "lblIconStatus": "lblIconStatus",
                    "lblName": "lblName",
                    "lblRole": "lblRole",
                    "lblSepartor": "lblSepartor",
                    "lblUsername": "lblUsername"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddCustomers = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "flxAddCustomers",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddCustomers.setDefaultUnit(kony.flex.DP);
            var btnAddCustomer = new kony.ui.Button({
                "centerX": "50%",
                "centerY": "50%",
                "height": "30dp",
                "id": "btnAddCustomer",
                "isVisible": true,
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CREATE_CUSTOMER\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCustomers = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoCustomers",
                "isVisible": true,
                "skin": "sknLblLato84939e13px",
                "text": "No business user created yet.",
                "top": "56dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddCustomers.add(btnAddCustomer, lblNoCustomers);
            var contextualMenu = new com.adminConsole.common.contextualMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "contextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "15%",
                "zIndex": 5,
                "overrides": {
                    "btnLink1": {
                        "isVisible": false
                    },
                    "btnLink2": {
                        "isVisible": false
                    },
                    "contextualMenu": {
                        "isVisible": false,
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "15%",
                        "zIndex": 5
                    },
                    "flxOption1": {
                        "isVisible": false
                    },
                    "flxOption2": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOption4": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "8%",
                        "width": "15%"
                    },
                    "flxOptionsSeperator": {
                        "isVisible": true
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCompanyDetailaCustomerContainer.add(btnCreateCustomer, flxCustomerHeader, segCompanyDetailCustomer, flxAddCustomers, contextualMenu);
            flxSubDetails.add(flxViewTabs, flxViewSeperator, flxCompanyDetailAccountsContainer, flxCompanyDetailaCustomerContainer);
            flxCompanyDetails.add(flxMainDetails, flxSubDetails);
            flxMainContent.add(flxSearchCompanies, flxCreateCompany, flxCompanyDetails);
            flxRightPanel.add(flxMainHeader, flxBreadcrumb, flxMainContent);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "60dp",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxUnlinkAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUnlinkAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxUnlinkAccount.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp({
                "clipBounds": true,
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "right": "150px"
                    },
                    "btnPopUpDelete": {
                        "text": "UNLINK"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Unlink Account"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Do you want to unlink the account?<br><br>This account is associated with the company. Deleting this may result in losing the data permanently.\n",
                        "width": "90%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxUnlinkAccount.add(popUp);
            flxMain.add(flxLeftPanel, flxRightPanel, flxToastMessage, flxHeaderDropdown, flxLoading, flxUnlinkAccount);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmCompanies,
            "enabledForIdleTimeout": true,
            "id": "frmCompanies",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_g1ee5d4ede2f498fa7a53677ed55f226(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});