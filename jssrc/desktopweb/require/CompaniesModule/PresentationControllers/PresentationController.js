define(['Promisify', 'ErrorInterceptor'], function(Promisify, ErrorInterceptor) {
    /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
    function PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.globalDetails = {
            country: [],
            region: [],
            city: []
        };
        this.companyViewModel = {
            searchData: null
        };
        this.CustomerAccounts = null;
    }
    inheritsFrom(PresentationController, kony.mvc.Presentation.BasePresenter);
    /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
    PresentationController.prototype.initializePresentationController = function() {};
    PresentationController.prototype.showCompanies = function(context) {
        var self = this;
        context = {
            action: "fetch"
        };
        this.presentUserInterface("frmCompanies", context);
    };
    PresentationController.prototype.navigateToCreateCustomerScreen = function(context) {
        var self = this;
        this.navigateTo('CustomerCreateModule', 'showCreatecompanyScreen', [context]);
    };
    /*
    * navigate to edit customer form
    * @param: {"id": "","DateOfBirth": "","DrivingLicenseNumber": "","FirstName": "","LastName": "","Lastlogintime": "","MiddleName":"","Ssn": "","UserName": "","createdts": "","role_name": "","status": "","Email": "","Phone": "",
	          "company": {"UserName": "","id": "","TypeId": "TYPE_ID_SMALL_BUSINESS"}}
    */
    PresentationController.prototype.navigateToEditCustomerScreen = function(context) {
        var self = this;
        this.navigateTo('CustomerCreateModule', 'showEditCustomerScreen', [context]);
    };
    PresentationController.prototype.navigateToCustomerPersonal = function(param, formDetails) {
        var self = this;
        var context = [param.Customer_id, formDetails];
        this.navigateTo('CustomerManagementModule', 'diplayCustomerProfileFromOtherModule', context);
    };
    PresentationController.prototype.showResultFromCustomerScreen = function(context, isLoading, toast) {
        var self = this;
        if (isLoading) self.showLoadingScreen();
        self.presentCompaniesScreen(toast);
        if (context) self.presentCompaniesScreen(context);
    };
    PresentationController.prototype.showLoadingScreen = function() {
        var self = this;
        this.presentUserInterface("frmCompanies", {
            "loadingScreen": {
                "focus": true
            }
        });
    };
    PresentationController.prototype.hideLoadingScreen = function() {
        var self = this;
        this.presentUserInterface("frmCompanies", {
            "loadingScreen": {
                "focus": false
            }
        });
    };
    PresentationController.prototype.showToastMessage = function(status, message) {
        var self = this;
        this.presentUserInterface("frmCompanies", {
            "toastMessage": {
                "status": status,
                "message": message
            }
        });
    };
    PresentationController.prototype.getAddressSuggestion = function(text, onSucess, OnError) {
        var context = {
            "input": text,
        };
        this.businessController.getAddressSuggestion(context, onSucess, OnError);
    };
    PresentationController.prototype.getPlaceDetails = function(PlaceId, onSucess, OnError) {
        var context = {
            "placeid": PlaceId,
        };
        this.businessController.getPlaceDetails(context, onSucess, OnError);
    };
    PresentationController.prototype.fetchLocationPrefillData = function(callback) {
        var self = this;
        self.globalDetails = {
            countries: null,
            regions: null,
            cities: null,
        };
        var promiseFetchCountryList = Promisify(this.businessController, 'fetchCountryList');
        var promiseFetchRegionList = Promisify(this.businessController, 'fetchRegionList');
        var promiseFetchCityList = Promisify(this.businessController, 'fetchCityList');
        Promise.all([
            promiseFetchCountryList({}),
            promiseFetchRegionList({}),
            promiseFetchCityList({}),
        ]).then(function(responses) {
            self.globalDetails.countries = responses[0];
            self.globalDetails.regions = responses[1];
            self.globalDetails.cities = responses[2];
            if (typeof callback === 'function') callback(self.globalDetails);
        }).catch(function(res) {
            callback("error");
            kony.print("unable to fetch preloaded data", res);
        });
    };
    PresentationController.prototype.presentCompaniesScreen = function(context) {
        var self = this;
        self.presentUserInterface("frmCompanies", context);
    };
    PresentationController.prototype.createCompany = function(context) {
        var self = this;

        function completionCreateCompanyCallback(response) {
            if (response['status'] == "Failure") {
                self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), response.dbpErrMsg);
                self.hideLoadingScreen();
            } else {
                self.presentUserInterface("frmCompanies", {
                    "createCompanySuccess": response
                });
            }
        }

        function onError(error) {
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(error));
            self.hideLoadingScreen();
        }
        this.showLoadingScreen();
        this.businessController.createCompany(context, completionCreateCompanyCallback, onError);
    };
    PresentationController.prototype.getAllAccounts = function(context) {
        var self = this;

        function completionFetchAccountsCallback(response) {
            self.presentUserInterface("frmCompanies", {
                "accountSearch": response.Accounts
            });
            self.hideLoadingScreen();
        }

        function onError(error) {
            self.presentUserInterface("frmCompanies", {
                "accountSearch": []
            });
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(error));
            self.hideLoadingScreen();
        }
        this.showLoadingScreen();
        this.businessController.getAllAccounts(context, completionFetchAccountsCallback, onError);
    };
    PresentationController.prototype.getCompanyDetails = function(context, success, error) {
        var self = this;
        var onSuccess = function(res) {
            var detailContext = {
                "CompanyContext": res.OrganisationDetails,
                "OwnerContext": res.OrganisationOwnerDetails
            };
            self.hideLoadingScreen();
            success(detailContext);
        };
        var onError = function(err) {
            self.hideLoadingScreen();
            kony.print("getCompanyDetails(presentation controller): not able to fetch company details", err);
            error(err);
        };
        this.showLoadingScreen();
        this.businessController.getCompanyDetails(context, onSuccess, onError);
    };
    PresentationController.prototype.getCompanyAccounts = function(context, success, error) {
        var self = this;
        var onSuccess = function(res) {
            self.CustomerAccounts = res.OgranizationAccounts;
            var accountContext = {
                "accountContext": res.OgranizationAccounts
            };
            success(accountContext);
            self.hideLoadingScreen();
        };
        var onError = function(err) {
            self.hideLoadingScreen();
            kony.print("getCompanyAccounts(presentation controller): not able to fetch acocunts ", err);
            error(err);
        };
        this.showLoadingScreen();
        this.businessController.getCompanyAccounts(context, onSuccess, onError);
    };
    PresentationController.prototype.getCompanyCustomers = function(context, success, error) {
        var self = this;
        var onSuccess = function(res) {
            var customerContext = {
                "customerContext": res.organizationEmployee
            };
            success(customerContext);
            self.hideLoadingScreen();
        };
        var onError = function(err) {
            self.hideLoadingScreen();
            kony.print("getCompanyCustomers(presentation controller): not able to fetch customers", err);
            error(err);
        };
        this.showLoadingScreen();
        this.businessController.getCompanyCustomers(context, onSuccess, onError);
    };
    PresentationController.prototype.showCompaniesForm = function(context) {
        this.currentForm = "frmCompanies";
        this.presentUserInterface('frmCompanies', context);
    };
    PresentationController.prototype.hideLocationsLoadingScreen = function() {
        this.showCompaniesForm({
            action: "hideLoadingScreen"
        });
    };
    PresentationController.prototype.getCompanyAllDetails = function(context) {
        this.showLoadingScreen();
        var self = this;
        self.globalDetails = {
            CompanyContext: null,
            OwnerContext: null,
            accountContext: null,
            customerContext: null,
        };
        var promiseGetCompanyDetailsList = Promisify(this.businessController, 'getCompanyDetails');
        var promiseGetCompanyAccountsList = Promisify(this.businessController, 'getCompanyAccounts');
        var promiseGetCompanyCustomersList = Promisify(this.businessController, 'getCompanyCustomers');
        Promise.all([
            promiseGetCompanyDetailsList({
                "id": context.id
            }),
            promiseGetCompanyAccountsList({
                "Organization_id": context.id
            }),
            promiseGetCompanyCustomersList({
                "Organization_id": context.id
            })
        ]).then(function(responses) {
            self.globalDetails.CompanyContext = responses[0].OrganisationDetails;
            self.globalDetails.OwnerContext = responses[0].OrganisationOwnerDetails;
            self.globalDetails.accountContext = responses[1].OgranizationAccounts;
            self.globalDetails.customerContext = responses[2].organizationEmployee;
            self.CustomerAccounts = responses[1].OgranizationAccounts;
            self.hideLocationsLoadingScreen();
            self.presentUserInterface("frmCompanies", {
                "action": "companyDetails",
                "companyDetails": self.globalDetails
            });
        }).catch(function(res) {
            self.hideLocationsLoadingScreen();
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(error));
            kony.print("unable to fetch preloaded data", res);
        });
    };
    PresentationController.prototype.validateTIN = function(context, callback) {
        var self = this;

        function completionvalidateTinCallback(response) {
            self.presentUserInterface("frmCompanies", {
                "tinValidation": response
            });
            self.hideLoadingScreen();
            if (callback && callback instanceof Function) {
                callback();
            }
        }

        function onError(err) {
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), err.dbpErrMsg);
            self.hideLoadingScreen();
            if (callback && callback instanceof Function) {
                callback();
            }
        }
        this.showLoadingScreen();
        this.businessController.validateTIN(context, completionvalidateTinCallback, onError);
    };
    PresentationController.prototype.getCompaniesSearch = function(payload) {
        var self = this;

        function onSuccess(response) {
            self.companyViewModel.searchData = response.OrganisationDetails;
            self.hideLoadingScreen();
            self.presentCompaniesScreen(self.companyViewModel);
        }

        function onError(error) {
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(error));
            self.hideLoadingScreen();
        }
        this.showLoadingScreen();
        this.businessController.getCompaniesSearch(payload, onSuccess, onError);
    };
    PresentationController.prototype.editCompany = function(context) {
        var self = this;

        function completionEditCompanyCallback(response) {
            if (response['status'] == "Failure") {
                self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), response.dbpErrMsg);
                self.hideLoadingScreen();
            } else {
                response.id = context.id;
                self.presentUserInterface("frmCompanies", {
                    "editCompanySuccess": response
                });
            }
        }

        function onError(error) {
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(error));
            self.hideLoadingScreen();
        }
        this.showLoadingScreen();
        this.businessController.editCompany(context, completionEditCompanyCallback, onError);
    };
    PresentationController.prototype.unlinkAccounts = function(context, success, error) {
        var self = this;
        var onSuccess = function(res) {
            self.hideLoadingScreen();
            success(res);
        };
        var onError = function(err) {
            self.hideLoadingScreen();
            error(err);
        };
        this.showLoadingScreen();
        this.businessController.unlinkAccounts(context, onSuccess, onError);
    };
    /**
     * @name getAccountTransactions
     * @member CustomerManagementModule.presentationController
     * @param {AccountNumber : string, StartDate : string, EndDate : string} data
     */
    PresentationController.prototype.getAccountTransactions = function(data) {
        var self = this;
        self.showLoadingScreen();

        function successCallback(response) {
            self.hideLoadingScreen();
            self.AccountTrasactions = response.Transactions;
            self.presentCompaniesScreen({
                "AccountTrasactions": self.AccountTrasactions
            });
        }

        function failureCallback(response) {
            self.hideLoadingScreen();
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(response));
        }
        self.businessController.getCustomerTransactions(data, successCallback, failureCallback);
    };
    /**
     * @name getCustomerAccountInfo
     * @member CompaniesModule.presentationController
     * @param string accountID
     */
    PresentationController.prototype.getCustomerAccountInfo = function(accountID) {
        return this.CustomerAccounts.filter(function(x) {
            return x.Account_id === accountID;
        });
    };
    return PresentationController;
});