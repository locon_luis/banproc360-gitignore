define("CompaniesModule/userfrmCompaniesController", {
    segCountry: [],
    segState: [],
    segLocationCity: [],
    AccountTrasactions: null,
    completeCompanyDetails: {},
    accountsUnlinkPayload: {},
    typeConfig: {
        smallBusiness: "TYPE_ID_SMALL_BUSINESS",
        microBusiness: "TYPE_ID_MICRO_BUSINESS"
    },
    tabsConfig: {
        accounts: 1,
        businessUsers: 2
    },
    recordsSize: 20,
    assignedAccount: [],
    microBusinessBankingAccounts: [],
    microBusinessInputs: {
        MembershipId: null,
        TIN: null,
        CompanyType: null
    },
    actionConfig: {
        create: "CREATE",
        edit: "EDIT"
    },
    action: "CREATE",
    willUpdateUI: function(context) {
        this.updateLeftMenu(context);
        this.view.forceLayout();
        if (context) {
            if (context.action === "fetch") {
                this.showSearchScreen("fetch");
            } else if (context.action === "createCustomer") {
                this.hideRequiredMainScreens(this.view.flxCompanyDetails);
                this.getCompanyAllDetails(context.id);
                this.getCompanyCustomers(context.id, this.setCompanyCustomers);
                this.backToCompanyDetails(this.tabsConfig.businessUsers);
            } else if (context.action === "hideLoadingScreen") {
                kony.adminConsole.utils.hideProgressBar(this.view);
            } else if (context.loadingScreen) {
                if (context.loadingScreen.focus) kony.adminConsole.utils.showProgressBar(this.view);
                else kony.adminConsole.utils.hideProgressBar(this.view);
            } else if (context.createCompanySuccess) {
                this.createCompaySuccess(context.createCompanySuccess);
            } else if (context.toastMessage) {
                if (context.toastMessage.status === kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success")) {
                    this.view.toastMessage.showToastMessage(context.toastMessage.message, this);
                } else if (context.toastMessage.status === kony.i18n.getLocalizedString("i18n.frmGroupsController.error")) {
                    this.view.toastMessage.showErrorToastMessage(context.toastMessage.message, this);
                }
            } else if (context.tinValidation) {
                this.updateTINValidationStatus(context.tinValidation);
            } else if (context.accountSearch) {
                this.microBusinessBankingAccounts = context.accountSearch;
                this.accountSearch(context.accountSearch);
            } else if (context.searchData) {
                this.setDatatoSearchSegment(context.searchData);
            } else if (context.editCompanySuccess) {
                this.editCompanySuccess(context.editCompanySuccess);
            } else if (context.AccountTrasactions) {
                this.AccountTrasactions = context.AccountTrasactions;
                this.setDataForProductTransactionSegment(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Successful"));
            } else if (context.action === "companyDetails") {
                this.getCompleteDataForCreatedCompany(context.companyDetails);
                this.backToCompanyDetails(this.tabsConfig.accounts);
            }
        }
    },
    companiesPreShow: function() {
        this.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
        this.view.map1.mapLocations.mapKey  =  this.getMapInitiationKey();
        this.setFlowActions();
        this.view.mainHeader.btnAddNewOption.focusSkin = "sknBg235682Font13pxBr1pxR20px";
        this.view.mainHeader.btnAddNewOption.skin = "sknBg235682Font13pxBr1pxR20px";
        this.view.commonButtonsDetails.btnNext.skin = "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px";
        this.view.commonButtonsDetails.btnNext.hoverSkin = "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px";
    },
    showSearchScreen: function(action) {
        this.hideRequiredMainScreens(this.view.flxSearchCompanies);
        if (action === "fetch") {
            this.view.tbxName.text = "";
            this.view.tbxEmailId.text = "";
            this.view.segSearchResults.setVisibility(false);
            this.view.lblNoResults.setVisibility(true);
            this.view.flxNoResultsFound.setVisibility(true);
            this.view.lblCreateCompanyLink.setVisibility(false);
            this.view.lblNoResults.text = kony.i18n.getLocalizedString("i18n.frmCompanies.NoSearchResultsFound");
        }
        var screenHeight = kony.os.deviceInfo().screenHeight;
        this.view.flxSearchResults.height = (screenHeight - 320) + "px";
        this.view.tbxName.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
        this.view.tbxEmailId.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
        this.view.flxNameError.setVisibility(false);
        this.view.flxEmailIdError.setVisibility(false);
        this.view.flxHeaderPermissions.setVisibility(false);
        this.view.mainHeader.btnAddNewOption.setVisibility(true);
        this.view.flxHeaderPermissions.setVisibility(true);
        this.view.segSearchResults.setVisibility(true);
        this.view.flxBreadcrumb.setVisibility(false);
        this.view.forceLayout();
    },
    downloadTransactionsAsCSV: function() {
        var segData = this.view.segTransactionHistory.data;
        var list = segData.map(function(record) {
            return {
                "RefNo": record.lblRefNo,
                "Type": record.lblType,
                "Description": record.lblTransctionDescription,
                "DateAndTime": record.lblDateAndTime,
                "Amount": record.lblAmountOriginal
            };
        });
        this.commonDownloadCSV(list, "Transactions.csv");
    },
    setFlowActions: function() {
        var scopeObj = this;
        this.view.transactionHistorySearch.flxDownload.onClick = function() {
            scopeObj.downloadTransactionsAsCSV();
        };
        this.view.addAndRemoveAccounts.flxSelectedType.onClick = function() {
            if (scopeObj.view.addAndRemoveAccounts.flxSegSearchType.isVisible) scopeObj.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(false);
            else scopeObj.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(true);
        };
        this.view.addAndRemoveAccounts.segSearchType.onRowClick = function() {
            scopeObj.dispalySelectedType();
        };
        this.view.popUp.btnPopUpCancel.onClick = function() {
            scopeObj.view.flxUnlinkAccount.setVisibility(false);
        };
        this.view.popUp.flxPopUpClose.onTouchEnd = function() {
            scopeObj.view.flxUnlinkAccount.setVisibility(false);
        };
        this.view.popUp.btnPopUpDelete.onClick = function() {
            scopeObj.accountUnlink();
        };
        this.view.contextualMenu.flxOption2.onClick = function() {
            var index = scopeObj.view.segCompanyDetailCustomer.selectedRowIndex[1];
            var data = scopeObj.view.segCompanyDetailCustomer.data;
            var customerEditContext = data[index].customerEditInfo;
            scopeObj.presenter.navigateToEditCustomerScreen(customerEditContext);
        };
        this.view.addAndRemoveAccounts.flxSearchClick.onClick = function() {
            scopeObj.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(false);
            var searchText = scopeObj.view.addAndRemoveAccounts.tbxFilterSearch.text;
            if (searchText) {
                var payload = {};
                var filter = scopeObj.view.addAndRemoveAccounts.lblSelSearchType.text;
                switch (filter) {
                    case 'Membership id':
                        payload.Membership_id = searchText;
                        break;
                    case 'Tax Number':
                        payload.Taxid = searchText;
                        break;
                    case 'Account Id':
                        payload.Account_id = searchText;
                        break;
                    default:
                        payload.Account_id = searchText;
                }
                scopeObj.showAccountSearchLoading();
                scopeObj.presenter.getAllAccounts(payload);
            }
        };
        this.view.backToAccounts.btnBack.onClick = function() {
            scopeObj.view.flxCompanyAccountsDetail.setVisibility(false);
            scopeObj.view.flxAccountSegment.setVisibility(true);
            scopeObj.view.forceLayout();
        };
        this.view.segCompanyDetailAccount.onRowClick = function() {
            scopeObj.view.flxAccountSegment.setVisibility(false);
            scopeObj.view.flxCompanyAccountsDetail.setVisibility(true);
            scopeObj.view.btnTransactionHistory.onClick();
            var acctNo = scopeObj.view.segCompanyDetailAccount.selecteditems[0].lblAccountNumber.text;
            var endDate = scopeObj.getTransactionDateForServiceCall(new Date());
            var startDate = scopeObj.getTransactionDateCustom(7, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DAYS"));
            scopeObj.setDataForProductDetailsScreen(scopeObj.presenter.getCustomerAccountInfo(acctNo));
            scopeObj.presenter.getAccountTransactions({
                "AccountNumber": acctNo,
                "StartDate": startDate,
                "EndDate": endDate
            });
            var from = startDate.split(" ");
            var to = endDate.split(" ");
            scopeObj.view.customCalCreatedDate.resetData = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Last_7_days");
            scopeObj.view.customCalCreatedDate.rangeType = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Last_7_days");
            var sDate = scopeObj.calWidgetFormatDate(from[0]);
            var eDate = scopeObj.calWidgetFormatDate(to[0]);
            scopeObj.view.customCalCreatedDate.value = sDate + " - " + eDate;
            scopeObj.tabUtilLabelFunction([scopeObj.view.flxRecent,
                scopeObj.view.flxPending, scopeObj.view.flxScheduled
            ], scopeObj.view.flxRecent);
        };
        this.view.flxRecent.onTouchEnd = function() {
            scopeObj.tabUtilLabelFunction([scopeObj.view.flxRecent,
                scopeObj.view.flxPending, scopeObj.view.flxScheduled
            ], scopeObj.view.flxRecent);
            kony.adminConsole.utils.showProgressBar(scopeObj.view);
            scopeObj.setDataForProductTransactionSegment(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Successful"));
            kony.adminConsole.utils.hideProgressBar(scopeObj.view);
        };
        this.view.flxPending.onTouchEnd = function() {
            scopeObj.tabUtilLabelFunction([scopeObj.view.flxRecent,
                scopeObj.view.flxPending, scopeObj.view.flxScheduled
            ], scopeObj.view.flxPending);
            kony.adminConsole.utils.showProgressBar(scopeObj.view);
            scopeObj.setDataForProductTransactionSegment(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Pending"));
            kony.adminConsole.utils.hideProgressBar(scopeObj.view);
        };
        this.view.flxScheduled.onTouchEnd = function() {
            scopeObj.tabUtilLabelFunction([scopeObj.view.flxRecent,
                scopeObj.view.flxPending, scopeObj.view.flxScheduled
            ], scopeObj.view.flxScheduled);
            kony.adminConsole.utils.showProgressBar(scopeObj.view);
            scopeObj.setDataForProductTransactionSegment(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Scheduled"));
            kony.adminConsole.utils.hideProgressBar(scopeObj.view);
        };
        this.view.btnTransactionHistory.onClick = function() {
            scopeObj.view.flxTransactionContainer.setVisibility(true);
            scopeObj.view.flxProductDetailContainer.setVisibility(false);
            scopeObj.changeSubTabSkin([scopeObj.view.btnTransactionHistory, scopeObj.view.btnProductsDetails], scopeObj.view.btnTransactionHistory);
        };
        this.view.btnProductsDetails.onClick = function() {
            scopeObj.view.flxTransactionContainer.setVisibility(false);
            scopeObj.view.flxProductDetailContainer.setVisibility(true);
            scopeObj.changeSubTabSkin([scopeObj.view.btnTransactionHistory, scopeObj.view.btnProductsDetails], scopeObj.view.btnProductsDetails);
        };
        this.view.btnCreateCustomer.onClick = function() {
            var companyContext = scopeObj.completeCompanyDetails.CompanyContext[0];
            var context = {
                companyID: companyContext.id,
                companyName: companyContext.Name,
                membershipID: companyContext.MembershipId,
                TIN: companyContext.Tin,
                type: companyContext.TypeId,
            };
            scopeObj.presenter.navigateToCreateCustomerScreen(context);
        };
        this.view.btnAddCustomer.onClick = function() {
            scopeObj.view.btnCreateCustomer.onClick();
        };
        this.view.flxViewTab1.onTouchEnd = function() {
            scopeObj.showAccounts();
        };
        this.view.flxViewTab2.onTouchEnd = function() {
            scopeObj.showCustomers();
        };
        this.view.tbxSearch.onKeyUp = function() {
            scopeObj.getGoogleSuggestion(scopeObj.view.tbxSearch.text);
        };
        this.view.tbxNameValue.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.tbxNameValue, scopeObj.view.flxNoNameError, 1);
        };
        this.view.contactNumber.txtContactNumber.onKeyUp = function() {
            scopeObj.view.contactNumber.hideErrorMsg(2);
        };
        this.view.contactNumber.txtContactNumber.onTouchStart = function() {
            scopeObj.AdminConsoleCommonUtils.restrictTextFieldToPhoneNumber('frmCompanies_contactNumber_txtContactNumber');
        };
        this.view.contactNumber.txtISDCode.onKeyUp = function() {
            scopeObj.view.contactNumber.hideErrorMsg(1);
        };
        this.view.tbxEmailValue.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.tbxEmailValue, scopeObj.view.flxNoEmailError, 1);
        };
        this.view.tbxStreetName.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.tbxStreetName, scopeObj.view.flxNoStreetName, 1);
        };
        this.view.tbxZipCode.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.tbxZipCode, scopeObj.view.flxNoZipCode, 1);
        };
        this.view.tbxMBMembershipIdValue.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.tbxMBMembershipIdValue, scopeObj.view.flxMBMembershipIdError, 1);
        };
        this.view.textBoxEntryTin.tbxEnterValue.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.textBoxEntryTin.tbxEnterValue, scopeObj.view.textBoxEntryTin.flxInlineError, 1);
            scopeObj.view.lblTinCheck.isValidTin = false;
            scopeObj.view.lblTinCheck.isTinValidated = false;
            scopeObj.view.textBoxEntryTin.tbxEnterValue.skin = "skntbxEnterValue";
        };
        /*owner details*/
        this.view.textBoxOwnerDetailsEntry11.tbxEnterValue.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.textBoxOwnerDetailsEntry11.flxEnterValue, scopeObj.view.textBoxOwnerDetailsEntry11.flxInlineError, 3);
        };
        this.view.textBoxOwnerDetailsEntry13.tbxEnterValue.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.textBoxOwnerDetailsEntry13.flxEnterValue, scopeObj.view.textBoxOwnerDetailsEntry13.flxInlineError, 3);
        };
        this.view.textBoxOwnerDetailsEntry21.tbxEnterValue.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.flxCalendarDOB, scopeObj.view.textBoxOwnerDetailsEntry21.flxInlineError, 3);
        };
        this.view.textBoxOwnerDetailsEntry22.tbxEnterValue.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.textBoxOwnerDetailsEntry22.flxEnterValue, scopeObj.view.textBoxOwnerDetailsEntry22.flxInlineError, 3);
        };
        this.view.textBoxOwnerDetailsEntry23.tbxEnterValue.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.textBoxOwnerDetailsEntry23.flxEnterValue, scopeObj.view.textBoxOwnerDetailsEntry23.flxInlineError, 3);
        };
        this.view.textBoxOwnerDetailsEntry31.txtContactNumber.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.textBoxOwnerDetailsEntry31.txtContactNumber, scopeObj.view.textBoxOwnerDetailsEntry31.flxError, 1);
        };
        this.view.textBoxOwnerDetailsEntry31.txtISDCode.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.textBoxOwnerDetailsEntry31.txtISDCode, scopeObj.view.textBoxOwnerDetailsEntry31.flxError, 1);
        };
        this.view.customCalOwnerDOB.event = function() {
            scopeObj.clearValidation(null, scopeObj.view.textBoxOwnerDetailsEntry21.flxInlineError);
        };
        /**************/
        this.view.typeHeadCountry.tbxSearchKey.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.typeHeadCountry.tbxSearchKey, scopeObj.view.flxNoCountry, 1);
            scopeObj.view.typeHeadCountry.tbxSearchKey.info.isValid = false;
            scopeObj.hideAddressSegments(scopeObj.view.typeHeadCountry);
            scopeObj.view.flxCountry.zIndex = 2;
            scopeObj.searchForAddress(scopeObj.view.typeHeadCountry.tbxSearchKey, scopeObj.view.typeHeadCountry.segSearchResult, scopeObj.view.typeHeadCountry.flxNoResultFound, 1);
            if (scopeObj.view.typeHeadCountry.tbxSearchKey.text === "") {
                scopeObj.view.typeHeadState.tbxSearchKey.setEnabled(false);
                scopeObj.view.typeHeadCity.tbxSearchKey.setEnabled(false);
            } else {
                scopeObj.view.typeHeadState.tbxSearchKey.setEnabled(true);
            }
            scopeObj.view.typeHeadState.tbxSearchKey.text = "";
            scopeObj.view.typeHeadCity.tbxSearchKey.text = "";
            scopeObj.view.forceLayout();
        };
        this.view.typeHeadCity.tbxSearchKey.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.typeHeadCity.tbxSearchKey, scopeObj.view.flxNoCity, 1);
            scopeObj.view.typeHeadCity.tbxSearchKey.info.isValid = false;
            scopeObj.hideAddressSegments(scopeObj.view.typeHeadCity);
            scopeObj.searchForAddress(scopeObj.view.typeHeadCity.tbxSearchKey, scopeObj.view.typeHeadCity.segSearchResult, scopeObj.view.typeHeadCity.flxNoResultFound, 3);
            scopeObj.view.forceLayout();
        };
        this.view.typeHeadState.tbxSearchKey.onKeyUp = function() {
            scopeObj.clearValidation(scopeObj.view.typeHeadState.tbxSearchKey, scopeObj.view.flxNoState, 1);
            scopeObj.view.typeHeadState.tbxSearchKey.info.isValid = false;
            if (scopeObj.view.typeHeadState.tbxSearchKey.text === "") {
                scopeObj.view.typeHeadCity.tbxSearchKey.setEnabled(false);
            } else {
                scopeObj.view.typeHeadCity.tbxSearchKey.setEnabled(true);
            }
            scopeObj.hideAddressSegments(scopeObj.view.typeHeadState);
            scopeObj.searchForAddress(scopeObj.view.typeHeadState.tbxSearchKey, scopeObj.view.typeHeadState.segSearchResult, scopeObj.view.typeHeadState.flxNoResultFound, 2);
            scopeObj.view.typeHeadCity.tbxSearchKey.text = "";
            scopeObj.view.forceLayout();
        };
        this.view.typeHeadCountry.tbxSearchKey.onEndEditing = function() {
            if (scopeObj.view.typeHeadCountry.flxNoResultFound.isVisible) {
                scopeObj.view.typeHeadCountry.flxNoResultFound.setVisibility(false);
            }
        };
        this.view.typeHeadCity.tbxSearchKey.onEndEditing = function() {
            if (scopeObj.view.typeHeadCity.flxNoResultFound.isVisible) {
                scopeObj.view.typeHeadCity.flxNoResultFound.setVisibility(false);
            }
        };
        this.view.typeHeadState.tbxSearchKey.onEndEditing = function() {
            if (scopeObj.view.typeHeadState.flxNoResultFound.isVisible) {
                scopeObj.view.typeHeadState.flxNoResultFound.setVisibility(false);
            }
        };
        this.view.typeHeadCountry.segSearchResult.onRowClick = function() {
            scopeObj.assingText(scopeObj.view.typeHeadCountry.segSearchResult, scopeObj.view.typeHeadCountry.tbxSearchKey);
            scopeObj.clearValidation(scopeObj.view.typeHeadCountry.tbxSearchKey, scopeObj.view.flxNoCountry, 1);
        };
        this.view.typeHeadCity.segSearchResult.onRowClick = function() {
            scopeObj.assingText(scopeObj.view.typeHeadCity.segSearchResult, scopeObj.view.typeHeadCity.tbxSearchKey);
            scopeObj.clearValidation(scopeObj.view.typeHeadCity.tbxSearchKey, scopeObj.view.flxNoCity, 1);
        };
        this.view.typeHeadState.segSearchResult.onRowClick = function() {
            scopeObj.assingText(scopeObj.view.typeHeadState.segSearchResult, scopeObj.view.typeHeadState.tbxSearchKey);
            scopeObj.clearValidation(scopeObj.view.typeHeadState.tbxSearchKey, scopeObj.view.flxNoState, 1);
        };
        this.view.contactNumber.txtISDCode.onEndEditing = function() {
            scopeObj.view.contactNumber.txtISDCode.text = scopeObj.view.contactNumber.addingPlus(scopeObj.view.contactNumber.txtISDCode.text);
        };
        this.view.textBoxOwnerDetailsEntry31.txtISDCode.onEndEditing = function() {
            scopeObj.view.textBoxOwnerDetailsEntry31.txtISDCode.text = scopeObj.view.textBoxOwnerDetailsEntry31.addingPlus(scopeObj.view.textBoxOwnerDetailsEntry31.txtISDCode.text);
        };
        this.view.segSearch.onRowClick = function() {
            scopeObj.mappingRowToWidgets();
        };
        this.view.commonButtonsDetails.btnNext.onClick = function() {
            scopeObj.createCompanyCompanyDetailsNextClickHandler();
        };
        this.view.commonButtonsDetails.btnCancel.onClick = function() {
            if (scopeObj.action === scopeObj.actionConfig.create) {
                scopeObj.showSearchScreen("detailsCancel");
            } else {
                scopeObj.backToCompanyDetails(scopeObj.tabsConfig.accounts);
            }
        };
        this.view.verticalTabsCompany.btnOption1.onClick = function() {
            scopeObj.showCompanyDetailsScreen();
        };
        this.view.verticalTabsCompany.btnOption2.onClick = function() {
            scopeObj.createCompanyCompanyDetailsNextClickHandler();
        };
        this.view.verticalTabsCompany.btnOption3.onClick = function() {
            var isValidCompanyDetails = scopeObj.validatingCompanyDetails();
            var isValidAccounts = false;
            if (isValidCompanyDetails) {
                scopeObj.trackCreateCompanyMembershipIdAndTinChange();
                isValidAccounts = scopeObj.checkAccountValidation();
                if (!isValidAccounts) {
                    scopeObj.view.addAndRemoveAccounts.flxError.setVisibility(true);
                }
            }
            scopeObj.showHideAddRemoveSegment();
            if (isValidCompanyDetails && isValidAccounts) {
                scopeObj.showCreateOwnerDetails();
            }
        };
        this.view.addAndRemoveAccounts.tbxSearchBox.onKeyUp = function() {
            var searchText = scopeObj.view.addAndRemoveAccounts.tbxSearchBox.text;
            if (searchText) {
                var accounts = scopeObj.microBusinessBankingAccounts.filter(function(account) {
                    return account.Account_id.indexOf(searchText) >= 0;
                });
                scopeObj.accountSearch(accounts);
            } else {
                scopeObj.accountSearch(scopeObj.microBusinessBankingAccounts);
            }
        };
        this.view.addAndRemoveAccounts.tbxSearchBox.onTouchStart = function() {
            scopeObj.view.addAndRemoveAccounts.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
        };
        this.view.addAndRemoveAccounts.tbxSearchBox.onEndEditing = function() {
            scopeObj.view.addAndRemoveAccounts.flxSearchContainer.skin = "sknflxd5d9ddop100";
        };
        this.view.addAndRemoveAccounts.tbxFilterSearch.onTouchStart = function() {
            if (scopeObj.view.addAndRemoveAccounts.flxSegSearchType.isVisible) {
                scopeObj.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(false);
            }
        };
        this.view.commonButtonsAccounts.btnCancel.onClick = function() {
            if (scopeObj.action === scopeObj.actionConfig.create) {
                scopeObj.showSearchScreen("accountsCancel");
                scopeObj.view.addAndRemoveAccounts.flxError.setVisibility(false);
            } else {
                scopeObj.backToCompanyDetails(scopeObj.tabsConfig.accounts);
            }
        };
        this.view.commonButtonsOwnerDetails.btnCancel.onClick = function() {
            if (scopeObj.action === scopeObj.actionConfig.create) {
                scopeObj.showSearchScreen("ownerDetailCancel");
                scopeObj.view.addAndRemoveAccounts.flxError.setVisibility(false);
            } else {
                scopeObj.backToCompanyDetails(scopeObj.tabsConfig.accounts);
            }
        };
        this.view.commonButtonsAccounts.btnSave.onClick = function() {
            var isValid = scopeObj.checkAccountValidation();
            scopeObj.showHideAddRemoveSegment();
            if (isValid) {
                scopeObj.requestCreateUpdateComapny();
            } else {
                scopeObj.view.addAndRemoveAccounts.flxError.setVisibility(true);
            }
        };
        this.view.commonButtonsAccounts.btnNext.onClick = function() {
            scopeObj.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skntxtbxDetails0bbf1235271384a";
            var isValid = scopeObj.checkAccountValidation();
            scopeObj.showHideAddRemoveSegment();
            if (isValid) {
                scopeObj.showCreateOwnerDetails();
                scopeObj.view.addAndRemoveAccounts.flxError.setVisibility(false);
            } else {
                scopeObj.view.addAndRemoveAccounts.flxError.setVisibility(true);
            }
        };
        this.view.commonButtonsOwnerDetails.btnSave.onClick = function() {
            var isValid = scopeObj.validateOwnerDetailsScreen();
            if (isValid) {
                scopeObj.requestCreateUpdateComapny();
            }
        };
        this.view.addAndRemoveAccounts.btnRemoveAll.onClick = function() {
            scopeObj.resetAccounts();
        };
        this.view.tbxName.onKeyUp = function() {
            scopeObj.view.tbxName.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
            scopeObj.view.flxNameError.setVisibility(false);
        };
        this.view.tbxEmailId.onKeyUp = function() {
            scopeObj.view.tbxEmailId.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
            scopeObj.view.flxEmailIdError.setVisibility(false);
        };
        this.view.btnSearch.onClick = function() {
            var validationOfSearch = scopeObj.validateInputFields();
            if (validationOfSearch) {
                scopeObj.view.tbxName.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
                scopeObj.view.tbxEmailId.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
                scopeObj.view.flxEmailIdError.setVisibility(false);
                scopeObj.view.flxNameError.setVisibility(false);
                scopeObj.view.flxNameError.setVisibility(false);
                scopeObj.companiesSearch();
            }
        };
        this.view.mainHeader.btnAddNewOption.onClick = function() {
            scopeObj.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmCompanies.COMPANIES");
            scopeObj.view.breadcrumbs.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.frmCompanies.CreateCompany");
            scopeObj.action = scopeObj.actionConfig.create;
            scopeObj.hideRequiredMainScreens(scopeObj.view.flxCreateCompany);
            scopeObj.view.flxBreadcrumb.setVisibility(true);
            scopeObj.view.addAndRemoveAccounts.segSelectedOptions.info = {
                "segData": []
            };
            scopeObj.view.typeHeadCity.tbxSearchKey.info = {
                "isValid": false,
                "data": ""
            };
            scopeObj.view.typeHeadCountry.tbxSearchKey.info = {
                "isValid": false,
                "data": ""
            };
            scopeObj.view.typeHeadState.tbxSearchKey.info = {
                "isValid": false,
                "data": ""
            };
            scopeObj.showCompanyDetailsScreen();
            scopeObj.view.mainHeader.btnAddNewOption.setVisibility(false);
            scopeObj.clearDataForOwnerDetails();
            scopeObj.clearDataForCreateCompanyDetails();
            scopeObj.view.commonButtonsAccounts.btnSave.text = kony.i18n.getLocalizedString("i18n.frmPermissionsController.CREATE");
            scopeObj.view.commonButtonsOwnerDetails.btnSave.text = kony.i18n.getLocalizedString("i18n.frmPermissionsController.CREATE");
            scopeObj.changeUIAccessBasedOnEditCreateCompanyFlow();
            scopeObj.view.tbxSearch.text = "";
            scopeObj.view.segSearch.setVisibility(false);
        };
        this.view.breadcrumbs.btnBackToMain.onClick = function() {
            if (scopeObj.view.breadcrumbs.btnBackToMain.text === kony.i18n.getLocalizedString("i18n.frmCompanies.SearchCompanies") || scopeObj.view.breadcrumbs.btnBackToMain.text === kony.i18n.getLocalizedString("i18n.frmCompanies.COMPANIES")) {
                scopeObj.showSearchScreen("breadCrumb");
            } else {
                scopeObj.backToCompanyDetails(scopeObj.tabsConfig.accounts);
            }
        };
        this.view.breadcrumbs.btnPreviousPage.onClick = function() {
            scopeObj.backToCompanyDetails(scopeObj.tabsConfig.accounts);
        };
        this.view.segSearchResults.onRowClick = function() {
            scopeObj.onSerchSegRowClick();
        };
        this.view.flxOwnershipHeader.onClick = function() {
            var check = scopeObj.view.flxMBOwnerDetails.isVisible;
            scopeObj.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skntxtbxDetails0bbf1235271384a";
            scopeObj.view.flxMBOwnerDetails.setVisibility(!check);
            scopeObj.view.lblArrowIcon.text = !check ? "\ue915" : "\ue922";
            scopeObj.view.lblArrowIcon.skin = "sknfontIconDescRightArrow14px";
            scopeObj.view.forceLayout();
        };
        this.view.radioCompanyType.onSelection = function(event) {
            scopeObj.changeSidebarOptionsBasedOnCompanyType(event.selectedKey);
        };
        this.view.textBoxEntryTin.btnCheck.onClick = function() {
            scopeObj.validateTin(null);
        };
        this.view.btnCompanyDetailEdit.onClick = function() {
            scopeObj.action = scopeObj.actionConfig.edit;
            scopeObj.view.typeHeadCity.tbxSearchKey.info = {
                "isValid": false,
                "data": ""
            };
            scopeObj.view.typeHeadCountry.tbxSearchKey.info = {
                "isValid": false,
                "data": ""
            };
            scopeObj.view.typeHeadState.tbxSearchKey.info = {
                "isValid": false,
                "data": ""
            };
            scopeObj.showEditCompany(scopeObj.completeCompanyDetails);
            scopeObj.view.commonButtonsAccounts.btnSave.text = kony.i18n.getLocalizedString("i18n.ConfigurationBundles.buttonUpdate");
            scopeObj.view.commonButtonsOwnerDetails.btnSave.text = kony.i18n.getLocalizedString("i18n.ConfigurationBundles.buttonUpdate");
            scopeObj.changeUIAccessBasedOnEditCreateCompanyFlow();
        };
        this.view.btnApply.onClick = function() {
            scopeObj.getTransactionsBasedOnDate();
        };
        //search on transaction history
        this.view.transactionHistorySearch.tbxSearchBox.onBeginEditing = function() {
            scopeObj.view.transactionHistorySearch.flxSearchContainer.skin = "sknflx0cc44f028949b4cradius30px";
        };
        this.view.transactionHistorySearch.tbxSearchBox.onEndEditing = function() {
            scopeObj.view.transactionHistorySearch.flxSearchContainer.skin = "sknflxBgffffffBorderc1c9ceRadius30px";
        };
        this.view.transactionHistorySearch.tbxSearchBox.onKeyUp = function() {
            scopeObj.view.transactionHistorySearch.flxClearSearchImage.setVisibility(true);
            var searchParameters = [{
                "searchKey": "lblRefNo",
                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
            }, {
                "searchKey": "lblDateAndTime",
                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
            }, {
                "searchKey": "lblTransctionDescription",
                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
            }, {
                "searchKey": "lblType",
                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
            }, {
                "searchKey": "lblAmount",
                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
            }];
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "template": "flxCustMangTransactionHistoryCompanies"
            };
            var listOfWidgetsToHide = [scopeObj.view.flxTransactionHistorySegmentHeader, scopeObj.view.flxSeperator4];
            scopeObj.search(scopeObj.view.segTransactionHistory, searchParameters, scopeObj.view.rtxMsgTransctions, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.TOEND"), scopeObj.recordsSize, toAdd, listOfWidgetsToHide);
        };
        this.view.transactionHistorySearch.flxClearSearchImage.onClick = function() {
            scopeObj.view.transactionHistorySearch.tbxSearchBox.text = "";
            scopeObj.view.transactionHistorySearch.flxClearSearchImage.setVisibility(false);
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "template": "flxCustMangTransactionHistoryCompanies"
            };
            var listOfWidgetsToHide = [scopeObj.view.flxTransactionHistorySegmentHeader, scopeObj.view.flxSeperator4];
            scopeObj.clearSearch(scopeObj.view.segTransactionHistory, scopeObj.view.rtxMsgTransctions, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.TOEND"), scopeObj.recordsSize, toAdd, listOfWidgetsToHide);
        };
        /*Search header*/
        this.view.flxCompanyName.onClick = function() {
            var segData = scopeObj.view.segSearchResults.data;
            var sortedData = scopeObj.sortAndSetData(segData, "lblCompanyName.info.value", "searchList");
            scopeObj.view.segSearchResults.setData(sortedData);
            scopeObj.view.forceLayout();
        };
        this.view.flxEmailHeader.onClick = function() {
            var segData = scopeObj.view.segSearchResults.data;
            var sortedData = scopeObj.sortAndSetData(segData, "lblEmail", "searchList");
            scopeObj.view.segSearchResults.setData(sortedData);
            scopeObj.view.forceLayout();
        };
        /*Accounts header*/
        this.view.flxAccountType.onClick = function() {
            var segData = scopeObj.view.segCompanyDetailAccount.data;
            var sortedData = scopeObj.sortAndSetData(segData, "lblProductType", "accounts");
            scopeObj.view.segCompanyDetailAccount.setData(sortedData);
            scopeObj.view.forceLayout();
        };
        this.view.flxAccountName.onClick = function() {
            var segData = scopeObj.view.segCompanyDetailAccount.data;
            var sortedData = scopeObj.sortAndSetData(segData, "lblProductName.text", "accounts");
            scopeObj.view.segCompanyDetailAccount.setData(sortedData);
            scopeObj.view.forceLayout();
        };
        this.view.flxAccountNumber.onClick = function() {
            var segData = scopeObj.view.segCompanyDetailAccount.data;
            var sortedData = scopeObj.sortAndSetData(segData, "lblAccountNumber.text", "accounts");
            scopeObj.view.segCompanyDetailAccount.setData(sortedData);
            scopeObj.view.forceLayout();
        };
        this.view.flxStatus.onClick = function() {
            var segData = scopeObj.view.segCompanyDetailAccount.data;
            var sortedData = scopeObj.sortAndSetData(segData, "lblStatus.text", "accounts");
            scopeObj.view.segCompanyDetailAccount.setData(sortedData);
            scopeObj.view.forceLayout();
        };
        /*business user header*/
        this.view.flxCustomerName.onClick = function() {
            var segData = scopeObj.view.segCompanyDetailCustomer.data;
            var sortedData = scopeObj.sortAndSetData(segData, "lblName.text", "businessUsers");
            scopeObj.view.segCompanyDetailCustomer.setData(sortedData);
            scopeObj.view.forceLayout();
        };
        this.view.flxRole.onClick = function() {
            var segData = scopeObj.view.segCompanyDetailCustomer.data;
            var sortedData = scopeObj.sortAndSetData(segData, "lblRole", "businessUsers");
            scopeObj.view.segCompanyDetailCustomer.setData(sortedData);
            scopeObj.view.forceLayout();
        };
        this.view.flxUserName.onClick = function() {
            var segData = scopeObj.view.segCompanyDetailCustomer.data;
            var sortedData = scopeObj.sortAndSetData(segData, "lblUsername", "businessUsers");
            scopeObj.view.segCompanyDetailCustomer.setData(sortedData);
            scopeObj.view.forceLayout();
        };
        this.view.flxCustomerEmailID.onClick = function() {
            var segData = scopeObj.view.segCompanyDetailCustomer.data;
            var sortedData = scopeObj.sortAndSetData(segData, "lblEmail", "businessUsers");
            scopeObj.view.segCompanyDetailCustomer.setData(sortedData);
            scopeObj.view.forceLayout();
        };
        /*transactions sorting*/
        this.view.flxTranasctionRefNo.onClick = function() {
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "flxAmountOriginal": "",
                "lblAmountOriginalSign": "",
                "lblAmountOriginalSymbol": "",
                "lblAmountOriginal": "",
                "flxAmountConverted": "",
                "lblAmountConvertedSign": "",
                "lblAmountConvertedSymbol": "",
                "lblAmountConverted": "",
                "template": "flxCustMangTransactionHistoryCompanies"
            };
            scopeObj.sortForTransactions(scopeObj.view.segTransactionHistory, "lblRefNo", scopeObj.view.fonticonSortTranasctionRefNo, "TOEND", scopeObj.recordsSize, toAdd);
        };
        this.view.flxTransactionDateAndTime.onClick = function() {
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "flxAmountOriginal": "",
                "lblAmountOriginalSign": "",
                "lblAmountOriginalSymbol": "",
                "lblAmountOriginal": "",
                "flxAmountConverted": "",
                "lblAmountConvertedSign": "",
                "lblAmountConvertedSymbol": "",
                "lblAmountConverted": "",
                "template": "flxCustMangTransactionHistoryCompanies"
            };
            scopeObj.sortForTransactions(scopeObj.view.segTransactionHistory, "lblDateAndTime", scopeObj.view.fonticonSortTransactionDateAndTime, "TOEND", scopeObj.recordsSize, toAdd);
        };
        this.view.flxTransactionType.onClick = function() {
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "flxAmountOriginal": "",
                "lblAmountOriginalSign": "",
                "lblAmountOriginalSymbol": "",
                "lblAmountOriginal": "",
                "flxAmountConverted": "",
                "lblAmountConvertedSign": "",
                "lblAmountConvertedSymbol": "",
                "lblAmountConverted": "",
                "template": "flxCustMangTransactionHistoryCompanies"
            };
            scopeObj.sortForTransactions(scopeObj.view.segTransactionHistory, "lblType", scopeObj.view.fonticonSortTransactionType, "TOEND", scopeObj.recordsSize, toAdd);
        };
        this.view.flxTransactionAmountOriginal.onClick = function() {
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "flxAmountOriginal": "",
                "lblAmountOriginalSign": "",
                "lblAmountOriginalSymbol": "",
                "lblAmountOriginal": "",
                "flxAmountConverted": "",
                "lblAmountConvertedSign": "",
                "lblAmountConvertedSymbol": "",
                "lblAmountConverted": "",
                "template": "flxCustMangTransactionHistoryCompanies"
            };
            scopeObj.sortForTransactions(scopeObj.view.segTransactionHistory, "lblAmountOriginal", scopeObj.view.fonticonSortTransactionAmountOriginal, "TOEND", scopeObj.recordsSize, toAdd);
        };
        this.view.flxTransactionAmountConverted.onClick = function() {
            var toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmount": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "flxAmountOriginal": "",
                "lblAmountOriginalSign": "",
                "lblAmountOriginalSymbol": "",
                "lblAmountOriginal": "",
                "flxAmountConverted": "",
                "lblAmountConvertedSign": "",
                "lblAmountConvertedSymbol": "",
                "lblAmountConverted": "",
                "template": "flxCustMangTransactionHistoryCompanies"
            };
            scopeObj.sortForTransactions(scopeObj.view.segTransactionHistory, "lblAmountConverted", scopeObj.view.fonticonSortTransactionAmountConverted, "TOEND", scopeObj.recordsSize, toAdd);
        };
        this.view.textBoxOwnerDetailsEntry22.tbxEnterValue.onKeyUp = function() {
            scopeObj.view.textBoxOwnerDetailsEntry22.flxEnterValue.skin = "sknflxEnterValueNormal";
            scopeObj.view.textBoxOwnerDetailsEntry22.flxInlineError.setVisibility(false);
        };
        this.view.lblCreateCompanyLink.onClick = function() {
            scopeObj.view.mainHeader.btnAddNewOption.onClick();
        };
        this.view.customCalOwnerDOB.event = function() {
            scopeObj.view.flxCalendarDOB.skin = "sknflxffffffoptemplateop3px";
            scopeObj.view.textBoxOwnerDetailsEntry21.flxInlineError.setVisibility(false);
            scopeObj.view.forceLayout();
        };
    },
    accountUnlink: function() {
        var self = this;
        var success = function(res) {
            self.view.flxUnlinkAccount.setVisibility(false);
            self.getCompanyAccounts(self.completeCompanyDetails.CompanyContext[0].id, self.setCompanyAccounts);
        };
        var error = function(res) {
            kony.print("error while unlinking the accounts", res);
        };
        this.presenter.unlinkAccounts(self.accountsUnlinkPayload, success, error);
    },
    onSerchSegRowClick: function() {
        var rowData = this.view.segSearchResults.selectedRowItems[0];
        this.showCreatedCompanyDetails(rowData.Organisation_id, 1);
        this.view.flxAccountSegment.setVisibility(true);
        this.view.flxCompanyAccountsDetail.setVisibility(false);
    },
    changeSubTabSkin: function(list, widget) {
        for (var i = 0; i < list.length; i++) {
            list[i].skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
        }
        widget.skin = "sknbtnBgffffffLato485c75Radius3Px12Px";
    },
    assingText: function(segment, textBox) {
        var selectedRow = segment.data[segment.selectedRowIndex[1]];
        textBox.text = selectedRow.lblAddress.text;
        textBox.info.isValid = true;
        textBox.info.data = selectedRow;
        segment.setVisibility(false);
        this.view.flxCountry.zIndex = 1;
        this.view.forceLayout();
    },
    showAccounts: function() {
        this.tabUtilLabelFunction([this.view.lblTabName1,
            this.view.lblTabName2
        ], this.view.lblTabName1);
        this.view.flxCompanyDetailAccountsContainer.setVisibility(true);
        this.view.flxCompanyDetailaCustomerContainer.setVisibility(false);
        this.view.forceLayout();
    },
    showCustomers: function(opt) {
        this.tabUtilLabelFunction([this.view.lblTabName1,
            this.view.lblTabName2
        ], this.view.lblTabName2);
        this.view.flxCompanyDetailaCustomerContainer.setVisibility(true);
        this.view.flxCompanyDetailAccountsContainer.setVisibility(false);
        this.view.forceLayout();
    },
    getCompanyAllDetails: function(id, success) {
        var self = this;
        var payLoad = {
            "id": id,
        };
        self.presenter.getCompanyAllDetails(payLoad);
    },
    getCompanyAccounts: function(id, success) {
        var self = this;
        var accountsSuccess = function(accountContext) {
            self.completeCompanyDetails.accountContext = accountContext.accountContext;
            success();
        };
        var payLoad = {
            "Organization_id": id
        };
        self.presenter.getCompanyAccounts(payLoad, accountsSuccess, self.createdCompanyDetailError);
    },
    getCompanyCustomers: function(id, success) {
        var self = this;
        var accountsSuccess = function(customerContext) {
            self.completeCompanyDetails.customerContext = customerContext.customerContext;
            success();
        };
        var payLoad = {
            "Organization_id": id
        };
        self.presenter.getCompanyCustomers(payLoad, accountsSuccess, self.createdCompanyDetailError);
    },
    createdCompanyDetailError: function(error) {
        kony.print("getCompanyAccounts(form controller)not able to fetch company details", error);
    },
    getCompleteDataForCreatedCompany: function(context) {
        var self = this;
        var detailSuccess = function() {
            self.setCompanyDetails();
            self.setCompanyAccounts();
            self.setCompanyCustomers();
        };
        self.completeCompanyDetails.CompanyContext = context.CompanyContext;
        self.completeCompanyDetails.OwnerContext = context.OwnerContext;
        self.completeCompanyDetails.CompanyType = context.CompanyContext[0].TypeId;
        self.completeCompanyDetails.accountContext = context.accountContext;
        self.completeCompanyDetails.customerContext = context.customerContext;
        detailSuccess();
    },
    showCreatedCompanyDetails: function(id) {
        this.getCompanyAllDetails(id);
    },
    detailScreenVisibilties: function() {
        this.view.flxCompanyDetailAccountsContainer.setVisibility(true);
        this.view.flxAccountSegment.setVisibility(true);
        this.view.flxCompanyAccountsDetail.setVisibility(false);
        this.view.backToAccounts.setVisibility(true);
        this.view.AccountsHeader.setVisibility(true);
        this.view.flxAccountsTabs.setVisibility(true);
        this.view.flxTransactionContainer.setVisibility(true);
        this.view.flxProductDetailContainer.setVisibility(false);
        this.view.flxCompanyDetailaCustomerContainer.setVisibility(false);
    },
    setCompanyDetails: function() {
        var companyDetails = this.completeCompanyDetails.CompanyContext[0];
        var ownerDetails = this.completeCompanyDetails.OwnerContext[0];
        this.detailScreenVisibilties();
        var companyName = this.completeCompanyDetails.CompanyContext[0].Name;
        this.view.breadcrumbs.lblCurrentScreen.text = companyName ? (companyName).toUpperCase() : "COMPANY";
        if (this.completeCompanyDetails.CompanyType === "TYPE_ID_SMALL_BUSINESS") {
            this.view.lblTypeValue.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.TypeName);
            this.view.lblCompanyDetailName.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.Name);
            this.view.flxFlagBackground.skin = "sknFlx1fd9bdBor1pxRound4px";
            this.view.lblCompanyDetailEmailValue.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.Email);
            this.view.lblCompanyDetailContactValue.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.Phone);
            this.view.lblCompanyDetailAddressValue.text = companyDetails.addressLine1 + " " + companyDetails.addressLine2 + " " + companyDetails.state + " " + companyDetails.cityName + " " + companyDetails.country + " " + companyDetails.zipCode;
            this.view.flxSamllBusiness.setVisibility(true);
            this.view.flxMicroBusiness.setVisibility(false);
            this.view.flxAccountTin.setVisibility(true);
            this.view.flxAccountNumber.width = "20%";
            this.view.flxAccountName.width = "22.50%";
        } else {
            this.view.lblTypeValue.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.TypeName);
            this.view.lblCompanyDetailName.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.Name);
            this.view.flxFlagBackground.skin = "sknFlx7b70a1BorRound4px";
            this.view.lblMBTinValue.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.Tin);
            this.view.lblMBCompanyIdValue.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.id);
            this.view.lblMBEmailValue.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.Email);
            this.view.lblMBContactValue.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.Phone);
            this.view.lblMBAddressValue.text = companyDetails.addressLine1 + " " + companyDetails.addressLine2 + " " + companyDetails.state + " " + companyDetails.cityName + " " + companyDetails.country + " " + companyDetails.zipCode;
            this.view.lblOwnerNameValue.text = ownerDetails.FirstName + " " + ownerDetails.MiddleName + " " + ownerDetails.LastName;
            this.view.lblOwnerEmailValue.text = this.AdminConsoleCommonUtils.getTruncatedString(this.AdminConsoleCommonUtils.getParamValueOrNA(ownerDetails.Email), 22, 19);
            if (this.view.lblOwnerEmailValue.text.length >= 22) this.view.lblOwnerEmailValue.toolTip = this.AdminConsoleCommonUtils.getParamValueOrNA(ownerDetails.Email);
            this.view.lblOwnerContactValue.text = this.AdminConsoleCommonUtils.getParamValueOrNA(ownerDetails.Phone);
            this.view.lblOwnerSSNValue.text = this.AdminConsoleCommonUtils.getParamValueOrNA(ownerDetails.Ssn);
            var modifiedDate = this.AdminConsoleCommonUtils.getParamValueOrNA(ownerDetails.DOB);
            this.view.lblOwnerDOBValue.text = modifiedDate !== "N/A" ? this.getLocaleDate(modifiedDate) : "N/A";
            this.view.flxSamllBusiness.setVisibility(false);
            this.view.flxMicroBusiness.setVisibility(true);
            this.view.lblArrowIcon.text = "\ue922";
            this.view.flxMBOwnerDetails.setVisibility(false);
            this.view.flxAccountTin.setVisibility(false);
            this.view.flxAccountNumber.width = "28.75%";
            this.view.flxAccountName.width = "30%";
        }
    },
    /*
     * set data to company accounts 
     */
    setCompanyAccounts: function() {
        var accountContext = this.completeCompanyDetails.accountContext;
        var finalData = accountContext.map(this.mappingCompanyAccounts);
        var widgetDataMap = {
            "flxCustMangProduct": "flxCustMangProduct",
            "flxCustMangProductInfo": "flxCustMangProductInfo",
            "flxProductType": "flxProductType",
            "flxStatus": "flxStatus",
            "lblAccountNumber": "lblAccountNumber",
            "lblProductName": "lblProductName",
            "lblProductType": "lblProductType",
            "lblSeperator": "lblSeperator",
            "btnAccountOwner": "btnAccountOwner",
            "lblAccountOwner": "lblAccountOwner",
            "fontIconStatus": "fontIconStatus",
            "lblStatus": "lblStatus",
            "flxUnlink": "flxUnlink",
            "flblUnlink": "flblUnlink",
            "flxAccountOwner": "flxAccountOwner"
        };
        this.view.segCompanyDetailAccount.widgetDataMap = widgetDataMap;
        this.sortBy = this.getObjectSorter("lblProductType");
        this.resetSortImages("accounts");
        var sortedData = finalData.sort(this.sortBy.sortData);
        this.view.segCompanyDetailAccount.setData(sortedData);
        this.view.forceLayout();
    },
    mappingCompanyAccounts: function(data) {
        var self = this;
        return {
            "flxCustMangProduct": "flxCustMangProduct",
            "flxCustMangProductInfo": {
                "onHover": self.onHoverNotificationSeg
            },
            "flxProductType": {
                "width": "22%"
            },
            "flxStatus": "flxStatus",
            "lblAccountNumber": {
                "text": data.Account_id ? data.Account_id : "N/A",
                "width": self.completeCompanyDetails.CompanyType === "TYPE_ID_SMALL_BUSINESS" ? "21.8%" : "31%"
            },
            "lblProductName": {
                "text": data.AccountName ? data.AccountName : "N/A",
                "width": self.completeCompanyDetails.CompanyType === "TYPE_ID_SMALL_BUSINESS" ? "24.2%" : "32%"
            },
            "lblProductType": data.Account_Type ? data.Account_Type : "N/A",
            "lblSeperator": ".",
            "btnAccountOwner": {
                "isVisible": false,
            },
            "lblAccountOwner": {
                "isVisible": self.completeCompanyDetails.CompanyType === "TYPE_ID_SMALL_BUSINESS" ? true : false,
                "text": self.completeCompanyDetails.CompanyType === "TYPE_ID_SMALL_BUSINESS" ? self.AdminConsoleCommonUtils.getParamValueOrNA(data.Taxid) : "",
            },
            "flxAccountOwner": {
                "isVisible": self.completeCompanyDetails.CompanyType === "TYPE_ID_SMALL_BUSINESS" ? true : false,
                "width": self.completeCompanyDetails.CompanyType === "TYPE_ID_SMALL_BUSINESS" ? "19.1%" : "0%"
            },
            "fontIconStatus": {
                "skin": data.StatusDesc.toLowerCase() === "active" ? "sknFontIconActivate" : "sknfontIconInactive"
            },
            "lblStatus": {
                "text": data.StatusDesc ? data.StatusDesc : "N/A",
                "skin": data.StatusDesc.toLowerCase() === "active" ? "sknlblLato5bc06cBold14px" : "sknlblLatocacacaBold12px"
            },
            "flxUnlink": {
                isVisible: false,
            },
            "flblUnlink": {
                "text": "\ue974",
                "tooltip": "Unlink"
            },
            "template": "flxCustMangProductInfo"
        };
    },
    setCompanyCustomers: function() {
        var customerContext = this.completeCompanyDetails.customerContext;
        if (customerContext.length > 0) {
            var finalData = customerContext.map(this.mappingCompanyCustomers);
            var widgetMap = {
                "lblName": "lblName",
                "lblRole": "lblRole",
                "lblUsername": "lblUsername",
                "lblCustomerStatus": "lblCustomerStatus",
                "lblEmail": "lblEmail",
                "lblSepartor": "lblSepartor",
                "customerEditInfo": "customerEditInfo",
                "flxOptions": "flxOptions",
                "lblFontIconOptions": "lblFontIconOptions",
                "lblIconStatus": "lblIconStatus"
            };
            this.view.segCompanyDetailCustomer.widgetDataMap = widgetMap;
            this.sortBy = this.getObjectSorter("lblName.text");
            this.resetSortImages("businessUsers");
            var sortedData = finalData.sort(this.sortBy.sortData);
            this.view.segCompanyDetailCustomer.setData(sortedData);
            this.view.flxAddCustomers.setVisibility(false);
            this.view.btnCreateCustomer.setVisibility(true);
            this.view.flxCustomerHeader.setVisibility(true);
            this.view.segCompanyDetailCustomer.setVisibility(true);
            this.view.contextualMenu.setVisibility(false);
            this.view.forceLayout();
        } else {
            this.view.flxAddCustomers.setVisibility(true);
            this.view.btnCreateCustomer.setVisibility(false);
            this.view.flxCustomerHeader.setVisibility(false);
            this.view.segCompanyDetailCustomer.setVisibility(false);
            this.view.forceLayout();
        }
    },
    mappingCompanyCustomers: function(data) {
        var self = this;
        var statusText;
        var statusSkin;
        var statusIcon;
        if (data.status === "NEW") {
            statusText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.New");
            statusSkin = "sknlblLato5bc06cBold14px";
            statusIcon = "sknFontIconActivate";
        } else if (data.status === "ACTIVE") {
            statusText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Active");
            statusSkin = "sknlblLato5bc06cBold14px";
            statusIcon = "sknFontIconActivate";
        } else if (data.status === "SUSPENDED") {
            statusText = kony.i18n.getLocalizedString("i18n.frmCustomers.Suspended");
            statusSkin = "sknlblLatoeab55d12px";
            statusIcon = "sknFontIconSuspend";
        } else if (data.status === "LOCKED") {
            statusText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Locked");
            statusSkin = "sknlblCustMngLocked";
            statusIcon = "sknfontIconLock12px";
        }
        var statusKeys = data.RiskStatus ? data.RiskStatus.split(",") : data.RiskStatus;
        return {
            "lblName": {
                "text": self.AdminConsoleCommonUtils.getParamValueOrEmptyString(data.FirstName) + self.AdminConsoleCommonUtils.getParamValueOrEmptyString(data.MiddleName) + self.AdminConsoleCommonUtils.getParamValueOrEmptyString(data.LastName),
                "onClick": self.onClickUserNavigateToProfile
            },
            "lblRole": data.role_name ? data.role_name : "N/A",
            "lblUsername": data.UserName ? data.UserName : "N/A",
            "lblCustomerStatus": {
                "text": statusText,
                "skin": statusSkin
            },
            "lblIconStatus": {
                "text": "\ue921",
                "skin": statusIcon
            },
            "lblEmail": data.Email ? data.Email : "N/A",
            "lblFontIconOptions": "",
            "flxOptions": {
                "onClick": function() {
                    self.toggleContextualMenu(50);
                }
            },
            "lblSepartor": {
                "text": "-"
            },
            "customerEditInfo": {
                "id": data.id,
                "DateOfBirth": data.DateOfBirth,
                "DrivingLicenseNumber": data.DrivingLicenseNumber,
                "FirstName": data.FirstName,
                "LastName": data.LastName,
                "Lastlogintime": data.Lastlogintime,
                "MiddleName": data.MiddleName,
                "Ssn": data.Ssn,
                "UserName": data.UserName,
                "createdts": data.createdts,
                "role_name": data.role_name,
                "status": data.status,
                "Email": data.Email,
                "Phone": data.Phone,
                "isEAgreementRequired": data.isEAgreementRequired,
                "isEagreementSigned": data.isEagreementSigned,
                "customerFlags": statusKeys,
                "company": self.completeCompanyDetails.CompanyContext[0]
            }
        };
    },
    toggleContextualMenu: function(rowHeight) {
        var index = this.view.segCompanyDetailCustomer.selectedIndex;
        this.sectionIndex = index[0];
        var rowIndex = index[1];
        var height = (((rowIndex) * rowHeight) + 50) - this.view.segCompanyDetailCustomer.contentOffsetMeasured.y;
        var flexLeft = this.view.segCompanyDetailCustomer.clonedTemplates[rowIndex].flxOptions.frame.x;
        this.view.contextualMenu.left = (flexLeft - 90) + "px";
        var contextMenuFrameHeight = this.view.contextualMenu.frame.height;
        var segCompanyDetailsFrameHeight = this.view.segCompanyDetailCustomer.frame.height;
        if (((contextMenuFrameHeight + height) > (segCompanyDetailsFrameHeight)) && contextMenuFrameHeight < segCompanyDetailsFrameHeight) {
            this.view.contextualMenu.top = ((height - contextMenuFrameHeight) - 19 + 100) + "px";
        } else {
            this.view.contextualMenu.top = (height + 100) + "px";
        }
        if (this.view.contextualMenu.isVisible === false) {
            this.view.contextualMenu.setVisibility(true);
        } else {
            this.view.contextualMenu.setVisibility(false);
        }
        this.view.forceLayout();
        kony.print(kony.i18n.getLocalizedString("i18n.frmGroupsController.called_in_form_controller"));
    },
    onHoverNotificationSeg: function(widget, context) {
        var self = this;
        var rowData = self.view.segCompanyDetailAccount.data[context.rowIndex];
        var Organization_id = self.completeCompanyDetails.CompanyContext[0].id;
        var unlinkFunction = function() {
            self.view.popUp.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Unlink_Account");
            self.view.popUp.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Unlink_accounts_message");
            self.view.popUp.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.frmCompanies.UNLINK");
            self.view.flxUnlinkAccount.setVisibility(true);
            self.accountsUnlinkPayload = {
                "Organization_id": Organization_id,
                "AccountsList": [{
                    "Account_id": rowData.lblAccountNumber.text,
                }]
            };
        };
        if (self.view.segCompanyDetailAccount.data.length > 1) {
            if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
                if (rowData.flxUnlink.isVisible === false) {
                    rowData.flxUnlink.isVisible = true;
                    rowData.flxUnlink.onClick = unlinkFunction;
                    self.view.segCompanyDetailAccount.setDataAt(rowData, context.rowIndex);
                }
            } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                if (rowData.flxUnlink.isVisible === true) {
                    rowData.flxUnlink.isVisible = false;
                    self.view.segCompanyDetailAccount.setDataAt(rowData, context.rowIndex);
                }
            }
        }
    },
    validatingCompanyDetails: function() {
        // company name
        var validation = true;
        if (this.view.tbxNameValue.text.trim() === "") {
            this.view.flxNoNameError.setVisibility(true);
            this.view.lblNoNameError.text = kony.i18n.getLocalizedString("i18n.frmLocationsController.Enter_Company_Name");
            this.view.tbxNameValue.skin = "skinredbg";
            validation = false;
        }
        // contact number
        var phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
        if (!this.view.contactNumber.txtContactNumber.text || !this.view.contactNumber.txtContactNumber.text.trim()) {
            this.view.contactNumber.flxError.width = "61%";
            this.view.contactNumber.flxError.isVisible = true;
            this.view.contactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_cannot_be_empty");
            this.view.contactNumber.txtContactNumber.skin = "skinredbg";
            validation = false;
        } else if (this.view.contactNumber.txtContactNumber.text.trim().length > 15) {
            this.view.contactNumber.flxError.width = "61%";
            this.view.contactNumber.flxError.isVisible = true;
            this.view.contactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_cannot_exceed");
            this.view.contactNumber.txtContactNumber.skin = "skinredbg";
            validation = false;
        } else if (phoneRegex.test(this.view.contactNumber.txtContactNumber.text) === false) {
            this.view.contactNumber.flxError.width = "61%";
            this.view.contactNumber.flxError.isVisible = true;
            this.view.contactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_not_valid");
            this.view.contactNumber.txtContactNumber.skin = "skinredbg";
            validation = false;
        }
        //ISD code
        var ISDRegex = /^\+(\d{1,3}|\d{1,3})$/;
        if (!this.view.contactNumber.txtISDCode.text || !this.view.contactNumber.txtISDCode.text.trim() || this.view.contactNumber.txtISDCode.text.trim().length > 4 || ISDRegex.test(this.view.contactNumber.txtISDCode.text) === false) {
            this.view.contactNumber.flxError.width = "98%";
            this.view.contactNumber.flxError.isVisible = true;
            this.view.contactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
            this.view.contactNumber.txtISDCode.skin = "skinredbg";
            validation = false;
        }
        // email id
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (this.view.tbxEmailValue.text.trim() === "") {
            this.view.lblNoEmailError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EmailId_cannot_be_empty");
            this.view.flxNoEmailError.isVisible = true;
            this.view.tbxEmailValue.skin = "skinredbg";
            validation = false;
        } else if (emailRegex.test(this.view.tbxEmailValue.text.trim()) === false) {
            this.view.lblNoEmailError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EmailID_not_valid");
            this.view.flxNoEmailError.isVisible = true;
            this.view.tbxEmailValue.skin = "skinredbg";
            validation = false;
        }
        //street name 
        if (this.view.tbxStreetName.text.trim() === "") {
            this.view.flxNoStreetName.setVisibility(true);
            this.view.lblNoStreetNameError.text = kony.i18n.getLocalizedString("i18n.common.enterstreetName");
            this.view.tbxStreetName.skin = "skinredbg";
            validation = false;
        }
        //ZipCode
        if (/^([a-zA-Z0-9_]+)$/.test(this.view.tbxZipCode.text) === false) {
            this.view.lblNoZipError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorZipCode");
            this.view.flxNoZipCode.isVisible = true;
            this.view.tbxZipCode.skin = "skinredbg";
            validation = false;
        }
        //City
        if (this.view.typeHeadCity.tbxSearchKey.text.trim() === "" || this.view.typeHeadCity.segSearchResult.isVisible || this.view.typeHeadCity.flxNoResultFound.isVisible) {
            this.view.lblNoCityError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorSelectCity");
            this.view.flxNoCity.isVisible = true;
            this.view.typeHeadCity.tbxSearchKey.skin = "skinredbg";
            validation = false;
        } else if (this.view.typeHeadCity.tbxSearchKey.info.isValid === false) {
            this.view.lblNoCityError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorCity");
            this.view.flxNoCity.isVisible = true;
            this.view.typeHeadCity.tbxSearchKey.skin = "skinredbg";
            validation = false;
        }
        //State
        if (this.view.typeHeadState.tbxSearchKey.text.trim() === "" || this.view.typeHeadState.segSearchResult.isVisible || this.view.typeHeadState.flxNoResultFound.isVisible) {
            this.view.lblNoStateError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorSelectState");
            this.view.flxNoState.isVisible = true;
            this.view.typeHeadState.tbxSearchKey.skin = "skinredbg";
            validation = false;
        } else if (this.view.typeHeadState.tbxSearchKey.info.isValid === false) {
            this.view.lblNoStateError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorState");
            this.view.flxNoState.isVisible = true;
            this.view.typeHeadState.tbxSearchKey.skin = "skinredbg";
            validation = false;
        }
        //Country
        if (this.view.typeHeadCountry.tbxSearchKey.text.trim() === "" || this.view.typeHeadCountry.segSearchResult.isVisible || this.view.typeHeadCountry.flxNoResultFound.isVisible) {
            this.view.lblNoCountryError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorSelectCountry");
            this.view.flxNoCountry.isVisible = true;
            this.view.typeHeadCountry.tbxSearchKey.skin = "skinredbg";
            validation = false;
        } else if (this.view.typeHeadCountry.tbxSearchKey.info.isValid === false) {
            this.view.lblNoCountryError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorCountry");
            this.view.flxNoCountry.isVisible = true;
            this.view.typeHeadCountry.tbxSearchKey.skin = "skinredbg";
            validation = false;
        }
        return validation;
    },
    clearValidation: function(widget, errorFlex, type) {
        if (type === 1) widget.skin = "skntxtbxDetails0bbf1235271384a";
        else if (type === 2) widget.skin = "sknlstbxNormal0f9abd8e88aa64a";
        else if (type === 3) widget.skin = "sknflxEnterValueNormal";
        errorFlex.setVisibility(false);
    },
    getGoogleSuggestion: function(text) {
        var self = this;

        function onSuccess(response) {
            console.log(response);
            self.view.segSearch.setVisibility(true);
            self.setSerchSegmentData(response);
            self.view.forceLayout();
        }

        function onError(response) {
            kony.print("Error", response);
        }
        this.presenter.getAddressSuggestion(text, onSuccess, onError);
    },
    mapping: function(data) {
        return {
            "lblId": data.place_id,
            "lblAddress": data.description,
            "lblPinIcon": "",
            "lat": "17.4947934",
            "long": "78.3996441"
        };
    },
    setSerchSegmentData: function(data) {
        var self = this;
        var finalData;
        if (data.predictions) {
            finalData = data.predictions.map(self.mapping);
            var dataMap = {
                lblAddress: "lblAddress",
                lblPinIcon: "lblPinIcon"
            };
            this.view.segSearch.widgetDataMap = dataMap;
            this.view.segSearch.setData(finalData);
        }
    },
    mappingRowToWidgets: function() {
        var self = this;
        var data = this.view.segSearch.data;
        var index = this.view.segSearch.selectedRowIndex[1];
        var id = data[index].lblId;

        function onSuccess(response) {
            self.fillingData(response, data[index]);
        }

        function onError(reponse) {
            kony.print("Error", reponse);
        }
        this.presenter.getPlaceDetails(id, onSuccess, onError);
    },
    fillingData: function(response, rowData) {
        this.clearAllAddressFields();
        var self = this;
        if (response.result) {
            var finalresponse = response.result;
            self.view.tbxSearch.text = "";
            self.view.segSearch.setVisibility(false);
            var pin = {
                id: rowData.lblId,
                lat: finalresponse[0].latitude,
                lon: finalresponse[0].longitude,
                name: "",
                image: "pinb.png",
                focusImage: "pinb.png",
                desc: "",
                showCallout: false
            };
            var locationData = {
                lat: finalresponse[0].latitude,
                lon: finalresponse[0].longitude,
            };
            self.view.typeHeadState.tbxSearchKey.setEnabled(true);
            self.view.typeHeadCity.tbxSearchKey.setEnabled(true);
            self.view.map1.mapLocations.addPin(pin);
            self.view.map1.mapLocations.navigateToLocation(locationData, false, true);
            self.view.tbxStreetName.text = rowData.lblAddress;
            for (var i = 0; i < finalresponse[0].address_components.length; i++) {
                if (finalresponse[0].address_components[i].types.search("postal_code") !== -1) {
                    self.view.tbxZipCode.text = finalresponse[0].address_components[i].long_name;
                } else if (finalresponse[0].address_components[i].types.search("country") !== -1) {
                    if (self.checkAvailabilty(finalresponse[0].address_components[i].long_name, self.view.typeHeadCountry.segSearchResult.data)) {
                        self.view.typeHeadCountry.tbxSearchKey.text = finalresponse[0].address_components[i].long_name;
                        self.view.typeHeadCountry.tbxSearchKey.info.isValid = true;
                    } else {
                        self.view.flxNoCountry.setVisibility(true);
                        self.view.lblNoCountryError.text = "invalid country";
                        self.view.forceLayout();
                    }
                } else if (finalresponse[0].address_components[i].types.search("locality") !== -1) {
                    if (self.checkAvailabilty(finalresponse[0].address_components[i].long_name, self.view.typeHeadCity.segSearchResult.data)) {
                        self.view.typeHeadCity.tbxSearchKey.text = finalresponse[0].address_components[i].long_name;
                        self.view.typeHeadCity.tbxSearchKey.info.isValid = true;
                    } else {
                        self.view.flxNoCity.setVisibility(true);
                        self.view.lblNoCityError.text = "invalid city";
                        self.view.forceLayout();
                    }
                } else if (finalresponse[0].address_components[i].types.search("administrative_area_level_1") !== -1) {
                    if (self.checkAvailabilty(finalresponse[0].address_components[i].long_name, self.view.typeHeadState.segSearchResult.data)) {
                        self.view.typeHeadState.tbxSearchKey.text = finalresponse[0].address_components[i].long_name;
                        self.view.typeHeadState.tbxSearchKey.info.isValid = true;
                    } else {
                        self.view.flxNoState.setVisibility(true);
                        self.view.lblNoStateError.text = "invalid state";
                        self.view.forceLayout();
                    }
                }
            }
            self.getAddressCodes([self.view.typeHeadCountry.tbxSearchKey.text,
                self.view.typeHeadState.tbxSearchKey.text,
                self.view.typeHeadCity.tbxSearchKey.text
            ]);
        }
    },
    checkAvailabilty: function(key, list) {
        for (var i = 0; i < list.length; i++) {
            if ((list[i].lblAddress.text).toLowerCase().indexOf(key.toLowerCase())) return true;
        }
        return false;
    },
    clearAllAddressFields: function() {
        this.view.tbxStreetName.text = "";
        this.view.tbxBuildingName.text = "";
        this.view.typeHeadCountry.tbxSearchKey.text = "";
        this.view.typeHeadCountry.tbxSearchKey.info.isValid = false;
        this.view.tbxZipCode.text = "";
        this.view.typeHeadCity.tbxSearchKey.text = "";
        this.view.typeHeadCity.tbxSearchKey.info.isValid = false;
        this.view.typeHeadState.tbxSearchKey.text = "";
        this.view.typeHeadState.tbxSearchKey.info.isValid = false;
        this.clearValidation(this.view.tbxStreetName, this.view.flxNoStreetName, 1);
        this.clearValidation(this.view.typeHeadCountry.tbxSearchKey, this.view.flxNoCountry, 1);
        this.clearValidation(this.view.tbxZipCode, this.view.flxNoZipCode, 1);
        this.clearValidation(this.view.typeHeadCity.tbxSearchKey, this.view.flxNoCity, 1);
        this.clearValidation(this.view.typeHeadState.tbxSearchKey, this.view.flxNoState, 1);
    },
    showCompanyDetailsScreen: function() {
        this.hideAllOptionsButtonImages();
        this.view.verticalTabsCompany.flxImgArrow1.setVisibility(true);
        this.view.flxCompanyDetailsTab.setVisibility(true);
        this.view.flxSearchCompanies.setVisibility(false);
        this.view.flxAccountsTab.setVisibility(false);
        this.view.flxOwnerDetailsTabs.setVisibility(false);
        var widgetArray = [this.view.verticalTabsCompany.btnOption1, this.view.verticalTabsCompany.btnOption2, this.view.verticalTabsCompany.btnOption3];
        this.tabUtilVerticleButtonFunction(widgetArray, this.view.verticalTabsCompany.btnOption1);
        this.getAddressSegmentData();
    },
    getAddressSegmentData: function() {
        var self = this;
        var callBack = function(response) {
            kony.print("listboxreponse", response);
            if (response !== "error") {
                self.segCountry = response.countries.reduce(function(list, country) {
                    return list.concat([{
                        "id": country.id,
                        "lblAddress": {
                            "text": country.Name,
                            "left": "10dp"
                        },
                        "template": "flxSearchCompanyMap"
                    }]);
                }, []);
                self.segState = response.regions.reduce(function(list, region) {
                    return list.concat([{
                        "id": region.id,
                        "lblAddress": {
                            "text": region.Name,
                            "left": "10dp"
                        },
                        "Country_id": region.Country_id,
                        "template": "flxSearchCompanyMap"
                    }]);
                }, []);
                self.segLocationCity = response.cities.reduce(function(list, city) {
                    return list.concat([{
                        "id": city.id,
                        "lblAddress": {
                            "text": city.Name,
                            "left": "10dp"
                        },
                        "Region_id": city.Region_id,
                        "template": "flxSearchCompanyMap"
                    }]);
                }, []);
            }
            if (self.action === self.actionConfig.edit) {
                self.getAddressCodes([self.view.typeHeadCountry.tbxSearchKey.text,
                    self.view.typeHeadState.tbxSearchKey.text,
                    self.view.typeHeadCity.tbxSearchKey.text
                ]);
            }
            self.setAddressSegmentData();
        };
        this.presenter.fetchLocationPrefillData(callBack);
    },
    setAddressSegmentData: function() {
        var widgetMap = {
            "flxSearchCompanyMap": "flxSearchCompanyMap",
            "lblAddress": "lblAddress",
            "id": "id",
            "Region_id": "Region_id",
            "Country_id": "Country_id"
        };
        this.view.typeHeadCountry.segSearchResult.widgetDataMap = widgetMap;
        this.view.typeHeadCity.segSearchResult.widgetDataMap = widgetMap;
        this.view.typeHeadState.segSearchResult.widgetDataMap = widgetMap;
        this.view.typeHeadCountry.segSearchResult.setData(this.segCountry);
        this.view.typeHeadCity.segSearchResult.setData(this.segLocationCity);
        this.view.typeHeadState.segSearchResult.setData(this.segState);
    },
    /*
     * show add accounts screen
     */
    showAddAccountsScreen: function() {
        var self = this;
        this.hideAllOptionsButtonImages();
        this.view.verticalTabsCompany.flxImgArrow2.setVisibility(true);
        self.view.flxCompanyDetailsTab.setVisibility(false);
        self.view.flxOwnerDetailsTabs.setVisibility(false);
        self.view.flxAccountsTab.setVisibility(true);
        self.view.addAndRemoveAccounts.segAddOptions.setData([]);
        self.view.addAndRemoveAccounts.segSelectedOptions.setData([]);
        self.view.addAndRemoveAccounts.tbxSearchBox.text = "";
        var widgetArray = [this.view.verticalTabsCompany.btnOption1, this.view.verticalTabsCompany.btnOption2, this.view.verticalTabsCompany.btnOption3];
        self.tabUtilVerticleButtonFunction(widgetArray, this.view.verticalTabsCompany.btnOption2);
        var data = self.view.addAndRemoveAccounts.segSelectedOptions.info.segData || [];
        self.view.addAndRemoveAccounts.segSelectedOptions.setData(data);
        self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Search_by_accountNo");
        self.showHideAddRemoveSegment();
        self.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(false);
        self.adjustAccountsUIBasedOnType(self.view.radioCompanyType.selectedKey);
        self.view.forceLayout();
    },
    /*
     * show create owner details screen
     */
    showCreateOwnerDetails: function() {
        var self = this;
        this.hideAllOptionsButtonImages();
        this.view.verticalTabsCompany.flxImgArrow3.setVisibility(true);
        self.view.flxCompanyDetailsTab.setVisibility(false);
        self.view.flxAccountsTab.setVisibility(false);
        self.view.flxOwnerDetailsTabs.setVisibility(true);
        var widgetArray = [this.view.verticalTabsCompany.btnOption1, this.view.verticalTabsCompany.btnOption2, this.view.verticalTabsCompany.btnOption3];
        self.tabUtilVerticleButtonFunction(widgetArray, this.view.verticalTabsCompany.btnOption3);
    },
    /*
     * hide the create screen and display search companies screen
     */
    hideCreateScreen: function() {
        var self = this;
        self.view.flxCreateCompany.setVisibility(false);
        self.view.flxSearchCompanies.setVisibility(true);
        self.view.forceLayout();
    },
    /*
     * filters accounts data based on search text
     */
    accountSearch: function(response) {
        var self = this;
        self.hideAccountSearchLoading();
        var filteredData = [],
            segData = [],
            searchResult = [];
        if (response && response instanceof Array) {
            searchResult = response;
        } else {
            self.view.addAndRemoveAccounts.segAddOptions.setData([]);
        }
        segData = self.mapAvailableSegmentData(searchResult);
        filteredData = self.filterAlreadyAddedData(segData);
        self.view.addAndRemoveAccounts.segAddOptions.setVisibility((filteredData.length > 0));
        if (filteredData.length <= 0) {
            self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.searchNoResultFoundCompanies");
        } else {
            self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Search_by_accountNo");
        }
        self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.setVisibility((filteredData.length <= 0));
        self.view.addAndRemoveAccounts.segAddOptions.setData(filteredData);
        self.view.forceLayout();
    },
    /*
     * filters already add data from the search result
     * data resulted from the search
     */
    filterAlreadyAddedData: function(searchResultData) {
        var self = this;
        var finalData = [],
            doesExsist = false;
        var searchDataLen = searchResultData.length || 0;
        var selectedAccounts = self.view.addAndRemoveAccounts.segSelectedOptions.data || [];
        if (searchDataLen > 0) {
            finalData = searchResultData.filter(function(rec) {
                doesExsist = false;
                for (var i = 0; i < selectedAccounts.length; i++) {
                    if (selectedAccounts[i].Account_id === rec.lblAccountNum.text) {
                        doesExsist = true;
                        break;
                    }
                }
                if (!doesExsist) {
                    return rec;
                }
            });
        }
        return finalData;
    },
    /*
     * function to map data to available segment data on search
     * @param: result of search
     */
    mapAvailableSegmentData: function(data) {
        var self = this;
        var result = [];
        var type = self.typeConfig.smallBusiness;
        if (self.view.flxHeading.isVisible) type = self.typeConfig.microBusiness;
        else type = self.typeConfig.smallBusiness;
        var widgetMap = {
            "lblAccountNum": "lblAccountNum",
            "lblAccFieldValue1": "lblAccFieldValue1",
            "lblAccFieldValue2": "lblAccFieldValue2",
            "lblAccFieldValue3": "lblAccFieldValue3",
            "lblAccountText": "lblAccountText",
            "lblAccFieldHeader1": "lblAccFieldHeader1",
            "lblAccFieldHeader2": "lblAccFieldHeader2",
            "lblAccFieldHeader3": "lblAccFieldHeader3",
            "btnAddAccount": "btnAddAccount",
            "flxSeperator": "flxSeperator",
            "flxAccField1": "flxAccField1",
            "flxAccField2": "flxAccField2",
            "flxAccField3": "flxAccField3",
            "lblLine": "lblLine",
            "flxAvailableAccounts": "flxAvailableAccounts"
        };
        self.view.addAndRemoveAccounts.segAddOptions.widgetDataMap = widgetMap;
        result = data.map(function(record) {
            if (record.AccountHolder) {
                try {
                    record.AccountHolder = JSON.parse(record.AccountHolder);
                } catch (err) {
                    kony.print("Invalid JSON format for AccountHolder");
                }
            } else {
                record.AccountHolder = {
                    username: "",
                    fullname: ""
                }
            }
            return {
                "lblAccountNum": {
                    "text": record.Account_id
                },
                "lblAccFieldValue1": {
                    "text": record.AccountName
                },
                "lblAccFieldValue2": (type === self.typeConfig.smallBusiness) ? {
                    "text": self.AdminConsoleCommonUtils.getTruncatedString(record.Membership_id, 13, 10),
                    "info": {
                        "value": record.Membership_id
                    },
                    "tooltip": record.Membership_id
                } : {
                    "text": self.AdminConsoleCommonUtils.getTruncatedString(record.AccountName, 13, 10),
                    "info": {
                        "value": record.AccountName
                    },
                    "tooltip": record.AccountName
                },
                "lblAccFieldValue3": (type === self.typeConfig.smallBusiness) ? {
                    "text": self.AdminConsoleCommonUtils.getTruncatedString(record.Taxid, 10, 8),
                    "info": {
                        "value": record.Taxid
                    },
                    "tooltip": record.Taxid
                } : {
                    "text": self.AdminConsoleCommonUtils.getTruncatedString(record.AccountHolder.username, 10, 8),
                    "info": {
                        "value": record.AccountHolder.username
                    },
                    "tooltip": record.AccountHolder.username
                },
                "lblAccountText": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ACCOUNT"),
                "lblAccFieldHeader1": kony.i18n.getLocalizedString("i18n.Group.TYPE"),
                "lblAccFieldHeader2": (type === self.typeConfig.smallBusiness) ? kony.i18n.getLocalizedString("i18n.frmCompanies.MEMBERSHIP_ID") : kony.i18n.getLocalizedString("i18n.permission.NAME"),
                "lblAccFieldHeader3": (type === self.typeConfig.smallBusiness) ? kony.i18n.getLocalizedString("i18n.frmCompanies.TAXNUMBER") : kony.i18n.getLocalizedString("i18n.frmCompanies.HOLDER"),
                "btnAddAccount": {
                    "onClick": self.addSelectedAccount
                },
                "lblLine": {
                    "text": "-"
                },
                "template": "flxAvailableAccounts"
            };
        });
        return result;
    },
    /*
     * (Add Accounts) add left segment selected row to right segment
     */
    addSelectedAccount: function() {
        var self = this;
        var selectedRowData = [];
        var type = self.typeConfig.smallBusiness;
        if (self.view.flxHeading.isVisible) type = self.typeConfig.microBusiness;
        else type = self.typeConfig.smallBusiness;
        var rowIndex = self.view.addAndRemoveAccounts.segAddOptions.selectedIndex[1];
        selectedRowData = self.view.addAndRemoveAccounts.segAddOptions.selectedRowItems[0];
        var widgetMap = {
            "flxClose": "flxClose",
            "fontIconClose": "fontIconClose",
            "lblOption": "lblOption",
            "Account_id": "Account_id",
            "Membership_id": "Membership_id",
            "Taxid": "Taxid",
            "Account_Type": "Account_Type",
            "AccountName": "AccountName",
            "holder": "holder",
            "flxOptionAdded": "flxOptionAdded"
        };
        var recordToRemove = {
            "flxClose": {
                "onClick": function() {
                    self.removeSelectedAccount();
                }
            },
            "fontIconClose": {
                "text": "\ue929",
                "tooltip": "Remove Account"
            },
            "lblOption": {
                "text": "Account  " + self.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.lblAccountNum.text, 15, 12),
                "tooltip": selectedRowData.lblAccountNum.text
            },
            "Account_id": selectedRowData.lblAccountNum.text,
            "Membership_id": (type === self.typeConfig.smallBusiness) ? selectedRowData.lblAccFieldValue2.info.value : self.view.lblMembershipValue.text,
            "Taxid": (type === self.typeConfig.smallBusiness) ? selectedRowData.lblAccFieldValue3.info.value : self.view.lblTinValue.text,
            "AccountName": selectedRowData.lblAccFieldValue2.info.value,
            "holder": selectedRowData.lblAccFieldValue3.info.value,
            "Account_Type": selectedRowData.lblAccFieldValue1.text,
            "template": "flxOptionAdded"
        };
        self.assignedAccount.push(recordToRemove);
        self.view.addAndRemoveAccounts.segSelectedOptions.widgetDataMap = widgetMap;
        self.view.addAndRemoveAccounts.segSelectedOptions.addDataAt(recordToRemove, 0);
        self.view.addAndRemoveAccounts.segAddOptions.removeAt(rowIndex);
        self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = self.view.addAndRemoveAccounts.segSelectedOptions.data;
        self.showHideAddRemoveSegment();
        self.view.forceLayout();
    },
    /*
     * (Remove Accounts) add right segment selected row to left segment
     */
    removeSelectedAccount: function() {
        var self = this;
        var selectedRowData = [],
            type = self.typeConfig.smallBusiness;
        var rowIndex = self.view.addAndRemoveAccounts.segSelectedOptions.selectedIndex[1];
        selectedRowData = self.view.addAndRemoveAccounts.segSelectedOptions.selectedRowItems[0];
        if (self.view.flxHeading.isVisible) type = self.typeConfig.microBusiness;
        else type = self.typeConfig.smallBusiness;
        var recordToAdd = {
            "lblAccountNum": {
                "text": selectedRowData.Account_id
            },
            "lblAccFieldValue1": {
                "text": selectedRowData.Account_Type
            },
            "lblAccFieldValue2": (type === self.typeConfig.smallBusiness) ? {
                "text": self.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.Membership_id, 13, 10),
                "info": {
                    "value": selectedRowData.Membership_id
                },
                "tooltip": selectedRowData.Membership_id
            } : {
                "text": self.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.AccountName, 13, 10),
                "info": {
                    "value": selectedRowData.AccountName
                },
                "tooltip": selectedRowData.AccountName
            },
            "lblAccFieldValue3": (type === self.typeConfig.smallBusiness) ? {
                "text": self.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.Taxid, 10, 8),
                "info": {
                    "value": selectedRowData.Taxid
                },
                "tooltip": selectedRowData.Taxid
            } : {
                "text": self.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.holder, 10, 8),
                "info": {
                    "value": selectedRowData.holder
                },
                "tooltip": selectedRowData.holder
            },
            "lblAccountText": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ACCOUNT"),
            "lblAccFieldHeader1": kony.i18n.getLocalizedString("i18n.Group.TYPE"),
            "lblAccFieldHeader2": (type === self.typeConfig.smallBusiness) ? kony.i18n.getLocalizedString("i18n.frmCompanies.MEMBERSHIP_ID") : kony.i18n.getLocalizedString("i18n.permission.NAME"),
            "lblAccFieldHeader3": (type === self.typeConfig.smallBusiness) ? kony.i18n.getLocalizedString("i18n.frmCompanies.TAXNUMBER") : kony.i18n.getLocalizedString("i18n.frmCompanies.HOLDER"),
            "btnAddAccount": {
                "onClick": self.addSelectedAccount
            },
            "lblLine": {
                "text": "-"
            },
            "template": "flxAvailableAccounts"
        };
        self.view.addAndRemoveAccounts.segAddOptions.addDataAt(recordToAdd, 0);
        self.view.addAndRemoveAccounts.segSelectedOptions.removeAt(rowIndex);
        self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = self.view.addAndRemoveAccounts.segSelectedOptions.data;
        self.showHideAddRemoveSegment();
        self.view.forceLayout();
    },
    /*
     * reset the added accounts in selected accounts column
     */
    resetAccounts: function() {
        var self = this;
        var selectedAcc = [],
            resetRec = [],
            exsistingData = [],
            dataToAdd = [];
        var type = self.typeConfig.smallBusiness;
        if (self.view.flxHeading.isVisible) type = self.typeConfig.microBusiness;
        else type = self.typeConfig.smallBusiness;
        selectedAcc = self.view.addAndRemoveAccounts.segSelectedOptions.data;
        resetRec = selectedAcc.map(function(record) {
            return {
                "lblAccountNum": {
                    "text": record.Account_id
                },
                "lblAccFieldValue1": {
                    "text": record.Account_Type
                },
                "lblAccFieldValue2": (type === self.typeConfig.smallBusiness) ? {
                    "text": self.AdminConsoleCommonUtils.getTruncatedString(record.Membership_id, 13, 10),
                    "info": {
                        "value": record.Membership_id
                    },
                    "tooltip": record.Membership_id
                } : {
                    "text": self.AdminConsoleCommonUtils.getTruncatedString(record.AccountName, 13, 10),
                    "info": {
                        "value": record.AccountName
                    },
                    "tooltip": record.AccountName
                },
                "lblAccFieldValue3": (type === self.typeConfig.smallBusiness) ? {
                    "text": self.AdminConsoleCommonUtils.getTruncatedString(record.Taxid, 10, 8),
                    "info": {
                        "value": record.Taxid
                    },
                    "tooltip": record.Taxid
                } : {
                    "text": self.AdminConsoleCommonUtils.getTruncatedString(record.holder, 10, 8),
                    "info": {
                        "value": record.holder
                    },
                    "tooltip": record.holder
                },
                "lblAccountText": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ACCOUNT"),
                "lblAccFieldHeader1": kony.i18n.getLocalizedString("i18n.Group.TYPE"),
                "lblAccFieldHeader2": (type === self.typeConfig.smallBusiness) ? kony.i18n.getLocalizedString("i18n.frmCompanies.MEMBERSHIP_ID") : kony.i18n.getLocalizedString("i18n.permission.NAME"),
                "lblAccFieldHeader3": (type === self.typeConfig.smallBusiness) ? kony.i18n.getLocalizedString("i18n.frmCompanies.TAXNUMBER") : kony.i18n.getLocalizedString("i18n.frmCompanies.HOLDER"),
                "btnAddAccount": {
                    "onClick": self.addSelectedAccount
                },
                "lblLine": {
                    "text": "-"
                },
                "template": "flxAvailableAccounts"
            };
        });
        exsistingData = self.view.addAndRemoveAccounts.segAddOptions.data;
        dataToAdd = exsistingData.concat(resetRec);
        self.view.addAndRemoveAccounts.segAddOptions.setData(dataToAdd);
        self.view.addAndRemoveAccounts.segSelectedOptions.setData([]);
        self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = [];
        self.showHideAddRemoveSegment();
        self.view.forceLayout();
    },
    /*
     * validation to check atleast one account is assigned 
     */
    checkAccountValidation: function() {
        var self = this;
        var selectedAcc = [];
        selectedAcc = self.view.addAndRemoveAccounts.segSelectedOptions.data;
        return (selectedAcc.length > 0);
    },
    /*
    validating Input Fields when click on search button
    */
    validateInputFields: function() {
        var validationOfSearch = true;
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if ((this.view.tbxName.text === null || this.view.tbxName.text === "") && (this.view.tbxEmailId.text === null || this.view.tbxEmailId.text === "")) {
            this.view.flxNameError.setVisibility(true);
            this.view.flxEmailIdError.setVisibility(true);
            this.view.tbxName.skin = "skinredbg";
            this.view.tbxEmailId.skin = "skinredbg";
            validationOfSearch = false;
        }
        if (this.view.tbxEmailId.text)
            if (emailRegex.test(this.view.tbxEmailId.text.trim()) === false) {
                this.view.lblEmailIdErrorText.text = kony.i18n.getLocalizedString("i18n.frmUsersController.Enter_a_valid_Email-id");
                this.view.flxError.setVisibility(true);
                this.view.flxEmailIdError.setVisibility(true);
                this.view.tbxEmailId.skin = "skinredbg";
                validationOfSearch = false;
            }
        return validationOfSearch;
    },
    /*
    Companies Search Results
    */
    companiesSearch: function() {
        var self = this;
        var searchNameText = "";
        var searchEmailText = "";
        if (this.view.tbxName.text !== null && this.view.tbxName.text !== "") {
            if (this.view.tbxEmailId.text !== null && this.view.tbxEmailId.text !== "") {
                searchNameText = this.view.tbxName.text;
                searchEmailText = this.view.tbxEmailId.text;
            } else {
                searchNameText = this.view.tbxName.text;
            }
        } else if (this.view.tbxEmailId.text !== null && this.view.tbxEmailId.text !== "") {
            searchEmailText = this.view.tbxEmailId.text;
        }
        var payload = {
            "Name": searchNameText,
            "Email": searchEmailText
        };
        self.presenter.getCompaniesSearch(payload);
    },
    mapResultsData: function(data) {
        var self = this;
        var widgetMap = {
            "lblCompanyName": "lblCompanyName",
            "lblTIN": "lblTIN",
            "lblEmail": "lblEmail",
            "lblType": "lblType",
            "lblCompanyId": "lblCompanyId",
            "lblSeperator": "lblSeperator",
            "Organisation_id": "Organisation_id"
        };
        self.view.addAndRemoveAccounts.segAddOptions.widgetDataMap = widgetMap;
        var result = data.map(function(record) {
            return {
                "Organisation_id": record.id,
                "lblCompanyName": {
                    "text": self.AdminConsoleCommonUtils.getTruncatedString(record.Name, 30, 27),
                    "info": {
                        "value": record.Name
                    },
                    "tooltip": record.Name
                },
                "lblType": record.TypeName,
                "lblTIN": record.Tin !== "NA" ? record.Tin : "N/A",
                "lblCompanyId": record.id,
                "lblEmail": record.Email,
                "lblSeperator": "-"
            };
        });
        return result;
    },
    /*
     * toggle segment and noResults_found based on data's length
     */
    showHideAddRemoveSegment: function() {
        var self = this;
        var availableSegData = [],
            selectedSegData = [];
        availableSegData = self.view.addAndRemoveAccounts.segAddOptions.data;
        selectedSegData = self.view.addAndRemoveAccounts.segSelectedOptions.data;
        self.view.addAndRemoveAccounts.segAddOptions.setVisibility(availableSegData.length > 0);
        self.view.addAndRemoveAccounts.segSelectedOptions.setVisibility(selectedSegData.length > 0);
        self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.setVisibility(availableSegData.length <= 0);
        self.view.addAndRemoveAccounts.rtxSelectedOptionsMessage.setVisibility(selectedSegData.length <= 0);
        self.view.addAndRemoveAccounts.btnRemoveAll.setVisibility(selectedSegData.length > 0);
        //validation -atleast one account should be selected
        if (selectedSegData.length > 0) {
            self.view.commonButtonsAccounts.btnSave.skin = "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px";
            self.view.commonButtonsAccounts.btnNext.skin = "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px";
            self.view.commonButtonsAccounts.btnNext.focusSkin = "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px";
            self.view.commonButtonsAccounts.btnNext.hoverSkin = "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px";
            self.view.commonButtonsAccounts.btnSave.focusSkin = "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px";
            self.view.commonButtonsAccounts.btnSave.hoverSkin = "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px";
            self.view.commonButtonsAccounts.btnSave.setEnabled(true);
            self.view.commonButtonsAccounts.btnNext.setEnabled(true);
        } else {
            self.view.commonButtonsAccounts.btnSave.skin = "btnSkinGrey";
            self.view.commonButtonsAccounts.btnNext.skin = "btnSkinGrey";
            self.view.commonButtonsAccounts.btnNext.focusSkin = "btnSkinGrey";
            self.view.commonButtonsAccounts.btnNext.hoverSkin = "btnSkinGrey";
            self.view.commonButtonsAccounts.btnSave.focusSkin = "btnSkinGrey";
            self.view.commonButtonsAccounts.btnSave.hoverSkin = "btnSkinGrey";
            self.view.commonButtonsAccounts.btnSave.setEnabled(false);
            self.view.commonButtonsAccounts.btnNext.setEnabled(false);
        }
    },
    /*
     * search on address fields while typing in textbox
     * @param: textbox path, sement path
     * @param: category ( 1-country, 2- state, 3-city)
     */
    searchForAddress: function(tbxPath, segPath, noResultFlex, category) {
        var self = this;
        var searchText = tbxPath.text;
        var sourceData = [],
            dataToAssign = [];
        if (category === 1) {
            sourceData = self.segCountry;
            dataToAssign = sourceData.filter(function(rec) {
                var name = (rec.lblAddress.text).toLowerCase();
                return (name.indexOf(searchText.toLowerCase()) > -1);
            });
        } else if (category === 2) {
            sourceData = self.segState;
            var country = self.view.typeHeadCountry.tbxSearchKey.info.data;
            dataToAssign = sourceData.filter(function(rec) {
                var name = (rec.lblAddress.text).toLowerCase();
                return ((name.indexOf(searchText.toLowerCase()) > -1) && (rec.Country_id === country.id));
            });
        } else if (category === 3) {
            sourceData = self.segLocationCity;
            var state = self.view.typeHeadState.tbxSearchKey.info.data;
            dataToAssign = sourceData.filter(function(rec) {
                var name = (rec.lblAddress.text).toLowerCase();
                return ((name.indexOf(searchText.toLowerCase()) > -1) && (rec.Region_id === state.id));
            });
        }
        if (searchText === "") dataToAssign = [];
        segPath.setData(dataToAssign);
        if (dataToAssign.length > 0) {
            segPath.setVisibility(true);
            noResultFlex.setVisibility(false);
            if (noResultFlex === this.view.typeHeadCountry.flxNoResultFound) {
                this.view.flxCountry.zIndex = 2;
            } else {
                this.view.flxCountry.zIndex = 1;
            }
        } else {
            segPath.setVisibility(false);
            noResultFlex.setVisibility(true);
            if (noResultFlex === this.view.typeHeadCountry.flxNoResultFound) {
                this.view.flxCountry.zIndex = 2;
            } else {
                this.view.flxCountry.zIndex = 1;
            }
        }
        self.view.forceLayout();
    },
    /*
     * displays required main flex(search,details,create) and hides other
     * @param: path of flex to show
     */
    hideRequiredMainScreens: function(reqFlexPath) {
        var self = this;
        self.view.flxSearchCompanies.setVisibility(false);
        self.view.flxCreateCompany.setVisibility(false);
        self.view.flxCompanyDetails.setVisibility(false);
        reqFlexPath.setVisibility(true);
        self.view.forceLayout();
    },
    /*
     * validate owner detail screen fields
     */
    validateOwnerDetailsScreen: function() {
        var self = this;
        var isValid = true;
        //firstname
        if (self.view.textBoxOwnerDetailsEntry11.tbxEnterValue.text.trim() === "") {
            self.view.textBoxOwnerDetailsEntry11.flxEnterValue.skin = "sknflxEnterValueError";
            self.view.textBoxOwnerDetailsEntry11.flxInlineError.setVisibility(true);
            self.view.textBoxOwnerDetailsEntry11.lblErrorText.text = kony.i18n.getLocalizedString("i18n.Common.frmGuestDashboard.FirstNameMissing");
            isValid = false;
        }
        //lastname
        if (self.view.textBoxOwnerDetailsEntry13.tbxEnterValue.text.trim() === "") {
            self.view.textBoxOwnerDetailsEntry13.flxEnterValue.skin = "sknflxEnterValueError";
            self.view.textBoxOwnerDetailsEntry13.flxInlineError.setVisibility(true);
            self.view.textBoxOwnerDetailsEntry13.lblErrorText.text = kony.i18n.getLocalizedString("i18n.Common.frmGuestDashboard.LastNameMissing");
            isValid = false;
        }
        //email-id
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (self.view.textBoxOwnerDetailsEntry23.tbxEnterValue.text.trim() === "") {
            self.view.textBoxOwnerDetailsEntry23.flxEnterValue.skin = "sknflxEnterValueError";
            self.view.textBoxOwnerDetailsEntry23.flxInlineError.setVisibility(true);
            self.view.textBoxOwnerDetailsEntry23.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EmailId_cannot_be_empty");
            isValid = false;
        } else if (emailRegex.test(self.view.textBoxOwnerDetailsEntry23.tbxEnterValue.text.trim()) === false) {
            self.view.textBoxOwnerDetailsEntry23.flxEnterValue.skin = "sknflxEnterValueError";
            self.view.textBoxOwnerDetailsEntry23.flxInlineError.setVisibility(true);
            self.view.textBoxOwnerDetailsEntry23.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EmailID_not_valid");
            isValid = false;
        }
        //contact num
        var phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
        if (!self.view.textBoxOwnerDetailsEntry31.txtContactNumber.text || !self.view.textBoxOwnerDetailsEntry31.txtContactNumber.text.trim()) {
            self.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skinredbg";
            self.view.textBoxOwnerDetailsEntry31.flxError.left = "80dp";
            self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(true);
            self.view.textBoxOwnerDetailsEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_cannot_be_empty");
            isValid = false;
        } else if (self.view.textBoxOwnerDetailsEntry31.txtContactNumber.text.trim().length > 15) {
            self.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skinredbg";
            self.view.textBoxOwnerDetailsEntry31.flxError.left = "80dp";
            self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(true);
            self.view.textBoxOwnerDetailsEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_cannot_exceed");
            isValid = false;
        } else if (phoneRegex.test(self.view.textBoxOwnerDetailsEntry31.txtContactNumber.text) === false) {
            self.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skinredbg";
            self.view.textBoxOwnerDetailsEntry31.flxError.left = "80dp";
            self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(true);
            self.view.textBoxOwnerDetailsEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_not_valid");
            isValid = false;
        }
        //ISD code
        var ISDRegex = /^\+(\d{1,3}|\d{1,3})$/;
        if (!self.view.textBoxOwnerDetailsEntry31.txtISDCode.text || !self.view.textBoxOwnerDetailsEntry31.txtISDCode.text.trim() || (self.view.textBoxOwnerDetailsEntry31.txtISDCode.text === "+")) {
            self.view.textBoxOwnerDetailsEntry31.txtISDCode.skin = "skinredbg";
            self.view.textBoxOwnerDetailsEntry31.flxError.left = "0dp";
            self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(true);
            self.view.textBoxOwnerDetailsEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
            isValid = false;
        } else if (self.view.textBoxOwnerDetailsEntry31.txtISDCode.text.trim().length > 4) {
            self.view.textBoxOwnerDetailsEntry31.txtISDCode.skin = "skinredbg";
            self.view.textBoxOwnerDetailsEntry31.flxError.left = "0dp";
            self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(true);
            self.view.textBoxOwnerDetailsEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
            isValid = false;
        } else if (ISDRegex.test(self.view.textBoxOwnerDetailsEntry31.txtISDCode.text) === false) {
            self.view.textBoxOwnerDetailsEntry31.txtISDCode.skin = "skinredbg";
            self.view.textBoxOwnerDetailsEntry31.flxError.left = "0dp";
            self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(true);
            self.view.textBoxOwnerDetailsEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
            isValid = false;
        }
        //DOB
        if (self.view.customCalOwnerDOB.value === "") {
            self.view.flxCalendarDOB.skin = "sknFlxCalendarError";
            self.view.textBoxOwnerDetailsEntry21.flxInlineError.setVisibility(true);
            self.view.textBoxOwnerDetailsEntry21.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.DOB_cannot_be_empty");
            isValid = false;
        }
        //SSN
        if (self.view.textBoxOwnerDetailsEntry22.tbxEnterValue.text.trim() === "") {
            self.view.textBoxOwnerDetailsEntry22.flxEnterValue.skin = "sknflxEnterValueError";
            self.view.textBoxOwnerDetailsEntry22.flxInlineError.setVisibility(true);
            self.view.textBoxOwnerDetailsEntry22.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.SSN_cannot_be_empty");
            isValid = false;
        }
        self.view.forceLayout();
        return isValid;
    },
    /*
     * clears all fields in create company owner details
     */
    clearDataForOwnerDetails: function() {
        var self = this;
        self.clearValidationsForOwnerDetails();
        self.view.textBoxOwnerDetailsEntry11.tbxEnterValue.text = "";
        self.view.textBoxOwnerDetailsEntry12.tbxEnterValue.text = "";
        self.view.textBoxOwnerDetailsEntry13.tbxEnterValue.text = "";
        self.view.textBoxOwnerDetailsEntry21.tbxEnterValue.text = "";
        self.view.textBoxOwnerDetailsEntry22.tbxEnterValue.text = "";
        self.view.textBoxOwnerDetailsEntry23.tbxEnterValue.text = "";
        self.view.textBoxOwnerDetailsEntry31.txtContactNumber.text = "";
        self.view.textBoxOwnerDetailsEntry31.txtISDCode.text = "";
        self.view.customCalOwnerDOB.resetData = kony.i18n.getLocalizedString("i18n.frmLogsController.Select_Date");
        self.view.forceLayout();
    },
    /*
     * clears the inline error for owner details in create company
     */
    clearValidationsForOwnerDetails: function(txtBoxPath, errFlexPath) {
        var self = this;
        if (txtBoxPath) {
            txtBoxPath.skin = "skntbxLato35475f14px";
            if (errFlexPath) errFlexPath.setVisibility(false);
        } else {
            self.view.textBoxOwnerDetailsEntry11.flxEnterValue.skin = "sknflxEnterValueNormal";
            self.view.textBoxOwnerDetailsEntry13.flxEnterValue.skin = "sknflxEnterValueNormal";
            self.view.flxCalendarDOB.skin = "sknflxEnterValueNormal";
            self.view.textBoxOwnerDetailsEntry22.flxEnterValue.skin = "sknflxEnterValueNormal";
            self.view.textBoxOwnerDetailsEntry23.flxEnterValue.skin = "sknflxEnterValueNormal";
            self.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skntbxLato35475f14px";
            self.view.textBoxOwnerDetailsEntry31.txtISDCode.skin = "skntbxLato35475f14px";
            self.view.textBoxOwnerDetailsEntry11.flxInlineError.setVisibility(false);
            self.view.textBoxOwnerDetailsEntry13.flxInlineError.setVisibility(false);
            self.view.textBoxOwnerDetailsEntry21.flxInlineError.setVisibility(false);
            self.view.textBoxOwnerDetailsEntry22.flxInlineError.setVisibility(false);
            self.view.textBoxOwnerDetailsEntry23.flxInlineError.setVisibility(false);
            self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(false);
        }
        self.view.forceLayout();
    },
    /*
     * display the selected type for search
     */
    dispalySelectedType: function() {
        var self = this;
        var selType = "",
            curType = "",
            ind = "";
        var selRowData = self.view.addAndRemoveAccounts.segSearchType.selectedRowItems[0];
        ind = self.view.addAndRemoveAccounts.segSearchType.selectedRowIndex[1];
        selType = selRowData.lblValue;
        curType = self.view.addAndRemoveAccounts.lblSelSearchType.text;
        selRowData.lblValue = curType;
        self.view.addAndRemoveAccounts.segSearchType.setDataAt(selRowData, ind);
        self.view.addAndRemoveAccounts.lblSelSearchType.text = selType;
        self.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(false);
        self.view.forceLayout();
    },
    /*
     * show the changes in accounts screen based on type
     */
    adjustAccountsUIBasedOnType: function(type) {
        var self = this;
        self.view.flxHeading.setVisibility(false);
        self.view.flxAccountsAddAndRemove.top = "0dp";
        self.view.addAndRemoveAccounts.flxFilteredSearch.setVisibility(true);
        self.view.addAndRemoveAccounts.flxSearchContainer.setVisibility(false);
        self.view.commonButtonsAccounts.btnNext.left = "0px";
        self.view.forceLayout();
    },
    /*
     *resets search type list box
     */
    resetSearchTypeList: function() {
        var self = this;
        self.view.lblSelSearchType.text = "Membership Id";
        var data = [{
            "lblValue": "Account Id"
        }, {
            "lblValue": "Tax Number"
        }];
        self.view.segSearchType.setData(data);
        self.view.forceLayout();
    },
    /*
     * returns company details information that user entered in create company
     */
    getCreateCompanyDetails: function() {
        var companyDetails = {};
        companyDetails.Type = this.view.radioCompanyType.selectedKey === "TYPE_SMALL_BUSINESS" ? "Small Business" : "Micro Business";
        companyDetails.Name = this.view.tbxNameValue.text;
        companyDetails.Description = "";
        companyDetails.Communication = [{
            "Phone": this.view.contactNumber.txtISDCode.text + "-" + this.view.contactNumber.txtContactNumber.text,
            "Email": this.view.tbxEmailValue.text
        }];
        companyDetails.Address = [{
            "country": this.view.typeHeadCountry.tbxSearchKey.text,
            "cityName": this.view.typeHeadCity.tbxSearchKey.text,
            "state": this.view.typeHeadState.tbxSearchKey.text,
            "zipCode": this.view.tbxZipCode.text,
            "addressLine1": this.view.tbxStreetName.text,
            "addressLine2": this.view.tbxBuildingName.text
        }];
        companyDetails.Membership = [{
            "Taxid": this.view.textBoxEntryTin.tbxEnterValue.text
        }];
        return companyDetails;
    },
    /*
     * returns owner details information that user entered in create company
     */
    getCreateCompanyOwnerDetails: function() {
        if (this.view.radioCompanyType.selectedKey === "TYPE_SMALL_BUSINESS") {
            return {
                "Owner": []
            };
        }
        var ownerDetails = {};
        ownerDetails.firstName = this.view.textBoxOwnerDetailsEntry11.tbxEnterValue.text;
        ownerDetails.middleName = this.view.textBoxOwnerDetailsEntry12.tbxEnterValue.text;
        ownerDetails.lastName = this.view.textBoxOwnerDetailsEntry13.tbxEnterValue.text;
        ownerDetails.dob = this.getDateFormatYYYYMMDD(this.view.customCalOwnerDOB.value);
        ownerDetails.ssn = this.view.textBoxOwnerDetailsEntry22.tbxEnterValue.text;
        ownerDetails.email = this.view.textBoxOwnerDetailsEntry23.tbxEnterValue.text;
        ownerDetails.contactNumber = this.view.textBoxOwnerDetailsEntry31.txtContactNumber.text;
        ownerDetails.isoCode = this.view.textBoxOwnerDetailsEntry31.txtISDCode.text;
        return {
            "Owner": [{
                "FirstName": ownerDetails.firstName,
                "MidleName": ownerDetails.middleName,
                "LastName": ownerDetails.lastName,
                "DOB": ownerDetails.dob,
                "EmailId": ownerDetails.email,
                "Ssn": ownerDetails.ssn,
                "PhoneNumber": ownerDetails.isoCode + "-" + ownerDetails.contactNumber,
                "IdType": "",
                "IdValue": ""
            }]
        };
    },
    /*
     * returns selected accounts information that user added in create company
     */
    getCreateCompanySelectedAccounts: function() {
        var selectedAccounts = this.view.addAndRemoveAccounts.segSelectedOptions.data || [];
        selectedAccounts = selectedAccounts.map(function(account) {
            return {
                "Account_id": account.Account_id
            };
        });
        return {
            "AccountsList": selectedAccounts
        };
    },
    /*
     * creates company with details provided by the user in create company module
     */
    requestCreateUpdateComapny: function() {
        const companyDetails = this.getCreateCompanyDetails();
        const ownerDetails = this.getCreateCompanyOwnerDetails();
        const selectedAccounts = this.getCreateCompanySelectedAccounts();
        var payLoad = {};
        payLoad = Object.assign({}, companyDetails, ownerDetails, selectedAccounts);
        kony.print("Create Company Payload:" + JSON.stringify(payLoad));
        if (payLoad.Type === "Small Business") {
            delete payLoad.Owner;
        }
        if (this.action === this.actionConfig.create) {
            this.presenter.createCompany(payLoad);
        } else {
            payLoad.id = this.completeCompanyDetails.CompanyContext[0].id;
            this.presenter.editCompany(payLoad);
        }
    },
    createCompaySuccess: function(context) {
        this.presenter.showToastMessage(kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"), "Company Created Successfully");
        this.showCreatedCompanyDetails(context.id, 1);
    },
    createCompanyFailed: function() {},
    /*
     * clears all fields in create company details
     */
    clearDataForCreateCompanyDetails: function() {
        this.view.radioCompanyType.selectedKey = "TYPE_MICRO_BUSINESS";
        this.view.tbxNameValue.text = "";
        this.view.tbxEmailValue.text = "";
        this.view.contactNumber.txtContactNumber.text = "";
        this.view.contactNumber.txtISDCode.text = "";
        this.view.tbxMBMembershipIdValue.text = "";
        this.view.textBoxEntryTin.tbxEnterValue.text = "";
        this.view.tbxStreetName.text = "";
        this.view.tbxBuildingName.text = "";
        this.view.typeHeadCountry.tbxSearchKey.text = "";
        this.view.tbxZipCode.text = "";
        this.view.typeHeadCity.tbxSearchKey.text = "";
        this.view.typeHeadState.tbxSearchKey.text = "";
        this.view.typeHeadCountry.tbxSearchKey.info.isValid = false;
        this.view.typeHeadCity.tbxSearchKey.info.isValid = false;
        this.view.typeHeadState.tbxSearchKey.info.isValid = false;
        this.view.addAndRemoveAccounts.tbxFilterSearch.text = "";
        this.view.addAndRemoveAccounts.tbxSearchBox.text = "";
        this.view.lblTinCheck.setVisibility(false);
        this.view.lblTINErrorIcon.setVisibility(false);
        this.clearValidation(this.view.tbxNameValue, this.view.flxNoNameError, 1);
        this.clearValidation(this.view.tbxEmailValue, this.view.flxNoEmailError, 1);
        this.clearValidation(this.view.contactNumber.txtContactNumber, this.view.contactNumber.flxError, 1);
        this.clearValidation(this.view.contactNumber.txtISDCode, this.view.contactNumber.flxError, 1);
        this.clearValidation(this.view.tbxMBMembershipIdValue, this.view.flxMBMembershipIdError, 1);
        this.clearValidation(this.view.textBoxEntryTin.tbxEnterValue, this.view.textBoxEntryTin.flxInlineError, 1);
        this.clearAllAddressFields();
        this.view.forceLayout();
    },
    /*
     *check  tin validation
     */
    validateTin: function(callback) {
        var scopeObj = this;
        var tin = scopeObj.view.textBoxEntryTin.tbxEnterValue.text;
        if (tin) {
            var payLoad = {
                "Tin": tin
            };
            scopeObj.presenter.validateTIN(payLoad, callback);
        }
    },
    /*
     * update tin validation status based on service response
     */
    updateTINValidationStatus: function(response) {
        if (response.status === "success") {
            if (response.hasOwnProperty("isTINExists")) {
                if (response.isTINExists == "true") {
                    this.view.lblTinCheck.text = kony.i18n.getLocalizedString("i18n.frmCompanies.tin_valid_success");
                    this.view.lblTinCheck.skin = "sknlblLato5bc06cBold14px";
                    this.view.lblTinCheck.setVisibility(true);
                    this.view.lblTinCheck.isValidTin = true;
                    this.view.lblTinCheck.left = "0dp";
                    this.view.lblTINErrorIcon.setVisibility(false);
                } else {
                    this.view.lblTinCheck.text = kony.i18n.getLocalizedString("i18n.frmCompanies.tin_validation_failed");
                    this.view.lblTinCheck.skin = "sknlblError";
                    this.view.lblTinCheck.setVisibility(true);
                    this.view.lblTinCheck.isValidTin = false;
                    this.view.lblTinCheck.left = "16dp";
                    this.view.lblTINErrorIcon.setVisibility(true);
                }
            } else {
                this.view.lblTinCheck.text = response.msg;
                this.view.lblTinCheck.skin = "sknlblLato5bc06cBold14px";
                this.view.lblTinCheck.setVisibility(true);
                this.view.lblTinCheck.isValidTin = true;
                this.view.lblTinCheck.left = "0dp";
                this.view.lblTINErrorIcon.setVisibility(false);
            }
        } else {
            this.view.lblTinCheck.text = response.msg; //kony.i18n.getLocalizedString("i18n.frmCompanies.tin_validation_failed");
            this.view.lblTinCheck.skin = "sknlblError";
            this.view.lblTinCheck.setVisibility(true);
            this.view.lblTinCheck.isValidTin = false;
            this.view.lblTinCheck.left = "16dp";
            this.view.lblTINErrorIcon.setVisibility(true);
        }
        this.view.lblTinCheck.isTinValidated = true;
        this.view.forceLayout();
    },
    setDatatoSearchSegment: function(searchResult) {
        var self = this;
        var segData = self.mapResultsData(searchResult);
        if (searchResult !== null && searchResult.length > 0) {
            var enteredText = self.returnEnteredText(this.view.tbxName.text, this.view.tbxEmailId.text);
            self.view.flxHeaderPermissions.setVisibility(true);
            self.view.segSearchResults.setVisibility(true);
            self.view.lblNoResults.setVisibility(false);
            self.view.flxNoResultsFound.setVisibility(false);
            self.view.flxSearchResultsFor.setVisibility(true);
            self.view.lblEnteredText.text = enteredText;
            self.view.segSearchResults.setVisibility((segData.length > 0));
            if (segData.length > 0) {
                if (segData.length === 1) {
                    var id = segData[0].Organisation_id;
                    self.view.segSearchResults.setData(segData);
                    self.showCreatedCompanyDetails(id, 1);
                } else {
                    self.view.segSearchResults.setData(segData);
                }
            }
            self.sortBy = self.getObjectSorter("lblCompanyName");
            self.resetSortImages("searchList");
            var sortedData = segData.sort(self.sortBy.sortData);
            self.view.segSearchResults.setData(sortedData);
            self.view.segSearchResults.height = (self.view.flxSearchResults.frame.height - 130) + "px";
        } else {
            self.view.flxNoResultsFound.setVisibility(true);
            self.view.lblNoResults.text = kony.i18n.getLocalizedString("i18n.frmCompanies.searchNoResultFoundCompanies");
            self.view.lblNoResults.setVisibility(true);
            self.view.lblCreateCompanyLink.setVisibility(true);
        }
        self.view.forceLayout();
    },
    /*
     * displays company creation page with company details
     */
    showEditCompany: function(companyCompleteDetails) {
        var scopeObj = this;
        this.hideRequiredMainScreens(scopeObj.view.flxCreateCompany);
        this.view.flxBreadcrumb.setVisibility(true);
        this.view.addAndRemoveAccounts.segSelectedOptions.info = {
            "segData": []
        };
        this.showCompanyDetailsScreen();
        this.view.mainHeader.btnAddNewOption.setVisibility(false);
        this.clearDataForOwnerDetails();
        this.clearDataForCreateCompanyDetails();
        /*Data population in company details create company*/
        this.view.radioCompanyType.selectedKey = companyCompleteDetails.CompanyType === "TYPE_ID_SMALL_BUSINESS" ? "TYPE_SMALL_BUSINESS" : "TYPE_MICRO_BUSINESS";
        this.changeSidebarOptionsBasedOnCompanyType(this.view.radioCompanyType.selectedKey);
        var companyDetails = companyCompleteDetails.CompanyContext[0];
        if (companyDetails) {
            this.view.tbxNameValue.text = companyDetails.Name;
            this.view.breadcrumbs.btnPreviousPage.text = companyDetails.Name;
            this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmCompanies.SearchCompanies"); //companyDetails.Name.toUpperCase();
            this.view.tbxEmailValue.text = companyDetails.Email;
            var phoneNumber = companyDetails.Phone ? companyDetails.Phone.split("-") : ["", ""];
            this.view.contactNumber.txtContactNumber.text = phoneNumber.length >= 2 ? phoneNumber[1] : phoneNumber[0];
            this.view.contactNumber.txtISDCode.text = phoneNumber.length >= 2 ? phoneNumber[0] : "";
            this.view.tbxMBMembershipIdValue.text = companyDetails.MembershipId;
            this.view.textBoxEntryTin.tbxEnterValue.text = companyDetails.Tin;
            this.view.tbxStreetName.text = companyDetails.addressLine1;
            this.view.tbxBuildingName.text = companyDetails.addressLine2;
            this.view.typeHeadCountry.tbxSearchKey.text = companyDetails.country;
            this.view.tbxZipCode.text = companyDetails.zipCode;
            this.view.typeHeadCity.tbxSearchKey.text = companyDetails.cityName;
            this.view.typeHeadState.tbxSearchKey.text = companyDetails.state;
            this.view.typeHeadCity.tbxSearchKey.info.isValid = true;
            this.view.typeHeadCountry.tbxSearchKey.info.isValid = true;
            this.view.typeHeadState.tbxSearchKey.info.isValid = true;
        }
        /****************************************************/
        /*Data population in owner details create company*/
        var ownerDetails = companyCompleteDetails.OwnerContext[0];
        if (ownerDetails) {
            this.view.textBoxOwnerDetailsEntry11.tbxEnterValue.text = ownerDetails.FirstName;
            this.view.textBoxOwnerDetailsEntry12.tbxEnterValue.text = ownerDetails.MiddleName;
            this.view.textBoxOwnerDetailsEntry13.tbxEnterValue.text = ownerDetails.LastName;
            this.view.textBoxOwnerDetailsEntry21.tbxEnterValue.text = ownerDetails.DOB;
            this.view.textBoxOwnerDetailsEntry22.tbxEnterValue.text = ownerDetails.Ssn;
            this.view.textBoxOwnerDetailsEntry23.tbxEnterValue.text = ownerDetails.Email;
            var phoneNumber = ownerDetails.Phone ? ownerDetails.Phone.split("-") : ["", ""];
            this.view.textBoxOwnerDetailsEntry31.txtContactNumber.text = phoneNumber.length >= 2 ? phoneNumber[1] : phoneNumber[0];
            this.view.textBoxOwnerDetailsEntry31.txtISDCode.text = phoneNumber.length >= 2 ? phoneNumber[0] : "";
            this.view.customCalOwnerDOB.value = this.getDateFormatMMDDYYYY(ownerDetails.DOB, "/");
            this.view.customCalOwnerDOB.resetData = this.getLocaleDate(ownerDetails.DOB);
        }
        /*********************************************************/
        /*Data population in accounts create company*/
        var accounts = companyCompleteDetails.accountContext ? companyCompleteDetails.accountContext : [];
        this.mapCompanyAccountsToEditCompany(accounts);
        /*********************************************************/
    },
    /*
     * map accounts to create company accounts module right section 
     */
    mapCompanyAccountsToEditCompany: function(accounts) {
        var self = this;
        var type = self.typeConfig.smallBusiness;
        if (self.view.flxHeading.isVisible) type = self.typeConfig.microBusiness;
        else type = self.typeConfig.smallBusiness;
        var widgetMap = {
            "flxClose": "flxClose",
            "fontIconClose": "fontIconClose",
            "lblOption": "lblOption",
            "Account_id": "Account_id",
            "Membership_id": "Membership_id",
            "Taxid": "Taxid",
            "Account_Type": "Account_Type",
            "AccountName": "AccountName",
            "holder": "holder",
            "flxOptionAdded": "flxOptionAdded"
        };
        accounts = this.mapAvailableSegmentData(accounts);
        accounts = accounts.map(function(account) {
            return {
                "flxClose": {
                    "onClick": function() {
                        self.removeSelectedAccount();
                    }
                },
                "fontIconClose": {
                    "text": "\ue929",
                    "tooltip": "Remove Account"
                },
                "lblOption": {
                    "text": "Account  " + self.AdminConsoleCommonUtils.getTruncatedString(account.lblAccountNum.text, 15, 12),
                    "tooltip": account.lblAccountNum.text
                },
                "Account_id": account.lblAccountNum.text,
                "Membership_id": (type === self.typeConfig.smallBusiness) ? account.lblAccFieldValue2.info.value : self.view.lblMembershipValue.text,
                "Taxid": (type === self.typeConfig.smallBusiness) ? account.lblAccFieldValue3.info.value : self.view.lblTinValue,
                "AccountName": (type === self.typeConfig.smallBusiness) ? "" : account.lblAccFieldValue2.info.value,
                "holder": (type === self.typeConfig.smallBusiness) ? "" : account.lblAccFieldValue3.info.value,
                "Account_Type": account.lblAccFieldValue1.text,
                "template": "flxOptionAdded"
            };
        });
        self.assignedAccount = Array.from(accounts);
        self.view.addAndRemoveAccounts.segSelectedOptions.widgetDataMap = widgetMap;
        self.view.addAndRemoveAccounts.segSelectedOptions.setData(accounts, 0);
        self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = accounts;
        self.view.forceLayout();
    },
    changeSidebarOptionsBasedOnCompanyType: function(type) {
        var scopeObj = this;
        scopeObj.view.textBoxEntryTin.flxBtnCheck.isVisible = false;
        if (type === "TYPE_MICRO_BUSINESS") {
            scopeObj.view.flxCompanyDetailsRow3.isVisible = true;
            scopeObj.view.flxMBTin.isVisible = true;
            scopeObj.view.verticalTabsCompany.flxOption3.isVisible = true;
            scopeObj.view.commonButtonsAccounts.btnNext.isVisible = true;
            scopeObj.view.commonButtonsAccounts.btnSave.isVisible = false;
        } else {
            scopeObj.view.flxCompanyDetailsRow3.isVisible = true;
            scopeObj.view.flxMBTin.isVisible = false;
            scopeObj.view.verticalTabsCompany.flxOption3.isVisible = false;
            scopeObj.view.commonButtonsAccounts.btnNext.isVisible = false;
            scopeObj.view.commonButtonsAccounts.btnSave.isVisible = true;
        }
    },
    editCompanySuccess: function(context) {
        this.presenter.showToastMessage(kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"), "Company Details Updated Successfully");
        this.showCreatedCompanyDetails(context.id, 1);
    },
    changeUIAccessBasedOnEditCreateCompanyFlow: function() {
        if (this.action === this.actionConfig.create) {
            this.view.flxCompanyType.setEnabled(true);
            this.view.flxMBMembershipId.setEnabled(true);
            this.view.flxMBTin.setEnabled(true);
            this.view.flxOwnerDetailsRow1.setEnabled(true);
            this.view.flxOwnerDetailsRow2.setEnabled(true);
            this.view.textBoxOwnerDetailsEntry31.txtContactNumber.setEnabled(true);
            this.view.textBoxOwnerDetailsEntry31.txtISDCode.setEnabled(true);
            this.view.customCalOwnerDOB.setEnabled(true);
            this.view.flxDOBDisabled.setVisibility(false);
            this.view.typeHeadState.tbxSearchKey.setEnabled(false);
            this.view.typeHeadCity.tbxSearchKey.setEnabled(false);
            this.view.textBoxEntryTin.tbxEnterValue.skin = "skntbxEnterValue";
            this.view.tbxMBMembershipIdValue.skin = "skntbxLato35475f14px";
            this.view.textBoxOwnerDetailsEntry11.tbxEnterValue.skin = "skntbxEnterValue";
            this.view.textBoxOwnerDetailsEntry12.tbxEnterValue.skin = "skntbxEnterValue";
            this.view.textBoxOwnerDetailsEntry13.tbxEnterValue.skin = "skntbxEnterValue";
            this.view.textBoxOwnerDetailsEntry21.tbxEnterValue.skin = "skntbxEnterValue";
            this.view.textBoxOwnerDetailsEntry22.tbxEnterValue.skin = "skntbxEnterValue";
            this.view.textBoxOwnerDetailsEntry23.tbxEnterValue.skin = "skntbxEnterValue";
            this.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skntbxEnterValue";
            this.view.textBoxOwnerDetailsEntry31.txtISDCode.text = "";
            this.changeSidebarOptionsBasedOnCompanyType("TYPE_MICRO_BUSINESS");
        } else {
            this.view.flxCompanyType.setEnabled(false);
            this.view.flxMBMembershipId.setEnabled(false);
            this.view.flxMBTin.setEnabled(false);
            this.view.flxOwnerDetailsRow1.setEnabled(false);
            this.view.flxOwnerDetailsRow2.setEnabled(false);
            this.view.textBoxOwnerDetailsEntry31.txtContactNumber.setEnabled(false);
            this.view.textBoxOwnerDetailsEntry31.txtISDCode.setEnabled(false);
            this.view.customCalOwnerDOB.setEnabled(false);
            this.view.flxDOBDisabled.setVisibility(true);
            this.view.flxDOBDisabled.onClick = function() {};
            this.view.textBoxEntryTin.tbxEnterValue.skin = "skntbxblockedf2f2f2";
            this.view.tbxMBMembershipIdValue.skin = "skntbxblockedf2f2f2";
            this.view.textBoxOwnerDetailsEntry11.tbxEnterValue.skin = "skntbxblockedf2f2f2";
            this.view.textBoxOwnerDetailsEntry12.tbxEnterValue.skin = "skntbxblockedf2f2f2";
            this.view.textBoxOwnerDetailsEntry13.tbxEnterValue.skin = "skntbxblockedf2f2f2";
            this.view.textBoxOwnerDetailsEntry21.tbxEnterValue.skin = "skntbxblockedf2f2f2";
            this.view.textBoxOwnerDetailsEntry22.tbxEnterValue.skin = "skntbxblockedf2f2f2";
            this.view.textBoxOwnerDetailsEntry23.tbxEnterValue.skin = "skntbxblockedf2f2f2";
            this.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skntbxblockedf2f2f2";
            if (this.view.textBoxOwnerDetailsEntry31.txtISDCode.text != "") {
                this.view.textBoxOwnerDetailsEntry31.txtISDCode.skin = "skntbxblockedf2f2f2";
                this.view.textBoxOwnerDetailsEntry31.txtISDCode.setEnabled(false);
            } else {
                this.view.textBoxOwnerDetailsEntry31.txtISDCode.skin = "skntbxLato35475f14px";
                this.view.textBoxOwnerDetailsEntry31.txtISDCode.setEnabled(true);
            }
            this.view.breadcrumbs.lblCurrentScreen.text = this.completeCompanyDetails.CompanyContext ? (this.completeCompanyDetails.CompanyContext[0].Name).toUpperCase() : "COMPANY"; //kony.i18n.getLocalizedString("i18n.frmCompanies.EditCompany");
        }
    },
    showAccountSearchLoading: function() {},
    hideAccountSearchLoading: function() {},
    getTransactionDateCustom: function(Num, target) {
        var date = this.getCustomDate(Num, target);
        return this.getTransactionDateForServiceCall(date);
    },
    setDataForProductTransactionSegment: function(target) {
        var dataMap = {
            "flxCustMangRequestHeader": "flxCustMangRequestHeader",
            "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
            "flxFirstColoum": "flxFirstColoum",
            "lblAmountOriginalSign": "lblAmountOriginalSign",
            "lblAmountOriginalSymbol": "lblAmountOriginalSymbol",
            "lblAmountOriginal": "lblAmountOriginal",
            "lblAmountConvertedSign": "lblAmountConvertedSign",
            "lblAmountConvertedSymbol": "lblAmountConvertedSymbol",
            "lblAmountConverted": "lblAmountConverted",
            "lblDateAndTime": "lblDateAndTime",
            "lblRefNo": "lblRefNo",
            "lblSeperator": "lblSeperator",
            "lblTransctionDescription": "lblTransctionDescription",
            "lblType": "lblType"
        };
        var toAdd;
        var data = [];
        if (target === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Successful") || target === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Pending")) {
            for (var i = 0; i < this.AccountTrasactions.length; i++) {
                var newDate = this.getFormattedDataTimeFromDBDateTime(this.AccountTrasactions[i].transactionDate.replace("T", " ").replace("Z", ""));
                if (this.AccountTrasactions[i].statusDescription && this.AccountTrasactions[i].statusDescription.toUpperCase() === target.toUpperCase()) {
                    toAdd = {
                        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                        "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
                        "flxFirstColoum": "flxFirstColoum",
                        "lblAmountOriginalSign": this.AccountTrasactions[i].amount && this.AccountTrasactions[i].amount.substring(0, 1) === "-" ? "-" : "",
                        "lblAmountOriginalSymbol": this.AccountTrasactions[i].amount ? this.defaultCurrencyCode(this.AccountTrasactions[i].baseCurrency) : "",
                        "lblAmountOriginal": this.AccountTrasactions[i].amount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].amount)) : "-",
                        "lblAmountConvertedSign": this.AccountTrasactions[i].convertedAmount && this.AccountTrasactions[i].convertedAmount.substring(0, 1) === "-" ? "-" : "",
                        "lblAmountConvertedSymbol": this.AccountTrasactions[i].convertedAmount ? this.defaultCurrencyCode(this.AccountTrasactions[i].transactionCurrency) : "",
                        "lblAmountConverted": this.AccountTrasactions[i].convertedAmount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].convertedAmount)) : "-",
                        "lblDateAndTime": this.getLocaleDateAndTime(newDate),
                        "lblRefNo": this.AccountTrasactions[i].transactionId,
                        "lblSeperator": ".",
                        "lblTransctionDescription": this.AccountTrasactions[i].description,
                        "lblType": this.AccountTrasactions[i].transactiontype,
                        "template": "flxCustMangTransactionHistoryCompanies"
                    };
                    data.push(toAdd);
                } else if (target === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Successful")) {
                    toAdd = {
                        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                        "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
                        "flxFirstColoum": "flxFirstColoum",
                        "lblAmountOriginalSign": this.AccountTrasactions[i].amount && this.AccountTrasactions[i].amount.substring(0, 1) === "-" ? "-" : "",
                        "lblAmountOriginalSymbol": this.AccountTrasactions[i].amount ? this.defaultCurrencyCode(this.AccountTrasactions[i].baseCurrency) : "",
                        "lblAmountOriginal": this.AccountTrasactions[i].amount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].amount)) : "-",
                        "lblAmountConvertedSign": this.AccountTrasactions[i].convertedAmount && this.AccountTrasactions[i].convertedAmount.substring(0, 1) === "-" ? "-" : "",
                        "lblAmountConvertedSymbol": this.AccountTrasactions[i].convertedAmount ? this.defaultCurrencyCode(this.AccountTrasactions[i].transactionCurrency) : "",
                        "lblAmountConverted": this.AccountTrasactions[i].convertedAmount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].convertedAmount)) : "-",
                        "lblDateAndTime": this.getLocaleDateAndTime(newDate),
                        "lblRefNo": this.AccountTrasactions[i].transactionId,
                        "lblSeperator": ".",
                        "lblTransctionDescription": this.AccountTrasactions[i].description,
                        "lblType": this.AccountTrasactions[i].fromAccountType,
                        "template": "flxCustMangTransactionHistoryCompanies"
                    };
                    data.push(toAdd);
                }
            }
        } else if (target === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Scheduled")) {
            for (var j = 0; j < this.AccountTrasactions.length; j++) {
                var newDate1 = this.getFormattedDataTimeFromDBDateTime(this.AccountTrasactions[j].transactionDate.replace("T", " ").replace("Z", ""));
                if (this.AccountTrasactions[j].isScheduled === "true") {
                    toAdd = {
                        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                        "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
                        "flxFirstColoum": "flxFirstColoum",
                        "lblAmountOriginalSign": this.AccountTrasactions[j].amount && this.AccountTrasactions[j].amount.substring(0, 1) === "-" ? "-" : "",
                        "lblAmountOriginalSymbol": this.AccountTrasactions[j].amount ? this.defaultCurrencyCode(this.AccountTrasactions[j].baseCurrency) : "",
                        "lblAmountOriginal": this.AccountTrasactions[j].amount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[j].amount)) : "-",
                        "lblAmountConvertedSign": this.AccountTrasactions[j].convertedAmount && this.AccountTrasactions[j].convertedAmount.substring(0, 1) === "-" ? "-" : "",
                        "lblAmountConvertedSymbol": this.AccountTrasactions[j].convertedAmount ? this.defaultCurrencyCode(this.AccountTrasactions[j].transactionCurrency) : "",
                        "lblAmountConverted": this.AccountTrasactions[j].convertedAmount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[j].convertedAmount)) : "-",
                        "lblDateAndTime": this.getLocaleDateAndTime(newDate1),
                        "lblRefNo": this.AccountTrasactions[j].transactionId,
                        "lblSeperator": ".",
                        "lblTransctionDescription": this.AccountTrasactions[j].description,
                        "lblType": this.AccountTrasactions[j].transactiontype,
                        "template": "flxCustMangTransactionHistoryCompanies"
                    };
                    data.push(toAdd);
                }
            }
        }
        this.view.transactionHistorySearch.tbxSearchBox.text = "";
        if (data.length > 0) {
            toAdd = {
                "flxCustMangRequestHeader": "flxCustMangRequestHeader",
                "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
                "flxFirstColoum": "flxFirstColoum",
                "lblAmountOriginalSign": "",
                "lblAmountOriginalSymbol": "",
                "lblAmountOriginal": "",
                "lblAmountConvertedSign": "",
                "lblAmountConvertedSymbol": "",
                "lblAmountConverted": "",
                "lblDateAndTime": "",
                "lblRefNo": "",
                "lblSeperator": ".",
                "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
                "lblType": "",
                "template": "flxCustMangTransactionHistoryCompanies"
            };
            this.view.segTransactionHistory.widgetDataMap = dataMap;
            this.view.segTransactionHistory.setData(data);
            this.view.segTransactionHistory.info = {
                "data": data,
                "searchAndSortData": data
            };
            this.view.segTransactionHistory.setVisibility(true);
            this.view.flxTransactionHistorySegmentHeader.setVisibility(true);
            this.view.flxSeperator4.setVisibility(true);
            this.view.rtxMsgTransctions.setVisibility(false);
            this.view.transactionHistorySearch.flxDownload.setVisibility(true);
        } else {
            this.view.segTransactionHistory.info = {
                "data": [],
                "searchAndSortData": []
            };
            this.view.transactionHistorySearch.flxDownload.setVisibility(false);
            this.view.segTransactionHistory.setVisibility(false);
            this.view.flxTransactionHistorySegmentHeader.setVisibility(false);
            this.view.flxSeperator4.setVisibility(false);
            this.view.rtxMsgTransctions.setVisibility(true);
        }
        this.showProductTransactionHistory();
    },
    showProductTransactionHistory: function() {
        this.view.flxProductHeaderAndDetails.setVisibility(true);
        this.view.flxProductInfoWrapper.setVisibility(true);
        this.view.flxProductDetails.setVisibility(true);
        this.view.forceLayout();
        var flxScroll = document.getElementById("frmCompanies_flxScrollTransctionsSegment");
        var scrollWidth = flxScroll.offsetWidth - flxScroll.clientWidth;
        this.view.flxTransactionHistorySegmentHeader.right = 35 + scrollWidth + "px";
        this.view.forceLayout();
    },
    getTransactionsBasedOnDate: function() {
        var scopeObj = this;
        var index = scopeObj.view.segCompanyDetailAccount.selectedRowIndex[1];
        var data = scopeObj.view.segCompanyDetailAccount.data;
        var acctNo = data[index].lblAccountNumber.text;
        var StartDate;
        var EndDate;
        var rangeType = scopeObj.view.customCalCreatedDate.value;
        if (rangeType !== "") {
            StartDate = rangeType.substring(0, rangeType.indexOf(" - "));
            var mm = StartDate.substr(0, 2);
            var dd = StartDate.substr(3, 2);
            var yyyy = StartDate.substr(6, 4);
            StartDate = yyyy + "-" + mm + "-" + dd + " 00:00:00";
            EndDate = rangeType.substring(rangeType.indexOf(" - ") + 3);
            var mm1 = EndDate.substr(0, 2);
            var dd1 = EndDate.substr(3, 2);
            var yyyy1 = EndDate.substr(6, 4);
            EndDate = yyyy1 + "-" + mm1 + "-" + dd1 + " 00:00:00";
        }
        kony.adminConsole.utils.showProgressBar(scopeObj.view);
        scopeObj.presenter.getAccountTransactions({
            "AccountNumber": acctNo,
            "StartDate": StartDate,
            "EndDate": EndDate
        });
    },
    setDataForProductDetailsScreen: function(CustomerAccounts) {
        var scopeObj = this;
        var customerDetails = CustomerAccounts[0];
        var status = customerDetails.StatusDesc || kony.i18n.getLocalizedString("i18n.frmCustomers.NA");
        //set data
        this.view.backToAccounts.btnBack.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ACCOUNTS_BTN_BACK");
        this.view.AccountsHeader.lblProductCardName.text = customerDetails.accountName;
        this.view.AccountsHeader.lblCardStatus.text = status;
        var StatusImg;
        //TODO: all images for status
        if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.secureimage.Active").toUpperCase()) {
            StatusImg = "sknFontIconActivate";
        } else if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomers.Suspended").toUpperCase()) {
            StatusImg = "sknFontIconSuspend";
        } else if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomers.Closed").toUpperCase()) {
            StatusImg = "sknfontIconInactive";
        } else if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomers.NA").toUpperCase()) {
            StatusImg = "sknfontIconInactive";
        }
        this.view.AccountsHeader.fontIconCardStatus.skin = StatusImg;
        if (customerDetails.IBAN) {
            this.view.AccountsHeader.lblIBAN.isVisible = true;
            this.view.AccountsHeader.lblIBANLabel.isVisible = true;
            this.view.AccountsHeader.lblIBAN.text = customerDetails.IBAN;
            this.view.AccountsHeader.lblIBAN.width = (8.75 * (customerDetails.IBAN).length) + "px";
            this.view.AccountsHeader.lblIBAN.right = "0px";
            this.view.AccountsHeader.lblIBANLabel.right = ((8.75 * (customerDetails.IBAN).length)) + "px";
        } else {
            this.view.AccountsHeader.lblIBAN.isVisible = false;
            this.view.AccountsHeader.lblIBANLabel.isVisible = false;
        }
        this.view.AccountsHeader.flxAccountDetailsColumn2.right = "2px";
        this.view.AccountsHeader.blAccountNumber.left = undefined;
        if (customerDetails.Account_id) {
            this.view.AccountsHeader.blAccountNumber.isVisible = true;
            this.view.AccountsHeader.lblAccountNumberLabel.isVisible = true;
            var cardOrAccountNumber = customerDetails.Account_id;
            this.view.AccountsHeader.blAccountNumber.text = cardOrAccountNumber;
            this.view.AccountsHeader.blAccountNumber.width = (8.75 * cardOrAccountNumber.length) + "px";
            if (customerDetails.IBAN) {
                var accountNumberRight = (8.75 * (customerDetails.IBAN).length) + 50;
                this.view.AccountsHeader.blAccountNumber.right = accountNumberRight + "px";
                this.view.AccountsHeader.lblAccountNumberLabel.right = (accountNumberRight + (8.75 * cardOrAccountNumber.length)) + "px";
            } else {
                this.view.AccountsHeader.blAccountNumber.right = "0px";
                this.view.AccountsHeader.lblAccountNumberLabel.right = ((8.75 * cardOrAccountNumber.length)) + "px";
            }
        } else {
            this.view.AccountsHeader.blAccountNumber.isVisible = false;
            this.view.AccountsHeader.lblAccountNumberLabel.isVisible = false;
        }
        //row1
        this.view.ProductRow1.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CREATED_ON");
        this.view.ProductRow1.lblData1.text = customerDetails.openingDate ? scopeObj.getLocaleDate(customerDetails.openingDate) : "N/A";
        this.view.ProductRow1.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CURRENT_BALANCE");
        this.view.ProductRow1.lblSignData2.left = "-7px";
        if (customerDetails.currentBalance) {
            this.view.ProductRow1.lblSignData2.text = customerDetails.currentBalance.substring(0, 1) === "-" ? "-" : "";
            this.view.ProductRow1.lblIconData2.text = this.defaultCurrencyCode(customerDetails.currencyCode);
            this.view.ProductRow1.lblData2.text = this.formatCurrencyByDeletingSign(this.getCurrencyFormat(customerDetails.currentBalance));
        } else {
            this.view.ProductRow1.lblData2.text = "N/A";
        }
        this.view.ProductRow1.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DIVIDEND_RATE");
        this.view.ProductRow1.lblData3.text = customerDetails.dividendRate ? customerDetails.dividendRate : "N/A";
        //row 2
        this.view.ProductRow2.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.OWNERSHIP");
        this.view.ProductRow2.lblData1.text = customerDetails.Ownership == "joint" ? kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.JOINT") : kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SINGLE");
        this.view.ProductRow2.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.AVAILABLE_BALANCE");
        this.view.ProductRow2.lblSignData2.left = "-7px";
        if (customerDetails.availableBalance) {
            this.view.ProductRow2.lblSignData2.text = customerDetails.availableBalance.substring(0, 1) === "-" ? "-" : "";
            this.view.ProductRow2.lblIconData2.text = this.defaultCurrencyCode(customerDetails.currencyCode);
            this.view.ProductRow2.lblData2.text = this.formatCurrencyByDeletingSign(this.getCurrencyFormat(customerDetails.availableBalance));
        } else {
            this.view.ProductRow2.lblData2.text = "N/A";
        }
        this.view.ProductRow2.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DIVIDEND_PAID_(YTD)");
        this.view.ProductRow2.lblData3.text = customerDetails.dividendPaidYTD ? customerDetails.dividendPaidYTD : "N/A";
        //row 3
        this.view.ProductRow3.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.JOINT_HOLDER_NAME");
        var accountHolder = customerDetails.accountHolder;
        if (accountHolder === null || accountHolder === undefined) {
            accountHolder = "N/A"
        } else if (typeof accountHolder === 'string') {
            try {
                accountHolder = JSON.parse(accountHolder);
            } catch (e) {
                accountHolder = "N/A";
                kony.print("Account Holder is not a valid json object " + e);
            }
        }
        var currentUsername = this.view.mainHeader.lblUserName;
        this.view.ProductRow3.lblData1.setVisibility(false);
        if (accountHolder !== "N/A") {
            if (currentUsername !== accountHolder.username) {
                this.view.ProductRow3.lblData1.setVisibility(true);
                this.view.ProductRow3.lblData1.text = accountHolder.fullname + " (Primary)";
            } else {
                this.view.ProductRow3.lblData1.setVisibility(true);
                this.view.ProductRow3.lblData1.text = this.view.generalInfoHeader.lblCustomerName.info.FirstName + " " + this.view.generalInfoHeader.lblCustomerName.info.LastName + " (Primary)";
            }
        } else {
            this.view.ProductRow3.lblData1.setVisibility(true);
            this.view.ProductRow3.lblData1.text = accountHolder;
        }
        this.view.ProductRow3.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ROUTING_NUMBER");
        this.view.ProductRow3.lblData2.text = customerDetails.routingNumber === undefined ? "N/A" : customerDetails.routingNumber;
        this.view.ProductRow3.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LAST_DIVIDEND_PAID");
        this.view.ProductRow3.lblData3.text = customerDetails.lastDividendPaidAmount ? customerDetails.lastDividendPaidAmount : "N/A";
        //row 4
        this.view.ProductRow4.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LAST_UPDATE_ON");
        this.view.ProductRow4.lblData1.text = customerDetails.lastPaymentDate ? this.getFormattedDataTimeFromDBDateTime(customerDetails.lastPaymentDate) : "N/A";
        this.view.ProductRow4.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SWIFT_CODE");
        this.view.ProductRow4.lblData2.text = customerDetails.swiftCode === undefined ? "N/A" : customerDetails.swiftCode;
        this.view.ProductRow4.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LAST_DIVIDEND_PAID_ON");
        this.view.ProductRow4.lblData3.text = customerDetails.dividendLastPaidDate ? scopeObj.getLocaleDate(customerDetails.dividendLastPaidDate) : "N/A";
        //row 5
        if (this.completeCompanyDetails.CompanyType === "TYPE_ID_SMALL_BUSINESS") {
            this.view.ProductRow5.setVisibility(true);
            this.view.ProductRow5.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCompanies.MEMBERSHIP_ID");
            this.view.ProductRow5.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCompanies.TAXNUMBER");
            this.view.ProductRow5.lblData1.text = customerDetails.Membership_id ? customerDetails.Membership_id : "N/A";
            this.view.ProductRow5.lblData2.text = customerDetails.Taxid ? customerDetails.Taxid : "N/A";
        } else {
            this.view.ProductRow5.setVisibility(false);
        }
        var eStatementStatus = customerDetails.eStatementEnable;
        if (eStatementStatus == "true") {
            this.view.imgRbEstatement.src = "radio_selected.png";
            this.view.imgRbPaper.src = "radio_notselected.png";
            this.view.forceLayout();
        } else {
            this.view.imgRbEstatement.src = "radio_notselected.png";
            this.view.imgRbPaper.src = "radio_selected.png";
        }
    },
    fetchAccountsForMicroBusinessBanking: function() {
        var scopeObj = this;
        var payload = {
            "Membership_id": scopeObj.view.lblMembershipValue.text,
            "Taxid": scopeObj.view.lblTinValue.text
        };
        scopeObj.showAccountSearchLoading();
        scopeObj.presenter.getAllAccounts(payload);
    },
    backToCompanyDetails: function(tabselection) {
        this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmCompanies.SearchCompanies");
        this.view.breadcrumbs.lblCurrentScreen.text = this.completeCompanyDetails.CompanyContext ? (this.completeCompanyDetails.CompanyContext[0].Name).toUpperCase() : "COMPANY";
        this.view.flxInlineError.setVisibility(false);
        this.hideRequiredMainScreens(this.view.flxCompanyDetails);
        this.view.flxBreadcrumb.setVisibility(true);
        this.view.mainHeader.btnAddNewOption.setVisibility(false);
        if (tabselection === this.tabsConfig.accounts) {
            this.view.flxViewTab1.onTouchEnd();
        } else {
            this.view.flxViewTab2.onTouchEnd();
        }
    },
    /*
     * onclick handler for create company details next
     */
    createCompanyCompanyDetailsNextClickHandler: function() {
        var scopeObj = this;
        var isValid = scopeObj.validatingCompanyDetails();
        scopeObj.hideAddressSegments();
        if (isValid) {
            scopeObj.showAddAccountsScreen();
        }
    },
    /*
     * Clears all the selected account in accounts tab.
     * 
     */
    clearCreateCompanySelectedAccounts: function() {
        this.assignedAccount = [];
        this.view.addAndRemoveAccounts.segSelectedOptions.setData([]);
        this.view.addAndRemoveAccounts.segSelectedOptions.info.segData = [];
        this.view.forceLayout();
    },
    /*
     * Track if there is any change in membership/tin values that previously entered and if there is 
     * any change clears all the previously selected accounts.
     */
    trackCreateCompanyMembershipIdAndTinChange: function() {
        var companyType = this.view.radioCompanyType.selectedKey;
        if (this.action == this.actionConfig.create) {
            if (this.view.radioCompanyType.selectedKey == "TYPE_MICRO_BUSINESS") {
                var membershipId = this.view.tbxMBMembershipIdValue.text;
                var tin = this.view.textBoxEntryTin.tbxEnterValue.text;
                if (this.microBusinessInputs.MembershipId != membershipId || this.microBusinessInputs.TIN != tin || this.microBusinessInputs.CompanyType != companyType) {
                    this.clearCreateCompanySelectedAccounts();
                    this.microBusinessInputs.MembershipId = membershipId;
                    this.microBusinessInputs.TIN = tin;
                    this.microBusinessInputs.CompanyType = companyType;
                }
            } else {
                if (this.microBusinessInputs.CompanyType != companyType) {
                    this.clearCreateCompanySelectedAccounts();
                    this.microBusinessInputs.CompanyType = companyType;
                }
            }
        }
    },
    sortIconFor: function(column, iconPath) {
        var self = this;
        self.determineSortFontIcon(this.sortBy, column, self.view[iconPath]);
    },
    resetSortImages: function(context) {
        var self = this;
        if (context === "searchList") {
            self.sortIconFor('lblCompanyName.info.value', 'fontIconSortName');
            self.sortIconFor('lblEmail', 'fontIconSortRoles');
        } else if (context === "accounts") {
            self.sortIconFor('lblProductType', 'lblAccountTypeSortIcon');
            self.sortIconFor('lblProductName.text', 'lblAccountNameSortIcon');
            self.sortIconFor('lblAccountNumber.text', 'lblAccountNumberSortIcon');
            self.sortIconFor('lblStatus.text', 'lblStatusSortIcon');
        } else if (context === "businessUsers") {
            self.sortIconFor('lblName.text', 'lblCustomerNameSortIcon');
            self.sortIconFor('lblRole', 'lblRoleSortIcon');
            self.sortIconFor('lblUsername', 'lblUserNameSortIcon');
            self.sortIconFor('lblEmail', 'lblCustomerEmailIDSortIocn');
        } else if (context === "transactions") {
            self.sortIconFor('lblRefNo', 'fonticonSortTranasctionRefNo');
            self.sortIconFor('lblDateAndTime', 'fonticonSortTransactionDateAndTime');
            self.sortIconFor('lblType', 'fonticonSortTransactionType');
            self.sortIconFor('lblAmountOriginal', 'fonticonSortTransactionAmountOriginal');
            self.sortIconFor('lblAmountConverted', 'fonticonSortTransactionAmountConverted');
        }
    },
    sortAndSetData: function(segData, sortColumn, context) {
        var self = this;
        self.sortBy.column(sortColumn);
        self.resetSortImages(context);
        return segData.sort(self.sortBy.sortData);
    },
    hideAllOptionsButtonImages: function() {
        this.view.verticalTabsCompany.flxImgArrow1.setVisibility(false);
        this.view.verticalTabsCompany.flxImgArrow2.setVisibility(false);
        this.view.verticalTabsCompany.flxImgArrow3.setVisibility(false);
        this.view.verticalTabsCompany.flxImgArrow4.setVisibility(false);
    },
    calWidgetFormatDate: function(dateString) {
        var yyyy = +dateString.substr(0, 4);
        var mm = +dateString.substr(5, 2);
        var dd = +dateString.substr(8, 2);
        if (dd < 10) {
            dd = "0" + dd;
        }
        if (mm < 10) {
            mm = "0" + mm;
        }
        return mm + "/" + dd + "/" + yyyy;
    },
    sortForTransactions: function(SegmentWidget, columnName, sortImgWidget, defaultScrollWidget, NumberofRecords, LoadMoreRow) {
        var data = SegmentWidget.info.searchAndSortData;
        var sortdata = this.sortAndSetData(data, columnName, "transactions");
        if (NumberofRecords === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ALL")) {
            SegmentWidget.info.searchAndSortData = sortdata;
            SegmentWidget.setData(sortdata);
        } else if (data.length > NumberofRecords) {
            SegmentWidget.info.searchAndSortData = sortdata;
            var newData = sortdata.slice(0, NumberofRecords);
            if (LoadMoreRow !== null) newData.push(LoadMoreRow);
            SegmentWidget.setData(newData);
        } else {
            SegmentWidget.info.searchAndSortData = sortdata;
            SegmentWidget.setData(sortdata);
        }
        this.sortIconFor(columnName, sortImgWidget);
        this.view.forceLayout();
    },
    hideAddressSegments: function(typeHeadPath) {
        this.view.typeHeadCity.segSearchResult.setVisibility(false);
        this.view.typeHeadCountry.segSearchResult.setVisibility(false);
        this.view.typeHeadState.segSearchResult.setVisibility(false);
        if (typeHeadPath) {
            typeHeadPath.segSearchResult.setVisibility(true);
        }
        this.view.forceLayout();
    },
    /*
     * function to get address codes of city,state,country populated
     */
    getAddressCodes: function(name) {
        var self = this;
        var country = self.segCountry;
        var state = self.segState;
        var city = self.segLocationCity;
        //country
        var r1 = country.filter(function(rec) {
            if (rec.lblAddress.text.indexOf(name[0]) >= 0) {
                return rec;
            }
        });
        self.view.typeHeadCountry.tbxSearchKey.info.data = r1[0] || {};
        //state
        var r2 = state.filter(function(rec) {
            if (rec.lblAddress.text.indexOf(name[1]) >= 0) {
                return rec;
            }
        });
        self.view.typeHeadState.tbxSearchKey.info.data = r2[0] || {};
        //city
        var r3 = city.filter(function(rec) {
            if (rec.lblAddress.text.indexOf(name[2]) >= 0) {
                return rec;
            }
        });
        self.view.typeHeadCity.tbxSearchKey.info.data = r3[0] || {};
    },
    returnEnteredText: function(name, email) {
        var enteredText = "";
        if (name !== null && name !== "") {
            if (email !== null && email !== "") {
                enteredText = name + " and " + email;
            } else {
                enteredText = name;
            }
        } else if (email !== null && email !== "") {
            enteredText = email;
        }
        return enteredText;
    },
    /*
     * function to navigate to customer profile form on click of business user name
     */
    onClickUserNavigateToProfile: function() {
        var self = this;
        var data = self.view.segCompanyDetailCustomer.data;
        var index = self.view.segCompanyDetailCustomer.selectedRowIndex[1];
        var rowData = data[index];
        var param = {
            "Customer_id": rowData.customerEditInfo.id
        };
        self.presenter.navigateToCustomerPersonal(param, "frmCompanies");
    }
});
define("CompaniesModule/frmCompaniesControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmCompanies **/
    AS_Form_g1ee5d4ede2f498fa7a53677ed55f226: function AS_Form_g1ee5d4ede2f498fa7a53677ed55f226(eventobject) {
        var self = this;
        this.companiesPreShow();
    }
});
define("CompaniesModule/frmCompaniesController", ["CompaniesModule/userfrmCompaniesController", "CompaniesModule/frmCompaniesControllerActions"], function() {
    var controller = require("CompaniesModule/userfrmCompaniesController");
    var controllerActions = ["CompaniesModule/frmCompaniesControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
