define([], function() {
    /**
     * User defined business controller
     * @constructor
     * @extends kony.mvc.Business.Controller
     */
    function BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(BusinessController, kony.mvc.Business.Controller);
    /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller gets initialized
     * @method
     */
    BusinessController.prototype.initializeBusinessController = function() {};
    BusinessController.prototype.getAddressSuggestion = function(context, OnSucess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ServerManagementManager").businessController.getAddressSuggestion(context, OnSucess, onError);
    };
    BusinessController.prototype.getPlaceDetails = function(context, OnSucess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ServerManagementManager").businessController.getPlaceDetails(context, OnSucess, onError);
    };
    /**
     * @name fetchCountryList
     * @param {} context
     * @param (response:[{Code : string, id : string, Name : string}])=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    BusinessController.prototype.fetchCountryList = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InternalusersManager").businessController.fetchCountryList(context, onSuccess, onError);
    };
    /**
     * @name fetchRegionList
     * @param {} context
     * @param (response:[{Code : string, Country_id : string, id : string, Name : string}])=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    BusinessController.prototype.fetchRegionList = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InternalusersManager").businessController.fetchRegionList(context, onSuccess, onError);
    };
    /**
     * @name fetchCityList
     * @param {} context
     * @param (response:[{Country_id : string, id : string, Name : string, Region_id : string}])=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    BusinessController.prototype.fetchCityList = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InternalusersManager").businessController.fetchCityList(context, onSuccess, onError);
    };
    /**
     * @name creates company
     * @param {} context
     * @param (response:[])=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    BusinessController.prototype.createCompany = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBankingManager").businessController.createCompany(context, onSuccess, onError);
    };
    /**
     * @name edit company
     * @param {} context
     * @param (response:[])=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    BusinessController.prototype.editCompany = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBankingManager").businessController.editCompany(context, onSuccess, onError);
    };
    /**
     * @name fetch accounts for create company
     * @param {} context
     * @param (response:[])=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    BusinessController.prototype.getAllAccounts = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBankingManager").businessController.getAllAccounts(context, onSuccess, onError);
    };
    /**
     * @name fetch company basic details
     * @param {"organizationId" : id} context
     * @param (response:[])=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    BusinessController.prototype.getCompanyDetails = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBankingManager").businessController.getCompanyDetails(context, onSuccess, onError);
    };
    /**
     * @name fetch company accounts
     * @param {"organizationId" : id} context
     * @param (response:[])=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    BusinessController.prototype.getCompanyAccounts = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBankingManager").businessController.getCompanyAccounts(context, onSuccess, onError);
    };
    /**
     * @name fetch company customers
     * @param {"organizationId" : id} context
     * @param (response:[])=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    BusinessController.prototype.getCompanyCustomers = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBankingManager").businessController.getCompanyCustomers(context, onSuccess, onError);
    };
    /* @name validateTin
     * @param {tin:"123"} context
     * @param (response:[])=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    BusinessController.prototype.validateTIN = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBankingManager").businessController.validateTIN(context, onSuccess, onError);
    };
    /* @name get Companies Search
     * @param {"Name":"abcd","Email":"abcdefgh@kony.com"} context
     * @param (response:[])=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    BusinessController.prototype.getCompaniesSearch = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBankingManager").businessController.getCompanyDetails(context, onSuccess, onError);
    };
    /* @name unlink accounts
     * @param 
     */
    BusinessController.prototype.unlinkAccounts = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBankingManager").businessController.unlinkAccounts(context, onSuccess, onError);
    };
    /**
     * @name getCustomerTransactions
     * @member CustomerManagementModule.businessController
     * @param {AccountNumber : string, StartDate : string, EndDate : string} context
     * @param (response:{Status : string, Transactions : [{amount : object, fromAccountNumber : object, toAccountType : object, fromAccountName : object, fromNickName : object, hasDepositImage : object, description : object, scheduledDate : object, isScheduled : object, transactionDate : object, transactionId : object, transactiontype : object, fromAccountType : object, toAccountName : object, statusDescription : object, fromAccountBalance : object, transactionsNotes : object, toAccountNumber : object, frequencyType : object}], opstatus : number, httpStatusCode : number, httpresponse : {headers : string, url : string, responsecode : number}})=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    BusinessController.prototype.getCustomerTransactions = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CustomerManagementManager").businessController.getCustomerTransactions(context, onSuccess, onError);
    };
    /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller is told to execute a command
     * @method
     * @param {Object} kony.mvc.Business.Command Object
     */
    BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return BusinessController;
});