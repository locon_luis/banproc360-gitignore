define("userflxMasterListController", {
    //Type your controller code here 
    toggleCheckbox: function() {
        var ScopeObj = this;
        var index = kony.application.getCurrentForm().SelectScheduler.segMasterList.selectedIndex;
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().SelectScheduler.segMasterList.data;
        for (var i = 0; i < data.length; i++) {
            data[i].imgCheckbox.src = "radio_notselected.png";
        }
        data[rowIndex].imgCheckbox.src = "radioselected_2x.png";
        kony.application.getCurrentForm().SelectScheduler.flxNoDetails.setVisibility(false);
        kony.application.getCurrentForm().SelectScheduler.flxDetails.setVisibility(true);
        kony.application.getCurrentForm().SelectScheduler.segMasterList.setData(data);
    }
});
define("flxMasterListControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxCheckbox **/
    AS_FlexContainer_cd67f564ca4943368a56063416331d0a: function AS_FlexContainer_cd67f564ca4943368a56063416331d0a(eventobject, context) {
        var self = this;
    },
    /** onClick defined for flxMain **/
    AS_FlexContainer_bfe51f206d024a2f919f09d75413b2ac: function AS_FlexContainer_bfe51f206d024a2f919f09d75413b2ac(eventobject, context) {
        var self = this;
        this.toggleCheckbox();
    }
});
define("flxMasterListController", ["userflxMasterListController", "flxMasterListControllerActions"], function() {
    var controller = require("userflxMasterListController");
    var controllerActions = ["flxMasterListControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
