define(['Promisify', 'ErrorInterceptor', 'ErrorIsNetworkDown'], function(Promisify, ErrorInterceptor, isNetworkDown) {
    function CampaignModule_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.campaignConfigurations = null;
    }
    inheritsFrom(CampaignModule_PresentationController, kony.mvc.Presentation.BasePresenter);
    CampaignModule_PresentationController.prototype.initializePresentationController = function() {
        var self = this;
        ErrorInterceptor.wrap(this, 'businessController').match(function(on) {
            return [
                on(isNetworkDown).do(function() {
                    self.presentUserInterface(self.currentForm, {
                        NetworkDownMessage: {}
                    });
                })
            ];
        });
    };
    /**
     * @name fetchCampaigns
     * @member CampaignModule.presentationController
     * 
     */
    CampaignModule_PresentationController.prototype.fetchCampaigns = function(isCampaignUpdated) {
        var self = this;
        self.showProgressBarCampaign();

        function onSuccess(response) {
            var responseObj = {
                "fetchCampaigns": "success",
                "campaignsList": response.campaigns,
            };
            if (isCampaignUpdated) {
                responseObj.action = "createOrUpdateStatus";
                responseObj.status = isCampaignUpdated.status;
                responseObj.msg = isCampaignUpdated.msg;
            }
            self.presentUserInterface('frmAdManagement', responseObj);
            self.hideProgressBarCampaign();
        }

        function onError(response) {
            self.presentUserInterface('frmAdManagement', {
                "fetchCampaigns": "failure"
            });
            self.hideProgressBarCampaign();
        }
        this.businessController.fetchCampaigns({}, onSuccess, onError);
    };
    CampaignModule_PresentationController.prototype.fetchCampaignURLandGroups = function(campaignId) {
        var self = this;
        self.showProgressBarCampaign();
        var payload = {
            "campaignId": campaignId
        };

        function onSuccess(response) {
            self.presentUserInterface('frmAdManagement', {
                "fetchURLandGroups": "success",
                "campaignSpecifications": response.campaignSpecifications,
                "campaignGroups": response.campaignGroups
            });
            self.hideProgressBarCampaign();
        }

        function onError(response) {
            self.presentUserInterface('frmAdManagement', {
                "fetchURLandGroups": "failure"
            });
            self.hideProgressBarCampaign();
        }
        this.businessController.fetchCampaignURLandGroups(payload, onSuccess, onError);
    };
    CampaignModule_PresentationController.prototype.getDefaultCampaign = function() {
        var self = this;
        self.showProgressBarCampaign();

        function onSuccess(response) {
            self.presentUserInterface('frmAdManagement', {
                "fetchDefaultCampaign": "success",
                "description": response.description,
                "specifications": response.specifications
            });
            self.hideProgressBarCampaign();
        }

        function onError(response) {
            self.presentUserInterface('frmAdManagement', {
                "fetchDefaultCampaign": "failure"
            });
            self.hideProgressBarCampaign();
        }
        self.businessController.getDefaultCampaign({}, onSuccess, onError);
    };
    CampaignModule_PresentationController.prototype.updateCampaignStatus = function(context) {
        var self = this;
        self.showProgressBarCampaign();

        function onSuccess(response) {
            self.fetchCampaigns();
            self.presentUserInterface('frmAdManagement', {
                "updatedCampaignStatus": "success",
                "toastMessage": kony.i18n.getLocalizedString("i18n.frmAdManagement.UpdatedSuccessfully")
            });
            self.hideProgressBarCampaign();
        }

        function onError(response) {
            self.presentUserInterface('frmAdManagement', {
                "updatedCampaignStatus": "failure",
                "toastMessage": "Update failure"
            });
            self.hideProgressBarCampaign();
        }
        this.businessController.updateCampaignStatus(context, onSuccess, onError);
    };
    CampaignModule_PresentationController.prototype.showProgressBarCampaign = function() {
        this.showCampaigns({
            progressBar: {
                show: "success"
            }
        });
    };
    CampaignModule_PresentationController.prototype.hideProgressBarCampaign = function() {
        this.showCampaigns({
            progressBar: {
                show: "fail"
            }
        });
    };
    CampaignModule_PresentationController.prototype.showCampaigns = function(context) {
        this.currentForm = "frmAdManagement";
        this.presentUserInterface("frmAdManagement", context);
    };
    CampaignModule_PresentationController.prototype.getCampaignConfigurations = function() {
        var self = this;
        this.currentForm = "frmAdDetails";

        function onSuccess(response) {
            self.presentUserInterface("frmAdDetails", {
                "action": "createCampaign",
                "channels": response ? response.channels : self.campaignConfigurations.channels
            });
        }

        function onError(response) {
            self.fetchCampaigns({
                status: "failure",
                msg: kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorCampaignConfigurations")
            });
        }
        if (self.campaignConfigurations === null) {
            this.businessController.fetchCampaignMasterData({}, onSuccess, onError);
        } else {
            onSuccess();
        }
    };
    CampaignModule_PresentationController.prototype.fetchCampaignPriorities = function(isFetchList) {
        var self = this;
        self.presentUserInterface("frmAdDetails", {
            "LoadingScreen": {
                "focus": true
            }
        });

        function onSuccess(response) {
            self.presentUserInterface("frmAdDetails", {
                "action": isFetchList ? "CampaignPriorityList" : "checkPriority",
                "campaignList": response.campaignPriorityList,
                "LoadingScreen": {
                    "focus": false
                }
            });
        }

        function onError(error) {
            self.showToastMessage("frmAdDetails", ErrorInterceptor.errorMessage(error), 'FAILURE');
        }
        this.businessController.fetchCampaignPriorities({}, onSuccess, onError);
    };
    CampaignModule_PresentationController.prototype.fetchCustomerGroups = function() {
        var self = this;
        self.presentUserInterface("frmAdDetails", {
            "LoadingScreen": {
                "focus": true
            }
        });

        function onSuccess(response) {
            self.presentUserInterface("frmAdDetails", {
                "action": "getCustomerGroups",
                "customerRoles": response,
                "LoadingScreen": {
                    "focus": false
                }
            });
        }

        function onError(response) {
            self.showToastMessage("frmAdDetails", ErrorInterceptor.errorMessage(error), 'FAILURE');
        }
        this.businessController.fetchCustomerGroups({}, onSuccess, onError);
    };
    CampaignModule_PresentationController.prototype.createCampaign = function(context) {
        var self = this;
        self.presentUserInterface("frmAdDetails", {
            "LoadingScreen": {
                "focus": true
            }
        });

        function onSuccess(response) {
            self.fetchCampaigns({
                status: "success",
                msg: kony.i18n.getLocalizedString("i18n.frmAdManagement.CreatedSuccessfully")
            });
        }

        function onError(error) {
            self.showToastMessage("frmAdDetails", ErrorInterceptor.errorMessage(error), 'FAILURE');
        }
        self.businessController.createCampaign(context, onSuccess, onError);
    };
    CampaignModule_PresentationController.prototype.getEditCampaignDetails = function(context) {
        var self = this;
        var hasSpecifications = false;
        self.showProgressBarCampaign();

        function onSuccess(response) {
            context.campaignSpecifications = response.campaignSpecifications;
            context.campaignGroups = response.campaignGroups;
            self.presentUserInterface('frmAdDetails', {
                "action": "editCampaign",
                "campaignData": context,
                "hasSpecifications": hasSpecifications,
                "campaignConfigurations": self.campaignConfigurations,
                "LoadingScreen": {
                    "focus": false
                }
            });
        }

        function onError(response) {
            self.fetchCampaigns({
                status: "failure",
                msg: kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorCampaignDetails")
            });
        }

        function getConfigurationsSuccess(response) {
            self.campaignConfigurations = response ? {
                "channels": response.channels
            } : self.campaignConfigurations;
            self.businessController.fetchCampaignURLandGroups({
                "campaignId": context.id
            }, onSuccess, onError);
        }

        function getConfigurationsfailure(response) {
            self.fetchCampaigns({
                status: "failure",
                msg: kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorCampaignConfigurations")
            });
        }
        if (self.campaignConfigurations === null) {
            hasSpecifications = true;
            self.businessController.fetchCampaignMasterData({}, getConfigurationsSuccess, getConfigurationsfailure);
        } else {
            getConfigurationsSuccess();
        }
    };
    CampaignModule_PresentationController.prototype.updateCampaign = function(context) {
        var self = this;
        self.presentUserInterface("frmAdDetails", {
            "LoadingScreen": {
                "focus": true
            }
        });

        function onSuccess(response) {
            self.fetchCampaigns({
                status: "success",
                msg: kony.i18n.getLocalizedString("i18n.frmAdManagement.UpdatedSuccessfully")
            });
        }

        function onError(error) {
            self.showToastMessage("frmAdDetails", ErrorInterceptor.errorMessage(error), 'FAILURE');
        }
        self.businessController.updateCampaign(context, onSuccess, onError);
    };
    CampaignModule_PresentationController.prototype.showToastMessage = function(form, message, status) {
        var self = this;
        self.presentUserInterface(form, {
            toastModel: {
                message: message,
                status: status
            },
            "LoadingScreen": {
                "focus": false
            }
        });
    };
    CampaignModule_PresentationController.prototype.updateDefaultCampaign = function(context) {
        var self = this;
        self.presentUserInterface("frmAdDetails", {
            "LoadingScreen": {
                "focus": true
            }
        });

        function onSuccess(response) {
            self.fetchCampaigns({
                status: "success",
                msg: kony.i18n.getLocalizedString("i18n.frmAdManagement.UpdatedSuccessfully")
            });
        }

        function onError(error) {
            self.showToastMessage("frmAdDetails", ErrorInterceptor.errorMessage(error), 'FAILURE');
        }
        self.businessController.updateDefaultCampaign(context, onSuccess, onError);
    };
    CampaignModule_PresentationController.prototype.editDefaultCampaign = function(context) {
        var self = this;
        self.presentUserInterface("frmAdDetails", {
            "action": "defaultCampaign",
            "defaultCampaignObject": context
        });
    };
    return CampaignModule_PresentationController;
});