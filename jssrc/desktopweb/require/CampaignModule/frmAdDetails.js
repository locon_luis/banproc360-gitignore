define("CampaignModule/frmAdDetails", function() {
    return function(controller) {
        function addWidgetsfrmAdDetails() {
            this.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305dp",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "konydbxlogobignverted.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "imgBottomLogo": {
                        "src": "konydbxinverted.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0b7aaa2e3bfa340",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.TEMPLATES\")",
                        "right": "212px"
                    },
                    "btnDropdownList": {
                        "text": "CREATE NEW MESSAGE"
                    },
                    "flxButtons": {
                        "isVisible": false
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "flxHeaderUserOptions": {
                        "isVisible": false
                    },
                    "flxMainHeader": {
                        "left": undefined,
                        "top": undefined
                    },
                    "imgLogout": {
                        "right": "0px",
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AdCampaigns\")"
                    },
                    "lblUserName": {
                        "right": "25px",
                        "text": "Preetish",
                        "top": "viz.val_cleared"
                    },
                    "mainHeader": {
                        "left": undefined,
                        "top": undefined
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "95px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "1px",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "viz.val_cleared",
                        "top": "1px",
                        "width": "100%"
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AdCampaignListCAPS\")"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "left": "10dp",
                        "right": 10,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "left": "10dp",
                        "right": 10,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxHeaderSeperator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxBreadCrumbs.add(breadcrumbs, flxHeaderSeperator);
            var flxContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "83%",
                "id": "flxContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "145px",
                "zIndex": 1
            }, {}, {});
            flxContainer.setDefaultUnit(kony.flex.DP);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "0%",
                "width": "230dp",
                "zIndex": 1
            }, {}, {});
            flxOptions.setDefaultUnit(kony.flex.DP);
            var flxAdDetailNav = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAdDetailNav",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdDetailNav.setDefaultUnit(kony.flex.DP);
            var lblAdDetailsHeader = new kony.ui.Label({
                "bottom": 15,
                "id": "lblAdDetailsHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblLatoBold12px485C75",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AdDetailsCAPS\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblBold485c7512pxHoverCursor"
            });
            var flxoptionSeperator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "49%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxoptionSeperator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxe1e5edop100",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxoptionSeperator1.setDefaultUnit(kony.flex.DP);
            flxoptionSeperator1.add();
            var fontIconImgSelected1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconImgSelected1",
                "isVisible": true,
                "right": "20px",
                "skin": "sknLblIIcoMoon485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdDetailNav.add(lblAdDetailsHeader, flxoptionSeperator1, fontIconImgSelected1);
            var flxAdSpecificationNav = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAdSpecificationNav",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdSpecificationNav.setDefaultUnit(kony.flex.DP);
            var lblAdSpecificationsHeader = new kony.ui.Label({
                "bottom": 15,
                "id": "lblAdSpecificationsHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AdSpecificationsCAPS\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbl485c7512pxHoverCursor"
            });
            var flxoptionSeperator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "49%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxoptionSeperator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxe1e5edop100",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxoptionSeperator2.setDefaultUnit(kony.flex.DP);
            flxoptionSeperator2.add();
            var fontIconImgSelected2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconImgSelected2",
                "isVisible": false,
                "right": "20px",
                "skin": "sknLblIIcoMoon485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdSpecificationNav.add(lblAdSpecificationsHeader, flxoptionSeperator2, fontIconImgSelected2);
            var flxTargetCustomersNav = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxTargetCustomersNav",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTargetCustomersNav.setDefaultUnit(kony.flex.DP);
            var lblTargetCustomersHeader = new kony.ui.Label({
                "bottom": 15,
                "id": "lblTargetCustomersHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.TargetCustomerRolesCAPS\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbl485c7512pxHoverCursor"
            });
            var flxoptionSeperator3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "49%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxoptionSeperator3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxe1e5edop100",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxoptionSeperator3.setDefaultUnit(kony.flex.DP);
            flxoptionSeperator3.add();
            var fontIconImgSelected3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconImgSelected3",
                "isVisible": false,
                "right": "20px",
                "skin": "sknLblIIcoMoon485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTargetCustomersNav.add(lblTargetCustomersHeader, flxoptionSeperator3, fontIconImgSelected3);
            flxOptions.add(flxAdDetailNav, flxAdSpecificationNav, flxTargetCustomersNav);
            var flxDetails = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxDetails.setDefaultUnit(kony.flex.DP);
            var flxContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "400dp",
                "horizontalScrollIndicator": true,
                "id": "flxContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknflxScrle4e6ec1pxfff",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxContent.setDefaultUnit(kony.flex.DP);
            var flxBasicDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBasicDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxBasicDetails.setDefaultUnit(kony.flex.DP);
            var flxAdName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAdName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxAdName.setDefaultUnit(kony.flex.DP);
            var lblAdName = new kony.ui.Label({
                "id": "lblAdName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AdName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAdNameSize = new kony.ui.Label({
                "id": "lblAdNameSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknLblLato485c7513px",
                "text": "0/50",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdName.add(lblAdName, lblAdNameSize);
            var txtAdName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtAdName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AdName\")",
                "secureTextEntry": false,
                "skin": "txtD7d9e0",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "sknTextGreyb2bdcd"
            });
            var AdNameError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "AdNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorAdName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblDescription = new kony.ui.Label({
                "id": "lblDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Description\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDescriptionSize = new kony.ui.Label({
                "id": "lblDescriptionSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknLblLato485c7513px",
                "text": "0/150",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescription.add(lblDescription, lblDescriptionSize);
            var txtDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknFocus",
                "height": "70dp",
                "id": "txtDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 150,
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Description\")",
                "skin": "skntxtAread7d9e0",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextAreaFocus"
            });
            var AdDescriptionError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "AdDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorAdDescription\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxPriorityContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPriorityContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPriorityContainer.setDefaultUnit(kony.flex.DP);
            var lblPriority = new kony.ui.Label({
                "id": "lblPriority",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Priority\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLine = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblLine",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "l",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPriorityInfoText = new kony.ui.Label({
                "id": "lblPriorityInfoText",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityInfoText\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPriority = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxPriority",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "230dp",
                "zIndex": 1
            }, {}, {});
            flxPriority.setDefaultUnit(kony.flex.DP);
            var txtPriority = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "100%",
                "id": "txtPriority",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
                "left": "0",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Priority\")",
                "secureTextEntry": false,
                "skin": "txtD7d9e0",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0",
                "width": "140dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [5, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var btnPriorityList = new kony.ui.Button({
                "height": "100%",
                "id": "btnPriorityList",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknBtnd7d9e0",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ViewList\")",
                "top": "0dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnd7d9e0"
            });
            flxPriority.add(txtPriority, btnPriorityList);
            flxPriorityContainer.add(lblPriority, lblLine, lblPriorityInfoText, flxPriority);
            var PriorityError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "PriorityError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorPriority\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblDateScheduler = new kony.ui.Label({
                "id": "lblDateScheduler",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DateScheduleforAd\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLine1 = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblLine1",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "l",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDateContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "25dp",
                "clipBounds": true,
                "id": "flxDateContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDateContainer.setDefaultUnit(kony.flex.DP);
            var lblDatesInfoText = new kony.ui.Label({
                "id": "lblDatesInfoText",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DateScheduleforAdInfoText\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStartDate = new kony.ui.Label({
                "id": "lblStartDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.StartDate\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStartDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxStartDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "15dp",
                "width": "225dp"
            }, {}, {});
            flxStartDate.setDefaultUnit(kony.flex.DP);
            var customStartDate = new kony.ui.CustomWidget({
                "id": "customStartDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "40px",
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxStartDate.add(customStartDate);
            var StartDateError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "StartDateError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorEndDate\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblEndDate = new kony.ui.Label({
                "id": "lblEndDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.EndDate\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEndDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxEndDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "15dp",
                "width": "225dp"
            }, {}, {});
            flxEndDate.setDefaultUnit(kony.flex.DP);
            var customEndDate = new kony.ui.CustomWidget({
                "id": "customEndDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "40px",
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxEndDate.add(customEndDate);
            var EndDateError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "EndDateError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorEndDate\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDateContainer.add(lblDatesInfoText, lblStartDate, flxStartDate, StartDateError, lblEndDate, flxEndDate, EndDateError);
            var rtxDefaultDateInfo = new kony.ui.RichText({
                "bottom": "25dp",
                "id": "rtxDefaultDateInfo",
                "isVisible": false,
                "left": "0",
                "linkSkin": "defRichTextLink",
                "skin": "sknrtxLatoRegular485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DefaultCampaignDatemsg\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBasicDetails.add(flxAdName, txtAdName, AdNameError, flxDescription, txtDescription, AdDescriptionError, flxPriorityContainer, PriorityError, lblDateScheduler, lblLine1, flxDateContainer, rtxDefaultDateInfo);
            var flxAdSpecifications = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAdSpecifications",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxAdSpecifications.setDefaultUnit(kony.flex.DP);
            flxAdSpecifications.add();
            var flxTargetCustomers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTargetCustomers",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxTargetCustomers.setDefaultUnit(kony.flex.DP);
            var flxAvailableOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAvailableOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "top": "0dp",
                "width": "49%",
                "zIndex": 1
            }, {}, {});
            flxAvailableOptions.setDefaultUnit(kony.flex.DP);
            var flxAvailableOptionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxAvailableOptionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxAvailableOptionsHeader.setDefaultUnit(kony.flex.DP);
            var lblAvailableOptionsHeading = new kony.ui.Label({
                "id": "lblAvailableOptionsHeading",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AvailableCustomerRoles\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddAll = new kony.ui.Button({
                "id": "btnAddAll",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknbtn117eb011px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AddAllCaps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAvailableOptionsHeader.add(lblAvailableOptionsHeading, btnAddAll);
            var flxAvailableOptionsContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAvailableOptionsContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgf7f9faBorderd7d9e0",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxAvailableOptionsContent.setDefaultUnit(kony.flex.DP);
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": 0,
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "focusSkin": "slFbox0ebc847fa67a243Search",
                "height": "40px",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": 10,
                "skin": "sknflxd5d9ddop100",
                "zIndex": 1
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var tbxSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "100%",
                "id": "tbxSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "35dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")",
                "right": "35dp",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "47%",
                "clipBounds": true,
                "height": "35px",
                "id": "flxClearSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearch.setDefaultUnit(kony.flex.DP);
            var fontIconImgCLose = new kony.ui.Label({
                "centerY": "50.00%",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearch.add(fontIconImgCLose);
            var fontIconImgSearchIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconImgSearchIcon",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconSearchimg\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchContainer.add(tbxSearchBox, flxClearSearch, fontIconImgSearchIcon);
            flxSearch.add(flxSearchContainer);
            var flxAddOptionsSegment = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAddOptionsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddOptionsSegment.setDefaultUnit(kony.flex.DP);
            var segAddOptions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "data": [{
                    "btnAdd": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segAddOptions",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknsegffffffOp100Bordere1e5ed",
                "rowTemplate": "flxCampaignAddCustomer",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnAdd": "btnAdd",
                    "flxAddUsersWrapper": "flxAddUsersWrapper",
                    "flxCampaignAddCustomer": "flxCampaignAddCustomer",
                    "flxDetails": "flxDetails",
                    "lblDesc": "lblDesc",
                    "lblName": "lblName"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCustomersAvailable = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoCustomersAvailable",
                "isVisible": false,
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.NoCustomersAvailable\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddOptionsSegment.add(segAddOptions, lblNoCustomersAvailable);
            flxAvailableOptionsContent.add(flxSearch, flxAddOptionsSegment);
            flxAvailableOptions.add(flxAvailableOptionsHeader, flxAvailableOptionsContent);
            var flxSelectedOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectedOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "2%",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0c11dce22bfe447",
                "width": "49%",
                "zIndex": 1
            }, {}, {});
            flxSelectedOptions.setDefaultUnit(kony.flex.DP);
            var flxSelectedOptionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxSelectedOptionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxSelectedOptionsHeader.setDefaultUnit(kony.flex.DP);
            var lblSelectedOption = new kony.ui.Label({
                "id": "lblSelectedOption",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.SelectedCustomerRoles\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnRemoveAll = new kony.ui.Button({
                "id": "btnRemoveAll",
                "isVisible": true,
                "right": 0,
                "skin": "sknbtn117eb011px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ResetCAPS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectedOptionsHeader.add(lblSelectedOption, btnRemoveAll);
            var flxSegSelectedOptions = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxSegSelectedOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknf9f9f9d7d9e03px",
                "top": "15dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegSelectedOptions.setDefaultUnit(kony.flex.DP);
            var segSelectedOptions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "fontIconClose": "",
                    "lblOption": "John Doe"
                }, {
                    "fontIconClose": "",
                    "lblOption": "John Doe"
                }],
                "groupCells": false,
                "id": "segSelectedOptions",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "sknsegf9f9f9Op100",
                "rowTemplate": "flxCampaignOptionsSelected",
                "sectionHeaderSkin": "sliPhoneSegmentHeader0h3448bdd9d8941",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "64646400",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": true,
                "top": "0px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAddOptionWrapper": "flxAddOptionWrapper",
                    "flxCampaignOptionsSelected": "flxCampaignOptionsSelected",
                    "flxClose": "flxClose",
                    "fontIconClose": "fontIconClose",
                    "lblOption": "lblOption"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoSelectedCustomers = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoSelectedCustomers",
                "isVisible": false,
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ClickToAddCustomer\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegSelectedOptions.add(segSelectedOptions, lblNoSelectedCustomers);
            flxSelectedOptions.add(flxSelectedOptionsHeader, flxSegSelectedOptions);
            var rtxdefaultCampaignCustomerMsg = new kony.ui.RichText({
                "id": "rtxdefaultCampaignCustomerMsg",
                "isVisible": false,
                "linkSkin": "defRichTextLink",
                "skin": "sknrtxLatoRegular485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DefaultCampaignCustomersRolesMsg\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTargetCustomers.add(flxAvailableOptions, flxSelectedOptions, rtxdefaultCampaignCustomerMsg);
            flxContent.add(flxBasicDetails, flxAdSpecifications, flxTargetCustomers);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxe4e6ec1px",
                "width": "100%",
                "zIndex": 11
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "isVisible": true,
                        "left": "20dp"
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Next\")",
                        "right": "0dp"
                    },
                    "flxRightButtons": {
                        "right": "20px",
                        "width": "250px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxButtons.add(commonButtons);
            flxDetails.add(flxContent, flxButtons);
            flxContainer.add(flxOptions, flxDetails);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPannel.add(flxMainHeader, flxHeaderDropdown, flxBreadCrumbs, flxContainer, flxLoading);
            var flxConsentPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxConsentPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxConsentPopup.setDefaultUnit(kony.flex.DP);
            var flxCampaignList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "450dp",
                "id": "flxCampaignList",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "150px",
                "width": "560dp",
                "zIndex": 10
            }, {}, {});
            flxCampaignList.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "490dp",
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblPopupClose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(lblPopupClose);
            var lblPopUpMainMessage = new kony.ui.Label({
                "height": "20dp",
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityList\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPriorityList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPriorityList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxPriorityList.setDefaultUnit(kony.flex.DP);
            var flxTable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTable",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxTable.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var flxCampaignName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCampaignName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60%"
            }, {}, {});
            flxCampaignName.setDefaultUnit(kony.flex.DP);
            var lblCampaignHeader = new kony.ui.Label({
                "id": "lblCampaignHeader",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblCampaignSort = new kony.ui.Label({
                "id": "lblCampaignSort",
                "isVisible": true,
                "left": "5px",
                "skin": "sknLblIIcoMoon485c7514px",
                "text": "",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCampaignName.add(lblCampaignHeader, lblCampaignSort);
            var flxPriorityHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPriorityHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "36%"
            }, {}, {});
            flxPriorityHeader.setDefaultUnit(kony.flex.DP);
            var lblPriorityHeader = new kony.ui.Label({
                "id": "lblPriorityHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblPrioritySort = new kony.ui.Label({
                "id": "lblPrioritySort",
                "isVisible": true,
                "left": "5px",
                "skin": "sknLblIIcoMoon485c7514px",
                "text": "",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPriorityHeader.add(lblPriorityHeader, lblPrioritySort);
            flxHeader.add(flxCampaignName, flxPriorityHeader);
            var lblSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "l",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSegmentScrol = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "320dp",
                "horizontalScrollIndicator": true,
                "id": "flxSegmentScrol",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxSegmentScrol.setDefaultUnit(kony.flex.DP);
            var segCampaigns = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblCampaign": kony.i18n.getLocalizedString("i18n.frmAdManagement.CampaignNameCAPS"),
                    "lblPriority": kony.i18n.getLocalizedString("i18n.frmAdManagement.PriorityCAPS")
                }, {
                    "lblCampaign": kony.i18n.getLocalizedString("i18n.frmAdManagement.CampaignNameCAPS"),
                    "lblPriority": kony.i18n.getLocalizedString("i18n.frmAdManagement.PriorityCAPS")
                }, {
                    "lblCampaign": kony.i18n.getLocalizedString("i18n.frmAdManagement.CampaignNameCAPS"),
                    "lblPriority": kony.i18n.getLocalizedString("i18n.frmAdManagement.PriorityCAPS")
                }, {
                    "lblCampaign": kony.i18n.getLocalizedString("i18n.frmAdManagement.CampaignNameCAPS"),
                    "lblPriority": kony.i18n.getLocalizedString("i18n.frmAdManagement.PriorityCAPS")
                }, {
                    "lblCampaign": kony.i18n.getLocalizedString("i18n.frmAdManagement.CampaignNameCAPS"),
                    "lblPriority": kony.i18n.getLocalizedString("i18n.frmAdManagement.PriorityCAPS")
                }, {
                    "lblCampaign": kony.i18n.getLocalizedString("i18n.frmAdManagement.CampaignNameCAPS"),
                    "lblPriority": kony.i18n.getLocalizedString("i18n.frmAdManagement.PriorityCAPS")
                }, {
                    "lblCampaign": kony.i18n.getLocalizedString("i18n.frmAdManagement.CampaignNameCAPS"),
                    "lblPriority": kony.i18n.getLocalizedString("i18n.frmAdManagement.PriorityCAPS")
                }, {
                    "lblCampaign": kony.i18n.getLocalizedString("i18n.frmAdManagement.CampaignNameCAPS"),
                    "lblPriority": kony.i18n.getLocalizedString("i18n.frmAdManagement.PriorityCAPS")
                }, {
                    "lblCampaign": kony.i18n.getLocalizedString("i18n.frmAdManagement.CampaignNameCAPS"),
                    "lblPriority": kony.i18n.getLocalizedString("i18n.frmAdManagement.PriorityCAPS")
                }, {
                    "lblCampaign": kony.i18n.getLocalizedString("i18n.frmAdManagement.CampaignNameCAPS"),
                    "lblPriority": kony.i18n.getLocalizedString("i18n.frmAdManagement.PriorityCAPS")
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segCampaigns",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCampaignPriorities",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCampaignPriorities": "flxCampaignPriorities",
                    "lblCampaign": "lblCampaign",
                    "lblPriority": "lblPriority"
                },
                "width": "100%",
                "zIndex": 20
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegmentScrol.add(segCampaigns);
            flxTable.add(flxHeader, lblSeperator, flxSegmentScrol);
            flxPriorityList.add(flxTable);
            flxPopupHeader.add(flxPopUpClose, lblPopUpMainMessage, flxPriorityList);
            flxCampaignList.add(flxPopUpTopColor, flxPopupHeader);
            var popUp = new com.adminConsole.common.popUp({
                "clipBounds": true,
                "height": "100%",
                "id": "popUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "left": "265px",
                        "right": "160px",
                        "width": "180px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesProceed\")"
                    },
                    "popUp": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDisplayImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10%",
                "clipBounds": true,
                "id": "flxDisplayImage",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "6%",
                "isModalContainer": false,
                "right": "6%",
                "skin": "sknBackgroundFFFFFF",
                "top": "10%"
            }, {}, {});
            flxDisplayImage.setDefaultUnit(kony.flex.DP);
            var flxTopBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopBar.setDefaultUnit(kony.flex.DP);
            flxTopBar.add();
            var flxChannelInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxChannelInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxChannelInfo.setDefaultUnit(kony.flex.DP);
            var flxPreviewMode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewMode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "101px",
                "zIndex": 1
            }, {}, {});
            flxPreviewMode.setDefaultUnit(kony.flex.DP);
            var lblPreviewMode = new kony.ui.Label({
                "id": "lblPreviewMode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485c75LatoBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.previewMode\")",
                "top": "27px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewMode.add(lblPreviewMode);
            var flxScreenInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxScreenInfo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxScreenInfo.setDefaultUnit(kony.flex.DP);
            var lblChannelTag = new kony.ui.Label({
                "id": "lblChannelTag",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485c75LatoBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ChannelColon\")",
                "top": "27px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblChannelName = new kony.ui.Label({
                "id": "lblChannelName",
                "isVisible": true,
                "left": "5px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Native Web App",
                "top": "27px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDisplayPageName = new kony.ui.Label({
                "id": "lblDisplayPageName",
                "isVisible": true,
                "left": "40px",
                "skin": "sknLbl485c75LatoBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.displayPageNameColon\")",
                "top": "27px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblScreenName = new kony.ui.Label({
                "id": "lblScreenName",
                "isVisible": true,
                "left": "5px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Pre Login Screen",
                "top": "27px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxScreenInfo.add(lblChannelTag, lblChannelName, lblDisplayPageName, lblScreenName);
            var flxClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxffffffCursorPointer",
                "top": "10px",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxClose.setDefaultUnit(kony.flex.DP);
            var lblClose = new kony.ui.Label({
                "height": "100%",
                "id": "lblClose",
                "isVisible": true,
                "left": "0px",
                "skin": "lblIconGrey",
                "text": "",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClose.add(lblClose);
            var flxAdSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAdSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxD7D9E0Separator",
                "zIndex": 1
            }, {}, {});
            flxAdSeparator.setDefaultUnit(kony.flex.DP);
            flxAdSeparator.add();
            flxChannelInfo.add(flxPreviewMode, flxScreenInfo, flxClose, flxAdSeparator);
            var flxShowImageParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "height": "89.48%",
                "id": "flxShowImageParent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%"
            }, {}, {});
            flxShowImageParent.setDefaultUnit(kony.flex.DP);
            var flxScrollImageDisplay = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "98%",
                "horizontalScrollIndicator": true,
                "id": "flxScrollImageDisplay",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_BOTH,
                "skin": "slFSbox",
                "top": "0px",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScrollImageDisplay.setDefaultUnit(kony.flex.DP);
            var flxBorder = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxBorder",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxBorder3px000000",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {}, {});
            flxBorder.setDefaultUnit(kony.flex.DP);
            flxBorder.add();
            var flxImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxImage.setDefaultUnit(kony.flex.DP);
            var richTextAdImage = new kony.ui.RichText({
                "id": "richTextAdImage",
                "isVisible": true,
                "left": "0px",
                "linkSkin": "defRichTextLink",
                "skin": "slRichText",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImage.add(richTextAdImage);
            flxScrollImageDisplay.add(flxBorder, flxImage);
            flxShowImageParent.add(flxScrollImageDisplay);
            flxDisplayImage.add(flxTopBar, flxChannelInfo, flxShowImageParent);
            flxConsentPopup.add(flxCampaignList, popUp, flxDisplayImage);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            this.add(flxLeftPannel, flxRightPannel, flxConsentPopup, flxToastMessage);
        };
        return [{
            "addWidgets": addWidgetsfrmAdDetails,
            "enabledForIdleTimeout": true,
            "id": "frmAdDetails",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_bf5188f919a741e0bb5187da73d26d40,
            "preShow": function(eventobject) {
                controller.AS_Form_d4c237d41e654e8188a8b4bf4eb981f4(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_b0638d9f664d41828d64a669d1ca619c,
            "retainScrollPosition": false
        }]
    }
});