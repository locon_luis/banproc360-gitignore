define([], function() {
    function CampaignModule_BusinessController() {
        kony.mvc.Business.Delegator.call(this);
    }
    inheritsFrom(CampaignModule_BusinessController, kony.mvc.Business.Delegator);
    CampaignModule_BusinessController.prototype.initializeBusinessController = function() {};
    CampaignModule_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    /**
     * @name fetchCampaigns
     * @member CampaignModule_BusinessController.businessController
     * @param {}=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    CampaignModule_BusinessController.prototype.fetchCampaigns = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CampaignManager").businessController.fetchCampaigns(context, onSuccess, onError);
    };
    CampaignModule_BusinessController.prototype.fetchCampaignURLandGroups = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CampaignManager").businessController.fetchCampaignURLandGroups(context, onSuccess, onError);
    };
    CampaignModule_BusinessController.prototype.getDefaultCampaign = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CampaignManager").businessController.getDefaultCampaign(context, onSuccess, onError);
    };
    CampaignModule_BusinessController.prototype.updateCampaignStatus = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CampaignManager").businessController.updateCampaignStatus(context, onSuccess, onError);
    };
    CampaignModule_BusinessController.prototype.fetchCampaignMasterData = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CampaignManager").businessController.fetchCampaignMasterData(context, onSuccess, onError);
    };
    CampaignModule_BusinessController.prototype.fetchCampaignPriorities = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CampaignManager").businessController.fetchCampaignPriorities(context, onSuccess, onError);
    };
    CampaignModule_BusinessController.prototype.fetchCustomerGroups = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CustomerGroupsAndEntitlManager").businessController.getCustomerGroupsView(context, onSuccess, onError);
    };
    CampaignModule_BusinessController.prototype.createCampaign = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CampaignManager").businessController.createCampaign(context, onSuccess, onError);
    };
    CampaignModule_BusinessController.prototype.updateCampaign = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CampaignManager").businessController.updateCampaign(context, onSuccess, onError);
    };
    CampaignModule_BusinessController.prototype.getDefaultCampaign = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CampaignManager").businessController.getDefaultCampaign(context, onSuccess, onError);
    };
    CampaignModule_BusinessController.prototype.updateDefaultCampaign = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CampaignManager").businessController.updateDefaultCampaign(context, onSuccess, onError);
    };
    return CampaignModule_BusinessController;
});