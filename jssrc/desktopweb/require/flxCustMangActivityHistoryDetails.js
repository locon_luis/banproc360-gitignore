define("flxCustMangActivityHistoryDetails", function() {
    return function(controller) {
        var flxCustMangActivityHistoryDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustMangActivityHistoryDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "skin": "slFbox"
        }, {}, {
            "hoverSkin": "sknfbfcfcWithoutPointer"
        });
        flxCustMangActivityHistoryDetails.setDefaultUnit(kony.flex.DP);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "20px",
            "skin": "sknlblSeperator",
            "text": ".",
            "width": "1440px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxCloumns = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCloumns",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "1400px"
        }, {}, {});
        flxCloumns.setDefaultUnit(kony.flex.DP);
        var lblDateAndTime = new kony.ui.Label({
            "bottom": "18px",
            "id": "lblDateAndTime",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoBold35475f14px",
            "text": "02/11/17 - 8:00 AM",
            "top": "17px",
            "width": "14.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblModuleName = new kony.ui.Label({
            "bottom": 18,
            "id": "lblModuleName",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Profile Management",
            "top": 17,
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActivityType = new kony.ui.Label({
            "bottom": "18px",
            "id": "lblActivityType",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Profile",
            "top": "17px",
            "width": "13%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActivityDescription = new kony.ui.Label({
            "bottom": "18px",
            "id": "lblActivityDescription",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Activty Desc",
            "top": "17px",
            "width": "26.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "11%"
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var fontIconStatus = new kony.ui.Label({
            "bottom": 18,
            "id": "fontIconStatus",
            "isVisible": true,
            "left": 0,
            "skin": "sknFontIconActivate",
            "text": "",
            "top": "19px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "bottom": 18,
            "id": "lblStatus",
            "isVisible": true,
            "left": "3px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Successful",
            "top": "17px",
            "width": "84%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(fontIconStatus, lblStatus);
        var lblReferenceID = new kony.ui.Label({
            "bottom": 18,
            "id": "lblReferenceID",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "23432344",
            "top": "17px",
            "width": "11%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorCode = new kony.ui.Label({
            "bottom": 18,
            "id": "lblErrorCode",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "text": "N.A",
            "top": "17px",
            "width": "8%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCloumns.add(lblDateAndTime, lblModuleName, lblActivityType, lblActivityDescription, flxStatus, lblReferenceID, lblErrorCode);
        flxCustMangActivityHistoryDetails.add(lblSeperator, flxCloumns);
        return flxCustMangActivityHistoryDetails;
    }
})