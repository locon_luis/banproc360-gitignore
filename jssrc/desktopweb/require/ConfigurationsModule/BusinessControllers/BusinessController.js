define([], function() {
    function ConfigurationsModule_BusinessController() {
        kony.mvc.Business.Delegator.call(this);
    }
    inheritsFrom(ConfigurationsModule_BusinessController, kony.mvc.Business.Delegator);
    ConfigurationsModule_BusinessController.prototype.initializeBusinessController = function() {};
    ConfigurationsModule_BusinessController.prototype.fetchEligibilityCriterias = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ConfigurationsManager").businessController.getEligibilityCriteria({}, onSuccess, onError);
    };
    ConfigurationsModule_BusinessController.prototype.deleteEligibilityCriteria = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ConfigurationsManager").businessController.deleteEligibilityCriteria(context, onSuccess, onError);
    };
    ConfigurationsModule_BusinessController.prototype.updateEligibilityCriteria = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ConfigurationsManager").businessController.updateEligibilityCriteria(context, onSuccess, onError);
    };
    ConfigurationsModule_BusinessController.prototype.addEligibilityCriterias = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ConfigurationsManager").businessController.addEligibilityCriterias(context, onSuccess, onError);
    };
    ConfigurationsModule_BusinessController.prototype.fetchAllBundles = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ConfigurationsManager").businessController.fetchAllBundles({}, onSuccess, onError);
    };
    ConfigurationsModule_BusinessController.prototype.fetchAllConfigurations = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ConfigurationsManager").businessController.fetchAllConfigurations({}, onSuccess, onError);
    };
    ConfigurationsModule_BusinessController.prototype.fetchConfigurations = function(bundleId, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ConfigurationsManager").businessController.fetchConfigurations(bundleId, onSuccess, onError);
    };
    ConfigurationsModule_BusinessController.prototype.manageBundleAndConfigurations = function(bundle, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ConfigurationsManager").businessController.manageBundleAndConfigurations(bundle, onSuccess, onError);
    };
    ConfigurationsModule_BusinessController.prototype.deleteBundleAndConfigurations = function(id, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ConfigurationsManager").businessController.deleteBundleAndConfigurations(id, onSuccess, onError);
    };
    ConfigurationsModule_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return ConfigurationsModule_BusinessController;
});