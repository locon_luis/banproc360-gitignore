define("ConfigurationsModule/frmConfigurationBundles", function() {
    return function(controller) {
        function addWidgetsfrmConfigurationBundles() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "konydbxlogobignverted.png"
                    },
                    "imgBottomLogo": {
                        "src": "konydbxinverted.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "126px",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.addBundleButton\")",
                        "isVisible": true,
                        "right": "0px"
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "isVisible": true,
                        "right": "71dp",
                        "width": "400dp"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": true,
                        "left": "0px",
                        "right": "70px"
                    },
                    "flxHeaderUserOptions": {
                        "height": "24dp",
                        "right": "70px",
                        "width": "30%"
                    },
                    "flxMainHeader": {
                        "left": "35dp",
                        "top": "0dp"
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.titleConfigurationBundle\")",
                        "left": "0dp",
                        "top": "54px"
                    },
                    "mainHeader": {
                        "height": "100px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadcrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "96px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadcrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.breadcrumbConfigurationBundles\")"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "text": "CONFIGURATION"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumb.add(breadcrumbs);
            flxMainHeader.add(mainHeader, flxBreadcrumb);
            var flxMainContent = new kony.ui.FlexContainer({
                "bottom": 0,
                "clipBounds": false,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "top": "126px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxAddConfigurationBundles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "691px",
                "id": "flxAddConfigurationBundles",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxConfigurationBundlesWithBorder",
                "top": "10px",
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationBundles.setDefaultUnit(kony.flex.DP);
            var flxAddConfigurationBundlesBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "600px",
                "id": "flxAddConfigurationBundlesBody",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxConfigurationBundlesNoBorder",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationBundlesBody.setDefaultUnit(kony.flex.DP);
            var flxAddConfigurationBundlesWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddConfigurationBundlesWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxConfigurationBundlesNoBorder",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationBundlesWrapper.setDefaultUnit(kony.flex.DP);
            var flxBundleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "105px",
                "id": "flxBundleName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxBundleName.setDefaultUnit(kony.flex.DP);
            var lblBundleNameHeader = new kony.ui.Label({
                "height": "16px",
                "id": "lblBundleNameHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblConfigurationBundlesNormal",
                "text": "Bundle Name",
                "top": "20px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBundleName = new kony.ui.Label({
                "height": "16px",
                "id": "lblBundleName",
                "isVisible": false,
                "left": "20px",
                "skin": "sknlblConfigurationBundlesNormal",
                "text": "Bundle Name",
                "top": "50px",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtBundleName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40px",
                "id": "txtBundleName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "20px",
                "maxTextLength": 100,
                "placeholder": "Bundle Name",
                "right": "10px",
                "secureTextEntry": false,
                "skin": "skntbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "45px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var flxNoBundleNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoBundleNameError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90px",
                "width": "150px",
                "zIndex": 1
            }, {}, {});
            flxNoBundleNameError.setDefaultUnit(kony.flex.DP);
            var lblNoBundleNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBundleNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoBundleNameError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBundleNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.enterBundleName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBundleNameError.add(lblNoBundleNameErrorIcon, lblNoBundleNameError);
            var flxBundleNameCharCount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "16px",
                "id": "flxBundleNameCharCount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20px",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxBundleNameCharCount.setDefaultUnit(kony.flex.DP);
            var lblBundleNameCharCount = new kony.ui.Label({
                "id": "lblBundleNameCharCount",
                "isVisible": true,
                "right": "0px",
                "skin": "sknllbl485c75Lato13px",
                "text": "0/100",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBundleNameCharCount.add(lblBundleNameCharCount);
            flxBundleName.add(lblBundleNameHeader, lblBundleName, txtBundleName, flxNoBundleNameError, flxBundleNameCharCount);
            var flxAppId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "105px",
                "id": "flxAppId",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxAppId.setDefaultUnit(kony.flex.DP);
            var lblAppIdHeader = new kony.ui.Label({
                "height": "16px",
                "id": "lblAppIdHeader",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblConfigurationBundlesNormal",
                "text": "App ID",
                "top": "20px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAppId = new kony.ui.Label({
                "height": "16px",
                "id": "lblAppId",
                "isVisible": false,
                "left": "10px",
                "skin": "sknlblConfigurationBundlesNormal",
                "text": "App ID",
                "top": "50px",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAppId = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40px",
                "id": "txtAppId",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10px",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.appId\")",
                "right": "20px",
                "secureTextEntry": false,
                "skin": "skntbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "45px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var lblFonticonBundleEdit = new kony.ui.Label({
                "id": "lblFonticonBundleEdit",
                "isVisible": true,
                "right": "17px",
                "skin": "sknIcomoonCurrencySymbol",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                "top": "17px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknIcomoonCurrencySymbolHover",
                "toolTip": "Edit Bundle details"
            });
            var flxNoAppIdError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoAppIdError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90px",
                "width": "150px",
                "zIndex": 1
            }, {}, {});
            flxNoAppIdError.setDefaultUnit(kony.flex.DP);
            var lblNoAppIdErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoAppIdErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoAppIdError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoAppIdError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.enterAppID\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoAppIdError.add(lblNoAppIdErrorIcon, lblNoAppIdError);
            flxAppId.add(lblAppIdHeader, lblAppId, txtAppId, lblFonticonBundleEdit, flxNoAppIdError);
            var flxConfigDetailsSeparatorH = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxConfigDetailsSeparatorH",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxConfigurationBundlesFillLightGrey",
                "top": "105px",
                "zIndex": 1
            }, {}, {});
            flxConfigDetailsSeparatorH.setDefaultUnit(kony.flex.DP);
            flxConfigDetailsSeparatorH.add();
            var flxConfigDetailsSeparatorV = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxConfigDetailsSeparatorV",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "45%",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxConfigurationBundlesFillLightGrey",
                "top": "20px",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxConfigDetailsSeparatorV.setDefaultUnit(kony.flex.DP);
            flxConfigDetailsSeparatorV.add();
            var flxConfigurations = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55px",
                "id": "flxConfigurations",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "105px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxConfigurations.setDefaultUnit(kony.flex.DP);
            var lblConfigurations = new kony.ui.Label({
                "centerY": "50%",
                "height": "16px",
                "id": "lblConfigurations",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblConfigurationBundlesNormal",
                "text": "Configurations",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblConfigurationsOptional = new kony.ui.Label({
                "centerY": "50%",
                "height": "16px",
                "id": "lblConfigurationsOptional",
                "isVisible": true,
                "left": "115px",
                "skin": "sknlblConfigurationBundleLightGrey",
                "text": "(Optional)",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddConfigurationButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxAddConfigurationButton",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "183px",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffop100Border424242Radius100px",
                "width": "130px",
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationButton.setDefaultUnit(kony.flex.DP);
            var lblAddConfigurationButton = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "46%",
                "id": "lblAddConfigurationButton",
                "isVisible": true,
                "skin": "sknlblConfigurationBundlesNormal",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.addConfiguration\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddConfigurationButton.add(lblAddConfigurationButton);
            var flxConfigurationSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "35px",
                "id": "flxConfigurationSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxConfigBundlesSearchNormal",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxConfigurationSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImg",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxConfigurationSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxConfigurationSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30px",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.searchConfigurationPlaceholder\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearSearchImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearSearchImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearchImage.setDefaultUnit(kony.flex.DP);
            var fontIconCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearchImage.add(fontIconCross);
            flxConfigurationSearchContainer.add(fontIconSearchImg, tbxConfigurationSearchBox, flxClearSearchImage);
            var flxConfigDetailsSeparatorVertical2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxConfigDetailsSeparatorVertical2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxConfigurationBundlesFillLightGrey",
                "zIndex": 1
            }, {}, {});
            flxConfigDetailsSeparatorVertical2.setDefaultUnit(kony.flex.DP);
            flxConfigDetailsSeparatorVertical2.add();
            flxConfigurations.add(lblConfigurations, lblConfigurationsOptional, flxAddConfigurationButton, flxConfigurationSearchContainer, flxConfigDetailsSeparatorVertical2);
            var flxAddConfiguration = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "400px",
                "id": "flxAddConfiguration",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "180px",
                "zIndex": 1
            }, {}, {});
            flxAddConfiguration.setDefaultUnit(kony.flex.DP);
            var flxConfigurationEmpty = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxConfigurationEmpty",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxConfigurationEmpty.setDefaultUnit(kony.flex.DP);
            var lblConfigurationEmpty = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblConfigurationEmpty",
                "isVisible": true,
                "skin": "sknlblConfigurationBundleGrey",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.clickOnConfigurationMessage\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxConfigurationEmpty.add(lblConfigurationEmpty);
            var flxConfigurationData = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxConfigurationData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxConfigurationData.setDefaultUnit(kony.flex.DP);
            var configurationData = new com.adminConsole.configurationBundle.configurationData({
                "clipBounds": true,
                "height": "100%",
                "id": "configurationData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "configurationData": {
                        "height": "100%",
                        "isVisible": true
                    },
                    "flxTableView": {
                        "height": "360px",
                        "isVisible": true
                    },
                    "imgNextArrow": {
                        "src": "left_cricle.png"
                    },
                    "imgPreviousArrow": {
                        "src": "rightarrow_circel2x.png"
                    },
                    "segConfiguration": {
                        "height": "100%",
                        "left": "0dp",
                        "top": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var contextualMenu1 = new com.adminConsole.common.contextualMenu1({
                "clipBounds": true,
                "height": "70px",
                "id": "contextualMenu1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "10px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "60px",
                "width": "150px",
                "overrides": {
                    "btnLink1": {
                        "isVisible": false
                    },
                    "btnLink2": {
                        "isVisible": false
                    },
                    "contextualMenu1": {
                        "height": "70px",
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "10px",
                        "top": "60px",
                        "width": "150px"
                    },
                    "flxOption1": {
                        "isVisible": false
                    },
                    "flxOption2": {
                        "height": "35px"
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOption4": {
                        "height": "35px"
                    },
                    "flxOptionsSeperator": {
                        "isVisible": false
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false
                    },
                    "lblIconOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")"
                    },
                    "lblIconOption4": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")"
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.edit\")"
                    },
                    "lblOption4": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.delete\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxConfigTypeFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxConfigTypeFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "32px",
                "width": "13%",
                "zIndex": 10
            }, {}, {});
            flxConfigTypeFilter.setDefaultUnit(kony.flex.DP);
            var configTypeFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "configTypeFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "flxArrowImage": {
                        "height": "12px",
                        "left": "0px",
                        "top": "0px"
                    },
                    "flxChechboxOuter": {
                        "left": "0px",
                        "top": "11px"
                    },
                    "imgUpArrow": {
                        "height": "12px",
                        "right": "15px",
                        "src": "uparrow_2x.png",
                        "top": "0px",
                        "width": "15px"
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Preference"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Image/Icon"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Skin"
                        }],
                        "left": "0px",
                        "top": "9px"
                    },
                    "statusFilterMenu": {
                        "left": "0px",
                        "top": "5px",
                        "zIndex": 10
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxConfigTypeFilter.add(configTypeFilterMenu);
            var flxConfigTargetFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxConfigTargetFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "72%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "32px",
                "width": "13%",
                "zIndex": 10
            }, {}, {});
            flxConfigTargetFilter.setDefaultUnit(kony.flex.DP);
            var configTargetFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "configTargetFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "flxArrowImage": {
                        "height": "12px",
                        "left": "0px",
                        "top": "0px"
                    },
                    "flxChechboxOuter": {
                        "left": "0px",
                        "top": "11px"
                    },
                    "imgUpArrow": {
                        "height": "12px",
                        "right": "15px",
                        "src": "uparrow_2x.png",
                        "top": "0px",
                        "width": "15px"
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Client"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Server"
                        }],
                        "left": "0px",
                        "top": "9px"
                    },
                    "statusFilterMenu": {
                        "left": "0px",
                        "top": "5px",
                        "zIndex": 10
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxConfigTargetFilter.add(configTargetFilterMenu);
            var rtxNoResultsFound = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15px",
                "id": "rtxNoResultsFound",
                "isVisible": false,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.rtxMsgActivityHistory\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxConfigurationData.add(configurationData, contextualMenu1, flxConfigTypeFilter, flxConfigTargetFilter, rtxNoResultsFound);
            flxAddConfiguration.add(flxConfigurationEmpty, flxConfigurationData);
            flxAddConfigurationBundlesWrapper.add(flxBundleName, flxAppId, flxConfigDetailsSeparatorH, flxConfigDetailsSeparatorV, flxConfigurations, flxAddConfiguration);
            flxAddConfigurationBundlesBody.add(flxAddConfigurationBundlesWrapper);
            var flxAddConfigurationBundlesButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "81px",
                "id": "flxAddConfigurationBundlesButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "CopyslFbox0dcf99bf67f1e4c",
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationBundlesButtons.setDefaultUnit(kony.flex.DP);
            var flxAddConfigurationBundlesSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddConfigurationBundlesSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxConfigurationBundlesFillLightGrey",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationBundlesSeparator.setDefaultUnit(kony.flex.DP);
            flxAddConfigurationBundlesSeparator.add();
            var btnConfigurationBundlesAdd = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px",
                "height": "40px",
                "id": "btnConfigurationBundlesAdd",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "text": "ADD",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnConfigurationBundlesCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnConfigurationBundlesWhite",
                "height": "40px",
                "id": "btnConfigurationBundlesCancel",
                "isVisible": true,
                "right": "140px",
                "skin": "sknBtnConfigurationBundlesWhite",
                "text": "CANCEL",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddConfigurationBundlesButtons.add(flxAddConfigurationBundlesSeparator, btnConfigurationBundlesAdd, btnConfigurationBundlesCancel);
            flxAddConfigurationBundles.add(flxAddConfigurationBundlesBody, flxAddConfigurationBundlesButtons);
            var flxNoConfigurationBundles = new kony.ui.FlexContainer({
                "bottom": "0px",
                "clipBounds": true,
                "height": "350px",
                "id": "flxNoConfigurationBundles",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "10px",
                "zIndex": 1
            }, {}, {});
            flxNoConfigurationBundles.setDefaultUnit(kony.flex.DP);
            var noStaticData = new com.adminConsole.staticContent.noStaticData({
                "clipBounds": true,
                "height": "350px",
                "id": "noStaticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "btnAddStaticContent": {
                        "maxWidth": "218px",
                        "text": "ADD BUNDLE",
                        "width": "218px"
                    },
                    "lblNoStaticContentCreated": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.noConfigurationBundles\")"
                    },
                    "lblNoStaticContentMsg": {
                        "text": "Click on \"ADD BUNDLE\" to continue."
                    },
                    "noStaticData": {
                        "centerX": "viz.val_cleared",
                        "height": "350px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoConfigurationBundles.add(noStaticData);
            var flxViewConfigurationBundles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "700px",
                "id": "flxViewConfigurationBundles",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxViewConfigurationBundles.setDefaultUnit(kony.flex.DP);
            var flxViewConfigurationBundlesTopBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewConfigurationBundlesTopBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewConfigurationBundlesTopBar.setDefaultUnit(kony.flex.DP);
            var lblBundleSortByHeader = new kony.ui.Label({
                "centerY": "50%",
                "height": "16px",
                "id": "lblBundleSortByHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblConfigurationBundlesNormal",
                "text": "Sort By",
                "top": "0px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxBundleSortByOptions = new kony.ui.ListBox({
                "centerY": "50%",
                "focusSkin": "defListBoxFocus",
                "height": "40px",
                "id": "lbxBundleSortByOptions",
                "isVisible": true,
                "left": "60px",
                "masterData": [
                    ["bundleName", "Bundle Name"],
                    ["bundleAppId", "App ID"]
                ],
                "right": "10px",
                "skin": "sknlbxConfigurationBundles",
                "top": "0px",
                "width": "180px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxSearchBundleContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "35px",
                "id": "flxSearchBundleContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxd5d9ddop100",
                "top": "0px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxSearchBundleContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearchBundleImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchBundleImg",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxBundleSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxBundleSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30px",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.searchBundlePlaceholder\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearBundleSearchImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearBundleSearchImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearBundleSearchImage.setDefaultUnit(kony.flex.DP);
            var fontIconBundleCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconBundleCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearBundleSearchImage.add(fontIconBundleCross);
            flxSearchBundleContainer.add(fontIconSearchBundleImg, tbxBundleSearchBox, flxClearBundleSearchImage);
            flxViewConfigurationBundlesTopBar.add(lblBundleSortByHeader, lbxBundleSortByOptions, flxSearchBundleContainer);
            var flxViewConfigurationBundleBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "630px",
                "id": "flxViewConfigurationBundleBody",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewConfigurationBundleBody.setDefaultUnit(kony.flex.DP);
            var flxViewConfigurationBundleBodyScroll = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxViewConfigurationBundleBodyScroll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0px",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewConfigurationBundleBodyScroll.setDefaultUnit(kony.flex.DP);
            var flxViewConfigurationBundleEntries = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewConfigurationBundleEntries",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewConfigurationBundleEntries.setDefaultUnit(kony.flex.DP);
            flxViewConfigurationBundleEntries.add();
            flxViewConfigurationBundleBodyScroll.add(flxViewConfigurationBundleEntries);
            var flxNoBundleFound = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxNoBundleFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoBundleFound.setDefaultUnit(kony.flex.DP);
            var rtxNoBundlesFound = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15px",
                "id": "rtxNoBundlesFound",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.noBundlesFound\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBundleFound.add(rtxNoBundlesFound);
            flxViewConfigurationBundleBody.add(flxViewConfigurationBundleBodyScroll, flxNoBundleFound);
            flxViewConfigurationBundles.add(flxViewConfigurationBundlesTopBar, flxViewConfigurationBundleBody);
            flxMainContent.add(flxAddConfigurationBundles, flxNoConfigurationBundles, flxViewConfigurationBundles);
            flxRightPanel.add(flxMainHeader, flxMainContent);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxMain.add(flxLeftPannel, flxRightPanel, flxToastMessage, flxHeaderDropdown, flxLoading);
            var flxAddConfigurationPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddConfigurationPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "CopyslFbox0ie07b6077b734c",
                "top": 0,
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxAddConfigurationPopUp.setDefaultUnit(kony.flex.DP);
            var flxAddConfigurationPopUpMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "505px",
                "id": "flxAddConfigurationPopUpMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "CopyslFbox0a55815af731d49",
                "top": 0,
                "width": "700px",
                "zIndex": 10
            }, {}, {});
            flxAddConfigurationPopUpMain.setDefaultUnit(kony.flex.DP);
            var flxAddConfigurationPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAddConfigurationPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "CopyslFbox0f849449508464e",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxAddConfigurationPopUpTopColor.add();
            var flxAddConfigurationPopUpHeader = new kony.ui.Label({
                "height": "25px",
                "id": "flxAddConfigurationPopUpHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.addConfiguration\")",
                "top": "35px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddConfigurationClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAddConfigurationClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "15px",
                "skin": "slFbox",
                "top": "25px",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAddConfigurationClose.setDefaultUnit(kony.flex.DP);
            var fontIconAddConfigurationClose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconAddConfigurationClose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddConfigurationClose.add(fontIconAddConfigurationClose);
            var flxAddConfigurationType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxAddConfigurationType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75px",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationType.setDefaultUnit(kony.flex.DP);
            var lblAddConfigurationType = new kony.ui.Label({
                "height": "16px",
                "id": "lblAddConfigurationType",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblConfigurationBundlesNormal",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.type\")",
                "top": "15px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxAddConfigurationType = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40px",
                "id": "lbxAddConfigurationType",
                "isVisible": true,
                "left": "20px",
                "masterData": [
                    ["PREFERENCE", "Preference"],
                    ["IMAGE/ICON", "Image/Icon"],
                    ["SKIN", "Skin"]
                ],
                "right": "10px",
                "skin": "sknlbxConfigurationBundles",
                "top": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxAddConfigurationType.add(lblAddConfigurationType, lbxAddConfigurationType);
            var flxAddConfigurationKey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxAddConfigurationKey",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75px",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationKey.setDefaultUnit(kony.flex.DP);
            var lblAddConfigurationKey = new kony.ui.Label({
                "height": "16px",
                "id": "lblAddConfigurationKey",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblConfigurationBundlesNormal",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.key\")",
                "top": "15px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAddConfigurationKey = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40px",
                "id": "txtAddConfigurationKey",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "20px",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.key\")",
                "right": "10px",
                "secureTextEntry": false,
                "skin": "skntbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "40px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var flxNoKeyError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoKeyError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "85px",
                "width": "150px",
                "zIndex": 1
            }, {}, {});
            flxNoKeyError.setDefaultUnit(kony.flex.DP);
            var lblNoKeyErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoKeyErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoKeyError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoKeyError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.enterKey\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoKeyError.add(lblNoKeyErrorIcon, lblNoKeyError);
            flxAddConfigurationKey.add(lblAddConfigurationKey, txtAddConfigurationKey, flxNoKeyError);
            var flxAddConfigurationValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxAddConfigurationValue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75px",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationValue.setDefaultUnit(kony.flex.DP);
            var lblAddConfigurationValue = new kony.ui.Label({
                "height": "16px",
                "id": "lblAddConfigurationValue",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblConfigurationBundlesNormal",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.value\")",
                "top": "15px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAddConfigurationValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40px",
                "id": "txtAddConfigurationValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "20px",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.value\")",
                "right": "20px",
                "secureTextEntry": false,
                "skin": "skntbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "40px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var flxNoValueError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoValueError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "85px",
                "width": "150px",
                "zIndex": 1
            }, {}, {});
            flxNoValueError.setDefaultUnit(kony.flex.DP);
            var lblNoValueErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoValueErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoValueError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoValueError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.enterValue\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoValueError.add(lblNoValueErrorIcon, lblNoValueError);
            flxAddConfigurationValue.add(lblAddConfigurationValue, txtAddConfigurationValue, flxNoValueError);
            var flxAddConfigurationTarget = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxAddConfigurationTarget",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "175px",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationTarget.setDefaultUnit(kony.flex.DP);
            var lblAddConfigurationTarget = new kony.ui.Label({
                "height": "16px",
                "id": "lblAddConfigurationTarget",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblConfigurationBundlesNormal",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.target\")",
                "top": "15px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxConfigurationTargetRadio = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxConfigurationTargetRadio",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "45px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxConfigurationTargetRadio.setDefaultUnit(kony.flex.DP);
            var flxRadioClient = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadioClient",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioClient.setDefaultUnit(kony.flex.DP);
            var imgRadioClient = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadioClient",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioClient.add(imgRadioClient);
            var lblClient = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblClient",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "Client",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRadioServer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadioServer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioServer.setDefaultUnit(kony.flex.DP);
            var imgRadioServer = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadioServer",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioServer.add(imgRadioServer);
            var lblServer = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblServer",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "Server",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxConfigurationTargetRadio.add(flxRadioClient, lblClient, flxRadioServer, lblServer);
            flxAddConfigurationTarget.add(lblAddConfigurationTarget, flxConfigurationTargetRadio);
            var flxAddConfigurationDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "155px",
                "id": "flxAddConfigurationDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "255px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationDescription.setDefaultUnit(kony.flex.DP);
            var lblAddConfigurationDescription = new kony.ui.Label({
                "height": "16px",
                "id": "lblAddConfigurationDescription",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblConfigurationBundlesNormal",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.description\")",
                "top": "15px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAreaAddConfigurationDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextAreaFocus",
                "height": "95px",
                "id": "txtAreaAddConfigurationDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "20px",
                "numberOfVisibleLines": 3,
                "placeholder": "Description",
                "right": "20px",
                "skin": "skntbxAreaConfigurationBundlesNormal",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextAreaPlaceholder"
            });
            var flxNoDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "140px",
                "width": "150px",
                "zIndex": 1
            }, {}, {});
            flxNoDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoDescriptionErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoDescriptionErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoDescriptionError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoDescriptionError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.enterDescription\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoDescriptionError.add(lblNoDescriptionErrorIcon, lblNoDescriptionError);
            var flxDescriptionCharCount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "16px",
                "id": "flxDescriptionCharCount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30px",
                "skin": "slFbox",
                "top": "15px",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxDescriptionCharCount.setDefaultUnit(kony.flex.DP);
            var lblDescriptionCharCount = new kony.ui.Label({
                "id": "lblDescriptionCharCount",
                "isVisible": true,
                "right": "0px",
                "skin": "sknllbl485c75Lato13px",
                "text": "0/1000",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescriptionCharCount.add(lblDescriptionCharCount);
            flxAddConfigurationDescription.add(lblAddConfigurationDescription, txtAreaAddConfigurationDescription, flxNoDescriptionError, flxDescriptionCharCount);
            var flxAddConfigurationButtons = new kony.ui.FlexContainer({
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxAddConfigurationButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxConfigBundlesFillVLightGrey",
                "zIndex": 1
            }, {}, {});
            flxAddConfigurationButtons.setDefaultUnit(kony.flex.DP);
            var btnAddConfigurationPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnConfigurationBundlesWhite",
                "height": "40px",
                "id": "btnAddConfigurationPopUpCancel",
                "isVisible": true,
                "right": "140px",
                "skin": "sknBtnConfigurationBundlesWhite",
                "text": "CANCEL",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddConfigurationPopUpAdd = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px",
                "height": "40px",
                "id": "btnAddConfigurationPopUpAdd",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "text": "ADD",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddConfigurationButtons.add(btnAddConfigurationPopUpCancel, btnAddConfigurationPopUpAdd);
            var lblConfigurationId = new kony.ui.Label({
                "height": "16px",
                "id": "lblConfigurationId",
                "isVisible": false,
                "left": "20px",
                "skin": "sknlblConfigurationBundlesNormal",
                "top": "260px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddConfigurationPopUpMain.add(flxAddConfigurationPopUpTopColor, flxAddConfigurationPopUpHeader, flxAddConfigurationClose, flxAddConfigurationType, flxAddConfigurationKey, flxAddConfigurationValue, flxAddConfigurationTarget, flxAddConfigurationDescription, flxAddConfigurationButtons, lblConfigurationId);
            flxAddConfigurationPopUp.add(flxAddConfigurationPopUpMain);
            var flxDeleteBundleOrConfiguration = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeleteBundleOrConfiguration",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDeleteBundleOrConfiguration.setDefaultUnit(kony.flex.DP);
            var popUpDelete = new com.adminConsole.common.popUp1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUpDelete",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "150px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDelete\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.deleteBundleHeader\")"
                    },
                    "popUp1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure to delete the selected bundle?"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeleteBundleOrConfiguration.add(popUpDelete);
            var flxPopUpWarning = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpWarning",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0db4ac791cac04d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpWarning.setDefaultUnit(kony.flex.DP);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0e35d4d07b2054b",
                "top": "250px",
                "width": "600px",
                "zIndex": 1
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var flxTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxee6565Op100NoBorder",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopColor.setDefaultUnit(kony.flex.DP);
            flxTopColor.add();
            var flxPopUpMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "182px",
                "id": "flxPopUpMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0cd4d17dca3e64b",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpMain.setDefaultUnit(kony.flex.DP);
            var lblBundleNameExistsHeader = new kony.ui.Label({
                "id": "lblBundleNameExistsHeader",
                "isVisible": true,
                "left": "3%",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Bundle name cannot be created",
                "top": "35px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBundleNameExistsBody1 = new kony.ui.Label({
                "height": "35px",
                "id": "lblBundleNameExistsBody1",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblConfigurationBundlesNormal",
                "text": "The bundle name entered is matching with an already existing bundle. Hence it is not allowed to create the bundle with the same name.",
                "top": "75px",
                "width": "560px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxBundleNameExistsBody2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxBundleNameExistsBody2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "120px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxBundleNameExistsBody2.setDefaultUnit(kony.flex.DP);
            var lblBundleNameExistsBody2Left = new kony.ui.Label({
                "height": "15px",
                "id": "lblBundleNameExistsBody2Left",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblConfigurationBundlesNormal",
                "text": "Click on \"",
                "top": "0px",
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnBundleNameExistsClick = new kony.ui.Button({
                "height": "15px",
                "id": "btnBundleNameExistsClick",
                "isVisible": true,
                "left": "0px",
                "skin": "sknBtnConfigBundle",
                "text": "Retail Banking",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBundleNameExistsBody2Right = new kony.ui.Label({
                "height": "15px",
                "id": "lblBundleNameExistsBody2Right",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblConfigurationBundlesNormal",
                "text": "\" to open the matched bundle.",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBundleNameExistsBody2.add(lblBundleNameExistsBody2Left, btnBundleNameExistsClick, lblBundleNameExistsBody2Right);
            var flxPopUpWarningClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpWarningClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxPopUpWarningClose.setDefaultUnit(kony.flex.DP);
            var fontIconImgWarning = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgWarning",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpWarningClose.add(fontIconImgWarning);
            var lblPopUpBundleId = new kony.ui.Label({
                "height": "15px",
                "id": "lblPopUpBundleId",
                "isVisible": false,
                "left": "20px",
                "skin": "sknlblConfigurationBundlesNormal",
                "top": "150px",
                "width": "560px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpMain.add(lblBundleNameExistsHeader, lblBundleNameExistsBody1, flxBundleNameExistsBody2, flxPopUpWarningClose, lblPopUpBundleId);
            var flxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0gf3cede0eb1c4f",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnOk = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLato12Pxee6565Radius25Px",
                "height": "40px",
                "id": "btnOk",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtnConfigBundleOkRed",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.OK\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxPopUpButtons.add(btnOk);
            flxPopUp.add(flxTopColor, flxPopUpMain, flxPopUpButtons);
            flxPopUpWarning.add(flxPopUp);
            this.add(flxMain, flxAddConfigurationPopUp, flxDeleteBundleOrConfiguration, flxPopUpWarning);
        };
        return [{
            "addWidgets": addWidgetsfrmConfigurationBundles,
            "enabledForIdleTimeout": true,
            "id": "frmConfigurationBundles",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_d803ec9f86eb477381027c0071831550(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_j4d9db03c2e84581b795e8d3c8f6a67a,
            "retainScrollPosition": false
        }]
    }
});