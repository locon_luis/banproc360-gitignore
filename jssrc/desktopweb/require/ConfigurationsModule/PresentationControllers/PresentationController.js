define(['Promisify', 'ErrorInterceptor'], function(Promisify, ErrorInterceptor) {
    /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
    function PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.configModel = {
            criteriaList: []
        };
    }
    inheritsFrom(PresentationController, kony.mvc.Presentation.BasePresenter);
    /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
    PresentationController.prototype.initializePresentationController = function() {};
    PresentationController.prototype.fetchEligibilityCriteria = function() {
        var self = this;
        self.presentUserInterface('frmBusinessConfigurations', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmBusinessConfigurations', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.configModel.criteriaList = response.eligibilitycriteria;
            self.presentUserInterface("frmBusinessConfigurations", self.configModel);
        }

        function failureCallback(error) {
            self.presentUserInterface('frmBusinessConfigurations', {
                "LoadingScreen": {
                    focus: false
                }
            });
            var context = "Error";
            self.presentUserInterface('frmBusinessConfigurations', {
                "toast": {
                    "status": "ERROR",
                    "message": ErrorInterceptor.errorMessage(error)
                }
            });
            self.presentUserInterface("frmBusinessConfigurations", context);
        }
        this.businessController.fetchEligibilityCriterias({}, successCallback, failureCallback);
        this.presentUserInterface("frmBusinessConfigurations", {});
    };
    PresentationController.prototype.deleteEligibilityCriteria = function(command) {
        var self = this;
        self.presentUserInterface('frmBusinessConfigurations', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmBusinessConfigurations', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmBusinessConfigurations', {
                "toast": {
                    "status": "SUCCESS",
                    "message": kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.deleteCriteriaSuccess")
                }
            });
            self.fetchEligibilityCriteria();
        }

        function failureCallback(error) {
            self.presentUserInterface('frmBusinessConfigurations', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmBusinessConfigurations', {
                "toastMessage": {
                    "status": "ERROR",
                    "message": ErrorInterceptor.errorMessage(error)
                }
            });
            self.fetchEligibilityCriteria();
        }
        this.businessController.deleteEligibilityCriteria({
            "id": command
        }, successCallback, failureCallback);
        this.presentUserInterface("frmBusinessConfigurations", {});
    };
    PresentationController.prototype.updateEligibilityCriteriaStatus = function(command) {
        var self = this;
        self.presentUserInterface('frmBusinessConfigurations', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmBusinessConfigurations', {
                "LoadingScreen": {
                    focus: false
                }
            });
            if (command.status_id === "SID_INACTIVE") self.presentUserInterface('frmBusinessConfigurations', {
                "toast": {
                    "status": "SUCCESS",
                    "message": kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.deactivateCriteriaSuccess")
                }
            });
            else self.presentUserInterface('frmBusinessConfigurations', {
                "toast": {
                    "status": "SUCCESS",
                    "message": kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.activateCriteriaSuccess")
                }
            });
            self.fetchEligibilityCriteria();
        }

        function failureCallback(error) {
            self.presentUserInterface('frmBusinessConfigurations', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmBusinessConfigurations', {
                "toast": {
                    "status": "ERROR",
                    "message": ErrorInterceptor.errorMessage(error)
                }
            });
            self.fetchEligibilityCriteria();
        }
        this.businessController.updateEligibilityCriteria(command, successCallback, failureCallback);
        this.presentUserInterface("frmBusinessConfigurations", {});
    };
    PresentationController.prototype.addEligibilityCriteria = function(input) {
        var self = this;
        self.presentUserInterface('frmBusinessConfigurations', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmBusinessConfigurations', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmBusinessConfigurations', {
                "toast": {
                    "status": "SUCCESS",
                    "message": kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.addCriteriaSuccess")
                }
            });
            self.fetchEligibilityCriteria();
        }

        function failureCallback(error) {
            self.presentUserInterface('frmBusinessConfigurations', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmBusinessConfigurations', {
                "toast": {
                    "status": "ERROR",
                    "message": ErrorInterceptor.errorMessage(error)
                }
            });
            self.presentUserInterface("frmBusinessConfigurations", {});
        }
        this.businessController.addEligibilityCriterias(input, successCallback, failureCallback);
        this.presentUserInterface("frmBusinessConfigurations", {});
    };
    PresentationController.prototype.updateEligibilityCriteria = function(command) {
        var self = this;
        self.presentUserInterface('frmBusinessConfigurations', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmBusinessConfigurations', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmBusinessConfigurations', {
                "toast": {
                    "status": "SUCCESS",
                    "message": kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.updateCriteriaSuccess")
                }
            });
            self.fetchEligibilityCriteria();
        }

        function failureCallback(error) {
            self.presentUserInterface('frmBusinessConfigurations', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmBusinessConfigurations', {
                "toast": {
                    "status": "ERROR",
                    "message": ErrorInterceptor.errorMessage(error)
                }
            });
            self.presentUserInterface("frmBusinessConfigurations", {});
        }
        this.businessController.updateEligibilityCriteria(command, successCallback, failureCallback);
        this.presentUserInterface("frmBusinessConfigurations", {});
    };
    PresentationController.prototype.fetchBundles = function() {
        var self = this;
        self.presentUserInterface('frmConfigurationBundles', {
            "LoadingScreen": {
                focus: true
            }
        });
        var promiseFetchAllBundles = Promisify(this.businessController, 'fetchAllBundles');
        var promiseFetchAllConfigurations = Promisify(this.businessController, 'fetchAllConfigurations');
        Promise.all([
            promiseFetchAllBundles({}),
            promiseFetchAllConfigurations({})
        ]).then(function(responses) {
            self.presentUserInterface('frmConfigurationBundles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmConfigurationBundles', {
                "fetchAllBundlesAndConfigurationsStatus": "success",
                "bundles": responses[0].bundles,
                "configurations": responses[1].configurations
            });
        }).catch(function(error) {
            self.presentUserInterface('frmConfigurationBundles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmConfigurationBundles', {
                "fetchAllBundlesAndConfigurationsStatus": "error"
            });
        });
    };
    PresentationController.prototype.fetchConfigurations = function(bundleId) {
        var self = this;
        self.presentUserInterface('frmConfigurationBundles', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmConfigurationBundles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmConfigurationBundles', {
                "fetchConfigurationsStatus": "success",
                "configurations": response.configurations
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmConfigurationBundles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmConfigurationBundles', {
                "fetchConfigurationsStatus": "error"
            });
        }
        this.businessController.fetchConfigurations(bundleId, successCallback, failureCallback);
    };
    PresentationController.prototype.addBundleAndConfigurations = function(bundle) {
        var self = this;
        self.presentUserInterface('frmConfigurationBundles', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmConfigurationBundles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmConfigurationBundles', {
                "addBundleAndConfigurationsStatus": "success"
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmConfigurationBundles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmConfigurationBundles', {
                "addBundleAndConfigurationsStatus": "failure"
            });
        }
        this.businessController.manageBundleAndConfigurations(bundle, successCallback, failureCallback);
    };
    PresentationController.prototype.editBundleAndConfigurations = function(bundle) {
        var self = this;
        self.presentUserInterface('frmConfigurationBundles', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmConfigurationBundles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmConfigurationBundles', {
                "editBundleAndConfigurationsStatus": "success"
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmConfigurationBundles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmConfigurationBundles', {
                "editBundleAndConfigurationsStatus": "failure"
            });
        }
        this.businessController.manageBundleAndConfigurations(bundle, successCallback, failureCallback);
    };
    PresentationController.prototype.deleteBundle = function(bundleId) {
        var self = this;
        self.presentUserInterface('frmConfigurationBundles', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmConfigurationBundles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmConfigurationBundles', {
                "deleteBundleStatus": "success",
                "bundleId": bundleId
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmConfigurationBundles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmConfigurationBundles', {
                "deleteBundleStatus": "failure"
            });
        }
        this.businessController.deleteBundleAndConfigurations(bundleId, successCallback, failureCallback);
    };
    PresentationController.prototype.deleteConfiguration = function(configurationId) {
        var self = this;
        self.presentUserInterface('frmConfigurationBundles', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmConfigurationBundles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmConfigurationBundles', {
                "deleteConfigurationStatus": "success",
                "configurationId": configurationId.configurationId
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmConfigurationBundles', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface('frmConfigurationBundles', {
                "deleteConfigurationStatus": "failure"
            });
        }
        this.businessController.deleteBundleAndConfigurations(configurationId, successCallback, failureCallback);
    };
    return PresentationController;
});