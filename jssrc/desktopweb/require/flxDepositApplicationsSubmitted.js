define("flxDepositApplicationsSubmitted", function() {
    return function(controller) {
        var flxDepositApplicationsSubmitted = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDepositApplicationsSubmitted",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDepositApplicationsSubmitted.setDefaultUnit(kony.flex.DP);
        var flxSubmittedApplications = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxSubmittedApplications",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSubmittedApplications.setDefaultUnit(kony.flex.DP);
        var lblDepositApplIdSubmit = new kony.ui.Label({
            "id": "lblDepositApplIdSubmit",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "4354545465656767",
            "top": "15dp",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDepositApplProductSubmit = new kony.ui.Label({
            "id": "lblDepositApplProductSubmit",
            "isVisible": true,
            "left": "22%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Club Saving Account",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDepositApplCreatedStarted = new kony.ui.Label({
            "id": "lblDepositApplCreatedStarted",
            "isVisible": true,
            "left": "43%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "25-05-2019",
            "top": "15dp",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDepositApplSubmitOn = new kony.ui.Label({
            "id": "lblDepositApplSubmitOn",
            "isVisible": true,
            "left": "61%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "29-05-2019",
            "top": "15dp",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDepositApplStatusSubmit = new kony.ui.Label({
            "id": "lblDepositApplStatusSubmit",
            "isVisible": true,
            "right": "35dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Approval Pending",
            "top": "15dp",
            "width": "18%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSubmittedApplications.add(lblDepositApplIdSubmit, lblDepositApplProductSubmit, lblDepositApplCreatedStarted, lblDepositApplSubmitOn, lblDepositApplStatusSubmit);
        var lblStartedSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblStartedSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": 20,
            "skin": "sknlblSeperator",
            "text": "Label",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDepositApplicationsSubmitted.add(flxSubmittedApplications, lblStartedSeperator);
        return flxDepositApplicationsSubmitted;
    }
})