define("userflxAddOptionController", function() {
    return {
        addService: function() {
            var lblOptionText = kony.application.getCurrentForm().addAndRemoveOptions.segAddOptions.selectedItems[0].lblName;
            var toAdd = {
                "imgClose": "close_blue.png",
                "lblOption": "" + lblOptionText
            };
            var data2 = kony.application.getCurrentForm().addAndRemoveOptions.segSelectedOptions.data;
            data2.push(toAdd);
            kony.application.getCurrentForm().addAndRemoveOptions.segSelectedOptions.setData(data2);
            kony.application.getCurrentForm().forceLayout();
            kony.print("called in template controller");
        }
    };
});
define("flxAddOptionControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnAdd **/
    AS_Button_d5a06f2ade8047b1894bb6670b26a421: function AS_Button_d5a06f2ade8047b1894bb6670b26a421(eventobject, context) {
        var self = this;
        this.addService();
    }
});
define("flxAddOptionController", ["userflxAddOptionController", "flxAddOptionControllerActions"], function() {
    var controller = require("userflxAddOptionController");
    var controllerActions = ["flxAddOptionControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
