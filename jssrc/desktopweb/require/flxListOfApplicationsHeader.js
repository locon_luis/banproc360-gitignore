define("flxListOfApplicationsHeader", function() {
    return function(controller) {
        var flxListOfApplicationsHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxListOfApplicationsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxListOfApplicationsHeader.setDefaultUnit(kony.flex.DP);
        var flxApplicationIDContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxApplicationIDContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxApplicationIDContainer.setDefaultUnit(kony.flex.DP);
        var lblApplicationIDHeader = new kony.ui.Label({
            "id": "lblApplicationIDHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl696c73LatoBold",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000"
        });
        var lblApplicationIDHeaderImg = new kony.ui.Label({
            "id": "lblApplicationIDHeaderImg",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblApplicationListIconSort",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxApplicationIDContainer.add(lblApplicationIDHeader, lblApplicationIDHeaderImg);
        var flxLoanTypeContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxLoanTypeContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "180dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxLoanTypeContainer.setDefaultUnit(kony.flex.DP);
        var lblLoanTypeHeader = new kony.ui.Label({
            "id": "lblLoanTypeHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl696c73LatoBold",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000"
        });
        var lblLoanTypeHeaderImg = new kony.ui.Label({
            "id": "lblLoanTypeHeaderImg",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblApplicationListIconSort",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLoanTypeContainer.add(lblLoanTypeHeader, lblLoanTypeHeaderImg);
        var flxAmountContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxAmountContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "350dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxAmountContainer.setDefaultUnit(kony.flex.DP);
        var lblAmountHeader = new kony.ui.Label({
            "id": "lblAmountHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl696c73LatoBold",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000"
        });
        var lblAmountHeaderImg = new kony.ui.Label({
            "id": "lblAmountHeaderImg",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblApplicationListIconSort",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAmountContainer.add(lblAmountHeader, lblAmountHeaderImg);
        var flxCreateOnContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxCreateOnContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "520dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxCreateOnContainer.setDefaultUnit(kony.flex.DP);
        var lblCreateOnHeader = new kony.ui.Label({
            "id": "lblCreateOnHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl696c73LatoBold",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000"
        });
        var lblCreateOnheaderImg = new kony.ui.Label({
            "id": "lblCreateOnheaderImg",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblApplicationListIconSort",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCreateOnContainer.add(lblCreateOnHeader, lblCreateOnheaderImg);
        var flxModifyOnContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxModifyOnContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "690dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxModifyOnContainer.setDefaultUnit(kony.flex.DP);
        var lblModifyOnHeader = new kony.ui.Label({
            "id": "lblModifyOnHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl696c73LatoBold",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000"
        });
        var lblModifyOnHeaderImg = new kony.ui.Label({
            "id": "lblModifyOnHeaderImg",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblApplicationListIconSort",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxModifyOnContainer.add(lblModifyOnHeader, lblModifyOnHeaderImg);
        var lblSeparatorApplicationHeader = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeparatorApplicationHeader",
            "isVisible": true,
            "left": "10dp",
            "right": "22dp",
            "skin": "sknLblSeparator696C73",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxListOfApplicationsHeader.add(flxApplicationIDContainer, flxLoanTypeContainer, flxAmountContainer, flxCreateOnContainer, flxModifyOnContainer, lblSeparatorApplicationHeader);
        return flxListOfApplicationsHeader;
    }
})