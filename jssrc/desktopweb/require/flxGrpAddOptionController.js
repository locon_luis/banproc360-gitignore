define("userflxGrpAddOptionController", function() {
    return {
        addService: function() {
            var lblOptionText = kony.application.getCurrentForm().addAndRemoveOptions.segAddOptions.selectedItems[0].lblName;
            var toAdd = {
                "imgClose": "close_blue.png",
                "lblOption": "" + lblOptionText
            };
            var data2 = kony.application.getCurrentForm().addAndRemoveOptions.segSelectedOptions.data;
            data2.push(toAdd);
            kony.application.getCurrentForm().addAndRemoveOptions.segSelectedOptions.setData(data2);
            kony.application.getCurrentForm().forceLayout();
            kony.print("called in template controller");
        }
    };
});
define("flxGrpAddOptionControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnAdd **/
    AS_Button_f5a5f3781fa641cf9f3fdb8692a05a99: function AS_Button_f5a5f3781fa641cf9f3fdb8692a05a99(eventobject, context) {
        var self = this;
        this.addService();
    }
});
define("flxGrpAddOptionController", ["userflxGrpAddOptionController", "flxGrpAddOptionControllerActions"], function() {
    var controller = require("userflxGrpAddOptionController");
    var controllerActions = ["flxGrpAddOptionControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
