define("flxHeaderDashBoardAlert", function() {
    return function(controller) {
        var flxHeaderDashBoardAlert = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeaderDashBoardAlert",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxHeaderDashBoardAlert.setDefaultUnit(kony.flex.DP);
        var lblRecivedOn = new kony.ui.Label({
            "id": "lblRecivedOn",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl192b45LatoBold13px",
            "text": "Today",
            "top": "30px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeaderDashBoardAlert.add(lblRecivedOn);
        return flxHeaderDashBoardAlert;
    }
})