define(['ModelManager'], function(ModelManager) {
    function CampaignManager() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(CampaignManager, kony.mvc.Business.Delegator);
    CampaignManager.prototype.initializeBusinessController = function() {};
    CampaignManager.prototype.fetchCampaigns = function(context, onSuccess, onError) {
        ModelManager.invoke('campaign', 'getCampaigns', context, onSuccess, onError);
    };
    CampaignManager.prototype.fetchCampaignURLandGroups = function(context, onSuccess, onError) {
        ModelManager.invoke('campaign', 'getCampaignSpecificationsAndGroups', context, onSuccess, onError);
    };
    CampaignManager.prototype.getDefaultCampaign = function(context, onSuccess, onError) {
        ModelManager.invoke('defaultcampaign', 'getDefaultCampaign', context, onSuccess, onError);
    };
    CampaignManager.prototype.updateCampaignStatus = function(context, onSuccess, onError) {
        ModelManager.invoke('campaign', 'updateCampaignStatus', context, onSuccess, onError);
    };
    CampaignManager.prototype.fetchCampaignMasterData = function(context, onSuccess, onError) {
        ModelManager.invoke('channelandscreen', 'getChannelsAndScreensInformation', context, onSuccess, onError);
    };
    CampaignManager.prototype.fetchCampaignPriorities = function(context, onSuccess, onError) {
        ModelManager.invoke('campaign', 'getCampaignPriorityList', context, onSuccess, onError);
    };
    CampaignManager.prototype.createCampaign = function(context, onSuccess, onError) {
        ModelManager.invoke('campaign', 'createCampaign', context, onSuccess, onError);
    };
    CampaignManager.prototype.updateCampaign = function(context, onSuccess, onError) {
        ModelManager.invoke('campaign', 'updateCampaign', context, onSuccess, onError);
    };
    CampaignManager.prototype.getDefaultCampaign = function(context, onSuccess, onError) {
        ModelManager.invoke('defaultcampaign', 'getDefaultCampaign', context, onSuccess, onError);
    };
    CampaignManager.prototype.updateDefaultCampaign = function(context, onSuccess, onError) {
        ModelManager.invoke('defaultcampaign', 'updateDefaultCampaign', context, onSuccess, onError);
    };
    return CampaignManager;
});