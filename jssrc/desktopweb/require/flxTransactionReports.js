define("flxTransactionReports", function() {
    return function(controller) {
        var flxTransactionReports = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxTransactionReports",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxTransactionReports.setDefaultUnit(kony.flex.DP);
        var flxTransaction = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": false,
            "id": "flxTransaction",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknLblTabShadow2",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxTransaction.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, {}, {});
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxTransactionType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTransactionType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "40%",
            "zIndex": 1
        }, {}, {});
        flxTransactionType.setDefaultUnit(kony.flex.DP);
        var lblTransactionType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTransactionType",
            "isVisible": true,
            "left": 20,
            "skin": "sknReportsSegTransaction13PX",
            "text": "TRANSACTION TYPE",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTransactionType.add(lblTransactionType);
        var flxMobile = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMobile",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxMobile.setDefaultUnit(kony.flex.DP);
        var lblMobile = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblMobile",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "MOBILE",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMobile.add(lblMobile);
        var flxOnline = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxOnline",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxOnline.setDefaultUnit(kony.flex.DP);
        var lblOnline = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOnline",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "ONLINE",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOnline.add(lblOnline);
        var flxTotal = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTotal",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxTotal.setDefaultUnit(kony.flex.DP);
        var lblTotal = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTotal",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "TOTAL",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTotal.add(lblTotal);
        flxHeader.add(flxTransactionType, flxMobile, flxOnline, flxTotal);
        var flxValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "30px",
            "id": "flxValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, {}, {});
        flxValue.setDefaultUnit(kony.flex.DP);
        var FlxTransactionValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "FlxTransactionValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "40%",
            "zIndex": 1
        }, {}, {});
        FlxTransactionValue.setDefaultUnit(kony.flex.DP);
        var lblValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblValue",
            "isVisible": true,
            "left": 20,
            "skin": "sknlblLato5d6c7f12px",
            "text": "VALUE",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        FlxTransactionValue.add(lblValue);
        var flxMobileValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMobileValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxMobileValue.setDefaultUnit(kony.flex.DP);
        var lblMobileSymbol = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblMobileSymbol",
            "isVisible": true,
            "left": "-7px",
            "skin": "sknIcomoonCurrencySymbol",
            "text": "",
            "top": "0px",
            "width": "18px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblMobileValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblMobileValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "400",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMobileValue.add(lblMobileSymbol, lblMobileValue);
        var flxOnlineValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxOnlineValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxOnlineValue.setDefaultUnit(kony.flex.DP);
        var lblOnlineSymbol = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOnlineSymbol",
            "isVisible": true,
            "left": "-7px",
            "skin": "sknIcomoonCurrencySymbol",
            "text": "",
            "top": "0px",
            "width": "18px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOnlineValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOnlineValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "1089",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOnlineValue.add(lblOnlineSymbol, lblOnlineValue);
        var flxTotalValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTotalValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxTotalValue.setDefaultUnit(kony.flex.DP);
        var lblTotalSymbol = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTotalSymbol",
            "isVisible": true,
            "left": "-7px",
            "skin": "sknIcomoonCurrencySymbol",
            "text": "",
            "top": "0px",
            "width": "18px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTotalValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTotalValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknReportsSegTransaction13PX",
            "text": "1489",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTotalValue.add(lblTotalSymbol, lblTotalValue);
        flxValue.add(FlxTransactionValue, flxMobileValue, flxOnlineValue, flxTotalValue);
        var flxVolume = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "flxVolume",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "zIndex": 1
        }, {}, {});
        flxVolume.setDefaultUnit(kony.flex.DP);
        var flxTransactionVolume = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTransactionVolume",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "top": "0dp",
            "width": "40%",
            "zIndex": 1
        }, {}, {});
        flxTransactionVolume.setDefaultUnit(kony.flex.DP);
        var lblTransactionVolume = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTransactionVolume",
            "isVisible": true,
            "left": 20,
            "skin": "sknlblLato5d6c7f12px",
            "text": "VOLUME",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTransactionVolume.add(lblTransactionVolume);
        var flxMobileVolume = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMobileVolume",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxMobileVolume.setDefaultUnit(kony.flex.DP);
        var lblMobileVolume = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblMobileVolume",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "# 98765423",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMobileVolume.add(lblMobileVolume);
        var flxOnlineVolume = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxOnlineVolume",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxOnlineVolume.setDefaultUnit(kony.flex.DP);
        var lblOnlineVolume = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOnlineVolume",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "# 98765423",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOnlineVolume.add(lblOnlineVolume);
        var flxTotalVolume = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTotalVolume",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxTotalVolume.setDefaultUnit(kony.flex.DP);
        var lblTotalVolume = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTotalVolume",
            "isVisible": true,
            "left": "0px",
            "skin": "sknReportsSegTransaction13PX",
            "text": "# 98765423",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTotalVolume.add(lblTotalVolume);
        flxVolume.add(flxTransactionVolume, flxMobileVolume, flxOnlineVolume, flxTotalVolume);
        var flxBottomSpace = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxBottomSpace",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxBottomSpace.setDefaultUnit(kony.flex.DP);
        var dummylabel1 = new kony.ui.Label({
            "id": "dummylabel1",
            "isVisible": false,
            "left": "47dp",
            "skin": "slLabel",
            "text": "Label",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxBottomSpace.add(dummylabel1);
        var flxShadow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10dp",
            "id": "flxShadow",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknShadow",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxShadow.setDefaultUnit(kony.flex.DP);
        var dummylabel2 = new kony.ui.Label({
            "id": "dummylabel2",
            "isVisible": false,
            "left": "47dp",
            "skin": "slLabel",
            "text": "Label",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxShadow.add(dummylabel2);
        flxTransaction.add(flxHeader, flxValue, flxVolume, flxBottomSpace, flxShadow);
        flxTransactionReports.add(flxTransaction);
        return flxTransactionReports;
    }
})