define("MFAModule/userfrmMFAConfigurationsController", [], function() {
    var mfaConfigEditState = {};
    var mouseYCoordinate = 0;
    var STATUS_CONTANTS = {
        active: "SID_ACTIVE",
        inactive: "SID_INACTIVE"
    };
    return {
        willUpdateUI: function(model) {
            this.updateLeftMenu(model);
            if (model.action === "hideLoadingScreen") {
                kony.adminConsole.utils.hideProgressBar(this.view);
            } else if (model.loadingScreen) {
                if (model.loadingScreen.focus) kony.adminConsole.utils.showProgressBar(this.view);
                else kony.adminConsole.utils.hideProgressBar(this.view);
            } else if (model && model.getMFAConfigurations) {
                this.mapMFAConfigurations(model.getMFAConfigurations);
            } else if (model && model.updateMFAConfiguration) {
                this.editPopupYesClickHandler();
                this.presenter.getAllMFAConfigurations({});
            } else if (model.toastMessage) {
                if (model.toastMessage.status === kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success")) {
                    this.view.toastMessage.showToastMessage(model.toastMessage.message, this);
                } else if (model.toastMessage.status === kony.i18n.getLocalizedString("i18n.frmGroupsController.error")) {
                    this.view.toastMessage.showErrorToastMessage(model.toastMessage.message, this);
                }
            }
        },
        initActions: function() {
            var scopeObj = this;
            this.view.segMFAConfigs.onHover = this.saveScreenY.bind(this);
        },
        preshow: function() {
            var scopeObj = this;
            this.setFlowActions();
            this.presenter.getAllMFAConfigurations({});
            this.editPopupYesClickHandler();
            this.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
            scopeObj.view.flxToastMessage.setVisibility(false);
            scopeObj.view.popUpCancelEdits.flxPopUp.skin = "sknFlxFFFFFF1000";
            scopeObj.view.popUpCancelEdits.lblPopUpMainMessage.skin = "sknLbl192b45LatoReg16px";
            scopeObj.view.popUpCancelEdits.lblPopupClose.skin = "sknIcon12px192b45";
            scopeObj.view.popUpCancelEdits.rtxPopUpDisclaimer.skin = "sknrtxLatoRegular485c7512px";
            scopeObj.view.popUpCancelEdits.flxPopUpTopColor.skin = "sknflxebb54cOp100";
            scopeObj.view.popUpCancelEdits.btnPopUpDelete.skin = "sknBtn006CCALatoRegKA";
            scopeObj.view.popUpCancelEdits.btnPopUpDelete.hoverSkin = "sknBtn004F93LatoRegKA";
            scopeObj.view.popUpCancelEdits.btnPopUpDelete.focusSkin = "sknBtn006CCALatoRegKA";
            scopeObj.view.popUpCancelEdits.btnPopUpCancel.skin = "sknBtnLatoRegular8b96a513pxKA";
            scopeObj.view.popUpCancelEdits.btnPopUpCancel.hoverSkin = "sknBtnLatoRegular8b96a513pxKAhover";
            scopeObj.view.popUpCancelEdits.btnPopUpCancel.focusSkin = "sknBtnLatoRegular8b96a513pxKA";
            scopeObj.view.commonButtonsEditMFAConfig.btnCancel.skin = "sknBtnLatoRegular8b96a513pxKA";
            scopeObj.view.commonButtonsEditMFAConfig.btnCancel.hoverSkin = "sknBtnLatoRegular8b96a513pxKAhover";
            scopeObj.view.commonButtonsEditMFAConfig.btnCancel.focusSkin = "sknBtnLatoRegular8b96a513pxKA";
            scopeObj.view.commonButtonsEditMFAConfig.btnSave.skin = "sknBtn006CCALatoRegKA";
            scopeObj.view.commonButtonsEditMFAConfig.btnSave.hoverSkin = "sknBtn004F93LatoRegKA";
            scopeObj.view.commonButtonsEditMFAConfig.btnSave.focusSkin = "sknBtn006CCALatoRegKA";
            scopeObj.view.mainHeader.lblHeading.skin = "sknlblLato485c7522Px";
        },
        postShow: function() {
            document.getElementById("frmMFAConfigurations_radioAfterMaxFailedAttempts").children[1].style["padding-left"] = "20px";
        },
        setFlowActions: function() {
            var scopeObj = this;
            this.view.inputNumberCodeLength.onChange = function(value) {
                scopeObj.view.inputNumberCodeLength.enableFocus();
                scopeObj.view.maxResendRequestsAllowedInput.disableFocus();
                scopeObj.view.inputNumberOfQuestions.disableFocus();
                scopeObj.view.maxFailedAttemptsInput.disableFocus();
                scopeObj.view.lblCodeLengthLimitMessage.isVisible = false;
                if (mfaConfigEditState.mfaConfigurations) {
                    mfaConfigEditState.mfaConfigurations.SAC_CODE_LENGTH.mfaValue = value;
                }
            };
            this.view.inputNumberCodeLength.onLimitExceeds = function(limit) {
                scopeObj.view.lblCodeLengthLimitMessage.isVisible = true;
                scopeObj.view.lblCodeLengthLimitMessage.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_more_than") + " " + limit;
            };
            this.view.inputNumberCodeLength.onLimitLowers = function(limit) {
                scopeObj.view.lblCodeLengthLimitMessage.isVisible = true;
                scopeObj.view.lblCodeLengthLimitMessage.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_less_than") + " " + limit;
            };
            this.view.flxCodeLengthInput.onHover = function(eventobject, context) {
                if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                    scopeObj.view.lblCodeLengthLimitMessage.isVisible = false;
                }
            };
            this.view.inputNumberOfQuestions.onChange = function(value) {
                scopeObj.view.inputNumberCodeLength.disableFocus();
                scopeObj.view.maxResendRequestsAllowedInput.disableFocus();
                scopeObj.view.inputNumberOfQuestions.enableFocus();
                scopeObj.view.maxFailedAttemptsInput.disableFocus();
                scopeObj.view.lblCodeLengthLimitMessage.isVisible = false;
                if (mfaConfigEditState.mfaConfigurations) {
                    mfaConfigEditState.mfaConfigurations.SQ_NUMBER_OF_QUESTION_ASKED.mfaValue = value;
                }
            };
            this.view.inputNumberOfQuestions.onLimitExceeds = function(limit) {
                scopeObj.view.lblCodeLengthLimitMessage.isVisible = true;
                scopeObj.view.lblCodeLengthLimitMessage.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_more_than") + " " + limit;
            };
            this.view.inputNumberOfQuestions.onLimitLowers = function(limit) {
                scopeObj.view.lblCodeLengthLimitMessage.isVisible = true;
                scopeObj.view.lblCodeLengthLimitMessage.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_less_than") + " " + limit;
            };
            this.view.maxResendRequestsAllowedInput.onLimitExceeds = function(limit) {
                scopeObj.view.lblResendRequestsAllowedLimitMessage.isVisible = true;
                scopeObj.view.lblResendRequestsAllowedLimitMessage.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_more_than") + " " + limit;
            };
            this.view.maxResendRequestsAllowedInput.onLimitLowers = function(limit) {
                scopeObj.view.lblResendRequestsAllowedLimitMessage.isVisible = true;
                scopeObj.view.lblResendRequestsAllowedLimitMessage.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_less_than") + " " + limit;
            };
            this.view.flxMaxResendRequestInput.onHover = function(eventobject, context) {
                if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                    scopeObj.view.lblResendRequestsAllowedLimitMessage.isVisible = false;
                }
            };
            this.view.maxFailedAttemptsInput.onLimitExceeds = function(limit) {
                scopeObj.view.lblMaxFailedAttemptsLimitMessage.isVisible = true;
                scopeObj.view.lblMaxFailedAttemptsLimitMessage.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_more_than") + " " + limit;
            };
            this.view.maxFailedAttemptsInput.onLimitLowers = function(limit) {
                scopeObj.view.lblMaxFailedAttemptsLimitMessage.isVisible = true;
                scopeObj.view.lblMaxFailedAttemptsLimitMessage.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_less_than") + " " + limit;
            };
            this.view.flxMaxFailedAttemptsInput.onHover = function(eventobject, context) {
                if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                    scopeObj.view.lblMaxFailedAttemptsLimitMessage.isVisible = false;
                }
            };
            this.view.maxResendRequestsAllowedInput.onChange = function(value) {
                scopeObj.view.inputNumberCodeLength.disableFocus();
                scopeObj.view.maxResendRequestsAllowedInput.enableFocus();
                scopeObj.view.inputNumberOfQuestions.disableFocus();
                scopeObj.view.maxFailedAttemptsInput.disableFocus();
                scopeObj.view.lblResendRequestsAllowedLimitMessage.isVisible = false;
                if (mfaConfigEditState.mfaConfigurations) {
                    mfaConfigEditState.mfaConfigurations.SAC_MAX_RESEND_REQUESTS_ALLOWED.mfaValue = value;
                }
            };
            this.view.maxFailedAttemptsInput.onChange = function(value) {
                scopeObj.view.inputNumberCodeLength.disableFocus();
                scopeObj.view.maxResendRequestsAllowedInput.disableFocus();
                scopeObj.view.inputNumberOfQuestions.disableFocus();
                scopeObj.view.maxFailedAttemptsInput.enableFocus();
                scopeObj.view.lblMaxFailedAttemptsLimitMessage.isVisible = false;
                if (mfaConfigEditState.mfaConfigurations) {
                    mfaConfigEditState.mfaConfigurations.MAX_FAILED_ATTEMPTS_ALLOWED.mfaValue = value;
                }
            };
            this.view.flxCheckboxLockUser.onClick = function() {
                var img = scopeObj.view.imgLockUserCheckImage.src;
                if (img === "checkboxdisable.png") {
                    scopeObj.view.imgLockUserCheckImage.src = "checkboxnormal.png";
                } else {
                    scopeObj.view.imgLockUserCheckImage.src = "checkboxdisable.png";
                    scopeObj.view.imgLogoutUserCheck.src = "checkboxdisable.png";
                }
            };
            this.view.flxCheckboxLogoutUser.onClick = function() {
                var img = scopeObj.view.imgLogoutUserCheck.src;
                if (img === "checkboxdisable.png") {
                    scopeObj.view.imgLogoutUserCheck.src = "checkboxnormal.png";
                } else {
                    scopeObj.view.imgLogoutUserCheck.src = "checkboxdisable.png";
                }
            };
            this.view.sliderCodeExpiresAfter.onSlide = function(event, selectedvalue) {
                scopeObj.view.lblSliderSelectedValue.text = event.selectedvalue + " " + kony.i18n.getLocalizedString("i18n.MFAConfigrations.mins");
                if (mfaConfigEditState.mfaConfigurations) {
                    mfaConfigEditState.mfaConfigurations.SAC_CODE_EXPIRES_AFTER.mfaValue = event.selectedvalue;
                }
            };
            this.view.commonButtonsEditMFAConfig.btnCancel.onClick = function() {
                var msg = "";
                var yesText = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.YES_CANCEL");
                var cancelText = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.NO_LEAVE_AS_IS");
                if (mfaConfigEditState.mfaTypeId === "SECURE_ACCESS_CODE") {
                    msg = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.cancel_security_access_code_message");
                } else {
                    msg = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.cancel_security_questions_message");
                }
                scopeObj.showAlertPopup("Cancel Changes", msg, cancelText, yesText, function() {
                    scopeObj.editPopupYesClickHandler();
                });
            };
            this.view.commonButtonsEditMFAConfig.btnSave.onClick = function() {
                scopeObj.onUpdateClickHandler();
            };
            this.view.popUpCancelEdits.btnPopUpCancel.onClick = function() {
                scopeObj.editPopupCancelClickHandler();
            };
            this.view.popUpCancelEdits.btnPopUpDelete.onClick = function() {
                scopeObj.editPopupYesClickHandler();
            };
            this.view.popUpCancelEdits.flxPopUpClose.onClick = function() {
                scopeObj.view.flxEditCancelConfirmation.isVisible = false;
            };
            this.view.breadcrumbs.btnBackToMain.onClick = function() {
                scopeObj.editPopupYesClickHandler();
            };
            this.view.radioAfterMaxFailedAttempts.onSelection = function(eventobject) {
                kony.print(eventobject.selectedKey);
                if (mfaConfigEditState.mfaConfigurations) {
                    if (eventobject.selectedKey === "LOCK_USER") {
                        mfaConfigEditState.mfaConfigurations["LOCK_USER"] = {
                            mfaValue: "true",
                            mfaKey: "LOCK_USER"
                        };
                        mfaConfigEditState.mfaConfigurations["LOGOUT_USER"] = {
                            mfaValue: "true",
                            mfaKey: "LOGOUT_USER"
                        };
                    } else {
                        mfaConfigEditState.mfaConfigurations["LOGOUT_USER"] = {
                            mfaValue: "true",
                            mfaKey: "LOGOUT_USER"
                        };
                        mfaConfigEditState.mfaConfigurations["LOCK_USER"] = {
                            mfaValue: "false",
                            mfaKey: "LOCK_USER"
                        };
                    }
                }
            };
        },
        /**
         * Map mfa configurations data to segment
         * @param {Array} data
         */
        mapMFAConfigurations: function(data) {
            var dataMap = {
                "lblSubHeadeing": "lblSubHeadeing",
                "fontIconImgViewDescription": "fontIconImgViewDescription",
                "lblMFAConfigName": "lblMFAConfigName",
                "lblIconOptions": "lblIconOptions",
                "lblHeading11": "lblHeading11",
                "lblHeading12": "lblHeading12",
                "lblHeading13": "lblHeading13",
                "lblData11": "lblData11",
                "lblData12": "lblData12",
                "lblData13": "lblData13",
                "lblHeading21": "lblHeading21",
                "lblHeading22": "lblHeading22",
                "lblHeading23": "lblHeading23",
                "lblData21": "lblData21",
                "lblData22": "lblData22",
                "lblData23": "lblData23",
                "lblSeperator": "lblSeperator",
                "flxOptions": "flxOptions",
                "flxViewEditButton": "flxViewEditButton",
                "lblViewEditButton": "lblViewEditButton"
            };
            this.view.segMFAConfigs.widgetDataMap = dataMap;
            data = this.processMFAConfigsForView(data);
            this.view.segMFAConfigs.setData(data);
        },
        /**
         * Called when clicked on segment row.Handles Expand/Collapse functionality.
         */
        showSelectedRow: function() {
            var index = this.view.segMFAConfigs.selectedIndex;
            var rowIndex = index[1];
            var data = this.view.segMFAConfigs.data;
            if (data[rowIndex].template === "flxSegMFAConfigsSelected") {
                /*for (var j = 0; j < data.length; j++) {
                 data[j].template = "flxSegMFAConfigs";
                }*/
                data[rowIndex].template = "flxSegMFAConfigs";
                data[rowIndex].fontIconImgViewDescription.text = "";
            } else {
                /*for (var i = 0; i < data.length; i++) {
                 data[i].template = "flxSegMFAConfigs";
                 data[i].fontIconImgViewDescription.text = "";
                }*/
                data[rowIndex].template = "flxSegMFAConfigsSelected";
                data[rowIndex].fontIconImgViewDescription.text = "";
            }
            this.view.segMFAConfigs.setDataAt(data[rowIndex], rowIndex, 0);
            this.view.forceLayout();
        },
        /**
         * Format mfa configurations response to segment data
         * @param {Array} mfaConfigs 
         */
        processMFAConfigsForView: function(mfaConfigs) {
            var scopeObj = this;
            if (mfaConfigs && mfaConfigs instanceof Array && mfaConfigs.length > 0) {
                return mfaConfigs.map(function(rec, index) {
                    var obj = Object.assign({}, rec);
                    switch (rec.mfaTypeId) {
                        case "SECURE_ACCESS_CODE":
                            obj.lblHeading11 = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.CODE_LENGTH");
                            obj.lblHeading12 = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.CODE_EXPIRED_AFTER");
                            obj.lblHeading13 = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.MAX_RESEND_REQUESTS_ALLOWED");
                            obj.lblData11 = rec.mfaConfigurations.SAC_CODE_LENGTH.mfaValue + " characters";
                            obj.lblData12 = rec.mfaConfigurations.SAC_CODE_EXPIRES_AFTER.mfaValue + (rec.mfaConfigurations.SAC_CODE_EXPIRES_AFTER.mfaValue == 1 ? " minute" : " minutes");
                            obj.lblData13 = rec.mfaConfigurations.SAC_MAX_RESEND_REQUESTS_ALLOWED.mfaValue + "";
                            break;
                        case "SECURITY_QUESTIONS":
                            obj.lblHeading11 = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.NUMBER_OF_QUESTIONS_ASKED_AT_A_TIME");
                            obj.lblHeading12 = "";
                            obj.lblHeading13 = "";
                            obj.lblData11 = rec.mfaConfigurations.SQ_NUMBER_OF_QUESTION_ASKED.mfaValue + "";
                            obj.lblData12 = "";
                            obj.lblData13 = "";
                            break;
                    }
                    obj.lblSubHeadeing = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Authentication_Failure_Settings") || "Authentication Failure Settings";
                    obj.flxOptions = {
                        isVisible: false
                    };
                    obj.template = "flxSegMFAConfigs";
                    obj.fontIconImgViewDescription = {
                        text: ""
                    };
                    obj.lblMFAConfigName = rec.mfaTypeName;
                    obj.lblHeading21 = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.MAX_FAILED_ATTEMPTS_ALLOWED");
                    obj.lblHeading22 = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.AFTER_MAX_FAILED_ATTEMPTS");
                    obj.lblData21 = rec.mfaConfigurations.MAX_FAILED_ATTEMPTS_ALLOWED.mfaValue;
                    if (rec.mfaConfigurations.LOCK_USER.mfaValue === "true") {
                        obj.lblData22 = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Logout_Lock_User");
                    } else if (rec.mfaConfigurations.LOGOUT_USER.mfaValue === "true") {
                        obj.lblData22 = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Logout_User");
                    }
                    obj.lblSeperator = ".";
                    obj.flxStatus = {
                        isVisible: false
                    };
                    obj.flxViewEditButton = {
                        isVisible: true,
                        onClick: scopeObj.toggleVisibility.bind(this)
                    };
                    obj.lblViewEditButton = {
                        text: kony.i18n.getLocalizedString("i18n.permission.Edit")
                    };
                    return obj;
                });
            }
            return [];
        },
        /**
         * Tracks y axis position
         */
        saveScreenY: function(widget, context) {
            mouseYCoordinate = context.screenY;
        },
        /**
         * Called when clicked on options button. Handle options menu.
         */
        toggleVisibility: function() {
            this.handleMFAConfigEditClick();
            this.view.forceLayout();
        },
        /**
         * Called when clicked on edit option from options menu.
         */
        handleMFAConfigEditClick: function() {
            var segIndex = this.view.segMFAConfigs.selectedRowIndex[1];
            var segData = this.view.segMFAConfigs.data[segIndex];
            this.showMFAConfigEdit(JSON.parse(JSON.stringify(segData)));
        },
        /**
         * Maps data to edit screen and displays edit form.
         * @param {object} data 
         */
        showMFAConfigEdit: function(data) {
            this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Edit_Multi_Factor_Authentication_Configuration");
            this.view.flxBreadcrumb.isVisible = true;
            this.view.flxMFACongifurations.isVisible = false;
            this.view.flxEditMFAConfig.isVisible = true;
            Object.assign(mfaConfigEditState, data);
            this.view.breadcrumbs.lblCurrentScreen.text = mfaConfigEditState.mfaTypeName.toString().toUpperCase();
            if (mfaConfigEditState.mfaTypeId === "SECURE_ACCESS_CODE") {
                this.view.flxRow2.isVisible = true;
                this.view.flxColumn12.isVisible = true;
                this.view.lblHeader11.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Code_Length");
                this.view.inputNumberCodeLength.isVisible = true;
                this.view.inputNumberOfQuestions.isVisible = false;
                this.view.inputNumberCodeLength.defaultValue = parseInt(mfaConfigEditState.mfaConfigurations.SAC_CODE_LENGTH.mfaValue);
                this.view.inputNumberCodeLength.minLimit = parseInt(mfaConfigEditState.mfaConfigurations.SAC_CODE_LENGTH.minLength);
                this.view.inputNumberCodeLength.maxLimit = parseInt(mfaConfigEditState.mfaConfigurations.SAC_CODE_LENGTH.maxLength);
                this.view.maxResendRequestsAllowedInput.defaultValue = parseInt(mfaConfigEditState.mfaConfigurations.SAC_MAX_RESEND_REQUESTS_ALLOWED.mfaValue);
                this.view.maxResendRequestsAllowedInput.minLimit = parseInt(mfaConfigEditState.mfaConfigurations.SAC_MAX_RESEND_REQUESTS_ALLOWED.minLength);
                this.view.maxResendRequestsAllowedInput.maxLimit = parseInt(mfaConfigEditState.mfaConfigurations.SAC_MAX_RESEND_REQUESTS_ALLOWED.maxLength);
                this.view.sliderCodeExpiresAfter.selectedValue = mfaConfigEditState.mfaConfigurations.SAC_CODE_EXPIRES_AFTER.mfaValue;
                this.view.lblSliderSelectedValue.text = mfaConfigEditState.mfaConfigurations.SAC_CODE_EXPIRES_AFTER.mfaValue + " " + kony.i18n.getLocalizedString("i18n.MFAConfigrations.mins");
            } else if (mfaConfigEditState.mfaTypeId === "SECURITY_QUESTIONS") {
                this.view.flxRow2.isVisible = false;
                this.view.flxColumn12.isVisible = false;
                this.view.lblHeader11.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Number_Of_Questions_Asked_At_A_Time");
                this.view.inputNumberCodeLength.isVisible = false;
                this.view.inputNumberOfQuestions.isVisible = true;
                this.view.inputNumberOfQuestions.defaultValue = parseInt(mfaConfigEditState.mfaConfigurations.SQ_NUMBER_OF_QUESTION_ASKED.mfaValue);
                this.view.inputNumberOfQuestions.minLimit = parseInt(mfaConfigEditState.mfaConfigurations.SQ_NUMBER_OF_QUESTION_ASKED.minLength);
                this.view.inputNumberOfQuestions.maxLimit = parseInt(mfaConfigEditState.mfaConfigurations.SQ_NUMBER_OF_QUESTION_ASKED.maxLength);
            }
            this.view.maxFailedAttemptsInput.defaultValue = parseInt(mfaConfigEditState.mfaConfigurations.MAX_FAILED_ATTEMPTS_ALLOWED.mfaValue);
            this.view.maxFailedAttemptsInput.minLimit = parseInt(mfaConfigEditState.mfaConfigurations.MAX_FAILED_ATTEMPTS_ALLOWED.minLength);
            this.view.maxFailedAttemptsInput.maxLimit = parseInt(mfaConfigEditState.mfaConfigurations.MAX_FAILED_ATTEMPTS_ALLOWED.maxLength);
            this.view.lblMFAConfigName.text = mfaConfigEditState.mfaTypeName;
            if (mfaConfigEditState.mfaConfigurations.LOCK_USER.mfaValue === "true") {
                this.view.radioAfterMaxFailedAttempts.selectedKey = "LOCK_USER";
            } else if (mfaConfigEditState.mfaConfigurations.LOGOUT_USER.mfaValue === "true") {
                this.view.radioAfterMaxFailedAttempts.selectedKey = "LOGOUT_USER";
            }
            this.view.flxEditContent.scrollToWidget(this.view.flxRowHeader);
            this.view.inputNumberCodeLength.disableFocus();
            this.view.maxResendRequestsAllowedInput.disableFocus();
            this.view.inputNumberOfQuestions.disableFocus();
            this.view.maxFailedAttemptsInput.disableFocus();
            this.view.forceLayout();
            document.getElementById("frmMFAConfigurations_radioAfterMaxFailedAttempts").children[1].style["padding-left"] = "20px";
        },
        /**
         * Called when clicked on cancel in confirmation popup.
         */
        editPopupCancelClickHandler: function() {
            this.view.flxEditCancelConfirmation.isVisible = false;
        },
        /**
         * Called when clicked on yes in confirmation popup.
         */
        editPopupYesClickHandler: function() {
            this.view.flxEditCancelConfirmation.isVisible = false;
            this.view.flxBreadcrumb.isVisible = false;
            this.view.flxMFACongifurations.isVisible = true;
            this.view.flxEditMFAConfig.isVisible = false;
            this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Multi_Factor_Authentication_Configuration");
        },
        /**
         * Called when clicked on update in edit form. It will invoke update service.
         */
        onUpdateClickHandler: function() {
            kony.print(JSON.stringify(mfaConfigEditState));
            var updatedObject = {
                "mfaTypeId": mfaConfigEditState.mfaTypeId + "",
                "mfaConfigurations": [{
                    "mfaKey": "MAX_FAILED_ATTEMPTS_ALLOWED",
                    "mfaValue": mfaConfigEditState.mfaConfigurations.MAX_FAILED_ATTEMPTS_ALLOWED.mfaValue + ""
                }, {
                    "mfaKey": "LOGOUT_USER",
                    "mfaValue": mfaConfigEditState.mfaConfigurations.LOGOUT_USER.mfaValue + ""
                }, {
                    "mfaKey": "LOCK_USER",
                    "mfaValue": mfaConfigEditState.mfaConfigurations.LOCK_USER.mfaValue + ""
                }]
            };
            if (mfaConfigEditState.mfaTypeId === "SECURE_ACCESS_CODE") {
                updatedObject.mfaConfigurations.push({
                    "mfaKey": "SAC_CODE_LENGTH",
                    "mfaValue": mfaConfigEditState.mfaConfigurations.SAC_CODE_LENGTH.mfaValue + ""
                });
                updatedObject.mfaConfigurations.push({
                    "mfaKey": "SAC_CODE_EXPIRES_AFTER",
                    "mfaValue": mfaConfigEditState.mfaConfigurations.SAC_CODE_EXPIRES_AFTER.mfaValue + ""
                });
                updatedObject.mfaConfigurations.push({
                    "mfaKey": "SAC_MAX_RESEND_REQUESTS_ALLOWED",
                    "mfaValue": mfaConfigEditState.mfaConfigurations.SAC_MAX_RESEND_REQUESTS_ALLOWED.mfaValue + ""
                });
            } else {
                updatedObject.mfaConfigurations.push({
                    "mfaKey": "SQ_NUMBER_OF_QUESTION_ASKED",
                    "mfaValue": mfaConfigEditState.mfaConfigurations.SQ_NUMBER_OF_QUESTION_ASKED.mfaValue + ""
                });
            }
            this.presenter.updateMFAConfiguration(updatedObject);
        },
        showAlertPopup: function(headerMsg, contentMsg, cancelText, yesText, yesCallback) {
            var scopeObj = this;
            this.view.popUpCancelEdits.lblPopUpMainMessage.text = headerMsg;
            this.view.popUpCancelEdits.rtxPopUpDisclaimer.text = contentMsg;
            this.view.popUpCancelEdits.btnPopUpCancel.text = cancelText;
            this.view.popUpCancelEdits.btnPopUpDelete.text = yesText;
            if (yesText.length > 12) {
                this.view.popUpCancelEdits.btnPopUpCancel.right = "170px";
            } else {
                this.view.popUpCancelEdits.btnPopUpCancel.right = "150px";
            }
            this.view.popUpCancelEdits.btnPopUpDelete.onClick = function() {
                scopeObj.view.flxEditCancelConfirmation.isVisible = false;
                yesCallback();
            };
            this.view.flxEditCancelConfirmation.isVisible = true;
            this.view.popUpCancelEdits.forceLayout();
        },
    };
});
define("MFAModule/frmMFAConfigurationsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmMFAConfigurations **/
    AS_Form_d16ab3ae371143ec8bf31f4e4576d324: function AS_Form_d16ab3ae371143ec8bf31f4e4576d324(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmMFAConfigurations **/
    AS_Form_ba807b9d26534a89a4f208d8de289d5c: function AS_Form_ba807b9d26534a89a4f208d8de289d5c(eventobject) {
        var self = this;
        this.preshow();
    },
    /** postShow defined for frmMFAConfigurations **/
    AS_Form_j03f1a6c32ee4cd185474451084e1edf: function AS_Form_j03f1a6c32ee4cd185474451084e1edf(eventobject) {
        var self = this;
        this.postShow();
    }
});
define("MFAModule/frmMFAConfigurationsController", ["MFAModule/userfrmMFAConfigurationsController", "MFAModule/frmMFAConfigurationsControllerActions"], function() {
    var controller = require("MFAModule/userfrmMFAConfigurationsController");
    var controllerActions = ["MFAModule/frmMFAConfigurationsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
