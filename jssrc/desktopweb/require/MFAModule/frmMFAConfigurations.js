define("MFAModule/frmMFAConfigurations", function() {
    return function(controller) {
        function addWidgetsfrmMFAConfigurations() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "306px",
                "zIndex": 1
            }, {}, {});
            flxLeftPanel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "konydbxlogobignverted.png"
                    },
                    "imgBottomLogo": {
                        "src": "konydbxinverted.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPanel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": 0,
                "skin": "CopyslFbox0j190b3ed67d04b",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "108dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "flxButtons": {
                        "isVisible": false
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Multi_Factor_Authentication_Configuration\")",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 10
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadcrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "95dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadcrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "isVisible": true
                    },
                    "btnBackToMain": {
                        "text": "MFA CONFIGURATIONS"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "text": "SECURE ACCESS CODE"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumb.add(breadcrumbs);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "63px",
                "id": "flxMainSubHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "106dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "clipBounds": true,
                "height": "48dp",
                "id": "subHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxSearch": {
                        "centerY": "viz.val_cleared"
                    },
                    "flxSearchContainer": {
                        "top": "12dp"
                    },
                    "flxSubHeader": {
                        "centerY": "viz.val_cleared"
                    },
                    "subHeader": {
                        "centerY": "viz.val_cleared",
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxScrollMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "75%",
                "horizontalScrollIndicator": true,
                "id": "flxScrollMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "130dp",
                "verticalScrollIndicator": true,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {}, {});
            flxScrollMainContent.setDefaultUnit(kony.flex.DP);
            var flxMFACongifurations = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMFACongifurations",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 2
            }, {}, {});
            flxMFACongifurations.setDefaultUnit(kony.flex.DP);
            var flxMFAConfigContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "100%",
                "id": "flxMFAConfigContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxMFAConfigContainer.setDefaultUnit(kony.flex.DP);
            var flxSegmentMFAConfigs = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxSegmentMFAConfigs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxSegmentMFAConfigs.setDefaultUnit(kony.flex.DP);
            var segMFAConfigs = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "data": [{
                    "fontIconImgViewDescription": "",
                    "imgOptions": "",
                    "imgServiceStatus": "",
                    "lblIconOptions": "",
                    "lblMFAConfigName": "",
                    "lblSeperator": ".",
                    "lblServiceStatus": "",
                    "lblViewEditButton": kony.i18n.getLocalizedString("i18n.permission.Edit")
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segMFAConfigs",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknSegRowTransparent",
                "rowTemplate": "flxSegMFAConfigs",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAccordianContainer": "flxAccordianContainer",
                    "flxArrow": "flxArrow",
                    "flxContent": "flxContent",
                    "flxOptions": "flxOptions",
                    "flxSegMFAConfigs": "flxSegMFAConfigs",
                    "flxStatus": "flxStatus",
                    "flxViewEditButton": "flxViewEditButton",
                    "fontIconImgViewDescription": "fontIconImgViewDescription",
                    "imgOptions": "imgOptions",
                    "imgServiceStatus": "imgServiceStatus",
                    "lblIconOptions": "lblIconOptions",
                    "lblMFAConfigName": "lblMFAConfigName",
                    "lblSeperator": "lblSeperator",
                    "lblServiceStatus": "lblServiceStatus",
                    "lblViewEditButton": "lblViewEditButton"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0d26883c7010e40",
                "top": "35px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator.setDefaultUnit(kony.flex.DP);
            flxSeparator.add();
            flxSegmentMFAConfigs.add(segMFAConfigs, flxSeparator);
            flxMFAConfigContainer.add(flxSegmentMFAConfigs);
            var flxNorecordsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxNorecordsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxNorecordsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoResultsFound = new kony.ui.RichText({
                "bottom": "150px",
                "centerX": "50%",
                "id": "rtxNoResultsFound",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.NoResultsFound\")",
                "top": "150px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNorecordsFound.add(rtxNoResultsFound);
            flxMFACongifurations.add(flxMFAConfigContainer, flxNorecordsFound);
            var flxEditMFAConfig = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditMFAConfig",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "zIndex": 2
            }, {}, {});
            flxEditMFAConfig.setDefaultUnit(kony.flex.DP);
            var flxEditContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxEditContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditContent.setDefaultUnit(kony.flex.DP);
            var flxEditContentMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditContentMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxEditContentMain.setDefaultUnit(kony.flex.DP);
            var flxRowHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxRowHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRowHeader.setDefaultUnit(kony.flex.DP);
            var lblMFAConfigName = new kony.ui.Label({
                "bottom": "20px",
                "id": "lblMFAConfigName",
                "isVisible": true,
                "left": "0px",
                "skin": "LblLatoRegular485c7516px",
                "text": "Secure Access Code",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRowHeader.add(lblMFAConfigName);
            var flxTitleSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxTitleSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTitleSeperator.setDefaultUnit(kony.flex.DP);
            flxTitleSeperator.add();
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var flxColumn11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumn11",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxColumn11.setDefaultUnit(kony.flex.DP);
            var lblHeader11 = new kony.ui.Label({
                "id": "lblHeader11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Code_Length\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxButtons11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxButtons11",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtons11.setDefaultUnit(kony.flex.DP);
            var btnNumber111 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber111",
                "isVisible": true,
                "left": "0dp",
                "skin": "Btneceff3Radius25",
                "text": "4",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNumber112 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber112",
                "isVisible": true,
                "left": "10dp",
                "skin": "Btneceff3Radius25Filled",
                "text": "5",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNumber113 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber113",
                "isVisible": true,
                "left": "10dp",
                "skin": "Btneceff3Radius25",
                "text": "6",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNumber114 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber114",
                "isVisible": true,
                "left": "10dp",
                "skin": "Btneceff3Radius25",
                "text": "7",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNumber115 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber115",
                "isVisible": true,
                "left": "10dp",
                "skin": "Btneceff3Radius25",
                "text": "8",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxButtons11.add(btnNumber111, btnNumber112, btnNumber113, btnNumber114, btnNumber115);
            var flxCodeLengthInput = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxCodeLengthInput",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "184px",
                "zIndex": 1
            }, {}, {});
            flxCodeLengthInput.setDefaultUnit(kony.flex.DP);
            var inputNumberCodeLength = new com.c360.common.inputNumber({
                "clipBounds": true,
                "height": "40px",
                "id": "inputNumberCodeLength",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "inputNumber": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            inputNumberCodeLength.maxLimit = 15;
            inputNumberCodeLength.minLimit = 8;
            inputNumberCodeLength.defaultValue = 11;
            var inputNumberOfQuestions = new com.c360.common.inputNumber({
                "clipBounds": true,
                "height": "40px",
                "id": "inputNumberOfQuestions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "inputNumber": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            inputNumberOfQuestions.maxLimit = 15;
            inputNumberOfQuestions.minLimit = 8;
            inputNumberOfQuestions.defaultValue = 11;
            var lblCodeLengthLimitMessage = new kony.ui.Label({
                "id": "lblCodeLengthLimitMessage",
                "isVisible": false,
                "left": "0dp",
                "skin": "slLabel0d4f692dab05249",
                "text": "Value cannot be more than 8",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCodeLengthInput.add(inputNumberCodeLength, inputNumberOfQuestions, lblCodeLengthLimitMessage);
            flxColumn11.add(lblHeader11, flxButtons11, flxCodeLengthInput);
            var flxColumn12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumn12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "55%",
                "zIndex": 1
            }, {}, {});
            flxColumn12.setDefaultUnit(kony.flex.DP);
            var lblHeader12 = new kony.ui.Label({
                "id": "lblHeader12",
                "isVisible": true,
                "left": "11dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Code_Expires_After\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSliderValues = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSliderValues",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknCustomFlex",
                "top": "30px",
                "width": "416px",
                "zIndex": 1
            }, {}, {});
            flxSliderValues.setDefaultUnit(kony.flex.DP);
            var lblMinValue = new kony.ui.Label({
                "id": "lblMinValue",
                "isVisible": true,
                "left": "12px",
                "skin": "sknLbl485C75Font12pxKA",
                "text": "1 Min",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSliderSelectedValue = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblSliderSelectedValue",
                "isVisible": true,
                "left": "28dp",
                "skin": "sknLbl485C75Font12pxKA",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxValue = new kony.ui.Label({
                "id": "lblMaxValue",
                "isVisible": true,
                "right": "12px",
                "skin": "sknLbl485C75Font12pxKA",
                "text": "10 Mins",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSliderValues.add(lblMinValue, lblSliderSelectedValue, lblMaxValue);
            var sliderCodeExpiresAfter = new kony.ui.Slider({
                "focusThumbImage": "slider.png",
                "id": "sliderCodeExpiresAfter",
                "isVisible": true,
                "left": "0dp",
                "leftSkin": "sknSlider",
                "max": 10,
                "min": 1,
                "rightSkin": "CopyslSliderRightBlue0d1eb41178f1e49",
                "selectedValue": 4,
                "step": 1,
                "thumbImage": "slider.png",
                "top": "15dp",
                "width": "416px",
                "zIndex": 1
            }, {}, {
                "maxLabelSkin": "sknSliderLabel",
                "minLabelSkin": "sknSliderLabel",
                "orientation": constants.SLIDER_HORIZONTAL_ORIENTATION,
                "thickness": 7,
                "viewType": constants.SLIDER_VIEW_TYPE_DEFAULT
            });
            flxColumn12.add(lblHeader12, flxSliderValues, sliderCodeExpiresAfter);
            flxRow1.add(flxColumn11, flxColumn12);
            var flxRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow2.setDefaultUnit(kony.flex.DP);
            var flxColumn21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumn21",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxColumn21.setDefaultUnit(kony.flex.DP);
            var lblHeader21 = new kony.ui.Label({
                "id": "lblHeader21",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Max_Resend_Requests_Allowed\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxButtons21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxButtons21",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtons21.setDefaultUnit(kony.flex.DP);
            var btnNumber211 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber211",
                "isVisible": true,
                "left": "0dp",
                "skin": "Btneceff3Radius25",
                "text": "0",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNumber212 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber212",
                "isVisible": true,
                "left": "10dp",
                "skin": "Btneceff3Radius25Filled",
                "text": "1",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNumber213 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber213",
                "isVisible": true,
                "left": "10dp",
                "skin": "Btneceff3Radius25",
                "text": "2",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNumber214 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber214",
                "isVisible": true,
                "left": "10dp",
                "skin": "Btneceff3Radius25",
                "text": "3",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxButtons21.add(btnNumber211, btnNumber212, btnNumber213, btnNumber214);
            var flxMaxResendRequestInput = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxMaxResendRequestInput",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "184px",
                "zIndex": 1
            }, {}, {});
            flxMaxResendRequestInput.setDefaultUnit(kony.flex.DP);
            var maxResendRequestsAllowedInput = new com.c360.common.inputNumber({
                "clipBounds": true,
                "height": "40px",
                "id": "maxResendRequestsAllowedInput",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "inputNumber": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            maxResendRequestsAllowedInput.maxLimit = 15;
            maxResendRequestsAllowedInput.minLimit = 8;
            maxResendRequestsAllowedInput.defaultValue = 11;
            var lblResendRequestsAllowedLimitMessage = new kony.ui.Label({
                "id": "lblResendRequestsAllowedLimitMessage",
                "isVisible": false,
                "left": "0dp",
                "skin": "slLabel0d4f692dab05249",
                "text": "Value cannot be more than 8",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxResendRequestInput.add(maxResendRequestsAllowedInput, lblResendRequestsAllowedLimitMessage);
            flxColumn21.add(lblHeader21, flxButtons21, flxMaxResendRequestInput);
            flxRow2.add(flxColumn21);
            var lblAuthFailureTitle = new kony.ui.Label({
                "id": "lblAuthFailureTitle",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLatoBold485c7513px",
                "text": "Authentication Failure Settings",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxRow3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow3.setDefaultUnit(kony.flex.DP);
            var flxColumn31 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumn31",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxColumn31.setDefaultUnit(kony.flex.DP);
            var lblHeader31 = new kony.ui.Label({
                "id": "lblHeader31",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Max_Failed_Attempts_Allowed\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxButtons31 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxButtons31",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtons31.setDefaultUnit(kony.flex.DP);
            var btnNumber311 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber311",
                "isVisible": true,
                "left": "0dp",
                "skin": "Btneceff3Radius25",
                "text": "1",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNumber312 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber312",
                "isVisible": true,
                "left": "10dp",
                "skin": "Btneceff3Radius25Filled",
                "text": "2",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNumber313 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber313",
                "isVisible": true,
                "left": "10dp",
                "skin": "Btneceff3Radius25",
                "text": "3",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNumber314 = new kony.ui.Button({
                "focusSkin": "Btneceff3Radius25",
                "height": "35px",
                "id": "btnNumber314",
                "isVisible": true,
                "left": "10dp",
                "skin": "Btneceff3Radius25",
                "text": "4",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxButtons31.add(btnNumber311, btnNumber312, btnNumber313, btnNumber314);
            var flxMaxFailedAttemptsInput = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxMaxFailedAttemptsInput",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "184px",
                "zIndex": 1
            }, {}, {});
            flxMaxFailedAttemptsInput.setDefaultUnit(kony.flex.DP);
            var maxFailedAttemptsInput = new com.c360.common.inputNumber({
                "clipBounds": true,
                "height": "40px",
                "id": "maxFailedAttemptsInput",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "inputNumber": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            maxFailedAttemptsInput.maxLimit = 15;
            maxFailedAttemptsInput.minLimit = 8;
            maxFailedAttemptsInput.defaultValue = 11;
            var lblMaxFailedAttemptsLimitMessage = new kony.ui.Label({
                "id": "lblMaxFailedAttemptsLimitMessage",
                "isVisible": false,
                "left": "0dp",
                "skin": "slLabel0d4f692dab05249",
                "text": "Value cannot be more than 8",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxFailedAttemptsInput.add(maxFailedAttemptsInput, lblMaxFailedAttemptsLimitMessage);
            flxColumn31.add(lblHeader31, flxButtons31, flxMaxFailedAttemptsInput);
            var flxColumn32 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumn32",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "65%",
                "zIndex": 1
            }, {}, {});
            flxColumn32.setDefaultUnit(kony.flex.DP);
            var lblHeading32 = new kony.ui.Label({
                "id": "lblHeading32",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Afetr_Max_Failed_Attempts\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var FlexContainer0c05a6007f4b547 = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "FlexContainer0c05a6007f4b547",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0c05a6007f4b547.setDefaultUnit(kony.flex.DP);
            var flxCheckboxLockUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxCheckboxLockUser",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "top": "0dp",
                "width": "105px",
                "zIndex": 1
            }, {}, {});
            flxCheckboxLockUser.setDefaultUnit(kony.flex.DP);
            var flxLockUserImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLockUserImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxLockUserImage.setDefaultUnit(kony.flex.DP);
            var imgLockUserCheckImage = new kony.ui.Image2({
                "height": "100%",
                "id": "imgLockUserCheckImage",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLockUserImage.add(imgLockUserCheckImage);
            var lblLockUser = new kony.ui.Label({
                "id": "lblLockUser",
                "isVisible": true,
                "left": "18dp",
                "skin": "sknlblLato5d6c7f12px",
                "text": "Lock user",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckboxLockUser.add(flxLockUserImage, lblLockUser);
            var flxCheckboxLogoutUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxCheckboxLogoutUser",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "top": "0dp",
                "width": "105px",
                "zIndex": 1
            }, {}, {});
            flxCheckboxLogoutUser.setDefaultUnit(kony.flex.DP);
            var flxLogoutUserImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogoutUserImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxLogoutUserImage.setDefaultUnit(kony.flex.DP);
            var imgLogoutUserCheck = new kony.ui.Image2({
                "height": "100%",
                "id": "imgLogoutUserCheck",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxdisable.png",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLogoutUserImage.add(imgLogoutUserCheck);
            var lblLogoutUser = new kony.ui.Label({
                "id": "lblLogoutUser",
                "isVisible": true,
                "left": "18dp",
                "skin": "sknlblLato5d6c7f12px",
                "text": "Logout user",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckboxLogoutUser.add(flxLogoutUserImage, lblLogoutUser);
            var radioAfterMaxFailedAttempts = new kony.ui.RadioButtonGroup({
                "height": "40dp",
                "id": "radioAfterMaxFailedAttempts",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["LOCK_USER", "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Logout_Lock_User\")"],
                    ["LOGOUT_USER", "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Logout_User\")"]
                ],
                "skin": "sknRadioGrp484b5213pxCustom",
                "top": "0dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0c05a6007f4b547.add(flxCheckboxLockUser, flxCheckboxLogoutUser, radioAfterMaxFailedAttempts);
            flxColumn32.add(lblHeading32, FlexContainer0c05a6007f4b547);
            flxRow3.add(flxColumn31, flxColumn32);
            flxEditContentMain.add(flxRowHeader, flxTitleSeperator, flxRow1, flxRow2, lblAuthFailureTitle, flxRow3);
            flxEditContent.add(flxEditContentMain);
            var flxEditFooterButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditFooterButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditFooterButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsEditMFAConfig = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtonsEditMFAConfig",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnCancel": {
                        "left": "viz.val_cleared",
                        "right": "140px",
                        "zIndex": 100
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.buttonUpdate\")",
                        "isVisible": true,
                        "right": "20dp"
                    },
                    "commonButtons": {
                        "left": "20px",
                        "right": "20px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDetailsButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxDetailsButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxDetailsButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxDetailsButtonsSeperator.add();
            flxEditFooterButtons.add(commonButtonsEditMFAConfig, flxDetailsButtonsSeperator);
            flxEditMFAConfig.add(flxEditContent, flxEditFooterButtons);
            flxScrollMainContent.add(flxMFACongifurations, flxEditMFAConfig);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 11
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxBreadcrumb, flxMainSubHeader, flxScrollMainContent, flxLoading);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.NO_LEAVE_AS_IS\")",
                        "right": "150px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.YES_CANCEL\")"
                    },
                    "imgPopUpClose": {
                        "src": "close_blue.png"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Cancel Edit of  Security Access Code Configurations"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "50%",
                        "centerY": "50%",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.cancel_mfa_configs_message\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var flxToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "45px",
                "id": "flxToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknflxSuccessToast1F844D",
                "top": "0%",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxToastContainer.setDefaultUnit(kony.flex.DP);
            var imgLeft = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgLeft",
                "isVisible": true,
                "left": "15px",
                "skin": "slImage",
                "src": "arrow2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbltoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbltoastMessage",
                "isVisible": true,
                "left": "45px",
                "right": "45px",
                "skin": "lblfffffflatoregular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SuccessfullyDeactivated\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgRight = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRight",
                "isVisible": true,
                "right": "15px",
                "skin": "slImage",
                "src": "close_small2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToastContainer.add(imgLeft, lbltoastMessage, imgRight);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(flxToastContainer, toastMessage);
            flxMain.add(flxLeftPanel, flxRightPanel, flxEditCancelConfirmation, flxToastMessage);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmMFAConfigurations,
            "enabledForIdleTimeout": true,
            "id": "frmMFAConfigurations",
            "init": controller.AS_Form_d16ab3ae371143ec8bf31f4e4576d324,
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_j03f1a6c32ee4cd185474451084e1edf,
            "preShow": function(eventobject) {
                controller.AS_Form_ba807b9d26534a89a4f208d8de289d5c(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});