define("MFAModule/userfrmMFAScenariosController", [], function() {
    var STATUS_CONTANTS = {
        active: "SID_ACTIVE",
        inactive: "SID_INACTIVE"
    };
    var ACTION_CONFIG = {
        create: "CREATE",
        edit: "EDIT",
        view: "VIEW"
    };
    var ACTION = "CREATE";
    var mfaScenarioState = {};
    var mouseYCoordinate = 0;
    var masterData = {
        applications: [],
        actionsTransactions: [],
        actionsNonTransactions: [],
        mfaTypes: [],
        mfaModes: [],
        frequencyTypes: [],
        isLoaded: false
    };
    var SELECTED_FILTERS = {
        appId: "ALL_APPLICATIONS",
        status: [kony.i18n.getLocalizedString("i18n.secureimage.Active"), kony.i18n.getLocalizedString("i18n.secureimage.Inactive")],
        searchText: ""
    };
    var CURRENT_SELECTION = {
        appId: "",
        actionId: "",
        statusId: ""
    };
    var NAVIGATE_FLAG = "main";
    return {
        state: {
            selectedMFAScenario: null
        },
        mfaScenariosList: [],
        mfaSegDataList: [],
        willUpdateUI: function(model) {
            this.updateLeftMenu(model);
            if (model && model.mfaScenariosList) {
                this.loadMFACompleteData();
            } else if (model.action === "hideLoadingScreen") {
                kony.adminConsole.utils.hideProgressBar(this.view);
            } else if (model.loadingScreen) {
                if (model.loadingScreen.focus) {
                    kony.adminConsole.utils.showProgressBar(this.view);
                } else {
                    kony.adminConsole.utils.hideProgressBar(this.view);
                }
            } else if (model.fetchMFAScenarios) {
                this.mfaScenariosList = model.fetchMFAScenarios;
                this.mfaSegDataList = model.fetchMFAScenarios;
                this.mapMFAScenarios(model.fetchMFAScenarios);
                var statusFilter = [];
                for (var i = 0; i < model.fetchMFAScenarios.length; i++) {
                    var statusText = model.fetchMFAScenarios[i].mfaScenarioStatusId === STATUS_CONTANTS.active ? kony.i18n.getLocalizedString("i18n.secureimage.Active") : kony.i18n.getLocalizedString("i18n.secureimage.Inactive");
                    if (!statusFilter.contains(statusText)) statusFilter.push(statusText);
                }
                this.setStatusFilterData(statusFilter);
                SELECTED_FILTERS.status = [kony.i18n.getLocalizedString("i18n.secureimage.Active"), kony.i18n.getLocalizedString("i18n.secureimage.Inactive")];
                this.performStatusAppSearchFilter();
            } else if (model.createMFAScenario) {
                if (NAVIGATE_FLAG === "view") {
                    this.backToCallerView();
                } else {
                    this.showMFAScenarios();
                    this.presenter.fetchMFAScenarios();
                }
            } else if (model.toastMessage) {
                if (model.toastMessage.status === kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success")) {
                    this.view.toastMessage.showToastMessage(model.toastMessage.message, this);
                } else if (model.toastMessage.status === kony.i18n.getLocalizedString("i18n.frmGroupsController.error")) {
                    this.view.toastMessage.showErrorToastMessage(model.toastMessage.message, this);
                }
            } else if (model.fetchMFAScenariosById) {
                if (ACTION === ACTION_CONFIG.edit) this.refreshMfaEdit(model.fetchMFAScenariosById);
                else this.refreshMfaView(model.fetchMFAScenariosById);
            }
        },
        initActions: function() {
            var scopeObj = this;
            this.view.segMFAScenarios.onHover = this.saveScreenY.bind(this);
        },
        preshow: function() {
            var scopeObj = this;
            this.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
            scopeObj.view.flxToastMessage.setVisibility(false);
            scopeObj.view.popUpCancelEdits.flxPopUp.skin = "sknFlxFFFFFF1000";
            scopeObj.view.popUpCancelEdits.lblPopUpMainMessage.skin = "sknLbl192b45LatoReg16px";
            scopeObj.view.popUpCancelEdits.lblPopupClose.skin = "sknIcon12px192b45";
            scopeObj.view.popUpCancelEdits.rtxPopUpDisclaimer.skin = "sknrtxLatoRegular485c7512px";
            scopeObj.view.popUpCancelEdits.flxPopUpTopColor.skin = "sknflxebb54cOp100";
            scopeObj.view.popUpCancelEdits.btnPopUpDelete.skin = "sknBtn006CCALatoRegKA";
            scopeObj.view.popUpCancelEdits.btnPopUpDelete.hoverSkin = "sknBtn004F93LatoRegKA";
            scopeObj.view.popUpCancelEdits.btnPopUpDelete.focusSkin = "sknBtn006CCALatoRegKA";
            scopeObj.view.popUpCancelEdits.btnPopUpCancel.skin = "sknBtnLatoRegular8b96a513pxKA";
            scopeObj.view.popUpCancelEdits.btnPopUpCancel.hoverSkin = "sknBtnLatoRegular8b96a513pxKAhover";
            scopeObj.view.popUpCancelEdits.btnPopUpCancel.focusSkin = "sknBtnLatoRegular8b96a513pxKA";
            scopeObj.view.commonButtonsEditMFAConfig.btnCancel.skin = "sknBtnLatoRegular8b96a513pxKA";
            scopeObj.view.commonButtonsEditMFAConfig.btnCancel.hoverSkin = "sknBtnLatoRegular8b96a513pxKAhover";
            scopeObj.view.commonButtonsEditMFAConfig.btnCancel.focusSkin = "sknBtnLatoRegular8b96a513pxKA";
            scopeObj.view.commonButtonsEditMFAConfig.btnSave.skin = "sknBtn006CCALatoRegKA";
            scopeObj.view.commonButtonsEditMFAConfig.btnSave.hoverSkin = "sknBtn004F93LatoRegKA";
            scopeObj.view.commonButtonsEditMFAConfig.btnSave.focusSkin = "sknBtn006CCALatoRegKA";
            scopeObj.view.mainHeader.lblHeading.skin = "sknlblLato485c7522Px";
            scopeObj.view.breadcrumbs.btnBackToMain.focusSkin = "sknBtnLatoReg006CCA10px";
            scopeObj.view.breadcrumbs.btnBackToMain.skin = "sknBtnLatoReg006CCA10px";
            scopeObj.view.breadcrumbs.lblCurrentScreen.skin = "sknlblLatoLight485c7510px";
            this.loadMFACompleteData();
        },
        loadMFACompleteData: function() {
            var scopeObj = this;
            if (!masterData.isLoaded) {
                this.presenter.loadMasterData(function(res) {
                    if (res !== "error") scopeObj.setMasterData(res);
                });
                this.presenter.getMFAVariableReferences(function(res) {
                    if (res !== "error") {
                        var data = res.map(function(rec) {
                            return {
                                lblIdName: rec.Name,
                                code: rec.Code,
                                template: "flxSegVariableOptions"
                            };
                        });
                        scopeObj.view.smsVariableReferencesMenu.segOptionsDropdown.setData(data);
                        scopeObj.view.emailVariableReferencesMenu.segOptionsDropdown.setData(data);
                    }
                });
            } else if (masterData.applications) {
                this.view.statusFilterMenu.segStatusFilterDropdown.setData(masterData.applications.map(function(rec) {
                    return {
                        lblIdName: rec.appName,
                        appId: rec.appId,
                        template: "flxIdResults",
                        selected: false
                    };
                }));
            }
            this.presenter.fetchMFAScenarios();
            this.view.mainHeader.btnAddNewOption.hoverSkin = "sknBtn1682b2LatoBoldffffff13px";
            this.view.mainHeader.btnAddNewOption.focusSkin = "sknBtn205584LatoBoldffffff13px";
            this.view.mainHeader.btnAddNewOption.skin = "sknBtn205584LatoBoldffffff13px";
            this.view.statusFilterMenu.segStatusFilterDropdown.template = "flxIdResults";
            this.view.statusFilterMenu.segStatusFilterDropdown.widgetDataMap = {
                "lblIdName": "lblIdName"
            };
            this.view.smsVariableReferencesMenu.segOptionsDropdown.widgetDataMap = {
                "lblIdName": "lblIdName"
            };
            this.view.emailVariableReferencesMenu.segOptionsDropdown.widgetDataMap = {
                "lblIdName": "lblIdName"
            };
            this.view.statusFilterMenu.isVisible = false;
            this.view.lblAppFilterTitle.text = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.All_Apps");
            SELECTED_FILTERS.appId = "ALL_APPLICATIONS";
            this.showMFAScenarios();
            this.setFlowActions();
        },
        mapMFAScenarios: function(data) {
            var dataMap = {
                "lblMFAConfigName": "lblMFAConfigName",
                "lblIconOptions": "lblIconOptions",
                "lblServiceStatus": "lblServiceStatus",
                "imgServiceStatus": "imgServiceStatus",
                "lblDescription": "lblDescription",
                "lblDescriptionTitle": "lblDescriptionTitle",
                "lblSeperator": "lblSeperator",
                "fontIconImgViewDescription": "fontIconImgViewDescription",
                "flxOptions": "flxOptions"
            };
            this.view.segMFAScenarios.widgetDataMap = dataMap;
            data = this.processMFAScenariosForView(data);
            this.view.segMFAScenarios.setData(data);
            if (data.length > 0) {
                this.view.flxNorecordsFound.setVisibility(false);
                this.view.flxMFAConfigContainer.setVisibility(true);
            } else {
                this.view.flxNorecordsFound.setVisibility(true);
                if (this.inFilter) {
                    this.view.flxNorecordsFound.top = "110dp";
                    this.view.flxNorecordsFound.height = "85%";
                    this.view.flxMFAConfigContainer.setVisibility(true);
                } else {
                    this.view.flxNorecordsFound.top = "60dp";
                    this.view.flxNorecordsFound.height = "90%";
                    this.view.flxMFAConfigContainer.setVisibility(false);
                }
            }
            NAVIGATE_FLAG = "main";
            this.view.forceLayout();
        },
        setMasterData: function(data) {
            var self = this;
            masterData = data;
            this.view.statusFilterMenu.segStatusFilterDropdown.setData(data.applications.map(function(rec) {
                return {
                    lblIdName: rec.appName,
                    appId: rec.appId,
                    template: "flxIdResults",
                    selected: false
                };
            }));
            var sortedDataObject = {
                "applications": self.sortListBoxData(data.applications, "appName"),
                "actionsNonTransactions": self.sortListBoxData(data.actionsNonTransactions, "serviceName"),
                "actionsTransactions": self.sortListBoxData(data.actionsTransactions, "serviceName"),
                "frequencyTypes": self.sortListBoxData(data.frequencyTypes, "frequencyTypeName"),
                "mfaTypes": self.sortListBoxData(data.mfaTypes, "mfaTypeName"),
            };
            this.view.lstBoxSelectApplication.masterData = [
                ["SELECT", kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Select_an_App")]
            ].concat(sortedDataObject.applications.map(function(rec) {
                return [rec.appId, rec.appName];
            }));
            this.view.lbxActivityType.masterData = [
                ["SELECT", kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Activity_Type")]
            ].concat(sortedDataObject.actionsNonTransactions.map(function(rec) {
                return [rec.serviceId, rec.serviceName];
            }));
            this.view.lbxTransactionType.masterData = [
                ["SELECT", kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Select_a_Type")]
            ].concat(sortedDataObject.actionsTransactions.map(function(rec) {
                return [rec.serviceId, rec.serviceName];
            }));
            this.view.lbxFrequency.masterData = [
                ["SELECT", kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Select_a_Frequency")]
            ].concat(sortedDataObject.frequencyTypes.map(function(rec) {
                return [rec.frequencyTypeId, rec.frequencyTypeName];
            }));
            this.view.lbxChallengePrimary.masterData = [
                ["SELECT", kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Select_an_Option")]
            ].concat(sortedDataObject.mfaTypes.map(function(rec) {
                return [rec.mfaTypeId, rec.mfaTypeName];
            }));
            this.view.lbxChallengeBackup.masterData = [
                ["SELECT", kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Select_an_Option")]
            ].concat(sortedDataObject.mfaTypes.map(function(rec) {
                return [rec.mfaTypeId, rec.mfaTypeName];
            }));
        },
        sortListBoxData: function(listBoxData, sortParam) {
            this.sortBy = this.getObjectSorter(sortParam);
            var sortedData = listBoxData.sort(this.sortBy.sortData);
            return sortedData;
        },
        setFlowActions: function() {
            var scopeObj = this;
            this.view.flxDeactivate.onClick = this.handleMFAActivateDeactivateClick.bind(this);
            this.view.flxDeactivateView.onClick = this.handleMFAActivateDeactivateClick.bind(this);
            this.view.flxDelete.onClick = this.handleMFAScenarioDeleteClick.bind(this);
            this.view.flxDeleteView.onClick = this.handleMFAScenarioDeleteClick.bind(this);
            this.view.flxAppFilterContainer.onClick = function() {
                scopeObj.view.statusFilterMenu.isVisible = true;
            };
            this.view.statusFilterMenu.onHover = this.onDropDownsHoverCallback;
            this.view.flxProductStatusFilter.onHover = this.onDropDownsHoverCallback;
            this.view.smsVariableReferencesMenu.onHover = this.onDropDownsHoverCallback;
            this.view.flxHeaderSMS.onHover = function(widget, context) {
                scopeObj.onDropDownsHoverCallbackLeave(scopeObj.view.smsVariableReferencesMenu, context);
            };
            this.view.btnSMSVariableReferences.onClick = function() {
                scopeObj.view.smsVariableReferencesMenu.isVisible = true;
            };
            this.view.emailVariableReferencesMenu.onHover = this.onDropDownsHoverCallback;
            this.view.flxHeaderEmail.onHover = function(widget, context) {
                scopeObj.onDropDownsHoverCallbackLeave(scopeObj.view.emailVariableReferencesMenu, context);
            };
            this.view.btnEmailVariableReferences.onClick = function() {
                scopeObj.view.emailVariableReferencesMenu.isVisible = true;
            };
            this.view.flxSelectOptions.isVisible = false;
            this.view.flxSelectOptions.onHover = this.onDropDownsHoverCallback;
            this.view.flxSelectOptionsView.isVisible = false;
            this.view.flxSelectOptionsView.onHover = this.onDropDownsHoverCallback;
            this.view.flxViewContent.onScrollStart = function() {
                scopeObj.view.flxSelectOptionsView.isVisible = false;
            };
            this.view.flxEdit.onClick = this.handleMFAConfigEditClick.bind(this);
            this.view.flxEditView.onClick = this.handleMFAConfigEditClick.bind(this);
            this.view.commonButtonsEditMFAConfig.btnCancel.onClick = function() {
                var msg = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.cancel_mfa_configs_message");
                var yesText = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.YES_CANCEL");
                var cancelText = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.NO_LEAVE_AS_IS");
                scopeObj.showAlertPopup("Cancel Changes", msg, cancelText, yesText, function() {
                    scopeObj.editPopupYesClickHandler();
                });
            };
            this.view.commonButtonsEditMFAConfig.btnSave.onClick = function() {
                scopeObj.handleCreateUpdateScenario();
            };
            this.view.flxTransactional.onClick = function() {
                scopeObj.transactionTypeClick();
            };
            this.view.flxNonTransactional.onClick = function() {
                scopeObj.nonTransactionTypeClick();
            };
            this.view.lstBoxSelectApplication.onSelection = function(eventObject) {
                mfaScenarioState = Object.assign({}, mfaScenarioState, {
                    appId: eventObject.selectedKey
                });
            };
            this.view.lbxChallengePrimary.onSelection = function(eventObject) {
                mfaScenarioState = Object.assign({}, mfaScenarioState, {
                    primaryMFATypeId: eventObject.selectedKey
                });
                scopeObj.changeMessageTemplateDisplay();
            };
            this.view.lbxChallengeBackup.onSelection = function(eventObject) {
                mfaScenarioState = Object.assign({}, mfaScenarioState, {
                    secondaryMFATypeId: eventObject.selectedKey
                });
                scopeObj.changeMessageTemplateDisplay();
            };
            this.view.lbxFrequency.onSelection = function(eventObject) {
                mfaScenarioState = Object.assign({}, mfaScenarioState, {
                    frequencyTypeId: eventObject.selectedKey
                });
                if (mfaScenarioState.frequencyTypeId === "VALUE_BASED") {
                    scopeObj.view.flxValue.isVisible = true;
                } else {
                    scopeObj.view.flxValue.isVisible = false;
                }
            };
            this.view.lbxTransactionType.onSelection = function(eventObject) {
                mfaScenarioState = Object.assign({}, mfaScenarioState, {
                    serviceId: eventObject.selectedKey
                });
            };
            this.view.lbxActivityType.onSelection = function(eventObject) {
                mfaScenarioState = Object.assign({}, mfaScenarioState, {
                    serviceId: eventObject.selectedKey
                });
            };
            this.view.switchMFAConfigStatus.onSlide = function(eventobject) {
                mfaScenarioState = Object.assign({}, mfaScenarioState, {
                    mfaScenarioStatusId: eventobject.selectedIndex === 1 ? STATUS_CONTANTS.inactive : STATUS_CONTANTS.active
                });
                scopeObj.view.lblMFAConfigStatus.text = mfaScenarioState.mfaScenarioStatusId === STATUS_CONTANTS.active ? "Status: " + kony.i18n.getLocalizedString("i18n.secureimage.Active") : "Status: " + kony.i18n.getLocalizedString("i18n.frmPermissionsController.InActive");
            };
            this.view.mainHeader.btnAddNewOption.onClick = function() {
                scopeObj.createSceanrioClickHandler();
            };
            this.view.popUpCancelEdits.btnPopUpCancel.onClick = function() {
                scopeObj.editPopupCancelClickHandler();
            };
            this.view.popUpCancelEdits.btnPopUpDelete.onClick = function() {
                scopeObj.editPopupYesClickHandler();
            };
            this.view.popUpCancelEdits.flxPopUpClose.onClick = function() {
                scopeObj.view.flxEditCancelConfirmation.isVisible = false;
            };
            this.view.subHeader.tbxSearchBox.onBeginEditing = function() {
                scopeObj.view.subHeader.flxSearchContainer.skin = "sknflx0cc44f028949b4cradius30px";
            };
            this.view.subHeader.tbxSearchBox.onEndEditing = function() {
                scopeObj.view.subHeader.flxSearchContainer.skin = "sknflxBgffffffBorderc1c9ceRadius30px";
            };
            this.view.subHeader.tbxSearchBox.onKeyUp = function() {
                if (scopeObj.view.subHeader.tbxSearchBox.text === "") scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
                else scopeObj.view.subHeader.flxClearSearchImage.setVisibility(true);
                var text = scopeObj.view.subHeader.tbxSearchBox.text;
                if (text) {
                    scopeObj.mapMFAScenarios(scopeObj.mfaScenariosList.filter(function(rec) {
                        return rec.service.serviceName.toLowerCase().indexOf(text.toLowerCase()) >= 0;
                    }));
                    if (scopeObj.view.segMFAScenarios.data.length > 0) {
                        scopeObj.view.rtxNoResultsFound.isVisible = false;
                        scopeObj.view.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.NoResultsFound");
                    } else {
                        scopeObj.view.rtxNoResultsFound.isVisible = true;
                        scopeObj.view.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.NoResultsFound");
                    }
                    scopeObj.mfaSegDataList = scopeObj.view.segMFAScenarios.data;
                    scopeObj.inFilter = false;
                    scopeObj.view.statusFilterMenu2.segStatusFilterDropdown.selectedIndices = [
                        [0, [0, 1]]
                    ];
                } else {
                    scopeObj.mfaSegDataList = scopeObj.mfaScenariosList;
                    scopeObj.performStatusFilter();
                    scopeObj.view.subHeader.flxClearSearchImage.isVisible = false;
                }
            };
            this.view.subHeader.flxClearSearchImage.onClick = function() {
                scopeObj.view.subHeader.tbxSearchBox.text = "";
                scopeObj.view.subHeader.flxClearSearchImage.isVisible = false;
                scopeObj.view.subHeader.tbxSearchBox.onKeyUp();
            };
            this.view.breadcrumbs.btnBackToMain.onClick = function() {
                scopeObj.showMFAScenarios();
                scopeObj.presenter.fetchMFAScenarios();
            };
            this.view.statusFilterMenu.segStatusFilterDropdown.onRowClick = this.onAppFilterOptionSelection.bind(this);
            this.view.txtSubject.onBeginEditing = function() {
                scopeObj.view.flxSubject.skin = "sknflxffffffop100Border006ccaRadius3px";
            };
            this.view.txtSubject.onEndEditing = function() {
                scopeObj.view.flxSubject.skin = "sknflxffffffop100Bordercbcdd1Radius3px";
            };
            this.view.flxHeaderStatus.onClick = function() {
                var filterLeft = scopeObj.view.flxHeaderStatus.frame.x;
                scopeObj.view.flxProductStatusFilter.left = (filterLeft - 25) + "px";
                if (scopeObj.view.flxProductStatusFilter.isVisible) {
                    scopeObj.view.flxProductStatusFilter.setVisibility(false);
                } else {
                    scopeObj.view.flxProductStatusFilter.setVisibility(true);
                }
                scopeObj.view.forceLayout();
            };
            this.view.statusFilterMenu2.segStatusFilterDropdown.onRowClick = function() {
                scopeObj.performStatusFilter();
            };
            this.view.smsVariableReferencesMenu.segOptionsDropdown.onRowClick = function() {
                scopeObj.view.smsVariableReferencesMenu.isVisible = false;
                var cursorPos;
                var strLength;
                var data = scopeObj.view.smsVariableReferencesMenu.segOptionsDropdown.data[scopeObj.view.smsVariableReferencesMenu.segOptionsDropdown.selectedIndex[1]];
                cursorPos = document.getElementById("frmMFAScenarios_txtSMSContent").selectionStart;
                strLength = scopeObj.view.txtSMSContent.text.length;
                scopeObj.view.txtSMSContent.text = scopeObj.view.txtSMSContent.text.substring(0, cursorPos) + "[#]" + data.lblIdName + "[/#]" + scopeObj.view.txtSMSContent.text.substring(cursorPos, strLength);
            };
            this.view.emailVariableReferencesMenu.segOptionsDropdown.onRowClick = function() {
                scopeObj.view.emailVariableReferencesMenu.isVisible = false;
                var data = scopeObj.view.emailVariableReferencesMenu.segOptionsDropdown.data[scopeObj.view.emailVariableReferencesMenu.segOptionsDropdown.selectedIndex[1]];
                document.getElementById("iframe_rtxMessage").contentWindow.document.execCommand("insertText", false, "[#]" + data.lblIdName + "[/#]");
            };
            this.view.flxOptions.onClick = function() {
                scopeObj.toggleVisibilityFromView();
            };
        },
        toggleVisibilityFromView: function() {
            if (this.view.flxSelectOptionsView.isVisible) this.view.flxSelectOptionsView.isVisible = false;
            else {
                if (CURRENT_SELECTION.statusId === STATUS_CONTANTS.active) {
                    this.view.lblActOrDeact.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate");
                    this.view.fontIconActOrDeact.text = "";
                } else {
                    this.view.lblActOrDeact.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Activate");
                    this.view.fontIconActOrDeact.text = "";
                }
                this.view.flxSelectOptionsView.isVisible = true;
            }
            this.view.forceLayout();
        },
        onAppFilterOptionSelection: function() {
            var index = this.view.statusFilterMenu.segStatusFilterDropdown.selectedIndex;
            var rowIndex = index[1];
            var data = this.view.statusFilterMenu.segStatusFilterDropdown.data[rowIndex];
            this.view.lblAppFilterTitle.text = data.lblIdName;
            this.view.statusFilterMenu.isVisible = false;
            var appsList = masterData.applications.map(function(rec) {
                return {
                    lblIdName: rec.appName,
                    appId: rec.appId,
                    template: "flxIdResults"
                };
            });
            var updatedData = appsList.filter(function(rec) {
                return rec.appId != data.appId;
            });
            SELECTED_FILTERS.appId = data.appId;
            if (data.appId != "ALL_APPLICATIONS") {
                updatedData = [{
                    lblIdName: kony.i18n.getLocalizedString("i18n.frmMFAScenarios.All_Apps"),
                    appId: "ALL_APPLICATIONS",
                    template: "flxIdResults"
                }].concat(updatedData);
            } else {
                //this.mapMFAScenarios(this.mfaScenariosList);        
            }
            this.view.statusFilterMenu.segStatusFilterDropdown.setData(updatedData);
            this.performStatusAppSearchFilter();
        },
        showSelectedRow: function() {
            var index = this.view.segMFAScenarios.selectedIndex;
            var rowIndex = index[1];
            var data = this.view.segMFAScenarios.data;
            this.view.segMFAScenarios.setData(data);
            var segData = this.view.segMFAScenarios.data[rowIndex];
            CURRENT_SELECTION.appId = segData.app.appId;
            CURRENT_SELECTION.actionId = segData.service.serviceId;
            CURRENT_SELECTION.statusId = segData.mfaScenarioStatusId;
            this.handleMFAViewClick();
            this.view.forceLayout();
        },
        processMFAScenariosForView: function(mfaScenarios) {
            if (mfaScenarios && mfaScenarios instanceof Array && mfaScenarios.length > 0) {
                return mfaScenarios.map(function(rec, index) {
                    var obj = Object.assign({}, rec);
                    obj.imgServiceStatus = rec.mfaScenarioStatusId === STATUS_CONTANTS.active ? "active_circle2x.png" : "inactive_circel2x.png";
                    obj.lblServiceStatus = rec.mfaScenarioStatusId === STATUS_CONTANTS.active ? {
                        "text": kony.i18n.getLocalizedString("i18n.secureimage.Active"),
                        "skin": "sknlblLato5bc06cBold14px"
                    } : {
                        "text": kony.i18n.getLocalizedString("i18n.frmPermissionsController.InActive"),
                        "skin": "sknlblLatoDeactive"
                    };
                    obj.lblIconOptions = {
                        text: ""
                    };
                    obj.template = "flxSegMFAConfigs";
                    obj.fontIconImgViewDescription = {
                        text: "",
                        visible: false
                    };
                    obj.lblMFAConfigName = rec.app.appName + " | " + rec.service.serviceName;
                    obj.lblDescription = rec.mfaScenarioDescription;
                    obj.lblDescriptionTitle = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Description").toUpperCase();
                    obj.lblSeperator = ".";
                    return obj;
                });
            }
            return [];
        },
        saveScreenY: function(widget, context) {
            mouseYCoordinate = context.screenY;
        },
        fixContextualMenu: function(heightVal) {
            if (((this.view.flxSelectOptions.frame.height + heightVal) > (this.view.segMFAScenarios.frame.height + 50)) && this.view.flxSelectOptions.frame.height < this.view.segMFAScenarios.frame.height) {
                this.view.flxSelectOptions.top = ((heightVal - this.view.flxSelectOptions.frame.height) - 39) + "px";
            } else {
                this.view.flxSelectOptions.top = (heightVal) + "px";
            }
            this.view.forceLayout();
        },
        onDropDownsHoverCallback: function(widget, context) {
            var self = this;
            var widgetId = widget.id;
            if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
                self.view[widgetId].setVisibility(true);
            } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                self.view[widgetId].setVisibility(false);
            }
            self.view.forceLayout();
        },
        onDropDownsHoverCallbackLeave: function(widget, context) {
            var self = this;
            var widgetId = widget.id;
            if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                self.view[widgetId].setVisibility(false);
            }
            self.view.forceLayout();
        },
        onVariableRefsDropDownsHoverCallback: function(widget, context) {
            var self = this;
            var widgetId = widget.id;
            if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
                self.view[widgetId].setVisibility(true);
            } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                self.view[widgetId].setVisibility(false);
            }
            self.view.forceLayout();
        },
        toggleVisibility: function() {
            var hgtValue;
            var selItems = this.view.segMFAScenarios.selectedItems[0];
            this.gblselIndex = this.view.segMFAScenarios.selectedIndex[1];
            var clckd_selectedRowIndex = this.view.segMFAScenarios.selectedRowIndex[1];
            kony.print("clckd_selectedRowIndex----" + JSON.stringify(clckd_selectedRowIndex));
            CURRENT_SELECTION.appId = selItems.app.appId;
            CURRENT_SELECTION.actionId = selItems.service.serviceId;
            CURRENT_SELECTION.statusId = selItems.mfaScenarioStatusId;
            if (this.view.flxSelectOptions.isVisible) {
                this.view.flxSelectOptions.setVisibility(false);
                this.view.forceLayout();
            } else {
                if (selItems.mfaScenarioStatusId === STATUS_CONTANTS.active) {
                    if (this.view.flxSelectOptions.isVisible === false) {
                        this.gblsegRoles = clckd_selectedRowIndex;
                        hgtValue = ((clckd_selectedRowIndex + 1) * 50) + 65 - this.view.segMFAScenarios.contentOffsetMeasured.y;
                        kony.print("hgtValue in roles------" + hgtValue);
                        this.view.flxSelectOptions.top = mouseYCoordinate - 148 + "px";
                        this.view.flxSelectOptions.setVisibility(true);
                        this.view.forceLayout();
                        this.view.lblOption2.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate");
                        this.view.fonticonDeactive.text = "";
                        this.fixContextualMenu(mouseYCoordinate - 148);
                    }
                } else {
                    if (this.view.flxSelectOptions.isVisible === false) {
                        this.gblsegRoles = clckd_selectedRowIndex;
                        hgtValue = ((clckd_selectedRowIndex + 1) * 50) + 65 - this.view.segMFAScenarios.contentOffsetMeasured.y;
                        kony.print("hgtValue in permissions------" + hgtValue);
                        this.view.flxSelectOptions.top = mouseYCoordinate - 148 + "px";
                        this.view.flxSelectOptions.setVisibility(true);
                        this.view.forceLayout();
                        this.view.lblOption2.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Activate");
                        this.view.fonticonDeactive.text = "";
                        this.fixContextualMenu(mouseYCoordinate - 148);
                    }
                }
            }
            //       if (this.view.flxSelectOptions.isVisible)
            //         this.view.flxSelectOptions.isVisible = false;
            //       else
            //         this.view.flxSelectOptions.isVisible = true;
            //       this.view.forceLayout();
        },
        createSceanrioClickHandler: function() {
            ACTION = ACTION_CONFIG.create;
            this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Create_Multi_Factor_Authentication_Scenario");
            this.view.commonButtonsEditMFAConfig.btnSave.text = "CREATE";
            this.view.breadcrumbs.lblCurrentScreen.text = "NEW SCENARIO";
            this.view.lblMFAScenarioName.text = "New Scenario Rule";
            this.view.flxScenarioTypeSelection.setEnabled(true);
            this.view.flxTransactionType.lbxTransactionType.setEnabled(true);
            this.view.flxActivityType.lbxActivityType.setEnabled(true);
            this.view.lstBoxSelectApplication.setEnabled(true);
            this.view.flxScenarioTypeSelection.opacity = 1;
            this.view.flxTransactionType.lbxTransactionType.opacity = 1;
            this.view.flxActivityType.lbxActivityType.opacity = 1;
            this.view.lstBoxSelectApplication.opacity = 1;
            this.resetErrorState();
            this.showMFAScenarioEdit({
                "appId": "SELECT",
                "serviceType": "SELECT",
                "serviceId": "SELECT",
                "frequencyTypeId": "SELECT",
                "frequencyValue": "",
                "mfaScenarioDescription": "",
                "primaryMFATypeId": "SELECT",
                "secondaryMFATypeId": "SELECT",
                "smsText": "",
                "emailSubject": "",
                "emailBody": "",
                "mfaScenarioStatusId": STATUS_CONTANTS.active
            });
        },
        handleMFAConfigEditClick: function() {
            ACTION = ACTION_CONFIG.edit;
            this.backToCallerView();
        },
        refreshMfaEdit: function(data) {
            data = data[0];
            this.view.flxSelectOptions.isVisible = false;
            this.view.flxSelectOptionsView.isVisible = false;
            var segIndex = this.view.segMFAScenarios.selectedRowIndex[1];
            var segData = this.view.segMFAScenarios.data[segIndex];
            this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Edit_Multi_Factor_Authentication_Scenario");
            this.view.commonButtonsEditMFAConfig.btnSave.text = "UPDATE";
            this.view.breadcrumbs.lblCurrentScreen.text = segData.service.serviceName.toUpperCase();
            this.view.lblMFAScenarioName.text = segData.app.appName + " | " + segData.service.serviceName;
            this.view.flxScenarioTypeSelection.setEnabled(false);
            this.view.flxTransactionType.lbxTransactionType.setEnabled(false);
            this.view.flxActivityType.lbxActivityType.setEnabled(false);
            this.view.lstBoxSelectApplication.setEnabled(false);
            this.view.flxScenarioTypeSelection.opacity = 0.5;
            this.view.flxTransactionType.lbxTransactionType.opacity = 0.5;
            this.view.flxActivityType.lbxActivityType.opacity = 0.5;
            this.view.lstBoxSelectApplication.opacity = 0.5;
            this.resetErrorState();
            this.showMFAScenarioEdit({
                "appId": data.app.appId,
                "serviceType": data.service.serviceType,
                "serviceId": data.service.serviceId,
                "serviceName": data.service.serviceName,
                "frequencyTypeId": data.frequencyType.frequencyTypeId,
                "frequencyValue": data.frequencyType.frequencyValue,
                "mfaScenarioDescription": data.mfaScenarioDescription,
                "primaryMFATypeId": data.primaryMFATypeId,
                "secondaryMFATypeId": data.secondaryMFATypeId,
                "smsText": data.smsText,
                "emailSubject": data.emailSubject,
                "emailBody": data.emailBody,
                "mfaScenarioStatusId": data.mfaScenarioStatusId,
                "appActionId": data.appActionId,
                "appActionMFAId": data.appActionMFAId
            });
        },
        showMFAScenarioEdit: function(data) {
            var scopeobj = this;
            mfaScenarioState = Object.assign({}, {}, data);
            this.hideTemplateOptions();
            this.view.mainHeader.btnAddNewOption.isVisible = false;
            this.view.flxValue.isVisible = false;
            this.view.lstBoxSelectApplication.selectedKey = mfaScenarioState.appId;
            this.view.lbxActivityType.selectedKey = mfaScenarioState.serviceId;
            this.view.lbxTransactionType.selectedKey = mfaScenarioState.serviceId;
            this.view.lbxFrequency.selectedKey = mfaScenarioState.frequencyTypeId;
            this.view.lbxChallengePrimary.selectedKey = mfaScenarioState.primaryMFATypeId;
            this.view.lbxChallengeBackup.selectedKey = mfaScenarioState.secondaryMFATypeId;
            this.view.tbxEnterValue.text = mfaScenarioState.frequencyValue;
            this.view.tbxDescription.text = mfaScenarioState.mfaScenarioDescription;
            this.view.txtSMSContent.text = mfaScenarioState.smsText;
            this.view.txtSubject.text = mfaScenarioState.emailSubject;
            var messageDocument = document.getElementById("iframe_rtxMessage").contentWindow.document;
            messageDocument.getElementById("editor").innerHTML = mfaScenarioState.emailBody;
            messageDocument.getElementsByClassName("table-palette")[0].style.marginLeft = "-180px";
            if (mfaScenarioState.serviceType === "SER_TYPE_TRNS") {
                this.transactionTypeClick();
            } else if (mfaScenarioState.serviceType === "SER_TYPE_NONTRANS") {
                this.nonTransactionTypeClick();
            } else {
                this.view.flxTransactional.skin = "slFbox";
                this.view.flxCircleTransaction.skin = "sknCircleGrey";
                this.view.lblTransactional.skin = "sknlblLato485c7514px";
                this.view.flxNonTransactional.skin = "slFbox";
                this.view.flxCircleNonTransaction.skin = "sknCircleGrey";
                this.view.lblNonTransactional.skin = "sknlblLato485c7514px";
                this.view.flxActivityType.isVisible = false;
                this.view.flxTransactionType.isVisible = false;
                this.view.flxFrequency.isVisible = false;
                this.view.flxValue.isVisible = false;
                this.view.lblSelectScenarioType.isVisible = true;
            }
            this.view.lblMFAConfigStatus.text = mfaScenarioState.mfaScenarioStatusId === STATUS_CONTANTS.active ? "Status: " + kony.i18n.getLocalizedString("i18n.secureimage.Active") : "Status: " + kony.i18n.getLocalizedString("i18n.frmPermissionsController.InActive");
            this.view.switchMFAConfigStatus.selectedIndex = mfaScenarioState.mfaScenarioStatusId === STATUS_CONTANTS.inactive ? 1 : 0;
            this.view.flxMFAScenarios.isVisible = false;
            this.view.flxViewMFAScenario.isVisible = false;
            this.view.flxEditMFAScenario.isVisible = true;
            this.view.flxMainSubHeader.isVisible = false;
            this.view.flxBreadcrumb.isVisible = true;
            this.view.lbxFrequency.onSelection(this.view.lbxFrequency);
            this.changeMessageTemplateDisplay();
            this.view.flxEditContent.scrollToWidget(this.view.flxRowHeader);
            this.view.forceLayout();
        },
        handleMFAViewClick: function() {
            ACTION = ACTION_CONFIG.view;
            this.backToCallerView();
        },
        showMFAScenarioView: function(data) {
            NAVIGATE_FLAG = "view";
            mfaScenarioState = Object.assign({}, {}, data);
            this.view.lblServiceStatus.skin = mfaScenarioState.mfaScenarioStatusId === STATUS_CONTANTS.active ? "sknlblLato5bc06cBold14px" : "sknlblLatocacacaBold12px";
            this.view.imgServiceStatus.skin = mfaScenarioState.mfaScenarioStatusId === STATUS_CONTANTS.active ? "sknFontIconActivate" : "sknfontIconInactive";
            var statusText = mfaScenarioState.mfaScenarioStatusId === STATUS_CONTANTS.active ? kony.i18n.getLocalizedString("i18n.secureimage.Active") : kony.i18n.getLocalizedString("i18n.secureimage.Inactive");
            this.view.lblServiceStatus.text = statusText;
            this.view.details.lblData1.text = mfaScenarioState.appName;
            var scenarioTypetext = mfaScenarioState.serviceType === "SER_TYPE_TRNS" ? kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Transactional") : kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Non_Transactional");
            this.view.details.lblData2.text = scenarioTypetext;
            this.view.details.lblData3.text = mfaScenarioState.serviceName;
            this.view.details2.lblData1.text = mfaScenarioState.frequencyTypeName;
            this.view.details2.lblData2.text = "$" + mfaScenarioState.frequencyValue;
            this.view.lblDescVal.text = mfaScenarioState.mfaScenarioDescription;
            this.view.details3.lblData1.text = mfaScenarioState.primaryMFAType;
            this.view.details3.lblData2.text = mfaScenarioState.secondaryMFAType;
            this.view.lblSmsContent.text = mfaScenarioState.smsText;
            this.view.lblSubjectEmailContent.text = kony.i18n.getLocalizedString("i18n.userwidgetmodel.lblSubject") + " " + mfaScenarioState.emailSubject;
            // Assign Email body to rtxViewer
            if (document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer")) {
                document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML = mfaScenarioState.emailBody;
            } else {
                if (!document.getElementById("iframe_rtxViewer").newOnload) {
                    document.getElementById("iframe_rtxViewer").newOnload = document.getElementById("iframe_rtxViewer").onload;
                }
                document.getElementById("iframe_rtxViewer").onload = function() {
                    document.getElementById("iframe_rtxViewer").newOnload();
                    document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML = mfaScenarioState.emailBody;
                };
            }
            this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.View_Multi_Factor_Authentication_Scenario");
            this.view.breadcrumbs.lblCurrentScreen.text = mfaScenarioState.serviceName.toUpperCase();
            this.view.lblMFAViewHeading.text = mfaScenarioState.appName + " | " + mfaScenarioState.serviceName;
            this.view.flxBreadcrumb.isVisible = true;
            this.view.flxSelectOptionsView.setVisibility(false);
            this.view.mainHeader.btnAddNewOption.setVisibility(false);
            this.view.flxViewMFAScenario.setVisibility(true);
            this.view.flxMFAScenarios.setVisibility(false);
            /* 1)If service type is "transactional" and frequency is "Always" no need to show "Value Above" field.
               2) If service type is "non transactional" then no need to show "Frequency" and "Value Above" field. 
               3)If mfa type is not secure access code no need to show template fields.*/
            if (mfaScenarioState.serviceType === "SER_TYPE_TRNS" && mfaScenarioState.frequencyTypeId == "ALWAYS") {
                this.view.flxViewRow2.setVisibility(true);
                this.view.details2.flxColumn2.setVisibility(false);
            } else if (mfaScenarioState.serviceType === "SER_TYPE_NONTRANS") {
                this.view.flxViewRow2.setVisibility(false);
            } else {
                this.view.flxViewRow2.setVisibility(true);
                this.view.details2.flxColumn2.setVisibility(true);
            }
            if (mfaScenarioState.primaryMFATypeId === "SECURE_ACCESS_CODE" || mfaScenarioState.secondaryMFATypeId === "SECURE_ACCESS_CODE") {
                this.showTemplateOptionsForView();
            } else {
                this.hideTemplateOptionsForView();
            }
            this.view.forceLayout();
        },
        showTemplateOptionsForView: function() {
            this.view.lblMfaChallengeTypeSubHeader.setVisibility(true);
            this.view.flxViewRow4.setVisibility(true);
            this.view.flxViewRow5.setVisibility(true);
            this.view.flxViewRow6.setVisibility(true);
        },
        hideTemplateOptionsForView: function() {
            this.view.lblMfaChallengeTypeSubHeader.setVisibility(false);
            this.view.flxViewRow4.setVisibility(false);
            this.view.flxViewRow5.setVisibility(false);
            this.view.flxViewRow6.setVisibility(false);
        },
        showMFAScenarios: function() {
            this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Multi_Factor_Authentication_Scenarios");
            this.view.flxMFAScenarios.isVisible = true;
            this.view.flxEditMFAScenario.isVisible = false;
            this.view.flxViewMFAScenario.isVisible = false;
            this.view.flxMainSubHeader.isVisible = true;
            this.view.flxBreadcrumb.isVisible = false;
            this.view.mainHeader.btnAddNewOption.isVisible = true;
        },
        transactionTypeClick: function() {
            mfaScenarioState = Object.assign({}, mfaScenarioState, {
                serviceType: "SER_TYPE_TRNS"
            });
            this.view.flxTransactional.skin = "sknFlx006CCA0pxRad";
            this.view.flxCircleTransaction.skin = "sknCircleBlue";
            this.view.lblTransactional.skin = "lblfffffflatoregular14px";
            this.view.flxNonTransactional.skin = "slFbox";
            this.view.flxCircleNonTransaction.skin = "sknCircleGrey";
            this.view.lblNonTransactional.skin = "sknlblLato485c7514px";
            this.view.flxActivityType.isVisible = false;
            this.view.flxTransactionType.isVisible = true;
            this.view.flxFrequency.isVisible = true;
            this.view.lblSelectScenarioType.isVisible = false;
        },
        nonTransactionTypeClick: function() {
            mfaScenarioState = Object.assign({}, mfaScenarioState, {
                serviceType: "SER_TYPE_NONTRANS"
            });
            this.view.flxNonTransactional.skin = "sknFlx006CCA0pxRad";
            this.view.flxCircleNonTransaction.skin = "sknCircleBlue";
            this.view.lblNonTransactional.skin = "lblfffffflatoregular14px";
            this.view.flxTransactional.skin = "slFbox";
            this.view.flxCircleTransaction.skin = "sknCircleGrey";
            this.view.lblTransactional.skin = "sknlblLato485c7514px";
            this.view.flxActivityType.isVisible = true;
            this.view.flxTransactionType.isVisible = false;
            this.view.flxFrequency.isVisible = false;
            this.view.flxValue.isVisible = false;
            this.view.lblSelectScenarioType.isVisible = false;
        },
        changeMessageTemplateDisplay: function() {
            if (mfaScenarioState.primaryMFATypeId === "SECURE_ACCESS_CODE" || mfaScenarioState.secondaryMFATypeId === "SECURE_ACCESS_CODE") {
                this.showTemplateOptions();
            } else {
                this.hideTemplateOptions();
            }
        },
        showTemplateOptions: function() {
            this.view.lblMessageContentTemplateTitle.isVisible = true;
            this.view.lblMessageContentTemplateNote.isVisible = true;
            this.view.flxRow6.isVisible = true;
            this.view.flxRow7.isVisible = true;
        },
        hideTemplateOptions: function() {
            this.view.lblMessageContentTemplateTitle.isVisible = false;
            this.view.lblMessageContentTemplateNote.isVisible = false;
            this.view.flxRow6.isVisible = false;
            this.view.flxRow7.isVisible = false;
        },
        handleCreateUpdateScenario: function() {
            var messageText = document.getElementById("iframe_rtxMessage").contentWindow.document.getElementById("editor").innerHTML;
            mfaScenarioState = Object.assign({}, mfaScenarioState, {
                frequencyValue: this.view.tbxEnterValue.text,
                mfaScenarioDescription: this.view.tbxDescription.text,
                smsText: this.view.txtSMSContent.text,
                emailSubject: this.view.txtSubject.text,
                emailBody: messageText,
                frequencyTypeId: this.view.lbxFrequency.selectedKey
            });
            if (this.isValidPayload(mfaScenarioState)) {
                if (ACTION === ACTION_CONFIG.create) {
                    this.presenter.createMFAScenario(mfaScenarioState);
                } else {
                    ACTION = ACTION_CONFIG.view;
                    this.presenter.updateMFAScenario(mfaScenarioState);
                }
            }
        },
        editPopupCancelClickHandler: function() {
            this.view.flxEditCancelConfirmation.isVisible = false;
        },
        editPopupYesClickHandler: function() {
            this.view.flxEditCancelConfirmation.isVisible = false;
            if (NAVIGATE_FLAG !== "view") {
                this.showMFAScenarios();
            } else {
                this.view.flxViewMFAScenario.setVisibility(true);
                this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.View_Multi_Factor_Authentication_Scenario");
            }
        },
        /**
         * Called when clicked on activate.deactivate option from options menu.
         */
        handleMFAActivateDeactivateClick: function() {
            var scopeObj = this;
            this.view.flxSelectOptions.isVisible = false;
            this.view.flxSelectOptionsView.isVisible = false;
            var segIndex = this.view.segMFAScenarios.selectedRowIndex[1];
            var segData = this.view.segMFAScenarios.data[segIndex];
            Object.assign(mfaScenarioState, {
                "appId": segData.app.appId,
                "serviceType": segData.service.serviceType,
                "serviceId": segData.service.serviceId,
                "frequencyTypeId": segData.frequencyType.frequencyTypeId,
                "frequencyValue": segData.frequencyType.frequencyValue,
                "mfaScenarioDescription": segData.mfaScenarioDescription,
                "primaryMFATypeId": segData.primaryMFATypeId,
                "secondaryMFATypeId": segData.secondaryMFATypeId,
                "smsText": segData.smsText,
                "emailSubject": segData.emailSubject,
                "emailBody": segData.emailBody,
                "mfaScenarioStatusId": segData.mfaScenarioStatusId,
                "appActionId": segData.appActionId,
                "appActionMFAId": segData.appActionMFAId
            });
            if (CURRENT_SELECTION.statusId === STATUS_CONTANTS.active) {
                var heading = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.deactivate_popup_header");
                var msg = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.deactivate_popup_content");
                var yesText = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.YES_DEACTIVATE");
                var cancelText = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.NO_LEAVE_AS_IS");
                this.showAlertPopup(heading, msg, cancelText, yesText, function() {
                    mfaScenarioState.mfaScenarioStatusId = STATUS_CONTANTS.inactive;
                    scopeObj.presenter.updateMFAScenario(mfaScenarioState);
                });
            } else {
                mfaScenarioState.mfaScenarioStatusId = STATUS_CONTANTS.active;
                this.presenter.updateMFAScenario(mfaScenarioState);
            }
        },
        handleMFAScenarioDeleteClick: function() {
            var scopeObj = this;
            this.view.flxSelectOptions.isVisible = false;
            var heading = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.delete_popup_header");
            var msg = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.delete_popup_content");
            var yesText = kony.i18n.getLocalizedString("i18n.frmMFAScenarios.YES_DELETE");
            var cancelText = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.NO_LEAVE_AS_IS");
            NAVIGATE_FLAG = "main";
            this.showAlertPopup(heading, msg, cancelText, yesText, function() {
                var segIndex = scopeObj.view.segMFAScenarios.selectedRowIndex[1];
                var segData = scopeObj.view.segMFAScenarios.data[segIndex];
                scopeObj.presenter.deleteMFAScenario({
                    "appActionId": segData.appActionId,
                    "appActionMFAId": segData.appActionMFAId
                });
            });
        },
        showAlertPopup: function(headerMsg, contentMsg, cancelText, yesText, yesCallback) {
            var scopeObj = this;
            this.view.popUpCancelEdits.lblPopUpMainMessage.text = headerMsg;
            this.view.popUpCancelEdits.rtxPopUpDisclaimer.text = contentMsg;
            this.view.popUpCancelEdits.btnPopUpCancel.text = cancelText;
            this.view.popUpCancelEdits.btnPopUpDelete.text = yesText;
            if (yesText.length > 12) {
                this.view.popUpCancelEdits.btnPopUpCancel.right = "170px";
            } else {
                this.view.popUpCancelEdits.btnPopUpCancel.right = "150px";
            }
            this.view.popUpCancelEdits.btnPopUpDelete.onClick = function() {
                scopeObj.view.flxEditCancelConfirmation.isVisible = false;
                yesCallback();
            };
            this.view.flxEditCancelConfirmation.isVisible = true;
            this.view.popUpCancelEdits.forceLayout();
        },
        setStatusFilterData: function(data) {
            var self = this;
            var widgetMap = {
                "flxSearchDropDown": "flxSearchDropDown",
                "flxCheckBox": "flxCheckBox",
                "imgCheckBox": "imgCheckBox",
                "lblDescription": "lblDescription"
            };
            var filterData = data.map(function(item) {
                return {
                    "flxSearchDropDown": "flxSearchDropDown",
                    "flxCheckBox": "flxCheckBox",
                    "lblDescription": item,
                    "imgCheckBox": {
                        "src": "checkbox.png"
                    }
                };
            });
            self.view.statusFilterMenu2.segStatusFilterDropdown.widgetDataMap = widgetMap;
            self.view.statusFilterMenu2.segStatusFilterDropdown.setData(filterData);
            var indices = [];
            for (var index = 0; index < filterData.length; index++) {
                indices.push(index);
            }
            self.view.statusFilterMenu2.segStatusFilterDropdown.selectedIndices = [
                [0, indices]
            ];
        },
        performStatusFilter: function() {
            var self = this;
            var selStatus = [];
            var selInd;
            var dataToShow = [];
            var indices = self.view.statusFilterMenu2.segStatusFilterDropdown.selectedIndices;
            if (indices !== null) {
                selInd = indices[0][1];
                for (var i = 0; i < selInd.length; i++) {
                    selStatus.push(self.view.statusFilterMenu2.segStatusFilterDropdown.data[selInd[i]].lblDescription);
                }
                SELECTED_FILTERS.status = selStatus;
                if (SELECTED_FILTERS.status.length === 2) self.inFilter = false;
                else self.inFilter = true;
                this.performStatusAppSearchFilter();
            } else {
                this.mapMFAScenarios([]);
                SELECTED_FILTERS.status = [];
                this.view.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmLogs.rtxNorecords");
            }
        },
        performStatusAppSearchFilter: function() {
            var self = this;
            var data = Array.from(this.mfaSegDataList);
            /*Applying App Filter*/
            if (SELECTED_FILTERS.appId != "ALL_APPLICATIONS") {
                data = this.mfaSegDataList.filter(function(rec) {
                    return rec.app.appId.toLowerCase() === SELECTED_FILTERS.appId.toLowerCase();
                });
                this.view.rtxNoResultsFound.text = kony.i18n.getLocalizedString("i18n.frmLogs.rtxNorecords");
            }
            /*Applying Status Filter*/
            var segStatusData = self.view.statusFilterMenu2.segStatusFilterDropdown.data;
            if (SELECTED_FILTERS.status.length === segStatusData.length) { //all are selected
            } else {
                data = data.filter(function(rec) {
                    if (SELECTED_FILTERS.status.indexOf(rec.mfaScenarioStatusId === STATUS_CONTANTS.active ? kony.i18n.getLocalizedString("i18n.secureimage.Active") : kony.i18n.getLocalizedString("i18n.secureimage.Inactive")) >= 0) {
                        return rec;
                    }
                });
            }
            this.mapMFAScenarios(data);
        },
        isValidPayload: function(data) {
            this.resetErrorState();
            var valid = true;
            if (data.appId === "SELECT") {
                this.view.lstBoxSelectApplication.skin = "redListBxSkin";
                this.view.flxErrorHeader.isVisible = true;
                valid = false;
            }
            if (data.serviceType != "SELECT") {
                if (data.serviceId === "SELECT") {
                    this.view.lbxActivityType.skin = "redListBxSkin";
                    this.view.lbxTransactionType.skin = "redListBxSkin";
                    this.view.flxErrorTransactionType.isVisible = true;
                    valid = false;
                }
            }
            if (data.serviceType === "SER_TYPE_TRNS") {
                if (data.frequencyTypeId === "SELECT") {
                    this.view.lbxFrequency.skin = "redListBxSkin";
                    this.view.flxErrorFrequency.isVisible = true;
                    valid = false;
                }
                if (data.frequencyTypeId === "VALUE_BASED") {
                    if (!data.frequencyValue) {
                        this.view.tbxEnterValue.skin = "skinredbg";
                        this.view.flxErrorFrequencyValue.isVisible = true;
                        valid = false;
                    }
                }
            }
            if (!data.mfaScenarioDescription) {
                this.view.tbxDescription.skin = "skinredbg";
                this.view.flxErrorDescription.isVisible = true;
                valid = false;
            }
            if (data.primaryMFATypeId === "SELECT") {
                this.view.lbxChallengePrimary.skin = "redListBxSkin";
                this.view.flxErrorPrimary.isVisible = true;
                valid = false;
            }
            if (data.secondaryMFATypeId === "SELECT") {
                this.view.lbxChallengeBackup.skin = "redListBxSkin";
                this.view.flxErrorBackup.isVisible = true;
                valid = false;
            }
            if (!data.smsText) {
                this.view.txtSMSContent.skin = "skinredbg";
                this.view.flxErrorSMSContent.isVisible = true;
                valid = false;
            }
            if (!data.emailSubject) {
                this.view.flxErrorEmailSubject.isVisible = true;
                valid = false;
            }
            if (!data.emailBody) {
                this.view.flxErrorEmailContent.isVisible = true;
                valid = false;
            }
            return valid;
        },
        resetErrorState: function() {
            this.view.lstBoxSelectApplication.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
            this.view.lbxActivityType.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
            this.view.lbxTransactionType.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
            this.view.lbxFrequency.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
            this.view.tbxEnterValue.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
            this.view.tbxDescription.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
            this.view.lbxChallengePrimary.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
            this.view.lbxChallengeBackup.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
            this.view.txtSMSContent.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
            this.view.flxErrorHeader.isVisible = false;
            this.view.flxErrorTransactionType.isVisible = false;
            this.view.flxErrorFrequency.isVisible = false;
            this.view.flxErrorFrequencyValue.isVisible = false;
            this.view.flxErrorDescription.isVisible = false;
            this.view.flxErrorEmailSubject.isVisible = false;
            this.view.flxErrorEmailContent.isVisible = false;
            this.view.flxErrorSMSContent.isVisible = false;
            this.view.flxErrorMessageTemplate.isVisible = false;
            this.view.flxErrorPrimary.isVisible = false;
            this.view.flxErrorBackup.isVisible = false;
        },
        backToCallerView: function() {
            var scopeObj = this;
            var payload = {
                "appId": CURRENT_SELECTION.appId,
                "actionId": CURRENT_SELECTION.actionId
            };
            this.presenter.fetchMFAScenariosById(payload);
        },
        refreshMfaView: function(viewData) {
            viewData = viewData[0];
            CURRENT_SELECTION.statusId = viewData.mfaScenarioStatusId;
            this.showMFAScenarioView({
                "appId": viewData.app.appId,
                "appName": viewData.app.appName,
                "serviceType": viewData.service.serviceType,
                "serviceId": viewData.service.serviceId,
                "serviceName": viewData.service.serviceName,
                "frequencyTypeId": viewData.frequencyType.frequencyTypeId,
                "frequencyTypeName": viewData.frequencyType.frequencyTypeName,
                "frequencyValue": viewData.frequencyType.frequencyValue,
                "mfaScenarioDescription": viewData.mfaScenarioDescription,
                "primaryMFATypeId": viewData.primaryMFATypeId,
                "secondaryMFATypeId": viewData.secondaryMFATypeId,
                "primaryMFAType": viewData.primaryMFATypeId == "SECURE_ACCESS_CODE" ? viewData.mfaTypes.SECURE_ACCESS_CODE.mfaTypeName : viewData.mfaTypes.SECURITY_QUESTIONS.mfaTypeName,
                "secondaryMFAType": viewData.secondaryMFATypeId == "SECURE_ACCESS_CODE" ? viewData.mfaTypes.SECURE_ACCESS_CODE.mfaTypeName : viewData.mfaTypes.SECURITY_QUESTIONS.mfaTypeName,
                "smsText": viewData.smsText,
                "emailSubject": viewData.emailSubject,
                "emailBody": viewData.emailBody,
                "mfaScenarioStatusId": viewData.mfaScenarioStatusId,
                "appActionId": viewData.appActionId,
                "appActionMFAId": viewData.appActionMFAId
            });
        }
    };
});
define("MFAModule/frmMFAScenariosControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmMFAScenarios **/
    AS_Form_he4dafed433a4a3f8c7a3a835ffa8873: function AS_Form_he4dafed433a4a3f8c7a3a835ffa8873(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmMFAScenarios **/
    AS_Form_f1e80b93c463429ab4135a1bd23f7a2b: function AS_Form_f1e80b93c463429ab4135a1bd23f7a2b(eventobject) {
        var self = this;
        this.preshow();
    }
});
define("MFAModule/frmMFAScenariosController", ["MFAModule/userfrmMFAScenariosController", "MFAModule/frmMFAScenariosControllerActions"], function() {
    var controller = require("MFAModule/userfrmMFAScenariosController");
    var controllerActions = ["MFAModule/frmMFAScenariosControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
