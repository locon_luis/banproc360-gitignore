define(['Promisify', 'ErrorInterceptor'], function(Promisify, ErrorInterceptor) {
    /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
    function PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }
    inheritsFrom(PresentationController, kony.mvc.Presentation.BasePresenter);
    /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
    PresentationController.prototype.initializePresentationController = function() {};
    PresentationController.prototype.showLoadingScreen = function() {
        var self = this;
        this.presentUserInterface(this.currentForm, {
            "loadingScreen": {
                "focus": true
            }
        });
    };
    PresentationController.prototype.hideLoadingScreen = function() {
        var self = this;
        this.presentUserInterface(this.currentForm, {
            "loadingScreen": {
                "focus": false
            }
        });
    };
    PresentationController.prototype.showToastMessage = function(status, message) {
        var self = this;
        this.presentUserInterface(this.currentForm, {
            "toastMessage": {
                "status": status,
                "message": message
            }
        });
    };
    PresentationController.prototype.showMFAConfigurations = function(context) {
        var self = this;
        context = {
            mfaConfigList: "mfaConfigList"
        };
        this.currentForm = "frmMFAConfigurations";
        this.presentUserInterface("frmMFAConfigurations", context);
    };
    PresentationController.prototype.showMFAScenarios = function(context) {
        var self = this;
        context = {
            mfaScenariosList: "mfaScenariosList"
        };
        this.currentForm = "frmMFAScenarios";
        this.presentUserInterface("frmMFAScenarios", context);
    };
    PresentationController.prototype.getAllMFAConfigurations = function(context) {
        var self = this;

        function completionGetAllConfigurationsCallback(response) {
            self.presentUserInterface(self.currentForm, {
                "getMFAConfigurations": response.mfaTypes
            });
            self.hideLoadingScreen();
        }

        function onError(error) {
            self.presentUserInterface(self.currentForm, {
                "getMFAConfigurations": []
            });
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(error));
            self.hideLoadingScreen();
        }
        this.showLoadingScreen();
        this.businessController.getAllMFAConfigurations(context, completionGetAllConfigurationsCallback, onError);
    };
    PresentationController.prototype.updateMFAConfiguration = function(context) {
        var self = this;

        function completionUpdateMFAConfigurationCallback(response) {
            self.presentUserInterface(self.currentForm, {
                "updateMFAConfiguration": response.status
            });
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"), kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.UpdatedSuccessfully"));
        }

        function onError(error) {
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(error));
            self.hideLoadingScreen();
        }
        this.showLoadingScreen();
        this.businessController.updateMFAConfiguration(context, completionUpdateMFAConfigurationCallback, onError);
    };
    PresentationController.prototype.fetchMFAScenarios = function(context) {
        var self = this;

        function completionFetchMFAScenariosCallback(response) {
            self.presentUserInterface(self.currentForm, {
                "fetchMFAScenarios": response.mfaScenarios
            });
            self.hideLoadingScreen();
        }

        function onError(error) {
            self.presentUserInterface(self.currentForm, {
                "fetchMFAScenarios": []
            });
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(error));
            self.hideLoadingScreen();
        }
        this.showLoadingScreen();
        this.businessController.fetchMFAScenarios(context, completionFetchMFAScenariosCallback, onError);
    };
    PresentationController.prototype.loadMasterData = function(callback) {
        var self = this;
        this.showLoadingScreen();
        self.masterData = {
            applications: [],
            actions: [],
            mfaTypes: [],
            mfaReferenceVariables: [],
            frequencyTypes: [],
            isLoaded: false
        };
        var promiseFetchApplicationsList = Promisify(this.businessController, 'fetchApplicationsList');
        var promiseFetchActionsList = Promisify(this.businessController, 'fetchActionsList');
        var promiseFetchMFATypes = Promisify(this.businessController, 'fetchMFATypes');
        var promiseFetchFrequencyTypes = Promisify(this.businessController, 'fetchFrequencyTypes');
        Promise.all([
            promiseFetchApplicationsList({}),
            promiseFetchActionsList({
                "serviceType": "SER_TYPE_TRNS"
            }),
            promiseFetchActionsList({
                "serviceType": "SER_TYPE_NONTRANS"
            }),
            promiseFetchMFATypes({}),
            promiseFetchFrequencyTypes({}),
        ]).then(function(responses) {
            self.hideLoadingScreen();
            self.masterData.applications = responses[0].apps;
            self.masterData.actionsTransactions = responses[1].services || [];
            self.masterData.actionsNonTransactions = responses[2].services || [];
            self.masterData.mfaTypes = responses[3].mfaTypes;
            self.masterData.frequencyTypes = responses[4].frequencyTypes;
            self.masterData.isLoaded = true;
            if (typeof callback === 'function') callback(self.masterData);
        }).catch(function(res) {
            self.hideLoadingScreen();
            callback("error");
            kony.print("unable to fetch preloaded data", res);
        });
    };
    PresentationController.prototype.createMFAScenario = function(context) {
        var self = this;

        function completionCreateMFAScenarioCallback(response) {
            self.presentUserInterface(self.currentForm, {
                "createMFAScenario": response.status
            });
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"), kony.i18n.getLocalizedString("i18n.frmMFAScenarios.UpdatedSuccessfully"));
        }

        function onError(error) {
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(error));
            self.hideLoadingScreen();
        }
        this.showLoadingScreen();
        this.businessController.createMFAScenario(context, completionCreateMFAScenarioCallback, onError);
    };
    PresentationController.prototype.updateMFAScenario = function(context) {
        var self = this;

        function completionUpdateMFAScenarioCallback(response) {
            self.presentUserInterface(self.currentForm, {
                "createMFAScenario": response.status
            });
            self.hideLoadingScreen();
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"), kony.i18n.getLocalizedString("i18n.frmMFAScenarios.UpdatedSuccessfully"));
        }

        function onError(error) {
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(error));
            self.hideLoadingScreen();
        }
        this.showLoadingScreen();
        this.businessController.updateMFAScenario(context, completionUpdateMFAScenarioCallback, onError);
    };
    PresentationController.prototype.deleteMFAScenario = function(context) {
        var self = this;

        function completionDeleteMFAScenarioCallback(response) {
            self.presentUserInterface(self.currentForm, {
                "createMFAScenario": response.status
            });
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"), kony.i18n.getLocalizedString("i18n.frmMFAScenarios.DeletedSuccessfully"));
        }

        function onError(error) {
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(error));
            self.hideLoadingScreen();
        }
        this.showLoadingScreen();
        this.businessController.deleteMFAScenario(context, completionDeleteMFAScenarioCallback, onError);
    };
    PresentationController.prototype.getMFAVariableReferences = function(callback) {
        var self = this;

        function completionGetMFAVariableReferencesCallback(response) {
            callback(response.mfavariablereference);
            self.hideLoadingScreen();
        }

        function onError(error) {
            callback("error");
            self.hideLoadingScreen();
        }
        this.showLoadingScreen();
        this.businessController.getMFAVariableReferences({}, completionGetMFAVariableReferencesCallback, onError);
    };
    PresentationController.prototype.fetchMFAScenariosById = function(context) {
        var self = this;

        function completionFetchMFAScenariosCallback(response) {
            self.presentUserInterface(self.currentForm, {
                "fetchMFAScenariosById": response.mfaScenarios
            });
            self.hideLoadingScreen();
        }

        function onError(error) {
            self.presentUserInterface(self.currentForm, {
                "fetchMFAScenariosById": []
            });
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmGroupsController.error"), ErrorInterceptor.errorMessage(error));
            self.hideLoadingScreen();
        }
        this.showLoadingScreen();
        this.businessController.fetchMFAScenarios(context, completionFetchMFAScenariosCallback, onError);
    };
    return PresentationController;
});