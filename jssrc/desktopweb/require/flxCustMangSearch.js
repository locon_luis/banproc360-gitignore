define("flxCustMangSearch", function() {
    return function(controller) {
        var flxCustMangSearch = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustMangSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "skin": "slFbox"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustMangSearch.setDefaultUnit(kony.flex.DP);
        var flxCustMangSearchWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustMangSearchWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "right": "35px",
            "skin": "sknCursor",
            "top": "1dp"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustMangSearchWrapper.setDefaultUnit(kony.flex.DP);
        var flxUserId = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxUserId",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "10%",
            "zIndex": 1
        }, {}, {});
        flxUserId.setDefaultUnit(kony.flex.DP);
        var lblUserId = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblUserId",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "123",
            "top": "20px",
            "width": "95%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUserId.add(lblUserId);
        var flxName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "13%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "16%",
            "zIndex": 1
        }, {}, {});
        flxName.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Mr. Bryan Victor Nash",
            "top": "20px",
            "width": "95%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxName.add(lblName);
        var flxUserName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxUserName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "10%",
            "zIndex": 1
        }, {}, {});
        flxUserName.setDefaultUnit(kony.flex.DP);
        var lblUserName = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblUserName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "TD12323",
            "top": "20px",
            "width": "95%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUserName.add(lblUserName);
        var flxEmalId = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmalId",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "41%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "18%",
            "zIndex": 1
        }, {}, {});
        flxEmalId.setDefaultUnit(kony.flex.DP);
        var lblEmailId = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblEmailId",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "johndoe@gmail.com",
            "top": "20px",
            "width": "95%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxEmalId.add(lblEmailId);
        var flxContactNo = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContactNo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "60%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "14%",
            "zIndex": 1
        }, {}, {});
        flxContactNo.setDefaultUnit(kony.flex.DP);
        var lblContactNo = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblContactNo",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "+919999999999",
            "top": "20px",
            "width": "95%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxContactNo.add(lblContactNo);
        var flxSSN = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSSN",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "75%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "12%",
            "zIndex": 1
        }, {}, {});
        flxSSN.setDefaultUnit(kony.flex.DP);
        var lblSSN = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblSSN",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "111-111-111",
            "top": "20px",
            "width": "95%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSSN.add(lblSSN);
        var flxDOB = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDOB",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "88%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "11%",
            "zIndex": 1
        }, {}, {});
        flxDOB.setDefaultUnit(kony.flex.DP);
        var lblDOB = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblDOB",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "MM/DD/YYYY",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDOB.add(lblDOB);
        var lblHiddenCustomerId = new kony.ui.Label({
            "id": "lblHiddenCustomerId",
            "isVisible": false,
            "left": "20dp",
            "skin": "slLabel",
            "text": "asd",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustMangSearchWrapper.add(flxUserId, flxName, flxUserName, flxEmalId, flxContactNo, flxSSN, flxDOB, lblHiddenCustomerId);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "35px",
            "right": "35px",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustMangSearch.add(flxCustMangSearchWrapper, lblSeperator);
        return flxCustMangSearch;
    }
})