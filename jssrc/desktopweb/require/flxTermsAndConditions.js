define("flxTermsAndConditions", function() {
    return function(controller) {
        var flxTermsAndConditions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxTermsAndConditions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxTermsAndConditions.setDefaultUnit(kony.flex.DP);
        var flxTermsAndConditionsWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "height": "50dp",
            "id": "flxTermsAndConditionsWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxTermsAndConditionsWrapper.setDefaultUnit(kony.flex.DP);
        var lblTitle = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTitle",
            "isVisible": true,
            "left": "70px",
            "skin": "sknlblLatoBold35475f14px",
            "text": " DBX footer terms & conditions",
            "width": "190px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCode = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCode",
            "isVisible": true,
            "left": "34%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Loans_Mortgage_ApplicantAgreement",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblApplicableApps = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblApplicableApps",
            "isVisible": true,
            "left": "65%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Retail Banking, Business Banking, Onboarding",
            "top": "10dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonActive = new kony.ui.Label({
            "centerY": "50%",
            "id": "fonticonActive",
            "isVisible": true,
            "right": "70dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "35dp",
            "right": "35dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTermsAndConditionsWrapper.add(lblTitle, lblCode, lblApplicableApps, fonticonActive, lblSeparator);
        flxTermsAndConditions.add(flxTermsAndConditionsWrapper);
        return flxTermsAndConditions;
    }
})