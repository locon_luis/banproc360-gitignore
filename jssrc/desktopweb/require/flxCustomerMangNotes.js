define("flxCustomerMangNotes", function() {
    return function(controller) {
        var flxCustomerMangNotes = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "id": "flxCustomerMangNotes",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknNormalDefault"
        }, {}, {
            "hoverSkin": "sknflxffffffop100"
        });
        flxCustomerMangNotes.setDefaultUnit(kony.flex.DP);
        var flxCustMangContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5px",
            "clipBounds": true,
            "id": "flxCustMangContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "right": "15px",
            "skin": "sknflxF5F6F80ab534450e73244Notes",
            "top": "5dp",
            "zIndex": 1
        }, {}, {});
        flxCustMangContent.setDefaultUnit(kony.flex.DP);
        var flxDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 20
        }, {}, {});
        flxDetails.setDefaultUnit(kony.flex.DP);
        var rtxNotesDescription = new kony.ui.RichText({
            "id": "rtxNotesDescription",
            "isVisible": true,
            "left": "15px",
            "right": "15px",
            "skin": "sknrtxLato485c7514px",
            "text": "\nLorem ipsum dolor sit amet, consectetur adipiscing elitswde. Vestibulum quis dignissim quam. Curabitur enim eros, aliquet sit amet maximus et, porttitor et ligula.",
            "top": "16px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDetails.add(rtxNotesDescription);
        var flxNotesUserProfile = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "7px",
            "clipBounds": true,
            "height": "30px",
            "id": "flxNotesUserProfile",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10px",
            "width": "100%",
            "zIndex": 2
        }, {}, {});
        flxNotesUserProfile.setDefaultUnit(kony.flex.DP);
        var flxUserPic = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "flxUserPic",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ib224ef181d44b028170bcbd8cb15cc5,
            "skin": "slFbox",
            "top": "0px",
            "width": "30px",
            "zIndex": 2
        }, {}, {});
        flxUserPic.setDefaultUnit(kony.flex.DP);
        var imgUser = new kony.ui.Image2({
            "height": "100%",
            "id": "imgUser",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "option3.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUserPic.add(imgUser);
        var lblUserName = new kony.ui.Label({
            "id": "lblUserName",
            "isVisible": true,
            "left": "55px",
            "skin": "sknlblLatoBold484b5213px",
            "text": "John Doe",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTime = new kony.ui.Label({
            "bottom": "0px",
            "id": "lblTime",
            "isVisible": true,
            "left": "55px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "6:46 am",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxNotesUserProfile.add(flxUserPic, lblUserName, lblTime);
        flxCustMangContent.add(flxDetails, flxNotesUserProfile);
        flxCustomerMangNotes.add(flxCustMangContent);
        return flxCustomerMangNotes;
    }
})