define("userflxViewEntitlementsController", {
    //Type your controller code here 
});
define("flxViewEntitlementsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxEntitlementsConfigure **/
    AS_FlexContainer_fb895631855f434eac40b66293684b9b: function AS_FlexContainer_fb895631855f434eac40b66293684b9b(eventobject, context) {
        var self = this;
        this.executeOnParent("showEntitlementDetails");
    }
});
define("flxViewEntitlementsController", ["userflxViewEntitlementsController", "flxViewEntitlementsControllerActions"], function() {
    var controller = require("userflxViewEntitlementsController");
    var controllerActions = ["flxViewEntitlementsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
