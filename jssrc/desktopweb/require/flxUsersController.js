define("userflxUsersController", {
    //Type your controller code here 
});
define("flxUsersControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxOptions **/
    AS_FlexContainer_ae6487600ac64deda71153ae8980e36f: function AS_FlexContainer_ae6487600ac64deda71153ae8980e36f(eventobject, context) {
        var self = this;
        this.executeOnParent("onClickOptions");
    }
});
define("flxUsersController", ["userflxUsersController", "flxUsersControllerActions"], function() {
    var controller = require("userflxUsersController");
    var controllerActions = ["flxUsersControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
