define("AlertsManagementModule/frmAlertsManagement", function() {
    return function(controller) {
        function addWidgetsfrmAlertsManagement() {
            this.setDefaultUnit(kony.flex.DP);
            var flxAlertsManagement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAlertsManagement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0.00%",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0.00%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertsManagement.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "konydbxlogobignverted.png"
                    },
                    "imgBottomLogo": {
                        "src": "konydbxinverted.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106px",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false,
                        "right": undefined
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "flxHeaderUserOptions": {
                        "isVisible": true
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagementController.Alerts\")"
                    },
                    "mainHeader": {
                        "left": undefined,
                        "top": undefined
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxReorderCategoriestBtn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "40px",
                "id": "flxReorderCategoriestBtn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknServiceDetailsContent",
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxReorderCategoriestBtn.setDefaultUnit(kony.flex.DP);
            var btnReorderCategories = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnReorderCategories",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Reorder\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReorderCategoriestBtn.add(btnReorderCategories);
            flxMainHeader.add(mainHeader, flxReorderCategoriestBtn);
            var flxAlertsBreadCrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertsBreadCrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertsBreadCrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0",
                "zIndex": 10,
                "overrides": {
                    "breadcrumbs1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "20px",
                        "left": "0px",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "35px",
                        "top": "0",
                        "width": "viz.val_cleared",
                        "zIndex": 10
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertsBreadCrumb.add(breadcrumbs);
            var flxMain = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "30px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "774px",
                "horizontalScrollIndicator": true,
                "id": "flxMain",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknAlertsMain",
                "top": "0px",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxAlertsMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertsMain",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "36px",
                "isModalContainer": false,
                "right": "34px",
                "skin": "CopyslFbox0id16272c19bf41",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAlertsMain.setDefaultUnit(kony.flex.DP);
            var flxAlertsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxAlertsTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertsTabs.setDefaultUnit(kony.flex.DP);
            var tabs = new com.adminConsole.customerMang.tabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "tabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "30px",
                "skin": "slFbox0hfd18814fd664dCM",
                "top": "0px",
                "overrides": {
                    "btnTabName5": {
                        "isVisible": false
                    },
                    "btnTabName6": {
                        "isVisible": false
                    },
                    "btnTabName7": {
                        "isVisible": false
                    },
                    "btnTabName8": {
                        "isVisible": false
                    },
                    "tabs": {
                        "height": "60px",
                        "isVisible": true,
                        "left": "30px",
                        "right": "30px",
                        "top": "0px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertsTabs.add(tabs);
            var flxAlertsSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAlertsSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "CopyslFbox0df892243e76c4d",
                "top": "0",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertsSeparator.setDefaultUnit(kony.flex.DP);
            flxAlertsSeparator.add();
            var flxAlertType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertType.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDetail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetail",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetail.setDefaultUnit(kony.flex.DP);
            var lblSubscriptionRequired = new kony.ui.Label({
                "height": "15px",
                "id": "lblSubscriptionRequired",
                "isVisible": true,
                "left": "30px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSubscriptionRequired\")",
                "top": "25px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSubscriptionStatus = new kony.ui.Label({
                "height": "15px",
                "id": "lblSubscriptionStatus",
                "isVisible": true,
                "left": "30px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSubscriptionStatus\")",
                "top": "50px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90px",
                "width": "110px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblDescription = new kony.ui.Label({
                "height": "15px",
                "id": "lblDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgDecription = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15px",
                "id": "imgDecription",
                "isVisible": false,
                "left": "5px",
                "skin": "slImage",
                "src": "img_down_arrow.png",
                "top": "0px",
                "width": "10px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconDescription",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon13pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "top": "7dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescription.add(lblDescription, imgDecription, lblIconDescription);
            var lblDescriptionText = new kony.ui.Label({
                "bottom": "25px",
                "id": "lblDescriptionText",
                "isVisible": true,
                "left": "30px",
                "right": "30px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblDescriptionText\")",
                "top": "115px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEditAlertType = new kony.ui.Button({
                "height": "20px",
                "id": "btnEditAlertType",
                "isVisible": true,
                "right": "30px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "25px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetail.add(lblSubscriptionRequired, lblSubscriptionStatus, flxDescription, lblDescriptionText, btnEditAlertType);
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var flxSearchInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchInner.setDefaultUnit(kony.flex.DP);
            var flxAlertsSeparator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxAlertsSeparator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknAlertsSeparator",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAlertsSeparator1.setDefaultUnit(kony.flex.DP);
            flxAlertsSeparator1.add();
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "58px",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "CopyslFbox2",
                "top": "1px",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false
                    },
                    "flxSearch": {
                        "right": undefined
                    },
                    "flxSubHeader": {
                        "width": "100%"
                    },
                    "subHeader": {
                        "height": "58px",
                        "left": "30px",
                        "right": "35px",
                        "top": "1px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAlertsSeparator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAlertsSeparator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknAlertsSeparator",
                "zIndex": 1
            }, {}, {});
            flxAlertsSeparator2.setDefaultUnit(kony.flex.DP);
            flxAlertsSeparator2.add();
            flxSearchInner.add(flxAlertsSeparator1, subHeader, flxAlertsSeparator2);
            flxSearch.add(flxSearchInner);
            var flxAlertSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "230dp",
                "id": "flxAlertSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertSegment.setDefaultUnit(kony.flex.DP);
            var flxSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxSegHeader.setDefaultUnit(kony.flex.DP);
            var flxNameContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxNameContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "60dp",
                "zIndex": 100
            }, {}, {});
            flxNameContainer.setDefaultUnit(kony.flex.DP);
            var lblHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var flxName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "30dp",
                "zIndex": 100
            }, {}, {});
            flxName.setDefaultUnit(kony.flex.DP);
            var imgImage = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "11px",
                "id": "imgImage",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "width": "11px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxName.add(imgImage, lblSortName);
            flxNameContainer.add(lblHeaderName, flxName);
            var lblHeaderDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderDescription",
                "isVisible": true,
                "left": "21%",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxChannelContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxChannelContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "61.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "90dp",
                "zIndex": 100
            }, {}, {});
            flxChannelContainer.setDefaultUnit(kony.flex.DP);
            var lblChannels = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblChannels",
                "isVisible": true,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblChannels\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var flxChannels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxChannels",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "30dp",
                "zIndex": 100
            }, {}, {});
            flxChannels.setDefaultUnit(kony.flex.DP);
            var imgChannels = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "11px",
                "id": "imgChannels",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "width": "11px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconChannels = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblIconChannels",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxChannels.add(imgChannels, lblIconChannels);
            flxChannelContainer.add(lblChannels, flxChannels);
            var flxStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "80.60%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "80dp",
                "zIndex": 100
            }, {}, {});
            flxStatusContainer.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "30dp",
                "zIndex": 100
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var imgStatus = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "11px",
                "id": "imgStatus",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "width": "11px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblIconStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStatus.add(imgStatus, lblIconStatus);
            flxStatusContainer.add(lblStatus, flxStatus);
            var lblSeperator = new kony.ui.Label({
                "bottom": "1dp",
                "height": "1px",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegHeader.add(flxNameContainer, lblHeaderDescription, flxChannelContainer, flxStatusContainer, lblSeperator);
            var flxSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "220dp",
                "id": "flxSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60px",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxSegment.setDefaultUnit(kony.flex.DP);
            var listingSegmentClient = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "listingSegmentClient",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "flxContextualMenu": {
                        "zIndex": 50
                    },
                    "flxListingSegmentWrapper": {
                        "height": "100%"
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0px",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0px",
                        "width": "100%"
                    },
                    "pagination": {
                        "isVisible": false
                    },
                    "pagination.flxPaginationWrapper": {
                        "isVisible": false
                    },
                    "rtxNoResultsFound": {
                        "centerY": "35%",
                        "isVisible": false
                    },
                    "segListing": {
                        "height": "93.50%",
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSegment.add(listingSegmentClient);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "statusFilterMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "56.60%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40px",
                "width": "130px",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "statusFilterMenu": {
                        "isVisible": false,
                        "left": "56.60%",
                        "top": "40px",
                        "width": "130px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertSegment.add(flxSegHeader, flxSegment, statusFilterMenu);
            flxAlertType.add(flxAlertTypeDetail, flxSearch, flxAlertSegment);
            flxAlertsMain.add(flxAlertsTabs, flxAlertsSeparator, flxAlertType);
            var flxViewAlertData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewAlertData",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffoptemplateop3px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxViewAlertData.setDefaultUnit(kony.flex.DP);
            var flxViewAlertDataHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxViewAlertDataHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertDataHeader.setDefaultUnit(kony.flex.DP);
            var flxViewAlertDataHeaderInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewAlertDataHeaderInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "20px",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertDataHeaderInner.setDefaultUnit(kony.flex.DP);
            var lblViewAlertName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewAlertName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknFlxViewServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertName\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertDataHeaderInner.add(lblViewAlertName);
            var btnViewAlertEdit = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnViewAlertEdit",
                "isVisible": true,
                "right": "112px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "20px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnViewAlertDelete = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnViewAlertDelete",
                "isVisible": true,
                "right": "34px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
                "top": "20px",
                "width": "68px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertDataHeader.add(flxViewAlertDataHeaderInner, btnViewAlertEdit, btnViewAlertDelete);
            var flxViewAlertDataContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewAlertDataContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertDataContent.setDefaultUnit(kony.flex.DP);
            var flxViewAlertDataContentInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewAlertDataContentInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxViewAlertDataContentInner.setDefaultUnit(kony.flex.DP);
            var flxViewAlertDataContent1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewAlertDataContent1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertDataContent1.setDefaultUnit(kony.flex.DP);
            var flxViewAlertTypeHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxViewAlertTypeHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertTypeHeader.setDefaultUnit(kony.flex.DP);
            var lblViewAlertTypeHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblViewAlertTypeHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TYPE\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertTypeHeader.add(lblViewAlertTypeHeader);
            var flxViewAlertTypeContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxViewAlertTypeContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertTypeContent.setDefaultUnit(kony.flex.DP);
            var lblViewAlertTypeContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblViewAlertTypeContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertTypeContent\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertTypeContent.add(lblViewAlertTypeContent);
            var flxViewAlertStatusHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxViewAlertStatusHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertStatusHeader.setDefaultUnit(kony.flex.DP);
            var lblViewAlertStatusHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblViewAlertStatusHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertStatusHeader.add(lblViewAlertStatusHeader);
            var flxViewAlertStatusContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxViewAlertStatusContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertStatusContent.setDefaultUnit(kony.flex.DP);
            var imgViewAlertStatusKey = new kony.ui.Image2({
                "centerY": "50%",
                "height": "10px",
                "id": "imgViewAlertStatusKey",
                "isVisible": false,
                "left": "0px",
                "skin": "slImage",
                "src": "active_circle2x.png",
                "width": "10px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewAlertStatusKey = new kony.ui.Label({
                "centerY": "54%",
                "height": "13dp",
                "id": "lblViewAlertStatusKey",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon13pxGreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "width": "13dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewAlertStatusValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewAlertStatusValue",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertStatusContent.add(imgViewAlertStatusKey, lblViewAlertStatusKey, lblViewAlertStatusValue);
            var flxViewAlertSubscriptionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxViewAlertSubscriptionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertSubscriptionHeader.setDefaultUnit(kony.flex.DP);
            var lblViewAlertSubscriptionHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblViewAlertSubscriptionHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSubscriptionRequired\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertSubscriptionHeader.add(lblViewAlertSubscriptionHeader);
            var flxViewAlertSubscriptionContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxViewAlertSubscriptionContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertSubscriptionContent.setDefaultUnit(kony.flex.DP);
            var lblViewAlertSubscriptionContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblViewAlertSubscriptionContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Yes\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertSubscriptionContent.add(lblViewAlertSubscriptionContent);
            var flxViewAlertChannelHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxViewAlertChannelHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "75%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertChannelHeader.setDefaultUnit(kony.flex.DP);
            var lblViewAlertChannelHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblViewAlertChannelHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewChannelHeader\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertChannelHeader.add(lblViewAlertChannelHeader);
            var flxViewAlertChannelContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxViewAlertChannelContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "75%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertChannelContent.setDefaultUnit(kony.flex.DP);
            var lblViewAlertChannelContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblViewAlertChannelContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewChannelContent\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertChannelContent.add(lblViewAlertChannelContent);
            flxViewAlertDataContent1.add(flxViewAlertTypeHeader, flxViewAlertTypeContent, flxViewAlertStatusHeader, flxViewAlertStatusContent, flxViewAlertSubscriptionHeader, flxViewAlertSubscriptionContent, flxViewAlertChannelHeader, flxViewAlertChannelContent);
            var flxViewAlertDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewAlertDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertDescription.setDefaultUnit(kony.flex.DP);
            var flxViewAlertDescriptionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxViewAlertDescriptionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertDescriptionHeader.setDefaultUnit(kony.flex.DP);
            var lblViewAlertDescriptionHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblViewAlertDescriptionHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertDescriptionHeader.add(lblViewAlertDescriptionHeader);
            var flxViewAlertDescriptionContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewAlertDescriptionContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertDescriptionContent.setDefaultUnit(kony.flex.DP);
            var lblViewAlertDescriptionContent = new kony.ui.Label({
                "id": "lblViewAlertDescriptionContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewDescriptionContent\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertDescriptionContent.add(lblViewAlertDescriptionContent);
            flxViewAlertDescription.add(flxViewAlertDescriptionHeader, flxViewAlertDescriptionContent);
            var flxDisplayDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDisplayDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDisplayDescription.setDefaultUnit(kony.flex.DP);
            var flxViewAlertContentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxViewAlertContentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertContentHeader.setDefaultUnit(kony.flex.DP);
            var lblViewAlertContentHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblViewAlertContentHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewAlertContentHeader\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertContentHeader.add(lblViewAlertContentHeader);
            var flxViewAlertContentContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "40px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxViewAlertContentContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "maxHeight": "540dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "20px",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewAlertContentContent.setDefaultUnit(kony.flex.DP);
            var lblViewAlertContentContent = new kony.ui.Label({
                "id": "lblViewAlertContentContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewAlertContentContent\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertContentContent.add(lblViewAlertContentContent);
            flxDisplayDescription.add(flxViewAlertContentHeader, flxViewAlertContentContent);
            flxViewAlertDataContentInner.add(flxViewAlertDataContent1, flxViewAlertDescription, flxDisplayDescription);
            flxViewAlertDataContent.add(flxViewAlertDataContentInner);
            flxViewAlertData.add(flxViewAlertDataHeader, flxViewAlertDataContent);
            var flxEditAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxEditAlert",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxEditAlert",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxEditAlert.setDefaultUnit(kony.flex.DP);
            var flxEditAlertContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "85%",
                "horizontalScrollIndicator": true,
                "id": "flxEditAlertContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertContent.setDefaultUnit(kony.flex.DP);
            var flxEditAlertMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertMain",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxEditAlertMain.setDefaultUnit(kony.flex.DP);
            var lblEditAlertMainName = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertMainName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertMainName\")",
                "top": "35px",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeNameCount = new kony.ui.Label({
                "height": "20px",
                "id": "lblAlertTypeNameCount",
                "isVisible": true,
                "left": "21%",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "-15px",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxEditAlertMainName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "tbxEditAlertMainName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 30,
                "placeholder": "Alert Name",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Default\")",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "15px",
                "width": "38%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoEditAlertMainNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoEditAlertMainNameError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxNoEditAlertMainNameError.setDefaultUnit(kony.flex.DP);
            var lblNoEditAlertMainNameErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditAlertMainNameErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEditAlertMainNameError = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditAlertMainNameError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEditAlertMainNameError.add(lblNoEditAlertMainNameErrorIcon, lblNoEditAlertMainNameError);
            var flxEditAlertTypeStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxEditAlertTypeStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "45%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-100dp",
                "width": "50%",
                "zIndex": 2
            }, {}, {});
            flxEditAlertTypeStatusContainer.setDefaultUnit(kony.flex.DP);
            var lblEditAlertMainSubscription = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertMainSubscription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertMainSubscription\")",
                "top": "0px",
                "width": "150px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEditAlertMainSubscriptionSwitch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEditAlertMainSubscriptionSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxEditAlertMainSubscriptionSwitch.setDefaultUnit(kony.flex.DP);
            var alertSubscriptionSwitch = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "alertSubscriptionSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "customSwitch": {
                        "height": "25dp",
                        "isVisible": true,
                        "left": "0px",
                        "top": "0px",
                        "width": "100%"
                    },
                    "switchToggle": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditAlertMainSubscriptionSwitch.add(alertSubscriptionSwitch);
            flxEditAlertTypeStatusContainer.add(lblEditAlertMainSubscription, flxEditAlertMainSubscriptionSwitch);
            var lblEditAlertMainDescription = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertMainDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertDescription\")",
                "top": "50px",
                "width": "150px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeDescriptionCount = new kony.ui.Label({
                "height": "20px",
                "id": "lblAlertTypeDescriptionCount",
                "isVisible": true,
                "right": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "-20px",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtareaEditAlertMainDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato35475f14Px",
                "height": "85px",
                "id": "txtareaEditAlertMainDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 250,
                "numberOfVisibleLines": 3,
                "placeholder": "Description",
                "skin": "skntxtAreaLato35475f14Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertsGenerated\")",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoEditAlertMainDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoEditAlertMainDescriptionError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxNoEditAlertMainDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoEditAlertMainDescriptionErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditAlertMainDescriptionErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEditAlertMainDescriptionError = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditAlertMainDescriptionError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertDescriptionError\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEditAlertMainDescriptionError.add(lblNoEditAlertMainDescriptionErrorIcon, lblNoEditAlertMainDescriptionError);
            var lblAssociatedAlerts = new kony.ui.Label({
                "height": "20px",
                "id": "lblAssociatedAlerts",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAssociatedAlerts\")",
                "top": "15px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAssociatedAlerts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxAssociatedAlerts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "CopyslFbox0d4620d53d9ea4b",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAssociatedAlerts.setDefaultUnit(kony.flex.DP);
            var flxAssociatedAlertsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "2px",
                "clipBounds": true,
                "id": "flxAssociatedAlertsInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknFlx5pxBorderffffff",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAssociatedAlertsInner.setDefaultUnit(kony.flex.DP);
            var segAssociatedAlerts = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAssociatedAlert": "Associated alert"
                }, {
                    "lblAssociatedAlert": "Associated alert"
                }, {
                    "lblAssociatedAlert": "Associated alert"
                }],
                "groupCells": false,
                "id": "segAssociatedAlerts",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "segAssociatedAlerts",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "lblAssociatedAlert": "lblAssociatedAlert",
                    "segAssociatedAlerts": "segAssociatedAlerts"
                },
                "width": "99.80%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAssociatedAlertsInner.add(segAssociatedAlerts);
            var flxNoAssociatedAlerts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5px",
                "clipBounds": true,
                "height": "100dp",
                "id": "flxNoAssociatedAlerts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5px",
                "isModalContainer": false,
                "right": "5px",
                "skin": "slFbox",
                "top": "5px",
                "zIndex": 1
            }, {}, {});
            flxNoAssociatedAlerts.setDefaultUnit(kony.flex.DP);
            var lblNoAssociatedAlerts = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblNoAssociatedAlerts",
                "isVisible": true,
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoAssociatedAlerts\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoAssociatedAlerts.add(lblNoAssociatedAlerts);
            flxAssociatedAlerts.add(flxAssociatedAlertsInner, flxNoAssociatedAlerts);
            flxEditAlertMain.add(lblEditAlertMainName, lblAlertTypeNameCount, tbxEditAlertMainName, flxNoEditAlertMainNameError, flxEditAlertTypeStatusContainer, lblEditAlertMainDescription, lblAlertTypeDescriptionCount, txtareaEditAlertMainDescription, flxNoEditAlertMainDescriptionError, lblAssociatedAlerts, flxAssociatedAlerts);
            var flxEditAlertInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertInner",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxEditAlertInner.setDefaultUnit(kony.flex.DP);
            var lblEditAlertName = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertMainName\")",
                "top": "35px",
                "width": "150px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEditAlertNameCount = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertNameCount",
                "isVisible": true,
                "left": "21%",
                "skin": "sknlblLatoBold485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "-15px",
                "width": "150dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxEditAlertName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "tbxEditAlertName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 50,
                "placeholder": "Alert Name",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PasswordReset\")",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "15px",
                "width": "38%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoEditAlertNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoEditAlertNameError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "200px",
                "zIndex": 2
            }, {}, {});
            flxNoEditAlertNameError.setDefaultUnit(kony.flex.DP);
            var lblNoEditAlertNameErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditAlertNameErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEditAlertNameError = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditAlertNameError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEditAlertNameError.add(lblNoEditAlertNameErrorIcon, lblNoEditAlertNameError);
            var flxEditAlertStatusChannelContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxEditAlertStatusChannelContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "45%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-120dp",
                "width": "45%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertStatusChannelContainer.setDefaultUnit(kony.flex.DP);
            var lblEditAlertStatus = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertStatus\")",
                "top": "20px",
                "width": "150px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEditAlertChannel = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertChannel",
                "isVisible": true,
                "left": "30%",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Channel\")",
                "top": "-20px",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEditAlertStatusSwitch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEditAlertStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxEditAlertStatusSwitch.setDefaultUnit(kony.flex.DP);
            var customServiceStatusSwitch = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "customServiceStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "customSwitch": {
                        "height": "25dp",
                        "isVisible": true,
                        "left": undefined,
                        "top": undefined
                    },
                    "switchToggle": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditAlertStatusSwitch.add(customServiceStatusSwitch);
            var flxEditAlertChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxEditAlertChannel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-30px",
                "width": "270px",
                "zIndex": 1
            }, {}, {});
            flxEditAlertChannel.setDefaultUnit(kony.flex.DP);
            var flxCheckBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCheckBox",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCheckBox.setDefaultUnit(kony.flex.DP);
            var flxSmsCheckBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxSmsCheckBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxSmsCheckBox.setDefaultUnit(kony.flex.DP);
            var imgSmsCheckBox = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "imgSmsCheckBox",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0px",
                "width": "98%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSmsCheckBox.add(imgSmsCheckBox);
            var lblSmsCheckBox = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSmsCheckBox",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSmsCheckBox\")",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEmailCheckBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxEmailCheckBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxEmailCheckBox.setDefaultUnit(kony.flex.DP);
            var imgEmailCheckBox = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "imgEmailCheckBox",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxselected.png",
                "top": "0px",
                "width": "98%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailCheckBox.add(imgEmailCheckBox);
            var lblEmailCheckBox = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailCheckBox",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEmailCheckBox\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPushCheckBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxPushCheckBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "26px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxPushCheckBox.setDefaultUnit(kony.flex.DP);
            var imgPushCheckBox = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "imgPushCheckBox",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0px",
                "width": "98%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPushCheckBox.add(imgPushCheckBox);
            var lblPushCheckBox = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPushCheckBox",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagementController.Push\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAllCheckBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxAllCheckBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxAllCheckBox.setDefaultUnit(kony.flex.DP);
            var imgAllCheckBox = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "imgAllCheckBox",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0px",
                "width": "98%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAllCheckBox.add(imgAllCheckBox);
            var lblAllCheckBox = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAllCheckBox",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAllCheckBox\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckBox.add(flxSmsCheckBox, lblSmsCheckBox, flxEmailCheckBox, lblEmailCheckBox, flxPushCheckBox, lblPushCheckBox, flxAllCheckBox, lblAllCheckBox);
            flxEditAlertChannel.add(flxCheckBox);
            var flxNoCheckBoxSelected = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoCheckBoxSelected",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCheckBoxSelected.setDefaultUnit(kony.flex.DP);
            var lblNoCheckBoxSelectedIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoCheckBoxSelectedIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCheckBoxSelected = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoCheckBoxSelected",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoCheckBoxSelected\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCheckBoxSelected.add(lblNoCheckBoxSelectedIcon, lblNoCheckBoxSelected);
            flxEditAlertStatusChannelContainer.add(lblEditAlertStatus, lblEditAlertChannel, flxEditAlertStatusSwitch, flxEditAlertChannel, flxNoCheckBoxSelected);
            var lblEditAlertDescription = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertDescription\")",
                "top": "25px",
                "width": "150px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEditAlertDescriptionCount = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertDescriptionCount",
                "isVisible": true,
                "right": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "-15px",
                "width": "150dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtareaEditAlertDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato35475f14Px",
                "height": "85px",
                "id": "txtareaEditAlertDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 250,
                "numberOfVisibleLines": 3,
                "placeholder": "Description",
                "skin": "skntxtAreaLato35475f14Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TriggeredWhen\")",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoEditAlertDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoEditAlertDescriptionError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxNoEditAlertDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoEditAlertDescriptionErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditAlertDescriptionErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEditAlertDescriptionError = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditAlertDescriptionError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertDescriptionError\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEditAlertDescriptionError.add(lblNoEditAlertDescriptionErrorIcon, lblNoEditAlertDescriptionError);
            var lblEditAlertAlertContent = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertAlertContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertAlertContent\")",
                "top": "20px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxVariableReferenceClick = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxVariableReferenceClick",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "100px",
                "isModalContainer": false,
                "skin": "sknFlxPointer",
                "top": "-20px",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxVariableReferenceClick.setDefaultUnit(kony.flex.DP);
            var lblEditAlertVariableReference = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertVariableReference",
                "isVisible": true,
                "left": "0px",
                "skin": "sknAlertBlue",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertVariableReference\")",
                "top": "0px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxVariableReferenceClick.add(lblEditAlertVariableReference);
            var lblEditAlertContentCount = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertContentCount",
                "isVisible": true,
                "right": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "-15px",
                "width": "150dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtareaEditAlertAlertContent = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato35475f14Px",
                "height": "85px",
                "id": "txtareaEditAlertAlertContent",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 250,
                "numberOfVisibleLines": 3,
                "placeholder": "Display Description",
                "skin": "skntxtAreaLato35475f14Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.YourOnlineBankingPassword\")",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoEditAlertAlertContentError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoEditAlertAlertContentError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxNoEditAlertAlertContentError.setDefaultUnit(kony.flex.DP);
            var lblNoEditAlertAlertContentErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditAlertAlertContentErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEditAlertAlertContentError = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditAlertAlertContentError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertAlertContentError\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEditAlertAlertContentError.add(lblNoEditAlertAlertContentErrorIcon, lblNoEditAlertAlertContentError);
            var flxAlertPreviewClick = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewClick",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxPointer",
                "top": "20px",
                "width": "50px",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewClick.setDefaultUnit(kony.flex.DP);
            var lblEditAlertPreview = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertPreview",
                "isVisible": true,
                "left": "0px",
                "skin": "sknAlertBlue",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertPreview\")",
                "top": "0px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewClick.add(lblEditAlertPreview);
            flxEditAlertInner.add(lblEditAlertName, lblEditAlertNameCount, tbxEditAlertName, flxNoEditAlertNameError, flxEditAlertStatusChannelContainer, lblEditAlertDescription, lblEditAlertDescriptionCount, txtareaEditAlertDescription, flxNoEditAlertDescriptionError, lblEditAlertAlertContent, flxVariableReferenceClick, lblEditAlertContentCount, txtareaEditAlertAlertContent, flxNoEditAlertAlertContentError, flxAlertPreviewClick);
            flxEditAlertContent.add(flxEditAlertMain, flxEditAlertInner);
            var flxEditAlertSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxEditAlertSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "CopyslFbox0i8b669b8d26e4c",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertSeparator.setDefaultUnit(kony.flex.DP);
            flxEditAlertSeparator.add();
            var flxCommonButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxCommonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCommonButtons.setDefaultUnit(kony.flex.DP);
            var flxCommonButtonsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCommonButtonsInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxCommonButtonsInner.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCommonButtonsInner.add(commonButtons);
            flxCommonButtons.add(flxCommonButtonsInner);
            flxEditAlert.add(flxEditAlertContent, flxEditAlertSeparator, flxCommonButtons);
            flxMain.add(flxAlertsMain, flxViewAlertData, flxEditAlert);
            var flxAlertConfiguration = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "774px",
                "id": "flxAlertConfiguration",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertConfiguration.setDefaultUnit(kony.flex.DP);
            var flxAlertBoxContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxAlertBoxContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": 35,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp"
            }, {}, {});
            flxAlertBoxContainer.setDefaultUnit(kony.flex.DP);
            var flxTabsAlerts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "61dp",
                "id": "flxTabsAlerts",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxTabsAlerts.setDefaultUnit(kony.flex.DP);
            var flxTabButtons = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "60px",
                "id": "flxTabButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "preShow": function(eventobject) {
                    controller.AS_FlexContainer_b1a3158c443a4c42acb8d6a705fbc11f(eventobject);
                },
                "skin": "slFbox0hfd18814fd664dCM",
                "top": "0px",
                "width": "95%"
            }, {}, {});
            flxTabButtons.setDefaultUnit(kony.flex.DP);
            var btnTabName1 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName1",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CONTACT\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName2 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName2",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PRODUCTS\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName3 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName3",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.HELP_CENTER\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName4 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName4",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Profile_tabs_CUSTOMER_Roles\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName5 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName5",
                "isVisible": false,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACTIVITY_HISTORY\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName6 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName6",
                "isVisible": false,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.AlertsManagement.ALERT\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName7 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName7",
                "isVisible": false,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DEVICE_INFO\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName8 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName8",
                "isVisible": false,
                "left": "0dp",
                "minWidth": "160px",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Profile_tabs_CUSTOMER_Permissions\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            flxTabButtons.add(btnTabName1, btnTabName2, btnTabName3, btnTabName4, btnTabName5, btnTabName6, btnTabName7, btnTabName8);
            var flxAlertsTabsSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAlertsTabsSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSeparatorCsr",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertsTabsSeprator.setDefaultUnit(kony.flex.DP);
            flxAlertsTabsSeprator.add();
            flxTabsAlerts.add(flxTabButtons, flxAlertsTabsSeprator);
            var flxBreadcrumbAlerts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadcrumbAlerts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadcrumbAlerts.setDefaultUnit(kony.flex.DP);
            var breadcrumbAlerts = new com.adminConsole.common.breadcrumbs1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbAlerts",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0",
                "zIndex": 10,
                "overrides": {
                    "breadcrumbs1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "20px",
                        "left": "0px",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "35px",
                        "top": "0",
                        "width": "viz.val_cleared",
                        "zIndex": 10
                    },
                    "btnBackToMain": {
                        "left": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumbAlerts.add(breadcrumbAlerts);
            var flxAlertCategories = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertCategories",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAlertCategories.setDefaultUnit(kony.flex.DP);
            var flxAlertCategoryDetailsScreen = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "660dp",
                "horizontalScrollIndicator": true,
                "id": "flxAlertCategoryDetailsScreen",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoryDetailsScreen.setDefaultUnit(kony.flex.DP);
            var flxAlertCategoryContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertCategoryContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxAlertCategoryContainer.setDefaultUnit(kony.flex.DP);
            var flxAlertCategoryDetailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxAlertCategoryDetailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoryDetailHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertCategoryHeaderContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxAlertCategoryHeaderContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoryHeaderContainer.setDefaultUnit(kony.flex.DP);
            var lblViewDetailsCategoryDisplayName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewDetailsCategoryDisplayName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular485C7518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SecurityAlert\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewDetailsCategoryStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewDetailsCategoryStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "70dp",
                "skin": "sknServiceDetailsContent",
                "top": "12dp",
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxViewDetailsCategoryStatusContainer.setDefaultUnit(kony.flex.DP);
            var lblIconViewDetailsCategoryStatus = new kony.ui.Label({
                "height": "13dp",
                "id": "lblIconViewDetailsCategoryStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon13pxGreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "2dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewDetailsCategoryStatus = new kony.ui.Label({
                "id": "lblViewDetailsCategoryStatus",
                "isVisible": true,
                "left": "23dp",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewDetailsCategoryStatusContainer.add(lblIconViewDetailsCategoryStatus, lblViewDetailsCategoryStatus);
            var flxCategoryOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCategoryOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "94.50%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_b2cebc80febc479da9a2cb6ecfce902e,
                "skin": "slFbox",
                "width": "30px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxCategoryOptions.setDefaultUnit(kony.flex.DP);
            var lblIconCategoryOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconCategoryOptions",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconImgOptions\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryOptions.add(lblIconCategoryOptions);
            flxAlertCategoryHeaderContainer.add(lblViewDetailsCategoryDisplayName, flxViewDetailsCategoryStatusContainer, flxCategoryOptions);
            var flxCategoryHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxCategoryHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxCategoryHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxCategoryHeaderSeperator.add();
            flxAlertCategoryDetailHeader.add(flxAlertCategoryHeaderContainer, flxCategoryHeaderSeperator);
            var flxAlertCategoryMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5px",
                "clipBounds": true,
                "height": "35px",
                "id": "flxAlertCategoryMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "95.50%",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoryMessage.setDefaultUnit(kony.flex.DP);
            var alertCategoryMessage = new com.adminConsole.alertMang.alertMessage({
                "clipBounds": true,
                "height": "35px",
                "id": "alertCategoryMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "alertMessage": {
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertCategoryMessage.add(alertCategoryMessage);
            var flxAlertCategoryDetailBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAlertCategoryDetailBody",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoryDetailBody.setDefaultUnit(kony.flex.DP);
            var flxCategoryBodyContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryBodyContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxCategoryBodyContainer.setDefaultUnit(kony.flex.DP);
            var flxCategoryDetailRow0 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCategoryDetailRow0",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDetailRow0.setDefaultUnit(kony.flex.DP);
            var flxCategoryDetailCol01 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryDetailCol01",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDetailCol01.setDefaultUnit(kony.flex.DP);
            var lblCategoryCode = new kony.ui.Label({
                "id": "lblCategoryCode",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CategoryCodeCaps\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCategoryCodeValue = new kony.ui.Label({
                "id": "lblCategoryCodeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CatSecurity\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryDetailCol01.add(lblCategoryCode, lblCategoryCodeValue);
            var flxCategoryDetailCol02 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryDetailCol02",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDetailCol02.setDefaultUnit(kony.flex.DP);
            var lblCategoryChannels = new kony.ui.Label({
                "id": "lblCategoryChannels",
                "isVisible": true,
                "left": "0dp",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Channels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCategoryChannelValue = new kony.ui.Label({
                "id": "lblCategoryChannelValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SMSEmail\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryDetailCol02.add(lblCategoryChannels, lblCategoryChannelValue);
            var flxCategoryDetailCol03 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryDetailCol03",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDetailCol03.setDefaultUnit(kony.flex.DP);
            var lblCategoryAccountLevel = new kony.ui.Label({
                "id": "lblCategoryAccountLevel",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ContainsAccountLevelAlerts\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCategoryAccountLevelValue = new kony.ui.Label({
                "id": "lblCategoryAccountLevelValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.YES\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryDetailCol03.add(lblCategoryAccountLevel, lblCategoryAccountLevelValue);
            flxCategoryDetailRow0.add(flxCategoryDetailCol01, flxCategoryDetailCol02, flxCategoryDetailCol03);
            var flxCategoryDetailRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryDetailRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDetailRow1.setDefaultUnit(kony.flex.DP);
            var flxCategoryDescHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxCategoryDescHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDescHeader.setDefaultUnit(kony.flex.DP);
            var lblCategoryDescHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblCategoryDescHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CategoryDescriptionCaps\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryDescHeader.add(lblCategoryDescHeader);
            var flxCategoryDescValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "60dp",
                "id": "flxCategoryDescValue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25px",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDescValue.setDefaultUnit(kony.flex.DP);
            var lblCategoryDescValue = new kony.ui.Label({
                "id": "lblCategoryDescValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SecurityAlertsDesc\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryDescValue.add(lblCategoryDescValue);
            flxCategoryDetailRow1.add(flxCategoryDescHeader, flxCategoryDescValue);
            flxCategoryBodyContainer.add(flxCategoryDetailRow0, flxCategoryDetailRow1);
            flxAlertCategoryDetailBody.add(flxCategoryBodyContainer);
            flxAlertCategoryContainer.add(flxAlertCategoryDetailHeader, flxAlertCategoryMessage, flxAlertCategoryDetailBody);
            var flxAlertsListing = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertsListing",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxAlertsListing.setDefaultUnit(kony.flex.DP);
            var flxAlertListingHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxAlertListingHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxAlertListingHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertListingHeaderValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertListingHeaderValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular485C7518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertGroup\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddAlertBtn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAlertBtn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "110dp",
                "skin": "sknServiceDetailsContent",
                "top": "0dp",
                "width": "115dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertBtn.setDefaultUnit(kony.flex.DP);
            var btnAddAlerts = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnAddAlerts",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddAlertGroup\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertBtn.add(btnAddAlerts);
            var flxReorderAlertBtn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxReorderAlertBtn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "5dp",
                "skin": "sknServiceDetailsContent",
                "top": "0dp",
                "width": "95dp",
                "zIndex": 1
            }, {}, {});
            flxReorderAlertBtn.setDefaultUnit(kony.flex.DP);
            var btnReorderAlerts = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnReorderAlerts",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Reorder\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReorderAlertBtn.add(btnReorderAlerts);
            var flxAlertsHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAlertsHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1dp",
                "isModalContainer": false,
                "right": "-1dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxAlertsHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxAlertsHeaderSeperator.add();
            flxAlertListingHeader.add(lblAlertListingHeaderValue, flxAddAlertBtn, flxReorderAlertBtn, flxAlertsHeaderSeperator);
            var flxAlertsSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxAlertsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "80px",
                "zIndex": 1
            }, {}, {});
            flxAlertsSegment.setDefaultUnit(kony.flex.DP);
            var flxAlertListSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxAlertListSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertListSegHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertNameContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxAlertNameContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxAlertNameContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertHeaderName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxAlertNameContainer.add(lblAlertHeaderName);
            var flxAlertCodeContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxAlertCodeContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "22%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxAlertCodeContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertHeaderCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertHeaderCode",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CodeCaps\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxAlertCodeContainer.add(lblAlertHeaderCode);
            var flxAlertTypeContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxAlertTypeContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "14%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertHeaderType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertHeaderType",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TypeCaps\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxAlertTypeContainer.add(lblAlertHeaderType);
            var flxAlertStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxAlertStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "71%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxAlertStatusContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertHeaderStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertHeaderStatus",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxAlertStatusContainer.add(lblAlertHeaderStatus);
            var lblAlertSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1px",
                "id": "lblAlertSeperator",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertListSegHeader.add(flxAlertNameContainer, flxAlertCodeContainer, flxAlertTypeContainer, flxAlertStatusContainer, lblAlertSeperator);
            var flxAlertListSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxAlertListSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertListSegment.setDefaultUnit(kony.flex.DP);
            var segListing = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "groupCells": false,
                "id": "segListing",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_d898d5eaf17548d398fc6a18e2acee55,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": 0,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertListSegment.add(segListing);
            var flxContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "25px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "105px",
                "width": "150px",
                "zIndex": 5
            }, {}, {});
            flxContextualMenu.setDefaultUnit(kony.flex.DP);
            var flxOption1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption1.setDefaultUnit(kony.flex.DP);
            var imgOption = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconOption = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconOption",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption1.add(imgOption, lblIconOption, lblOption);
            var flxOption2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption2.setDefaultUnit(kony.flex.DP);
            var imgOption2 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption2",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconOption2",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption2.add(imgOption2, lblIconOption2, lblOption2);
            var flxOption3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption3",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption3.setDefaultUnit(kony.flex.DP);
            var imgOption3 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption3",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconOption3",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption3",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption3.add(imgOption3, lblIconOption3, lblOption3);
            flxContextualMenu.add(flxOption1, flxOption2, flxOption3);
            var flxNoAlertGroups = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxNoAlertGroups",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxf5f6f8Op100BorderE1E5Ed",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxNoAlertGroups.setDefaultUnit(kony.flex.DP);
            var rtxNoAlertGroups = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "rtxNoAlertGroups",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.NoAssociatedAlertGroup\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoAlertGroups.add(rtxNoAlertGroups);
            var flxListingSegmentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxListingSegmentWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxListingSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var rtxNoResultFound = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "52%",
                "id": "rtxNoResultFound",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "90px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxListingSegmentWrapper.add(rtxNoResultFound);
            flxAlertsSegment.add(flxAlertListSegHeader, flxAlertListSegment, flxContextualMenu, flxNoAlertGroups, flxListingSegmentWrapper);
            flxAlertsListing.add(flxAlertListingHeader, flxAlertsSegment);
            flxAlertCategoryDetailsScreen.add(flxAlertCategoryContainer, flxAlertsListing);
            var flxEditAlertCategoryScreen = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "660dp",
                "horizontalScrollIndicator": true,
                "id": "flxEditAlertCategoryScreen",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryScreen.setDefaultUnit(kony.flex.DP);
            var flxEditAlertCategory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCategory",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategory.setDefaultUnit(kony.flex.DP);
            var flxEditAlertCategoryHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxEditAlertCategoryHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryHeading.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategoryName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEditAlertCategoryName",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoRegular485c7518Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.EditSecurityAlert\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEditCategoryHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditCategoryHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxEditCategoryHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxEditCategoryHeaderSeperator.add();
            flxEditAlertCategoryHeading.add(lblEditAlertCategoryName, flxEditCategoryHeaderSeperator);
            var flxEditAlertCaegoryRow0 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCaegoryRow0",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "80dp",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCaegoryRow0.setDefaultUnit(kony.flex.DP);
            var flxEditCategoryCol00 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxEditCategoryCol00",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxEditCategoryCol00.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategoryChannelHeader = new kony.ui.Label({
                "id": "lblEditAlertCategoryChannelHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ChannelMultiple\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSupportedChannelEntries = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSupportedChannelEntries",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSupportedChannelEntries.setDefaultUnit(kony.flex.DP);
            flxSupportedChannelEntries.add();
            var flxCategoryChannelError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryChannelError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCategoryChannelError.setDefaultUnit(kony.flex.DP);
            var lblIconErrorCategoryChannel = new kony.ui.Label({
                "id": "lblIconErrorCategoryChannel",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgCategoryChannel = new kony.ui.Label({
                "id": "lblErrMsgCategoryChannel",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.errCode.20703\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryChannelError.add(lblIconErrorCategoryChannel, lblErrMsgCategoryChannel);
            flxEditCategoryCol00.add(lblEditAlertCategoryChannelHeader, flxSupportedChannelEntries, flxCategoryChannelError);
            var flxEditCategoryCol11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxEditCategoryCol11",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxEditCategoryCol11.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategoryRadioHeader = new kony.ui.Label({
                "id": "lblEditAlertCategoryRadioHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Contains Account Level Alerts?",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customCategoryRadioButtonType = new com.adminConsole.common.customRadioButtonGroup({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "customCategoryRadioButtonType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "customRadioButtonGroup": {
                        "top": "30dp"
                    },
                    "flxRadioButton3": {
                        "isVisible": false
                    },
                    "imgRadioButton1": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton2": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton3": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton4": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton5": {
                        "src": "radionormal_1x.png"
                    },
                    "lblRadioButtonValue1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.YES\")"
                    },
                    "lblRadioButtonValue2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.No\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxCategoryRadioButtonError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryRadioButtonError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCategoryRadioButtonError.setDefaultUnit(kony.flex.DP);
            var lblIconErrorRadioCategory = new kony.ui.Label({
                "id": "lblIconErrorRadioCategory",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgRadioCategory = new kony.ui.Label({
                "id": "lblErrMsgRadioCategory",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_type\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryRadioButtonError.add(lblIconErrorRadioCategory, lblErrMsgRadioCategory);
            flxEditCategoryCol11.add(lblEditAlertCategoryRadioHeader, customCategoryRadioButtonType, flxCategoryRadioButtonError);
            var flxEditCategoryCol12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxEditCategoryCol12",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxEditCategoryCol12.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategoryStatusHeader = new kony.ui.Label({
                "id": "lblEditAlertCategoryStatusHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Status\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEditAlertCategoryStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCategoryStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxEditAlertCategoryStatus.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategoryActive = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditAlertCategoryActive",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Active\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEditAlertCategoryIconStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEditAlertCategoryIconStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryIconStatus.setDefaultUnit(kony.flex.DP);
            var editAlertCategoryStatusSwitch = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "editAlertCategoryStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "customSwitch": {
                        "height": "25dp",
                        "isVisible": true,
                        "left": "0px",
                        "top": "0px",
                        "width": "100%"
                    },
                    "switchToggle": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditAlertCategoryIconStatus.add(editAlertCategoryStatusSwitch);
            flxEditAlertCategoryStatus.add(lblEditAlertCategoryActive, flxEditAlertCategoryIconStatus);
            flxEditCategoryCol12.add(lblEditAlertCategoryStatusHeader, flxEditAlertCategoryStatus);
            flxEditAlertCaegoryRow0.add(flxEditCategoryCol00, flxEditCategoryCol11, flxEditCategoryCol12);
            var flxEditCategoryBottom = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditCategoryBottom",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "160dp",
                "width": "100%"
            }, {}, {});
            flxEditCategoryBottom.setDefaultUnit(kony.flex.DP);
            var flxEditAlertCategoryLanguageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxEditAlertCategoryLanguageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryLanguageHeader.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategoryLanguageValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEditAlertCategoryLanguageValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485c7513pxHover",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SupportedLanguages\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEditAlertCategoryLanguagesError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCategoryLanguagesError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "200dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryLanguagesError.setDefaultUnit(kony.flex.DP);
            var lblIconCategoryLanguagesError = new kony.ui.Label({
                "id": "lblIconCategoryLanguagesError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgCategoryLanguages = new kony.ui.Label({
                "id": "lblErrorMsgCategoryLanguages",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_fill_all_the_fields\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditAlertCategoryLanguagesError.add(lblIconCategoryLanguagesError, lblErrorMsgCategoryLanguages);
            flxEditAlertCategoryLanguageHeader.add(lblEditAlertCategoryLanguageValue, flxEditAlertCategoryLanguagesError);
            var flxEditAlertCategoryLanguageSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCategoryLanguageSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryLanguageSegment.setDefaultUnit(kony.flex.DP);
            var flxEditAlertCategoryLangSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxEditAlertCategoryLangSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryLangSegHeader.setDefaultUnit(kony.flex.DP);
            var flxEditAlertCategorySegLanguage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditAlertCategorySegLanguage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategorySegLanguage.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategorySegLanguage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEditAlertCategorySegLanguage",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485c7513pxHover",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Language\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditAlertCategorySegLanguage.add(lblEditAlertCategorySegLanguage);
            var flxEditAlertCategorySegDisplayName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditAlertCategorySegDisplayName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "21%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "23%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategorySegDisplayName.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategorySegDisplayName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEditAlertCategorySegDisplayName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485c7513pxHover",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DisplayName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditAlertCategorySegDisplayName.add(lblEditAlertCategorySegDisplayName);
            var flxEditAlertCategorySegDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditAlertCategorySegDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "44%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategorySegDescription.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategorySegDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEditAlertCategorySegDescription",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485c7513pxHover",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CategoryDescription\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditAlertCategorySegDescription.add(lblEditAlertCategorySegDescription);
            var flxEditAlertCategorySegLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditAlertCategorySegLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategorySegLine.setDefaultUnit(kony.flex.DP);
            flxEditAlertCategorySegLine.add();
            flxEditAlertCategoryLangSegHeader.add(flxEditAlertCategorySegLanguage, flxEditAlertCategorySegDisplayName, flxEditAlertCategorySegDescription, flxEditAlertCategorySegLine);
            var flxEditAlertCategoryLangSegList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCategoryLangSegList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryLangSegList.setDefaultUnit(kony.flex.DP);
            var segEditAlertCategoryLanguages = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblCategoryDescription": "Label",
                    "lblCategoryDisplayName": "Label",
                    "lblCategoryLanguage": "Label",
                    "lblDescCount": "0/500",
                    "lblIconDeleteLang": "",
                    "lblSeprator": "Label",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": ""
                }, {
                    "lblCategoryDescription": "Label",
                    "lblCategoryDisplayName": "Label",
                    "lblCategoryLanguage": "Label",
                    "lblDescCount": "0/500",
                    "lblIconDeleteLang": "",
                    "lblSeprator": "Label",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": ""
                }, {
                    "lblCategoryDescription": "Label",
                    "lblCategoryDisplayName": "Label",
                    "lblCategoryLanguage": "Label",
                    "lblDescCount": "0/500",
                    "lblIconDeleteLang": "",
                    "lblSeprator": "Label",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": ""
                }],
                "groupCells": false,
                "id": "segEditAlertCategoryLanguages",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAlertsCategoryLanguageData",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAlertsCategoryLanguageData": "flxAlertsCategoryLanguageData",
                    "flxCategoryDescription": "flxCategoryDescription",
                    "flxCategoryDisplayName": "flxCategoryDisplayName",
                    "flxCategoryLangDelete": "flxCategoryLangDelete",
                    "flxCategoryLanguage": "flxCategoryLanguage",
                    "flxDescription": "flxDescription",
                    "flxLanguageRowContainer": "flxLanguageRowContainer",
                    "lblCategoryDescription": "lblCategoryDescription",
                    "lblCategoryDisplayName": "lblCategoryDisplayName",
                    "lblCategoryLanguage": "lblCategoryLanguage",
                    "lblDescCount": "lblDescCount",
                    "lblIconDeleteLang": "lblIconDeleteLang",
                    "lblSeprator": "lblSeprator",
                    "lstBoxSelectLanguage": "lstBoxSelectLanguage",
                    "tbxDescription": "tbxDescription",
                    "tbxDisplayName": "tbxDisplayName"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditAlertCategoryLangSegList.add(segEditAlertCategoryLanguages);
            flxEditAlertCategoryLanguageSegment.add(flxEditAlertCategoryLangSegHeader, flxEditAlertCategoryLangSegList);
            var flxEditAlertCategoryButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxEditAlertCategoryButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryButtons.setDefaultUnit(kony.flex.DP);
            var EditAlertCategoryButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "EditAlertCategoryButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 3,
                "overrides": {
                    "btnCancel": {
                        "left": "viz.val_cleared",
                        "right": "120dp"
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "commonButtons": {
                        "left": "20dp",
                        "right": 20,
                        "width": "viz.val_cleared",
                        "zIndex": 3
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditAlertCategoryButtons.add(EditAlertCategoryButtons);
            flxEditCategoryBottom.add(flxEditAlertCategoryLanguageHeader, flxEditAlertCategoryLanguageSegment, flxEditAlertCategoryButtons);
            flxEditAlertCategory.add(flxEditAlertCategoryHeading, flxEditAlertCaegoryRow0, flxEditCategoryBottom);
            flxEditAlertCategoryScreen.add(flxEditAlertCategory);
            var flxCategoryOptionsMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryOptionsMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "50dp",
                "width": "150dp",
                "zIndex": 1
            }, {}, {});
            flxCategoryOptionsMenu.setDefaultUnit(kony.flex.DP);
            var contextualMenu1 = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "contextualMenu1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnLink1": {
                        "isVisible": false
                    },
                    "btnLink2": {
                        "isVisible": false
                    },
                    "flxOption1": {
                        "isVisible": false
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOptionsSeperator": {
                        "isVisible": false
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCategoryOptionsMenu.add(contextualMenu1);
            flxAlertCategories.add(flxAlertCategoryDetailsScreen, flxEditAlertCategoryScreen, flxCategoryOptionsMenu);
            var flxAlertTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypes",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAlertTypes.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDetailsScreen = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "660px",
                "horizontalScrollIndicator": true,
                "id": "flxAlertTypeDetailsScreen",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailsScreen.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDetailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxAlertTypeDetailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeHeaderContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxAlertTypeHeaderContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeHeaderContainer.setDefaultUnit(kony.flex.DP);
            var lblViewDetailsAlertDisplayName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewDetailsAlertDisplayName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular485C7518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SecurityAlert\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewDetailsAlertStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewDetailsAlertStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "70dp",
                "skin": "sknServiceDetailsContent",
                "top": "12dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxViewDetailsAlertStatusContainer.setDefaultUnit(kony.flex.DP);
            var lblIconViewDetailsStatus = new kony.ui.Label({
                "height": "13dp",
                "id": "lblIconViewDetailsStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon13pxGreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "2dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewDetailsAlertTypeStatus = new kony.ui.Label({
                "id": "lblViewDetailsAlertTypeStatus",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewDetailsAlertStatusContainer.add(lblIconViewDetailsStatus, lblViewDetailsAlertTypeStatus);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "94.50%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_b2cebc80febc479da9a2cb6ecfce902e,
                "skin": "sknFlxBgFFFFFFRounded",
                "width": "30px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxOptions.setDefaultUnit(kony.flex.DP);
            var lblIconAlertTypeOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconAlertTypeOptions",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconImgOptions\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptions.add(lblIconAlertTypeOptions);
            flxAlertTypeHeaderContainer.add(lblViewDetailsAlertDisplayName, flxViewDetailsAlertStatusContainer, flxOptions);
            var flxGroupHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxGroupHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxGroupHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxGroupHeaderSeperator.add();
            flxAlertTypeDetailHeader.add(flxAlertTypeHeaderContainer, flxGroupHeaderSeperator);
            var flxAlertTypeMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5px",
                "clipBounds": true,
                "height": "35px",
                "id": "flxAlertTypeMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeMessage.setDefaultUnit(kony.flex.DP);
            var alertTypeMessage = new com.adminConsole.alertMang.alertMessage({
                "clipBounds": true,
                "height": "35px",
                "id": "alertTypeMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "alertMessage": {
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertTypeMessage.add(alertTypeMessage);
            var flxAlertTypeDetailBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxAlertTypeDetailBody",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailBody.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeBodyContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeBodyContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeBodyContainer.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDetailRow01 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxAlertTypeDetailRow01",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailRow01.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDetailCol11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailCol11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailCol11.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeAlertName = new kony.ui.Label({
                "id": "lblAlertTypeAlertName",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ALERT_GROUP_NAME\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeAlertNameValue = new kony.ui.Label({
                "id": "lblAlertTypeAlertNameValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ProfileManagement\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailCol11.add(lblAlertTypeAlertName, lblAlertTypeAlertNameValue);
            var flxAlertTypeDetailCol12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailCol12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailCol12.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeCode = new kony.ui.Label({
                "id": "lblAlertTypeCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ALERT_GROUP_CODE\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeCodeValue = new kony.ui.Label({
                "id": "lblAlertTypeCodeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "AL001AL001AL001",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailCol12.add(lblAlertTypeCode, lblAlertTypeCodeValue);
            var flxAlertTypeDetailCol13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailCol13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailCol13.setDefaultUnit(kony.flex.DP);
            var lblAlertType = new kony.ui.Label({
                "id": "lblAlertType",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.TYPE\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeValue = new kony.ui.Label({
                "id": "lblAlertTypeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.UserAlert\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailCol13.add(lblAlertType, lblAlertTypeValue);
            flxAlertTypeDetailRow01.add(flxAlertTypeDetailCol11, flxAlertTypeDetailCol12, flxAlertTypeDetailCol13);
            var flxAlertTypeDetailRow02 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxAlertTypeDetailRow02",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailRow02.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDetailCol21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailCol21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailCol21.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeApps = new kony.ui.Label({
                "id": "lblAlertTypeApps",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.APPS\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeAppsValue = new kony.ui.Label({
                "id": "lblAlertTypeAppsValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.RetailBankingNative\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailCol21.add(lblAlertTypeApps, lblAlertTypeAppsValue);
            var flxAlertTypeDetailCol22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailCol22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailCol22.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeUserType = new kony.ui.Label({
                "id": "lblAlertTypeUserType",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.USER_TYPES\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeUserTypeValue = new kony.ui.Label({
                "id": "lblAlertTypeUserTypeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.RetailCustomer\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailCol22.add(lblAlertTypeUserType, lblAlertTypeUserTypeValue);
            var flxAlertTypeDetailCol23 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailCol23",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailCol23.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeReqAttribute = new kony.ui.Label({
                "id": "lblAlertTypeReqAttribute",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.REQUIRES_ATTRIBUTE\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeReqAttributeValue = new kony.ui.Label({
                "id": "lblAlertTypeReqAttributeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CurrentBalance\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailCol23.add(lblAlertTypeReqAttribute, lblAlertTypeReqAttributeValue);
            flxAlertTypeDetailRow02.add(flxAlertTypeDetailCol21, flxAlertTypeDetailCol22, flxAlertTypeDetailCol23);
            var flxAlertTypeDetailRow03 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailRow03",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailRow03.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDescHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertTypeDescHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDescHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeDescHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertTypeDescHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDescHeader.add(lblAlertTypeDescHeader);
            var flxAlertTypeDescValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxAlertTypeDescValue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25px",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDescValue.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeDescValue = new kony.ui.Label({
                "id": "lblAlertTypeDescValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.LoremIpsumA\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDescValue.add(lblAlertTypeDescValue);
            flxAlertTypeDetailRow03.add(flxAlertTypeDescHeader, flxAlertTypeDescValue);
            flxAlertTypeBodyContainer.add(flxAlertTypeDetailRow01, flxAlertTypeDetailRow02, flxAlertTypeDetailRow03);
            flxAlertTypeDetailBody.add(flxAlertTypeBodyContainer);
            var flxSubAlertsListing = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlertsListing",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlertsListing.setDefaultUnit(kony.flex.DP);
            var flxSubAlertListingHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxSubAlertListingHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxSubAlertListingHeader.setDefaultUnit(kony.flex.DP);
            var flxSubAlertListingHeaderValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "flxSubAlertListingHeaderValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular485C7518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagementController.Alerts\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddSubAlertBtn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddSubAlertBtn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknServiceDetailsContent",
                "top": "0dp",
                "width": "13%",
                "zIndex": 1
            }, {}, {});
            flxAddSubAlertBtn.setDefaultUnit(kony.flex.DP);
            var btnAddSubAlerts = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnAddSubAlerts",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddAlert\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddSubAlertBtn.add(btnAddSubAlerts);
            var flxSubAlertsHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSubAlertsHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxSubAlertsHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxSubAlertsHeaderSeperator.add();
            flxSubAlertListingHeader.add(flxSubAlertListingHeaderValue, flxAddSubAlertBtn, flxSubAlertsHeaderSeperator);
            var flxSegSubAlerts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": false,
                "id": "flxSegSubAlerts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "70px",
                "zIndex": 1
            }, {}, {});
            flxSegSubAlerts.setDefaultUnit(kony.flex.DP);
            var flxSubAlertsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxSubAlertsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0",
                "zIndex": 11
            }, {}, {});
            flxSubAlertsHeader.setDefaultUnit(kony.flex.DP);
            var flxSubAlertHeaderDisplayName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSubAlertHeaderDisplayName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_a43b80cc872f4831807a918bd48f0ac3,
                "skin": "slFbox",
                "top": "0dp",
                "width": "13.85%",
                "zIndex": 1
            }, {}, {});
            flxSubAlertHeaderDisplayName.setDefaultUnit(kony.flex.DP);
            var lblSubAlertHeaderDisplayName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubAlertHeaderDisplayName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DISPLAYNAME\")",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxSubAlertHeaderDisplayName.add(lblSubAlertHeaderDisplayName);
            var flxSubAlertHeaderCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSubAlertHeaderCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_if8688b9f1b14c0796ccfa73761ffbf7,
                "skin": "slFbox",
                "top": "0dp",
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxSubAlertHeaderCode.setDefaultUnit(kony.flex.DP);
            var lblSubAlertHeaderCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubAlertHeaderCode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CODE\")",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxSubAlertHeaderCode.add(lblSubAlertHeaderCode);
            var flxSubAlertHeaderDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSubAlertHeaderDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "42%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_f184888da9f042978e3b0332711e0319,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxSubAlertHeaderDescription.setDefaultUnit(kony.flex.DP);
            var lblSubAlertHeaderDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubAlertHeaderDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubAlertHeaderDescription.add(lblSubAlertHeaderDescription);
            var flxSubAlertHeaderStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSubAlertHeaderStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "82%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxSubAlertHeaderStatus.setDefaultUnit(kony.flex.DP);
            var lblSubAlertHeaderStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubAlertHeaderStatus",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxSubAlertHeaderStatus.add(lblSubAlertHeaderStatus);
            var lblSubAlertHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblSubAlertHeaderSeperator",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTableHeaderLine",
                "text": ".",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubAlertsHeader.add(flxSubAlertHeaderDisplayName, flxSubAlertHeaderCode, flxSubAlertHeaderDescription, flxSubAlertHeaderStatus, lblSubAlertHeaderSeperator);
            var flxSegRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSegRoles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxSegRoles.setDefaultUnit(kony.flex.DP);
            var segSubAlerts = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "fontIconStatusImg": "",
                    "lblAlertStatus": "Active",
                    "lblCode": "AL001AL001",
                    "lblDescription": "Alert when the customer primary address is changed/updated",
                    "lblDisplayName": "Admin Role",
                    "lblIconImgOptions": "",
                    "lblSeperator": ".-"
                }, {
                    "fontIconStatusImg": "",
                    "lblAlertStatus": "Active",
                    "lblCode": "AL001AL001",
                    "lblDescription": "Alert when the customer primary phone number is changed/updated",
                    "lblDisplayName": " Phone Number Change",
                    "lblIconImgOptions": "",
                    "lblSeperator": "-"
                }],
                "groupCells": false,
                "id": "segSubAlerts",
                "isVisible": true,
                "left": "1dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_d898d5eaf17548d398fc6a18e2acee55,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": 0,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxSubAlerts",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "2dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxOptions": "flxOptions",
                    "flxStatus": "flxStatus",
                    "flxSubAlerts": "flxSubAlerts",
                    "fontIconStatusImg": "fontIconStatusImg",
                    "lblAlertStatus": "lblAlertStatus",
                    "lblCode": "lblCode",
                    "lblDescription": "lblDescription",
                    "lblDisplayName": "lblDisplayName",
                    "lblIconImgOptions": "lblIconImgOptions",
                    "lblSeperator": "lblSeperator"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegRoles.add(segSubAlerts);
            var flxNoResultFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxNoResultFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxf5f6f8Op100BorderE1E5Ed",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxNoResultFound.setDefaultUnit(kony.flex.DP);
            var rtxSearchMesg = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "rtxSearchMesg",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.NoAssociatedSubAlerts\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultFound.add(rtxSearchMesg);
            flxSegSubAlerts.add(flxSubAlertsHeader, flxSegRoles, flxNoResultFound);
            flxSubAlertsListing.add(flxSubAlertListingHeader, flxSegSubAlerts);
            flxAlertTypeDetailsScreen.add(flxAlertTypeDetailHeader, flxAlertTypeMessage, flxAlertTypeDetailBody, flxSubAlertsListing);
            var flxAddAlertTypeScreen = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "660dp",
                "horizontalScrollIndicator": true,
                "id": "flxAddAlertTypeScreen",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeScreen.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeContainer.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxAddAlertTypeHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeHeading.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertTypeName",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoRegular485c7518Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertName\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEditGroupHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditGroupHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxEditGroupHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxEditGroupHeaderSeperator.add();
            flxAddAlertTypeHeading.add(lblAddAlertTypeName, flxEditGroupHeaderSeperator);
            var flxAddAlertTypeRow01 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxAddAlertTypeRow01",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeRow01.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeCol11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxAddAlertTypeCol11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeCol11.setDefaultUnit(kony.flex.DP);
            var dataEntryAlertName = new com.adminConsole.common.dataEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "dataEntryAlertName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblData": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_Group_Name\")"
                    },
                    "lblErrorMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_name_cannot_be_empty\")",
                        "right": "viz.val_cleared",
                        "width": "90%"
                    },
                    "lblTextCounter": {
                        "isVisible": false
                    },
                    "tbxData": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_Group_Name\")",
                        "maxTextLength": 50
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertTypeCol11.add(dataEntryAlertName);
            var flxAddAlertTypeCol12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxAddAlertTypeCol12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "340dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeCol12.setDefaultUnit(kony.flex.DP);
            var dataEntryCode = new com.adminConsole.common.dataEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "dataEntryCode",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "dataEntry": {
                        "isVisible": false
                    },
                    "flxError": {
                        "isVisible": false
                    },
                    "lblData": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_Group_Code\")"
                    },
                    "lblErrorMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_group_code_name_cannot_be_empty\")",
                        "width": "90%"
                    },
                    "lblTextCounter": {
                        "isVisible": false
                    },
                    "tbxData": {
                        "maxTextLength": 20,
                        "placeholder": "Alert Group Code"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblAddAlertTypeCodeHeading = new kony.ui.Label({
                "id": "lblAddAlertTypeCodeHeading",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_Group_Code\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxAddAlertTypeCode = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxAddAlertTypeCode",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxAddAlertTypeCodeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeCodeError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeCodeError.setDefaultUnit(kony.flex.DP);
            var lblIconErrAlertTypeCode = new kony.ui.Label({
                "id": "lblIconErrAlertTypeCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgAlertTypeCode = new kony.ui.Label({
                "id": "lblErrMsgAlertTypeCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectCode\")",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeCodeError.add(lblIconErrAlertTypeCode, lblErrMsgAlertTypeCode);
            flxAddAlertTypeCol12.add(dataEntryCode, lblAddAlertTypeCodeHeading, lstBoxAddAlertTypeCode, flxAddAlertTypeCodeError);
            flxAddAlertTypeRow01.add(flxAddAlertTypeCol11, flxAddAlertTypeCol12);
            var flxAddAlertTypeRow02 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeRow02",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "170dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeRow02.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeCol21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxAddAlertTypeCol21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeCol21.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeRadioHeader = new kony.ui.Label({
                "id": "lblAddAlertTypeRadioHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.type\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customRadioButtonType = new com.adminConsole.common.customRadioButtonGroup({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "customRadioButtonType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "customRadioButtonGroup": {
                        "top": "30dp"
                    },
                    "flxRadioButton3": {
                        "isVisible": false
                    },
                    "imgRadioButton1": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton2": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton3": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton4": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton5": {
                        "src": "radionormal_1x.png"
                    },
                    "lblRadioButtonValue1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.GlobalAlert\")"
                    },
                    "lblRadioButtonValue2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.UserAlert\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxRadioGroupError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRadioGroupError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRadioGroupError.setDefaultUnit(kony.flex.DP);
            var lblIconErrorRadioGroup = new kony.ui.Label({
                "id": "lblIconErrorRadioGroup",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgRadioGroup = new kony.ui.Label({
                "id": "lblErrMsgRadioGroup",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_type\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioGroupError.add(lblIconErrorRadioGroup, lblErrMsgRadioGroup);
            flxAddAlertTypeCol21.add(lblAddAlertTypeRadioHeader, customRadioButtonType, flxRadioGroupError);
            var flxAddAlertTypeCol22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxAddAlertTypeCol22",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "340dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeCol22.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeStatusHeader = new kony.ui.Label({
                "id": "lblAddAlertTypeStatusHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertStatus\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddAlertStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxAddAlertStatus.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeActive = new kony.ui.Label({
                "height": "20px",
                "id": "lblAddAlertTypeActive",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Active\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddAlertTypeIconStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAddAlertTypeIconStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeIconStatus.setDefaultUnit(kony.flex.DP);
            var addAlertTypeStatusSwitch = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "addAlertTypeStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "customSwitch": {
                        "height": "25dp",
                        "isVisible": true,
                        "left": "0px",
                        "top": "0px",
                        "width": "100%"
                    },
                    "switchToggle": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertTypeIconStatus.add(addAlertTypeStatusSwitch);
            flxAddAlertStatus.add(lblAddAlertTypeActive, flxAddAlertTypeIconStatus);
            flxAddAlertTypeCol22.add(lblAddAlertTypeStatusHeader, flxAddAlertStatus);
            flxAddAlertTypeRow02.add(flxAddAlertTypeCol21, flxAddAlertTypeCol22);
            var flxAddAlertTypeRow03 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeRow03",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxAddAlertTypeRow03.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeCol31 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeCol31",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeCol31.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeApps = new kony.ui.Label({
                "id": "lblAddAlertTypeApps",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Apps\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customListboxApps = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "customListboxApps",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "overrides": {
                    "customListbox": {
                        "top": "25dp"
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "lblErrorMsg": {
                        "width": "90%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAddAlertTypeErrorApps = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeErrorApps",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeErrorApps.setDefaultUnit(kony.flex.DP);
            var lblIconErrorApps = new kony.ui.Label({
                "id": "lblIconErrorApps",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgApps = new kony.ui.Label({
                "id": "lblErrorMsgApps",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_apps\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeErrorApps.add(lblIconErrorApps, lblErrorMsgApps);
            flxAddAlertTypeCol31.add(lblAddAlertTypeApps, customListboxApps, flxAddAlertTypeErrorApps);
            var flxAddAlertTypeCol32 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeCol32",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "340dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeCol32.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeUser = new kony.ui.Label({
                "id": "lblAddAlertTypeUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.User_Type\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customListboxUsertypes = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "customListboxUsertypes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "customListbox": {
                        "top": "25dp",
                        "zIndex": 1
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "lblErrorMsg": {
                        "width": "90%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAddAlertTypeErrorUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeErrorUsers",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeErrorUsers.setDefaultUnit(kony.flex.DP);
            var lblIconErrorUsers = new kony.ui.Label({
                "id": "lblIconErrorUsers",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgUsers = new kony.ui.Label({
                "id": "lblErrorMsgUsers",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_usertype\")",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeErrorUsers.add(lblIconErrorUsers, lblErrorMsgUsers);
            flxAddAlertTypeCol32.add(lblAddAlertTypeUser, customListboxUsertypes, flxAddAlertTypeErrorUsers);
            flxAddAlertTypeRow03.add(flxAddAlertTypeCol31, flxAddAlertTypeCol32);
            var flxAddAlertBottom = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertBottom",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "350dp",
                "width": "100%"
            }, {}, {});
            flxAddAlertBottom.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeSelectAttribute = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxAddAlertTypeSelectAttribute",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeSelectAttribute.setDefaultUnit(kony.flex.DP);
            var imgCheckboxAttribute = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgCheckboxAttribute",
                "isVisible": true,
                "left": "20dp",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "9dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddAdditionalAttributes = new kony.ui.Label({
                "id": "lblAddAdditionalAttributes",
                "isVisible": true,
                "left": "50dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Additional_Attributes\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeSelectAttribute.add(imgCheckboxAttribute, lblAddAdditionalAttributes);
            var flxAddAlertTypeAddAttribute = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxAddAlertTypeAddAttribute",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeAddAttribute.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeAttributeName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeAttributeName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeAttributeName.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeAttributeName = new kony.ui.Label({
                "id": "lblAddAlertTypeAttributeName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Attribute_Name\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxAddAlertTypeAttribute = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxAddAlertTypeAttribute",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxAttributesError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAttributesError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAttributesError.setDefaultUnit(kony.flex.DP);
            var lblIconErrorAttributes = new kony.ui.Label({
                "id": "lblIconErrorAttributes",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMessageAttributes = new kony.ui.Label({
                "id": "lblErrorMessageAttributes",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_attribute_name\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributesError.add(lblIconErrorAttributes, lblErrorMessageAttributes);
            flxAddAlertTypeAttributeName.add(lblAddAlertTypeAttributeName, lstBoxAddAlertTypeAttribute, flxAttributesError);
            var flxAddAlertTypeAttributeCriteria = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeAttributeCriteria",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeAttributeCriteria.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeCriteria = new kony.ui.Label({
                "id": "lblAddAlertTypeCriteria",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Criteria\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxAddAlertTypeCriteria = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxAddAlertTypeCriteria",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBorderc1c9ceRadius3px",
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxAddAlertTypeErrorCriteria = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeErrorCriteria",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeErrorCriteria.setDefaultUnit(kony.flex.DP);
            var lblIconErrorCriteria = new kony.ui.Label({
                "id": "lblIconErrorCriteria",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMessageCriteria = new kony.ui.Label({
                "id": "lblErrorMessageCriteria",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_criteria\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeErrorCriteria.add(lblIconErrorCriteria, lblErrorMessageCriteria);
            flxAddAlertTypeAttributeCriteria.add(lblAddAlertTypeCriteria, lstBoxAddAlertTypeCriteria, flxAddAlertTypeErrorCriteria);
            var flxAddAlertTypeFreqValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeFreqValue",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeFreqValue.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeFrequency = new kony.ui.Label({
                "id": "lblAddAlertTypeFrequency",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.value\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxAddAlertTypeFrequency = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxAddAlertTypeFrequency",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxAddAlertTypeErrorFrequency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeErrorFrequency",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeErrorFrequency.setDefaultUnit(kony.flex.DP);
            var lblIconAddAlertTypeErrorFrequency = new kony.ui.Label({
                "id": "lblIconAddAlertTypeErrorFrequency",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMessageFrequency = new kony.ui.Label({
                "id": "lblErrorMessageFrequency",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_value\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeErrorFrequency.add(lblIconAddAlertTypeErrorFrequency, lblErrorMessageFrequency);
            flxAddAlertTypeFreqValue.add(lblAddAlertTypeFrequency, lstBoxAddAlertTypeFrequency, flxAddAlertTypeErrorFrequency);
            var flxAddAlertTypeFromValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeFromValue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeFromValue.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeFromValue = new kony.ui.Label({
                "id": "lblAddAlertTypeFromValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Value_from\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxAddAlertTypeFromValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "tbxAddAlertTypeFromValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var flxErrorFromValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorFromValue",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorFromValue.setDefaultUnit(kony.flex.DP);
            var lblIconFromValue = new kony.ui.Label({
                "id": "lblIconFromValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMessageFromValue = new kony.ui.Label({
                "id": "lblErrorMessageFromValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Value_cannot_be_empty\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorFromValue.add(lblIconFromValue, lblErrorMessageFromValue);
            flxAddAlertTypeFromValue.add(lblAddAlertTypeFromValue, tbxAddAlertTypeFromValue, flxErrorFromValue);
            var flxAddAlertTypeToValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeToValue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeToValue.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeToValue = new kony.ui.Label({
                "id": "lblAddAlertTypeToValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Value_to\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxAddAlertTypeToValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "tbxAddAlertTypeToValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var flxAddAlertTypeErrorToValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeErrorToValue",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeErrorToValue.setDefaultUnit(kony.flex.DP);
            var lblIconErrorToValue = new kony.ui.Label({
                "id": "lblIconErrorToValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMessageToValue = new kony.ui.Label({
                "id": "lblErrorMessageToValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Value_cannot_be_empty\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeErrorToValue.add(lblIconErrorToValue, lblErrorMessageToValue);
            flxAddAlertTypeToValue.add(lblAddAlertTypeToValue, tbxAddAlertTypeToValue, flxAddAlertTypeErrorToValue);
            flxAddAlertTypeAddAttribute.add(flxAddAlertTypeAttributeName, flxAddAlertTypeAttributeCriteria, flxAddAlertTypeFreqValue, flxAddAlertTypeFromValue, flxAddAlertTypeToValue);
            var flxAddAlertTypeLanguageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAddAlertTypeLanguageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeLanguageHeader.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeLanguageValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertTypeLanguageValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45Lato15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Supported_Language\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddAlertTypeLanguagesError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeLanguagesError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "200dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeLanguagesError.setDefaultUnit(kony.flex.DP);
            var lblIconLanguagesError = new kony.ui.Label({
                "id": "lblIconLanguagesError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgLanguages = new kony.ui.Label({
                "id": "lblErrorMsgLanguages",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_fill_all_the_fields\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeLanguagesError.add(lblIconLanguagesError, lblErrorMsgLanguages);
            flxAddAlertTypeLanguageHeader.add(lblAddAlertTypeLanguageValue, flxAddAlertTypeLanguagesError);
            var flxAddAlertTypeLanguageSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeLanguageSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeLanguageSegment.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeLangSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAddAlertTypeLangSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeLangSegHeader.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeSegLanguage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAlertTypeSegLanguage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeSegLanguage.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeSegLanguage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertTypeSegLanguage",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.LANGUAGE\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeSegLanguage.add(lblAddAlertTypeSegLanguage);
            var flxAddAlertTypeSegDisplayName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAlertTypeSegDisplayName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "21%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "23%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeSegDisplayName.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeSegDisplayName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertTypeSegDisplayName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DISPLAYNAME\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeSegDisplayName.add(lblAddAlertTypeSegDisplayName);
            var flxAddAlertTypeSegDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAlertTypeSegDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "44%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeSegDescription.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeSegDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertTypeSegDescription",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.DISPLAY_DESCRIPTION\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeSegDescription.add(lblAddAlertTypeSegDescription);
            var flxAddAlertTypeSegLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAddAlertTypeSegLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeSegLine.setDefaultUnit(kony.flex.DP);
            flxAddAlertTypeSegLine.add();
            flxAddAlertTypeLangSegHeader.add(flxAddAlertTypeSegLanguage, flxAddAlertTypeSegDisplayName, flxAddAlertTypeSegDescription, flxAddAlertTypeSegLine);
            var flxAddAlertTypeLangSegList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeLangSegList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeLangSegList.setDefaultUnit(kony.flex.DP);
            var segaddalertTypeLanguages = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAddAlertTypeSegDescription": "Label",
                    "lblAddAlertTypeSegDisplayName": "Label",
                    "lblAddAlertTypeSegLanguage": "Label",
                    "lblDescCount": "Label",
                    "lblIconDeleteLang": "Label",
                    "lblSeprator": "Label",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": {
                        "placeholder": "",
                        "text": ""
                    }
                }, {
                    "lblAddAlertTypeSegDescription": "Label",
                    "lblAddAlertTypeSegDisplayName": "Label",
                    "lblAddAlertTypeSegLanguage": "Label",
                    "lblDescCount": "Label",
                    "lblIconDeleteLang": "Label",
                    "lblSeprator": "Label",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": {
                        "placeholder": "",
                        "text": ""
                    }
                }, {
                    "lblAddAlertTypeSegDescription": "Label",
                    "lblAddAlertTypeSegDisplayName": "Label",
                    "lblAddAlertTypeSegLanguage": "Label",
                    "lblDescCount": "Label",
                    "lblIconDeleteLang": "Label",
                    "lblSeprator": "Label",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": {
                        "placeholder": "",
                        "text": ""
                    }
                }],
                "groupCells": false,
                "id": "segaddalertTypeLanguages",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAlertsLanguageData",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAddDescription": "flxAddDescription",
                    "flxAddDisplayName": "flxAddDisplayName",
                    "flxAddLanguage": "flxAddLanguage",
                    "flxAlertsLanguageData": "flxAlertsLanguageData",
                    "flxDelete": "flxDelete",
                    "flxDescription": "flxDescription",
                    "flxLanguageRowContainer": "flxLanguageRowContainer",
                    "lblAddAlertTypeSegDescription": "lblAddAlertTypeSegDescription",
                    "lblAddAlertTypeSegDisplayName": "lblAddAlertTypeSegDisplayName",
                    "lblAddAlertTypeSegLanguage": "lblAddAlertTypeSegLanguage",
                    "lblDescCount": "lblDescCount",
                    "lblIconDeleteLang": "lblIconDeleteLang",
                    "lblSeprator": "lblSeprator",
                    "lstBoxSelectLanguage": "lstBoxSelectLanguage",
                    "tbxDescription": "tbxDescription",
                    "tbxDisplayName": "tbxDisplayName"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeLangSegList.add(segaddalertTypeLanguages);
            flxAddAlertTypeLanguageSegment.add(flxAddAlertTypeLangSegHeader, flxAddAlertTypeLangSegList);
            var flxAddAlertTypeButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxAddAlertTypeButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeButtons.setDefaultUnit(kony.flex.DP);
            var addAlertTypeButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "addAlertTypeButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnCancel": {
                        "left": "viz.val_cleared",
                        "right": "120dp",
                        "zIndex": 3
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "commonButtons": {
                        "left": "20dp",
                        "right": 20,
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertTypeButtons.add(addAlertTypeButtons);
            flxAddAlertBottom.add(flxAddAlertTypeSelectAttribute, flxAddAlertTypeAddAttribute, flxAddAlertTypeLanguageHeader, flxAddAlertTypeLanguageSegment, flxAddAlertTypeButtons);
            flxAddAlertTypeContainer.add(flxAddAlertTypeHeading, flxAddAlertTypeRow01, flxAddAlertTypeRow02, flxAddAlertTypeRow03, flxAddAlertBottom);
            flxAddAlertTypeScreen.add(flxAddAlertTypeContainer);
            var flxAlertTypesContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypesContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "50dp",
                "width": "150dp",
                "zIndex": 1
            }, {}, {});
            flxAlertTypesContextualMenu.setDefaultUnit(kony.flex.DP);
            var contextualMenuAlertTypeDetail = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "contextualMenuAlertTypeDetail",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnLink1": {
                        "isVisible": false
                    },
                    "btnLink2": {
                        "isVisible": false
                    },
                    "flxOption1": {
                        "isVisible": false
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOptionsSeperator": {
                        "isVisible": false
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertTypesContextualMenu.add(contextualMenuAlertTypeDetail);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100px",
                "width": "150px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var flxEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEdit.setDefaultUnit(kony.flex.DP);
            var fonticonEdit = new kony.ui.Label({
                "centerY": "50%",
                "id": "fonticonEdit",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgOption1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption1",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEdit.add(fonticonEdit, imgOption1, lblOption1);
            var flxDeactivate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDeactivate.setDefaultUnit(kony.flex.DP);
            var imgDeactivate = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgDeactivate",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeactivateIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeactivateIcon",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeactivate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeactivate",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeactivate.add(imgDeactivate, lblDeactivateIcon, lblDeactivate);
            flxSelectOptions.add(flxEdit, flxDeactivate);
            flxAlertTypes.add(flxAlertTypeDetailsScreen, flxAddAlertTypeScreen, flxAlertTypesContextualMenu, flxSelectOptions);
            var flxSubAlerts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxSubAlerts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlerts.setDefaultUnit(kony.flex.DP);
            var flxViewSubAlert = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "700dp",
                "horizontalScrollIndicator": true,
                "id": "flxViewSubAlert",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true
            }, {}, {});
            flxViewSubAlert.setDefaultUnit(kony.flex.DP);
            var flxSubAlertDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlertDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "97.70%",
                "zIndex": 1
            }, {}, {});
            flxSubAlertDetails.setDefaultUnit(kony.flex.DP);
            var lblSubAlertName = new kony.ui.Label({
                "height": "20px",
                "id": "lblSubAlertName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular485C7518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddressChange\")",
                "top": "15px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAlertHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAlertHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "top": "50px",
                "zIndex": 1
            }, {}, {});
            flxAlertHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxAlertHeaderSeperator.add();
            var flxAlertMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5px",
                "clipBounds": true,
                "height": "35px",
                "id": "flxAlertMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "60px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAlertMessage.setDefaultUnit(kony.flex.DP);
            var alertMessage = new com.adminConsole.alertMang.alertMessage({
                "clipBounds": true,
                "height": "35px",
                "id": "alertMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "alertMessage": {
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertMessage.add(alertMessage);
            var flxViewAlertCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewAlertCode",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "105dp",
                "width": "35%"
            }, {}, {});
            flxViewAlertCode.setDefaultUnit(kony.flex.DP);
            var lblSubAlertCode = new kony.ui.Label({
                "id": "lblSubAlertCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertCodeCaps\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSubAlertCodeValue = new kony.ui.Label({
                "id": "lblSubAlertCodeValue",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknllbl485c75Lato13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SampleAlertCode\")",
                "top": "25px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertCode.add(lblSubAlertCode, lblSubAlertCodeValue);
            var flxViewAlertChannels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewAlertChannels",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "37%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "105dp",
                "width": "30%"
            }, {}, {});
            flxViewAlertChannels.setDefaultUnit(kony.flex.DP);
            var lblAlertChannels = new kony.ui.Label({
                "id": "lblAlertChannels",
                "isVisible": true,
                "left": "0dp",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SupportedChannelsCaps\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertChannelsValue = new kony.ui.Label({
                "id": "lblAlertChannelsValue",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknllbl485c75Lato13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SampleAlertCode\")",
                "top": "25px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertChannels.add(lblAlertChannels, lblAlertChannelsValue);
            var flxViewAlertDesc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewAlertDesc",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "175dp"
            }, {}, {});
            flxViewAlertDesc.setDefaultUnit(kony.flex.DP);
            var lblSubAlertDesc = new kony.ui.Label({
                "id": "lblSubAlertDesc",
                "isVisible": true,
                "left": "0dp",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DescriptionCaps\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxAlertDescription = new kony.ui.RichText({
                "bottom": 35,
                "id": "rtxAlertDescription",
                "isVisible": true,
                "left": "0px",
                "right": "65px",
                "skin": "sknrtxLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.LoremIpsumB\")",
                "top": "25px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertDesc.add(lblSubAlertDesc, rtxAlertDescription);
            var flxOptionsSubAlerts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxOptionsSubAlerts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_b2cebc80febc479da9a2cb6ecfce902e,
                "right": "40px",
                "skin": "sknFlxBgFFFFFFRounded",
                "top": "10dp",
                "width": "30px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxOptionsSubAlerts.setDefaultUnit(kony.flex.DP);
            var lblIconSubAlertOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconSubAlertOptions",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconImgOptions\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptionsSubAlerts.add(lblIconSubAlertOptions);
            var flxViewDetailsAlertStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewDetailsAlertStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "90dp",
                "skin": "sknServiceDetailsContent",
                "top": "17dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxViewDetailsAlertStatus.setDefaultUnit(kony.flex.DP);
            var lblViewDetailsStatusIcon = new kony.ui.Label({
                "height": "13dp",
                "id": "lblViewDetailsStatusIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon13pxGreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "2dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewDetailsAlertStatus = new kony.ui.Label({
                "id": "lblViewDetailsAlertStatus",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewDetailsAlertStatus.add(lblViewDetailsStatusIcon, lblViewDetailsAlertStatus);
            flxSubAlertDetails.add(lblSubAlertName, flxAlertHeaderSeperator, flxAlertMessage, flxViewAlertCode, flxViewAlertChannels, flxViewAlertDesc, flxOptionsSubAlerts, flxViewDetailsAlertStatus);
            var flxAlertContentTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertContentTemplate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "97.50%"
            }, {}, {});
            flxAlertContentTemplate.setDefaultUnit(kony.flex.DP);
            var lblTemplateHeader = new kony.ui.Label({
                "id": "lblTemplateHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoreg485c7515px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertContentTemplate\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddTemplateButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxAddTemplateButton",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "200dp",
                "skin": "sknflxffffffop100Border424242Radius100px",
                "top": "5dp",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxAddTemplateButton.setDefaultUnit(kony.flex.DP);
            var lblAddTemplateButton = new kony.ui.Label({
                "centerX": "48%",
                "centerY": "47%",
                "id": "lblAddTemplateButton",
                "isVisible": true,
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddTemplateButton.add(lblAddTemplateButton);
            var flxOptionEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxOptionEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": 115,
                "skin": "sknflxffffffop100Border424242Radius100px",
                "top": "5dp",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxOptionEdit.setDefaultUnit(kony.flex.DP);
            var fontIconOptionEdit = new kony.ui.Label({
                "centerX": "48%",
                "centerY": "47%",
                "id": "fontIconOptionEdit",
                "isVisible": true,
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.edit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptionEdit.add(fontIconOptionEdit);
            var flxEyeicon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeicon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100Border424242Radius100px",
                "top": "5dp",
                "width": "105dp",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "hoverhandSkin"
            });
            flxEyeicon.setDefaultUnit(kony.flex.DP);
            var lblEyeicon = new kony.ui.Label({
                "centerX": "48%",
                "centerY": "47%",
                "height": 25,
                "id": "lblEyeicon",
                "isVisible": true,
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertPreview\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeicon.add(lblEyeicon);
            var flxViewTemplateListBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140dp",
                "id": "flxViewTemplateListBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxf5f6f8Op100BorderE1E5Ed",
                "top": "60dp"
            }, {}, {});
            flxViewTemplateListBox.setDefaultUnit(kony.flex.DP);
            var lblContentBy = new kony.ui.Label({
                "id": "lblContentBy",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoreg485c7515px",
                "text": "View Content By",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSubAlertResponseState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlertResponseState",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlertResponseState.setDefaultUnit(kony.flex.DP);
            var lblSubAlertResponseState = new kony.ui.Label({
                "id": "lblSubAlertResponseState",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ResponseState\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxSubAlertResponseState = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxSubAlertResponseState",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Success"],
                    ["lb2", "Failure"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxSubAlertResponseState.add(lblSubAlertResponseState, lstBoxSubAlertResponseState);
            var flxSubAlertLanguages = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlertLanguages",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "240dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlertLanguages.setDefaultUnit(kony.flex.DP);
            var lblSubAlertLanguages = new kony.ui.Label({
                "id": "lblSubAlertLanguages",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Languages\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxSubAlertLanguages = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxSubAlertLanguages",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "English-UK"],
                    ["lb2", "German"],
                    ["lb3", "Spanish"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxSubAlertLanguages.add(lblSubAlertLanguages, lstBoxSubAlertLanguages);
            flxViewTemplateListBox.add(lblContentBy, flxSubAlertResponseState, flxSubAlertLanguages);
            flxAlertContentTemplate.add(lblTemplateHeader, flxAddTemplateButton, flxOptionEdit, flxEyeicon, flxViewTemplateListBox);
            var flxViewTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "97.50%"
            }, {}, {});
            flxViewTemplate.setDefaultUnit(kony.flex.DP);
            var flxViewSMSTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewSMSTemplate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxViewSMSTemplate.setDefaultUnit(kony.flex.DP);
            var ViewTemplateSMS = new com.adminConsole.alerts.ViewTemplate({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "ViewTemplateSMS",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "overrides": {
                    "ViewTemplate": {
                        "bottom": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "flxTitle": {
                        "isVisible": false
                    },
                    "lblChannelMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.YouHaveSuccessfully\")"
                    },
                    "lblChannelName": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SMSText\")"
                    },
                    "lblTitle": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TitleColon\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewSMSTemplate.add(ViewTemplateSMS);
            var flxViewNotificationCenter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewNotificationCenter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxViewNotificationCenter.setDefaultUnit(kony.flex.DP);
            var ViewTemplateCenter = new com.adminConsole.alerts.ViewTemplate({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "ViewTemplateCenter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "overrides": {
                    "ViewTemplate": {
                        "bottom": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "flxTitle": {
                        "isVisible": true
                    },
                    "lblChannelMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.YouHaveSuccessfully\")"
                    },
                    "lblChannelName": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.NotificationCenter\")"
                    },
                    "lblTitle": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TitleColon\")"
                    },
                    "lblTitleValue": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PasswordChange\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewNotificationCenter.add(ViewTemplateCenter);
            var flxViewPushNotification = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewPushNotification",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxViewPushNotification.setDefaultUnit(kony.flex.DP);
            var ViewTemplatePush = new com.adminConsole.alerts.ViewTemplate({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "ViewTemplatePush",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "overrides": {
                    "ViewTemplate": {
                        "bottom": "viz.val_cleared"
                    },
                    "lblChannelMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.YouHaveSuccessfully\")"
                    },
                    "lblChannelName": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PushNotification\")"
                    },
                    "lblTitle": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TitleColon\")"
                    },
                    "lblTitleValue": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PasswordChange\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewPushNotification.add(ViewTemplatePush);
            var flxViewEmailTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewEmailTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxViewEmailTemplate.setDefaultUnit(kony.flex.DP);
            var lblChannelName = new kony.ui.Label({
                "id": "lblChannelName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192b45LatoBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagementController._Email\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEmailTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailTemplate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxEmailTemplate.setDefaultUnit(kony.flex.DP);
            var flxEmailTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailTitle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "10dp"
            }, {}, {});
            flxEmailTitle.setDefaultUnit(kony.flex.DP);
            var lblEmailTitle = new kony.ui.Label({
                "id": "lblEmailTitle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoReg485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SubjectColon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailTitleValue = new kony.ui.Label({
                "id": "lblEmailTitleValue",
                "isVisible": true,
                "left": "50dp",
                "right": "30dp",
                "skin": "sknlblLatoReg485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.UsernameChanged\")",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailTitle.add(lblEmailTitle, lblEmailTitleValue);
            var rtxEmailViewer = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableNativeCommunication": false,
                "enableZoom": false,
                "id": "rtxEmailViewer",
                "isVisible": true,
                "left": "-8dp",
                "requestURLConfig": {
                    "URL": "richtextViewer.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailTemplate.add(flxEmailTitle, rtxEmailViewer);
            flxViewEmailTemplate.add(lblChannelName, flxEmailTemplate);
            flxViewTemplate.add(flxViewSMSTemplate, flxViewNotificationCenter, flxViewPushNotification, flxViewEmailTemplate);
            var flxAddTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxAddTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "15dp",
                "width": "97.50%"
            }, {}, {});
            flxAddTemplate.setDefaultUnit(kony.flex.DP);
            var flxAddSMSTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddSMSTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAddSMSTemplate.setDefaultUnit(kony.flex.DP);
            var flxSMSHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxSMSHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxPointer",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxSMSHeader.setDefaultUnit(kony.flex.DP);
            var lblSMSHeader = new kony.ui.Label({
                "id": "lblSMSHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SMSText\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSMSVariableReference = new kony.ui.Label({
                "height": "20px",
                "id": "lblSMSVariableReference",
                "isVisible": true,
                "left": "30px",
                "skin": "sknAlertBlue",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertVariableReference\")",
                "top": "0px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSMSHeader.add(lblSMSHeader, lblSMSVariableReference);
            var txtSMSMsg = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "100dp",
                "id": "txtSMSMsg",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 300,
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.addContentHere\")",
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblSMSMsgSize = new kony.ui.Label({
                "id": "lblSMSMsgSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount2\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoSMSMsgError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoSMSMsgError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "130dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoSMSMsgError.setDefaultUnit(kony.flex.DP);
            var lblNoSMSMsgErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSMSMsgErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoSMSMsgError = new kony.ui.Label({
                "id": "lblNoSMSMsgError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SMSCannotBeEmpty\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoSMSMsgError.add(lblNoSMSMsgErrorIcon, lblNoSMSMsgError);
            flxAddSMSTemplate.add(flxSMSHeader, txtSMSMsg, lblSMSMsgSize, flxNoSMSMsgError);
            var flxAddPushNotification = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddPushNotification",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "15dp",
                "zIndex": 1
            }, {}, {});
            flxAddPushNotification.setDefaultUnit(kony.flex.DP);
            var flxPushNotificationHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxPushNotificationHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxPointer",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxPushNotificationHeader.setDefaultUnit(kony.flex.DP);
            var lblPushNotificationHeader = new kony.ui.Label({
                "id": "lblPushNotificationHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PushNotification\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPushNotiVariableReference = new kony.ui.Label({
                "height": "20px",
                "id": "lblPushNotiVariableReference",
                "isVisible": true,
                "left": "30px",
                "skin": "sknAlertBlue",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertVariableReference\")",
                "top": "0px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPushNotificationHeader.add(lblPushNotificationHeader, lblPushNotiVariableReference);
            var txtPushNotification = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "100dp",
                "id": "txtPushNotification",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 300,
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.addContentHere\")",
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "65dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblPushNotificationSize = new kony.ui.Label({
                "id": "lblPushNotificationSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount2\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoPushNotification = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoPushNotification",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "170dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoPushNotification.setDefaultUnit(kony.flex.DP);
            var lblNoPushNotificationErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoPushNotificationErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoPushNotificationError = new kony.ui.Label({
                "id": "lblNoPushNotificationError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PushCannotBeEmpty\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoPushNotification.add(lblNoPushNotificationErrorIcon, lblNoPushNotificationError);
            var flxPushNotificationTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxPushNotificationTitle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "30dp",
                "zIndex": 1
            }, {}, {});
            flxPushNotificationTitle.setDefaultUnit(kony.flex.DP);
            var lblPushNotificationTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPushNotificationTitle",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TitleColon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxPushNotificationTitle = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxPushNotificationTitle",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "40px",
                "maxTextLength": 100,
                "onTouchStart": controller.AS_TextField_ee75a43a99344e72b47654fe9d064f6e,
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxPushNotificationTitle.add(lblPushNotificationTitle, tbxPushNotificationTitle);
            flxAddPushNotification.add(flxPushNotificationHeader, txtPushNotification, lblPushNotificationSize, flxNoPushNotification, flxPushNotificationTitle);
            var flxAddNotificationCenter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddNotificationCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "15dp",
                "zIndex": 1
            }, {}, {});
            flxAddNotificationCenter.setDefaultUnit(kony.flex.DP);
            var flxNotiCenterHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxNotiCenterHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxPointer",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxNotiCenterHeader.setDefaultUnit(kony.flex.DP);
            var lblNotiCenterHeader = new kony.ui.Label({
                "id": "lblNotiCenterHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.NotificationCenter\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNotiCenterVariableReference = new kony.ui.Label({
                "height": "20px",
                "id": "lblNotiCenterVariableReference",
                "isVisible": true,
                "left": "30px",
                "skin": "sknAlertBlue",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertVariableReference\")",
                "top": "0px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNotiCenterHeader.add(lblNotiCenterHeader, lblNotiCenterVariableReference);
            var txtNotiCenterMsg = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "100dp",
                "id": "txtNotiCenterMsg",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 300,
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.addContentHere\")",
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "65dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblNotiCenterMsgSize = new kony.ui.Label({
                "id": "lblNotiCenterMsgSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount2\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoNotiCenterMsgError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoNotiCenterMsgError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "170dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoNotiCenterMsgError.setDefaultUnit(kony.flex.DP);
            var lblNoNotiCenterErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoNotiCenterErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoNotiCenterError = new kony.ui.Label({
                "id": "lblNoNotiCenterError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "text": "Title cannot be empty",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoNotiCenterMsgError.add(lblNoNotiCenterErrorIcon, lblNoNotiCenterError);
            var flxNotiCenterTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxNotiCenterTitle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "30dp",
                "zIndex": 1
            }, {}, {});
            flxNotiCenterTitle.setDefaultUnit(kony.flex.DP);
            var lblNotiCenterTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNotiCenterTitle",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TitleColon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxNotiCenterTitle = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxNotiCenterTitle",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "40px",
                "maxTextLength": 100,
                "onTouchStart": controller.AS_TextField_ee75a43a99344e72b47654fe9d064f6e,
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxNotiCenterTitle.add(lblNotiCenterTitle, tbxNotiCenterTitle);
            flxAddNotificationCenter.add(flxNotiCenterHeader, txtNotiCenterMsg, lblNotiCenterMsgSize, flxNoNotiCenterMsgError, flxNotiCenterTitle);
            var flxAddEmailTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxAddEmailTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "15dp",
                "zIndex": 1
            }, {}, {});
            flxAddEmailTemplate.setDefaultUnit(kony.flex.DP);
            var flxEmailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxEmailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxPointer",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxEmailHeader.setDefaultUnit(kony.flex.DP);
            var lblEmailHeader = new kony.ui.Label({
                "id": "lblEmailHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagementController._Email\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailVariableReference = new kony.ui.Label({
                "height": "20px",
                "id": "lblEmailVariableReference",
                "isVisible": true,
                "left": "20px",
                "skin": "sknAlertBlue",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertVariableReference\")",
                "top": "0px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailHeader.add(lblEmailHeader, lblEmailVariableReference);
            var flxEmailSubject = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEmailSubject",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "30dp",
                "zIndex": 1
            }, {}, {});
            flxEmailSubject.setDefaultUnit(kony.flex.DP);
            var lblEmailSubject = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailSubject",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SubjectColon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxEmailSubject = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxEmailSubject",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "55px",
                "maxTextLength": 100,
                "onTouchStart": controller.AS_TextField_ee75a43a99344e72b47654fe9d064f6e,
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxEmailSubject.add(lblEmailSubject, tbxEmailSubject);
            var flxEmailRtxEditor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxEmailRtxEditor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxFFFFFFBr1pxe1e5edR3px3sided",
                "top": "65dp",
                "zIndex": 1
            }, {}, {});
            flxEmailRtxEditor.setDefaultUnit(kony.flex.DP);
            var rtxEmailTemplate = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "id": "rtxEmailTemplate",
                "isVisible": true,
                "left": "0px",
                "requestURLConfig": {
                    "URL": "richtext.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "right": "0px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxEmailRtxEditor.add(rtxEmailTemplate);
            var lblSubjectSize = new kony.ui.Label({
                "id": "lblSubjectSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount2\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoEmailSubject = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoEmailSubject",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "310dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoEmailSubject.setDefaultUnit(kony.flex.DP);
            var lblNoSubjectErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSubjectErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoSubjectError = new kony.ui.Label({
                "id": "lblNoSubjectError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "text": "Email subject cannot be empty",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEmailSubject.add(lblNoSubjectErrorIcon, lblNoSubjectError);
            flxAddEmailTemplate.add(flxEmailHeader, flxEmailSubject, flxEmailRtxEditor, lblSubjectSize, flxNoEmailSubject);
            flxAddTemplate.add(flxAddSMSTemplate, flxAddPushNotification, flxAddNotificationCenter, flxAddEmailTemplate);
            var flxSaveTemplateButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxSaveTemplateButtons",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxBorE1E5ED1pxKA",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSaveTemplateButtons.setDefaultUnit(kony.flex.DP);
            var btnTemplateCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnTemplateCancel",
                "isVisible": true,
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "right": "155px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnTemplateSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40dp",
                "id": "btnTemplateSave",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "top": "0%",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxSaveTemplateButtons.add(btnTemplateCancel, btnTemplateSave);
            var flxNoContentTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "160dp",
                "id": "flxNoContentTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100BorderE1E5Ed",
                "top": "10dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxNoContentTemplate.setDefaultUnit(kony.flex.DP);
            var rtxNoTemplate = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "rtxNoTemplate",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.NoAssociatedSubAlerts\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoContentTemplate.add(rtxNoTemplate);
            flxViewSubAlert.add(flxSubAlertDetails, flxAlertContentTemplate, flxViewTemplate, flxAddTemplate, flxSaveTemplateButtons, flxNoContentTemplate);
            var flxSubAlertContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlertContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "6%",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100dp",
                "width": "150dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlertContextualMenu.setDefaultUnit(kony.flex.DP);
            var flxEditSubAlertView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEditSubAlertView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditSubAlertView.setDefaultUnit(kony.flex.DP);
            var fonticonEditView = new kony.ui.Label({
                "centerY": "50%",
                "id": "fonticonEditView",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgOptionView = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOptionView",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOptionView = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOptionView",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditSubAlertView.add(fonticonEditView, imgOptionView, lblOptionView);
            var flxDeactivateAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDeactivateAlert",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDeactivateAlert.setDefaultUnit(kony.flex.DP);
            var imgDeactivateAlert = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgDeactivateAlert",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeactivateAlertIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeactivateAlertIcon",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeactivateAlert = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeactivateAlert",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeactivateAlert.add(imgDeactivateAlert, lblDeactivateAlertIcon, lblDeactivateAlert);
            flxSubAlertContextualMenu.add(flxEditSubAlertView, flxDeactivateAlert);
            var flxVariableReferenceContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "180px",
                "id": "flxVariableReferenceContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "233dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "316dp",
                "width": "280px",
                "zIndex": 2
            }, {}, {});
            flxVariableReferenceContainer.setDefaultUnit(kony.flex.DP);
            var imgVariableReferencePionter = new kony.ui.Image2({
                "centerY": "50%",
                "height": "20px",
                "id": "imgVariableReferencePionter",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "arrowvariableref2x.png",
                "width": "20px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxVariableReference = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "180px",
                "id": "flxVariableReference",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "-5px",
                "isModalContainer": false,
                "skin": "CopyslFbox0fd3f91d547a249",
                "top": "0px",
                "width": "240px",
                "zIndex": 1
            }, {}, {});
            flxVariableReference.setDefaultUnit(kony.flex.DP);
            var flxVariableReferenceHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxVariableReferenceHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxVariableReferenceHeader.setDefaultUnit(kony.flex.DP);
            var lblVariableReferenceHeader = new kony.ui.Label({
                "centerY": "50.00%",
                "id": "lblVariableReferenceHeader",
                "isVisible": true,
                "left": "15px",
                "right": "10px",
                "skin": "sknlblLatoBold12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblVariableReferenceHeader\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxVariableReferenceHeader.add(lblVariableReferenceHeader);
            var flxVariableReferenceSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "49%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxVariableReferenceSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "CopyslFbox0a4e08ac4307f4a",
                "width": "85%",
                "zIndex": 1
            }, {}, {});
            flxVariableReferenceSeparator.setDefaultUnit(kony.flex.DP);
            flxVariableReferenceSeparator.add();
            var flxVariableReferenceContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "125px",
                "id": "flxVariableReferenceContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxVariableReferenceContent.setDefaultUnit(kony.flex.DP);
            var flxVariableReferenceInner = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxVariableReferenceInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "pagingEnabled": false,
                "right": "10px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxVariableReferenceInner.setDefaultUnit(kony.flex.DP);
            var segVariableReference = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lbVariableReference": "Variable reference"
                }, {
                    "lbVariableReference": "Variable reference"
                }, {
                    "lbVariableReference": "Variable reference"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segVariableReference",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "Copyseg0f43ae421b7954e",
                "rowTemplate": "segVariableReference",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "lbVariableReference": "lbVariableReference",
                    "segVariableReference": "segVariableReference"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxVariableReferenceInner.add(segVariableReference);
            flxVariableReferenceContent.add(flxVariableReferenceInner);
            flxVariableReference.add(flxVariableReferenceHeader, flxVariableReferenceSeparator, flxVariableReferenceContent);
            flxVariableReferenceContainer.add(imgVariableReferencePionter, flxVariableReference);
            flxSubAlerts.add(flxViewSubAlert, flxSubAlertContextualMenu, flxVariableReferenceContainer);
            flxAlertBoxContainer.add(flxTabsAlerts, flxBreadcrumbAlerts, flxAlertCategories, flxAlertTypes, flxSubAlerts);
            flxAlertConfiguration.add(flxAlertBoxContainer);
            flxRightPanel.add(flxMainHeader, flxAlertsBreadCrumb, flxMain, flxAlertConfiguration);
            var flxAlertPreview = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAlertPreview",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "CopyslFbox0ie07b6077b734c",
                "top": 0,
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxAlertPreview.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "400px",
                "id": "flxAlertPreviewMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "CopyslFbox0a55815af731d49",
                "top": 0,
                "width": "700px",
                "zIndex": 10
            }, {}, {});
            flxAlertPreviewMain.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10dp",
                "id": "flxAlertPreviewTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f849449508464e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewTopColor.setDefaultUnit(kony.flex.DP);
            flxAlertPreviewTopColor.add();
            var lblAlertPreviewHeader = new kony.ui.Label({
                "id": "lblAlertPreviewHeader",
                "isVisible": true,
                "left": "30px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewHeader\")",
                "top": "35dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAlertPreviewClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAlertPreviewClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "sknCursor",
                "top": "35dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxAlertPreviewClose.setDefaultUnit(kony.flex.DP);
            var imgAlertPreviewClose = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgAlertPreviewClose",
                "isVisible": false,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertPreviewClose = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblAlertPreviewClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewClose.add(imgAlertPreviewClose, lblAlertPreviewClose);
            var flxAlertPreviewContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300dp",
                "id": "flxAlertPreviewContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "skin": "CopyslFbox0gad664d41d864b",
                "top": "100dp",
                "width": "91%",
                "zIndex": 10
            }, {}, {});
            flxAlertPreviewContent.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewContentInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300px",
                "id": "flxAlertPreviewContentInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewContentInner.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewContent1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxAlertPreviewContent1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewContent1.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewNameHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewNameHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewNameHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewNameHeader.add(lblAlertPreviewNameHeader);
            var flxAlertPreviewNameContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertPreviewNameContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "95%",
                "zIndex": 2
            }, {}, {});
            flxAlertPreviewNameContent.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewNameContent = new kony.ui.Label({
                "id": "lblAlertPreviewNameContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewNameContent\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewNameContent.add(lblAlertPreviewNameContent);
            flxAlertPreviewContent1.add(flxAlertPreviewNameHeader, flxAlertPreviewNameContent);
            var flxAlertPreviewContent2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxAlertPreviewContent2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewContent2.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewStatusHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewStatusHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewStatusHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewStatusHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewStatusHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewStatusHeader.add(lblAlertPreviewStatusHeader);
            var flxAlertPreviewStatusContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewStatusContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewStatusContent.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewStatusContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewStatusContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Yes\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewStatusContent.add(lblAlertPreviewStatusContent);
            var flxAlertPreviewChannelHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewChannelHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewChannelHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewChannelHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewChannelHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewChannelHeader\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewChannelHeader.add(lblAlertPreviewChannelHeader);
            var flxAlertPreviewChannelContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewChannelContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewChannelContent.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewChannelContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewChannelContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewChannelContent\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewChannelContent.add(lblAlertPreviewChannelContent);
            flxAlertPreviewContent2.add(flxAlertPreviewStatusHeader, flxAlertPreviewStatusContent, flxAlertPreviewChannelHeader, flxAlertPreviewChannelContent);
            var flxAlertPreviewDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertPreviewDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewDescription.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewDescriptionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewDescriptionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewDescriptionHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewDescription = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewDescriptionHeader.add(lblAlertPreviewDescription);
            var flxAlertPreviewDescriptionContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertPreviewDescriptionContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewDescriptionContent.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewDescriptionContent = new kony.ui.Label({
                "id": "lblAlertPreviewDescriptionContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewDescriptionContent\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewDescriptionContent.add(lblAlertPreviewDescriptionContent);
            flxAlertPreviewDescription.add(flxAlertPreviewDescriptionHeader, flxAlertPreviewDescriptionContent);
            var flxAlertPreviewAlertContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertPreviewAlertContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewAlertContent.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewAlertContentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewAlertContentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewAlertContentHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewAlertContentHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewAlertContentHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewAlertContentHeader\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewAlertContentHeader.add(lblAlertPreviewAlertContentHeader);
            var flxAlertPreviewAlertContentContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "70px",
                "horizontalScrollIndicator": true,
                "id": "flxAlertPreviewAlertContentContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "20px",
                "verticalScrollIndicator": true,
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewAlertContentContent.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewAlertContentContent = new kony.ui.Label({
                "id": "lblAlertPreviewAlertContentContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewAlertContentContent\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewAlertContentContent.add(lblAlertPreviewAlertContentContent);
            flxAlertPreviewAlertContent.add(flxAlertPreviewAlertContentHeader, flxAlertPreviewAlertContentContent);
            flxAlertPreviewContentInner.add(flxAlertPreviewContent1, flxAlertPreviewContent2, flxAlertPreviewDescription, flxAlertPreviewAlertContent);
            flxAlertPreviewContent.add(flxAlertPreviewContentInner);
            flxAlertPreviewMain.add(flxAlertPreviewTopColor, lblAlertPreviewHeader, flxAlertPreviewClose, flxAlertPreviewContent);
            flxAlertPreview.add(flxAlertPreviewMain);
            var flxDeleteAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeleteAlert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDeleteAlert.setDefaultUnit(kony.flex.DP);
            var popUpDeactivate = new com.adminConsole.common.popUp({
                "clipBounds": true,
                "height": "100%",
                "id": "popUpDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "right": undefined
                    },
                    "btnPopUpDelete": {
                        "left": undefined
                    },
                    "flxPopUpButtons": {
                        "layoutType": kony.flex.FREE_FORM
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeleteAlert.add(popUpDeactivate);
            var flxPreviewPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPreviewPopup.setDefaultUnit(kony.flex.DP);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "470dp",
                "id": "flxPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "120px",
                "width": "800px",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx24ace8",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPreviewPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPreviewPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPreviewPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "slFbox",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblPopupClose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(lblPopupClose);
            var lblPopUpHeader = new kony.ui.Label({
                "id": "lblPopUpHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PreviewMode\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewPopupHeader.add(flxPopUpClose, lblPopUpHeader);
            var flxProductsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxProductsTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductsTabs.setDefaultUnit(kony.flex.DP);
            var flxProductsTabsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxProductsTabsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxedefefOpNoBorder",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxProductsTabsWrapper.setDefaultUnit(kony.flex.DP);
            var btnSMS = new kony.ui.Button({
                "height": "37px",
                "id": "btnSMS",
                "isVisible": true,
                "left": "20px",
                "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SMSCaps\")",
                "top": "13px",
                "width": "60dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPushNoti = new kony.ui.Button({
                "height": "37px",
                "id": "btnPushNoti",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PUSHCaps\")",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEmail = new kony.ui.Button({
                "height": "37px",
                "id": "btnEmail",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.EMAILCaps\")",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNotifCenter = new kony.ui.Button({
                "height": "37px",
                "id": "btnNotifCenter",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.NOTIFCENTERCaps\")",
                "top": "13px",
                "width": "200dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProductsTabsWrapper.add(btnSMS, btnPushNoti, btnEmail, btnNotifCenter);
            flxProductsTabs.add(flxProductsTabsWrapper);
            var flxTemplatePreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68px",
                "id": "flxTemplatePreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewHeader.setDefaultUnit(kony.flex.DP);
            var flxPreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPreviewHeader.setDefaultUnit(kony.flex.DP);
            var lblPreviewSubHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader1",
                "isVisible": true,
                "left": 35,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SummerDiscount\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxVerticalSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": 20,
                "id": "flxVerticalSeprator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxVerticalSeprator.setDefaultUnit(kony.flex.DP);
            flxVerticalSeprator.add();
            var lblPreviewSubHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader2",
                "isVisible": false,
                "left": 15,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLatoBold35475f14px",
                "text": "12/12/2017",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewHeader.add(lblPreviewSubHeader1, flxVerticalSeprator, lblPreviewSubHeader2);
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxTemplatePreviewHeader.add(flxPreviewHeader, flxHeaderSeperator);
            var flxTemplatePreviewBody = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 20,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "150dp",
                "horizontalScrollIndicator": true,
                "id": "flxTemplatePreviewBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "91%"
            }, {}, {});
            flxTemplatePreviewBody.setDefaultUnit(kony.flex.DP);
            var lblPreviewTemplateBody = new kony.ui.Label({
                "id": "lblPreviewTemplateBody",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TitleGetCredited\")",
                "top": "30dp",
                "width": "730dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxViewer = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxViewer",
                "isVisible": false,
                "left": "0px",
                "requestURLConfig": {
                    "URL": "richtextViewer.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewBody.add(lblPreviewTemplateBody, rtxViewer);
            var flxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "3px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnPopUpCancel",
                "isVisible": true,
                "right": "35px",
                "skin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.close\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpButtons.add(btnPopUpCancel);
            flxPopUp.add(flxPopUpTopColor, flxPreviewPopupHeader, flxProductsTabs, flxTemplatePreviewHeader, flxTemplatePreviewBody, flxPopUpButtons);
            flxPreviewPopup.add(flxPopUp);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxAddSubAlertPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddSubAlertPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxAddSubAlertPopUp.setDefaultUnit(kony.flex.DP);
            var flxAddSubAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxAddSubAlert",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "80px",
                "width": "650px",
                "zIndex": 1
            }, {}, {});
            flxAddSubAlert.setDefaultUnit(kony.flex.DP);
            var flxAlertTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAlertTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflx24ace8",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTopColor.setDefaultUnit(kony.flex.DP);
            flxAlertTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAlertClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAlertClose.setDefaultUnit(kony.flex.DP);
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertClose.add(fontIconImgCLose);
            var flxheader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxheader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxheader.setDefaultUnit(kony.flex.DP);
            var lblAddSubAlertHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddSubAlertHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddSubAlert\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxheader.add(lblAddSubAlertHeader);
            var flxAlertDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxAlertDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "15dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetails.setDefaultUnit(kony.flex.DP);
            var flxSubAlertName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxSubAlertName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlertName.setDefaultUnit(kony.flex.DP);
            var txtbxSubAlertName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxSubAlertName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "placeholder": "Sub Alert Name",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblAlertName = new kony.ui.Label({
                "id": "lblAlertName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertName\")",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertNameCount = new kony.ui.Label({
                "id": "lblAlertNameCount",
                "isVisible": false,
                "right": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupNameCount\")",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxErrorAlertName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorAlertName",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorAlertName.setDefaultUnit(kony.flex.DP);
            var lblErrorAlertNameIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorAlertNameIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorAlertName = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorAlertName",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertNameCannotBeEmpty\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorAlertName.add(lblErrorAlertNameIcon, lblErrorAlertName);
            var flxAlertNameAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "2px",
                "clipBounds": true,
                "height": "15dp",
                "id": "flxAlertNameAvailable",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertNameAvailable.setDefaultUnit(kony.flex.DP);
            var lblAlertNameAvailable = new kony.ui.Label({
                "height": "15dp",
                "id": "lblAlertNameAvailable",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertNameAvailable\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertNameAvailable.add(lblAlertNameAvailable);
            flxSubAlertName.add(txtbxSubAlertName, lblAlertName, lblAlertNameCount, flxErrorAlertName, flxAlertNameAvailable);
            var flxSubAlertCodeListBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSubAlertCodeListBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "217dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlertCodeListBox.setDefaultUnit(kony.flex.DP);
            var lblSubAlertCodeKey = new kony.ui.Label({
                "id": "lblSubAlertCodeKey",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertCode\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstbxSubAlertCode = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstbxSubAlertCode",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "select alert code"],
                    ["lb2", "Alert_Code_1"],
                    ["lb3", "Alert_Code_2"],
                    ["lb4", "Alert_Code_3"]
                ],
                "selectedKey": "lb1",
                "selectedKeyValue": ["lb1", "select alert code"],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxErrorAlertCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorAlertCode",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorAlertCode.setDefaultUnit(kony.flex.DP);
            var lblErrorAlertCodeIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorAlertCodeIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorAlertCode = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorAlertCode",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SelectAlertCode\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorAlertCode.add(lblErrorAlertCodeIcon, lblErrorAlertCode);
            flxSubAlertCodeListBox.add(lblSubAlertCodeKey, lstbxSubAlertCode, flxErrorAlertCode);
            var flxSubAlertCodeGrey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSubAlertCodeGrey",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 217,
                "isModalContainer": false,
                "skin": "sknTbxDisabledf3f3f3",
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursorDisabled"
            });
            flxSubAlertCodeGrey.setDefaultUnit(kony.flex.DP);
            flxSubAlertCodeGrey.add();
            var flxAlertStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85px",
                "id": "flxAlertStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "445dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100dp"
            }, {}, {});
            flxAlertStatus.setDefaultUnit(kony.flex.DP);
            var lblAlertStatus = new kony.ui.Label({
                "id": "lblAlertStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Status\")",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertStatusActive = new kony.ui.Label({
                "id": "lblAlertStatusActive",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Active\")",
                "top": "37dp",
                "width": "38dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAlertStatusIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAlertStatusIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "48px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35px",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxAlertStatusIcon.setDefaultUnit(kony.flex.DP);
            var AlertStatusSwitch = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "AlertStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "customSwitch": {
                        "height": "25dp",
                        "isVisible": true,
                        "left": "0px",
                        "top": "0px",
                        "width": "100%"
                    },
                    "switchToggle": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertStatusIcon.add(AlertStatusSwitch);
            flxAlertStatus.add(lblAlertStatus, lblAlertStatusActive, flxAlertStatusIcon);
            flxAlertDetails.add(flxSubAlertName, flxSubAlertCodeListBox, flxSubAlertCodeGrey, flxAlertStatus);
            var flxTextArea = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "200dp",
                "id": "flxTextArea",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxTextArea.setDefaultUnit(kony.flex.DP);
            var lblAlertDescription = new kony.ui.Label({
                "id": "lblAlertDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertDesc\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertDescriptionSize = new kony.ui.Label({
                "id": "lblAlertDescriptionSize",
                "isVisible": false,
                "right": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount2\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAlertDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "170dp",
                "id": "txtAlertDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 200,
                "numberOfVisibleLines": 3,
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxTextArea.add(lblAlertDescription, lblAlertDescriptionSize, txtAlertDescription);
            var flxNoDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxNoDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoDescriptionErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoDescriptionErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoDescriptionError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblNoDescriptionError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertDescCannotBeEmpty\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoDescriptionError.add(lblNoDescriptionErrorIcon, lblNoDescriptionError);
            flxPopupHeader.add(flxAlertClose, flxheader, flxAlertDetails, flxTextArea, flxNoDescriptionError);
            var flxEditButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFooter",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditButtons.setDefaultUnit(kony.flex.DP);
            var flxError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxError.setDefaultUnit(kony.flex.DP);
            var lblErrorMsg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorMsg",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.lblErrorMsg\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgError = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgError",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "error_1x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxError.add(lblErrorMsg, imgError);
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnCancel",
                "isVisible": true,
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "right": "155px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnsave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40dp",
                "id": "btnsave",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                "top": "0%",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxEditButtons.add(flxError, btnCancel, btnsave);
            flxAddSubAlert.add(flxAlertTopColor, flxPopupHeader, flxEditButtons);
            flxAddSubAlertPopUp.add(flxAddSubAlert);
            var flxSequencePopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSequencePopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxSequencePopUp.setDefaultUnit(kony.flex.DP);
            var flxSequenceContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxSequenceContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "80px",
                "width": "380px",
                "zIndex": 1
            }, {}, {});
            flxSequenceContainer.setDefaultUnit(kony.flex.DP);
            var flxTopVerticalColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopVerticalColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxBanner006CCA",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopVerticalColor.setDefaultUnit(kony.flex.DP);
            flxTopVerticalColor.add();
            var flxSequenceBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSequenceBody",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSequenceBody.setDefaultUnit(kony.flex.DP);
            var flxReorderAlertHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxReorderAlertHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxReorderAlertHeader.setDefaultUnit(kony.flex.DP);
            var lblReorderAlert = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblReorderAlert",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ReorderAlertGroups\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPopupCloseSequence = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopupCloseSequence",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxPopupCloseSequence.setDefaultUnit(kony.flex.DP);
            var lblIconCloseSeqPopUp = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblIconCloseSeqPopUp",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopupCloseSequence.add(lblIconCloseSeqPopUp);
            flxReorderAlertHeader.add(lblReorderAlert, flxPopupCloseSequence);
            var flxSubHeaderSequencePopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxSubHeaderSequencePopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "50dp",
                "zIndex": 1
            }, {}, {});
            flxSubHeaderSequencePopUp.setDefaultUnit(kony.flex.DP);
            var lblSubHeaderSequencePopUp = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubHeaderSequencePopUp",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ReorderAlertGroup\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubHeaderSequencePopUp.add(lblSubHeaderSequencePopUp);
            var flxSequenceListSegment = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "20dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "250dp",
                "horizontalScrollIndicator": true,
                "id": "flxSequenceListSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "pagingEnabled": false,
                "right": 15,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxffffffbordere1e5ed",
                "top": "90dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxSequenceListSegment.setDefaultUnit(kony.flex.DP);
            var segSequenceList = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "groupCells": false,
                "id": "segSequenceList",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "59dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "e1e5ed00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSequenceListSegment.add(segSequenceList);
            var flxAlertSequenceIcons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "248dp",
                "id": "flxAlertSequenceIcons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxFFFFFF1000",
                "top": "91dp",
                "width": "60dp",
                "zIndex": 1
            }, {}, {});
            flxAlertSequenceIcons.setDefaultUnit(kony.flex.DP);
            var lblVerticalSep = new kony.ui.Label({
                "height": "250dp",
                "id": "lblVerticalSep",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknSeparator",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxArrowTopMostPosition = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxArrowTopMostPosition",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxArrowTopMostPosition.setDefaultUnit(kony.flex.DP);
            var lblArrowTopMostPosition = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "id": "lblArrowTopMostPosition",
                "isVisible": true,
                "skin": "sknIcon20px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator1 = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeperator1",
                "isVisible": true,
                "left": "5dp",
                "right": "5dp",
                "skin": "sknSeparator",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowTopMostPosition.add(lblArrowTopMostPosition, lblSeperator1);
            var flxArrowTopPosition = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxArrowTopPosition",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "60dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxArrowTopPosition.setDefaultUnit(kony.flex.DP);
            var lblArrowTopPosition = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "id": "lblArrowTopPosition",
                "isVisible": true,
                "skin": "sknIcon00000014px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator2 = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeperator2",
                "isVisible": true,
                "left": "5dp",
                "right": "5dp",
                "skin": "sknSeparator",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowTopPosition.add(lblArrowTopPosition, lblSeperator2);
            var flxArrowBottomPosition = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxArrowBottomPosition",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "120dp",
                "width": "60dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxArrowBottomPosition.setDefaultUnit(kony.flex.DP);
            var lblArrowBottomPosition = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "id": "lblArrowBottomPosition",
                "isVisible": true,
                "skin": "sknIcon00000014px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator3 = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeperator3",
                "isVisible": true,
                "left": "5dp",
                "right": "5dp",
                "skin": "sknSeparator",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowBottomPosition.add(lblArrowBottomPosition, lblSeperator3);
            var flxArrowBottomMostPosition = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxArrowBottomMostPosition",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "180dp",
                "width": "60dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxArrowBottomMostPosition.setDefaultUnit(kony.flex.DP);
            var lblArrowBottomMostPosition = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "id": "lblArrowBottomMostPosition",
                "isVisible": true,
                "skin": "sknIcon20px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowBottomMostPosition.add(lblArrowBottomMostPosition);
            flxAlertSequenceIcons.add(lblVerticalSep, flxArrowTopMostPosition, flxArrowTopPosition, flxArrowBottomPosition, flxArrowBottomMostPosition);
            flxSequenceBody.add(flxReorderAlertHeader, flxSubHeaderSequencePopUp, flxSequenceListSegment, flxAlertSequenceIcons);
            var flxSequencePopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxSequencePopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFooter",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSequencePopUpButtons.setDefaultUnit(kony.flex.DP);
            var flxSequencePopUpError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxSequencePopUpError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxSequencePopUpError.setDefaultUnit(kony.flex.DP);
            var lblErrorMessageSequencePopUp = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorMessageSequencePopUp",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.lblErrorMsg\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgErrorSequencePopUp = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgErrorSequencePopUp",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "error_1x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSequencePopUpError.add(lblErrorMessageSequencePopUp, imgErrorSequencePopUp);
            var btnCancelSequencePopup = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnCancelSequencePopup",
                "isVisible": true,
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "right": "155px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnUpdateSequencePopup = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40dp",
                "id": "btnUpdateSequencePopup",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.UPDATE\")",
                "top": "0%",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxSequencePopUpButtons.add(flxSequencePopUpError, btnCancelSequencePopup, btnUpdateSequencePopup);
            flxSequenceContainer.add(flxTopVerticalColor, flxSequenceBody, flxSequencePopUpButtons);
            flxSequencePopUp.add(flxSequenceContainer);
            flxAlertsManagement.add(flxLeftPannel, flxRightPanel, flxAlertPreview, flxDeleteAlert, flxPreviewPopup, flxHeaderDropdown, flxToastMessage, flxLoading, flxAddSubAlertPopUp, flxSequencePopUp);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxAlertsManagement, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmAlertsManagement,
            "enabledForIdleTimeout": true,
            "id": "frmAlertsManagement",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_c9e9acfca53b4907a1442bb367a1ca10(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_cfc12b9489e74d88ba6cee52fcb55a15,
            "retainScrollPosition": false
        }]
    }
});