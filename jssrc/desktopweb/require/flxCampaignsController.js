define("userflxCampaignsController", {
    //Type your controller code here 
});
define("flxCampaignsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_aeb8168ea3624740bf06d583316c5596: function AS_FlexContainer_aeb8168ea3624740bf06d583316c5596(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});
define("flxCampaignsController", ["userflxCampaignsController", "flxCampaignsControllerActions"], function() {
    var controller = require("userflxCampaignsController");
    var controllerActions = ["flxCampaignsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
