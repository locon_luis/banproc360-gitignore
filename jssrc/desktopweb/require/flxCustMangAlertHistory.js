define("flxCustMangAlertHistory", function() {
    return function(controller) {
        var flxCustMangAlertHistory = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxCustMangAlertHistory",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "skin": "slFbox"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustMangAlertHistory.setDefaultUnit(kony.flex.DP);
        var flxAlertName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxAlertName.setDefaultUnit(kony.flex.DP);
        var lblAlertName = new kony.ui.Label({
            "id": "lblAlertName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Summer discount",
            "top": "18dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAlertName.add(lblAlertName);
        var flxSentDate = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSentDate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "12%",
            "zIndex": 1
        }, {}, {});
        flxSentDate.setDefaultUnit(kony.flex.DP);
        var lblSentDate = new kony.ui.Label({
            "id": "lblSentDate",
            "isVisible": true,
            "left": 0,
            "skin": "sknlblLatoBold35475f14px",
            "text": "12/12/2017",
            "top": "18dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSentDate.add(lblSentDate);
        var flxFirstColoum = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxFirstColoum",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "37%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "41.50%",
            "zIndex": 2
        }, {}, {});
        flxFirstColoum.setDefaultUnit(kony.flex.DP);
        var flxChannel1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxChannel1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxChannel1.setDefaultUnit(kony.flex.DP);
        var lblChannel1 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblChannel1",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Push",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgIconChannel1 = new kony.ui.Label({
            "id": "imgIconChannel1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "text": "",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxChannel1.add(lblChannel1, imgIconChannel1);
        var flxChannel2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxChannel2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxChannel2.setDefaultUnit(kony.flex.DP);
        var lblChannel2 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblChannel2",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "SMS",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgIconChannel2 = new kony.ui.Label({
            "id": "imgIconChannel2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "text": "",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxChannel2.add(lblChannel2, imgIconChannel2);
        var flxChannel3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxChannel3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxChannel3.setDefaultUnit(kony.flex.DP);
        var lblChannel3 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblChannel3",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Email",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgIconChannel3 = new kony.ui.Label({
            "id": "imgIconChannel3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "text": "",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxChannel3.add(lblChannel3, imgIconChannel3);
        var flxChannel4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxChannel4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "60%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "40%",
            "zIndex": 1
        }, {}, {});
        flxChannel4.setDefaultUnit(kony.flex.DP);
        var lblChannel4 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblChannel4",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Notification Center",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgIconChannel4 = new kony.ui.Label({
            "id": "imgIconChannel4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "text": "",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxChannel4.add(lblChannel4, imgIconChannel4);
        flxFirstColoum.add(flxChannel1, flxChannel2, flxChannel3, flxChannel4);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxAddAlertBtn = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAddAlertBtn",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "85%",
            "isModalContainer": false,
            "skin": "sknServiceDetailsContent",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, {}, {});
        flxAddAlertBtn.setDefaultUnit(kony.flex.DP);
        var btnAddAlerts = new kony.ui.Button({
            "centerY": "50%",
            "height": "20px",
            "id": "btnAddAlerts",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "text": "Preview",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAddAlertBtn.add(btnAddAlerts);
        flxCustMangAlertHistory.add(flxAlertName, flxSentDate, flxFirstColoum, lblSeperator, flxAddAlertBtn);
        return flxCustMangAlertHistory;
    }
})