define("flxDeviceManagement", function() {
    return function(controller) {
        var flxDeviceManagement = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "51dp",
            "id": "flxDeviceManagement",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDeviceManagement.setDefaultUnit(kony.flex.DP);
        var flxRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxRow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {}, {
            "hoverSkin": "sknHoverWithoutHandCursor"
        });
        flxRow.setDefaultUnit(kony.flex.DP);
        var flxDeviceName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxDeviceName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a43b80cc872f4831807a918bd48f0ac3,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20.30%",
            "zIndex": 1
        }, {}, {});
        flxDeviceName.setDefaultUnit(kony.flex.DP);
        var flxDeviceNameValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDeviceNameValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, {}, {});
        flxDeviceNameValue.setDefaultUnit(kony.flex.DP);
        var lblDeviceName = new kony.ui.Label({
            "centerY": "50%",
            "height": "15px",
            "id": "lblDeviceName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDeviceNameValue.add(lblDeviceName);
        var flxDeviceInfo = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDeviceInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20px"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxDeviceInfo.setDefaultUnit(kony.flex.DP);
        var fontIconDeviceInfo = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconDeviceInfo",
            "isVisible": true,
            "skin": "sknfontIcon13px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDeviceInfo.add(fontIconDeviceInfo);
        flxDeviceName.add(flxDeviceNameValue, flxDeviceInfo);
        var lblVersion = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblVersion",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.customermanagement.deviceversion\")",
            "width": "14.20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblChannel = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblChannel",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
            "width": "10.20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLastUserIP = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblLastUserIP",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
            "width": "16.40%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLastAcessedOn = new kony.ui.Label({
            "height": "100%",
            "id": "lblLastAcessedOn",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
            "width": "20.30%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "height": "100%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
            "top": "0",
            "width": "13.40%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatusInfo = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxStatusInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "70dp",
            "skin": "slFbox",
            "top": "0px",
            "width": "5.40%",
            "zIndex": 10
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxStatusInfo.setDefaultUnit(kony.flex.DP);
        var fontIconStatusInfo = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconStatusInfo",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIcon13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonActiveSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatusInfo.add(fontIconStatusInfo);
        flxRow.add(flxDeviceName, lblVersion, lblChannel, lblLastUserIP, lblLastAcessedOn, lblStatus, flxStatusInfo);
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "50dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDeviceManagement.add(flxRow, lblSeperator);
        return flxDeviceManagement;
    }
})