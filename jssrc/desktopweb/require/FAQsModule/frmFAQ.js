define("FAQsModule/frmFAQ", function() {
    return function(controller) {
        function addWidgetsfrmFAQ() {
            this.setDefaultUnit(kony.flex.DP);
            var flxFAQ = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "624dp",
                "id": "flxFAQ",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFAQ.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 100
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "right": "0px"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQController.FAQs\")"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxNoFAQWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "89.40%",
                "id": "flxNoFAQWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "106dp",
                "zIndex": 1
            }, {}, {});
            flxNoFAQWrapper.setDefaultUnit(kony.flex.DP);
            var flxNoFAQCreated = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "350px",
                "id": "flxNoFAQCreated",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "35px",
                "zIndex": 1
            }, {}, {});
            flxNoFAQCreated.setDefaultUnit(kony.flex.DP);
            var noStaticData = new com.adminConsole.staticContent.noStaticData({
                "clipBounds": true,
                "height": "340px",
                "id": "noStaticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "btnAddStaticContent": {
                        "maxWidth": "100px",
                        "text": "ADD FAQ",
                        "width": "100px"
                    },
                    "noStaticData": {
                        "left": "0px",
                        "right": "0px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoFAQCreated.add(noStaticData);
            flxNoFAQWrapper.add(flxNoFAQCreated);
            var flxAddFAQ = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxAddFAQ",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "pagingEnabled": false,
                "right": "35px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknScrollFlxffffffOpd5d9dd",
                "top": "140px",
                "verticalScrollIndicator": true,
                "zIndex": 100
            }, {}, {});
            flxAddFAQ.setDefaultUnit(kony.flex.DP);
            var flxAddMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddMain.setDefaultUnit(kony.flex.DP);
            var flxFAQContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxFAQContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "35px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFAQContainer.setDefaultUnit(kony.flex.DP);
            var flxFAQQuestion = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxFAQQuestion",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "400px",
                "zIndex": 1
            }, {}, {});
            flxFAQQuestion.setDefaultUnit(kony.flex.DP);
            var lblFAQQuestionId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFAQQuestionId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.lblFAQQuestionId\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFAQValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFAQValue",
                "isVisible": true,
                "left": "5px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.lblFAQValue\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFAQQuestion.add(lblFAQQuestionId, lblFAQValue);
            var flxFAQCategory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxFAQCategory",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxFAQCategory.setDefaultUnit(kony.flex.DP);
            var lblFAQCategory = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFAQCategory",
                "isVisible": true,
                "left": "50px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.Select_Category\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxCategory = new kony.ui.ListBox({
                "centerY": "50%",
                "height": "20px",
                "id": "lbxCategory",
                "isVisible": true,
                "right": "4px",
                "skin": "sknlstbx485c7513px",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxFAQCategory.add(lblFAQCategory, lbxCategory);
            flxFAQContainer.add(flxFAQQuestion, flxFAQCategory);
            var flxQuestion = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxQuestion",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxQuestion.setDefaultUnit(kony.flex.DP);
            var lblQuestion = new kony.ui.Label({
                "id": "lblQuestion",
                "isVisible": true,
                "left": "35px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.lblQuestion\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgMandatory = new kony.ui.Image2({
                "centerY": "50%",
                "height": "12dp",
                "id": "imgMandatory",
                "isVisible": false,
                "left": "80px",
                "right": "5px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMessageSize = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMessageSize",
                "isVisible": true,
                "right": "35px",
                "skin": "sknllbl485c75Lato13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.lblMessageSize\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxQuestion.add(lblQuestion, imgMandatory, lblMessageSize);
            var flxQuestionBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxQuestionBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "15px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxQuestionBox.setDefaultUnit(kony.flex.DP);
            var txbFaqQuestion = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "50px",
                "id": "txbFaqQuestion",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "35px",
                "maxTextLength": 300,
                "placeholder": "Add Question",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxQuestionBox.add(txbFaqQuestion);
            var flxNoQuestionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoQuestionError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoQuestionError.setDefaultUnit(kony.flex.DP);
            var lblNoQuestionError = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNoQuestionError",
                "isVisible": true,
                "left": "50dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.lblNoQuestion\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoQuestionErrorIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblNoQuestionErrorIcon",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknErrorIcon",
                "text": "",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoQuestionError.add(lblNoQuestionError, lblNoQuestionErrorIcon);
            var addStaticData = new com.adminConsole.staticContent.addStaticData({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "height": "300px",
                "id": "addStaticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100",
                "top": "25px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "overrides": {
                    "addStaticData": {
                        "bottom": "10px",
                        "height": "300px",
                        "left": "0px",
                        "right": "0px",
                        "top": "25px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxRichText": {
                        "bottom": "viz.val_cleared",
                        "height": "175dp",
                        "left": "0px",
                        "top": "15dp"
                    },
                    "lblMessageSize": {
                        "text": "0/200"
                    },
                    "lblNoDescriptionError": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.lblNoAnswer\")"
                    },
                    "rtxAddStaticContent": {
                        "bottom": "viz.val_cleared",
                        "height": "170px",
                        "left": "35px",
                        "maxTextLength": 4000,
                        "right": "35px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddMain.add(flxFAQContainer, flxQuestion, flxQuestionBox, flxNoQuestionError, addStaticData);
            var flxFAQSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxFAQSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "CopyslFbox0i4769b70120c42",
                "top": "4px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFAQSeparator.setDefaultUnit(kony.flex.DP);
            flxFAQSeparator.add();
            var flxBtns = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxBtns",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "4px",
                "width": "94%",
                "zIndex": 1
            }, {}, {});
            flxBtns.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "2px"
                    },
                    "btnNext": {
                        "text": "SAVE AND UPDATE"
                    },
                    "commonButtons": {
                        "right": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "right": "1px",
                        "width": "270px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBtns.add(commonButtons);
            flxAddFAQ.add(flxAddMain, flxFAQSeparator, flxBtns);
            var flxMainContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "780px",
                "id": "flxMainContent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "Copysknscrollflxf0ja093a1ef62648",
                "top": "106px",
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var FlexSearchBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "FlexSearchBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            FlexSearchBar.setDefaultUnit(kony.flex.DP);
            var flxSelectOptionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "30px",
                "width": "107px",
                "zIndex": 111
            }, {}, {});
            flxSelectOptionsHeader.setDefaultUnit(kony.flex.DP);
            var flxActiveOption = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxActiveOption",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "35px",
                "zIndex": 2
            }, {}, {});
            flxActiveOption.setDefaultUnit(kony.flex.DP);
            var imgActive = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgActive",
                "isVisible": false,
                "skin": "slImage",
                "src": "activate_select2.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Activate"
            });
            var fonticonActiveSearch = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonActiveSearch",
                "isVisible": true,
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonActiveSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Activate"
            });
            flxActiveOption.add(imgActive, fonticonActiveSearch);
            var flxSepartor1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxSepartor1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0px",
                "width": "1px",
                "zIndex": 2
            }, {}, {});
            flxSepartor1.setDefaultUnit(kony.flex.DP);
            flxSepartor1.add();
            var flxDeactivateOption = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDeactivateOption",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {});
            flxDeactivateOption.setDefaultUnit(kony.flex.DP);
            var imgDeactivate = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgDeactivate",
                "isVisible": false,
                "skin": "slImage",
                "src": "deactive_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Deactivate"
            });
            var fonticonDeactiveSearch = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonDeactiveSearch",
                "isVisible": true,
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDeactive\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Deactivate"
            });
            flxDeactivateOption.add(imgDeactivate, fonticonDeactiveSearch);
            var flxSepartor2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxSepartor2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0px",
                "width": "1px",
                "zIndex": 2
            }, {}, {});
            flxSepartor2.setDefaultUnit(kony.flex.DP);
            flxSepartor2.add();
            var flxDeleteOption = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDeleteOption",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {});
            flxDeleteOption.setDefaultUnit(kony.flex.DP);
            var imgDelete = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgDelete",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "delete_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Delete"
            });
            var fonticonDeleteSearch = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonDeleteSearch",
                "isVisible": true,
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Delete"
            });
            flxDeleteOption.add(imgDelete, fonticonDeleteSearch);
            flxSelectOptionsHeader.add(flxActiveOption, flxSepartor1, flxDeactivateOption, flxSepartor2, flxDeleteOption);
            var search = new com.adminConsole.header.subHeader({
                "centerY": "50%",
                "clipBounds": true,
                "height": "48dp",
                "id": "search",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "width": "100%",
                "overrides": {
                    "flxClearSearchImage": {
                        "centerY": "50%",
                        "height": "12px",
                        "isVisible": false,
                        "right": "15px",
                        "top": "viz.val_cleared",
                        "width": "15px"
                    },
                    "flxMenu": {
                        "centerY": "50%",
                        "isVisible": false,
                        "top": "viz.val_cleared"
                    },
                    "flxSearch": {
                        "centerY": "50%",
                        "right": "0px",
                        "top": "viz.val_cleared"
                    },
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": "36px",
                        "top": "viz.val_cleared"
                    },
                    "flxSubHeader": {
                        "right": "0px"
                    },
                    "fontIconCross": {
                        "left": "viz.val_cleared",
                        "right": "0px"
                    },
                    "fontIconSearchImg": {
                        "left": "13px",
                        "top": "viz.val_cleared"
                    },
                    "lbxPageNumbers": {
                        "masterData": [
                            ["lb1", "10"],
                            ["lb2", "50"],
                            ["lb3", "100"]
                        ]
                    },
                    "subHeader": {
                        "centerY": "50%",
                        "isVisible": true,
                        "top": "viz.val_cleared"
                    },
                    "tbxSearchBox": {
                        "centerY": "46%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            FlexSearchBar.add(flxSelectOptionsHeader, search);
            var FlexUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "FlexUsers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffoptemplateop3px",
                "top": "80px",
                "zIndex": 1
            }, {}, {});
            FlexUsers.setDefaultUnit(kony.flex.DP);
            var FlexUserTable = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "250px",
                "id": "FlexUserTable",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            FlexUserTable.setDefaultUnit(kony.flex.DP);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100px",
                "width": "200px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var flxDeactivate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a35230cb45374d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDeactivate.setDefaultUnit(kony.flex.DP);
            var fonticonDeactive = new kony.ui.Label({
                "centerY": "50%",
                "id": "fonticonDeactive",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDeactive\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgOption2 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption2",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "deactive_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold0hf492575252c4cStatic"
            });
            flxDeactivate.add(fonticonDeactive, imgOption2, lblOption2);
            var flxEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEdit.setDefaultUnit(kony.flex.DP);
            var fonticonEdit = new kony.ui.Label({
                "centerY": "50%",
                "id": "fonticonEdit",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgOption1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption1",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold0hf492575252c4cStatic"
            });
            flxEdit.add(fonticonEdit, imgOption1, lblOption1);
            var flxDelete = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDelete",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0h1f34f2be70f43",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDelete.setDefaultUnit(kony.flex.DP);
            var fonticonDelete = new kony.ui.Label({
                "centerY": "50%",
                "id": "fonticonDelete",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgOption3 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption3",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "delete_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption3",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold0hf492575252c4cStatic"
            });
            flxDelete.add(fonticonDelete, imgOption3, lblOption3);
            flxSelectOptions.add(flxDeactivate, flxEdit, flxDelete);
            var flxtreeviewContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxtreeviewContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxtreeviewContainer.setDefaultUnit(kony.flex.DP);
            var tableView = new com.adminConsole.StaticContentManagement.faq.tableView({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "tableView",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxHeaders": {
                        "right": "0dp",
                        "width": "viz.val_cleared",
                        "zIndex": 1
                    },
                    "flxNoRecordsFound": {
                        "height": "200px",
                        "isVisible": false,
                        "top": "60px",
                        "zIndex": 1
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "flxTableView": {
                        "bottom": "0dp",
                        "height": "viz.val_cleared",
                        "isVisible": true,
                        "left": "0dp",
                        "top": "60dp",
                        "width": "viz.val_cleared"
                    },
                    "fonticonHeaderServiceStatus": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")"
                    },
                    "imgHeaderCategoryStatus": {
                        "src": "u195.png"
                    },
                    "imgHeaderCheckBox": {
                        "src": "checkbox.png"
                    },
                    "imgHeaderServiceStatus": {
                        "src": "u195.png"
                    },
                    "imgNextArrow": {
                        "src": "left_cricle.png"
                    },
                    "imgPreviousArrow": {
                        "src": "rightarrow_circel2x.png"
                    },
                    "lblHeaderSeparator": {
                        "left": "35dp",
                        "right": "35dp",
                        "width": "viz.val_cleared"
                    },
                    "rtxNoRecords": {
                        "centerY": "50%",
                        "isVisible": false,
                        "top": "viz.val_cleared"
                    },
                    "segServicesAndFaq": {
                        "bottom": "viz.val_cleared",
                        "height": "250px",
                        "left": "0dp",
                        "right": "0dp",
                        "width": "100%"
                    },
                    "tableView": {
                        "bottom": "0dp",
                        "centerX": undefined,
                        "centerY": undefined,
                        "height": "viz.val_cleared",
                        "isVisible": true,
                        "left": undefined,
                        "maxHeight": undefined,
                        "maxWidth": undefined,
                        "minHeight": undefined,
                        "minWidth": undefined,
                        "right": undefined,
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxtreeviewContainer.add(tableView);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "804dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "38dp",
                "width": "120px",
                "zIndex": 101,
                "overrides": {
                    "segStatusFilterDropdown": {
                        "width": "100%"
                    },
                    "statusFilterMenu": {
                        "left": "804dp",
                        "right": "viz.val_cleared",
                        "top": "38dp",
                        "width": "120px",
                        "zIndex": 101
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            FlexUserTable.add(flxSelectOptions, flxtreeviewContainer, statusFilterMenu);
            FlexUsers.add(FlexUserTable);
            flxMainContent.add(FlexSearchBar, FlexUsers);
            var flxBreadCrum = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrum",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "106dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxBreadCrum.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "left": "35px"
                    },
                    "lblCurrentScreen": {
                        "width": "100px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrum.add(breadcrumbs);
            flxRightPanel.add(flxMainHeader, flxNoFAQWrapper, flxAddFAQ, flxMainContent, flxBreadCrum);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "60px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "60px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxFAQ.add(flxLeftPannel, flxRightPanel, flxHeaderDropdown, flxToastMessage, flxLoading);
            var flxDeactivateFAQ = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeactivateFAQ",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDeactivateFAQ.setDefaultUnit(kony.flex.DP);
            var popUpDeactivate = new com.adminConsole.common.popUp({
                "clipBounds": true,
                "height": "100%",
                "id": "popUpDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "right": "180px"
                    },
                    "btnPopUpDelete": {
                        "left": "viz.val_cleared"
                    },
                    "flxPopUpButtons": {
                        "layoutType": kony.flex.FREE_FORM
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeactivateFAQ.add(popUpDeactivate);
            var flxDeleteFAQ = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeleteFAQ",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDeleteFAQ.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp({
                "clipBounds": true,
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "right": "150px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeleteFAQ.add(popUp);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxFAQ, flxDeactivateFAQ, flxDeleteFAQ, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmFAQ,
            "enabledForIdleTimeout": true,
            "id": "frmFAQ",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_e54ab14a468c42918556ffa2e5676112(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_c9a507440aa44451968a3593664409f3,
            "retainScrollPosition": false
        }]
    }
});