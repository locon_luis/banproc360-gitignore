define("flxSegMFAConfigs", function() {
    return function(controller) {
        var flxSegMFAConfigs = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxSegMFAConfigs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxSegMFAConfigs.setDefaultUnit(kony.flex.DP);
        var flxContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "50px",
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxContent.setDefaultUnit(kony.flex.DP);
        var flxAccordianContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxAccordianContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b5840c35f5c64575896ea4c51eae05f4,
            "right": "115px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxAccordianContainer.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "15px",
            "zIndex": 1
        }, {}, {});
        flxArrow.setDefaultUnit(kony.flex.DP);
        var fontIconImgViewDescription = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "fontIconImgViewDescription",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknfontIcon33333316px",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(fontIconImgViewDescription);
        var lblMFAConfigName = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblMFAConfigName",
            "isVisible": true,
            "left": "35px",
            "right": "0px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Secure Access Code",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccordianContainer.add(flxArrow, lblMFAConfigName);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "94.50%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_f93b89299fd440b58a6dbfb1f86740e4,
            "skin": "slFbox",
            "top": "10dp",
            "width": "25px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var imgOptions = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "imgOptions",
            "isVisible": false,
            "left": 14,
            "skin": "slImage",
            "src": "dots3x.png",
            "width": "6dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "lblIconOptions",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(imgOptions, lblIconOptions);
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "86%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "8%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblServiceStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblServiceStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgServiceStatus = new kony.ui.Image2({
            "centerY": "50%",
            "height": "10dp",
            "id": "imgServiceStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "width": "10dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblServiceStatus, imgServiceStatus);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxViewEditButton = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "22dp",
            "id": "flxViewEditButton",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "sknflxffffffop100Border424242Radius100px",
            "top": 15,
            "width": "50px",
            "zIndex": 1
        }, {}, {});
        flxViewEditButton.setDefaultUnit(kony.flex.DP);
        var lblViewEditButton = new kony.ui.Label({
            "centerX": "48%",
            "centerY": "47%",
            "id": "lblViewEditButton",
            "isVisible": true,
            "skin": "sknLbl48C75Font12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewEditButton.add(lblViewEditButton);
        flxContent.add(flxAccordianContainer, flxOptions, flxStatus, lblSeperator, flxViewEditButton);
        flxSegMFAConfigs.add(flxContent);
        return flxSegMFAConfigs;
    }
})