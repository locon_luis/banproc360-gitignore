define("userflxSegMFAConfigsController", {
    //Type your controller code here 
});
define("flxSegMFAConfigsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxAccordianContainer **/
    AS_FlexContainer_b5840c35f5c64575896ea4c51eae05f4: function AS_FlexContainer_b5840c35f5c64575896ea4c51eae05f4(eventobject, context) {
        var self = this;
        this.executeOnParent("showSelectedRow");
    },
    /** onClick defined for flxOptions **/
    AS_FlexContainer_f93b89299fd440b58a6dbfb1f86740e4: function AS_FlexContainer_f93b89299fd440b58a6dbfb1f86740e4(eventobject, context) {
        var self = this;
        //this.toggleVisibility();
        this.executeOnParent("toggleVisibility");
    }
});
define("flxSegMFAConfigsController", ["userflxSegMFAConfigsController", "flxSegMFAConfigsControllerActions"], function() {
    var controller = require("userflxSegMFAConfigsController");
    var controllerActions = ["flxSegMFAConfigsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
