define("OutageMessageModule/userfrmOutageMessageController", {
    servicesList: [],
    masterData: [],
    records: 0,
    CREATE_SCREEN: 1,
    EDIT_SCREEN: 2,
    inAscendingOrder: true,
    preShowActions: function() {
        this.setflowActions();
        kony.adminConsole.utils.showProgressBar(this.view);
        this.setPreshowData();
        this.setScrollHeight();
        this.view.flxOutageMsg.height = kony.os.deviceInfo().screenHeight + "px";
        this.view.tableView.flxTableView.setVisibility(true);
        this.view.tableView.flxNoRecordsFound.setVisibility(false);
        this.view.search.tbxSearchBox.text = "";
        this.view.search.flxSearchCancel.setVisibility(false);
        this.view.flxSelectOptionsHeader.setVisibility(false);
        this.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
        this.view.flxToastMessage.setVisibility(false);
    },
    shouldUpdateUI: function(viewModel) {
        return viewModel !== undefined && viewModel !== null;
    },
    willUpdateUI: function(outageMsgModel) {
        this.updateLeftMenu(outageMsgModel);
        if (outageMsgModel.serviceViewList) {
            this.servicesList = outageMsgModel.serviceViewList;
            this.setServiceLstBoxData(outageMsgModel.serviceViewList);
            kony.adminConsole.utils.hideProgressBar(this.view);
        }
        if (outageMsgModel.context === "viewOutageMessages") {
            this.renderOutageMessageList(outageMsgModel.outageMessageList);
            kony.adminConsole.utils.hideProgressBar(this.view);
        } else if (outageMsgModel.context === "createOutageMessage") {
            if (outageMsgModel.data) {
                this.showOutageMessageList(outageMsgModel.data.records, outageMsgModel.context);
            } else {
                this.showOutageMessageList(outageMsgModel.data, outageMsgModel.context);
            }
            this.showEditOutageMessage(this.CREATE_SCREEN);
            kony.adminConsole.utils.hideProgressBar(this.view);
        } else if (outageMsgModel.toast) {
            if (outageMsgModel.toast.status === kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success")) {
                if (outageMsgModel.toast.message === kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Outage_Message_Activated_successfully") || outageMsgModel.toast.message === kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Outage_Message_Deactivated_successfully")) this.toggleActive();
                this.view.toastMessage.showToastMessage(outageMsgModel.toast.message, this);
            } else if (outageMsgModel.toast.status === kony.i18n.getLocalizedString("i18n.frmGroupsController.error")) {
                this.view.toastMessage.showErrorToastMessage(outageMsgModel.toast.message, this);
            }
            kony.adminConsole.utils.hideProgressBar(this.view);
        } else if (outageMsgModel.context === "showLoadingScreen") {
            kony.adminConsole.utils.showProgressBar(this.view);
        } else if (outageMsgModel.context === "hideLoadingScreen") {
            kony.adminConsole.utils.hideProgressBar(this.view);
        }
        this.view.forceLayout();
    },
    renderOutageMessageList: function(outageMessageList) {
        var scope = this;
        if (outageMessageList !== undefined) {
            if (outageMessageList.length === 0) {
                //show Add new screen
                this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Add_Service_Outage_Messages");
                this.view.breadcrumbs.setVisibility(false);
                this.view.flxAddOutageMessage.setVisibility(false);
                this.view.flxNoOutageMsgWrapper.setVisibility(true);
                this.view.tableView.segServicesAndFaq.setData([]);
                this.view.mainHeader.flxHeaderSeperator.setVisibility(false);
                this.view.mainHeader.btnAddNewOption.isVisible = false;
                this.view.flxMainContent.setVisibility(false);
            } else {
                //display the list
                this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.userwidgetmodel.lblServiceOutageMessages");
                this.view.flxNoOutageMsgWrapper.setVisibility(false);
                this.view.mainHeader.btnAddNewOption.isVisible = true;
                this.view.mainHeader.flxHeaderSeperator.setVisibility(true);
                if (outageMessageList) {
                    var self = this;
                    this.sortBy = this.getObjectSorter("Name");
                    self.resetSortImages();
                    this.loadPageData = function() {
                        self.showOutageMessageList(outageMessageList.sort(self.sortBy.sortData));
                    };
                    this.loadPageData();
                }
            }
        }
    },
    setflowActions: function() {
        var scopeObj = this;
        this.view.tableView.segServicesAndFaq.onHover = this.saveScreenY;
        this.view.noStaticData.btnAddStaticContent.onClick = function() {
            scopeObj.showEditOutageMessage(scopeObj.CREATE_SCREEN);
        };
        this.view.search.tbxSearchBox.onTouchStart = function() {
            scopeObj.view.search.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
        };
        this.view.search.tbxSearchBox.onKeyUp = function() {
            if (scopeObj.view.search.tbxSearchBox.text !== "") scopeObj.view.search.flxSearchCancel.setVisibility(true);
            else scopeObj.view.search.flxSearchCancel.setVisibility(false);
            scopeObj.view.flxSelectOptionsHeader.setVisibility(false);
            scopeObj.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
            scopeObj.searchOutageMessages();
        };
        this.view.search.tbxSearchBox.onEndEditing = function() {
            scopeObj.view.search.flxSearchContainer.skin = "sknflxd5d9ddop100";
        };
        this.view.commonButtons.btnCancel.onClick = function() {
            scopeObj.fetchOutageMessageData();
        };
        this.view.addStaticData.rtxAddStaticContent.onKeyUp = function() {
            var messageLength = scopeObj.view.addStaticData.rtxAddStaticContent.text.trim().length;
            if (messageLength === 0) {
                scopeObj.view.addStaticData.lblMessageSize.setVisibility(false);
            } else {
                scopeObj.view.addStaticData.lblMessageSize.setVisibility(true);
                scopeObj.view.addStaticData.lblMessageSize.text = messageLength + "/200";
            }
            scopeObj.view.forceLayout();
            scopeObj.hideDescriptionError();
        };
        this.view.commonButtons.btnNext.onClick = function() {
            var isEdit, currPage;
            scopeObj.view.lbxOutageMsg.setEnabled(true); // Edit viene Deshabilitada 
            if (scopeObj.view.lbxOutageMsg.selectedKey !== "placeholder" && scopeObj.view.addStaticData.rtxAddStaticContent.text.trim() !== "") {
                if (scopeObj.view.commonButtons.btnSave.text === kony.i18n.getLocalizedString("i18n.permission.SAVE")) {
                    isEdit = false;
                    currPage = true;
                    scopeObj.onSaveClicked(isEdit, currPage);
                    scopeObj.view.addStaticData.rtxAddStaticContent.text = "";
                } else {
                    isEdit = true;
                    currPage = true;
                    scopeObj.onSaveClicked(isEdit, currPage);
                }
            } else {
                scopeObj.showErrorMessage();
            }
        };
        this.view.commonButtons.btnSave.onClick = function() {
            var isEdit = false;
            var currPage = false;
            if (scopeObj.view.lbxOutageMsg.selectedKey !== "placeholder" && scopeObj.view.addStaticData.rtxAddStaticContent.text.trim() !== "") {
                if (scopeObj.view.commonButtons.btnSave.text === kony.i18n.getLocalizedString("i18n.permission.SAVE")) {
                    isEdit = false;
                } else {
                    isEdit = true;
                }
                scopeObj.onSaveClicked(isEdit, currPage);
                scopeObj.showOutageMessageList();
            } else {
                scopeObj.showErrorMessage();
            }
        };
        this.view.search.flxSearchCancel.onClick = function() {
            scopeObj.view.search.tbxSearchBox.text = "";
            scopeObj.view.search.flxSearchCancel.setVisibility(false);
            scopeObj.view.flxSelectOptionsHeader.setVisibility(false);
            scopeObj.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
            scopeObj.hideNoResultsFound();
            scopeObj.searchOutageMessages();
            scopeObj.view.forceLayout();
        };
        this.view.tableView.flxHeaderCheckbox.onTouchEnd = function() {
            scopeObj.selectAllRows();
        };
        this.view.tableView.flxHeaderServiceName.onClick = function() {
            scopeObj.sortBy.column("Name");
            scopeObj.loadPageData();
            scopeObj.resetSortImages();
        };
        this.view.tableView.flxHeaderStatus.onClick = function() {
            scopeObj.sortBy.column("Status_id");
            scopeObj.loadPageData();
            scopeObj.resetSortImages();
        };
        this.view.mainHeader.btnAddNewOption.onClick = function() {
            scopeObj.view.addStaticData.lblMessageSize.setVisibility(false);
            scopeObj.hideDescriptionError();
            scopeObj.showEditOutageMessage(scopeObj.CREATE_SCREEN);
        };
        this.view.breadcrumbs.btnBackToMain.onClick = function() {
            scopeObj.fetchOutageMessageData();
        };
        this.view.flxDeactivate.onClick = function() {
            if (scopeObj.view.lblOption2.text === kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate")) scopeObj.showDeactivate();
            else scopeObj.showActivate();
        };
        this.view.flxDelete.onClick = function() {
            scopeObj.showDeleteOutage();
        };
        this.view.flxEdit.onClick = function() {
            scopeObj.hideDescriptionError();
            scopeObj.showEditOutageMessage(scopeObj.EDIT_SCREEN);
        };
        this.view.popUpDeactivate.btnPopUpCancel.onClick = function() {
            scopeObj.view.flxDeactivateOutageMessage.setVisibility(false);
            scopeObj.view.search.tbxSearchBox.text = "";
            scopeObj.view.search.flxSearchCancel.setVisibility(false);
            scopeObj.view.flxSelectOptionsHeader.setVisibility(false);
            scopeObj.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
            scopeObj.fetchOutageMessageData();
        };
        this.view.popUpDeactivate.btnPopUpDelete.onClick = function() {
            scopeObj.view.flxDeactivateOutageMessage.setVisibility(false);
            scopeObj.view.flxSelectOptions.isVisible = false;
            scopeObj.view.flxDeactivateOutageMessage.isVisible = false;
            scopeObj.changeStatus();
        };
        this.view.popUp.btnPopUpCancel.onClick = function() {
            scopeObj.view.flxDeleteOutageMessage.setVisibility(false);
            scopeObj.fetchOutageMessageData();
        };
        this.view.popUpDeactivate.flxPopUpClose.onClick = function() {
            scopeObj.view.flxDeactivateOutageMessage.setVisibility(false);
            scopeObj.fetchOutageMessageData();
        };
        this.view.popUp.flxPopUpClose.onClick = function() {
            scopeObj.view.flxDeleteOutageMessage.setVisibility(false);
            scopeObj.view.search.tbxSearchBox.text = "";
            scopeObj.view.search.flxSearchCancel.setVisibility(false);
            scopeObj.view.flxSelectOptionsHeader.setVisibility(false);
            scopeObj.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
            scopeObj.fetchOutageMessageData();
        };
        this.view.tableView.segServicesAndFaq.onRowClick = function() {
            var limit = scopeObj.view.tableView.segServicesAndFaq.data.length;
            if (scopeObj.view.tableView.segServicesAndFaq.selectedIndices) {
                scopeObj.view.flxSelectOptionsHeader.setVisibility(true);
                var index = scopeObj.view.tableView.segServicesAndFaq.selectedIndices[0][1].length;
                scopeObj.toggleCheckbox2();
                if (index === limit) {
                    scopeObj.view.tableView.imgHeaderCheckBox.src = "checkboxselected.png";
                } else {
                    scopeObj.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
                }
            } else {
                scopeObj.view.flxSelectOptionsHeader.setVisibility(false);
            }
        };
        this.view.popUp.btnPopUpDelete.onClick = function() {
            scopeObj.deleteMessage();
            scopeObj.view.flxDeleteOutageMessage.setVisibility(false);
            scopeObj.fetchOutageMessageData();
        };
        this.view.lbxOutageMsg.onSelection = function() {
            scopeObj.checkMessageAvailable(scopeObj.view.lbxOutageMsg.selectedKey);
        };
        this.view.flxSelectOptionsHeader.flxDeleteOption.onTouchEnd = function() {
            scopeObj.allDelete();
        };
        this.view.flxSelectOptionsHeader.flxDeactivateOption.onTouchEnd = function() {
            scopeObj.allStatusChange(kony.i18n.getLocalizedString("i18n.frmOutageMessageController.deactive"));
        };
        this.view.flxSelectOptionsHeader.flxActiveOption.onTouchEnd = function() {
            scopeObj.allStatusChange(kony.i18n.getLocalizedString("i18n.frmOutageMessageController.active"));
        };
        this.view.flxSelectOptions.onHover = scopeObj.onHoverEventCallback;
    },
    onHoverEventCallback: function(widget, context) {
        var self = this;
        var path = "";
        if (widget.id === "flxSelectOptions") path = this.view.flxSelectOptions;
        if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
            path.setVisibility(true);
        } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
            path.setVisibility(false);
            this.clearSelection();
        }
    },
    clearSelection: function() {
        this.view.tableView.segServicesAndFaq.selectedIndices = null;
        this.view.flxSelectOptionsHeader.setVisibility(false);
        this.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
        this.view.search.flxSearchCancel.setVisibility(false);
        this.view.forceLayout();
    },
    showDeleteOutage: function() {
        this.view.popUp.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.OutageMessage.delete");
        this.view.popUp.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.delete_Outage_Message_Popup");
        this.view.popUp.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.NO__LEAVE_AS_IT_IS");
        this.view.popUp.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesDelete");
        this.view.popUp.btnPopUpCancel.right = 150 + "px";
        this.view.toastMessage.lbltoastMessage.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Deleted_Outage_Message_successfully");
        this.view.flxDeleteOutageMessage.setVisibility(true);
        this.view.search.tbxSearchBox.text = "";
        this.view.search.flxSearchCancel.setVisibility(false);
        this.view.flxSelectOptionsHeader.setVisibility(false);
        this.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
        this.view.forceLayout();
    },
    toggleActive: function() {
        var index = this.view.tableView.segServicesAndFaq.selectedRowIndex;
        var a = index[1];
        var rowIndex = kony.store.getItem("mainSegmentRowIndex");
        var data = this.view.tableView.segServicesAndFaq.data;
        if (this.view.tableView.segServicesAndFaq.data[a].lblServiceStatus.text === kony.i18n.getLocalizedString("i18n.secureimage.Active")) {
            this.view.tableView.segServicesAndFaq.data[a].lblServiceStatus.text = kony.i18n.getLocalizedString("i18n.secureimage.Inactive");
            this.view.tableView.segServicesAndFaq.data[a].lblServiceStatus.skin = "sknlblLatocacacaBold12px";
            this.view.tableView.segServicesAndFaq.data[a].lblIconStatus.skin = "sknfontIconInactive";
            this.view.forceLayout();
        } else {
            this.view.tableView.segServicesAndFaq.data[a].lblServiceStatus.text = kony.i18n.getLocalizedString("i18n.secureimage.Active");
            this.view.tableView.segServicesAndFaq.data[a].lblServiceStatus.skin = "sknlblLato5bc06cBold14px";
            this.view.tableView.segServicesAndFaq.data[a].lblIconStatus.skin = "sknFontIconActivate";
            this.view.forceLayout();
        }
        this.view.flxSelectOptions.isVisible = false;
        this.view.flxDeactivateOutageMessage.isVisible = false;
        this.view.tableView.segServicesAndFaq.setData(data);
        this.view.forceLayout();
    },
    showDeactivate: function() {
        this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.OutageMessage.deactivate");
        this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.deactivate_Outage_Message_Popup");
        this.view.popUpDeactivate.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.NO__LEAVE_AS_IT_IS");
        this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesDeactivate");
        this.view.popUpDeactivate.btnPopUpCancel.right = 180 + "px";
        this.view.flxDeactivateOutageMessage.setVisibility(true);
        this.view.forceLayout();
    },
    showActivate: function() {
        this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Activate_Outage_Message");
        this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.activate_Outage_Message_Popup");
        this.view.popUpDeactivate.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.NO__LEAVE_AS_IT_IS");
        this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.frmPermissionsController.YES__ACTIVATE");
        this.view.popUpDeactivate.btnPopUpCancel.right = 155 + "px";
        this.view.flxDeactivateOutageMessage.setVisibility(true);
        this.view.forceLayout();
    },
    showDeleteFaq: function() {
        this.view.popUp.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Delete_FAQ");
        this.view.popUp.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.delete_Outage_message_Popup");
        this.view.popUp.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.NO__LEAVE_AS_IT_IS");
        this.view.popUp.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesDelete");
        this.view.popUp.btnPopUpCancel.right = 155 + "px";
        this.view.toastMessage.lbltoastMessage.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Deleted_Outage_Message_successfully");
        this.view.flxDeleteOutageMessage.setVisibility(true);
        this.view.forceLayout();
    },
    setPreshowData: function() {
        this.view.flxAddOutageMessage.setVisibility(false);
        this.view.flxMainHeader.setVisibility(true);
        this.view.breadcrumbs.setVisibility(false);
        this.view.flxDeleteOutageMessage.setVisibility(false);
        this.view.flxDeactivateOutageMessage.setVisibility(false);
        this.view.tableView.flxNoRecordsFound.setVisibility(false);
        this.view.flxNoOutageMsgWrapper.setVisibility(false);
        this.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
        this.view.noStaticData.lblNoStaticContentCreated.text = kony.i18n.getLocalizedString("i18n.Outage.NoOutageService");
        this.view.noStaticData.lblNoStaticContentMsg.text = kony.i18n.getLocalizedString("i18n.Outage.AddOutageServiceMsg");
        this.view.noStaticData.btnAddStaticContent.text = kony.i18n.getLocalizedString("i18n.Outage.AddOutageService");
        this.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
        this.view.mainHeader.btnDropdownList.isVisible = false;
    },
    setScrollHeight: function() {
        var screenHeight = kony.os.deviceInfo().screenHeight;
        var scrollHeight;
        scrollHeight = screenHeight - 106;
        this.view.flxAddOutageMessage.height = scrollHeight - 20 - 35 + "px";
        this.view.FlexUserTable.height = scrollHeight - 100 + "px";
        this.view.flxMainContent.height = scrollHeight - 30 + "px";
        this.view.tableView.flxNoRecordsFound.height = "400px";
    },
    prepareCreateScreen: function() {
        this.view.flxNoOutageMsgWrapper.setVisibility(false);
        this.view.flxMainContent.setVisibility(false);
        this.view.breadcrumbs.setVisibility(false);
        this.view.lbxOutageMsg.selectedKey = "placeholder";
        this.view.addStaticData.rtxAddStaticContent.text = "";
        this.view.mainHeader.flxHeaderSeperator.setVisibility(false);
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Add_Service_Outage_Messages");
        this.view.commonButtons.btnNext.text = kony.i18n.getLocalizedString("i18n.CommonButtons.Save_AddNew");
        this.view.commonButtons.btnSave.text = kony.i18n.getLocalizedString("i18n.permission.SAVE");
        this.view.addStaticData.rtxAddStaticContent.placeholder = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Add_Outage_Message");
        this.view.addStaticData.flxNoDescriptionError.setVisibility(false);
        this.view.flxNoOutageMsgError.setVisibility(false);
        this.view.addStaticData.SwitchToggleStatus.selectedIndex = 0;
        this.view.addStaticData.flxDescription.setVisibility(true);
        this.view.addStaticData.lblDescription.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Message");
        this.view.addStaticData.lblMessageSize.setVisibility(false);
        this.view.addStaticData.lblMessageSize.text = "0/200";
        this.view.lbxOutageMsg.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
        this.view.addStaticData.rtxAddStaticContent.skin = "skntxtAreaLato0i538905e260549Stat";
        this.view.flxAddOutageMessage.setVisibility(true);
        this.view.mainHeader.btnAddNewOption.setVisibility(false);
    },
    saveScreenY: function(widget, context) {
        this.mouseYCoordinate = context.screenY;
    },
    prepareEditScreen: function() {
        this.view.flxNoOutageMsgWrapper.setVisibility(false);
        this.view.flxMainContent.setVisibility(false);
        this.view.breadcrumbs.setVisibility(false);
        this.view.mainHeader.flxHeaderSeperator.setVisibility(false);
        this.view.addStaticData.flxDescription.setVisibility(true);
        this.view.addStaticData.lblDescription.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Message");
        this.view.addStaticData.lblMessageSize.setVisibility(true);
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Edit_Service_Outage_Messages");
        this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.SERVICE_OUTAGE_MESSAGES");
        this.view.commonButtons.btnNext.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.UPDATE_ADD_NEW");
        this.view.commonButtons.btnSave.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.UPDATE");
        this.view.breadcrumbs.setVisibility(true);
        this.view.flxNoOutageMsgError.setVisibility(false);
        this.view.lbxOutageMsg.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
        this.view.lbxOutageMsg.setEnabled(false); // en Edit no es Editable
        this.view.flxAddOutageMessage.setVisibility(true);
        this.view.mainHeader.btnAddNewOption.setVisibility(false);
        var index = this.view.tableView.segServicesAndFaq.selectedIndex;
        if (index !== null) {
            this.showPrefillDataForEdit();
        }
    },
    showEditOutageMessage: function(opt) {
        var methodName = opt === this.CREATE_SCREEN ? kony.i18n.getLocalizedString("i18n.frmOutageMessageController.prepareCreateScreen") : "prepareEditScreen";
        this[methodName]();
        this.view.forceLayout();
    },
    showPrefillDataForEdit: function() {
        var index = this.view.tableView.segServicesAndFaq.selectedIndex;
        var rowIndex = index[1];
        var data = this.view.tableView.segServicesAndFaq.data;
        var selectedService = data[rowIndex].lblOutageServiceName;
        var serviceFound = this.view.lbxOutageMsg.masterData.find(function(service) {
            return service[1] === selectedService;
        });
        var keySel, valSel;
        if (serviceFound) {
            keySel = serviceFound[0];
            valSel = serviceFound[1];
        }
        this.view.breadcrumbs.btnPreviousPage.text = valSel;
        var status = data[rowIndex].lblServiceStatus.text;
        this.view.lbxOutageMsg.selectedKey = keySel;
        this.view.breadcrumbs.lblCurrentScreen.text = this.view.lbxOutageMsg.selectedKeyValue[1].toUpperCase();
        this.view.addStaticData.rtxAddStaticContent.text = data[rowIndex].lblDescription.text;
        if (this.view.addStaticData.rtxAddStaticContent.text.trim()) {
            var messageLength = this.view.addStaticData.rtxAddStaticContent.text.trim().length;
            this.view.addStaticData.lblMessageSize.text = messageLength + "/200";
        }
        if (status.toLowerCase() === kony.i18n.getLocalizedString("i18n.frmOutageMessageController.active")) {
            this.view.addStaticData.SwitchToggleStatus.selectedIndex = 0;
        } else {
            this.view.addStaticData.SwitchToggleStatus.selectedIndex = 1;
        }
        this.view.addStaticData.SwitchToggleStatus.selectedIndex = 0; // SIEMPRE ACTIVO al EDITAR
    },
    showOutageMessageList: function(res, context) {
        this.view.flxSelectOptions.isVisible = false;
        if (context === undefined || context !== "createOutageMessage") {
            this.view.flxAddOutageMessage.setVisibility(false);
        }
        this.view.breadcrumbs.setVisibility(false);
        this.view.flxMainContent.setVisibility(true);
        this.view.flxSelectOptions.setVisibility(false);
        this.view.mainHeader.flxHeaderSeperator.setVisibility(true);
        this.view.flxSelectOptionsHeader.setVisibility(false);
        this.view.tableView.flxNoRecordsFound.setVisibility(false);
        this.view.tableView.flxTableView.setVisibility(true);
        this.view.FlexUserTable.skin = "Copysknflxffffffop0e23e7fddf7bd4b";
        this.view.mainHeader.btnAddNewOption.text = kony.i18n.getLocalizedString("i18n.Outage.AddOutageService");
        this.view.mainHeader.btnAddNewOption.isVisible = true;
        if (res !== null && res !== undefined) {
            this.SetOutageMessageSegmentData(res);
        }
        this.view.flxDeleteOutageMessage.setVisibility(false);
    },
    SetOutageMessageSegmentData: function(resData) {
        var self = this;
        var data = [];
        this.view.tableView.lblHeaderServiceName.text = kony.i18n.getLocalizedString("i18n.Outage.ServiceName");
        this.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
        this.view.tableView.lblHeaderDescription.text = kony.i18n.getLocalizedString("i18n.frmServiceManagement.lblDetailsServiceOutageMsgHeader");
        this.view.tableView.lblHeaderServiceStatus.text = kony.i18n.getLocalizedString("i18n.roles.STATUS");
        var dataMap = {
            outageId: "outageId",
            serviceId: "serviceId",
            flxOptions: "flxOptions",
            flxOutage: "flxOutage",
            flxOutageCheckbox: "flxOutageCheckbox",
            flxOutageLeft: "flxOutageLeft",
            flxStatus: "flxStatus",
            lblOptionsIcon: "lblOptionsIcon",
            imgCheckBox: "imgCheckBox",
            lblIconStatus: "lblIconStatus",
            lblDescription: "lblDescription",
            lblOutageServiceName: "lblOutageServiceName",
            lblSeparator: "lblSeparator",
            lblServiceStatus: "lblServiceStatus"
        };
        if (resData) {
            data = resData.map(this.toOutageSegment.bind(self));
        }
        this.view.tableView.segServicesAndFaq.widgetDataMap = dataMap;
        this.masterData = data;
        this.view.tableView.segServicesAndFaq.setData(data);
        this.view.forceLayout();
        this.view.tableView.segServicesAndFaq.height = this.view.FlexUserTable.frame.height - 63 + "px";
    },
    toOutageSegment: function(outageMessage) {
        var self = this;
        var statusText = "",
            statusSkin = "",
            statusImgSkin = "";
        if (outageMessage.Status_id === "SID_ACTIVE") {
            statusText = kony.i18n.getLocalizedString("i18n.secureimage.Active");
            statusSkin = "sknlblLato5bc06cBold14px";
            statusImgSkin = "sknFontIconActivate";
        } else if (outageMessage.Status_id === "SID_INACTIVE") {
            statusText = kony.i18n.getLocalizedString("i18n.secureimage.Inactive");
            statusSkin = "sknlblLatocacacaBold12px";
            statusImgSkin = "sknfontIconInactive";
        } else {
            statusText = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Susupended");
            statusSkin = "sknlblLatocacacaBold12px";
            statusImgSkin = "sknfontIconInactive";
        }
        return {
            outageId: outageMessage.id,
            serviceId: outageMessage.Service_id,
            statusId: outageMessage.Status_id,
            lblOptionsIcon: {
                "text": "\ue91f",
                "skin": "sknFontIconOptionMenu"
            },
            imgCheckBox: {
                src: "checkbox.png"
            },
            lblIconStatus: {
                "skin": statusImgSkin
            },
            lblDescription: {
                text: outageMessage.MessageText
            },
            flxOutageCheckbox: {
                "onClick": this.toggleCheckbox2,
            },
            flxOptions: {
                "onClick": this.toggleContextualMenu,
            },
            lblOutageServiceName: outageMessage.Name,
            lblSeparator: ".",
            lblServiceStatus: {
                text: statusText,
                skin: statusSkin
            },
            template: "flxOutage"
        };
    },
    selectAllRows: function() {
        //outage
        this.view.flxSelectOptions.setVisibility(false);
        var data = this.view.tableView.segServicesAndFaq.data;
        var index = this.view.tableView.segServicesAndFaq.selectedIndices;
        if (index === null) {
            this.view.tableView.segServicesAndFaq.selectedIndices = null;
        }
        if (this.view.tableView.imgHeaderCheckBox.src === "checkbox.png") {
            this.view.flxSepartor2.setVisibility(true);
            this.view.flxSelectOptionsHeader.setVisibility(true);
            this.view.tableView.imgHeaderCheckBox.src = "checkboxselected.png";
            var limit = data.length;
            var indices = [
                [0, []]
            ];
            for (var i = 0; i < limit; i++) {
                indices[0][1].push(i);
            }
            this.view.tableView.segServicesAndFaq.selectedRowIndices = indices;
            var rowIndex = this.view.tableView.segServicesAndFaq.selectedRowIndices[0][1];
            var active = false;
            var inactive = false;
            for (var j = 0; j < rowIndex.length; j++) {
                if (data[rowIndex[j]].lblServiceStatus.text === kony.i18n.getLocalizedString("i18n.secureimage.Active")) active = true;
                else inactive = true;
            }
            if (active && inactive) this.view.flxSelectOptionsHeader.width = "107px";
            else this.view.flxSelectOptionsHeader.width = "71px";
            this.view.flxSelectOptionsHeader.flxDeactivateOption.setVisibility(active);
            this.view.flxSelectOptionsHeader.flxActiveOption.setVisibility(inactive);
        } else {
            this.view.tableView.segServicesAndFaq.selectedIndices = null;
            this.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
            this.view.flxSelectOptionsHeader.setVisibility(false);
        }
        this.view.mainHeader.btnDropdownList.isVisible = false;
        this.view.forceLayout();
    },
    /*
     * function to set data to services listbox
     */
    setServiceLstBoxData: function(listData) {
        this.view.lbxOutageMsg.masterData = listData.reduce(function(acc, elem) {
            return acc.concat([
                [elem.id, elem.Name]
            ]);
        }, [
            ["placeholder", kony.i18n.getLocalizedString("i18n.frmOutageMessageController.Select_a_Service")]
        ]);
        this.view.lbxOutageMsg.selectedKey = "placeholder";
    },
    /*
     * function to update status of message from contextmenu actions
     */
    changeStatus: function() {
        var self = this;
        var updateStatusTo;
        var rowIndex = this.view.tableView.segServicesAndFaq.selectedRowIndex[1];
        var segData = this.view.tableView.segServicesAndFaq.data;
        if (segData[rowIndex].statusId === "SID_ACTIVE") updateStatusTo = "SID_INACTIVE";
        else updateStatusTo = "SID_ACTIVE";
        var reqParam = [{
            id: segData[rowIndex].outageId,
            Status_id: updateStatusTo,
            Service_id: segData[rowIndex].serviceId,
            MessageText: "",
            modifiedby: kony.i18n.getLocalizedString("i18n.frmOutageMessageController.kony")
        }];
        kony.adminConsole.utils.showProgressBar(self.view);
        self.view.search.tbxSearchBox.text = "";
        self.view.search.flxSearchCancel.setVisibility(false);
        self.view.flxSelectOptionsHeader.setVisibility(false);
        self.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
        self.presenter.updateStatusOutageMessage(reqParam);
    },
    /*
     * function perform save by calling presentation controller
     */
    onSaveClicked: function(isEdit, currPage) {
        var scopeObj = this;
        var statusId = "";
        var service = this.view.lbxOutageMsg.selectedKeyValue;
        var togIndex = this.view.addStaticData.SwitchToggleStatus.selectedIndex;
        if (togIndex === 0) {
            statusId = "SID_ACTIVE";
        } else {
            statusId = "SID_INACTIVE";
        }
        if (!isEdit) {
            var newData1 = {
                ServiceName: service[1],
                Service_id: service[0],
                MessageText: this.view.addStaticData.rtxAddStaticContent.text.trim(),
                Status_id: statusId
            };
            kony.adminConsole.utils.showProgressBar(scopeObj.view);
            scopeObj.presenter.createOutageMessage(newData1, currPage);
        } else {
            var index = this.view.tableView.segServicesAndFaq.selectedIndex;
            var newData = [];
            if (index !== null) {
                var rowIndex = index[1];
                var segData1 = this.view.tableView.segServicesAndFaq.data;
                var outageId = segData1[rowIndex].outageId;
                newData = [{
                    Service_id: service[0],
                    MessageText: this.view.addStaticData.rtxAddStaticContent.text.trim(),
                    Status_id: statusId,
                    id: outageId,
                    modifiedby: kony.mvc.MDAApplication.getSharedInstance().appContext.userID
                }];
            } else {
                var segData = this.view.tableView.segServicesAndFaq.data;
                var getId = segData.find(function(record) {
                    return record.serviceId === service[0];
                });
                newData = [{
                    Service_id: service[0],
                    MessageText: this.view.addStaticData.rtxAddStaticContent.text.trim(),
                    Status_id: statusId,
                    id: getId.outageId,
                    modifiedby: kony.mvc.MDAApplication.getSharedInstance().appContext.userID
                }];
            }
            kony.adminConsole.utils.showProgressBar(scopeObj.view);
            scopeObj.presenter.editOutageMessage(newData, currPage);
        }
    },
    checkMessageAvailable: function(selectedId) {
        var self = this;
        var dataToShow;
        var outageMessageExists = false;
        var data = self.view.tableView.segServicesAndFaq.data;
        if (data !== null) {
            var serviceIds = data.map(function(value) {
                return value.serviceId;
            });
            outageMessageExists = serviceIds.indexOf(selectedId) >= 0;
            dataToShow = data.find(function(val) {
                return val.serviceId === selectedId;
            });
        }
        if (outageMessageExists) {
            self.showEditOutageMessage(self.EDIT_SCREEN);
            this.view.lbxOutageMsg.skin = "redListBxSkin";
            this.view.flxNoOutageMsgError.setVisibility(true);
            this.view.lblNoOutageMsgError.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.service_Has_OutageMessage");
            self.view.addStaticData.rtxAddStaticContent.text = dataToShow.lblDescription.text;
        } else {
            self.showEditOutageMessage(self.CREATE_SCREEN);
            this.view.lbxOutageMsg.selectedKey = selectedId;
        }
    },
    /*
     * function called on click of delete button in popup
     * call presentation controllers delete
     */
    deleteMessage: function() {
        var self = this;
        var rowIndex;
        var index = this.view.tableView.segServicesAndFaq.selectedRowIndex;
        if (index !== null) {
            rowIndex = index[1];
        }
        var segData = this.view.tableView.segServicesAndFaq.data;
        var outageId = segData[rowIndex].outageId;
        var param = [{
            id: outageId
        }];
        kony.adminConsole.utils.showProgressBar(self.view);
        self.view.search.tbxSearchBox.text = "";
        self.view.search.flxSearchCancel.setVisibility(false);
        self.view.flxSelectOptionsHeader.setVisibility(false);
        self.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
        self.presenter.deleteOutageMessage(param);
    },
    hideDescriptionError: function() {
        this.view.addStaticData.flxNoDescriptionError.setVisibility(false);
        this.view.addStaticData.rtxAddStaticContent.skin = "skntxtAreaLato0i538905e260549Stat";
    },
    showErrorMessage: function() {
        if (this.view.lbxOutageMsg.selectedKey === "placeholder") {
            this.view.lbxOutageMsg.skin = "redListBxSkin";
            this.view.flxNoOutageMsgError.setVisibility(true);
        } else this.view.lbxOutageMsg.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
        if (this.view.addStaticData.rtxAddStaticContent.text === "") {
            this.view.addStaticData.rtxAddStaticContent.skin = "skinredbg";
            this.view.addStaticData.rtxAddStaticContent.height = "170px";
            this.view.addStaticData.flxNoDescriptionError.setVisibility(true);
        } else this.view.addStaticData.rtxAddStaticContent.skin = "skntxtAreaLato0i538905e260549Stat";
    },
    /*
     * function to call presentation controller to fetch segment data
     */
    fetchOutageMessageData: function() {
        kony.adminConsole.utils.showProgressBar(this.view);
        this.presenter.fetchOutageMessage();
    },
    /*
     * function called to delete all the rows of segment
     */
    allDelete: function() {
        var param;
        var segData = this.view.tableView.segServicesAndFaq.data;
        if (this.view.tableView.imgHeaderCheckBox.src === "checkboxselected.png") {
            segData = this.view.tableView.segServicesAndFaq.data;
            param = segData.map(function(value) {
                return {
                    id: value.outageId
                };
            });
            kony.adminConsole.utils.showProgressBar(this.view);
            this.presenter.deleteOutageMessage(param);
        } else {
            var indices = this.view.tableView.segServicesAndFaq.selectedIndices; //indices[0][1]-array
            var ind = indices[0][1];
            var params = ind.reduce(function(acc, elem) {
                return acc.concat({
                    id: segData[elem].outageId
                });
            }, []);
            kony.adminConsole.utils.showProgressBar(this.view);
            this.view.search.tbxSearchBox.text = "";
            this.view.search.flxSearchCancel.setVisibility(false);
            this.view.flxSelectOptionsHeader.setVisibility(false);
            this.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
            this.presenter.deleteOutageMessage(params);
        }
    },
    showNoResultsFound: function() {
        this.view.tableView.flxTableView.setVisibility(false);
        this.view.tableView.flxNoRecordsFound.setVisibility(true);
        this.view.tableView.rtxNoRecords.setVisibility(true);
        this.view.tableView.rtxNoRecords.text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found") + '"' + this.view.search.tbxSearchBox.text + '"' + kony.i18n.getLocalizedString("i18n.frmPoliciesController.Try_with_another_keyword");
    },
    hideNoResultsFound: function() {
        this.view.tableView.flxTableView.setVisibility(true);
        this.view.tableView.flxNoRecordsFound.setVisibility(false);
        this.view.tableView.rtxNoRecords.setVisibility(false);
    },
    /*
     * function to get search box text and call search
     */
    searchOutageMessages: function() {
        var searchTxt = this.view.search.tbxSearchBox.text;
        this.searchResults(searchTxt);
    },
    sortData: function(Permission1, Permission2) {
        var s1 = Permission1.statusId;
        var s2 = Permission2.statusId;
        var comparisonResult = this.sortAlphaNum(s1, s2);
        if (this.inAscendingOrder) {
            return comparisonResult;
        } else {
            return comparisonResult === 1 ? -1 : 1;
        }
    },
    sortAlphaNum: function(a, b) {
        var reA = /[^a-zA-Z]/g;
        var reN = /[^0-9]/g;
        var aA = a.replace(reA, "");
        var bA = b.replace(reA, "");
        if (aA === bA) {
            var aN = parseInt(a.replace(reN, ""), 10);
            var bN = parseInt(b.replace(reN, ""), 10);
            return aN === bN ? 0 : aN > bN ? 1 : -1;
        } else {
            return aA > bA ? 1 : -1;
        }
    },
    sortByStatus: function() {
        var scopeObj = this;
        var segData = this.view.tableView.segServicesAndFaq.data;
        scopeObj.view.tableView.segServicesAndFaq.setData(segData.sort(this.sortData));
        if (this.inAscendingOrder) {
            this.view.tableView.lblFontIconSortStatus.text = kony.adminConsole.utils.fonticons.ASCENDING_IMAGE;
            this.view.tableView.lblFontIconSortStatus.skin = kony.adminConsole.utils.fonticons.ASCENDING_SKIN;
        } else {
            this.view.tableView.lblFontIconSortStatus.text = kony.adminConsole.utils.fonticons.DESCENDING_IMAGE;
            this.view.tableView.lblFontIconSortStatus.skin = kony.adminConsole.utils.fonticons.DESCENDING_SKIN;
        }
        scopeObj.view.forceLayout();
    },
    resetSortImages: function() {
        var self = this;
        self.determineSortFontIcon(self.sortBy, 'Status_id', self.view.tableView.lblFontIconSortStatus);
        self.determineSortFontIcon(self.sortBy, 'Name', self.view.tableView.lblFontIconSortName);
    },
    sortIconFor: function(column) {
        var self = this;
        return self.determineSortFontIcon(self.sortBy, column);
    },
    /*
     * function to populate segment based on search box text
     *@param: text to be searched
     */
    searchResults: function(searchKey) {
        var searchRes;
        var scopeObj = this;
        var requiredData = function(outageMsg) {
            return outageMsg.lblDescription.text.toLowerCase().indexOf(searchKey.toLowerCase()) > -1 || outageMsg.lblOutageServiceName.toLowerCase().indexOf(searchKey.toLowerCase()) > -1;
        };
        searchRes = this.masterData.filter(requiredData);
        this.records = searchRes.length;
        if (this.records === 0) {
            scopeObj.showNoResultsFound();
        } else {
            scopeObj.hideNoResultsFound();
            scopeObj.view.tableView.segServicesAndFaq.setData(searchRes);
        }
        scopeObj.view.forceLayout();
    },
    /*
     * function to update status for all records
     */
    allStatusChange: function(status) {
        var param, updateStatusTo;
        if (status === kony.i18n.getLocalizedString("i18n.frmOutageMessageController.active")) {
            updateStatusTo = "SID_ACTIVE";
        } else {
            updateStatusTo = "SID_INACTIVE";
        }
        var segData = this.view.tableView.segServicesAndFaq.data;
        if (this.view.tableView.imgHeaderCheckBox.src === "checkboxselected.png") {
            segData = this.view.tableView.segServicesAndFaq.data;
            param = segData.map(function(value) {
                return {
                    id: value.outageId,
                    Status_id: updateStatusTo,
                    Service_id: value.serviceId,
                    MessageText: "",
                    modifiedby: kony.mvc.MDAApplication.getSharedInstance().appContext.userID
                };
            });
            kony.adminConsole.utils.showProgressBar(this.view);
            this.presenter.updateStatusOutageMessage(param);
        } else {
            var indices = this.view.tableView.segServicesAndFaq.selectedIndices; //indices[0][1]-array
            var ind = indices[0][1];
            var params = ind.reduce(function(acc, elem) {
                return acc.concat({
                    id: segData[elem].outageId,
                    Status_id: updateStatusTo,
                    Service_id: segData[elem].serviceId,
                    MessageText: "",
                    modifiedby: kony.mvc.MDAApplication.getSharedInstance().appContext.userID
                });
            }, []);
            kony.adminConsole.utils.showProgressBar(this.view);
            this.view.search.tbxSearchBox.text = "";
            this.view.search.flxSearchCancel.setVisibility(false);
            this.view.flxSelectOptionsHeader.setVisibility(false);
            this.view.tableView.imgHeaderCheckBox.src = "checkbox.png";
            this.presenter.updateStatusOutageMessage(params);
        }
    },
    //Temporary fix for onclick actions on the segment (8.3 issue)
    toggleContextualMenu: function() {
        kony.application.getCurrentForm().flxSelectOptionsHeader.setVisibility(false);
        kony.application.getCurrentForm().tableView.imgHeaderCheckBox.src = "checkbox.png";
        var index = kony.application.getCurrentForm().tableView.segServicesAndFaq.selectedRowIndex;
        var sectionIndex = index[0];
        var rowIndex = index[1];
        kony.application.getCurrentForm().tableView.segServicesAndFaq.selectedRowIndices = [
            [0, [rowIndex]]
        ];
        var mainSegmentRowIndex;
        kony.store.setItem(mainSegmentRowIndex, rowIndex);
        var templateHeight;
        var data = kony.application.getCurrentForm().tableView.segServicesAndFaq.data;
        if (data[0].template === "flxOutage") templateHeight = 65;
        else templateHeight = 50;
        var height = 45 + ((rowIndex + 1) * templateHeight);
        if (data[rowIndex].lblServiceStatus.text === "Active") {
            kony.application.getCurrentForm().flxSelectOptions.flxDeactivate.lblOption2.text = "Deactivate";
            kony.application.getCurrentForm().flxSelectOptions.flxDeactivate.lblIconOption1.text = kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonDeactive");
            if (data[0].template === "flxOutage") {
                kony.application.getCurrentForm().flxSelectOptions.flxDelete.setVisibility(false);
            }
        } else {
            kony.application.getCurrentForm().flxSelectOptions.flxDeactivate.lblOption2.text = "Activate";
            kony.application.getCurrentForm().flxSelectOptions.flxDeactivate.lblIconOption1.text = kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonActiveSearch");
            if (data[0].template === "flxOutage") {
                kony.application.getCurrentForm().flxSelectOptions.flxDelete.setVisibility(true);
            }
        }
        //kony.application.getCurrentForm().flxSelectOptions.top = height+"px";
        if (kony.application.getCurrentForm().flxSelectOptions.isVisible === false) {
            kony.application.getCurrentForm().flxSelectOptions.setVisibility(true);
        } else {
            kony.application.getCurrentForm().flxSelectOptions.setVisibility(false);
            kony.application.getCurrentForm().tableView.segServicesAndFaq.selectedIndices = null;
            kony.application.getCurrentForm().forceLayout();
        }
        this.view.forceLayout();
        this.fixContextualMenu(this.mouseYCoordinate - 168);
        kony.print("toggleVisibility TableView called");
    },
    fixContextualMenu: function(heightVal) {
        if (((this.view.flxSelectOptions.frame.height + heightVal) > (this.view.tableView.segServicesAndFaq.frame.height + 50)) && this.view.flxSelectOptions.frame.height < this.view.tableView.segServicesAndFaq.frame.height) {
            this.view.flxSelectOptions.top = ((heightVal - this.view.flxSelectOptions.frame.height) - 50) + "px";
        } else {
            this.view.flxSelectOptions.top = (heightVal) + "px";
        }
        this.view.forceLayout();
    },
    toggleCheckbox: function() {
        var ScopeObj = this;
        var index = kony.application.getCurrentForm().tableView.segServicesAndFaq.selectedIndex;
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().tableView.segServicesAndFaq.data;
        if (data[rowIndex].imgCheckBox.src === "checkbox.png") {
            if (data[rowIndex].lblServiceStatus.text === "Active") ScopeObj.showDeactivateAndDelete();
            else ScopeObj.showActivateAndDelete();
            data[rowIndex].imgCheckBox.src = "checkboxselected.png";
        } else {
            data[rowIndex].imgCheckBox.src = "checkbox.png";
            kony.application.getCurrentForm().flxSelectOptionsHeader.setVisibility(false);
        }
        kony.application.getCurrentForm().tableView.segServicesAndFaq.setDataAt(data[rowIndex], rowIndex);
    },
    toggleCheckbox2: function() {
        var ScopeObj = this;
        var length = 0;
        var active = false;
        var inactive = false;
        var index = kony.application.getCurrentForm().tableView.segServicesAndFaq.selectedIndices;
        if (index !== null) {
            length = index[0][1].length;
            var rowIndex = index[0][1];
        }
        var data = kony.application.getCurrentForm().tableView.segServicesAndFaq.data;
        if (length === 1) {
            if (data[rowIndex].lblServiceStatus.text === "Active") ScopeObj.showDeactivateAndDelete();
            else ScopeObj.showActivateAndDelete();
            data[rowIndex].imgCheckBox.src = "checkboxselected.png";
        } else if (length > 1) {
            for (var i = 0; i < length; i++) {
                if (data[index[0][1][i]].lblServiceStatus.text === "Active") {
                    active = true;
                }
                if (data[index[0][1][i]].lblServiceStatus.text === "Inactive") {
                    inactive = true;
                }
            }
            if (active && inactive) {
                ScopeObj.showAllOptions();
            } else if (active === true && inactive === false) {
                ScopeObj.showDeactivateAndDelete();
            } else if (active === false && inactive === true) {
                ScopeObj.showActivateAndDelete();
            }
        } else {
            kony.application.getCurrentForm().flxSelectOptionsHeader.setVisibility(false);
            kony.application.getCurrentForm().tableView.imgHeaderCheckBox.src = "checkbox.png";
        }
        if (index !== null && data.length === index[0][1].length) kony.application.getCurrentForm().tableView.imgHeaderCheckBox.src = "checkboxselected.png";
        else kony.application.getCurrentForm().tableView.imgHeaderCheckBox.src = "checkbox.png";
    },
    showAllOptions: function() {
        kony.application.getCurrentForm().flxSelectOptionsHeader.width = "107px";
        kony.application.getCurrentForm().flxSelectOptionsHeader.flxDeactivateOption.setVisibility(true);
        kony.application.getCurrentForm().flxSelectOptionsHeader.flxActiveOption.setVisibility(true);
        kony.application.getCurrentForm().flxSepartor2.setVisibility(true);
        kony.application.getCurrentForm().flxSelectOptionsHeader.setVisibility(true);
        kony.application.getCurrentForm().forceLayout();
    },
    showDeactivateAndDelete: function() {
        kony.application.getCurrentForm().flxSelectOptionsHeader.width = "71px";
        kony.application.getCurrentForm().flxSelectOptionsHeader.flxDeactivateOption.setVisibility(true);
        kony.application.getCurrentForm().flxSelectOptionsHeader.flxActiveOption.setVisibility(false);
        kony.application.getCurrentForm().flxSepartor2.setVisibility(false);
        kony.application.getCurrentForm().flxSelectOptionsHeader.setVisibility(true);
        kony.application.getCurrentForm().forceLayout();
    },
    showActivateAndDelete: function() {
        kony.application.getCurrentForm().flxSelectOptionsHeader.width = "71px";
        kony.application.getCurrentForm().flxSelectOptionsHeader.flxActiveOption.setVisibility(true);
        kony.application.getCurrentForm().flxSelectOptionsHeader.flxDeactivateOption.setVisibility(false);
        kony.application.getCurrentForm().flxSepartor2.setVisibility(false);
        kony.application.getCurrentForm().flxSelectOptionsHeader.setVisibility(true);
        kony.application.getCurrentForm().forceLayout();
    }
});
define("OutageMessageModule/frmOutageMessageControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmOutageMessage **/
    AS_Form_f99c5cffa0864cb8a65d17ad1e600555: function AS_Form_f99c5cffa0864cb8a65d17ad1e600555(eventobject) {
        var self = this;
        this.preShowActions();
    },
    /** onDeviceBack defined for frmOutageMessage **/
    AS_Form_fcb2e4c3e55a4a96bf1354b6caf7e017: function AS_Form_fcb2e4c3e55a4a96bf1354b6caf7e017(eventobject) {
        var self = this;
        //registered
        this.onBrowserBack();
    }
});
define("OutageMessageModule/frmOutageMessageController", ["OutageMessageModule/userfrmOutageMessageController", "OutageMessageModule/frmOutageMessageControllerActions"], function() {
    var controller = require("OutageMessageModule/userfrmOutageMessageController");
    var controllerActions = ["OutageMessageModule/frmOutageMessageControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
