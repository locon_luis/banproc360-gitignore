define({
    AS_AppEvents_d9f275df03bb4f2c9c87f0aca8dac598: function AS_AppEvents_d9f275df03bb4f2c9c87f0aca8dac598(eventobject) {
        var self = this;
        var qpExists = window.location.href.indexOf("qp=");
        if (qpExists !== -1) {
            return "frmErrorLogin";
        } else {
            return "frmLogin";
        }
    },
    AS_AppEvents_hf8e5e479c1348dc9f9ed8e64ecde98e: function AS_AppEvents_hf8e5e479c1348dc9f9ed8e64ecde98e(eventobject) {
        var self = this;
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        var SecurityModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SecurityModule");
        clearCache();
    },
    appInit: function(params) {
        skinsInit();
        kony.mvc.registry.add("com.adminConsole.BusinessConfigurations.configLeftMenu", "configLeftMenu", "configLeftMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.BusinessConfigurations",
            "classname": "configLeftMenu",
            "name": "com.adminConsole.BusinessConfigurations.configLeftMenu"
        });
        kony.mvc.registry.add("com.adminConsole.BusinessConfigurations.criteriaList", "criteriaList", "criteriaListController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.BusinessConfigurations",
            "classname": "criteriaList",
            "name": "com.adminConsole.BusinessConfigurations.criteriaList"
        });
        kony.mvc.registry.add("com.adminConsole.CSR.TemplateMessage", "TemplateMessage", "TemplateMessageController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.CSR",
            "classname": "TemplateMessage",
            "name": "com.adminConsole.CSR.TemplateMessage"
        });
        kony.mvc.registry.add("com.adminConsole.CSR.detailHeader", "detailHeader", "detailHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.CSR",
            "classname": "detailHeader",
            "name": "com.adminConsole.CSR.detailHeader"
        });
        kony.mvc.registry.add("com.adminConsole.CustomerManagement.CustomerSearchandResults", "CustomerSearchandResults", "CustomerSearchandResultsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.CustomerManagement",
            "classname": "CustomerSearchandResults",
            "name": "com.adminConsole.CustomerManagement.CustomerSearchandResults"
        });
        kony.mvc.registry.add("com.adminConsole.Customers.ToolTip", "ToolTip", "ToolTipController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Customers",
            "classname": "ToolTip",
            "name": "com.adminConsole.Customers.ToolTip"
        });
        kony.mvc.registry.add("com.adminConsole.Groups.AdvancedSearchDropDown", "AdvancedSearchDropDown", "AdvancedSearchDropDownController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Groups",
            "classname": "AdvancedSearchDropDown",
            "name": "com.adminConsole.Groups.AdvancedSearchDropDown"
        });
        kony.mvc.registry.add("com.adminConsole.Groups.SelectScheduler", "SelectScheduler", "SelectSchedulerController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Groups",
            "classname": "SelectScheduler",
            "name": "com.adminConsole.Groups.SelectScheduler"
        });
        kony.mvc.registry.add("com.adminConsole.LeadManagement.viewLeads", "viewLeads", "viewLeadsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.LeadManagement",
            "classname": "viewLeads",
            "name": "com.adminConsole.LeadManagement.viewLeads"
        });
        kony.mvc.registry.add("com.adminConsole.LeadManagment.countTabs", "countTabs", "countTabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.LeadManagment",
            "classname": "countTabs",
            "name": "com.adminConsole.LeadManagment.countTabs"
        });
        kony.mvc.registry.add("com.adminConsole.Products.productDetailsView", "productDetailsView", "productDetailsViewController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Products",
            "classname": "productDetailsView",
            "name": "com.adminConsole.Products.productDetailsView"
        });
        kony.mvc.registry.add("com.adminConsole.Products.productTypeFilterMenu", "productTypeFilterMenu", "productTypeFilterMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Products",
            "classname": "productTypeFilterMenu",
            "name": "com.adminConsole.Products.productTypeFilterMenu"
        });
        kony.mvc.registry.add("com.adminConsole.StaticContentManagement.faq.tableView", "tableView", "tableViewController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.StaticContentManagement.faq",
            "classname": "tableView",
            "name": "com.adminConsole.StaticContentManagement.faq.tableView"
        });
        kony.mvc.registry.add("com.adminConsole.StaticContentManagement.tableView", "tableView", "tableViewController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.StaticContentManagement",
            "classname": "tableView",
            "name": "com.adminConsole.StaticContentManagement.tableView"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.channelDefaultScreen", "channelDefaultScreen", "channelDefaultScreenController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "channelDefaultScreen",
            "name": "com.adminConsole.adManagement.channelDefaultScreen"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.channelScreen", "channelScreen", "channelScreenController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "channelScreen",
            "name": "com.adminConsole.adManagement.channelScreen"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.channelTemplate", "channelTemplate", "channelTemplateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "channelTemplate",
            "name": "com.adminConsole.adManagement.channelTemplate"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.moduleTemplate", "moduleTemplate", "moduleTemplateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "moduleTemplate",
            "name": "com.adminConsole.adManagement.moduleTemplate"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.selectOptions", "selectOptions", "selectOptionsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "selectOptions",
            "name": "com.adminConsole.adManagement.selectOptions"
        });
        kony.mvc.registry.add("com.adminConsole.advanceSearch.dateRangePicker", "dateRangePicker", "dateRangePickerController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.advanceSearch",
            "classname": "dateRangePicker",
            "name": "com.adminConsole.advanceSearch.dateRangePicker"
        });
        kony.mvc.registry.add("com.adminConsole.alertMang.alertMessage", "alertMessage", "alertMessageController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.alertMang",
            "classname": "alertMessage",
            "name": "com.adminConsole.alertMang.alertMessage"
        });
        kony.mvc.registry.add("com.adminConsole.alerts.ViewTemplate", "ViewTemplate", "ViewTemplateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.alerts",
            "classname": "ViewTemplate",
            "name": "com.adminConsole.alerts.ViewTemplate"
        });
        kony.mvc.registry.add("com.adminConsole.alerts.customListbox", "customListbox", "customListboxController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.alerts",
            "classname": "customListbox",
            "name": "com.adminConsole.alerts.customListbox"
        });
        kony.mvc.registry.add("com.adminConsole.alerts.supportedChannel", "supportedChannel", "supportedChannelController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.alerts",
            "classname": "supportedChannel",
            "name": "com.adminConsole.alerts.supportedChannel"
        });
        kony.mvc.registry.add("com.adminConsole.common.HeaderGuest", "HeaderGuest", "HeaderGuestController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "HeaderGuest",
            "name": "com.adminConsole.common.HeaderGuest"
        });
        kony.mvc.registry.add("com.adminConsole.common.addAndRemoveOptions", "addAndRemoveOptions", "addAndRemoveOptionsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "addAndRemoveOptions",
            "name": "com.adminConsole.common.addAndRemoveOptions"
        });
        kony.mvc.registry.add("com.adminConsole.common.addAndRemoveOptions1", "addAndRemoveOptions1", "addAndRemoveOptions1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "addAndRemoveOptions1",
            "name": "com.adminConsole.common.addAndRemoveOptions1"
        });
        kony.mvc.registry.add("com.adminConsole.common.addNewRow", "addNewRow", "addNewRowController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "addNewRow",
            "name": "com.adminConsole.common.addNewRow"
        });
        kony.mvc.registry.add("com.adminConsole.common.breadcrumbs", "breadcrumbs", "breadcrumbsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "breadcrumbs",
            "name": "com.adminConsole.common.breadcrumbs"
        });
        kony.mvc.registry.add("com.adminConsole.common.breadcrumbs1", "breadcrumbs1", "breadcrumbs1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "breadcrumbs1",
            "name": "com.adminConsole.common.breadcrumbs1"
        });
        kony.mvc.registry.add("com.adminConsole.common.commonButtons", "commonButtons", "commonButtonsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "commonButtons",
            "name": "com.adminConsole.common.commonButtons"
        });
        kony.mvc.registry.add("com.adminConsole.common.contactNumber", "contactNumber", "contactNumberController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "contactNumber",
            "name": "com.adminConsole.common.contactNumber"
        });
        kony.mvc.registry.add("com.adminConsole.common.contextualMenu", "contextualMenu", "contextualMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "contextualMenu",
            "name": "com.adminConsole.common.contextualMenu"
        });
        kony.mvc.registry.add("com.adminConsole.common.contextualMenu1", "contextualMenu1", "contextualMenu1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "contextualMenu1",
            "name": "com.adminConsole.common.contextualMenu1"
        });
        kony.mvc.registry.add("com.adminConsole.common.customRadioButtonGroup", "customRadioButtonGroup", "customRadioButtonGroupController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "customRadioButtonGroup",
            "name": "com.adminConsole.common.customRadioButtonGroup"
        });
        kony.mvc.registry.add("com.adminConsole.common.customSwitch", "customSwitch", "customSwitchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "customSwitch",
            "name": "com.adminConsole.common.customSwitch"
        });
        kony.mvc.registry.add("com.adminConsole.common.dataEntry", "dataEntry", "dataEntryController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "dataEntry",
            "name": "com.adminConsole.common.dataEntry"
        });
        kony.mvc.registry.add("com.adminConsole.common.dropdownMainHeader", "dropdownMainHeader", "dropdownMainHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "dropdownMainHeader",
            "name": "com.adminConsole.common.dropdownMainHeader"
        });
        kony.mvc.registry.add("com.adminConsole.common.leadContextualMenu", "leadContextualMenu", "leadContextualMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "leadContextualMenu",
            "name": "com.adminConsole.common.leadContextualMenu"
        });
        kony.mvc.registry.add("com.adminConsole.common.pagination1", "pagination1", "pagination1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "pagination1",
            "name": "com.adminConsole.common.pagination1"
        });
        kony.mvc.registry.add("com.adminConsole.common.popUp", "popUp", "popUpController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "popUp",
            "name": "com.adminConsole.common.popUp"
        });
        kony.mvc.registry.add("com.adminConsole.common.popUp1", "popUp1", "popUp1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "popUp1",
            "name": "com.adminConsole.common.popUp1"
        });
        kony.mvc.registry.add("com.adminConsole.common.popUpCancelEdits", "popUpCancelEdits", "popUpCancelEditsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "popUpCancelEdits",
            "name": "com.adminConsole.common.popUpCancelEdits"
        });
        kony.mvc.registry.add("com.adminConsole.common.productHeader", "productHeader", "productHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "productHeader",
            "name": "com.adminConsole.common.productHeader"
        });
        kony.mvc.registry.add("com.adminConsole.common.statusFilterMenu", "statusFilterMenu", "statusFilterMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "statusFilterMenu",
            "name": "com.adminConsole.common.statusFilterMenu"
        });
        kony.mvc.registry.add("com.adminConsole.common.tabs", "tabs", "tabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "tabs",
            "name": "com.adminConsole.common.tabs"
        });
        kony.mvc.registry.add("com.adminConsole.common.textBoxEntry", "textBoxEntry", "textBoxEntryController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "textBoxEntry",
            "name": "com.adminConsole.common.textBoxEntry"
        });
        kony.mvc.registry.add("com.adminConsole.common.textBoxEntryAsst", "textBoxEntryAsst", "textBoxEntryAsstController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "textBoxEntryAsst",
            "name": "com.adminConsole.common.textBoxEntryAsst"
        });
        kony.mvc.registry.add("com.adminConsole.common.timePicker", "timePicker", "timePickerController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "timePicker",
            "name": "com.adminConsole.common.timePicker"
        });
        kony.mvc.registry.add("com.adminConsole.common.toastMessage", "toastMessage", "toastMessageController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "toastMessage",
            "name": "com.adminConsole.common.toastMessage"
        });
        kony.mvc.registry.add("com.adminConsole.common.toastMessageWithLink", "toastMessageWithLink", "toastMessageWithLinkController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "toastMessageWithLink",
            "name": "com.adminConsole.common.toastMessageWithLink"
        });
        kony.mvc.registry.add("com.adminConsole.common.toastMessageWithWarning", "toastMessageWithWarning", "toastMessageWithWarningController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "toastMessageWithWarning",
            "name": "com.adminConsole.common.toastMessageWithWarning"
        });
        kony.mvc.registry.add("com.adminConsole.common.tooltip", "tooltip", "tooltipController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "tooltip",
            "name": "com.adminConsole.common.tooltip"
        });
        kony.mvc.registry.add("com.adminConsole.common.typeHead", "typeHead", "typeHeadController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "typeHead",
            "name": "com.adminConsole.common.typeHead"
        });
        kony.mvc.registry.add("com.adminConsole.common.variableReferencesMenu", "variableReferencesMenu", "variableReferencesMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "variableReferencesMenu",
            "name": "com.adminConsole.common.variableReferencesMenu"
        });
        kony.mvc.registry.add("com.adminConsole.common.verticalTabs", "verticalTabs", "verticalTabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "verticalTabs",
            "name": "com.adminConsole.common.verticalTabs"
        });
        kony.mvc.registry.add("com.adminConsole.common.verticalTabs1", "verticalTabs1", "verticalTabs1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "verticalTabs1",
            "name": "com.adminConsole.common.verticalTabs1"
        });
        kony.mvc.registry.add("com.adminConsole.commonTabsDashboard.dashboardCommonTab", "dashboardCommonTab", "dashboardCommonTabController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.commonTabsDashboard",
            "classname": "dashboardCommonTab",
            "name": "com.adminConsole.commonTabsDashboard.dashboardCommonTab"
        });
        kony.mvc.registry.add("com.adminConsole.configurationBundle.bundleData", "bundleData", "bundleDataController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.configurationBundle",
            "classname": "bundleData",
            "name": "com.adminConsole.configurationBundle.bundleData"
        });
        kony.mvc.registry.add("com.adminConsole.configurationBundle.configurationData", "configurationData", "configurationDataController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.configurationBundle",
            "classname": "configurationData",
            "name": "com.adminConsole.configurationBundle.configurationData"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.Address", "Address", "AddressController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "Address",
            "name": "com.adminConsole.customerMang.Address"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.CSRAssist", "CSRAssist", "CSRAssistController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "CSRAssist",
            "name": "com.adminConsole.customerMang.CSRAssist"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.ContactNum", "ContactNum", "ContactNumController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "ContactNum",
            "name": "com.adminConsole.customerMang.ContactNum"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.EditAddress", "EditAddress", "EditAddressController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "EditAddress",
            "name": "com.adminConsole.customerMang.EditAddress"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.EditGeneralInfo", "EditGeneralInfo", "EditGeneralInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "EditGeneralInfo",
            "name": "com.adminConsole.customerMang.EditGeneralInfo"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.Llimits.Limits", "Limits", "LimitsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang.Llimits",
            "classname": "Limits",
            "name": "com.adminConsole.customerMang.Llimits.Limits"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.Notes", "Notes", "NotesController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "Notes",
            "name": "com.adminConsole.customerMang.Notes"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.alertMessage", "alertMessage", "alertMessageController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "alertMessage",
            "name": "com.adminConsole.customerMang.alertMessage"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.backToPageHeader", "backToPageHeader", "backToPageHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "backToPageHeader",
            "name": "com.adminConsole.customerMang.backToPageHeader"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.deleteRow.deleteRow", "deleteRow", "deleteRowController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang.deleteRow",
            "classname": "deleteRow",
            "name": "com.adminConsole.customerMang.deleteRow.deleteRow"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.deviceInfo", "deviceInfo", "deviceInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "deviceInfo",
            "name": "com.adminConsole.customerMang.deviceInfo"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.generalInfoHeader", "generalInfoHeader", "generalInfoHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "generalInfoHeader",
            "name": "com.adminConsole.customerMang.generalInfoHeader"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.productDetails", "productDetails", "productDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "productDetails",
            "name": "com.adminConsole.customerMang.productDetails"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.selectOptions", "selectOptions", "selectOptionsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "selectOptions",
            "name": "com.adminConsole.customerMang.selectOptions"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.tabs", "tabs", "tabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "tabs",
            "name": "com.adminConsole.customerMang.tabs"
        });
        kony.mvc.registry.add("com.adminConsole.dashBoard.dashBoardTab", "dashBoardTab", "dashBoardTabController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.dashBoard",
            "classname": "dashBoardTab",
            "name": "com.adminConsole.dashBoard.dashBoardTab"
        });
        kony.mvc.registry.add("com.adminConsole.decisionManagement.DecisionFolder", "DecisionFolder", "DecisionFolderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.decisionManagement",
            "classname": "DecisionFolder",
            "name": "com.adminConsole.decisionManagement.DecisionFolder"
        });
        kony.mvc.registry.add("com.adminConsole.header.mainHeader", "mainHeader", "mainHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.header",
            "classname": "mainHeader",
            "name": "com.adminConsole.header.mainHeader"
        });
        kony.mvc.registry.add("com.adminConsole.header.search", "search", "searchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.header",
            "classname": "search",
            "name": "com.adminConsole.header.search"
        });
        kony.mvc.registry.add("com.adminConsole.header.subHeader", "subHeader", "subHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.header",
            "classname": "subHeader",
            "name": "com.adminConsole.header.subHeader"
        });
        kony.mvc.registry.add("com.adminConsole.history.search", "search", "searchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.history",
            "classname": "search",
            "name": "com.adminConsole.history.search"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.consent", "consent", "consentController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "consent",
            "name": "com.adminConsole.loans.applications.consent"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.errorMsg", "errorMsg", "errorMsgController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "errorMsg",
            "name": "com.adminConsole.loans.applications.errorMsg"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.incomeGroup", "incomeGroup", "incomeGroupController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "incomeGroup",
            "name": "com.adminConsole.loans.applications.incomeGroup"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.leftMenu", "leftMenu", "leftMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "leftMenu",
            "name": "com.adminConsole.loans.applications.leftMenu"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.loanApplicationDetails", "loanApplicationDetails", "loanApplicationDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "loanApplicationDetails",
            "name": "com.adminConsole.loans.applications.loanApplicationDetails"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.loansSectionHeader", "loansSectionHeader", "loansSectionHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "loansSectionHeader",
            "name": "com.adminConsole.loans.applications.loansSectionHeader"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.loantype.loaninfo", "loaninfo", "loaninfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications.loantype",
            "classname": "loaninfo",
            "name": "com.adminConsole.loans.applications.loantype.loaninfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.simulationTemplate.simulationTemplate", "simulationTemplate", "simulationTemplateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications.simulationTemplate",
            "classname": "simulationTemplate",
            "name": "com.adminConsole.loans.applications.simulationTemplate.simulationTemplate"
        });
        kony.mvc.registry.add("com.adminConsole.location.supportedCurrency", "supportedCurrency", "supportedCurrencyController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.location",
            "classname": "supportedCurrency",
            "name": "com.adminConsole.location.supportedCurrency"
        });
        kony.mvc.registry.add("com.adminConsole.logs.LogCustomTabs", "LogCustomTabs", "LogCustomTabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.logs",
            "classname": "LogCustomTabs",
            "name": "com.adminConsole.logs.LogCustomTabs"
        });
        kony.mvc.registry.add("com.adminConsole.logs.LogDefaultTabs", "LogDefaultTabs", "LogDefaultTabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.logs",
            "classname": "LogDefaultTabs",
            "name": "com.adminConsole.logs.LogDefaultTabs"
        });
        kony.mvc.registry.add("com.adminConsole.logs.popUpSaveFilter", "popUpSaveFilter", "popUpSaveFilterController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.logs",
            "classname": "popUpSaveFilter",
            "name": "com.adminConsole.logs.popUpSaveFilter"
        });
        kony.mvc.registry.add("com.adminConsole.logs.searchCustomer", "searchCustomer", "searchCustomerController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.logs",
            "classname": "searchCustomer",
            "name": "com.adminConsole.logs.searchCustomer"
        });
        kony.mvc.registry.add("com.adminConsole.logs.selectAmount", "selectAmount", "selectAmountController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.logs",
            "classname": "selectAmount",
            "name": "com.adminConsole.logs.selectAmount"
        });
        kony.mvc.registry.add("com.adminConsole.navigation.leftMenuNew", "leftMenuNew", "leftMenuNewController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.navigation",
            "classname": "leftMenuNew",
            "name": "com.adminConsole.navigation.leftMenuNew"
        });
        kony.mvc.registry.add("com.adminConsole.onBoarding.popUp", "popUp", "popUpController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.onBoarding",
            "classname": "popUp",
            "name": "com.adminConsole.onBoarding.popUp"
        });
        kony.mvc.registry.add("com.adminConsole.reports.search", "search", "searchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.reports",
            "classname": "search",
            "name": "com.adminConsole.reports.search"
        });
        kony.mvc.registry.add("com.adminConsole.search.advancedSearch", "advancedSearch", "advancedSearchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.search",
            "classname": "advancedSearch",
            "name": "com.adminConsole.search.advancedSearch"
        });
        kony.mvc.registry.add("com.adminConsole.search.modifySearch", "modifySearch", "modifySearchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.search",
            "classname": "modifySearch",
            "name": "com.adminConsole.search.modifySearch"
        });
        kony.mvc.registry.add("com.adminConsole.selectDate.selectDate", "selectDate", "selectDateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.selectDate",
            "classname": "selectDate",
            "name": "com.adminConsole.selectDate.selectDate"
        });
        kony.mvc.registry.add("com.adminConsole.staticContent.addStaticData", "addStaticData", "addStaticDataController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.staticContent",
            "classname": "addStaticData",
            "name": "com.adminConsole.staticContent.addStaticData"
        });
        kony.mvc.registry.add("com.adminConsole.staticContent.noStaticData", "noStaticData", "noStaticDataController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.staticContent",
            "classname": "noStaticData",
            "name": "com.adminConsole.staticContent.noStaticData"
        });
        kony.mvc.registry.add("com.adminConsole.staticContent.staticData", "staticData", "staticDataController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.staticContent",
            "classname": "staticData",
            "name": "com.adminConsole.staticContent.staticData"
        });
        kony.mvc.registry.add("com.adminConsole.templateMessage.Message", "Message", "MessageController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.templateMessage",
            "classname": "Message",
            "name": "com.adminConsole.templateMessage.Message"
        });
        kony.mvc.registry.add("com.adminConsole.view.Description", "Description", "DescriptionController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.view",
            "classname": "Description",
            "name": "com.adminConsole.view.Description"
        });
        kony.mvc.registry.add("com.adminConsole.view.Description1", "Description1", "Description1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.view",
            "classname": "Description1",
            "name": "com.adminConsole.view.Description1"
        });
        kony.mvc.registry.add("com.adminConsole.view.details", "details", "detailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.view",
            "classname": "details",
            "name": "com.adminConsole.view.details"
        });
        kony.mvc.registry.add("com.adminConsole.view.details1", "details1", "details1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.view",
            "classname": "details1",
            "name": "com.adminConsole.view.details1"
        });
        kony.mvc.registry.add("com.adminConsole.view.detailsMultiLine", "detailsMultiLine", "detailsMultiLineController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.view",
            "classname": "detailsMultiLine",
            "name": "com.adminConsole.view.detailsMultiLine"
        });
        kony.mvc.registry.add("com.adminConsole.view.detailsWithLink", "detailsWithLink", "detailsWithLinkController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.view",
            "classname": "detailsWithLink",
            "name": "com.adminConsole.view.detailsWithLink"
        });
        kony.mvc.registry.add("com.c360.common.inputNumber", "inputNumber", "inputNumberController");
        kony.application.registerMaster({
            "namespace": "com.c360.common",
            "classname": "inputNumber",
            "name": "com.c360.common.inputNumber"
        });
        kony.mvc.registry.add("com.customer360.common.CharactersplusMinus", "CharactersplusMinus", "CharactersplusMinusController");
        kony.application.registerMaster({
            "namespace": "com.customer360.common",
            "classname": "CharactersplusMinus",
            "name": "com.customer360.common.CharactersplusMinus"
        });
        kony.mvc.registry.add("com.customer360.common.policies", "policies", "policiesController");
        kony.application.registerMaster({
            "namespace": "com.customer360.common",
            "classname": "policies",
            "name": "com.customer360.common.policies"
        });
        kony.mvc.registry.add("com.customer360.common.policiesRulesView", "policiesRulesView", "policiesRulesViewController");
        kony.application.registerMaster({
            "namespace": "com.customer360.common",
            "classname": "policiesRulesView",
            "name": "com.customer360.common.policiesRulesView"
        });
        kony.mvc.registry.add("com.konymp.map1", "map1", "map1Controller");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "map1",
            "name": "com.konymp.map1"
        });
        kony.mvc.registry.add("com.adminConsole.CSR.MessageFilter", "MessageFilter", "MessageFilterController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.CSR",
            "classname": "MessageFilter",
            "name": "com.adminConsole.CSR.MessageFilter"
        });
        kony.mvc.registry.add("com.adminConsole.CustomerManagement.ProfileGeneralInfo", "ProfileGeneralInfo", "ProfileGeneralInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.CustomerManagement",
            "classname": "ProfileGeneralInfo",
            "name": "com.adminConsole.CustomerManagement.ProfileGeneralInfo"
        });
        kony.mvc.registry.add("com.adminConsole.Groups.AdvancedSearch", "AdvancedSearch", "AdvancedSearchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Groups",
            "classname": "AdvancedSearch",
            "name": "com.adminConsole.Groups.AdvancedSearch"
        });
        kony.mvc.registry.add("com.adminConsole.LeadManagment.createUpdate", "createUpdate", "createUpdateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.LeadManagment",
            "classname": "createUpdate",
            "name": "com.adminConsole.LeadManagment.createUpdate"
        });
        kony.mvc.registry.add("com.adminConsole.Permissions.viewConfigureCSRAssist", "viewConfigureCSRAssist", "viewConfigureCSRAssistController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Permissions",
            "classname": "viewConfigureCSRAssist",
            "name": "com.adminConsole.Permissions.viewConfigureCSRAssist"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.imageSourceURL", "imageSourceURL", "imageSourceURLController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "imageSourceURL",
            "name": "com.adminConsole.adManagement.imageSourceURL"
        });
        kony.mvc.registry.add("com.adminConsole.common.listingSegmentClient", "listingSegmentClient", "listingSegmentClientController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "listingSegmentClient",
            "name": "com.adminConsole.common.listingSegmentClient"
        });
        kony.mvc.registry.add("com.adminConsole.common.listingSegmentLogs", "listingSegmentLogs", "listingSegmentLogsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "listingSegmentLogs",
            "name": "com.adminConsole.common.listingSegmentLogs"
        });
        kony.mvc.registry.add("com.adminConsole.csrEdit.editMessages", "editMessages", "editMessagesController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.csrEdit",
            "classname": "editMessages",
            "name": "com.adminConsole.csrEdit.editMessages"
        });
        kony.mvc.registry.add("com.adminConsole.customer.alerts", "alerts", "alertsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customer",
            "classname": "alerts",
            "name": "com.adminConsole.customer.alerts"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.DeviceMang", "DeviceMang", "DeviceMangController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "DeviceMang",
            "name": "com.adminConsole.customerMang.DeviceMang"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.EditContact", "EditContact", "EditContactController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "EditContact",
            "name": "com.adminConsole.customerMang.EditContact"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.RequestsHelpCenter", "RequestsHelpCenter", "RequestsHelpCenterController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "RequestsHelpCenter",
            "name": "com.adminConsole.customerMang.RequestsHelpCenter"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editAdditionalIncome", "editAdditionalIncome", "editAdditionalIncomeController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editAdditionalIncome",
            "name": "com.adminConsole.loans.applications.editAdditionalIncome"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editAddressInfo", "editAddressInfo", "editAddressInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editAddressInfo",
            "name": "com.adminConsole.loans.applications.editAddressInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editAddressInfoAsPrevious", "editAddressInfoAsPrevious", "editAddressInfoAsPreviousController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editAddressInfoAsPrevious",
            "name": "com.adminConsole.loans.applications.editAddressInfoAsPrevious"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editConsent", "editConsent", "editConsentController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editConsent",
            "name": "com.adminConsole.loans.applications.editConsent"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editContactInfo", "editContactInfo", "editContactInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editContactInfo",
            "name": "com.adminConsole.loans.applications.editContactInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editEmployementInfo", "editEmployementInfo", "editEmployementInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editEmployementInfo",
            "name": "com.adminConsole.loans.applications.editEmployementInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editExpenditureInfo", "editExpenditureInfo", "editExpenditureInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editExpenditureInfo",
            "name": "com.adminConsole.loans.applications.editExpenditureInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editLoaninformation", "editLoaninformation", "editLoaninformationController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editLoaninformation",
            "name": "com.adminConsole.loans.applications.editLoaninformation"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editPersonalInfo", "editPersonalInfo", "editPersonalInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editPersonalInfo",
            "name": "com.adminConsole.loans.applications.editPersonalInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewAdditionalIncome", "viewAdditionalIncome", "viewAdditionalIncomeController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewAdditionalIncome",
            "name": "com.adminConsole.loans.applications.viewAdditionalIncome"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewAddressInfo", "viewAddressInfo", "viewAddressInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewAddressInfo",
            "name": "com.adminConsole.loans.applications.viewAddressInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewConsent", "viewConsent", "viewConsentController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewConsent",
            "name": "com.adminConsole.loans.applications.viewConsent"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewContactInfo", "viewContactInfo", "viewContactInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewContactInfo",
            "name": "com.adminConsole.loans.applications.viewContactInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewEmployerDetails", "viewEmployerDetails", "viewEmployerDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewEmployerDetails",
            "name": "com.adminConsole.loans.applications.viewEmployerDetails"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewExpenditureInfo", "viewExpenditureInfo", "viewExpenditureInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewExpenditureInfo",
            "name": "com.adminConsole.loans.applications.viewExpenditureInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewIncomeSource", "viewIncomeSource", "viewIncomeSourceController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewIncomeSource",
            "name": "com.adminConsole.loans.applications.viewIncomeSource"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewLoaninformation", "viewLoaninformation", "viewLoaninformationController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewLoaninformation",
            "name": "com.adminConsole.loans.applications.viewLoaninformation"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewPersonalInfo", "viewPersonalInfo", "viewPersonalInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewPersonalInfo",
            "name": "com.adminConsole.loans.applications.viewPersonalInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editAUDetails", "editAUDetails", "editAUDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editAUDetails",
            "name": "com.adminConsole.loans.applications.editAUDetails"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editApplicantDetails", "editApplicantDetails", "editApplicantDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editApplicantDetails",
            "name": "com.adminConsole.loans.applications.editApplicantDetails"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewAUDetails", "viewAUDetails", "viewAUDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewAUDetails",
            "name": "com.adminConsole.loans.applications.viewAUDetails"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewApplicantDetails", "viewApplicantDetails", "viewApplicantDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewApplicantDetails",
            "name": "com.adminConsole.loans.applications.viewApplicantDetails"
        });
        kony.mvc.registry.add("flxOptionList", "flxOptionList", "flxOptionListController");
        kony.mvc.registry.add("flxAccountAlertsCategoryDetails", "flxAccountAlertsCategoryDetails", "flxAccountAlertsCategoryDetailsController");
        kony.mvc.registry.add("flxDetailsURLs", "flxDetailsURLs", "flxDetailsURLsController");
        kony.mvc.registry.add("flxAddOption", "flxAddOption", "flxAddOptionController");
        kony.mvc.registry.add("flxAddPermissions", "flxAddPermissions", "flxAddPermissionsController");
        kony.mvc.registry.add("flxAddRole", "flxAddRole", "flxAddRoleController");
        kony.mvc.registry.add("flxAddSecurityQuestion", "flxAddSecurityQuestion", "flxAddSecurityQuestionController");
        kony.mvc.registry.add("flxAddUsers", "flxAddUsers", "flxAddUsersController");
        kony.mvc.registry.add("flxAdminConsoleLogList", "flxAdminConsoleLogList", "flxAdminConsoleLogListController");
        kony.mvc.registry.add("flxAlertCategory", "flxAlertCategory", "flxAlertCategoryController");
        kony.mvc.registry.add("flxAlertGroupManagement", "flxAlertGroupManagement", "flxAlertGroupManagementController");
        kony.mvc.registry.add("flxAlertSequenceMap", "flxAlertSequenceMap", "flxAlertSequenceMapController");
        kony.mvc.registry.add("segAssociatedAlerts", "segAssociatedAlerts", "segAssociatedAlertsController");
        kony.mvc.registry.add("flxAlertsCategoryLanguageData", "flxAlertsCategoryLanguageData", "flxAlertsCategoryLanguageDataController");
        kony.mvc.registry.add("flxAlertsLanguageData", "flxAlertsLanguageData", "flxAlertsLanguageDataController");
        kony.mvc.registry.add("segVariableReference", "segVariableReference", "segVariableReferenceController");
        kony.mvc.registry.add("flxAssignUsers", "flxAssignUsers", "flxAssignUsersController");
        kony.mvc.registry.add("flxTAndC", "flxTAndC", "flxTAndCController");
        kony.mvc.registry.add("flxAvailableAccounts", "flxAvailableAccounts", "flxAvailableAccountsController");
        kony.mvc.registry.add("flxCSRMessageTemplate", "flxCSRMessageTemplate", "flxCSRMessageTemplateController");
        kony.mvc.registry.add("flxCSRMsgList", "flxCSRMsgList", "flxCSRMsgListController");
        kony.mvc.registry.add("flxCSRQueueList", "flxCSRQueueList", "flxCSRQueueListController");
        kony.mvc.registry.add("flxCampaignAddCustomer", "flxCampaignAddCustomer", "flxCampaignAddCustomerController");
        kony.mvc.registry.add("flxCampaignOptionsSelected", "flxCampaignOptionsSelected", "flxCampaignOptionsSelectedController");
        kony.mvc.registry.add("flxCampaigns", "flxCampaigns", "flxCampaignsController");
        kony.mvc.registry.add("flxCompanies", "flxCompanies", "flxCompaniesController");
        kony.mvc.registry.add("flxCompanyCustomer", "flxCompanyCustomer", "flxCompanyCustomerController");
        kony.mvc.registry.add("flxConfiguration", "flxConfiguration", "flxConfigurationController");
        kony.mvc.registry.add("flxConfigurationWithDescription", "flxConfigurationWithDescription", "flxConfigurationWithDescriptionController");
        kony.mvc.registry.add("flxCriterias", "flxCriterias", "flxCriteriasController");
        kony.mvc.registry.add("flxSelected", "flxSelected", "flxSelectedController");
        kony.mvc.registry.add("flxCustMangActivityHistory", "flxCustMangActivityHistory", "flxCustMangActivityHistoryController");
        kony.mvc.registry.add("flxCustMangActivityHistoryDetails", "flxCustMangActivityHistoryDetails", "flxCustMangActivityHistoryDetailsController");
        kony.mvc.registry.add("flxCustMangAlertHistory", "flxCustMangAlertHistory", "flxCustMangAlertHistoryController");
        kony.mvc.registry.add("flxCustMangNotification", "flxCustMangNotification", "flxCustMangNotificationController");
        kony.mvc.registry.add("flxCustMangNotificationSelected", "flxCustMangNotificationSelected", "flxCustMangNotificationSelectedController");
        kony.mvc.registry.add("flxCustMangProductInfo", "flxCustMangProductInfo", "flxCustMangProductInfoController");
        kony.mvc.registry.add("flxCustMangRequest", "flxCustMangRequest", "flxCustMangRequestController");
        kony.mvc.registry.add("flxCustMangRequestSelected", "flxCustMangRequestSelected", "flxCustMangRequestSelectedController");
        kony.mvc.registry.add("flxCustMangSearch", "flxCustMangSearch", "flxCustMangSearchController");
        kony.mvc.registry.add("flxCustMangTransactionHistoryCompanies", "flxCustMangTransactionHistoryCompanies", "flxCustMangTransactionHistoryCompaniesController");
        kony.mvc.registry.add("flxCustMangTransctionHistory", "flxCustMangTransctionHistory", "flxCustMangTransctionHistoryController");
        kony.mvc.registry.add("flxCustomerActivityLog", "flxCustomerActivityLog", "flxCustomerActivityLogController");
        kony.mvc.registry.add("flxCustomerActivityLogAccounts", "flxCustomerActivityLogAccounts", "flxCustomerActivityLogAccountsController");
        kony.mvc.registry.add("flxCustomerAdminLog", "flxCustomerAdminLog", "flxCustomerAdminLogController");
        kony.mvc.registry.add("flxCustomerCardsList", "flxCustomerCardsList", "flxCustomerCardsListController");
        kony.mvc.registry.add("flxCustomerCare", "flxCustomerCare", "flxCustomerCareController");
        kony.mvc.registry.add("flxCustomerCareSelected", "flxCustomerCareSelected", "flxCustomerCareSelectedController");
        kony.mvc.registry.add("flxCustomerInformation", "flxCustomerInformation", "flxCustomerInformationController");
        kony.mvc.registry.add("flxCustomerMangEntitlements", "flxCustomerMangEntitlements", "flxCustomerMangEntitlementsController");
        kony.mvc.registry.add("flxCustomerMangGroup", "flxCustomerMangGroup", "flxCustomerMangGroupController");
        kony.mvc.registry.add("flxCustomerResults", "flxCustomerResults", "flxCustomerResultsController");
        kony.mvc.registry.add("flxCustomerSubAlerts", "flxCustomerSubAlerts", "flxCustomerSubAlertsController");
        kony.mvc.registry.add("flxsegCustomerGroup", "flxsegCustomerGroup", "flxsegCustomerGroupController");
        kony.mvc.registry.add("flxDashBoardAlert", "flxDashBoardAlert", "flxDashBoardAlertController");
        kony.mvc.registry.add("flxDecisionComments", "flxDecisionComments", "flxDecisionCommentsController");
        kony.mvc.registry.add("flxDecisionFiles", "flxDecisionFiles", "flxDecisionFilesController");
        kony.mvc.registry.add("flxDetailsDefaultURLs", "flxDetailsDefaultURLs", "flxDetailsDefaultURLsController");
        kony.mvc.registry.add("flxDepositApplicationsStarted", "flxDepositApplicationsStarted", "flxDepositApplicationsStartedController");
        kony.mvc.registry.add("flxDepositApplicationsSubmitted", "flxDepositApplicationsSubmitted", "flxDepositApplicationsSubmittedController");
        kony.mvc.registry.add("flxDepositsLeads", "flxDepositsLeads", "flxDepositsLeadsController");
        kony.mvc.registry.add("flxDeviceManagement", "flxDeviceManagement", "flxDeviceManagementController");
        kony.mvc.registry.add("flxMain", "flxMain", "flxMainController");
        kony.mvc.registry.add("flxDropDown", "flxDropDown", "flxDropDownController");
        kony.mvc.registry.add("flxDynamicLogs", "flxDynamicLogs", "flxDynamicLogsController");
        kony.mvc.registry.add("flxEmailMain", "flxEmailMain", "flxEmailMainController");
        kony.mvc.registry.add("flxEmailMainLeftPadding", "flxEmailMainLeftPadding", "flxEmailMainLeftPaddingController");
        kony.mvc.registry.add("flxFAQ", "flxFAQ", "flxFAQController");
        kony.mvc.registry.add("flxFeeRange", "flxFeeRange", "flxFeeRangeController");
        kony.mvc.registry.add("flxGroupseg", "flxGroupseg", "flxGroupsegController");
        kony.mvc.registry.add("flxGroupsAdvSearch", "flxGroupsAdvSearch", "flxGroupsAdvSearchController");
        kony.mvc.registry.add("flxGroupsAssignedCustomers", "flxGroupsAssignedCustomers", "flxGroupsAssignedCustomersController");
        kony.mvc.registry.add("flxGrpAddOption", "flxGrpAddOption", "flxGrpAddOptionController");
        kony.mvc.registry.add("flxHeaderDashBoardAlert", "flxHeaderDashBoardAlert", "flxHeaderDashBoardAlertController");
        kony.mvc.registry.add("flxIdResults", "flxIdResults", "flxIdResultsController");
        kony.mvc.registry.add("segUploadedImageListKA", "segUploadedImageListKA", "segUploadedImageListKAController");
        kony.mvc.registry.add("flxImageUploadListHeader", "flxImageUploadListHeader", "flxImageUploadListHeaderController");
        kony.mvc.registry.add("flxLeads", "flxLeads", "flxLeadsController");
        kony.mvc.registry.add("flxLeftMenuItem", "flxLeftMenuItem", "flxLeftMenuItemController");
        kony.mvc.registry.add("flxLeftMenuSectionHeader", "flxLeftMenuSectionHeader", "flxLeftMenuSectionHeaderController");
        kony.mvc.registry.add("flxLeftMenuSectionItem", "flxLeftMenuSectionItem", "flxLeftMenuSectionItemController");
        kony.mvc.registry.add("flxLimitsCustomer", "flxLimitsCustomer", "flxLimitsCustomerController");
        kony.mvc.registry.add("flxListOfApplicationsSubmittedHeader", "flxListOfApplicationsSubmittedHeader", "flxListOfApplicationsSubmittedHeaderController");
        kony.mvc.registry.add("flxListOfApplications", "flxListOfApplications", "flxListOfApplicationsController");
        kony.mvc.registry.add("flxListOfApplicationsHeader", "flxListOfApplicationsHeader", "flxListOfApplicationsHeaderController");
        kony.mvc.registry.add("flxListOfApplicationsSubmitted", "flxListOfApplicationsSubmitted", "flxListOfApplicationsSubmittedController");
        kony.mvc.registry.add("flxListOfLeads", "flxListOfLeads", "flxListOfLeadsController");
        kony.mvc.registry.add("flxLocationFacilites", "flxLocationFacilites", "flxLocationFacilitesController");
        kony.mvc.registry.add("flxsegLocations", "flxsegLocations", "flxsegLocationsController");
        kony.mvc.registry.add("flxCustSearch", "flxCustSearch", "flxCustSearchController");
        kony.mvc.registry.add("flxSegMFAConfigs", "flxSegMFAConfigs", "flxSegMFAConfigsController");
        kony.mvc.registry.add("flxSegMFAConfigsSelected", "flxSegMFAConfigsSelected", "flxSegMFAConfigsSelectedController");
        kony.mvc.registry.add("flxSegMFAScenarioSelected", "flxSegMFAScenarioSelected", "flxSegMFAScenarioSelectedController");
        kony.mvc.registry.add("flxMasterList", "flxMasterList", "flxMasterListController");
        kony.mvc.registry.add("flxCustomerMangNotes", "flxCustomerMangNotes", "flxCustomerMangNotesController");
        kony.mvc.registry.add("segNotes", "segNotes", "segNotesController");
        kony.mvc.registry.add("flxNotificationsOuter", "flxNotificationsOuter", "flxNotificationsOuterController");
        kony.mvc.registry.add("flxOptionAdded", "flxOptionAdded", "flxOptionAddedController");
        kony.mvc.registry.add("flxOutage", "flxOutage", "flxOutageController");
        kony.mvc.registry.add("flxPermissions", "flxPermissions", "flxPermissionsController");
        kony.mvc.registry.add("flxPermissionsHeader", "flxPermissionsHeader", "flxPermissionsHeaderController");
        kony.mvc.registry.add("flxPolicies", "flxPolicies", "flxPoliciesController");
        kony.mvc.registry.add("flxPolicyDescription", "flxPolicyDescription", "flxPolicyDescriptionController");
        kony.mvc.registry.add("flxProducts", "flxProducts", "flxProductsController");
        kony.mvc.registry.add("flxReportsMangMessages", "flxReportsMangMessages", "flxReportsMangMessagesController");
        kony.mvc.registry.add("flxReportsMangSuggestions", "flxReportsMangSuggestions", "flxReportsMangSuggestionsController");
        kony.mvc.registry.add("flxReportsMangThreads", "flxReportsMangThreads", "flxReportsMangThreadsController");
        kony.mvc.registry.add("flxTransactionReports", "flxTransactionReports", "flxTransactionReportsController");
        kony.mvc.registry.add("flxUsersReport", "flxUsersReport", "flxUsersReportController");
        kony.mvc.registry.add("flxRequestsOuter", "flxRequestsOuter", "flxRequestsOuterController");
        kony.mvc.registry.add("flxRoles", "flxRoles", "flxRolesController");
        kony.mvc.registry.add("flxRolesHeader", "flxRolesHeader", "flxRolesHeaderController");
        kony.mvc.registry.add("flxSearchCompanyMap", "flxSearchCompanyMap", "flxSearchCompanyMapController");
        kony.mvc.registry.add("flxSearchDropDown", "flxSearchDropDown", "flxSearchDropDownController");
        kony.mvc.registry.add("flxSecurityQuestions", "flxSecurityQuestions", "flxSecurityQuestionsController");
        kony.mvc.registry.add("flxServicesAndFaq", "flxServicesAndFaq", "flxServicesAndFaqController");
        kony.mvc.registry.add("flxServices2", "flxServices2", "flxServices2Controller");
        kony.mvc.registry.add("flxServicesAndFaqSelected", "flxServicesAndFaqSelected", "flxServicesAndFaqSelectedController");
        kony.mvc.registry.add("flxSegDefineFeeRange", "flxSegDefineFeeRange", "flxSegDefineFeeRangeController");
        kony.mvc.registry.add("flxServicesScheduleMaster", "flxServicesScheduleMaster", "flxServicesScheduleMasterController");
        kony.mvc.registry.add("flxSubAlerts", "flxSubAlerts", "flxSubAlertsController");
        kony.mvc.registry.add("flxTargetCustRoles", "flxTargetCustRoles", "flxTargetCustRolesController");
        kony.mvc.registry.add("flxTermsAndConditions", "flxTermsAndConditions", "flxTermsAndConditionsController");
        kony.mvc.registry.add("flxTransactionLogs", "flxTransactionLogs", "flxTransactionLogsController");
        kony.mvc.registry.add("flxUsers", "flxUsers", "flxUsersController");
        kony.mvc.registry.add("flxUsersHeader", "flxUsersHeader", "flxUsersHeaderController");
        kony.mvc.registry.add("flxSegVariableOptions", "flxSegVariableOptions", "flxSegVariableOptionsController");
        kony.mvc.registry.add("flxViewConfigureCSR", "flxViewConfigureCSR", "flxViewConfigureCSRController");
        kony.mvc.registry.add("flxVewCustomerRoles", "flxVewCustomerRoles", "flxVewCustomerRolesController");
        kony.mvc.registry.add("flxViewEntitlements", "flxViewEntitlements", "flxViewEntitlementsController");
        kony.mvc.registry.add("flxViewPermissions", "flxViewPermissions", "flxViewPermissionsController");
        kony.mvc.registry.add("flxViewRoles", "flxViewRoles", "flxViewRolesController");
        kony.mvc.registry.add("flxViewServiceDetailsTF", "flxViewServiceDetailsTF", "flxViewServiceDetailsTFController");
        kony.mvc.registry.add("flxViewUsers", "flxViewUsers", "flxViewUsersController");
        kony.mvc.registry.add("flxCampaignPriorities", "flxCampaignPriorities", "flxCampaignPrioritiesController");
        kony.mvc.registry.add("flxGuestSimulationResults", "flxGuestSimulationResults", "flxGuestSimulationResultsController");
        kony.mvc.registry.add("flxSimulationResults", "flxSimulationResults", "flxSimulationResultsController");
        kony.mvc.registry.add("frmAlertsManagement", "AlertsManagementModule/frmAlertsManagement", "AlertsManagementModule/frmAlertsManagementController");
        kony.mvc.registry.add("frmErrorLogin", "AuthModule/frmErrorLogin", "AuthModule/frmErrorLoginController");
        kony.mvc.registry.add("frmLogin", "AuthModule/frmLogin", "AuthModule/frmLoginController");
        kony.mvc.registry.add("frmCSR", "CSRModule/frmCSR", "CSRModule/frmCSRController");
        kony.mvc.registry.add("CampaignModule/frmAdDetails", "CampaignModule/frmAdDetails", "CampaignModule/frmAdDetailsController");
        kony.mvc.registry.add("CampaignModule/frmAdManagement", "CampaignModule/frmAdManagement", "CampaignModule/frmAdManagementController");
        kony.mvc.registry.add("frmCompanies", "CompaniesModule/frmCompanies", "CompaniesModule/frmCompaniesController");
        kony.mvc.registry.add("frmBusinessConfigurations", "ConfigurationsModule/frmBusinessConfigurations", "ConfigurationsModule/frmBusinessConfigurationsController");
        kony.mvc.registry.add("frmConfigurationBundles", "ConfigurationsModule/frmConfigurationBundles", "ConfigurationsModule/frmConfigurationBundlesController");
        kony.mvc.registry.add("frmCustomerCreate", "CustomerCreateModule/frmCustomerCreate", "CustomerCreateModule/frmCustomerCreateController");
        kony.mvc.registry.add("frmGroups", "CustomerGroupsModule/frmGroups", "CustomerGroupsModule/frmGroupsController");
        kony.mvc.registry.add("CustomerManagementModule/frmAssistedOnboarding", "CustomerManagementModule/frmAssistedOnboarding", "CustomerManagementModule/frmAssistedOnboardingController");
        kony.mvc.registry.add("frmCustomerManagement", "CustomerManagementModule/frmCustomerManagement", "CustomerManagementModule/frmCustomerManagementController");
        kony.mvc.registry.add("CustomerManagementModule/frmCustomerProfileAccounts", "CustomerManagementModule/frmCustomerProfileAccounts", "CustomerManagementModule/frmCustomerProfileAccountsController");
        kony.mvc.registry.add("frmCustomerProfileActivityHistory", "CustomerManagementModule/frmCustomerProfileActivityHistory", "CustomerManagementModule/frmCustomerProfileActivityHistoryController");
        kony.mvc.registry.add("CustomerManagementModule/frmCustomerProfileAlerts", "CustomerManagementModule/frmCustomerProfileAlerts", "CustomerManagementModule/frmCustomerProfileAlertsController");
        kony.mvc.registry.add("CustomerManagementModule/frmCustomerProfileContacts", "CustomerManagementModule/frmCustomerProfileContacts", "CustomerManagementModule/frmCustomerProfileContactsController");
        kony.mvc.registry.add("frmCustomerProfileDeviceInfo", "CustomerManagementModule/frmCustomerProfileDeviceInfo", "CustomerManagementModule/frmCustomerProfileDeviceInfoController");
        kony.mvc.registry.add("CustomerManagementModule/frmCustomerProfileEntitlements", "CustomerManagementModule/frmCustomerProfileEntitlements", "CustomerManagementModule/frmCustomerProfileEntitlementsController");
        kony.mvc.registry.add("CustomerManagementModule/frmCustomerProfileHelpCenter", "CustomerManagementModule/frmCustomerProfileHelpCenter", "CustomerManagementModule/frmCustomerProfileHelpCenterController");
        kony.mvc.registry.add("CustomerManagementModule/frmCustomerProfileRoles", "CustomerManagementModule/frmCustomerProfileRoles", "CustomerManagementModule/frmCustomerProfileRolesController");
        kony.mvc.registry.add("frmDepositsDashboard", "CustomerManagementModule/frmDepositsDashboard", "CustomerManagementModule/frmDepositsDashboardController");
        kony.mvc.registry.add("frmLoansDashboard", "CustomerManagementModule/frmLoansDashboard", "CustomerManagementModule/frmLoansDashboardController");
        kony.mvc.registry.add("CustomerManagementModule/frmNewCustomer", "CustomerManagementModule/frmNewCustomer", "CustomerManagementModule/frmNewCustomerController");
        kony.mvc.registry.add("CustomerManagementModule/frmTrackApplication", "CustomerManagementModule/frmTrackApplication", "CustomerManagementModule/frmTrackApplicationController");
        kony.mvc.registry.add("CustomerManagementModule/frmUpgradeUser", "CustomerManagementModule/frmUpgradeUser", "CustomerManagementModule/frmUpgradeUserController");
        kony.mvc.registry.add("frmDashboard", "DashboardModule/frmDashboard", "DashboardModule/frmDashboardController");
        kony.mvc.registry.add("DecisionManagementModule/frmDecisionManagement", "DecisionManagementModule/frmDecisionManagement", "DecisionManagementModule/frmDecisionManagementController");
        kony.mvc.registry.add("frmGuestDashboard", "DetailsModule/frmGuestDashboard", "DetailsModule/frmGuestDashboardController");
        kony.mvc.registry.add("frmFAQ", "FAQsModule/frmFAQ", "FAQsModule/frmFAQController");
        kony.mvc.registry.add("frmUsers", "InternalUserModule/frmUsers", "InternalUserModule/frmUsersController");
        kony.mvc.registry.add("LeadManagementModule/frmLeadManagement", "LeadManagementModule/frmLeadManagement", "LeadManagementModule/frmLeadManagementController");
        kony.mvc.registry.add("frmLogs", "LogsModule/frmLogs", "LogsModule/frmLogsController");
        kony.mvc.registry.add("frmMFAConfigurations", "MFAModule/frmMFAConfigurations", "MFAModule/frmMFAConfigurationsController");
        kony.mvc.registry.add("frmMFAScenarios", "MFAModule/frmMFAScenarios", "MFAModule/frmMFAScenariosController");
        kony.mvc.registry.add("frmCustomerCare", "MasterDataModule/frmCustomerCare", "MasterDataModule/frmCustomerCareController");
        kony.mvc.registry.add("frmLocations", "MasterDataModule/frmLocations", "MasterDataModule/frmLocationsController");
        kony.mvc.registry.add("MasterDataModule/frmProduct", "MasterDataModule/frmProduct", "MasterDataModule/frmProductController");
        kony.mvc.registry.add("frmOutageMessage", "OutageMessageModule/frmOutageMessage", "OutageMessageModule/frmOutageMessageController");
        kony.mvc.registry.add("frmPasswordAgeAndLockoutSettings", "PasswordAgeAndLockoutModule/frmPasswordAgeAndLockoutSettings", "PasswordAgeAndLockoutModule/frmPasswordAgeAndLockoutSettingsController");
        kony.mvc.registry.add("frmPermissions", "PermissionsModule/frmPermissions", "PermissionsModule/frmPermissionsController");
        kony.mvc.registry.add("frmPolicies", "PolicyModule/frmPolicies", "PolicyModule/frmPoliciesController");
        kony.mvc.registry.add("frmPrivacyPolicy", "PrivacyPolicyModule/frmPrivacyPolicy", "PrivacyPolicyModule/frmPrivacyPolicyController");
        kony.mvc.registry.add("frmReportsManagement", "ReportsManagementModule/frmReportsManagement", "ReportsManagementModule/frmReportsManagementController");
        kony.mvc.registry.add("frmRoles", "RoleModule/frmRoles", "RoleModule/frmRolesController");
        kony.mvc.registry.add("frmSecureImage", "SecurityModule/frmSecureImage", "SecurityModule/frmSecureImageController");
        kony.mvc.registry.add("frmSecurityQuestions", "SecurityModule/frmSecurityQuestions", "SecurityModule/frmSecurityQuestionsController");
        kony.mvc.registry.add("frmServiceManagement", "ServicesManagementModule/frmServiceManagement", "ServicesManagementModule/frmServiceManagementController");
        kony.mvc.registry.add("frmTermsAndConditions", "TermsAndConditionsModule/frmTermsAndConditions", "TermsAndConditionsModule/frmTermsAndConditionsController");
        setAppBehaviors();
    },
    postAppInitCallBack: function(eventObj) {},
    appmenuseq: function() {
        new kony.mvc.Navigation("frmLogin").navigate();
    }
});