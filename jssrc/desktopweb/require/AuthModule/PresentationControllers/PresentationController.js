define([], function() {
    function Auth_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }
    inheritsFrom(Auth_PresentationController, kony.mvc.Presentation.BasePresenter);
    Auth_PresentationController.prototype.initializePresentationController = function() {};
    /**
     * Function called to show Login screen.
     * @name showLoginScreen
     * @member AuthModule.presentationController
     * @param {action : string} context
     */
    Auth_PresentationController.prototype.showLoginScreen = function(context) {
        this.presentUserInterface("frmLogin", context);
    };
    /**
     * Is called after successfull login to show the Dashboard screen
     * @name showDashboard
     * @member AuthModule.presentationController
     * @param undefined context
     */
    Auth_PresentationController.prototype.showDashboard = function(context) {
        // var dashBoard = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("DashboardModule");
        // dashBoard.presentationController.fetchDashBoardData();
        this.navigateTo('DashboardModule', 'fetchDashBoardData');
    };
    /**
     * Function called to show Change Password screen.
     * @name showChangePasswordScreen
     * @member AuthModule.presentationController
     */
    Auth_PresentationController.prototype.showChangePasswordScreen = function() {
        this.showErrorLoginScreen({
            "action": "changePassword"
        });
    };
    /**
     * Function called to show ErrorLogin screen.
     * @name showErrorLoginScreen
     * @member AuthModule.presentationController
     * @param {action : string} context for UI changes
     */
    Auth_PresentationController.prototype.showErrorLoginScreen = function(context) {
        this.presentUserInterface("frmErrorLogin", context);
    };
    /**
     * Function called when the user clicks LogIn button .
     * Parameters: usernamePasswordObj - username ,password and rememberme Status
     * @name onLogin
     * @member AuthModule.presentationController
     * @param {username : string, password : string, rememberMe : null} usernamePasswordObj
     * @param ()=>any loginSuccessCallBack
     * @param (...callbackArgs)=>any loginFailureCallBack
     */
    Auth_PresentationController.prototype.onLogin = function(usernamePasswordObj, loginSuccessCallBack, loginFailureCallBack) {
        var self = this;
        this.businessController.doLogin(usernamePasswordObj, onSuccess, onError);

        function onSuccess(response) {
            kony.mvc.MDAApplication.getSharedInstance().appContext.userName = usernamePasswordObj.username;
            self.businessController.getServerTimeZoneOffset();
            kony.application.dismissLoadingScreen();
            loginSuccessCallBack(response, usernamePasswordObj);
        }

        function onError(response) {
            kony.mvc.MDAApplication.getSharedInstance().appContext.userName = usernamePasswordObj.username;
            kony.application.dismissLoadingScreen();
            loginFailureCallBack(response);
        }
    };
    /**
     * Function to set Idle Timeout and it internally calls onSessionExpire function
     * @name setIdleTimeout
     * @member AuthModule.presentationController
     */
    Auth_PresentationController.prototype.setIdleTimeout = function() {
        var self = this;

        function idleTimeoutCompletionCallback(response) {
            self.onSessionExpire();
        }
        this.businessController.registerIdleTimeout({}, idleTimeoutCompletionCallback);
    };
    /**
     * @name checkSessionTimedout
     * @member AuthModule.presentationController
     */
    Auth_PresentationController.prototype.checkSessionTimedout = function() {
        var sessionExpiredTime = kony.ds.read('SessionExpired');
        if (sessionExpiredTime && (new Date() - new Date(sessionExpiredTime)) < 10000) {
            var context = {
                "action": "SessionExpired"
            };
            this.showLoginScreen(context);
        }
    };
    /**
     * Function to logout once the session is expired
     */
    Auth_PresentationController.prototype.onSessionExpire = function() {
        kony.ds.save(new Date(), 'SessionExpired', {
            dsmode: "cache"
        });
        this.doLogout();
    };
    /**
     * Function specifies the action like Logout/SessionExpired
     * @name doLogout
     * @member AuthModule.presentationController
     * @param undefined context
     * @param undefined logoutSuccessCallback
     * @param undefined logoutErrorCallback
     */
    Auth_PresentationController.prototype.doLogout = function(context, logoutSuccessCallback, logoutErrorCallback) {
        var self = this;
        logoutSuccessCallback = logoutSuccessCallback || self.logoutSuccessCallback.bind(self);
        logoutErrorCallback = logoutErrorCallback || self.logoutErrorCallback.bind(self);

        function onLogoutSuccess() {
            self.businessController.resetLocalStorage({}, function successfullyClearedData() {}, function failedToReload() {});
            logoutSuccessCallback(context);
        }
        self.businessController.doLogout({}, onLogoutSuccess, logoutErrorCallback);
    };
    /**
     * Function to De Register the Idle Time Out
     * Parameters: context
     * @name setIdleTimeout
     * @member AuthModule.presentationController
     * 
     */
    Auth_PresentationController.prototype.deRegisterIdleTimeout = function(context) {
        var self = this;

        function deregisterCompletionCallback() {
            self.deregisterSuccessCallback(context);
        }
        self.businessController.deregisterIdleTimeout({}, deregisterCompletionCallback);
    };
    /**
     * Function to De Register the Idle Time Out Completion Callback
     * Parameters: context
     */
    Auth_PresentationController.prototype.deregisterSuccessCallback = function(context) {
        // alert("Logout Successful");
        //Removed the login screen as it is causing a jerk 
        //The login screen will be shown after browser refresh 
        //this.showLoginScreen(context);
    };
    /**
     * Function to reset password
     * @name toResetPassword
     * @member AuthModule.presentationController
     * @param string userName
     * @param string currentPassword
     * @param string newPassword
     * @param (...callbackArgs)=>any toResetPasswordResponse
     */
    Auth_PresentationController.prototype.toResetPassword = function(userName, currentPassword, newPassword, toResetPasswordResponse) {
        var self = this;
        var userDetails = {
            "inputUserName": userName,
            "previousPassword": currentPassword,
            "newPassword": newPassword
        };
        this.businessController.resetPassword(userDetails, resetPasswordSuccess, resetPasswordError);

        function resetPasswordSuccess(response) {
            toResetPasswordResponse(response);
        }

        function resetPasswordError(response) {
            toResetPasswordResponse(response);
        }
    };
    /**
     * function to show Login page with message: "User ID & password not entered"
     * @name userIdPasswordNotEntered
     * @member AuthModule.presentationController
     * @param {username : string, password : string, rememberMe : null} usernamePasswordObj
     */
    Auth_PresentationController.prototype.userIdPasswordNotEntered = function(usernamePasswordObj) {
        var context;
        if (usernamePasswordObj.username === "" && usernamePasswordObj.password === "") {
            context = {
                "action": "userIdAndPasswordNotEntered"
            };
        } else if (usernamePasswordObj.username === "") {
            context = {
                "action": "userIdNotEntered"
            };
        } else if (usernamePasswordObj.password === "") {
            context = {
                "action": "passwordNotEntered"
            };
        }
        this.showLoginScreen(context);
    };
    /**
     * Function to save username in local storage 
     * @name saveUserName
     * @member AuthModule.presentationController
     * @param {username : string, password : string, rememberMe : null} usernamePasswordObj
     */
    Auth_PresentationController.prototype.saveUserName = function(usernamePasswordObj) {
        this.businessController.saveUserName({
            "username": usernamePasswordObj.username,
            "rememberme": usernamePasswordObj.rememberMe,
        });
    };
    /** 
     * Function to send reset password link to the email id of user 
     * @name sendResetPasswordEmail
     * @member AuthModule.presentationController
     * @param FormController emailView
     * @param {emailType : string, emailId : string} emailJSON
     */
    Auth_PresentationController.prototype.sendResetPasswordEmail = function(emailView, emailJSON) {
        var self = this;
        // alert("emailJSON: "+JSON.stringify(emailJSON));
        this.businessController.sendEmail(emailJSON, onSuccess, onError);

        function onSuccess(response) {
            self.showErrorLoginScreen({
                "action": "showEmailSentFlex",
                "emailId": response.emailId
            });
        }

        function onError(response) {
            self.showErrorLoginScreen({
                "action": "showIncorrectEmailFlex",
                "emailMessage": response
            });
        }
    };
    /**
     * Function to open Reset Password form
     */
    Auth_PresentationController.prototype.openResetPasswordForm = function(qpJSON) {
        var self = this;
        var decodedVal = atob(qpJSON.qp);
        var qp = decodedVal.substring(0, decodedVal.indexOf("_-_"));
        var id = decodedVal.substring(decodedVal.indexOf("_-_") + 3, decodedVal.lastIndexOf("_-_"));
        var username = decodedVal.substring(decodedVal.lastIndexOf("_-_") + 3);
        var resetPasswordAttributesJSON = {
            "qp": qp,
            "id": id
        };
        kony.mvc.MDAApplication.getSharedInstance().appContext.resetPasswordAttributesJSON = resetPasswordAttributesJSON;
        context = {
            "action": "changePasswordForgotUser",
            "username": username
        };
        self.showErrorLoginScreen(context);
    };
    Auth_PresentationController.prototype.toResetPasswordForForgotPassword = function(userDetailsForForgotPasswordJSON, toResetPasswordForForgotResponse) {
        var self = this;
        this.businessController.resetPasswordForForgotPassword(userDetailsForForgotPasswordJSON, resetPasswordForForgotPasswordSuccess, resetPasswordForForgotPasswordError);

        function resetPasswordForForgotPasswordSuccess(response) {
            toResetPasswordForForgotResponse(response);
        }

        function resetPasswordForForgotPasswordError(response) {
            if (response == "Link expired") {
                var context = {
                    "action": "changePasswordForgotUserInvalidLink"
                };
                self.showLoginScreen(context);
            } else {
                toResetPasswordForForgotResponse(response);
            }
        }
    };
    /**
     * Function on logout Success calls deRegisterIdleTimeout 
     * @name logoutSuccessCallback
     * @member AuthModule.presentationController
     * @param undefined context
     */
    Auth_PresentationController.prototype.logoutSuccessCallback = function(context) {
        this.deRegisterIdleTimeout(context);
    };
    /**
     * Function on logout failure navigates to serverdown screen 
     * Parameters: context
     * Logout Error Callback 
     */
    Auth_PresentationController.prototype.logoutErrorCallback = function(context) {
        //Logout Failed navigate to downtime page.
        context = {
            "action": "downTime"
        };
        this.showErrorLoginScreen(context);
    };
    return Auth_PresentationController;
});