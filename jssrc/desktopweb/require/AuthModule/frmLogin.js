define("AuthModule/frmLogin", function() {
    return function(controller) {
        function addWidgetsfrmLogin() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMainLogin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "700dp",
                "id": "flxMainLogin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0bec2566294e74d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainLogin.setDefaultUnit(kony.flex.DP);
            var lblKony = new kony.ui.Label({
                "id": "lblKony",
                "isVisible": false,
                "left": "360dp",
                "skin": "slLabel0a8c6a6ddbc5f41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.kony\")",
                "top": "110dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTimeExpired = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxTimeExpired",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0e8d4a065064f41",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTimeExpired.setDefaultUnit(kony.flex.DP);
            var flxCross = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "13dp",
                "id": "flxCross",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "17dp",
                "skin": "slFbox",
                "width": "13dp"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCross.setDefaultUnit(kony.flex.DP);
            var lblCross = new kony.ui.Label({
                "height": "100%",
                "id": "lblCross",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon13pxWhite",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCross.add(lblCross);
            var flxtimeContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxtimeContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "390dp",
                "zIndex": 1
            }, {}, {});
            flxtimeContainer.setDefaultUnit(kony.flex.DP);
            var lblTimeExpired = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblTimeExpired",
                "isVisible": true,
                "skin": "slLabel0f65ab23851324d",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblTimeExpired\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTimeout = new kony.ui.Label({
                "centerY": "50%",
                "height": "32dp",
                "id": "lblTimeout",
                "isVisible": true,
                "left": "19dp",
                "skin": "sknIcon32pxWhiteNormal",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblTimeout\")",
                "width": "32dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxtimeContainer.add(lblTimeExpired, lblTimeout);
            flxTimeExpired.add(flxCross, flxtimeContainer);
            var flxUnderlinelight = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxUnderlinelight",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "515dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "underlinelight0h970648ba15246",
                "top": "400dp",
                "width": "280dp",
                "zIndex": 1
            }, {}, {});
            flxUnderlinelight.setDefaultUnit(kony.flex.DP);
            flxUnderlinelight.add();
            var flxLoginWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "440px",
                "id": "flxLoginWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": "700px",
                "zIndex": 1
            }, {}, {});
            flxLoginWrapper.setDefaultUnit(kony.flex.DP);
            var flxKonyLogo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "440dp",
                "id": "flxKonyLogo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox0f803df02b16843",
                "top": "0dp",
                "width": "352dp",
                "zIndex": 1
            }, {}, {});
            flxKonyLogo.setDefaultUnit(kony.flex.DP);
            var KonyImg = new kony.ui.Image2({
                "height": "60dp",
                "id": "KonyImg",
                "isVisible": false,
                "left": "100dp",
                "skin": "slImage0ie17d400557f45",
                "src": "loader.png",
                "top": "20dp",
                "width": "77dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var KonyBankLbl = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "KonyBankLbl",
                "isVisible": false,
                "skin": "slLabel0hc85d74cc7f74e",
                "text": "Customer 360",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Welcomelbl = new kony.ui.Label({
                "id": "Welcomelbl",
                "isVisible": false,
                "left": "38dp",
                "skin": "slLabel0ea43a2a5545d46",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.Welcomelbl\")",
                "top": "230dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var konytextlogo = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "60dp",
                "id": "konytextlogo",
                "isVisible": true,
                "skin": "slImage0ie17d400557f45",
                "src": "konydbxlogobignverted.png",
                "width": "200dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var konybgimg = new kony.ui.Image2({
                "height": "235dp",
                "id": "konybgimg",
                "isVisible": false,
                "left": "149dp",
                "skin": "slImage0ie17d400557f45",
                "src": "konybg.png",
                "top": "219dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxKonyLogo.add(KonyImg, KonyBankLbl, Welcomelbl, konytextlogo, konybgimg);
            var flxLogin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "440dp",
                "id": "flxLogin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "350dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0i27a4037c45c43",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxLogin.setDefaultUnit(kony.flex.DP);
            var lblUserid = new kony.ui.Label({
                "id": "lblUserid",
                "isVisible": true,
                "left": "40dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.User_ID\")",
                "top": "62dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtUserName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40dp",
                "id": "txtUserName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "52dp",
                "secureTextEntry": false,
                "skin": "Skinuseridfield0bc733c4148544e",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "85dp",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "onBeginEditing": controller.AS_TextField_b9c2411768f14bf19f3784bfbd790d85
            });
            var lblPassword = new kony.ui.Label({
                "id": "lblPassword",
                "isVisible": true,
                "left": "40dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblPasswordUser\")",
                "top": "149dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtPassword = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40dp",
                "id": "txtPassword",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "52dp",
                "secureTextEntry": true,
                "skin": "skinPasswordLogin",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "174dp",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "onBeginEditing": controller.AS_TextField_b8175308d8424df495ecc3ca9d37b599
            });
            var chkbxRemember = new kony.ui.CheckBoxGroup({
                "height": "23px",
                "id": "chkbxRemember",
                "isVisible": false,
                "left": "42dp",
                "masterData": [
                    ["true", "Remember me"]
                ],
                "selectedKeyValues": [
                    ["true", "Remember me"]
                ],
                "selectedKeys": ["true"],
                "skin": "slCheckBoxGroup0h1860dff06d344",
                "top": "242dp",
                "width": "50%",
                "zIndex": 1
            }, {
                "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxForgotCredentials = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxForgotCredentials",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "hoverhandSkin2",
                "top": "364dp",
                "width": "200px"
            }, {}, {
                "hoverSkin": "hoverhandSkin2"
            });
            flxForgotCredentials.setDefaultUnit(kony.flex.DP);
            var lblForgotCredentials = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblForgotCredentials",
                "isVisible": true,
                "skin": "sknLblLato13px117eb0",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.flxForgotCredentials\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            flxForgotCredentials.add(lblForgotCredentials);
            var flxLoginRememberMe = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "56.59%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxLoginRememberMe",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "42dp",
                "isModalContainer": false,
                "top": "242dp",
                "width": "105px",
                "zIndex": 1
            }, {}, {});
            flxLoginRememberMe.setDefaultUnit(kony.flex.DP);
            var flxRememberMe = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRememberMe",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRememberMe.setDefaultUnit(kony.flex.DP);
            var imgLoginRememberMe = new kony.ui.Image2({
                "height": "100%",
                "id": "imgLoginRememberMe",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRememberMe.add(imgLoginRememberMe);
            var lblLoginRememberMe = new kony.ui.Label({
                "id": "lblLoginRememberMe",
                "isVisible": true,
                "left": "18dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblLoginRememberMe\")",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLoginRememberMe.add(flxRememberMe, lblLoginRememberMe);
            var btnImage = new kony.ui.Button({
                "focusSkin": "slButtonGlossRed0ac4572db3b9342",
                "height": "25dp",
                "id": "btnImage",
                "isVisible": false,
                "left": "250dp",
                "skin": "slButtonGlossBlue0a0c10b45f20d47",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "182dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEyeicon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeicon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "182dp",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyeicon.setDefaultUnit(kony.flex.DP);
            var lblEyeicon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": 25,
                "id": "lblEyeicon",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeicon.add(lblEyeicon);
            var lblUsernameErrorMsg = new kony.ui.Label({
                "id": "lblUsernameErrorMsg",
                "isVisible": false,
                "left": "40dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblUsernameErrorMsg\")",
                "top": "29%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPasswordErrorMsg = new kony.ui.Label({
                "id": "lblPasswordErrorMsg",
                "isVisible": false,
                "left": "40dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblPasswordErrorMsg\")",
                "top": "50%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEyecross = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyecross",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "182dp",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyecross.setDefaultUnit(kony.flex.DP);
            var lblEyecross = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "25dp",
                "id": "lblEyecross",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknEyeIconHover"
            });
            flxEyecross.add(lblEyecross);
            var btnLogin = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "height": "40dp",
                "id": "btnLogin",
                "isVisible": true,
                "left": "52dp",
                "onClick": controller.AS_Button_bf1990dfa2b74dfaa8871e0cd02dd961,
                "skin": "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.btnLogin\")",
                "top": "297dp",
                "width": "271dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px"
            });
            flxLogin.add(lblUserid, txtUserName, lblPassword, txtPassword, chkbxRemember, flxForgotCredentials, flxLoginRememberMe, btnImage, flxEyeicon, lblUsernameErrorMsg, lblPasswordErrorMsg, flxEyecross, btnLogin);
            var flxErrorAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "52dp",
                "id": "flxErrorAlert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "350dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0e26999f688114d",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxErrorAlert.setDefaultUnit(kony.flex.DP);
            var lblError = new kony.ui.Label({
                "height": "20dp",
                "id": "lblError",
                "isVisible": true,
                "left": "12dp",
                "skin": "sknIcon20pxWhite",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblError\")",
                "top": "15dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtError = new kony.ui.Label({
                "centerY": "50%",
                "id": "txtError",
                "isVisible": false,
                "left": "40dp",
                "right": "40px",
                "skin": "skinLabelError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.txtError\")",
                "top": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtError1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "txtError1",
                "isVisible": true,
                "left": "60dp",
                "skin": "skinLabelError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.txtError1\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorAlert.add(lblError, txtError, txtError1);
            flxLoginWrapper.add(flxKonyLogo, flxLogin, flxErrorAlert);
            var flxFooter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100px",
                "id": "flxFooter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "540dp",
                "width": "700px",
                "zIndex": 1
            }, {}, {});
            flxFooter.setDefaultUnit(kony.flex.DP);
            var lblFooterText = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50%",
                "id": "lblFooterText",
                "isVisible": true,
                "skin": "slLabel0i46e09deb8f34e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblFooterText\")",
                "width": "50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCopright = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCopright",
                "isVisible": true,
                "left": "30%",
                "skin": "lblLato14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblCopright\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFooter.add(lblFooterText, lblCopright);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxMainLogin.add(lblKony, flxTimeExpired, flxUnderlinelight, flxLoginWrapper, flxFooter, flxLoading);
            this.add(flxMainLogin);
        };
        return [{
            "addWidgets": addWidgetsfrmLogin,
            "enabledForIdleTimeout": false,
            "id": "frmLogin",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_d80f17429434469ba228dc2bf1d92f17(eventobject);
            },
            "skin": "sknfrm11"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_e4cb462ee4f54821a534ae29712c76a2,
            "retainScrollPosition": false
        }]
    }
});