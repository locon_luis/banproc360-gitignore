define(['ModelManager', 'permissionsConfig', 'WidgetConfig_FormExtn', 'WidgetConfigImporter'], function(ModelManager, permissionsConfig, configurationJSON, configurationImporter) {
    return {
        doLogin: function(context, onSuccess, onError) {
            var self = this;
            var authParams = {
                // "inputUsername": context.username,
                // "inputPassword": context.password,
                "userid": context.username,
                "password": context.password,
                "loginOptions": {
                    "isOfflineEnabled": false
                }
            };

            function successCallback(resSuccess) {
                //kony.mvc.MDAApplication.getSharedInstance().appContext.userID = KNYMobileFabric.getUserId();
                kony.print("successCallback" + JSON.stringify(resSuccess));
                var options = {
                    "IdentityServiceName": "KonyBankingAdminConsoleIdentityService",
                    "AuthParams": {
                        "loginOptions": {
                            "isOfflineEnabled": false
                        }
                    }
                };

                function profileSuccessCallback(profile) {
                    kony.print("Profle success" + JSON.stringify(profile));
                    context.firstName = profile.firstname;
                    context.lastName = profile.lastname;
                    self.doLoginC360(context, onSuccess, onError);
                }

                function profileErrorCallback(resTokenError) {
                    kony.print("tokenErrorCallback" + JSON.stringify(resTokenError));
                    onError(resTokenError);
                }
                try {
                    // authClient.getBackendToken(false, options, TokenSuccessCallback, tokenErrorCallback);
                    authClient.getProfile(false, profileSuccessCallback, profileErrorCallback);
                } catch (err) {
                    onError(err);
                }
            }

            function errorCallback(error) {
                onError(error);
            }
            try {
                console.log("The new one!")
                    // authClient = KNYMobileFabric.getIdentityService("KonyBankingAdminConsoleIdentityService");
                authClient = KNYMobileFabric.getIdentityService("AdminConsoleIdentityAD");
                authClient.login(authParams, successCallback, errorCallback);
            } catch (err) {
                kony.print(err);
                onError(err);
            }
        },
        doLoginC360: function(context, onSuccess, onError) {
            var self = this;
            var authParams = {
                "inputUsername": context.username,
                "inputPassword": context.password,
                "firstName": context.firstName,
                "lastName": context.lastName,
                "loginOptions": {
                    "isOfflineEnabled": false
                }
            };

            function successCallback(resSuccess) {
                //kony.mvc.MDAApplication.getSharedInstance().appContext.userID = KNYMobileFabric.getUserId();
                kony.print("successCallback" + JSON.stringify(resSuccess));
                var options = {
                    "IdentityServiceName": "KonyBankingIdentityAD",
                    "AuthParams": {
                        "loginOptions": {
                            "isOfflineEnabled": false
                        }
                    }
                };

                function TokenSuccessCallback(session_token) {
                    kony.print("TokenSuccessCallback" + JSON.stringify(session_token));
                    kony.mvc.MDAApplication.getSharedInstance().appContext.session_token = session_token.value;
                    kony.setUserID(context.username);
                    //self.getSecurityDetails(session_token, onSuccess, onError);
                    var permissionsString;
                    if (session_token.params && session_token.params.security_attributes && session_token.params.security_attributes.permissions) {
                        permissionsString = session_token.params.security_attributes.permissions;
                    } else {
                        onError("Unable to parse the permissions from indentity scope");
                    }
                    kony.mvc.MDAApplication.getSharedInstance().appContext.accessDetails = JSON.parse(permissionsString);
                    permissionsConfig.registerConfig(configurationJSON);
                    kony.mvc.MDAApplication.getSharedInstance().appContext.BASE_CURRENCY = kony.servicesapp.preferenceConfigHandler.getInstance().getPreferenceValue("BaseCurrency");
                    kony.mvc.MDAApplication.getSharedInstance().appContext.CURRENCY_SYMBOL_TO_USE = kony.servicesapp.preferenceConfigHandler.getInstance().getPreferenceValue("CurrencySymbolToUse");
                    self.getUserAttributes(session_token, onSuccess, onError);
                }

                function tokenErrorCallback(resTokenError) {
                    kony.print("tokenErrorCallback" + JSON.stringify(resTokenError));
                    onError(resTokenError);
                }
                try {
                    authClient.getBackendToken(false, options, TokenSuccessCallback, tokenErrorCallback);
                } catch (err) {
                    onError(err);
                }
            }

            function errorCallback(error) {
                onError(error);
            }
            try {
                authClient = KNYMobileFabric.getIdentityService("KonyBankingIdentityAD");
                authClient.login(authParams, successCallback, errorCallback);
            } catch (err) {
                kony.print(err);
                onError(err);
            }
        },
        doLogout: function(context, onSuccess, onError) {
            var self = this;
            var integrationClient;
            var serviceName = "CustomIdentity";
            var operationName = "logout";
            var params = {};
            // var headers = {
            //   session_token : kony.mvc.MDAApplication.getSharedInstance().appContext.session_token,
            //   "Content-Type":"application/json"
            // };
            var options = {
                "httpRequestOptions": {
                    "timeoutIntervalForRequest": 60,
                    "timeoutIntervalForResource": 600
                }
            };
            try {
                integrationClient = KNYMobileFabric.getIdentityService("KonyBankingIdentityAD");
                integrationClient.logout(function(result) {
                    kony.print("Integration Service Response is :" + JSON.stringify(result));
                    kony.mvc.MDAApplication.getSharedInstance().appContext.session_token = "";
                    //Remove all the active OLB CSR assist windows
                    var activeWindows = kony.mvc.MDAApplication.getSharedInstance().appContext.activeCSRAssistWindows;
                    if (activeWindows && activeWindows.length > 0) {
                        for (var count = 0; count < activeWindows.length; count++) {
                            activeWindows[count].windowObject.close();
                        }
                    }
                    //Remove all the active Loans CSR assist windows
                    activeWindows = kony.mvc.MDAApplication.getSharedInstance().appContext.activeLoansCSRAssistWindows;
                    if (activeWindows && activeWindows.length > 0) {
                        for (count = 0; count < activeWindows.length; count++) {
                            activeWindows[count].windowObject.close();
                        }
                    }
                    onSuccess();
                }, function(error) {
                    kony.print(error);
                    onError(error);
                }, options);
            } catch (exception) {
                onError(exception);
                kony.print("Exception" + exception.message);
            }
        }
    };
});