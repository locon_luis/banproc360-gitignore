define("AuthModule/frmErrorLogin", function() {
    return function(controller) {
        function addWidgetsfrmErrorLogin() {
            this.setDefaultUnit(kony.flex.DP);
            var frmErrorLoginWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "frmErrorLoginWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            frmErrorLoginWrapper.setDefaultUnit(kony.flex.DP);
            var flxMainError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "440px",
                "id": "flxMainError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "480dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0d1de3266f24048",
                "top": "120dp",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxMainError.setDefaultUnit(kony.flex.DP);
            var flxForgotPassword = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "440dp",
                "id": "flxForgotPassword",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxForgotPassword.setDefaultUnit(kony.flex.DP);
            var userid = new kony.ui.Label({
                "id": "userid",
                "isVisible": true,
                "left": "11%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")",
                "top": "100px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnResetPassword1 = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "slButtonGlossBlue0f445e8af7f3c49Hover",
                "height": "40dp",
                "id": "btnResetPassword1",
                "isVisible": true,
                "left": "36dp",
                "skin": "sknbtnPrimary",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.btnResetPassword\")",
                "top": "355px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "slButtonGlossBlue0f445e8af7f3c49Hover",
                "toolTip": "Reset password"
            });
            var Registererrormsg = new kony.ui.Label({
                "id": "Registererrormsg",
                "isVisible": false,
                "left": "11%",
                "skin": "sknLabelRed",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Registererrormsg\")",
                "top": "57%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var setpwdText = new kony.ui.Label({
                "centerX": "50%",
                "id": "setpwdText",
                "isVisible": true,
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblTimeForNewPassword\")",
                "top": "40px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtUserName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "id": "txtUserName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10%",
                "secureTextEntry": false,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "122dp",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var txtNewPassword1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "height": "40dp",
                "id": "txtNewPassword1",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10%",
                "secureTextEntry": true,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "207px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var useridlbl2 = new kony.ui.Label({
                "id": "useridlbl2",
                "isVisible": false,
                "left": "69dp",
                "skin": "slLabel0f437c33b952a41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.useridlbl2\")",
                "top": "126dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var useridtextboxKA = new kony.ui.Label({
                "centerX": "50%",
                "height": "40dp",
                "id": "useridtextboxKA",
                "isVisible": false,
                "left": "10%",
                "skin": "slLabel0g62ff74aee4b41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.useridtextboxKA\")",
                "top": "122px",
                "width": "271dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 3, 0, 0],
                "paddingInPixel": false
            }, {});
            var newpwdtext = new kony.ui.Label({
                "id": "newpwdtext",
                "isVisible": true,
                "left": "11%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblNewPassword\")",
                "top": "182px",
                "width": "50%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var reenterpwdtext = new kony.ui.Label({
                "id": "reenterpwdtext",
                "isVisible": true,
                "left": "11%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblReenterPassword\")",
                "top": "268px",
                "width": "80%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtReenterPassword1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "height": "40dp",
                "id": "txtReenterPassword1",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10%",
                "secureTextEntry": true,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "292dp",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var pwdrules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "170dp",
                "id": "pwdrules",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox0a30851fc80b24b",
                "top": "47%",
                "width": "90%",
                "zIndex": 10
            }, {}, {});
            pwdrules.setDefaultUnit(kony.flex.DP);
            var pwdheading = new kony.ui.Label({
                "id": "pwdheading",
                "isVisible": true,
                "left": "19dp",
                "skin": "slLabel0ff9cfa99545646",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesA\")",
                "top": "22dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var pwdlb2 = new kony.ui.Label({
                "id": "pwdlb2",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesB\")",
                "top": "62px",
                "width": "275px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var pwdlbl4 = new kony.ui.Label({
                "id": "pwdlbl4",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesC\")",
                "top": "118px",
                "width": "275px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var pwdlbl1 = new kony.ui.Label({
                "id": "pwdlbl1",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.pwdlbl1\")",
                "top": "42px",
                "width": "275px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var pwdlbl3 = new kony.ui.Label({
                "id": "pwdlbl3",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesE\")",
                "top": "82px",
                "width": "275px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var dot1 = new kony.ui.Label({
                "height": "5dp",
                "id": "dot1",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "48px",
                "width": "5px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var dot2 = new kony.ui.Label({
                "height": "5dp",
                "id": "dot2",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "68px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var dot3 = new kony.ui.Label({
                "height": "5dp",
                "id": "dot3",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "88px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var dot4 = new kony.ui.Label({
                "height": "5dp",
                "id": "dot4",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "123px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            pwdrules.add(pwdheading, pwdlb2, pwdlbl4, pwdlbl1, pwdlbl3, dot1, dot2, dot3, dot4);
            var Rulestxt = new kony.ui.RichText({
                "id": "Rulestxt",
                "isVisible": true,
                "right": "11%",
                "skin": "slRichText0f825e4c814004c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Rulestxt\")",
                "top": "182px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "linkFocusSkin": "slRichText0e25a156e33f94bHover"
            });
            var Image0d38ffe50455b45 = new kony.ui.Image2({
                "height": "15dp",
                "id": "Image0d38ffe50455b45",
                "isVisible": false,
                "left": "280dp",
                "skin": "slImage0e4c8240073b34d",
                "src": "arrow.png",
                "top": "190dp",
                "width": "15dp",
                "zIndex": 10
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFPToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "49.14%",
                "centerY": "4.62%",
                "clipBounds": true,
                "height": "9.09%",
                "id": "flxFPToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0e26999f688114d",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFPToastContainer.setDefaultUnit(kony.flex.DP);
            var lblFPtoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFPtoastMessage",
                "isVisible": true,
                "left": "3%",
                "skin": "lblfffffflatoregular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblFPtoastMessage\")",
                "top": "18%",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFPToastContainer.add(lblFPtoastMessage);
            var flxEyecrossFP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyecrossFP",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "215dp",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyecrossFP.setDefaultUnit(kony.flex.DP);
            var lblEyecrossFP = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "25dp",
                "id": "lblEyecrossFP",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknEyeIconHover"
            });
            flxEyecrossFP.add(lblEyecrossFP);
            var flxEyecrossFPReEnter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyecrossFPReEnter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "299dp",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyecrossFPReEnter.setDefaultUnit(kony.flex.DP);
            var lblEyecrossFPReEnter = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "25dp",
                "id": "lblEyecrossFPReEnter",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknEyeIconHover"
            });
            flxEyecrossFPReEnter.add(lblEyecrossFPReEnter);
            var flxEyeiconFP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeiconFP",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "215dp",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyeiconFP.setDefaultUnit(kony.flex.DP);
            var lblEyeiconFP = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": 25,
                "id": "lblEyeiconFP",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeiconFP.add(lblEyeiconFP);
            var flxEyeiconFPReEnter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeiconFPReEnter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "299dp",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyeiconFPReEnter.setDefaultUnit(kony.flex.DP);
            var lblEyeiconFPReEnter = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": 25,
                "id": "lblEyeiconFPReEnter",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeiconFPReEnter.add(lblEyeiconFPReEnter);
            flxForgotPassword.add(userid, btnResetPassword1, Registererrormsg, setpwdText, txtUserName, txtNewPassword1, useridlbl2, useridtextboxKA, newpwdtext, reenterpwdtext, txtReenterPassword1, pwdrules, Rulestxt, Image0d38ffe50455b45, flxFPToastContainer, flxEyecrossFP, flxEyecrossFPReEnter, flxEyeiconFP, flxEyeiconFPReEnter);
            var flxDownTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "440dp",
                "id": "flxDownTime",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxDownTime.setDefaultUnit(kony.flex.DP);
            var lblErrorMsg = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "lblErrorMsg",
                "isVisible": true,
                "left": "13%",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblErrorMsg\")",
                "top": "216px",
                "width": "65%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnRetry = new kony.ui.Button({
                "centerX": "50.00%",
                "focusSkin": "slButtonGlossBlue0f445e8af7f3c49Hover",
                "height": "40dp",
                "id": "btnRetry",
                "isVisible": true,
                "left": "36dp",
                "onClick": controller.AS_Button_j56af8bb73c74538a56fed0c025e0fe7,
                "skin": "sknbtnPrimary",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.btnRetry\")",
                "top": "296px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "slButtonGlossBlue0f445e8af7f3c49Hover",
                "toolTip": "Retry"
            });
            var imgErrorIcon = new kony.ui.Image2({
                "centerX": "50%",
                "height": "74px",
                "id": "imgErrorIcon",
                "isVisible": true,
                "left": "566dp",
                "skin": "slImage0e4c8240073b34d",
                "src": "timeerror.png",
                "top": "90dp",
                "width": "74px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCallSupport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCallSupport",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "366dp",
                "width": "246dp"
            }, {}, {});
            flxCallSupport.setDefaultUnit(kony.flex.DP);
            var lblCallsupport = new kony.ui.Label({
                "id": "lblCallsupport",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblCallsupport\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMobileno = new kony.ui.Label({
                "id": "lblMobileno",
                "isVisible": true,
                "left": "3px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblMobileno\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCallSupport.add(lblCallsupport, lblMobileno);
            flxDownTime.add(lblErrorMsg, btnRetry, imgErrorIcon, flxCallSupport);
            var flxChangePassword = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "440dp",
                "id": "flxChangePassword",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxChangePassword.setDefaultUnit(kony.flex.DP);
            var lblTimeForNewPassword = new kony.ui.Label({
                "centerX": "50.00%",
                "id": "lblTimeForNewPassword",
                "isVisible": true,
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblTimeForNewPassword\")",
                "top": "51px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPasswordExpired = new kony.ui.Label({
                "centerX": "50.00%",
                "id": "lblPasswordExpired",
                "isVisible": true,
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblPasswordExpired\")",
                "top": "74px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCurrentPassword = new kony.ui.Label({
                "id": "lblCurrentPassword",
                "isVisible": true,
                "left": "11%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblCurrentPassword\")",
                "top": "110px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtCurrentPassword = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40dp",
                "id": "txtCurrentPassword",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "onTextChange": controller.AS_TextField_c0cd55f145e445c28c87d89d7c3d11be,
                "secureTextEntry": true,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "131px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblIncorrectCurrentPassword = new kony.ui.Label({
                "id": "lblIncorrectCurrentPassword",
                "isVisible": false,
                "left": "11%",
                "skin": "sknLabelRed",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblIncorrectCurrentPassword\")",
                "top": "171px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNewPassword = new kony.ui.Label({
                "id": "lblNewPassword",
                "isVisible": true,
                "left": "11%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblNewPassword\")",
                "top": "190px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtNewPassword = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40dp",
                "id": "txtNewPassword",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "onTextChange": controller.AS_TextField_e04eb0dbcd444b0f88e9220c88d473c8,
                "secureTextEntry": true,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "210px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "onBeginEditing": controller.AS_TextField_cd8722ebe6464241a6bd61cdf2fff02c,
                "onEndEditing": controller.AS_TextField_jeddfdb3bde64867aa1c02b4247462fa
            });
            var lblPasswordNotValid = new kony.ui.Label({
                "id": "lblPasswordNotValid",
                "isVisible": false,
                "left": "11%",
                "skin": "sknLabelRed",
                "text": "Password not valid",
                "top": "250px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblReenterPassword = new kony.ui.Label({
                "id": "lblReenterPassword",
                "isVisible": true,
                "left": "11%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblReenterPassword\")",
                "top": "270px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtReenterPassword = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40dp",
                "id": "txtReenterPassword",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "onTextChange": controller.AS_TextField_ia26aac89ec7412589c844d50bfd780d,
                "secureTextEntry": true,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "290px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "onBeginEditing": controller.AS_TextField_e0222c1fba7c47efac938499c9fc7494,
                "onEndEditing": controller.AS_TextField_e36efe970c2942f0968dc694a460ab9e
            });
            var lblPasswordDontMatch = new kony.ui.Label({
                "id": "lblPasswordDontMatch",
                "isVisible": false,
                "left": "11%",
                "skin": "sknLabelRed",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblPasswordDontMatch\")",
                "top": "332px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "170px",
                "id": "flxRules",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox0a30851fc80b24b",
                "top": "47%",
                "width": "90%",
                "zIndex": 10
            }, {}, {});
            flxRules.setDefaultUnit(kony.flex.DP);
            var lblRulesA = new kony.ui.Label({
                "id": "lblRulesA",
                "isVisible": true,
                "left": "19dp",
                "skin": "slLabel0ff9cfa99545646",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesA\")",
                "top": "22dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesB = new kony.ui.Label({
                "id": "lblRulesB",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesB\")",
                "top": "62px",
                "width": "275px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesC = new kony.ui.Label({
                "id": "lblRulesC",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesC\")",
                "top": "118px",
                "width": "275px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesD = new kony.ui.Label({
                "id": "lblRulesD",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesD\")",
                "top": "42px",
                "width": "275px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesE = new kony.ui.Label({
                "id": "lblRulesE",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesE\")",
                "top": "82px",
                "width": "275px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesF = new kony.ui.Label({
                "height": "5dp",
                "id": "lblRulesF",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "48px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesG = new kony.ui.Label({
                "height": "5dp",
                "id": "lblRulesG",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "68px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesH = new kony.ui.Label({
                "height": "5dp",
                "id": "lblRulesH",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "88px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesI = new kony.ui.Label({
                "height": "5dp",
                "id": "lblRulesI",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "123px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRules.add(lblRulesA, lblRulesB, lblRulesC, lblRulesD, lblRulesE, lblRulesF, lblRulesG, lblRulesH, lblRulesI);
            var btnResetPassword = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "slButtonGlossBlue0f445e8af7f3c49Hover",
                "height": "40dp",
                "id": "btnResetPassword",
                "isVisible": true,
                "onClick": controller.AS_Button_db01e0af504849298743c29397e3464d,
                "skin": "sknbtnPrimary",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.btnResetPassword\")",
                "top": "350dp",
                "width": "271dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "slButtonGlossBlue0f445e8af7f3c49Hover",
                "toolTip": "Reset password"
            });
            var btnCancel = new kony.ui.Button({
                "centerX": "50.00%",
                "focusSkin": "slButtonGlossRed0cad9addff35240",
                "height": "40dp",
                "id": "btnCancel",
                "isVisible": false,
                "left": "36dp",
                "onClick": controller.AS_Button_fd2d4688e7114c4cb16c25d02a266c33,
                "skin": "btnSkinBlueEnabled",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "400dp",
                "width": "271dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Reset password"
            });
            var flxCancelText = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxCancelText",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "hoverhandSkin2",
                "top": "405px",
                "width": "60px",
                "zIndex": 10
            }, {}, {
                "hoverSkin": "hoverhandSkin2"
            });
            flxCancelText.setDefaultUnit(kony.flex.DP);
            var lblCancelText = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblCancelText",
                "isVisible": true,
                "onTouchStart": controller.AS_Label_j53a331fdcaf4816ab30f8f05ffce42c,
                "skin": "sknlblLatoRegularBlue13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCancelText.add(lblCancelText);
            var flxToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "49.14%",
                "centerY": "4.62%",
                "clipBounds": true,
                "height": "9.09%",
                "id": "flxToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0e26999f688114d",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxToastContainer.setDefaultUnit(kony.flex.DP);
            var lbltoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbltoastMessage",
                "isVisible": true,
                "left": "3%",
                "skin": "lblfffffflatoregular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lbltoastMessage\")",
                "top": "18%",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToastContainer.add(lbltoastMessage);
            var flxRichTextRules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxRichTextRules",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "190dp",
                "width": 50,
                "zIndex": 10
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRichTextRules.setDefaultUnit(kony.flex.DP);
            var lblRules = new kony.ui.RichText({
                "id": "lblRules",
                "isVisible": true,
                "onClick": controller.AS_RichText_bd35a9814af54de48c78a578bdfcf027,
                "right": "0%",
                "skin": "slRichText0f825e4c814004c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Rulestxt\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRichTextRules.add(lblRules);
            var flxEyeiconCPCurrent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeiconCPCurrent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "139dp",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyeiconCPCurrent.setDefaultUnit(kony.flex.DP);
            var lblEyeiconCPCurrent = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": 25,
                "id": "lblEyeiconCPCurrent",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeiconCPCurrent.add(lblEyeiconCPCurrent);
            var flxEyecrossCPCurrent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyecrossCPCurrent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "139dp",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyecrossCPCurrent.setDefaultUnit(kony.flex.DP);
            var lblEyecrossCPCurrent = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "25dp",
                "id": "lblEyecrossCPCurrent",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknEyeIconHover"
            });
            flxEyecrossCPCurrent.add(lblEyecrossCPCurrent);
            var flxEyeiconCPNew = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeiconCPNew",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "217dp",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyeiconCPNew.setDefaultUnit(kony.flex.DP);
            var lblEyeiconCPNew = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": 25,
                "id": "lblEyeiconCPNew",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeiconCPNew.add(lblEyeiconCPNew);
            var flxEyecrossCPNew = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyecrossCPNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "217dp",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyecrossCPNew.setDefaultUnit(kony.flex.DP);
            var lblEyecrossCPNew = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "25dp",
                "id": "lblEyecrossCPNew",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknEyeIconHover"
            });
            flxEyecrossCPNew.add(lblEyecrossCPNew);
            var flxEyeiconCPReEnter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeiconCPReEnter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "298dp",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyeiconCPReEnter.setDefaultUnit(kony.flex.DP);
            var lblEyeiconCPReEnter = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": 25,
                "id": "lblEyeiconCPReEnter",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeiconCPReEnter.add(lblEyeiconCPReEnter);
            var flxEyecrossCPReEnter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyecrossCPReEnter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "297dp",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyecrossCPReEnter.setDefaultUnit(kony.flex.DP);
            var lblEyecrossCPReEnter = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "25dp",
                "id": "lblEyecrossCPReEnter",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknEyeIconHover"
            });
            flxEyecrossCPReEnter.add(lblEyecrossCPReEnter);
            flxChangePassword.add(lblTimeForNewPassword, lblPasswordExpired, lblCurrentPassword, txtCurrentPassword, lblIncorrectCurrentPassword, lblNewPassword, txtNewPassword, lblPasswordNotValid, lblReenterPassword, txtReenterPassword, lblPasswordDontMatch, flxRules, btnResetPassword, btnCancel, flxCancelText, flxToastContainer, flxRichTextRules, flxEyeiconCPCurrent, flxEyecrossCPCurrent, flxEyeiconCPNew, flxEyecrossCPNew, flxEyeiconCPReEnter, flxEyecrossCPReEnter);
            var flxForgotEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "440dp",
                "id": "flxForgotEmail",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxForgotEmail.setDefaultUnit(kony.flex.DP);
            var registertextbox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40dp",
                "id": "registertextbox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "35dp",
                "secureTextEntry": false,
                "skin": "skinPasswordLogin",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "180px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var ContinueButton = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "slButtonGlossBlue0f445e8af7f3c49Hover",
                "height": "40dp",
                "id": "ContinueButton",
                "isVisible": true,
                "left": "43dp",
                "onClick": controller.AS_Button_c373839f16154bbb9a1222ca80dbb1ef,
                "skin": "sknbtnPrimary",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.ContinueButton\")",
                "top": "250px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "slButtonGlossBlue0f445e8af7f3c49Hover",
                "toolTip": "CONTINUE"
            });
            var registerLbl = new kony.ui.Label({
                "height": "30px",
                "id": "registerLbl",
                "isVisible": true,
                "left": "40dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.registerLbl\")",
                "top": "152px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var registerErrorMsgTextbox = new kony.ui.Label({
                "id": "registerErrorMsgTextbox",
                "isVisible": false,
                "left": "40dp",
                "skin": "sknLabelRed",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Registererrormsg\")",
                "top": "51%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var ResetLInk = new kony.ui.Label({
                "centerX": "50%",
                "height": "30px",
                "id": "ResetLInk",
                "isVisible": true,
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.ResetLInk\")",
                "top": "84px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var underline = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "groupCells": false,
                "height": "13.89%",
                "id": "underline",
                "isVisible": false,
                "left": "10%",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg0a4ad1052cc5246",
                "rowSkin": "seg0a46ee49b8f994e",
                "sectionHeaderSkin": "sliPhoneSegmentHeader0c51fb80aad8644",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "d6c1d600",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "7%",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "width": "80%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var heading = new kony.ui.Label({
                "centerX": "50%",
                "height": "30px",
                "id": "heading",
                "isVisible": true,
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.heading\")",
                "top": "64dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "317dp",
                "width": "150px",
                "zIndex": 10
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCancel.setDefaultUnit(kony.flex.DP);
            var Canceltext = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "Canceltext",
                "isVisible": true,
                "onTouchStart": controller.AS_Label_eeaae02c2cad4778971282477141f8b7,
                "skin": "sknLblLato13px117eb0",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.BckToLoginPage\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCancel.add(Canceltext);
            var tbxnulltextboxKA = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "40dp",
                "id": "tbxnulltextboxKA",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "110dp",
                "secureTextEntry": false,
                "skin": "slTextBox0ccadf8e598664b",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "216dp",
                "width": "260dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var tbxwrongTextboxKA = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "40dp",
                "id": "tbxwrongTextboxKA",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "131dp",
                "secureTextEntry": false,
                "skin": "slTextBox0ccadf8e598664b",
                "text": "admin@konybank.com",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "215dp",
                "width": "260dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxForgotEmail.add(registertextbox, ContinueButton, registerLbl, registerErrorMsgTextbox, ResetLInk, underline, heading, flxCancel, tbxnulltextboxKA, tbxwrongTextboxKA);
            var flxMail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "440dp",
                "id": "flxMail",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxMail.setDefaultUnit(kony.flex.DP);
            var lbl1 = new kony.ui.Label({
                "id": "lbl1",
                "isVisible": true,
                "left": "10%",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lbl1\")",
                "top": "170px",
                "width": "25%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRegistererrormsgKA = new kony.ui.Label({
                "id": "lblRegistererrormsgKA",
                "isVisible": false,
                "left": "10%",
                "skin": "sknLabelRed",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Registererrormsg\")",
                "top": "53%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var verificationlnk = new kony.ui.Label({
                "centerX": "50%",
                "id": "verificationlnk",
                "isVisible": true,
                "left": "13%",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.verificationlnk\")",
                "top": "96px",
                "width": "80%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var SegunderlineKA = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "groupCells": false,
                "height": "13.89%",
                "id": "SegunderlineKA",
                "isVisible": false,
                "left": "10%",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg0a4ad1052cc5246",
                "rowSkin": "seg0a46ee49b8f994e",
                "sectionHeaderSkin": "sliPhoneSegmentHeader0c51fb80aad8644",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "d6c1d600",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "7%",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "width": "80%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var mailTxt = new kony.ui.Label({
                "centerX": "50%",
                "id": "mailTxt",
                "isVisible": true,
                "skin": "sknlblLatoRegular20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.mailTxt\")",
                "top": "64px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmail = new kony.ui.Label({
                "id": "lblEmail",
                "isVisible": true,
                "left": "10%",
                "skin": "sknlblLatoBold0d5b3368f619940",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblEmail\")",
                "top": "194px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbl3 = new kony.ui.Label({
                "id": "lbl3",
                "isVisible": false,
                "left": "10%",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lbl3\")",
                "top": "52%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbl4 = new kony.ui.Label({
                "centerX": "50%",
                "id": "lbl4",
                "isVisible": true,
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lbl4\")",
                "top": "320px",
                "width": "100%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbl5 = new kony.ui.Label({
                "centerX": "50%",
                "id": "lbl5",
                "isVisible": false,
                "skin": "sknlblLatoRegularBlue13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.contacttext\")",
                "top": "340px",
                "width": "100%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbl485c7513pxHoverCursor"
            });
            var Resendlbl = new kony.ui.RichText({
                "id": "Resendlbl",
                "isVisible": true,
                "right": "11%",
                "skin": "slRichText0f825e4c814004c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Resend\")",
                "top": "170px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "linkFocusSkin": "slRichText0e25a156e33f94bHover"
            });
            var underline3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "underline3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25dp",
                "isModalContainer": false,
                "skin": "underlinelight0h40367f3fcfe4d",
                "top": "300px",
                "width": "280dp",
                "zIndex": 1
            }, {}, {});
            underline3.setDefaultUnit(kony.flex.DP);
            underline3.add();
            var flxCallSupport2KA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCallSupport2KA",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "375dp",
                "width": "246dp"
            }, {}, {});
            flxCallSupport2KA.setDefaultUnit(kony.flex.DP);
            var lblOrCallSupport2KA = new kony.ui.Label({
                "id": "lblOrCallSupport2KA",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblCallsupport\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMobileNumber2KA = new kony.ui.Label({
                "id": "lblMobileNumber2KA",
                "isVisible": true,
                "left": "3px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblMobileno\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCallSupport2KA.add(lblOrCallSupport2KA, lblMobileNumber2KA);
            flxMail.add(lbl1, lblRegistererrormsgKA, verificationlnk, SegunderlineKA, mailTxt, lblEmail, lbl3, lbl4, lbl5, Resendlbl, underline3, flxCallSupport2KA);
            var flxExcededAttempts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "440dp",
                "id": "flxExcededAttempts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxExcededAttempts.setDefaultUnit(kony.flex.DP);
            var successmsg = new kony.ui.Label({
                "centerX": "50%",
                "id": "successmsg",
                "isVisible": true,
                "left": "13%",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.successmsg\")",
                "top": "184px",
                "width": "60%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var underlinelight = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "underlinelight",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25dp",
                "isModalContainer": false,
                "skin": "underlinelight0h40367f3fcfe4d",
                "top": "300px",
                "width": "280dp",
                "zIndex": 1
            }, {}, {});
            underlinelight.setDefaultUnit(kony.flex.DP);
            underlinelight.add();
            var contacttext = new kony.ui.Label({
                "centerX": "50%",
                "id": "contacttext",
                "isVisible": true,
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.contacttext\")",
                "top": "330dp",
                "width": "100%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var error2img = new kony.ui.Image2({
                "centerX": "50%",
                "height": "59px",
                "id": "error2img",
                "isVisible": true,
                "left": "92dp",
                "skin": "slImage0e4c8240073b34d",
                "src": "error2.png",
                "top": "100px",
                "width": "139px",
                "zIndex": 10
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCallSupportKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCallSupportKA",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "360dp",
                "width": "246dp"
            }, {}, {});
            flxCallSupportKA.setDefaultUnit(kony.flex.DP);
            var lblcallsupportKA = new kony.ui.Label({
                "id": "lblcallsupportKA",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblCallsupport\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblmobilenoKA = new kony.ui.Label({
                "id": "lblmobilenoKA",
                "isVisible": true,
                "left": "3px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblMobileno\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCallSupportKA.add(lblcallsupportKA, lblmobilenoKA);
            flxExcededAttempts.add(successmsg, underlinelight, contacttext, error2img, flxCallSupportKA);
            var flxResetPasswordConfirm = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "440dp",
                "id": "flxResetPasswordConfirm",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxResetPasswordConfirm.setDefaultUnit(kony.flex.DP);
            var imgSuccess = new kony.ui.Image2({
                "centerX": "50%",
                "height": "58dp",
                "id": "imgSuccess",
                "isVisible": true,
                "left": "86dp",
                "skin": "slImage0e4c8240073b34d",
                "src": "success1.png",
                "top": "111dp",
                "width": "142dp",
                "zIndex": 20
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSuccessMessage = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblSuccessMessage",
                "isVisible": true,
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblSuccessMessage\")",
                "top": "197px",
                "width": "100%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnRelogin = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "slButtonGlossBlue0f445e8af7f3c49Hover",
                "height": "40dp",
                "id": "btnRelogin",
                "isVisible": true,
                "left": "36dp",
                "onClick": controller.AS_Button_fcd23acf92dc4eb48b87832870f60bcc,
                "skin": "sknbtnPrimary",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.btnRelogin\")",
                "top": "320dp",
                "width": "271dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "slButtonGlossBlue0f445e8af7f3c49Hover",
                "toolTip": "Re-Sign in"
            });
            flxResetPasswordConfirm.add(imgSuccess, lblSuccessMessage, btnRelogin);
            flxMainError.add(flxForgotPassword, flxDownTime, flxChangePassword, flxForgotEmail, flxMail, flxExcededAttempts, flxResetPasswordConfirm);
            var flxFooter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100px",
                "id": "flxFooter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "570dp",
                "width": "700px",
                "zIndex": 1
            }, {}, {});
            flxFooter.setDefaultUnit(kony.flex.DP);
            var lblFooterText = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50%",
                "id": "lblFooterText",
                "isVisible": true,
                "skin": "slLabel0i46e09deb8f34e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblFooterText\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCopright = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCopright",
                "isVisible": true,
                "left": "30%",
                "skin": "lblLato14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblCopright\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFooter.add(lblFooterText, lblCopright);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0d1de3266f24048",
                "top": 10,
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var Konysmalltxt = new kony.ui.Label({
                "id": "Konysmalltxt",
                "isVisible": false,
                "left": "100dp",
                "skin": "slLabel0bffd884822a345",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Konysmalltxt\")",
                "top": "70dp",
                "width": "45%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var konyImgKA = new kony.ui.Image2({
                "height": "50dp",
                "id": "konyImgKA",
                "isVisible": true,
                "left": "100dp",
                "skin": "slImage0e4c8240073b34d",
                "src": "konydbxlogo.png",
                "top": "30dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeader.add(Konysmalltxt, konyImgKA);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            frmErrorLoginWrapper.add(flxMainError, flxFooter, flxHeader, flxLoading);
            this.add(frmErrorLoginWrapper);
        };
        return [{
            "addWidgets": addWidgetsfrmErrorLogin,
            "enabledForIdleTimeout": false,
            "id": "frmErrorLogin",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_a7af3565c840460f98fbc9638c2db7e1(eventobject);
            },
            "skin": "sknfrm11"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_g4cef829c2384a968122661209cb0326,
            "retainScrollPosition": false
        }]
    }
});