define("AuthModule/userfrmErrorLoginController", {
    /**
     * Function  to update UI
     * Parameters: context
     */
    getPresenter: function() {
        return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule").presentationController;
    },
    willUpdateUI: function(context) {
        if (context !== undefined) {
            if (context.action === "passwordExpired") {
                this.passwordExpiredUI();
            } else if (context.action === "resetPasswordConfirm") {
                this.resetPasswordConfirmUI();
            } else if (context.action === "changePassword") {
                this.changePasswordUI();
            } else if (context.action === "downTime") {
                this.downTimeUI();
            } else if (context.action === "exceddedAttempts") {
                this.exceddedAttemptsUI();
            } else if (context.action === "forgotEmail") {
                this.forgotEmailUI();
            } else if (context.action === "showIncorrectEmailFlex") {
                this.showIncorrectEmailFlexUI(context);
            } else if (context.action === "showEmailSentFlex") {
                this.showEmailSentFlexUI(context);
            } else if (context.action === "changePasswordForgotUser") {
                this.changePasswordForgotUserUI(context);
            }
        }
        this.view.forceLayout();
    },
    /**
     * Function preshow of form
     **/
    preshowLoginError: function() {
        this.preshowActions();
        this.view.txtCurrentPassword.text = "";
        this.view.txtNewPassword.text = "";
        this.view.registerErrorMsgTextbox.isVisible = false;
        this.view.registertextbox.skin = "skinPasswordTextField";
        this.view.registertextbox.text = "";
        this.view.txtReenterPassword.text = "";
        this.view.txtReenterPassword.text = "";
        this.view.txtNewPassword.text = "";
        this.view.txtCurrentPassword.text = "";
        this.view.txtNewPassword.skin = "skinPasswordTextField";
        this.view.txtCurrentPassword.skin = "skinPasswordTextField";
        this.view.txtReenterPassword.skin = "skinPasswordTextField";
        this.view.lblIncorrectCurrentPassword.isVisible = false;
        this.view.lblPasswordNotValid.isVisible = false;
        this.view.lblPasswordDontMatch.isVisible = false;
        this.view.txtNewPassword1.text = "";
        this.view.txtReenterPassword1.text = "";
        this.view.txtReenterPassword1.skin = "skinPasswordTextField";
        this.view.Registererrormsg.isVisible = false;
        this.view.pwdrules.setVisibility(false);
        this.view.flxRules.setVisibility(false);
    },
    /**
     * Function preshow actions
     **/
    preshowActions: function() {
        var scopeObj = this;
        this.view.btnResetPassword1.onClick = function() {
            scopeObj.resetPasswordForForgotPasswordCall();
        };
        this.view.registertextbox.onBeginEditing = function() {
            scopeObj.view.registerErrorMsgTextbox.isVisible = false;
            scopeObj.view.registertextbox.skin = "skinPasswordTextField";
        };
        this.view.txtUserName.onEndEditing = function() {
            scopeObj.validateUsernameForForgotPassword(scopeObj.view.txtUserName.text);
        };
        this.view.txtNewPassword1.onBeginEditing = function() {
            scopeObj.view.flxToastContainer.isVisible = false;
            scopeObj.view.btnResetPassword1.setEnabled(false);
            scopeObj.view.txtNewPassword1.skin = "btnSkinGrey";
            scopeObj.view.btnResetPassword1.skin = "btnSkinGrey";
            scopeObj.view.btnResetPassword1.hoverSkin = "btnSkinGrey";
            scopeObj.view.txtNewPassword1.skin = "skinPasswordTextField"; //normal skin
            scopeObj.view.Registererrormsg.isVisible = false;
            scopeObj.view.txtReenterPassword1.skin = "skinPasswordTextField";
        };
        this.view.txtNewPassword1.onKeyUp = function() {
            scopeObj.view.txtReenterPassword1.text = "";
            scopeObj.view.txtReenterPassword1.skin = "skinPasswordTextField"; //normal skin
            scopeObj.view.Registererrormsg.isVisible = false;
            scopeObj.validateNewPasswordWhileTypingForForgotPassword(scopeObj.view.txtNewPassword1.text);
        };
        this.view.txtNewPassword.onKeyUp = function() {
            scopeObj.view.txtReenterPassword.text = "";
            scopeObj.view.txtReenterPassword.skin = "skinPasswordTextField"; //normal skin
            scopeObj.view.lblPasswordDontMatch.isVisible = false;
            scopeObj.view.lblPasswordNotValid.setVisibility(false);
            scopeObj.validateNewPasswordWhileTypingForChangePassword(scopeObj.view.txtNewPassword.text);
        };
        this.view.txtNewPassword1.onEndEditing = function() {
            scopeObj.validateNewPasswordForForgotPassword(scopeObj.view.txtNewPassword1.text);
        };
        this.view.txtReenterPassword1.onBeginEditing = function() {
            scopeObj.view.flxToastContainer.isVisible = false;
            scopeObj.view.btnResetPassword1.setEnabled(false);
            scopeObj.view.btnResetPassword1.skin = "btnSkinGrey";
            scopeObj.view.btnResetPassword1.hoverSkin = "btnSkinGrey";
            scopeObj.view.txtReenterPassword1.skin = "skinPasswordTextField"; //normal skin
            scopeObj.view.Registererrormsg.isVisible = false;
        };
        this.view.txtReenterPassword1.onKeyUp = function() {
            scopeObj.validateReenterPasswordWhileTypingForForgotPassword(scopeObj.view.txtReenterPassword1.text);
        };
        this.view.txtReenterPassword.onKeyUp = function() {
            scopeObj.validateReenterPasswordWhileTypingForChangePassword(scopeObj.view.txtReenterPassword.text);
        };
        this.view.txtReenterPassword1.onEndEditing = function() {
            scopeObj.validateReenterPasswordForForgotPassword(scopeObj.view.txtReenterPassword1.text);
        };
        this.view.Rulestxt.onClick = function() {
            if (scopeObj.view.pwdrules.isVisible) {
                scopeObj.view.pwdrules.isVisible = false;
            } else {
                scopeObj.view.pwdrules.isVisible = true;
            }
        };
        this.view.txtCurrentPassword.onTextChange = function() {
            scopeObj.view.flxToastContainer.isVisible = false;
            if (scopeObj.view.txtCurrentPassword.text === "") {
                scopeObj.view.btnResetPassword.setEnabled(false);
                scopeObj.view.btnResetPassword.skin = "btnSkinGrey";
                scopeObj.view.btnResetPassword.hoverSkin = "btnSkinGrey";
            }
            if (scopeObj.view.txtCurrentPassword.text !== "" && scopeObj.view.txtNewPassword.text !== "" && scopeObj.view.txtReenterPassword.text !== "" && scopeObj.view.txtNewPassword.text === scopeObj.view.txtReenterPassword.text) {
                scopeObj.view.btnResetPassword.setEnabled(true);
                scopeObj.view.btnResetPassword.skin = "sknbtnPrimary";
                scopeObj.view.btnResetPassword.hoverSkin = "slButtonGlossBlue0f445e8af7f3c49Hover";
            }
        };
        this.view.txtCurrentPassword.onBeginEditing = function() {
            scopeObj.view.flxToastContainer.isVisible = false;
            scopeObj.view.txtCurrentPassword.skin = "skinPasswordTextField"; //normal skin
            scopeObj.view.lblIncorrectCurrentPassword.setVisibility(false);
        };
        this.view.txtNewPassword.onEndEditing = function() {
            scopeObj.validateNewPassword(scopeObj.view.txtNewPassword.text);
        };
        this.view.txtReenterPassword.onBeginEditing = function() {
            scopeObj.view.flxToastContainer.isVisible = false;
            scopeObj.view.btnResetPassword.setEnabled(false);
            scopeObj.view.btnResetPassword.skin = "btnSkinGrey";
            scopeObj.view.btnResetPassword.hoverSkin = "btnSkinGrey";
            scopeObj.view.txtReenterPassword.skin = "skinPasswordTextField"; //normal skin
            scopeObj.view.lblPasswordDontMatch.isVisible = false;
        };
        this.view.txtNewPassword.onTextChange = function() {
            scopeObj.validateNewPassword(scopeObj.view.txtNewPassword.text);
        };
        this.view.txtReenterPassword.onTextChange = function() {
            scopeObj.validateNewPassword(scopeObj.view.txtNewPassword.text);
            scopeObj.validateConfirmPassword(scopeObj.view.txtReenterPassword.text);
        };
        this.view.txtNewPassword.onBeginEditing = function() {
            scopeObj.view.flxToastContainer.isVisible = false;
            scopeObj.view.btnResetPassword.setEnabled(false);
            scopeObj.view.btnResetPassword.skin = "btnSkinGrey";
            scopeObj.view.btnResetPassword.hoverSkin = "btnSkinGrey";
            scopeObj.view.txtNewPassword.skin = "skinPasswordTextField"; //normal skin
            scopeObj.view.lblPasswordNotValid.setVisibility(false);
        };
        this.view.txtReenterPassword.onEndEditing = function() {
            scopeObj.validateConfirmPassword(scopeObj.view.txtReenterPassword.text);
        };
        this.view.lblRules.onClick = function() {
            if (scopeObj.view.flxRules.isVisible) {
                scopeObj.view.flxRules.isVisible = false;
            } else {
                scopeObj.view.flxRules.isVisible = true;
            }
        };
        this.view.btnResetPassword.onClick = function() {
            scopeObj.resetPasswordCall();
        };
        this.view.flxCancelText.onClick = function() {
            scopeObj.cancelResetPassword();
        };
        this.view.flxCancel.onClick = function() {
            scopeObj.showLoginPage();
        };
        this.view.ContinueButton.onClick = function() {
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            var userEmailId = scopeObj.view.registertextbox.text;
            var emailCheck = (/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(userEmailId));
            if (emailCheck === true) {
                scopeObj.view.registerErrorMsgTextbox.isVisible = false;
                kony.adminConsole.utils.showProgressBar(scopeObj.view);
                authModule.presentationController.sendResetPasswordEmail(scopeObj, {
                    "emailType": "resetPassword",
                    "emailId": userEmailId
                });
            } else {
                scopeObj.view.registerErrorMsgTextbox.isVisible = true;
                scopeObj.view.registerErrorMsgTextbox.text = kony.i18n.getLocalizedString("i18n.frmErrorLogin.Registererrormsg");
                scopeObj.view.registertextbox.skin = "skinredbg";
            }
        };
        this.view.btnRelogin.onClick = function() {
            scopeObj.showLoginPage();
        };
        this.view.Resendlbl.onClick = function() {
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            var userEmailId = scopeObj.view.lblEmail.text;
            scopeObj.view.Resendlbl.isVisible = false;
            kony.adminConsole.utils.showProgressBar(scopeObj.view);
            authModule.presentationController.sendResetPasswordEmail(scopeObj, {
                "emailType": "resetPassword",
                "emailId": userEmailId
            });
        };
        this.view.btnRetry.onClick = function() {
            scopeObj.showLoginPage();
        };
        this.view.flxEyeiconCPCurrent.onClick = function() {
            scopeObj.maskText("lblIncorrectCurrentPassword", "txtCurrentPassword", "flxEyeiconCPCurrent", "flxEyecrossCPCurrent");
        };
        this.view.flxEyecrossCPCurrent.onClick = function() {
            scopeObj.maskText("lblIncorrectCurrentPassword", "txtCurrentPassword", "flxEyeiconCPCurrent", "flxEyecrossCPCurrent");
        };
        this.view.flxEyeiconCPNew.onClick = function() {
            scopeObj.maskText("lblPasswordNotValid", "txtNewPassword", "flxEyeiconCPNew", "flxEyecrossCPNew");
        };
        this.view.flxEyecrossCPNew.onClick = function() {
            scopeObj.maskText("lblPasswordNotValid", "txtNewPassword", "flxEyeiconCPNew", "flxEyecrossCPNew");
        };
        this.view.flxEyeiconCPReEnter.onClick = function() {
            scopeObj.maskText("lblPasswordDontMatch", "txtReenterPassword", "flxEyeiconCPReEnter", "flxEyecrossCPReEnter");
        };
        this.view.flxEyecrossCPReEnter.onClick = function() {
            scopeObj.maskText("lblPasswordDontMatch", "txtReenterPassword", "flxEyeiconCPReEnter", "flxEyecrossCPReEnter");
        };
        this.view.forceLayout();
    },
    showLoginPage: function() {
        var scopeObj = this;
        scopeObj.getPresenter().showLoginScreen({
            "action": "login"
        });
    },
    /**
     * Function  to update UI for showIncorrectEmailFlexUI
     */
    showIncorrectEmailFlexUI: function(context) {
        kony.adminConsole.utils.hideProgressBar(this.view);
        this.view.flxMail.isVisible = false;
        if (context.emailMessage === "emailNotPresentInDB") {
            this.view.flxDownTime.isVisible = false;
            this.view.flxForgotEmail.isVisible = true;
            this.view.registerErrorMsgTextbox.text = kony.i18n.getLocalizedString("i18n.frmErrorLogin.email_not_found_error_msg");
            this.view.registerErrorMsgTextbox.isVisible = true;
            this.view.registertextbox.skin = "skinredbg";
        } else {
            this.view.flxForgotEmail.isVisible = false;
            this.view.flxDownTime.isVisible = true;
        }
    },
    /**
     * Function  to update UI for showEmailSentFlexUI
     */
    showEmailSentFlexUI: function(context) {
        kony.adminConsole.utils.hideProgressBar(this.view);
        this.view.flxForgotEmail.isVisible = false;
        this.view.flxMail.isVisible = true;
        this.view.lblEmail.text = context.emailId;
    },
    /**
     * Function  to update UI for changePasswordForgotUser
     */
    changePasswordForgotUserUI: function() {
        kony.adminConsole.utils.hideProgressBar(this.view);
        this.view.txtUserName.text = context.username;
        this.view.txtUserName.setEnabled(false);
        this.view.flxChangePassword.isVisible = false;
        this.view.flxMail.isVisible = false;
        this.view.flxResetPasswordConfirm.isVisible = false;
        this.view.flxExcededAttempts.isVisible = false;
        this.view.flxForgotPassword.isVisible = true;
        this.view.flxForgotEmail.isVisible = false;
        this.view.btnResetPassword1.setEnabled(false);
        this.view.btnResetPassword1.skin = "btnSkinGrey";
        this.view.btnResetPassword1.hoverSkin = "btnSkinGrey";
    },
    /**
     * Function  to update UI for reset Password Confirm
     */
    resetPasswordConfirmUI: function() {
        this.view.flxChangePassword.isVisible = false;
        this.view.flxMail.isVisible = false;
        this.view.flxResetPasswordConfirm.isVisible = true;
        this.view.flxExcededAttempts.isVisible = false;
        this.view.flxForgotPassword.isVisible = false;
        this.view.flxForgotEmail.isVisible = false;
        this.view.flxDownTime.isVisible = false;
    },
    /**
     * Function  to update UI when password expired
     */
    passwordExpiredUI: function() {
        this.view.flxChangePassword.isVisible = true;
        this.view.flxMail.isVisible = false;
        this.view.btnResetPassword.setEnabled(false);
        this.view.btnResetPassword.skin = "btnSkinGrey";
        this.view.btnResetPassword.hoverSkin = "btnSkinGrey";
        this.view.flxExcededAttempts.isVisible = false;
        this.view.flxForgotPassword.isVisible = false;
        this.view.flxForgotEmail.isVisible = false;
        this.view.flxDownTime.isVisible = false;
        this.view.flxResetPasswordConfirm.isVisible = false;
        this.view.btnResetPassword.text = kony.i18n.getLocalizedString("i18n.frmErrorLogin.btnResetPassword");
    },
    /**
     * Function  to update UI when change Password is called
     */
    changePasswordUI: function() {
        this.view.flxChangePassword.isVisible = true;
        this.view.flxForgotPassword.isVisible = false;
        this.view.flxMail.isVisible = false;
        this.view.btnResetPassword.setEnabled(false);
        this.view.btnResetPassword.skin = "btnSkinGrey";
        this.view.btnResetPassword.hoverSkin = "btnSkinGrey";
        this.view.flxResetPasswordConfirm.isVisible = false;
        this.view.flxExcededAttempts.isVisible = false;
        this.view.flxChangePassword.lblPasswordExpired.isVisible = false;
        this.view.flxChangePassword.lblTimeForNewPassword.text = kony.i18n.getLocalizedString("i18n.frmErrorLogin.lblTimeForNewPassword");
        this.view.flxChangePassword.isVisible = true;
        this.view.flxForgotEmail.isVisible = false;
        this.view.flxToastContainer.isVisible = false;
        this.view.btnResetPassword.text = kony.i18n.getLocalizedString("i18n.frmErrorLogin.btnSetPassword");
    },
    /**
     * Function  to update UI for downtime page
     */
    downTimeUI: function() {
        this.view.flxChangePassword.isVisible = false;
        this.view.flxMail.isVisible = false;
        this.view.flxExcededAttempts.isVisible = false;
        this.view.flxForgotPassword.isVisible = false;
        this.view.flxForgotEmail.isVisible = false;
        this.view.flxDownTime.isVisible = true;
        this.view.flxResetPasswordConfirm.isVisible = false;
    },
    /**
     * Function  to update UI when excedded Attempts error page
     */
    exceddedAttemptsUI: function() {
        this.view.flxChangePassword.isVisible = false;
        this.view.flxMail.isVisible = false;
        this.view.flxExcededAttempts.isVisible = true;
        this.view.flxForgotPassword.isVisible = false;
        this.view.flxForgotEmail.isVisible = false;
        this.view.flxDownTime.isVisible = false;
        this.view.flxResetPasswordConfirm.isVisible = false;
    },
    /**
     * Function  to update UI for forgot Email page
     */
    forgotEmailUI: function() {
        this.view.flxChangePassword.isVisible = false;
        this.view.flxMail.isVisible = false;
        this.view.flxExcededAttempts.isVisible = false;
        this.view.flxForgotPassword.isVisible = false;
        this.view.flxForgotEmail.isVisible = true;
        this.view.flxDownTime.isVisible = false;
        this.view.flxResetPasswordConfirm.isVisible = false;
    },
    /**
     * Function  to update UI for reset Password Failure page
     */
    resetPasswordFailureUI: function(response) {
        this.view.flxChangePassword.isVisible = true;
        if (response === "Incorrect current password") {
            this.view.lblIncorrectCurrentPassword.setVisibility(true);
            this.view.txtCurrentPassword.skin = "skinredbg";
        } else if (response === "Current password & new password are same") {
            this.view.lblPasswordNotValid.setVisibility(true);
            this.view.lblPasswordNotValid.text = "Current & new password cannot be similar";
            this.view.txtNewPassword.skin = "skinredbg";
        } else if (response === "New password fails criteria") {
            this.view.lblPasswordNotValid.setVisibility(true);
            this.view.lblPasswordNotValid.text = "Password not valid";
            this.view.txtNewPassword.skin = "skinredbg";
        } else {
            this.view.flxToastContainer.isVisible = true;
        }
        this.view.flxMail.isVisible = false;
        this.view.txtReenterPassword.text = "";
        this.view.txtNewPassword.text = "";
        this.view.txtCurrentPassword.text = "";
        this.view.btnResetPassword.setEnabled(false);
        this.view.btnResetPassword.skin = "btnSkinGrey";
        this.view.btnResetPassword.hoverSkin = "btnSkinGrey";
        this.view.forceLayout();
    },
    /**
     * Function to update UI for Reset Password for Forgot Password Mismatch or Failed Criteria page
     */
    resetPasswordForForgotPasswordMismatchOrFailedCriteriaUI: function(errmsg) {
        this.view.txtUserName.text = context.username;
        this.view.txtUserName.setEnabled(false);
        this.view.flxChangePassword.isVisible = false;
        this.view.flxMail.isVisible = false;
        this.view.flxResetPasswordConfirm.isVisible = false;
        this.view.flxExcededAttempts.isVisible = false;
        this.view.flxForgotEmail.isVisible = false;
        this.view.flxForgotPassword.isVisible = true;
        this.view.Registererrormsg.isVisible = true;
        if (errmsg === "New password & confirm password don't match") {
            this.view.Registererrormsg.text = "Passwords don't match";
        } else if (errmsg === "New password fails criteria") {
            this.view.Registererrormsg.text = "New password does not meet criteria";
        }
        this.view.txtNewPassword1.skin = "skinredbg";
        this.view.txtReenterPassword1.skin = "skinredbg";
        this.view.btnResetPassword1.setEnabled(false);
        this.view.btnResetPassword1.skin = "btnSkinGrey";
        this.view.btnResetPassword1.hoverSkin = "btnSkinGrey";
        this.view.forceLayout();
    },
    /**
     * Function to update UI for Reset Password for Forgot Password Failure page
     */
    resetPasswordForForgotPasswordFailureUI: function(errmsg) {
        this.view.txtUserName.text = context.username;
        this.view.txtUserName.setEnabled(false);
        this.view.flxChangePassword.isVisible = false;
        this.view.flxMail.isVisible = false;
        this.view.flxResetPasswordConfirm.isVisible = false;
        this.view.flxExcededAttempts.isVisible = false;
        this.view.flxForgotEmail.isVisible = false;
        this.view.flxForgotPassword.isVisible = true;
        this.view.flxFPToastContainer.isVisible = true;
        if (errmsg === "Invalid reset password request") {
            this.view.lblFPtoastMessage.text = "Invalid reset password request";
        } else {
            this.view.lblFPtoastMessage.text = "FAILURE";
        }
        this.view.txtNewPassword1.skin = "skinredbg";
        this.view.txtReenterPassword1.skin = "skinredbg";
        this.view.btnResetPassword1.setEnabled(false);
        this.view.btnResetPassword1.skin = "btnSkinGrey";
        this.view.btnResetPassword1.hoverSkin = "btnSkinGrey";
        this.view.forceLayout();
    },
    /**
     * Function to check if password match
     **/
    isPasswordValidAndMatchedWithReEnteredValue: function() {
        if (this.view.txtNewPassword.text && this.view.txtReenterPassword.text) {
            if (this.view.txtNewPassword.text === this.view.txtReenterPassword.text) {
                return true;
            }
        }
        return false;
    },
    /**
     * Function to check if current password & new password match
     **/
    isCurrentPasswordSimilarToNewPassword: function() {
        if (this.view.txtCurrentPassword.text && this.view.txtNewPassword.text && this.view.txtCurrentPassword.text === this.view.txtNewPassword.text) {
            return true;
        }
        return false;
    },
    /**
     * Function to check if password contains 9 consecutive digits
     **/
    consecutiveDigits: function(input) {
        var count = 9;
        var i = 0;
        for (i in input) {
            if (input[i] <= 0 && input[i] > 9) count--;
            else count = 9;
        }
        if (count === 0) return true;
        return false;
    },
    /**
     * Function to validate New Password
     **/
    validateNewPassword: function(enteredPassword) {
        if (this.isPasswordValid(enteredPassword)) {
            if (this.view.txtReenterPassword.text !== "" && !this.isCurrentPasswordSimilarToNewPassword() && this.isPasswordValidAndMatchedWithReEnteredValue()) {
                this.view.txtNewPassword.skin = "skinPasswordTextField"; //normal skin
                this.view.lblPasswordNotValid.text = '';
                this.view.btnResetPassword.setEnabled(true);
                this.view.btnResetPassword.skin = "sknbtnPrimary";
                this.view.btnResetPassword.hoverSkin = "slButtonGlossBlue0f445e8af7f3c49Hover";
            } else if (this.isCurrentPasswordSimilarToNewPassword()) {
                this.view.txtNewPassword.skin = "skinredbg";
                this.view.lblPasswordNotValid.setVisibility(true);
                this.view.lblPasswordNotValid.text = "Current & new password cannot be similar";
                this.view.btnResetPassword.setEnabled(false);
                this.view.btnResetPassword.skin = "btnSkinGrey";
                this.view.btnResetPassword.hoverSkin = "btnSkinGrey";
            } else {
                this.view.txtReenterPassword.skin = "skinredbg";
                this.view.lblPasswordDontMatch.isVisible = true;
                this.view.lblPasswordDontMatch.text = "Password don't match";
                this.view.btnResetPassword.setEnabled(false);
                this.view.btnResetPassword.skin = "btnSkinGrey";
                this.view.btnResetPassword.hoverSkin = "btnSkinGrey";
            }
        } else {
            this.view.txtNewPassword.skin = "skinredbg";
            this.view.lblPasswordNotValid.isVisible = true;
            this.view.lblPasswordNotValid.text = "Password not valid";
            this.view.btnResetPassword.setEnabled(false);
            this.view.btnResetPassword.skin = "btnSkinGrey";
            this.view.btnResetPassword.hoverSkin = "btnSkinGrey";
        }
        this.view.forceLayout();
    },
    /**
     * Function to validate Confirm Password
     */
    validateConfirmPassword: function(enteredPassword) {
        if (this.isPasswordValid(enteredPassword)) {
            if (!this.isCurrentPasswordSimilarToNewPassword() && this.isPasswordValidAndMatchedWithReEnteredValue()) {
                this.view.txtReenterPassword.skin = "skinPasswordTextField";
                this.view.lblPasswordDontMatch.isVisible = false;
                this.view.lblPasswordDontMatch.text = " ";
                this.view.btnResetPassword.setEnabled(true);
                this.view.btnResetPassword.skin = "sknbtnPrimary";
                this.view.btnResetPassword.hoverSkin = "slButtonGlossBlue0f445e8af7f3c49Hover";
            } else if (this.isCurrentPasswordSimilarToNewPassword()) {
                this.view.txtReenterPassword.skin = "skinPasswordTextField";
                this.view.lblPasswordDontMatch.isVisible = false;
                this.view.lblPasswordDontMatch.text = " ";
                this.view.btnResetPassword.setEnabled(false);
                this.view.btnResetPassword.skin = "btnSkinGrey";
                this.view.btnResetPassword.hoverSkin = "btnSkinGrey";
            } else {
                this.view.txtReenterPassword.skin = "skinredbg";
                this.view.lblPasswordDontMatch.isVisible = true;
                this.view.lblPasswordDontMatch.text = "Password don't match";
                this.view.btnResetPassword.setEnabled(false);
                this.view.btnResetPassword.skin = "btnSkinGrey";
                this.view.btnResetPassword.hoverSkin = "btnSkinGrey";
            }
        }
        this.view.forceLayout();
    },
    /**
     * password check between 8 to 20 characters which contain at least one lowercase letter, one uppercase letter,
     * one numeric digit, and one special character
     */
    isPasswordValid: function(enteredPassword) {
        var password = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,20}$/;
        var userName = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
        if (enteredPassword.match(password) && enteredPassword !== userName) {
            return true;
        }
        return false;
    },
    /**
     * reset password call
     */
    resetPasswordCall: function() {
        var userName = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
        var currentPassword = this.view.flxChangePassword.txtCurrentPassword.text;
        var newPassword = this.view.flxChangePassword.txtNewPassword.text;
        if (this.view.flxChangePassword.txtNewPassword.text !== "" && this.view.flxChangePassword.txtCurrentPassword.text !== "" && this.view.flxChangePassword.txtReenterPassword.text !== "") {
            kony.adminConsole.utils.showProgressBar(this.view);
            this.getPresenter().toResetPassword(userName, currentPassword, newPassword, this.toResetPasswordResponse);
        } else {
            this.view.txtNewPassword.skin = "skinPasswordTextField";
            this.view.txtCurrentPassword.skin = "skinPasswordTextField";
            this.view.txtReenterPassword.skin = "skinPasswordTextField";
            this.view.lblPasswordDontMatch.isVisible = true;
            this.view.lblPasswordDontMatch.text = "Fields cannot be empty";
        }
    },
    /**
     * function to show Reset Confirmation Screen
     * Parameters: form controller,response
     */
    toResetPasswordResponse: function(response) {
        kony.adminConsole.utils.hideProgressBar(this.view);
        kony.application.dismissLoadingScreen();
        var context;
        if (response === "Reset password successful") {
            context = {
                "action": "resetPasswordConfirm"
            };
            this.getPresenter().showErrorLoginScreen(context);
        } else if (response === "Session expired") {
            context = {
                "action": "SessionExpired"
            };
            this.getPresenter().showLoginScreen(context);
        } else {
            this.resetPasswordFailureUI(response);
        }
    },
    cancelResetPassword: function() {
        if (this.canNavigateBack()) {
            this.navigateBack();
        } else {
            this.getPresenter().showLoginScreen({
                "action": "login"
            });
        }
    },
    validateUsernameForForgotPassword: function(enteredUsername) {
        if (enteredUsername === "") {
            this.view.btnResetPassword1.setEnabled(false);
            this.view.btnResetPassword1.skin = "btnSkinGrey";
            this.view.btnResetPassword1.hoverSkin = "btnSkinGrey";
        }
        if (this.view.txtUserName.text !== "" && this.view.txtNewPassword1.text !== "" && this.view.txtReenterPassword1.text !== "") {
            this.view.btnResetPassword1.setEnabled(true);
            this.view.btnResetPassword1.skin = "sknbtnPrimary";
            this.view.btnResetPassword1.hoverSkin = "slButtonGlossBlue0f445e8af7f3c49Hover";
        }
    },
    isPasswordValidForForgotPassword: function(enteredPassword) {
        var password = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,20}$/;
        var userName = this.view.txtUserName.text;
        if (enteredPassword.match(password) && enteredPassword !== userName) {
            return true;
        }
        return false;
    },
    validateNewPasswordWhileTypingForForgotPassword: function(enteredPassword) {
        if (!this.isPasswordValidForForgotPassword(enteredPassword)) {
            this.view.btnResetPassword1.setEnabled(false);
            this.view.btnResetPassword1.skin = "btnSkinGrey";
            this.view.btnResetPassword1.hoverSkin = "btnSkinGrey";
            this.view.forceLayout();
            return false;
        } else {
            this.view.Registererrormsg.isVisible = false;
            this.view.forceLayout();
        }
    },
    validateNewPasswordWhileTypingForChangePassword: function(enteredPassword) {
        if (!this.isPasswordValidForForgotPassword(enteredPassword)) {
            this.view.btnResetPassword.setEnabled(false);
            this.view.btnResetPassword.skin = "btnSkinGrey";
            this.view.btnResetPassword.hoverSkin = "btnSkinGrey";
            this.view.forceLayout();
            return false;
        } else {
            this.view.lblPasswordDontMatch.isVisible = false;
            this.view.forceLayout();
        }
    },
    validateNewPasswordForForgotPassword: function(enteredPassword) {
        if (!this.isPasswordValidForForgotPassword(enteredPassword)) {
            this.view.txtNewPassword1.skin = "skinredbg";
            this.view.Registererrormsg.text = "Password not valid";
            this.view.Registererrormsg.isVisible = true;
            this.view.btnResetPassword1.setEnabled(false);
            this.view.btnResetPassword1.skin = "btnSkinGrey";
            this.view.btnResetPassword1.hoverSkin = "btnSkinGrey";
            this.view.forceLayout();
            return false;
        } else {
            this.view.txtNewPassword1.skin = "skinPasswordTextField";
            this.view.Registererrormsg.isVisible = false;
            this.view.forceLayout();
        }
    },
    validateReenterPasswordWhileTypingForForgotPassword: function(enteredPassword) {
        if (this.view.txtNewPassword1.text && this.view.txtReenterPassword1.text && this.view.txtNewPassword1.text !== this.view.txtReenterPassword1.text) {
            this.view.btnResetPassword1.setEnabled(false);
            this.view.btnResetPassword1.skin = "btnSkinGrey";
            this.view.btnResetPassword1.hoverSkin = "btnSkinGrey";
            this.view.forceLayout();
            return false;
        } else {
            this.view.Registererrormsg.isVisible = false;
            this.view.forceLayout();
        }
        if (this.view.txtUserName.text !== "" && this.view.txtNewPassword1.text !== "" && this.view.txtReenterPassword1.text !== "") {
            if (!this.isPasswordValidForForgotPassword(enteredPassword)) {
                this.view.btnResetPassword1.setEnabled(false);
                this.view.btnResetPassword1.skin = "btnSkinGrey";
                this.view.btnResetPassword1.hoverSkin = "btnSkinGrey";
                this.view.forceLayout();
                return false;
            } else {
                this.view.Registererrormsg.isVisible = false;
                this.view.btnResetPassword1.setEnabled(true);
                this.view.btnResetPassword1.skin = "sknbtnPrimary";
                this.view.btnResetPassword1.hoverSkin = "slButtonGlossBlue0f445e8af7f3c49Hover";
                this.view.forceLayout();
            }
        }
    },
    validateReenterPasswordWhileTypingForChangePassword: function(enteredPassword) {
        if (this.view.txtNewPassword.text && this.view.txtReenterPassword.text && this.view.txtNewPassword.text !== this.view.txtReenterPassword.text) {
            this.view.btnResetPassword.setEnabled(false);
            this.view.btnResetPassword.skin = "btnSkinGrey";
            this.view.btnResetPassword.hoverSkin = "btnSkinGrey";
            this.view.forceLayout();
            return false;
        } else {
            this.view.lblPasswordDontMatch.isVisible = false;
            this.view.forceLayout();
        }
        if (this.view.txtCurrentPassword.text !== "" && this.view.txtNewPassword.text !== "" && this.view.txtReenterPassword.text !== "") {
            if (!this.isPasswordValidForForgotPassword(enteredPassword) || this.isCurrentPasswordSimilarToNewPassword()) {
                this.view.btnResetPassword.setEnabled(false);
                this.view.btnResetPassword.skin = "btnSkinGrey";
                this.view.btnResetPassword.hoverSkin = "btnSkinGrey";
                this.view.forceLayout();
                return false;
            } else {
                this.view.lblPasswordDontMatch.isVisible = false;
                this.view.btnResetPassword.setEnabled(true);
                this.view.btnResetPassword.skin = "sknbtnPrimary";
                this.view.btnResetPassword.hoverSkin = "slButtonGlossBlue0f445e8af7f3c49Hover";
                this.view.forceLayout();
            }
        }
    },
    validateReenterPasswordForForgotPassword: function(enteredPassword) {
        if (this.view.txtNewPassword1.text && this.view.txtReenterPassword1.text && this.view.txtNewPassword1.text !== this.view.txtReenterPassword1.text) {
            this.view.txtReenterPassword1.skin = "skinredbg";
            this.view.Registererrormsg.text = "Passwords don't match";
            this.view.Registererrormsg.isVisible = true;
            this.view.btnResetPassword1.setEnabled(false);
            this.view.btnResetPassword1.skin = "btnSkinGrey";
            this.view.btnResetPassword1.hoverSkin = "btnSkinGrey";
            this.view.forceLayout();
            return false;
        } else {
            this.view.txtReenterPassword1.skin = "skinPasswordTextField";
            this.view.Registererrormsg.isVisible = false;
            this.view.forceLayout();
        }
        if (this.view.txtUserName.text !== "" && this.view.txtNewPassword1.text !== "" && this.view.txtReenterPassword1.text !== "") {
            if (!this.isPasswordValidForForgotPassword(enteredPassword)) {
                this.view.txtNewPassword1.skin = "skinredbg";
                this.view.txtReenterPassword1.skin = "skinredbg";
                this.view.Registererrormsg.text = "Password not valid";
                this.view.Registererrormsg.isVisible = true;
                this.view.btnResetPassword1.setEnabled(false);
                this.view.btnResetPassword1.skin = "btnSkinGrey";
                this.view.btnResetPassword1.hoverSkin = "btnSkinGrey";
                this.view.forceLayout();
                return false;
            } else {
                this.view.Registererrormsg.isVisible = false;
                this.view.btnResetPassword1.setEnabled(true);
                this.view.btnResetPassword1.skin = "sknbtnPrimary";
                this.view.btnResetPassword1.hoverSkin = "slButtonGlossBlue0f445e8af7f3c49Hover";
                this.view.forceLayout();
            }
        }
    },
    resetPasswordForForgotPasswordCall: function() {
        var userDetailsForForgotPasswordJSON = {
            "password": this.view.flxForgotPassword.txtNewPassword1.text,
            "confirmPassword": this.view.flxForgotPassword.txtReenterPassword1.text,
            "id": kony.mvc.MDAApplication.getSharedInstance().appContext.resetPasswordAttributesJSON.id,
            "qp": kony.mvc.MDAApplication.getSharedInstance().appContext.resetPasswordAttributesJSON.qp
        };
        kony.adminConsole.utils.showProgressBar(this.view);
        this.getPresenter().toResetPasswordForForgotPassword(userDetailsForForgotPasswordJSON, this.toResetPasswordForForgotResponse);
    },
    toResetPasswordForForgotResponse: function(response) {
        kony.adminConsole.utils.hideProgressBar(this.view);
        kony.application.dismissLoadingScreen();
        var context;
        if (response === "Reset password for forgot password successful") {
            context = {
                "action": "resetPasswordConfirm"
            };
            this.getPresenter().showErrorLoginScreen(context);
        } else {
            if (response === "Session expired") {
                context = {
                    "action": "SessionExpired"
                };
                this.getPresenter().showLoginScreen(context);
            } else if (response === "New password & confirm password don't match" || response === "New password fails criteria") {
                this.resetPasswordForForgotPasswordMismatchOrFailedCriteriaUI(response);
            } else {
                this.resetPasswordForForgotPasswordFailureUI(response);
            }
        }
    },
    /**
     * Function to mask and unmask password field
     */
    maskText: function(lblPasswordErrorMsg, txtPassword, flxEyeicon, flxEyecross) {
        if (this.view[lblPasswordErrorMsg].isVisible === false) {
            this.view[txtPassword].skin = "skinpwdfield";
        } else {
            this.view[txtPassword].skin = "skinpwdfieldRed";
        }
        if (this.view[txtPassword].text !== "") {
            if (this.view[txtPassword].secureTextEntry) {
                this.view[txtPassword].secureTextEntry = false;
                this.view[flxEyecross].isVisible = false;
                this.view[flxEyeicon].isVisible = true;
            } else {
                this.view[txtPassword].secureTextEntry = true;
                this.view[flxEyecross].isVisible = true;
                this.view[flxEyeicon].isVisible = false;
            }
        }
        this.view.forceLayout();
    },
});
define("AuthModule/frmErrorLoginControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for Canceltext **/
    AS_Label_eeaae02c2cad4778971282477141f8b7: function AS_Label_eeaae02c2cad4778971282477141f8b7(eventobject, x, y) {
        var self = this;
        this.presenter.showLoginScreen({
            "action": "login"
        });
    },
    /** preShow defined for frmErrorLogin **/
    AS_Form_a7af3565c840460f98fbc9638c2db7e1: function AS_Form_a7af3565c840460f98fbc9638c2db7e1(eventobject) {
        var self = this;
        var vizServerURL = window.location.href;
        var qpExists = vizServerURL.indexOf("qp=");
        this.preshowLoginError();
        if (qpExists !== -1) {
            this.view.flxChangePassword.isVisible = false;
            var qp = vizServerURL.substring(qpExists + 3);
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.openResetPasswordForm({
                "qp": qp
            });
        }
    },
    /** onDeviceBack defined for frmErrorLogin **/
    AS_Form_g4cef829c2384a968122661209cb0326: function AS_Form_g4cef829c2384a968122661209cb0326(eventobject) {
        var self = this;
        this.onBrowserBack();
    }
});
define("AuthModule/frmErrorLoginController", ["AuthModule/userfrmErrorLoginController", "AuthModule/frmErrorLoginControllerActions"], function() {
    var controller = require("AuthModule/userfrmErrorLoginController");
    var controllerActions = ["AuthModule/frmErrorLoginControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
