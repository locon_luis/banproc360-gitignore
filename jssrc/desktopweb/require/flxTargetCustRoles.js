define("flxTargetCustRoles", function() {
    return function(controller) {
        var flxTargetCustRoles = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "46px",
            "id": "flxTargetCustRoles",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxTargetCustRoles.setDefaultUnit(kony.flex.DP);
        var lblRoleName = new kony.ui.Label({
            "centerY": "50%",
            "height": "17px",
            "id": "lblRoleName",
            "isVisible": true,
            "left": "10px",
            "maxWidth": "20%",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Platinum DBX Customers",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRoleDescription = new kony.ui.Label({
            "centerY": "50%",
            "height": "17px",
            "id": "lblRoleDescription",
            "isVisible": true,
            "left": "277px",
            "maxWidth": "75%",
            "right": "10px",
            "skin": "sknLbl696C73LatoRegular14px",
            "text": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTargetCustRoles.add(lblRoleName, lblRoleDescription);
        return flxTargetCustRoles;
    }
})