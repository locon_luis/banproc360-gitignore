define("flxSelected", function() {
    return function(controller) {
        var flxSelected = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSelected",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSelected.setDefaultUnit(kony.flex.DP);
        var flxCsr = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxCsr",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "CopyslFbox0ica8ce013fdc40",
            "top": "16dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxCsr.setDefaultUnit(kony.flex.DP);
        var lblCsrAssist = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCsrAssist",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "aa",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgCross = new kony.ui.Image2({
            "centerY": "50%",
            "height": "10dp",
            "id": "imgCross",
            "isVisible": true,
            "right": "5px",
            "skin": "slImage",
            "src": "close_blue.png",
            "width": "10dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCsr.add(lblCsrAssist, imgCross);
        flxSelected.add(flxCsr);
        return flxSelected;
    }
})