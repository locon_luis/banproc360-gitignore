define("userflxCriteriasController", {
    //Type your controller code here //
});
define("flxCriteriasControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxOptions **/
    AS_FlexContainer_d739a5de33754674ac5a0eb876088e1c: function AS_FlexContainer_d739a5de33754674ac5a0eb876088e1c(eventobject, context) {
        var self = this;
        this.executeOnParent("onClickOptions");
    }
});
define("flxCriteriasController", ["userflxCriteriasController", "flxCriteriasControllerActions"], function() {
    var controller = require("userflxCriteriasController");
    var controllerActions = ["flxCriteriasControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
