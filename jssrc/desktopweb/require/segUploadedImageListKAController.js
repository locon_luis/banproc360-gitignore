define("usersegUploadedImageListKAController", {
    //Type your controller code here 
});
define("segUploadedImageListKAControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxOptions **/
    AS_FlexContainer_i98b59c7d77540748c4d450772683aab: function AS_FlexContainer_i98b59c7d77540748c4d450772683aab(eventobject, context) {
        var self = this;
        this.executeOnParent("onClickOptions");
    }
});
define("segUploadedImageListKAController", ["usersegUploadedImageListKAController", "segUploadedImageListKAControllerActions"], function() {
    var controller = require("usersegUploadedImageListKAController");
    var controllerActions = ["segUploadedImageListKAControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
