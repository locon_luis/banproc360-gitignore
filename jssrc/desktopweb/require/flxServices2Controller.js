define("userflxServices2Controller", {
    //Type your controller code here 
});
define("flxServices2ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxOptions **/
    AS_FlexContainer_e4c339e0e0b941679ef3bc3986fa22a1: function AS_FlexContainer_e4c339e0e0b941679ef3bc3986fa22a1(eventobject, context) {
        var self = this;
        this.executeOnParent("onClickOptions");
    }
});
define("flxServices2Controller", ["userflxServices2Controller", "flxServices2ControllerActions"], function() {
    var controller = require("userflxServices2Controller");
    var controllerActions = ["flxServices2ControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
