define(['Promisify', 'ErrorInterceptor', 'ErrorIsNetworkDown'], function(Promisify, ErrorInterceptor, isNetworkDown) {
    var RoleStatusConstants = {
        active: 'SID_ACTIVE',
        inactive: 'SID_INACTIVE'
    };

    function Role_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.roleModel = {
            fetchRoleList: null,
            fetchRoleUpdates: {
                fetchActiveUsers: null,
                fetchRolePermissions: null,
                fetchRoleUsers: null,
                fetchActivePermissions: null
            },
            fetchRoleDetails: {
                roleDetails: null,
                rolePermissions: null,
                roleUsers: null
            },
            fetchCompositePermissions: null,
            context: null
        };
        this.context = {
            toast: null,
            message: null
        };
    }
    inheritsFrom(Role_PresentationController, kony.mvc.Presentation.BasePresenter);
    Role_PresentationController.prototype.reqCSVObj = null;
    Role_PresentationController.prototype.initializePresentationController = function() {
        var self = this;
        ErrorInterceptor.wrap(this, 'businessController').match(function(on) {
            return [
                on(isNetworkDown).do(function() {
                    self.presentUserInterface("frmRoles", {
                        NetworkDownMessage: {}
                    });
                })
            ];
        });
    };
    Role_PresentationController.prototype.showErrorToastMessage = function(message) {
        self.context.toast = "Error";
        self.context.message = message;
        self.presentUserInterface("frmRoles", self.context);
    };
    /**
     * Opening Method for the Roles module fetches and presents the Roles List
     * @name fetchRoleList
     * @member RoleModule.presentationController
     */
    Role_PresentationController.prototype.fetchRoleList = function() {
        var self = this;

        function successCallback(response) {
            self.roleModel.context = "viewRoles";
            self.reqCSVObj = response.roles_view;
            self.roleModel.fetchRoleList = response.roles_view;
            self.presentUserInterface("frmRoles", self.roleModel);
        }

        function failureCallback(error) {
            self.context.toast = "Error";
            self.context.message = ErrorInterceptor.errorMessage(error);
            self.presentUserInterface("frmRoles", self.context);
        }
        this.businessController.fetchAllRoles({}, successCallback, failureCallback);
        this.presentUserInterface("frmRoles", {});
    };
    /**
     * @name createRoleDetails
     * @member RoleModule.presentationController
     * @param FormController fController
     * @param {Role_Name : string, Role_Desc : string, Status_id : string, system_user : string, Permission_ids : [string], User_ids : [string]} param
     */
    Role_PresentationController.prototype.createRoleDetails = function(fController, param) {
        var self = this;

        function successCallback(response) {
            self.fetchRoleList();
        }

        function failureCallback(error) {
            kony.print("error");
            self.context.toast = "Error";
            self.context.message = ErrorInterceptor.errorMessage(error);
            self.presentUserInterface("frmRoles", self.context);
        }
        this.businessController.createRole(param, successCallback.bind(this), failureCallback.bind(this));
    };
    /**
     * @name UpdateRoleDetails
     * @member RoleModule.presentationController
     * @param {User_id : string, Role_Details : {id : string, Name : string, Description : string, Status_id : string}, AssignedTo : {permissionsList : [string], usersList : []}, RemovedFrom : {permissionsList : [], usersList : []}} param
     */
    Role_PresentationController.prototype.UpdateRoleDetails = function(param) {
        var self = this;

        function successCallback(response) {
            self.fetchRoleList();
        }

        function failureCallback(error) {
            self.context.toast = "Error";
            self.context.message = ErrorInterceptor.errorMessage(error);
            self.presentUserInterface("frmRoles", self.context);
            self.fetchRoleList();
        }
        this.businessController.updateRole(param, successCallback.bind(this), failureCallback.bind(this));
    };
    /**
     * @name fetchAllActiveUsersAndAllActivePermissions
     * @member RoleModule.presentationController
     * @param FormController fController
     */
    Role_PresentationController.prototype.fetchAllActiveUsersAndAllActivePermissions = function(fController) {
        var self = this;
        var promiseActiveUsers = Promisify(this.businessController, 'fetchActiveUsers');
        var promiseActivePermissions = Promisify(this.businessController, 'fetchActivePermissions');
        var promiseAllCustomerRoles = Promisify(this.businessController, 'getAllCustomerRoles');
        Promise.all([
            promiseActiveUsers({}),
            promiseActivePermissions({}),
            promiseAllCustomerRoles({})
        ]).then(function(response) {
            self.roleModel.context = "createRole";
            self.roleModel.fetchRoleUpdates.fetchActiveUsers = response[0];
            self.roleModel.fetchRoleUpdates.fetchRoleUsers = [];
            self.roleModel.fetchRoleUpdates.fetchActivePermissions = response[1];
            self.roleModel.fetchRoleUpdates.fetchRolePermissions = [];
            self.roleModel.fetchRoleUpdates.fetchAllCustomerRoles = response[2].membergroup;
            self.roleModel.fetchRoleUpdates.fetchCustomerRoleForUserRole = [];
            self.presentUserInterface("frmRoles", self.roleModel);
        }).catch(function failureCallback(error) {
            self.context.toast = "Error";
            self.context.message = ErrorInterceptor.errorMessage(error);
            self.presentUserInterface("frmRoles", self.context);
        });
    };
    /**
     * @name fetchUpdateRoleData
     * @member RoleModule.presentationController
     * @param FormController fController
     * @param {role_id : string} param
     */
    Role_PresentationController.prototype.fetchUpdateRoleData = function(fController, param) {
        var self = this;
        var promiseRoleUsers = Promisify(this.businessController, 'fetchRoleUsers');
        var promiseRolePermissions = Promisify(this.businessController, 'fetchRolePermissions');
        var promiseActiveUsers = Promisify(this.businessController, 'fetchActiveUsers');
        var promiseActivePermissions = Promisify(this.businessController, 'fetchActivePermissions');
        var promiseCustomerRoleForUserRole = Promisify(this.businessController, 'getInternalRoleToCustomerRoleMapping');
        var promiseAllCustomerRoles = Promisify(this.businessController, 'getAllCustomerRoles');
        Promise.all([
            promiseRoleUsers(param.role_id),
            promiseRolePermissions(param.role_id),
            promiseActiveUsers({}),
            promiseActivePermissions({}),
            promiseCustomerRoleForUserRole({
                "InternalRole_id": param.role_id
            }),
            promiseAllCustomerRoles({})
        ]).then(function onSuccess(responses) {
            self.roleModel.context = "updateRole";
            self.roleModel.fetchRoleUpdates.fetchRoleUsers = responses[0];
            self.roleModel.fetchRoleUpdates.fetchRolePermissions = responses[1];
            self.roleModel.fetchRoleUpdates.fetchActiveUsers = responses[2];
            self.roleModel.fetchRoleUpdates.fetchActivePermissions = responses[3];
            self.roleModel.fetchRoleUpdates.fetchCustomerRoleForUserRole = responses[4].internal_role_to_customer_role_mapping;
            self.roleModel.fetchRoleUpdates.fetchAllCustomerRoles = responses[5].membergroup;
            self.presentUserInterface("frmRoles", self.roleModel);
        }).catch(function failureCallback(error) {
            self.context.toast = "Error";
            self.context.message = ErrorInterceptor.errorMessage(error);
            self.presentUserInterface("frmRoles", self.context);
        });
    };
    /**
     * @name changeStatusOf
     * @member RoleModule.presentationController
     * @param string roleId
     * @param string statusId
     * @param string user_id
     */
    Role_PresentationController.prototype.changeStatusOf = function(roleId, statusId, user_id) {
        var self = this;
        var updateStatusTo = null;
        if (RoleStatusConstants.active === statusId) {
            updateStatusTo = RoleStatusConstants.inactive;
        } else {
            updateStatusTo = RoleStatusConstants.active;
        }
        var params = {
            "User_id": user_id,
            "Role_Details": {
                "id": roleId,
                "Status_id": updateStatusTo
            }
        };

        function successCallback(response) {
            self.context.toast = "Success";
            if (updateStatusTo === RoleStatusConstants.inactive) self.context.message = kony.i18n.getLocalizedString("i18n.frmRolesController.Role_successfully_deactivated");
            else self.context.message = kony.i18n.getLocalizedString("i18n.frmRolesController.Role_successfully_activated");
            self.presentUserInterface("frmRoles", self.context);
            self.fetchRoleList();
        }

        function failureCallback(error) {
            self.context.toast = "Error";
            self.context.message = ErrorInterceptor.errorMessage(error);
            self.presentUserInterface("frmRoles", self.context);
        }
        self.businessController.updateRoleStatus(params, successCallback, failureCallback);
    };
    /**
     * @name fetchRoleDetails
     * @member RoleModule.presentationController
     * @param FormController fController
     * @param {roleId : string, roleName : string, roleDesc : string, roleStatus : string} param
     */
    Role_PresentationController.prototype.fetchRoleDetails = function(fController, param) {
        var self = this;
        var promiseRolePermissions = Promisify(this.businessController, 'fetchRolePermissions');
        var promiseRoleUsers = Promisify(this.businessController, 'fetchRoleUsers');
        var promiseCustomerRolesForRole = Promisify(this.businessController, 'getInternalRoleToCustomerRoleMapping');
        Promise.all([
            promiseRolePermissions(param.roleId),
            promiseRoleUsers(param.roleId),
            promiseCustomerRolesForRole({
                "InternalRole_id": param.roleId
            }),
        ]).then(function onSuccess(responses) {
            self.roleModel.context = "fetchRoleDetails";
            self.roleModel.fetchRoleDetails = {};
            self.roleModel.fetchRoleDetails.roleDetails = param;
            self.roleModel.fetchRoleDetails.rolePermissions = responses[0];
            self.roleModel.fetchRoleDetails.roleUsers = responses[1];
            self.roleModel.fetchRoleDetails.roleCustomerRoles = responses[2].internal_role_to_customer_role_mapping;
            self.presentUserInterface("frmRoles", self.roleModel);
        }).catch(function failureCallback(error) {
            self.context.toast = "Error";
            self.context.message = ErrorInterceptor.errorMessage(error);
            self.presentUserInterface("frmRoles", self.context);
        });
    };
    /**
     * @name fetchCompositePermissions
     * @member RoleModule.presentationController
     * @param {Role_id : string, Permission_id : string} params
     */
    Role_PresentationController.prototype.fetchCompositePermissions = function(params) {
        var self = this;

        function successCallback(response) {
            self.roleModel.fetchCompositePermissions = response;
            self.roleModel.context = "fetchCompositePermissions";
            self.presentUserInterface("frmRoles", self.roleModel);
        }

        function failureCallback(error) {
            kony.print("error");
            self.context.toast = "Error";
            self.context.message = ErrorInterceptor.errorMessage(error);
            self.presentUserInterface("frmRoles", self.context);
        }
        this.businessController.getUserOrRoleCompositePermissions(params, successCallback, failureCallback);
    };
    /**
     * @name updateRoleCompositePermissions
     * @member RoleModule.presentationController
     * @param {roleId : string, addedCompositePermissions : [string], removedCompositePermissions : [string]} params
     * @param string opt
     */
    Role_PresentationController.prototype.updateRoleCompositePermissions = function(params, opt) {
        var self = this;
        var operation = "";
        if (opt === kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add")) {
            operation = "addCompositePermissions";
        } else if (opt === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Remove")) {
            operation = "removedCompositePermissions";
        }

        function successCallback(response) {
            self.context.toast = "Success";
            self.context.message = "CSR assist permissions successfully configured";
            self.presentUserInterface("frmRoles", self.context);
        }

        function failureCallback(error) {
            self.context.toast = "Error";
            self.context.message = ErrorInterceptor.errorMessage(error);
            self.presentUserInterface("frmRoles", self.context);
        }
        this.businessController.updateRoleCompositePermissions(params, successCallback, failureCallback);
    };
    return Role_PresentationController;
});