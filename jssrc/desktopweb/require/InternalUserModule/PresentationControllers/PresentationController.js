define(['Promisify', 'ErrorInterceptor', 'ErrorIsNetworkDown'], function(Promisify, ErrorInterceptor, isNetworkDown) {
    function InternalUser_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.userModel = {
            usersList: null,
            userDetails: null,
            editDetails: null,
            addressDetails: {
                city: [],
                country: [],
                region: []
            },
            allRoles: null,
            allPermissions: {
                permissions_view: null
            }
        };
        this.globalAddressDetails = {
            country: [],
            region: [],
            city: []
        };
    }
    inheritsFrom(InternalUser_PresentationController, kony.mvc.Presentation.BasePresenter);
    InternalUser_PresentationController.prototype.initializePresentationController = function() {
        var self = this;
        ErrorInterceptor.wrap(this, 'businessController').match(function(on) {
            return [
                on(isNetworkDown).do(function() {
                    self.presentUserInterface('frmUsers', {
                        NetworkDownMessage: {}
                    });
                })
            ];
        });
    };
    /**
     * @name fetchUsersList
     * @member InternalUserModule.presentationController
     * 
     */
    InternalUser_PresentationController.prototype.fetchUsersList = function() {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.userModel.userDetails = null;
            self.userModel.editDetails = null;
            self.userModel.usersList = response.internalusers_view;
            self.fetchAddressData();
            self.fetchAllBranches();
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            var context = {
                status: "Error",
                message: ErrorInterceptor.errorMessage(error)
            };
            self.presentUserInterface("frmUsers", context);
            self.fetchAllBranches();
        }
        this.businessController.fetchInternalUsers({}, successCallback, failureCallback);
        this.presentUserInterface("frmUsers", {});
    };
    /**
     * @name updateUserStatus
     * @member InternalUserModule.presentationController
     * @param FormController fController
     * @param {Systemuser_id : string, Status_id : string} params
     */
    InternalUser_PresentationController.prototype.updateUserStatus = function(fController, params) {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            //self.presentUserInterface('frmUsers',{"LoadingScreen":{focus:false}});
            kony.print("Status Updated successfully");
            self.fetchUpdatedUsersList();
            self.showToastMessage(kony.i18n.getLocalizedString("i18n.frmCSRController.Update_Successful"), 'SUCCESS');
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessage(ErrorInterceptor.errorMessage(error), 'FAILURE');
        }
        this.businessController.updateUserStatus(params, successCallback, failureCallback);
    };
    /**
     * @name fetchUpdatedUsersList
     * @member InternalUserModule.presentationController
     * @param FormController fController
     */
    InternalUser_PresentationController.prototype.fetchUpdatedUsersList = function() {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface("frmUsers", {
                "updatedUsersList": response.internalusers_view
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            var context = "Error";
            self.presentUserInterface("frmUsers", context);
        }
        this.businessController.fetchInternalUsers({}, successCallback, failureCallback);
    };
    /**
     * @name fetchUserDetails
     * @member InternalUserModule.presentationController
     * @param FormController fController
     * @param {User_id : string, isEdit : boolean, target : string} params
     */
    InternalUser_PresentationController.prototype.fetchUserDetails = function(fController, params) {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.userModel.userDetails = null;
            self.userModel.editDetails = null;
            self.userModel.usersList = null;
            if (params.isEdit) {
                self.userModel.editDetails = response;
            } else {
                self.userModel.userDetails = response;
            }
            if (params.target) {
                self.userModel.editDetails.target = params.target;
            }
            self.presentUserInterface("frmUsers", self.userModel);
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            var context = {
                status: "Error",
                message: ErrorInterceptor.errorMessage(error)
            };
            self.presentUserInterface("frmUsers", context);
        }
        this.businessController.fetchUserProfile(params, successCallback, failureCallback);
    };
    /*
     * function to call multiple command handlers to get the city,country,region
     * @name fetchAddressData
     * @member InternalUserModule.presentationController
     */
    InternalUser_PresentationController.prototype.fetchAddressData = function() {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });
        var promiseCountryList = Promisify(this.businessController, 'fetchCountryList');
        var promiseRegionList = Promisify(this.businessController, 'fetchRegionList');
        var promiseCityList = Promisify(this.businessController, 'fetchCityList');
        Promise.all([
            promiseCountryList({}),
            promiseRegionList({}),
            promiseCityList({})
        ]).then(function successCallback(response) {
            self.userModel.addressDetails.country = response[0];
            self.globalAddressDetails.country = response[0];
            self.userModel.addressDetails.region = response[1];
            self.globalAddressDetails.region = response[1];
            self.userModel.addressDetails.city = response[2];
            self.globalAddressDetails.city = response[2];
            self.presentUserInterface("frmUsers", self.userModel);
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
        }).catch(function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            var context = {
                status: "Error",
                message: ErrorInterceptor.errorMessage(error)
            };
            self.presentUserInterface("frmUsers", context);
        });
    };
    /*
     * function to call command handler to create New User
     * @param :  new user data
     */
    /**
     * @name onSaveBtnClick
     * @member InternalUserModule.presentationController
     * @param {userData : {firstName : string, lastName : string, middleName : string, userName : string, email : string}, homeAddrData : {addressLine1 : string, addressLine2 : string, city_id : string, region_id : string, country_id : string, zipcode : string}, workAddrData : {location : string, branch : string, workAddress : string, zipcode : string}, roleData : {user_id : string, role_id : string}, permissionList : [string]} createUserData
     */
    InternalUser_PresentationController.prototype.onSaveBtnClick = function(createUserData) {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });
        kony.print(" New user data to save" + JSON.stringify(createUserData));

        function successCallback(response) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print(response);
            self.showToastMessage('User created successfully', 'SUCCESS');
            self.fetchUsersList();
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("ERROR in on save Button click" + error);
            self.showToastMessage(ErrorInterceptor.errorMessage(error), 'FAILURE');
        }
        this.businessController.createInternalUser(createUserData, successCallback, failureCallback);
    };
    /*
     * function to call command handler to fetch all Roles
     */
    /**
     * @name fetchAllRoles
     * @member InternalUserModule.presentationController
     * 
     */
    InternalUser_PresentationController.prototype.fetchAllRoles = function() {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print(response);
            self.presentUserInterface("frmUsers", response);
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("ERROR in fetchAllRoles" + error);
        }
        this.businessController.fetchAllRoles({}, successCallback, failureCallback);
    };
    /*
     * function to call command handler to fetch all the permissions
     */
    /**
     * @name fetchAllPermissions
     * @member InternalUserModule.presentationController
     * 
     */
    InternalUser_PresentationController.prototype.fetchAllPermissions = function() {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface("frmUsers", {
                "permissions_view": response
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print('ERROR:Not able to fetch permission records', error);
        }
        this.businessController.fetchPermissions({}, successCallback, failureCallback);
    };
    /**
     * @name editInternalUser
     * @member InternalUserModule.presentationController
     * @param {User_id : string, ModifiedByID : string, ModifiedByName : string, FirstName : string, LastName : string, MiddleName : string, UserName : string, Email : string, addr1 : string, addr2 : string, City_id : string, State_id : string, Country_id : string, City_Name : string, State_Name : string, Country_Name : string, Zipcode : string, BranchLocation_Name : string, BranchLocation_id : string, zipcode : string, Role_id : string, Role_Name : string, listOfAddedPermissionsNames : [], listOfRemovedPermissionsNames : [], listOfAddedPermissions : [], listOfRemovedPermissions : []} editDetails
     */
    InternalUser_PresentationController.prototype.editInternalUser = function(editDetails) {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("edit success");
            self.showToastMessage('Internal user has been edited successfully.', 'SUCCESS');
            self.fetchUsersList();
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessage(ErrorInterceptor.errorMessage(error), 'FAILURE');
            kony.print('ERROR:Not able to edit user details', error);
        }
        this.businessController.editInternalUser(editDetails, successCallback, failureCallback);
    };
    /**
     * @name showToastMessage
     * @member InternalUserModule.presentationController
     * @param string message
     * @param string status
     */
    InternalUser_PresentationController.prototype.showToastMessage = function(message, status) {
        this.presentUserInterface("frmUsers", {
            toast: {
                message: message,
                status: status
            }
        });
    };
    /**
     * @name sendResetPasswordEmail
     * @member InternalUserModule.presentationController
     * @param {emailId : string, emailType : string} data
     */
    InternalUser_PresentationController.prototype.sendResetPasswordEmail = function(data) {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessage('Email sent successfully', 'SUCCESS');
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessage(ErrorInterceptor.errorMessage(error), 'FAILURE');
            kony.print('ERROR:Not able to send email', error);
        }
        this.businessController.sendEmail(data, successCallback, failureCallback);
    };
    /**
     * @name fetchAllRolePermissions
     * @member InternalUserModule.presentationController
     * 
     */
    InternalUser_PresentationController.prototype.fetchAllRolePermissions = function() {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("get RolePermissions successful");
            self.presentUserInterface("frmUsers", {
                "RolePermissions": response
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print('get RolePermissions failed', error);
        }
        this.businessController.fetchAllRolePermissions({}, successCallback, failureCallback);
    };
    /**
     * @name fetchAllBranches
     * @member InternalUserModule.presentationController
     * 
     */
    InternalUser_PresentationController.prototype.fetchAllBranches = function() {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            //self.presentUserInterface('frmUsers',{"LoadingScreen":{focus:false}});  
            kony.print("get Branches successful");
            self.presentUserInterface("frmUsers", {
                "Branches": response
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print('get Branches failed', error);
        }
        this.businessController.fetchAllBranches({}, successCallback, failureCallback);
    };
    /*
     * function to call global city,country,region
     */
    /**
     * @name fetchGlobalAddressData
     * @member InternalUserModule.presentationController
     * @param string addressString
     * @param string addressCode
     */
    InternalUser_PresentationController.prototype.fetchGlobalAddressData = function(addressString, addressCode) {
        kony.print("Inside fetchGlobalAddressData() of InternalUser_PresentationController");
        var self = this;
        if (addressString === "country") {
            var globalRegion = [];
            var globalCity = [];
            globalCity.push(["lb1", "Select a City"]);
            globalRegion.push(["lb1", "Select a State"]);
            for (var i = 0; i < Object.keys(self.globalAddressDetails.region).length; ++i) {
                if (self.globalAddressDetails.region[i].Country_id === addressCode) {
                    globalRegion.push([self.globalAddressDetails.region[i].id, self.globalAddressDetails.region[i].Name]);
                }
            }
            for (var j = 0; j < Object.keys(self.globalAddressDetails.city).length; ++j) {
                if (self.globalAddressDetails.city[j].Region_id === addressCode) {
                    globalCity.push([self.globalAddressDetails.city[j].id, self.globalAddressDetails.city[j].Name]);
                }
            }
            self.presentUserInterface("frmUsers", {
                "globalAddress": {
                    "globalRegion": globalRegion,
                    "globalCity": globalCity
                }
            });
        } else if (addressString === "region") {
            var globalCity = [];
            globalCity.push(["lb1", "Select a City"]);
            for (var k = 0; k < Object.keys(self.globalAddressDetails.city).length; ++k) {
                if (self.globalAddressDetails.city[k].Region_id === addressCode) {
                    globalCity.push([self.globalAddressDetails.city[k].id, self.globalAddressDetails.city[k].Name]);
                }
            }
            self.presentUserInterface("frmUsers", {
                "globalAddress": {
                    "globalCity": globalCity
                }
            });
        }
    };
    /**
     * @name fetchCompositePermissions
     * @member InternalUserModule.presentationController
     * @param {User_id : string, Role_id : string, Permission_id : string} params
     */
    InternalUser_PresentationController.prototype.fetchCompositePermissions = function(params) {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.presentUserInterface("frmUsers", {
                "compositePermissionsCSR": response
            });
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            kony.print("error in fetch composite permissions");
        }
        this.businessController.getUserOrRoleCompositePermissions(params, successCallback, failureCallback);
    };
    /*
     * function to update the role compositePermission
     * @params:JSON{roleId,addedCompositePermissions:[],removedCompositePermissions:[]}
     */
    InternalUser_PresentationController.prototype.updateUserCompositePermissions = function(params) {
        var self = this;
        self.presentUserInterface('frmUsers', {
            "LoadingScreen": {
                focus: true
            }
        });

        function successCallback(response) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessage('CSR assist permission successfully configured', 'SUCCESS');
        }

        function failureCallback(error) {
            self.presentUserInterface('frmUsers', {
                "LoadingScreen": {
                    focus: false
                }
            });
            self.showToastMessage(ErrorInterceptor.errorMessage(error), 'FAILURE');
        }
        this.businessController.updateUserCompositePermissions(params, successCallback, failureCallback);
    };
    return InternalUser_PresentationController;
});