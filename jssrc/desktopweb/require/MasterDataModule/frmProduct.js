define("MasterDataModule/frmProduct", function() {
    return function(controller) {
        function addWidgetsfrmProduct() {
            this.setDefaultUnit(kony.flex.DP);
            var flxProduct = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxProduct",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProduct.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0.00%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "text": "IMPORT"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")"
                    },
                    "flxButtons": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "right": "0px",
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmProductController.Banking_Product_Management\")"
                    },
                    "lblUserName": {
                        "right": "25px",
                        "text": "Preetish",
                        "top": "viz.val_cleared"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 8
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "63dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "110dp",
                "width": "100%",
                "zIndex": 7
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "48dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false,
                        "left": "35dp"
                    },
                    "flxSearch": {
                        "top": "0px",
                        "width": "350px"
                    },
                    "flxSearchContainer": {
                        "left": "0dp",
                        "right": "0dp",
                        "top": "5dp"
                    },
                    "flxSubHeader": {
                        "bottom": "0dp",
                        "height": "100%",
                        "top": "0dp"
                    },
                    "subHeader": {
                        "centerY": "50%",
                        "height": "48dp",
                        "top": "0dp"
                    },
                    "tbxSearchBox": {
                        "placeholder": "Search by Product Name"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxBreadCrum = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrum",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "120dp",
                "width": "100%",
                "zIndex": 4
            }, {}, {});
            flxBreadCrum.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "centerY": "50%",
                        "left": "35px"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrum.add(breadcrumbs);
            var flxViewProductDetails = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "50px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxViewProductDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "pagingEnabled": false,
                "right": "25px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "155px",
                "verticalScrollIndicator": true,
                "width": "94%",
                "zIndex": 10
            }, {}, {});
            flxViewProductDetails.setDefaultUnit(kony.flex.DP);
            var flxViewProductDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewProductDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxViewProductDetailsContainer.setDefaultUnit(kony.flex.DP);
            var productDetailsView = new com.adminConsole.Products.productDetailsView({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "productDetailsView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxViewEditButton": {
                        "isVisible": false
                    },
                    "rtxViewDescription": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewProductDetailsContainer.add(productDetailsView);
            flxViewProductDetails.add(flxViewProductDetailsContainer);
            var flxMainContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": false,
                "height": "75%",
                "id": "flxMainContent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "Copysknscrollflxf0ja093a1ef62648",
                "top": "175px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var FlexProducts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexProducts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            FlexProducts.setDefaultUnit(kony.flex.DP);
            var FlexProductTable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "id": "FlexProductTable",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            FlexProductTable.setDefaultUnit(kony.flex.DP);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "760px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100px",
                "width": "200px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var flxDeactivate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i1d238dfcce748",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDeactivate.setDefaultUnit(kony.flex.DP);
            var lblIconOption1 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblIconOption1",
                "isVisible": true,
                "left": "14px",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDeactive\")",
                "width": "20px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold0hf492575252c4cStatic"
            });
            flxDeactivate.add(lblIconOption1, lblOption2);
            var flxEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ca7ba3b92f9949",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEdit.setDefaultUnit(kony.flex.DP);
            var lblIconOption2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblIconOption2",
                "isVisible": true,
                "left": "14px",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": "20px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold0hf492575252c4cStatic"
            });
            flxEdit.add(lblIconOption2, lblOption1);
            flxSelectOptions.add(flxDeactivate, flxEdit);
            var flxProductList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "id": "flxProductList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp"
            }, {}, {});
            flxProductList.setDefaultUnit(kony.flex.DP);
            var flxProductsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxProductsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxProductsHeader.setDefaultUnit(kony.flex.DP);
            var flxProductName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxProductName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "70px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_i87bd852f9f74801ae5ceff0e9ee81d1,
                "skin": "slFbox",
                "top": 0,
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxProductName.setDefaultUnit(kony.flex.DP);
            var lblProductName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProductName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "PRODUCT NAME",
                "top": 0,
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortProductName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortProductName",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxProductName.add(lblProductName, fontIconSortProductName);
            var flxProductType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxProductType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "34%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxProductType.setDefaultUnit(kony.flex.DP);
            var lblProductType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProductType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "PRODUCT TYPE",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconFilterType = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilterType",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxProductType.add(lblProductType, fontIconFilterType);
            var flxProductStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxProductStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_i88e2961592c4b338de61134a3117f51,
                "right": "115dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {}, {});
            flxProductStatus.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": 0,
                "width": "46px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconFilterStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilterStatus",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxProductStatus.add(lblStatus, fontIconFilterStatus);
            var lblHeaderSeperator = new kony.ui.Label({
                "bottom": "1px",
                "centerX": "50%",
                "height": "1px",
                "id": "lblHeaderSeperator",
                "isVisible": true,
                "left": "35dp",
                "right": "35px",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProductsHeader.add(flxProductName, flxProductType, flxProductStatus, lblHeaderSeperator);
            var segProducts = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "fonticonActive": "",
                    "imgOptions": "dots3x.png",
                    "lblIconOptions": "",
                    "lblProductName": "Jompay",
                    "lblProductType": "Mobile App",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active"
                }, {
                    "fonticonActive": "",
                    "imgOptions": "dots3x.png",
                    "lblIconOptions": "",
                    "lblProductName": "Jompay",
                    "lblProductType": "Mobile App",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active"
                }, {
                    "fonticonActive": "",
                    "imgOptions": "dots3x.png",
                    "lblIconOptions": "",
                    "lblProductName": "Jompay",
                    "lblProductType": "Mobile App",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active"
                }],
                "groupCells": false,
                "height": "385dp",
                "id": "segProducts",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_a938b0b3348e41a1a31e0a5eaafb7671,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxProducts",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "65px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxOptions": "flxOptions",
                    "flxProducts": "flxProducts",
                    "flxStatus": "flxStatus",
                    "fonticonActive": "fonticonActive",
                    "imgOptions": "imgOptions",
                    "lblIconOptions": "lblIconOptions",
                    "lblProductName": "lblProductName",
                    "lblProductType": "lblProductType",
                    "lblSeparator": "lblSeparator",
                    "lblServiceStatus": "lblServiceStatus"
                },
                "width": "100%",
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "5px",
                "clipBounds": false,
                "id": "flxPagination",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox0d9c3974835234d",
                "top": 0,
                "width": "222px",
                "zIndex": 1
            }, {}, {});
            flxPagination.setDefaultUnit(kony.flex.DP);
            var flxPaginationWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": false,
                "height": "30px",
                "id": "flxPaginationWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox0d9c3974835234d",
                "top": 20,
                "width": "222px",
                "zIndex": 1
            }, {}, {});
            flxPaginationWrapper.setDefaultUnit(kony.flex.DP);
            var lbxPagination = new kony.ui.ListBox({
                "height": "30dp",
                "id": "lbxPagination",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Page 1 of 20"],
                    ["lb2", "Page 2 of 20"],
                    ["lb3", "Page 3 of 20"],
                    ["lb4", "Page 4 of 20"]
                ],
                "selectedKey": "lb1",
                "selectedKeyValue": ["lb1", "Page 1 of 20"],
                "skin": "sknlbxNobgNoBorderPagination",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var flxPaginationSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPaginationSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "2dp",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxPaginationSeperator.setDefaultUnit(kony.flex.DP);
            flxPaginationSeperator.add();
            var flxPrevious = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "32dp",
                "id": "flxPrevious",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "right": "3dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "32px",
                "zIndex": 1
            }, {}, {});
            flxPrevious.setDefaultUnit(kony.flex.DP);
            var lblIconPrevious = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconPrevious",
                "isVisible": true,
                "skin": "sknFontIconPrevNextDisable",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconPrevious\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPrevious.add(lblIconPrevious);
            var flxNext = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "32dp",
                "id": "flxNext",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "32px",
                "zIndex": 1
            }, {}, {});
            flxNext.setDefaultUnit(kony.flex.DP);
            var lblIconNext = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconNext",
                "isVisible": true,
                "skin": "sknFontIconPrevNextDisable",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconNext\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNext.add(lblIconNext);
            flxPaginationWrapper.add(lbxPagination, flxPaginationSeperator, flxPrevious, flxNext);
            flxPagination.add(flxPaginationWrapper);
            var flxProductStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "345dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "120px",
                "zIndex": 11
            }, {}, {});
            flxProductStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "left": "viz.val_cleared",
                        "right": "15dp",
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxProductStatusFilter.add(statusFilterMenu);
            var flxProductTypeFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductTypeFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "755dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "160dp",
                "zIndex": 11
            }, {}, {});
            flxProductTypeFilter.setDefaultUnit(kony.flex.DP);
            var productTypeFilterMenu = new com.adminConsole.Products.productTypeFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "productTypeFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxFilterSort": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblSeperator": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxProductTypeFilter.add(productTypeFilterMenu);
            var flxNoRecordsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxNoRecordsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoRecords = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoRecords",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecordsFound.add(rtxNoRecords);
            flxProductList.add(flxProductsHeader, segProducts, flxPagination, flxProductStatusFilter, flxProductTypeFilter, flxNoRecordsFound);
            FlexProductTable.add(flxSelectOptions, flxProductList);
            FlexProducts.add(FlexProductTable);
            flxMainContent.add(FlexProducts);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxMainSubHeader, flxBreadCrum, flxViewProductDetails, flxMainContent, flxLoading);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 12
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            flxProduct.add(flxLeftPannel, flxRightPanel, flxToastMessage);
            this.add(flxProduct);
        };
        return [{
            "addWidgets": addWidgetsfrmProduct,
            "enabledForIdleTimeout": true,
            "id": "frmProduct",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_d9031c26bdde47fe93cabcf46ab4e718(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_i7e627b88d1d4832ad4472348ecd0e6c,
            "retainScrollPosition": false
        }]
    }
});