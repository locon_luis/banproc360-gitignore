define("MasterDataModule/userfrmProductController", {
    productsList: [],
    allProductsList: [],
    productTypeList: [
        ["0", "Select"],
        ["PRODUCT_TYPE1", "Savings"],
        ["PRODUCT_TYPE2", "Checking"],
        ["PRODUCT_TYPE3", "Credit Card"],
        ["PRODUCT_TYPE4", "Deposit"],
        ["PRODUCT_TYPE5", "Mortgage"]
    ],
    inFilter: false,
    preShowActions: function() {
        this.setflowActions();
        this.view.mainHeader.flxButtons.setVisibility(false);
        kony.adminConsole.utils.showProgressBar(this.view);
        this.setPreshowData();
        this.setScrollHeight();
        var screenHeight = kony.os.deviceInfo().screenHeight;
        this.view.flxMainContent.height = screenHeight - 115 + "px";
        this.view.flxProductList.height = screenHeight - 202 + "px";
        this.view.FlexProducts.height = screenHeight - 115 + "px";
        this.view.segProducts.height = screenHeight - 270 + "px";
        this.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100";
        this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmProductController.BANKING_PRODUCT_MANAGEMENT");
        this.view.subHeader.flxClearSearchImage.setVisibility(false);
        this.view.flxProductList.setVisibility(true);
        this.view.flxNoRecordsFound.setVisibility(false);
        this.view.flxViewProductDetails.setVisibility(false);
        this.view.productDetailsView.setVisibility(false);
        this.view.flxNoRecordsFound.setVisibility(false);
    },
    clearProductDefaults: function() {
        this.currentPage = 1;
        this.view.subHeader.tbxSearchBox.text = "";
        this.view.subHeader.flxClearSearchImage.setVisibility(false);
        //this.view.lbxPagination.selectedKey = this.currentPage;
        this.view.flxViewProductDetails.setVisibility(false);
        this.view.flxBreadCrum.setVisibility(false);
    },
    shouldUpdateUI: function(viewModel) {
        return viewModel !== undefined && viewModel !== null;
    },
    willUpdateUI: function(productsModel) {
        var self = this;
        this.clearProductDefaults();
        this.updateLeftMenu(productsModel);
        if (productsModel.context === "viewProductsList") {
            var searchResult = productsModel.productsList.filter(self.searchFilter);
            this.productsList = productsModel.productsList;
            this.view.flxProductList.setVisibility(true);
            this.renderProductsList();
            kony.adminConsole.utils.hideProgressBar(this.view);
        } else if (productsModel.progressBar && productsModel.progressBar.show && productsModel) {
            if (productsModel.progressBar.show === kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success")) kony.adminConsole.utils.showProgressBar(this.view);
            else kony.adminConsole.utils.hideProgressBar(this.view);
        } else if (productsModel.context === "error") {
            this.view.flxProductList.setVisibility(false);
            this.view.toastMessage.showErrorToastMessage(productsModel.message, this);
        }
        this.view.forceLayout();
    },
    setScrollHeight: function() {
        var screenHeight = kony.os.deviceInfo().screenHeight;
        var scrollHeight;
        //var scrollHeight= screenHeight-this.view.flxMainHeader.height;
        scrollHeight = screenHeight - 106;
        this.view.FlexProducts.height = scrollHeight - 100 + "px";
        this.view.flxMainContent.height = scrollHeight - 30 + "px";
        this.view.flxNoRecordsFound.height = "400px";
    },
    setPreshowData: function() {
        this.view.flxMainHeader.setVisibility(true);
        //this.view.breadcrumbs.setVisibility(false);
        //this.view.flxNoRecordsFound.setVisibility(false);
        this.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
    },
    hideAll: function() {
        this.view.flxBreadCrum.setVisibility(false);
        this.view.flxViewProductDetails.setVisibility(false);
        this.view.FlexProducts.setVisibility(false);
        this.view.flxMainSubHeader.setVisibility(false);
    },
    searchFilter: function(product) {
        var searchText = this.view.subHeader.tbxSearchBox.text;
        if (typeof searchText === "string" && searchText.length > 0) {
            return product.productName.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
        } else {
            return true;
        }
    },
    getSelectedIndex: function() {
        var product_name = this.view.segProducts.selecteditems[0].lblProductName;
        for (var i = 0; i < this.productsList.length; i++) {
            if (this.productsList[i].productName === product_name) {
                this.selectedId = i;
                break;
            }
        }
    },
    productDetailsView: function() {
        this.hideAll();
        this.getSelectedIndex();
        var statusId = this.productsList[this.selectedId].Status_id;
        var skin, img;
        if (statusId === kony.i18n.getLocalizedString("i18n.frmProductController.SID_PRO_ACTIVE")) {
            statusId = kony.i18n.getLocalizedString("i18n.secureimage.Active");
            skin = "sknlblLato5bc06cBold14px";
            img = "sknFontIconActivate";
        } else if (statusId === kony.i18n.getLocalizedString("i18n.frmProductController.SID_PRO_INACTIVE")) {
            statusId = kony.i18n.getLocalizedString("i18n.users.disabled");
            img = "sknfontIconInactive";
            skin = "sknlblLatoDeactive";
        }
        this.view.breadcrumbs.lblCurrentScreen.text = this.productsList[this.selectedId].productName;
        this.view.flxBreadCrum.setVisibility(true);
        this.view.productDetailsView.lblViewValue1.text = this.productsList[this.selectedId].productName;
        this.view.productDetailsView.lblViewValue3.text = this.productsList[this.selectedId].productType;
        this.view.productDetailsView.lblViewValue2.text = statusId;
        this.view.productDetailsView.lblViewValue2.skin = skin;
        this.view.productDetailsView.fontIconViewValue2.skin = img;
        this.view.productDetailsView.lblViewValue4.text = this.productsList[this.selectedId].productTypeId;
        this.view.productDetailsView.rtxViewDescription.text = this.productsList[this.selectedId].features;
        this.view.productDetailsView.rtxChargesFee.text = this.productsList[this.selectedId].rates;
        this.view.productDetailsView.rtxAdditionalInfo.text = this.productsList[this.selectedId].info;
        this.view.productDetailsView.rtxViewDescription.setVisibility(false);
        this.view.productDetailsView.rtxChargesFee.setVisibility(false);
        this.view.productDetailsView.rtxAdditionalInfo.setVisibility(false);
        this.view.productDetailsView.fontIconImgView1.text = "\ue922";
        this.view.productDetailsView.fontIconImgView1.skin = "sknfontIconDescRightArrow14px";
        this.view.productDetailsView.fontIconImgView2.text = "\ue922";
        this.view.productDetailsView.fontIconImgView2.skin = "sknfontIconDescRightArrow14px";
        this.view.productDetailsView.fontIconImgView3.text = "\ue922";
        this.view.productDetailsView.fontIconImgView3.skin = "sknfontIconDescRightArrow14px";
        this.view.flxViewProductDetails.setVisibility(true);
        this.view.productDetailsView.setVisibility(true);
        this.view.productDetailsView.flxDropDown1.onClick();
        this.view.productDetailsView.flxDropDown2.onClick();
        this.view.productDetailsView.flxDropDown3.onClick();
        this.view.forceLayout();
    },
    setflowActions: function() {
        var scopeObj = this;
        this.view.flxProductName.onClick = function() {
            var data = scopeObj.sortBy.column("productName");
            scopeObj.currentPage = 1;
            //scopeObj.view.lbxPagination.selectedKey = scopeObj.currentPage;
            scopeObj.loadPageData();
            scopeObj.resetSortImages();
        };
        this.view.flxProductType.onClick = function() {
            //scopeObj.setDataForTypeFilter("filterClick");
            var filterLeft = scopeObj.view.flxProductType.frame.x;
            scopeObj.view.flxProductTypeFilter.left = (filterLeft - 35) + "px";
            scopeObj.view.flxProductStatusFilter.setVisibility(false);
            if (scopeObj.view.flxProductTypeFilter.isVisible) {
                scopeObj.view.flxProductTypeFilter.setVisibility(false);
            } else {
                scopeObj.view.flxProductTypeFilter.setVisibility(true);
            }
            scopeObj.view.forceLayout();
        };
        this.view.subHeader.flxClearSearchImage.onClick = function() {
            scopeObj.view.subHeader.tbxSearchBox.text = "";
            scopeObj.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100";
            scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
            scopeObj.loadPageData();
            scopeObj.hideNoResultsFound();
            scopeObj.view.forceLayout();
        };
        this.view.subHeader.tbxSearchBox.onKeyUp = function() {
            scopeObj.loadPageData();
            if (scopeObj.view.subHeader.tbxSearchBox.text === "") {
                scopeObj.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100";
                scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
                scopeObj.hideNoResultsFound();
                scopeObj.view.forceLayout();
            } else {
                scopeObj.view.subHeader.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
                scopeObj.view.subHeader.flxClearSearchImage.setVisibility(true);
            }
            if (scopeObj.records === 0) {
                scopeObj.showNoResultsFound();
                scopeObj.view.forceLayout();
            } else {
                scopeObj.hideNoResultsFound();
                scopeObj.view.forceLayout();
            }
        };
        this.view.flxProductStatus.onClick = function() {
            scopeObj.view.flxProductTypeFilter.setVisibility(false);
            var filterLeft = scopeObj.view.flxProductStatus.frame.x;
            scopeObj.view.flxProductStatusFilter.left = (filterLeft - 85) + "px";
            if (scopeObj.view.flxProductStatusFilter.isVisible) {
                scopeObj.view.flxProductStatusFilter.setVisibility(false);
            } else {
                scopeObj.view.flxProductStatusFilter.setVisibility(true);
            }
            scopeObj.view.forceLayout();
        };
        this.view.segProducts.onRowClick = function() {
            scopeObj.productDetailsView();
        };
        this.view.productTypeFilterMenu.segStatusFilterDropdown.onRowClick = function() {
            scopeObj.currentPage = 1;
            scopeObj.performTypeFilter();
        };
        this.view.statusFilterMenu.segStatusFilterDropdown.onRowClick = function() {
            scopeObj.currentPage = 1;
            scopeObj.performStatusFilter();
        };
        this.view.breadcrumbs.btnBackToMain.onClick = function() {
            scopeObj.view.flxBreadCrum.setVisibility(false);
            scopeObj.renderProductsList();
        };
        this.view.subHeader.tbxSearchBox.onTouchStart = function() {
            scopeObj.view.subHeader.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
        };
        //     this.view.subHeader.lbxPageNumbers.onSelection = function() {
        //       scopeObj.changeNumberOfRecordsPerPage();
        //     };
        //     this.view.lbxPagination.onSelection = function() {
        //       scopeObj.gotoPage();
        //     };
        //     this.view.lblIconPrevious.onTouchStart = function() {
        //       scopeObj.prevPage();
        //     };
        //     this.view.lblIconNext.onTouchStart = function() {
        //       scopeObj.nextPage();
        //     };
        this.view.productDetailsView.lblProductFeatures.onClick = function() {
            scopeObj.view.productDetailsView.flxDropDown1.onClick();
        };
        this.view.productDetailsView.flxDropDown1.onClick = function() {
            if (scopeObj.view.productDetailsView.rtxViewDescription.isVisible === true) {
                scopeObj.view.productDetailsView.fontIconImgView1.text = "\ue922";
                scopeObj.view.productDetailsView.fontIconImgView1.skin = "sknfontIconDescRightArrow14px";
                scopeObj.view.productDetailsView.rtxViewDescription.setVisibility(false);
            } else {
                scopeObj.view.productDetailsView.fontIconImgView1.text = "\ue915";
                scopeObj.view.productDetailsView.fontIconImgView1.skin = "sknfontIconDescDownArrow12px";
                scopeObj.view.productDetailsView.rtxViewDescription.setVisibility(true);
            }
        };
        this.view.productDetailsView.lblChargesAndFees.onClick = function() {
            scopeObj.view.productDetailsView.flxDropDown2.onClick();
        };
        this.view.productDetailsView.flxDropDown2.onClick = function() {
            if (scopeObj.view.productDetailsView.rtxChargesFee.isVisible === true) {
                scopeObj.view.productDetailsView.fontIconImgView2.text = "\ue922";
                scopeObj.view.productDetailsView.fontIconImgView2.skin = "sknfontIconDescRightArrow14px";
                scopeObj.view.productDetailsView.rtxChargesFee.setVisibility(false);
            } else {
                scopeObj.view.productDetailsView.fontIconImgView2.text = "\ue915";
                scopeObj.view.productDetailsView.fontIconImgView2.skin = "sknfontIconDescDownArrow12px";
                scopeObj.view.productDetailsView.rtxChargesFee.setVisibility(true);
            }
        };
        this.view.productDetailsView.lblAdditionalInfo.onClick = function() {
            scopeObj.view.productDetailsView.flxDropDown3.onClick();
        };
        this.view.productDetailsView.flxDropDown3.onClick = function() {
            if (scopeObj.view.productDetailsView.rtxAdditionalInfo.isVisible === true) {
                scopeObj.view.productDetailsView.fontIconImgView3.text = "\ue922";
                scopeObj.view.productDetailsView.fontIconImgView3.skin = "sknfontIconDescRightArrow14px";
                scopeObj.view.productDetailsView.rtxAdditionalInfo.setVisibility(false);
            } else {
                scopeObj.view.productDetailsView.fontIconImgView3.text = "\ue915";
                scopeObj.view.productDetailsView.fontIconImgView3.skin = "sknfontIconDescDownArrow12px";
                scopeObj.view.productDetailsView.rtxAdditionalInfo.setVisibility(true);
            }
        };
        this.view.flxDeactivate.onClick = function() {};
        this.view.flxEdit.onClick = function() {};
        this.view.flxProductTypeFilter.onHover = scopeObj.onHoverEventCallback;
        this.view.flxProductStatusFilter.onHover = scopeObj.onHoverEventCallback;
        this.view.flxSelectOptions.onHover = scopeObj.onHoverEventCallback;
    },
    onHoverEventCallback: function(widget, context) {
        var scopeObj = this;
        var widGetId = widget.id;
        if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
            scopeObj.view[widGetId].setVisibility(true);
        } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
            scopeObj.view[widGetId].setVisibility(false);
        }
    },
    showNoResultsFound: function() {
        this.view.flxProductsHeader.setVisibility(false);
        //this.view.flxProductList.setVisibility(false);
        this.view.rtxNoRecords.text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found") + '"' + this.view.subHeader.tbxSearchBox.text + '"' + kony.i18n.getLocalizedString("i18n.frmPoliciesController.Try_with_another_keyword");
        this.view.rtxNoRecords.setVisibility(true);
        this.view.flxNoRecordsFound.setVisibility(true);
        //     this.view.flxPagination.setVisibility(false);
    },
    hideNoResultsFound: function() {
        this.view.flxProductsHeader.setVisibility(true);
        this.view.flxProductList.setVisibility(true);
        this.view.rtxNoRecords.setVisibility(false);
        //     this.view.flxPagination.setVisibility(true);
    },
    //   changeNumberOfRecordsPerPage: function() {
    //     this.currentPage = 1;
    //     this.loadPageData();
    //   },
    renderProductsList: function() {
        var scope = this;
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmProductController.Banking_Product_Management");
        if (this.productsList) {
            if (this.productsList.length === 0) {
                //show Add new screen
                this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmProductController.Banking_Product_Management");
                this.view.flxBreadCrum.setVisibility(false);
                this.view.segProducts.setData([]);
                this.view.flxProductListContainer.setVisibility(true);
                //this.view.FlexProductTable.height=this.view.flxProductList.frame.height;
                this.view.mainHeader.flxHeaderSeperator.setVisibility(false);
                this.view.FlexProducts.setVisibility(false);
            } else {
                //display the list
                this.view.mainHeader.flxHeaderSeperator.setVisibility(true);
                this.sortBy = scope.getObjectSorter("productName");
                scope.resetSortImages();
                this.loadPageData = function() {
                    scope.showProductsList(scope.productsList.sort(scope.sortBy.sortData), false);
                    var searchResult = scope.productsList.filter(scope.searchFilter).sort(this.sortBy.sortData);
                    scope.records = scope.productsList.filter(scope.searchFilter).length;
                    scope.showProductsUI(searchResult);
                    //             if(scope.nextPageDisabled){
                    //             scope.view.flxNext.hoverSkin ="sknDisableCursor";
                    //             scope.view.lblIconNext.skin="sknFontIconPrevNextDisable";
                    //           }else{
                    //             scope.view.flxNext.hoverSkin ="sknCursor";
                    //             scope.view.lblIconNext.skin= "sknFontIconPrevNextPage";
                    //           }
                    //           if(scope.prevPageDisabled){
                    //             scope.view.flxPrevious.hoverSkin ="sknDisableCursor";
                    //             scope.view.lblIconPrevious.skin="sknFontIconPrevNextDisable";
                    //           }else{
                    //             scope.view.flxPrevious.hoverSkin ="sknCursor";
                    //             scope.view.lblIconPrevious.skin="sknFontIconPrevNextPage";
                    //           }
                };
                this.loadPageData();
                this.view.forceLayout();
            }
        }
    },
    updateContextualMenu: function() {
        var data = this.view.segProducts.data;
        if (data[this.rowIndex].lblServiceStatus.text === kony.i18n.getLocalizedString("i18n.secureimage.Active")) {
            this.view.lblOption2.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate");
            this.view.lblIconOption1.text = "\ue91c";
        } else {
            this.view.lblOption2.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Activate");
            this.view.lblIconOption1.text = "\ue931";
        }
    },
    //   getProductsList:function(){
    //     this.presenter.fetchAllProducts();
    //   },
    showProductsList: function(res, context) {
        this.view.flxSelectOptions.isVisible = false;
        //this.view.flxBreadCrum.setVisibility(false);
        this.view.flxMainContent.setVisibility(true);
        this.view.FlexProducts.setVisibility(true);
        this.view.flxMainSubHeader.setVisibility(true);
        this.view.flxSelectOptions.setVisibility(false);
        this.view.mainHeader.flxHeaderSeperator.setVisibility(true);
        this.view.flxViewProductDetails.setVisibility(false);
        this.view.flxNoRecordsFound.setVisibility(false);
        this.view.flxProductList.setVisibility(true);
        this.view.flxProductsHeader.setVisibility(true);
        //this.view.FlexProductTable.skin = "Copysknflxffffffop0e23e7fddf7bd4b";
        if (res !== null && res !== undefined) {
            this.SetProductSegmentData(res, context);
        }
    },
    SetProductSegmentData: function(resData, isFilter) {
        var self = this;
        var data = [];
        //this.SegProductsData=resData;
        this.view.lblProductName.text = kony.i18n.getLocalizedString("i18n.frmProductController.PRODUCT_NAME");
        this.view.lblProductType.text = kony.i18n.getLocalizedString("i18n.frmProductController.PRODUCT_TYPE");
        this.view.lblStatus.text = kony.i18n.getLocalizedString("i18n.roles.STATUS");
        var dataMap = {
            flxProducts: "flxOutage",
            flxStatus: "flxStatus",
            lblIconOptions: "lblIconOptions",
            fonticonActive: "fonticonActive",
            lblProductName: "lblProductName",
            lblSeparator: "lblSeparator",
            lblProductType: "lblProductType",
            lblServiceStatus: "lblServiceStatus"
        };
        if (isFilter && this.view.subHeader.tbxSearchBox.text !== "") {
            resData = resData.filter(self.searchFilter).sort(this.sortBy.sortData);
        }
        if (resData) {
            data = resData.map(self.toProductSegment.bind(self));
        } else if (resData == []) resData.map(this.toProductSegment.bind(self));
        var statusFilter = [];
        var typeFilter = [];
        this.allProductsList = data;
        for (var i = 0; i < data.length; i++) {
            if (!statusFilter.contains(data[i].lblServiceStatus.text)) statusFilter.push(data[i].lblServiceStatus.text);
        }
        for (var j = 0; j < data.length; j++) {
            if (!typeFilter.contains(data[j].lblProductType)) typeFilter.push(data[j].lblProductType);
        }
        if (!isFilter && self.currentPage === 1) {
            self.setProductStatusFilterData(statusFilter);
            self.setProductTypeFilterData(typeFilter);
        }
        if (data.length > 8) this.view.flxProductStatus.right = "124px";
        else this.view.flxProductStatus.right = "115px";
        this.view.segProducts.widgetDataMap = dataMap;
        this.view.segProducts.setData(data);
        this.view.forceLayout();
    },
    setProductStatusFilterData: function(data) {
        var self = this;
        var widgetMap = {
            "flxSearchDropDown": "flxSearchDropDown",
            "flxCheckBox": "flxCheckBox",
            "imgCheckBox": "imgCheckBox",
            "lblDescription": "lblDescription"
        };
        var filterData = data.map(function(data) {
            return {
                "flxSearchDropDown": "flxSearchDropDown",
                "flxCheckBox": "flxCheckBox",
                "lblDescription": data,
                "imgCheckBox": {
                    "src": "checkbox.png"
                }
            };
        });
        self.view.statusFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
        self.view.statusFilterMenu.segStatusFilterDropdown.setData(filterData);
        var indices = [];
        for (index = 0; index < filterData.length; index++) {
            indices.push(index);
        }
        self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices = [
            [0, indices]
        ];
    },
    setProductTypeFilterData: function(data) {
        var self = this;
        var widgetMap = {
            "flxSearchDropDown": "flxSearchDropDown",
            "flxCheckBox": "flxCheckBox",
            "imgCheckBox": "imgCheckBox",
            "lblDescription": "lblDescription"
        };
        var filterData = data.map(function(data) {
            return {
                "flxSearchDropDown": "flxSearchDropDown",
                "flxCheckBox": "flxCheckBox",
                "lblDescription": data,
                "imgCheckBox": {
                    "src": "checkbox.png"
                }
            };
        });
        self.view.productTypeFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
        if (filterData.length > 3) self.view.flxProductStatusFilter.width = "170dp";
        else self.view.flxProductStatusFilter.width = "120dp";
        self.view.productTypeFilterMenu.segStatusFilterDropdown.setData(filterData);
        var indices = [];
        for (index = 0; index < filterData.length; index++) {
            indices.push(index);
        }
        self.view.productTypeFilterMenu.segStatusFilterDropdown.selectedIndices = [
            [0, indices]
        ];
        self.view.forceLayout();
    },
    toProductSegment: function(productData) {
        var self = this;
        var statusText = "",
            statusSkin = "",
            statusImgSkin = "";
        if (productData.Status_id === kony.i18n.getLocalizedString("i18n.frmProductController.SID_PRO_ACTIVE")) {
            statusText = kony.i18n.getLocalizedString("i18n.secureimage.Active");
            statusSkin = "sknlblLato5bc06cBold14px";
            statusImgSkin = "sknFontIconActivate";
        } else if (productData.Status_id === kony.i18n.getLocalizedString("i18n.frmProductController.SID_PRO_INACTIVE")) {
            statusText = kony.i18n.getLocalizedString("i18n.secureimage.Inactive");
            statusSkin = "sknlblLatocacacaBold12px";
            statusImgSkin = "sknfontIconInactive";
        }
        return {
            lblIconOptions: {
                "text": "\ue91f",
                "skin": "sknFontIconOptionMenu"
            },
            fonticonActive: {
                "skin": statusImgSkin
            },
            lblProductName: productData.productName,
            lblProductType: productData.productType,
            lblSeparator: ".",
            lblServiceStatus: {
                text: statusText,
                skin: statusSkin
            },
            template: "flxProducts"
        };
    },
    resetSortImages: function() {
        this.determineSortFontIcon(this.sortBy, 'productName', this.view.fontIconSortProductName);
    },
    //     disableNextPrev: function() {
    //     var data = this.view.lbxPagination.masterData;
    //     var noOfPages = data.length;
    //     if (this.currentPage === 1) {
    //       this.prevPageDisabled = true;
    //     } else if (this.currentPage === noOfPages) {
    //       this.nextPageDisabled = true;
    //     } else {
    //       this.prevPageDisabled = false;
    //       this.nextPageDisabled = false;
    //     }
    //   },
    //   getNumPerPage: function() {
    //     return this.view.subHeader.lbxPageNumbers.selectedKeyValue ? this.view.subHeader.lbxPageNumbers.selectedKeyValue[1] : "10";
    //   },
    //   gotoPage: function() {
    //     this.currentPage = this.view.lbxPagination.selectedKey;
    //     this.loadPageData();
    //     this.view.lbxPagination.selectedKey = this.currentPage;
    //     //this.disableNextPrev();
    //   },
    //   nextPage: function() {
    //     if (this.nextPageDisabled) {
    //       return;
    //     }
    //     this.currentPage++;
    //     // this.disableNextPrev();
    //     this.view.lbxPagination.selectedKey = this.currentPage;
    //     this.loadPageData();
    //   },
    //   prevPage: function() {
    //     if (this.prevPageDisabled) {
    //       return;
    //     }
    //     this.currentPage--;
    //     //this.disableNextPrev();
    //     this.view.lbxPagination.selectedKey = this.currentPage;
    //     this.loadPageData();
    //   },
    //   selectPage: function() {
    //     this.currentPage = this.view.lbxPagination.selectedKey;
    //     this.loadPageData();
    //   },
    //   assignPageList: function(pageData) {
    //     var lbxPagination = this.view.lbxPagination;
    //     var selectedPage = Number(lbxPagination.selectedKey) || 1;
    //     lbxPagination.masterData = pageData;
    //     lbxPagination.selectedKey = selectedPage;
    //   },
    showProductsUI: function(searchResult) {
        if (this.records.length !== 0) {
            this.hideNoResultsFound();
            this.showProductsList(searchResult, false);
        } else {
            this.showNoResultsFound();
            this.view.forceLayout();
        }
        this.view.forceLayout();
    },
    performStatusFilter: function() {
        var self = this;
        var selStatus = [];
        var selInd;
        var dataToShow = [];
        var allData = self.productsList;
        var segStatusData = self.view.statusFilterMenu.segStatusFilterDropdown.data;
        var indices = self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices;
        self.inFilter = true;
        if (indices !== null) { //show selected indices data
            selInd = indices[0][1];
            for (var i = 0; i < selInd.length; i++) {
                selStatus.push(self.view.statusFilterMenu.segStatusFilterDropdown.data[selInd[i]].lblDescription);
            }
            if (selInd.length === segStatusData.length) { //all are selected
                //self.view.flxPagination.setVisibility(true);
                //self.view.subHeader.flxMenu.setVisibility(true);
                self.inFilter = false;
                self.SetProductSegmentData(self.productsList, true);
            } else {
                dataToShow = allData.filter(function(rec) {
                    if (selStatus.indexOf(rec.Status_id === kony.i18n.getLocalizedString("i18n.frmProductController.SID_PRO_ACTIVE") ? kony.i18n.getLocalizedString("i18n.secureimage.Active") : kony.i18n.getLocalizedString("i18n.secureimage.Inactive")) >= 0) {
                        return rec;
                    }
                });
                if (dataToShow.length > 0) {
                    //self.view.flxPagination.setVisibility(true);
                    //self.view.subHeader.flxMenu.setVisibility(true);
                    self.SetProductSegmentData(dataToShow, true);
                } else {
                    //self.SegProductsData=[];
                    self.view.segProducts.setData([]);
                    //self.view.FlexProductTable.height=self.view.flxProductList.frame.height;
                }
            }
        } else {
            self.view.rtxNoRecords.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
            self.view.rtxNoRecords.setVisibility(true);
            self.view.flxNoRecordsFound.setVisibility(true);
            //this.view.flxProductList.setVisibility(false);
            //self.view.flxPagination.setVisibility(false);
            //self.view.subHeader.flxMenu.setVisibility(false);
            var data = self.view.segProducts.data;
            data = [];
            //self.SegProductsData=[];
            self.view.segProducts.setData(data);
            //self.view.FlexProductTable.height=self.view.flxProductList.frame.height;
        }
        self.view.forceLayout();
    },
    performTypeFilter: function() {
        var self = this;
        var selStatus = [];
        var selInd;
        var dataToShow = [];
        var allData = self.productsList;
        var segTypeData = self.view.productTypeFilterMenu.segStatusFilterDropdown.data;
        var indices = self.view.productTypeFilterMenu.segStatusFilterDropdown.selectedIndices;
        self.inFilter = true;
        if (indices !== null) { //show selected indices data
            selInd = indices[0][1];
            for (var i = 0; i < selInd.length; i++) {
                selStatus.push(self.view.productTypeFilterMenu.segStatusFilterDropdown.data[selInd[i]].lblDescription);
            }
            if (selInd.length === segTypeData.length) {
                if (self.view.subHeader.tbxSearchBox.text === "") { //all are selected
                    //self.view.flxPagination.setVisibility(true);
                    //self.view.subHeader.flxMenu.setVisibility(true);
                    self.inFilter = false;
                    self.SetProductSegmentData(self.productsList, true);
                } else self.loadPageData();
            } else {
                dataToShow = allData.filter(function(rec) {
                    if (selStatus.indexOf(rec.productType) >= 0) {
                        return rec;
                    }
                });
                if (dataToShow.length > 0) {
                    //self.view.flxPagination.setVisibility(true);
                    //self.view.subHeader.flxMenu.setVisibility(true);
                    self.SetProductSegmentData(dataToShow, true);
                } else {
                    //self.SegProductsData=[];
                    self.view.segProducts.setData([]);
                    //self.view.FlexProductTable.height=self.view.flxProductList.frame.height;
                }
            }
        } else {
            self.view.rtxNoRecords.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
            self.view.rtxNoRecords.setVisibility(true);
            self.view.flxNoRecordsFound.setVisibility(true);
            //self.view.flxProductList.setVisibility(false);
            //self.view.flxPagination.setVisibility(false);
            //self.view.subHeader.flxMenu.setVisibility(false);
            var data = self.view.segProducts.data;
            data = [];
            //self.SegProductsData=[];
            self.view.segProducts.setData(data);
            //self.view.FlexProductTable.height=self.view.flxProductList.frame.height;
        }
        self.view.forceLayout();
    },
});
define("MasterDataModule/frmProductControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmProduct **/
    AS_Form_d9031c26bdde47fe93cabcf46ab4e718: function AS_Form_d9031c26bdde47fe93cabcf46ab4e718(eventobject) {
        var self = this;
        this.preShowActions();
    },
    /** onDeviceBack defined for frmProduct **/
    AS_Form_i7e627b88d1d4832ad4472348ecd0e6c: function AS_Form_i7e627b88d1d4832ad4472348ecd0e6c(eventobject) {
        var self = this;
        this.onBrowserBack();
    }
});
define("MasterDataModule/frmProductController", ["MasterDataModule/userfrmProductController", "MasterDataModule/frmProductControllerActions"], function() {
    var controller = require("MasterDataModule/userfrmProductController");
    var controllerActions = ["MasterDataModule/frmProductControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
