define("flxPolicies", function() {
    return function(controller) {
        var flxPolicies = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxPolicies",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknbackGroundffffff100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxPolicies.setDefaultUnit(kony.flex.DP);
        var flxAccordianContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxAccordianContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_gad12a58e60c494384b37fd321e2a1fb,
            "right": "115px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxAccordianContainer.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, {}, {});
        flxArrow.setDefaultUnit(kony.flex.DP);
        var fontIconImgViewDescription = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "fontIconImgViewDescription",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknfontIconDescRightArrow14px",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(fontIconImgViewDescription);
        var lblPolicyName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPolicyName",
            "isVisible": true,
            "left": "35px",
            "right": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Username Policy for end customer",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccordianContainer.add(flxArrow, lblPolicyName);
        var lblPoliciesSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblPoliciesSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": 0,
            "skin": "sknSeparator",
            "text": ".",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxViewEditButton = new kony.ui.Button({
            "bottom": "0px",
            "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "height": "22px",
            "id": "flxViewEditButton",
            "isVisible": true,
            "right": "35px",
            "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
            "text": "Edit",
            "top": "15px",
            "width": "50px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
        });
        flxPolicies.add(flxAccordianContainer, lblPoliciesSeperator, flxViewEditButton);
        return flxPolicies;
    }
})