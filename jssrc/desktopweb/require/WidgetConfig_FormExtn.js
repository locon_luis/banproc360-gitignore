define({
    //See permissionsConfig.js for expected format
    components: {
        leftMenuNew: {
            "virtualDOM": {
                "Dashboard": "ViewDashboard",
                "Messages": "ViewMessages",
                "Alerts": "ViewAlerts",
                "REPORTS & LOGS": {
                    "Reports": "ViewReports",
                    "Transactions & Audit Logs": "ViewLogs"
                },
                "MEMBER MANAGEMENT": {
                    "Customers": "ViewCustomer",
                    "Groups": "ViewGroup",
                    "Companies": "ViewCompanies"
                },
                "EMPLOYEE MANAGEMENT": {
                    "Users": "ViewUser",
                    "Roles": "ViewRoles",
                    "Permissions": "ViewPermissions"
                },
                "CONFIGURATIONS": {
                    "Configuration Bundles": "ViewConfigurationBundles",
                    "Business Configurations": "ViewConfigurations"
                },
                "APPLICATIONS CONTENT MANAGEMENT": {
                    "Privacy Policies": "ViewAppContent",
                    "FAQs": "ViewAppContent",
                    "Credential Policies": "ViewAppContent",
                    "T&C": "ViewAppContent",
                    "Service Outage Message": "ViewAppContent",
                    "Locations": "ViewAppContent",
                    "Customer Care Information": "ViewAppContent",
                    "Ad Management": "ViewCampaign"
                },
                "MASTER DATA MANAGEMENT": {
                    "Products": "ViewAppContent",
                    "Banks for transfer": "NotImplemented",
                    "Services": "ViewEntitlement",
                    "Schedule Master": "NotImplemented",
                    "Secure Images": "ViewAppContent",
                    "Security Questions": "ViewAppContent",
                    "Decision Management": "ViewDecisionFiles"
                },
                "Change Password": "ChangePassword",
                "SECURITY & AUTHENTICATION": {
                    "Credential Policy": "ViewIdentityManagement",
                    "Password Settings": "ViewIdentityManagement",
                    "MFA Configurations": "ViewMFAConfig",
                    "MFA Scenarios": "ViewMFAConfig"
                }
            }
        }
    },
    forms: {
        frmDashboard: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "flxTabs": "ViewMessages",
            "flxAlertSegment": "ViewDashboard",
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmLoansDashboard: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"],
            "dashboardCommonTab": {
                "btnBanking": "NotImplemented"
            }
        },
        frmGuestDashboard: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmCSR: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "frmCSR_btnCreateNewMessage": "CreateNewMessage",
                "frmCSR_btnCreateMessageTemplate": "CreateMessageTemplate"
            },
            "MessageFilter": {
                "SegSearchResult": ["segment", {
                    "flxOptions": "UpdateMessages"
                }]
            },
            "editMessages": {
                "btnStatus": "UpdateMessages",
                "btnAssign": "UpdateMessages",
                "btnReply": "UpdateMessages"
            },
            "TemplateMessage": {
                "btnAdd": "UpdateMessageTemplate",
                "btnEdit": "UpdateMessageTemplate"
            },
            "listingSegment": {
                "segListing": ["segment", {
                    "flxOptions": "UpdateMessageTemplate"
                }]
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmProduct: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmAssistedOnboarding: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmConfigurationBundles: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"],
            "mainHeader": {
                "btnAddNewOption": "UpdateConfigurationBundles"
            },
            "lblFonticonBundleEdit": "UpdateConfigurationBundles",
            "flxAddConfigurationButton": "UpdateConfigurationBundles",
            "configurationData": {
                "segConfiguration": ["segment", {
                    "flxOptions": "UpdateConfigurationBundles"
                }]
            },
            "bundle0": {
                "lblBundleEdit": "UpdateConfigurationBundles",
                "lblBundleDelete": "UpdateConfigurationBundles"
            },
            "bundle1": {
                "lblBundleEdit": "UpdateConfigurationBundles",
                "lblBundleDelete": "UpdateConfigurationBundles"
            },
            "bundle2": {
                "lblBundleEdit": "UpdateConfigurationBundles",
                "lblBundleDelete": "UpdateConfigurationBundles"
            },
            "bundle3": {
                "lblBundleEdit": "UpdateConfigurationBundles",
                "lblBundleDelete": "UpdateConfigurationBundles"
            },
            "bundle4": {
                "lblBundleEdit": "UpdateConfigurationBundles",
                "lblBundleDelete": "UpdateConfigurationBundles"
            },
            "bundle5": {
                "lblBundleEdit": "UpdateConfigurationBundles",
                "lblBundleDelete": "UpdateConfigurationBundles"
            },
            "bundle6": {
                "lblBundleEdit": "UpdateConfigurationBundles",
                "lblBundleDelete": "UpdateConfigurationBundles"
            },
            "bundle7": {
                "lblBundleEdit": "UpdateConfigurationBundles",
                "lblBundleDelete": "UpdateConfigurationBundles"
            }
        },
        frmBusinessConfigurations: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"],
            "flxAddButton": "UpdateConfigurations",
            "criteriaList": {
                "segCriteria": ["segment", {
                    "flxOptions": "UpdateConfigurations"
                }]
            }
        },
        frmPolicies: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"],
            "flxPolicyDescription": {
                "flxAddNewButton": {
                    "btnAddNew": "CreateUpdateIdentityManagement"
                },
                "flxAddNewPolicyDescription": {
                    "flxCommonButtons": {
                        "btnSave": "CreateUpdateIdentityManagement"
                    }
                },
                "segPolicyDescriptions": ["segment", {
                    "lblEdit": "CreateUpdateIdentityManagement",
                    "lblDelete": "CreateUpdateIdentityManagement"
                }]
            },
            "customersPolicies": {
                "lblEditIconUsername": "CreateUpdateIdentityManagement",
                "lblEditIconPassword": "CreateUpdateIdentityManagement"
            },
            "policiesRulesView": {
                "lblIconOption1": "CreateUpdateIdentityManagement"
            },
            "commonButtons": {
                "btnSave": "CreateUpdateIdentityManagement"
            }
        },
        frmPasswordAgeAndLockoutSettings: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"],
            "btnUpdate": "CreateUpdateIdentityManagement"
        },
        frmAlertsManagement: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "btnsave": "UpdateAlerts",
            "btnUpdateSequencePopup": "UpdateAlerts",
            "btnTemplateSave": "UpdateAlerts",
            "btnAddAlerts": "UpdateAlerts",
            "btnReorderAlerts": "UpdateAlerts",
            "btnReorderCategories": "UpdateAlerts",
            "btnAddSubAlerts": "UpdateAlerts",
            "popUpDeactivate": {
                "btnPopUpDelete": "UpdateAlerts"
            },
            "addAlertTypeButtons": {
                "btnSave": "UpdateAlerts"
            },
            "EditAlertCategoryButtons": {
                "btnSave": "UpdateAlerts"
            },
            "flxCategoryOptions": "UpdateAlerts",
            "flxOptions": "UpdateAlerts",
            "flxOptionsSubAlerts": "UpdateAlerts",
            "flxAddTemplateButton": "UpdateAlerts",
            "flxOptionEdit": "UpdateAlerts",
            "segListing": ["segment", {
                "flxOptions": "UpdateAlerts"
            }],
            "segSubAlerts": ["segment", {
                "flxOptions": "UpdateAlerts"
            }],
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmLogs: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "flxLogDefaultTabs1": "ViewTransactionLogs",
            "flxLogDefaultTabs2": "ViewAdminLogs",
            "flxLogDefaultTabs3": "ViewCustomerActivityLogs",
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmReportsManagement: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "flxReportsTabs1": "ViewUserReports",
            "flxReportsTabs2": "ViewTransactionReports",
            "flxReportsTabs3": "ViewMessageReports",
            "search": {
                "flxDownload": "NotImplemented"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmServiceManagement: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "commonButtons": {
                "btnNext": "NotImplemented"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateService"
            },
            "flxViewEditButton": "UpdateServiceDesc",
            "listingSegment": {
                "contextualMenu": {
                    "flxOption1": "UpdateServiceDesc",
                    "flxOption2": "ModifyServiceStatus"
                }
            },
            "flxOperations": "NotImplemented",
            "lblSchedule": "NotImplemented",
            "flxScheduleClick": "NotImplemented",
            "verticalTabs": {
                "flxOption2": ["all", ["UpdateServiceLimitsFee", "NotImplemented"]],
                "flxOption3": "NotImplemented"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmCustomerManagement: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            leftMenuNew: ["component", "leftMenuNew"],
            btnCreateCustomer: "DoAssistedOnboarding"
        },
        frmCustomerProfileAccounts: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateCustomer"
            },
            flxGeneralInfoWrapper: {
                generalInfoHeader: {
                    flxCSRAssist: "CSRAssist",
                    flxUnlock: "UpdateCustomer"
                },
                dashboardCommonTab: {
                    "btnBanking": "NotImplemented"
                },
                flxSelectOptions: {
                    "flxUpgrade": "UpgradeRetailUserToBusiness"
                }
            },
            "segGroups": ["segment", {
                "flxDelete": "UpdateCustomerGroup"
            }],
            "btnGroupsEdit": ["any", ["UpdateCustomer", "UpdateCustomerGroup", "AssignCustomerGroup"]],
            "ContactPrefTimeMethod": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "Address": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactNum": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactEmail": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "tabs": {
                "btnTabName7": ["all", ["UpdateCustomerDevice"]],
                "btnTabName8": "UpdateCustomerEntitlements"
            },
            "entitlementsContextualMenu": {
                "flxOption1": "NotImplemented"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmCustomerProfileActivityHistory: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateCustomer"
            },
            flxGeneralInfoWrapper: {
                generalInfoHeader: {
                    flxCSRAssist: "CSRAssist",
                    flxUnlock: "UpdateCustomer"
                },
                dashboardCommonTab: {
                    "btnBanking": "NotImplemented"
                },
                flxSelectOptions: {
                    "flxUpgrade": "UpgradeRetailUserToBusiness"
                }
            },
            "segGroups": ["segment", {
                "flxDelete": "UpdateCustomerGroup"
            }],
            "btnGroupsEdit": ["any", ["UpdateCustomer", "UpdateCustomerGroup", "AssignCustomerGroup"]],
            "ContactPrefTimeMethod": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "Address": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactNum": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactEmail": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "tabs": {
                "btnTabName7": ["all", ["UpdateCustomerDevice"]],
                "btnTabName8": "UpdateCustomerEntitlements"
            },
            "entitlementsContextualMenu": {
                "flxOption1": "NotImplemented"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmCustomerProfileAlerts: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateCustomer"
            },
            flxGeneralInfoWrapper: {
                generalInfoHeader: {
                    flxCSRAssist: "CSRAssist",
                    flxUnlock: "UpdateCustomer"
                },
                dashboardCommonTab: {
                    "btnBanking": "NotImplemented"
                },
                flxSelectOptions: {
                    "flxUpgrade": "UpgradeRetailUserToBusiness"
                }
            },
            "segGroups": ["segment", {
                "flxDelete": "UpdateCustomerGroup"
            }],
            "btnGroupsEdit": ["any", ["UpdateCustomer", "UpdateCustomerGroup", "AssignCustomerGroup"]],
            "ContactPrefTimeMethod": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "Address": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactNum": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactEmail": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "tabs": {
                "btnTabName7": ["all", ["UpdateCustomerDevice"]],
                "btnTabName8": "UpdateCustomerEntitlements"
            },
            "entitlementsContextualMenu": {
                "flxOption1": "NotImplemented"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmCustomerProfileContacts: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateCustomer"
            },
            flxGeneralInfoWrapper: {
                generalInfoHeader: {
                    flxCSRAssist: "CSRAssist",
                    flxUnlock: "UpdateCustomer"
                },
                dashboardCommonTab: {
                    "btnBanking": "NotImplemented"
                },
                flxSelectOptions: {
                    "flxUpgrade": "UpgradeRetailUserToBusiness"
                }
            },
            "segGroups": ["segment", {
                "flxDelete": "UpdateCustomerGroup"
            }],
            "btnGroupsEdit": ["any", ["UpdateCustomer", "UpdateCustomerGroup", "AssignCustomerGroup"]],
            "ContactPrefTimeMethod": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "Address": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactNum": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactEmail": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "tabs": {
                "btnTabName7": ["all", ["UpdateCustomerDevice"]],
                "btnTabName8": "UpdateCustomerEntitlements"
            },
            "entitlementsContextualMenu": {
                "flxOption1": "NotImplemented"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmCustomerProfileDeviceInfo: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateCustomer"
            },
            flxGeneralInfoWrapper: {
                generalInfoHeader: {
                    flxCSRAssist: "CSRAssist",
                    flxUnlock: "UpdateCustomer"
                },
                dashboardCommonTab: {
                    "btnBanking": "NotImplemented"
                },
                flxSelectOptions: {
                    "flxUpgrade": "UpgradeRetailUserToBusiness"
                }
            },
            "segGroups": ["segment", {
                "flxDelete": "UpdateCustomerGroup"
            }],
            "btnGroupsEdit": ["any", ["UpdateCustomer", "UpdateCustomerGroup", "AssignCustomerGroup"]],
            "ContactPrefTimeMethod": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "Address": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactNum": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactEmail": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "tabs": {
                "btnTabName7": ["all", ["UpdateCustomerDevice"]],
                "btnTabName8": "UpdateCustomerEntitlements"
            },
            "entitlementsContextualMenu": {
                "flxOption1": "NotImplemented"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmCustomerProfileEntitlements: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateCustomer"
            },
            flxGeneralInfoWrapper: {
                generalInfoHeader: {
                    flxCSRAssist: "CSRAssist",
                    flxUnlock: "UpdateCustomer"
                },
                dashboardCommonTab: {
                    "btnBanking": "NotImplemented"
                },
                flxSelectOptions: {
                    "flxUpgrade": "UpgradeRetailUserToBusiness"
                }
            },
            "segGroups": ["segment", {
                "flxDelete": "UpdateCustomerGroup"
            }],
            "btnGroupsEdit": ["any", ["UpdateCustomer", "UpdateCustomerGroup", "AssignCustomerGroup"]],
            "ContactPrefTimeMethod": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "Address": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactNum": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactEmail": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "tabs": {
                "btnTabName7": ["all", ["UpdateCustomerDevice"]],
                "btnTabName8": "UpdateCustomerEntitlements"
            },
            "entitlementsContextualMenu": {
                "flxOption1": "NotImplemented"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmCustomerProfileHelpCenter: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateCustomer"
            },
            flxGeneralInfoWrapper: {
                generalInfoHeader: {
                    flxCSRAssist: "CSRAssist",
                    flxUnlock: "UpdateCustomer"
                },
                dashboardCommonTab: {
                    "btnBanking": "NotImplemented"
                },
                flxSelectOptions: {
                    "flxUpgrade": "UpgradeRetailUserToBusiness"
                }
            },
            "segGroups": ["segment", {
                "flxDelete": "UpdateCustomerGroup"
            }],
            "btnGroupsEdit": ["any", ["UpdateCustomer", "UpdateCustomerGroup", "AssignCustomerGroup"]],
            "ContactPrefTimeMethod": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "Address": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactNum": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactEmail": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "tabs": {
                "btnTabName7": ["all", ["UpdateCustomerDevice"]],
                "btnTabName8": "UpdateCustomerEntitlements"
            },
            "entitlementsContextualMenu": {
                "flxOption1": "NotImplemented"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmCustomerProfileRoles: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateCustomer"
            },
            flxGeneralInfoWrapper: {
                generalInfoHeader: {
                    flxCSRAssist: "CSRAssist",
                    flxUnlock: "UpdateCustomer"
                },
                dashboardCommonTab: {
                    "btnBanking": "NotImplemented"
                },
                flxSelectOptions: {
                    "flxUpgrade": "UpgradeRetailUserToBusiness"
                }
            },
            "segGroups": ["segment", {
                "flxDelete": "UpdateCustomerGroup"
            }],
            "btnGroupsEdit": ["any", ["UpdateCustomer", "UpdateCustomerGroup", "AssignCustomerGroup"]],
            "ContactPrefTimeMethod": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "Address": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactNum": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "ContactEmail": {
                "btnEdit": ["any", ["UpdateCustomer", "UpdateCustomerContact"]],
                "btnAdd": ["any", ["UpdateCustomer", "UpdateCustomerContact"]]
            },
            "tabs": {
                "btnTabName7": ["all", ["UpdateCustomerDevice"]],
                "btnTabName8": "UpdateCustomerEntitlements"
            },
            "entitlementsContextualMenu": {
                "flxOption1": "NotImplemented"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmGroups: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "noStaticData": {
                "btnAddStaticContent": "CreateGroup"
            },
            "segViewSegment": ["segment", {
                "flxEntitlementsConfigure": "NotImplemented"
            }],
            "listingSegment": {
                "segListing": ["segment", {
                    "flxOptions": ["any", ["CreateUpdateGroupEntitlements", "AssignCustomerGroup", "UpdateGroup", "ModifyGroupStatus"]]
                }],
                "contextualMenu": {
                    "lblHeader": ["any", ["UpdateGroup", "CreateUpdateGroupEntitlements", "AssignCustomerGroup"]],
                    "flxOption2": "UpdateGroup",
                    "flxOption4": "ModifyGroupStatus",
                    "btnLink1": ["any", ["UpdateGroup", "CreateUpdateGroupEntitlements"]],
                    "btnLink2": ["any", ["AssignCustomerGroup", "UpdateGroup"]]
                }
            },
            "btnAddGroupNext": "UpdateGroup",
            "btnEdit": "UpdateGroup",
            "verticalTabs": {
                "flxOption1": "UpdateGroup",
                "flxOption2": ["any", ["UpdateGroup", "CreateUpdateGroupEntitlements"]],
                "flxOption3": ["any", ["UpdateGroup", "AssignCustomerGroup"]]
            },
            "mainHeader": {
                "btnAddNewOption": "CreateGroup"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmUsers: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateUser"
            },
            "flxViewEditButton": "UpdateUser",
            "segUsers": ["segment", {
                "flxOptions": ["any", ["AssignUserRole", "AssignUserPermission", "ModifyUserStatus", "UpdateUser"]]
            }],
            "lblDescription": ["any", ["AssignUserRole", "AssignUserPermission", "UpdateUser"]],
            "flxOption2": "UpdateUser",
            "flxOption3": "ModifyUserStatus",
            "flxOption4": "ModifyUserStatus",
            "btnRoles": ["any", ["AssignUserRole", "UpdateUser"]],
            "btnPermissions": ["any", ["AssignUserPermission", "UpdateUser"]],
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmRoles: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateRoles"
            },
            "flxViewEditButton": "UpdateRoles",
            "segPermissions": ["segment", {
                "flxOptions": ["any", ["AssignUserRole", "ModifyRoleStatus", "UpdateRoles"]]
            }],
            "lblDescription": ["any", ["AssignUserRole", "AssignRolePermissions", "UpdateRoles"]],
            "flxOption2": "UpdateRoles",
            "flxOption4": "ModifyRoleStatus",
            "btnRoles": ["any", ["AssignUserRole", "UpdateRoles"]],
            "btnPermissions": ["any", ["AssignRolePermissions", "UpdateRoles"]],
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmPermissions: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "flxViewEditButton": "UpdatePermission",
            "segPermissions": ["segment", {
                "flxOptions": ["any", ["AssignUserPermission", "ModifyPermissionStatus", "UpdatePermission"]]
            }],
            "lblDescription": ["any", ["AssignUserPermission", "AssignRolePermissions", "UpdatePermission"]],
            "flxOption2": "UpdatePermission",
            "flxOption4": "ModifyPermissionStatus",
            "btnRoles": ["any", ["AssignRolePermissions", "UpdatePermission"]],
            "btnPermissions": ["any", ["AssignUserPermission", "UpdatePermission"]],
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmSecureImage: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "flxPhraseStatus": "ModifyAppContentStatus",
            "segUploadedImageListKA": ["segment", {
                "flxOptions": ["any", ["UpdateAppContent", "ModifyAppContentStatus"]]
            }],
            "flxUploadImage2KA": "CreateAppContent",
            "flxBrowseOrDrag2": "CreateAppContent",
            "flxOption2": "ModifyAppContentStatus",
            "flxOption4": "UpdateAppContent",
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmSecurityQuestions: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainSegment": {
                "segMain": ["segment", {
                    "flxOptions": ["any", ["UpdateAppContent", "ModifyAppContentStatus"]]
                }],
                "flxOption1": "ModifyAppContentStatus",
                "flxOption2": "UpdateAppContent"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateAppContent"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmTermsAndConditions: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "noStaticData": {
                "btnAddStaticContent": "CreateAppContent",
                "lblNoStaticContentMsg": "CreateAppContent"
            },
            "staticData": {
                "flxOptions": ["any", ["UpdateAppContent", "ModifyAppContentStatus"]],
                "flxEditOption": "UpdateAppContent",
                "flxDeleteOption": "UpdateAppContent",
                "flxDeactivateOption": "ModifyAppContentStatus"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmPrivacyPolicy: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "noStaticData": {
                "btnAddStaticContent": "CreateAppContent",
                "lblNoStaticContentMsg": "CreateAppContent"
            },
            "staticData": {
                "flxOptions": ["any", ["UpdateAppContent", "ModifyAppContentStatus"]],
                "flxEditOption": "UpdateAppContent",
                "flxDeleteOption": "UpdateAppContent",
                "flxDeactivateOption": "ModifyAppContentStatus"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmFAQ: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateAppContent"
            },
            "noStaticData": {
                "btnAddStaticContent": "CreateAppContent",
                "lblNoStaticContentMsg": "CreateAppContent"
            },
            "flxEdit": "UpdateAppContent",
            "flxDelete": "UpdateAppContent",
            "flxDeactivate": "ModifyAppContentStatus",
            "flxDeleteOption": "UpdateAppContent",
            "tableView": {
                "flxHeaderCheckbox": ["any", ["UpdateAppContent", "ModifyAppContentStatus"]],
                "segServicesAndFaq": ["segment", {
                    "flxOptions": ["any", ["UpdateAppContent", "ModifyAppContentStatus"]],
                    "flxCheckbox": ["any", ["UpdateAppContent", "ModifyAppContentStatus"]]
                }]
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmOutageMessage: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateAppContent"
            },
            "flxSelectOptionsHeader": "ModifyAppContentStatus",
            "noStaticData": {
                "btnAddStaticContent": "CreateAppContent",
                "lblNoStaticContentMsg": "CreateAppContent"
            },
            "flxDeactivate": "ModifyAppContentStatus",
            "flxDelete": "UpdateAppContent",
            "flxEdit": "UpdateAppContent",
            "flxDeleteOption": "UpdateAppContent",
            "tableView": {
                "flxHeaderCheckbox": ["any", ["UpdateAppContent", "ModifyAppContentStatus"]],
                "segServicesAndFaq": ["segment", {
                    "flxOutageCheckbox": "ModifyAppContentStatus",
                    "flxOptions": ["any", ["UpdateAppContent", "ModifyAppContentStatus"]]
                }]
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmLocations: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "flxViewEditButton": "UpdateAppContent",
            "btnAddCase": "CreateAppContent",
            "mainHeader": {
                "btnAddNewOption": "CreateAppContent"
            },
            "btnImport": "CreateAppContent",
            "listingSegment": {
                "segListing": ["segment", {
                    "flxOptions": ["any", ["UpdateAppContent", "ModifyAppContentStatus"]]
                }],
                "contextualMenu": {
                    "flxOption1": "UpdateAppContent",
                    "flxOption2": "ModifyAppContentStatus",
                    "flxOption3": "UpdateAppContent"
                }
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmCustomerCare: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "mainHeader": {
                "btnAddNewOption": "CreateAppContent"
            },
            "addNewRow": {
                "btnAddMore": "CreateAppContent"
            },
            "listingSegment": {
                "segListing": ["segment", {
                    "flxOptions": ["any", ["UpdateAppContent", "ModifyAppContentStatus"]]
                }],
                "contextualMenu": {
                    "flxOption1": "UpdateAppContent",
                    "flxOption2": "ModifyAppContentStatus",
                    "flxOption3": "UpdateAppContent"
                }
            },
            "noStaticData": {
                "lblNoStaticContentMsg": "CreateAppContent",
                "btnAddStaticContent": "CreateAppContent"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmCompanies: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"],
            "mainHeader": {
                "btnAddNewOption": "UpdateCompany"
            },
            "btnCompanyDetailEdit": "UpdateCompany",
            "segCompanyDetailAccount": ["segment", {
                "flxUnlink": "UpdateCompany"
            }],
            "btnAddCustomer": "UpdateBusinessUsers",
            "lblNoCustomers": "UpdateBusinessUsers"
        },
        frmCustomerCreate: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"]
        },
        frmMFAConfigurations: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"],
            "segMFAConfigs": ["segment", {
                "flxOptions": "CreateUpdateMFAConfig"
            }],
            "flxEditFooterButtons": "CreateUpdateMFAConfig"
        },
        frmMFAScenarios: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"],
            "segMFAScenarios": ["segment", {
                "flxOptions": "CreateUpdateMFAConfig"
            }],
            "flxEditFooterButtons": "CreateUpdateMFAConfig",
            "mainHeader": {
                "btnAddNewOption": "CreateUpdateMFAConfig"
            },
            "flxOptions": "CreateUpdateMFAConfig"
        },
        frmUpgradeUser: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            leftMenuNew: ["component", "leftMenuNew"]
        },
        frmDecisionManagement: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            "leftMenuNew": ["component", "leftMenuNew"],
            "mainHeader": {
                "btnAddNewOption": "UpdateDecision"
            },
            "noStaticData": {
                "btnAddStaticContent": "UpdateDecision"
            },
            "decision0": {
                "flxLblImgEdit": "UpdateDecision",
                "flxDelete": "DeleteDecision"
            },
            "decision1": {
                "flxLblImgEdit": "UpdateDecision",
                "flxDelete": "DeleteDecision"
            },
            "decision2": {
                "flxLblImgEdit": "UpdateDecision",
                "flxDelete": "DeleteDecision"
            },
            "decision3": {
                "flxLblImgEdit": "UpdateDecision",
                "flxDelete": "DeleteDecision"
            },
            "decision4": {
                "flxLblImgEdit": "UpdateDecision",
                "flxDelete": "DeleteDecision"
            },
            "decision5": {
                "flxLblImgEdit": "UpdateDecision",
                "flxDelete": "DeleteDecision"
            },
            "decision6": {
                "flxLblImgEdit": "UpdateDecision",
                "flxDelete": "DeleteDecision"
            },
            "decision7": {
                "flxLblImgEdit": "UpdateDecision",
                "flxDelete": "DeleteDecision"
            },
            "decision8": {
                "flxLblImgEdit": "UpdateDecision",
                "flxDelete": "DeleteDecision"
            },
            "decision9": {
                "flxLblImgEdit": "UpdateDecision",
                "flxDelete": "DeleteDecision"
            }
        },
        frmNewCustomer: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            leftMenuNew: ["component", "leftMenuNew"]
        },
        frmLeadManagement: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            leftMenuNew: ["component", "leftMenuNew"]
        },
        frmDepositsDashboard: {
            "dropdownMainHeader": {
                "flxChangePassword": "ChangePassword"
            },
            leftMenuNew: ["component", "leftMenuNew"]
        },
        frmAdManagement: {
            "mainHeader": {
                "btnDropdownList": "CreateUpdateCampaign",
                "btnAddNewOption": "CreateUpdateCampaign"
            },
            "segCampaigns": ["segment", {
                "flxOptions": "CreateUpdateCampaign"
            }],
            "flxOptions": "CreateUpdateCampaign"
        }
    }
});