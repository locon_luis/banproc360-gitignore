define("TermsAndConditionsModule/frmTermsAndConditions", function() {
    return function(controller) {
        function addWidgetsfrmTermsAndConditions() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "konydbxlogobignverted.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "imgBottomLogo": {
                        "src": "konydbxinverted.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "126px",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "right": "207dp"
                    },
                    "flxButtons": {
                        "isVisible": true,
                        "right": "71dp",
                        "width": "400dp"
                    },
                    "flxHeaderSeperator": {
                        "left": "0dp"
                    },
                    "flxHeaderUserOptions": {
                        "height": "24dp",
                        "right": "70px",
                        "width": "30%"
                    },
                    "flxMainHeader": {
                        "left": "35dp",
                        "top": "0dp"
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.addTnC\")",
                        "left": "0dp",
                        "top": "54px"
                    },
                    "mainHeader": {
                        "height": "100px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadcrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "106px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadcrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "clipBounds": true,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnBackToMain": {
                        "text": "TERMS & CONDITIONS LIST"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumb.add(breadcrumbs);
            flxMainHeader.add(mainHeader, flxBreadcrumb);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "63dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "110dp",
                "width": "100%",
                "zIndex": 7
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "48dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false,
                        "left": "35dp"
                    },
                    "flxSearch": {
                        "top": "0px",
                        "width": "350px"
                    },
                    "flxSearchContainer": {
                        "left": "0dp",
                        "right": "0dp",
                        "top": "5dp"
                    },
                    "flxSubHeader": {
                        "bottom": "0dp",
                        "height": "100%",
                        "top": "0dp"
                    },
                    "subHeader": {
                        "centerY": "50%",
                        "height": "48dp",
                        "top": "0dp"
                    },
                    "tbxSearchBox": {
                        "placeholder": "Search by Code / Title"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxScrollMainContent = new kony.ui.FlexContainer({
                "bottom": 0,
                "clipBounds": false,
                "id": "flxScrollMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "top": "146px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScrollMainContent.setDefaultUnit(kony.flex.DP);
            var flxNoTermsAndConditions = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "350px",
                "horizontalScrollIndicator": true,
                "id": "flxNoTermsAndConditions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "15px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxNoTermsAndConditions.setDefaultUnit(kony.flex.DP);
            var noStaticData = new com.adminConsole.staticContent.noStaticData({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "340px",
                "id": "noStaticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "btnAddStaticContent": {
                        "maxWidth": "218px",
                        "width": "218px"
                    },
                    "noStaticData": {
                        "centerX": "viz.val_cleared",
                        "left": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoTermsAndConditions.add(noStaticData);
            var flxDetailTermsAndConditions = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxDetailTermsAndConditions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "0px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxDetailTermsAndConditions.setDefaultUnit(kony.flex.DP);
            var flxDetailTermsAndConditionsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35dp",
                "clipBounds": true,
                "id": "flxDetailTermsAndConditionsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "CopysknflxffffffOp0e4fae09f31bc4e",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxDetailTermsAndConditionsWrapper.setDefaultUnit(kony.flex.DP);
            var lblTandCHeading = new kony.ui.Label({
                "id": "lblTandCHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular485C7518px",
                "text": "DBX footer terms & conditions",
                "top": "37dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTandCHeadingSeperator = new kony.ui.Label({
                "centerX": "50%",
                "height": "1px",
                "id": "lblTandCHeadingSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "35px",
                "skin": "sknLblSeparator",
                "text": "-",
                "top": "70dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewEditButton = new kony.ui.Button({
                "bottom": "0px",
                "focusSkin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "height": "22px",
                "id": "flxViewEditButton",
                "isVisible": true,
                "right": "35px",
                "skin": "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "25px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            var flxDetailBody = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxDetailBody",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "83dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxDetailBody.setDefaultUnit(kony.flex.DP);
            var flxTitleDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTitleDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%"
            }, {}, {});
            flxTitleDetails.setDefaultUnit(kony.flex.DP);
            var lblTitleHeading = new kony.ui.Label({
                "id": "lblTitleHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "TITLE",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTitleValue = new kony.ui.Label({
                "id": "lblTitleValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "DBX footer terms & conditions",
                "top": "31dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTitleDetails.add(lblTitleHeading, lblTitleValue);
            var flxApplicableAppsDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxApplicableAppsDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "363dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "40%"
            }, {}, {});
            flxApplicableAppsDetails.setDefaultUnit(kony.flex.DP);
            var lblApplicableAppsHeadings = new kony.ui.Label({
                "id": "lblApplicableAppsHeadings",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "APPLICABLE APPS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblApplicableAppsValue = new kony.ui.Label({
                "id": "lblApplicableAppsValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Retail Banking, Business Banking, Onboarding Applications",
                "top": "31dp",
                "width": "360dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxApplicableAppsDetails.add(lblApplicableAppsHeadings, lblApplicableAppsValue);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "77dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblDescriptionHeading = new kony.ui.Label({
                "id": "lblDescriptionHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "DESCRIPTION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDescriptionValue = new kony.ui.Label({
                "id": "lblDescriptionValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "This terms & conditions will be used in the footer link of retail banking",
                "top": "32dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescription.add(lblDescriptionHeading, lblDescriptionValue);
            var flxCodeDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCodeDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "155dp",
                "width": "35%"
            }, {}, {});
            flxCodeDetails.setDefaultUnit(kony.flex.DP);
            var lblCodeHeading = new kony.ui.Label({
                "id": "lblCodeHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "CODE",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCodeValue = new kony.ui.Label({
                "id": "lblCodeValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "RB_Footer_T&C",
                "top": "31dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCodeDetails.add(lblCodeHeading, lblCodeValue);
            var flxLastModifiedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLastModifiedOn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "363dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "155dp",
                "width": "20%"
            }, {}, {});
            flxLastModifiedOn.setDefaultUnit(kony.flex.DP);
            var lblLastModifiedOnHeading = new kony.ui.Label({
                "id": "lblLastModifiedOnHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "LAST MODIFIED ON",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLastModifiedOnValue = new kony.ui.Label({
                "id": "lblLastModifiedOnValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "01/08/2019",
                "top": "31dp",
                "width": "177dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLastModifiedOn.add(lblLastModifiedOnHeading, lblLastModifiedOnValue);
            var flxLastModifiedBy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLastModifiedBy",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "102dp",
                "skin": "slFbox",
                "top": "155dp",
                "width": "20%"
            }, {}, {});
            flxLastModifiedBy.setDefaultUnit(kony.flex.DP);
            var lblLastModifiedByHeading = new kony.ui.Label({
                "id": "lblLastModifiedByHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "LAST MODIFIED BY",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLastModifiedByValue = new kony.ui.Label({
                "id": "lblLastModifiedByValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Peter Smith William",
                "top": "31dp",
                "width": "177dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLastModifiedBy.add(lblLastModifiedByHeading, lblLastModifiedByValue);
            var flxContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "232dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxContent.setDefaultUnit(kony.flex.DP);
            var lblContent = new kony.ui.Label({
                "id": "lblContent",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "CONTENT",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUrlContent = new kony.ui.Label({
                "id": "lblUrlContent",
                "isVisible": false,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "www.google.com",
                "top": "33dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var staticData = new com.adminConsole.staticContent.staticData({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "50dp",
                "clipBounds": false,
                "id": "staticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxFFFFFFbrdre1e5ed",
                "top": "33px",
                "zIndex": 1,
                "overrides": {
                    "flxDeactivateOption": {
                        "isVisible": false
                    },
                    "flxHeader": {
                        "isVisible": false
                    },
                    "flxSelectOptions": {
                        "isVisible": false,
                        "top": "62px"
                    },
                    "flxStaticContantData": {
                        "bottom": "50px",
                        "left": "0px",
                        "top": "0px",
                        "zIndex": 100,
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "flxStatus": {
                        "width": "60px"
                    },
                    "lblStaticContentHeader": {
                        "text": "KONY DIGITAL BANKING TERMS AND CONDITIONS"
                    },
                    "rtxViewer": {
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "right": "0px"
                    },
                    "staticData": {
                        "bottom": "50dp",
                        "height": "viz.val_cleared",
                        "isVisible": true,
                        "left": "20px",
                        "right": "20px",
                        "top": "33px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContent.add(lblContent, lblUrlContent, staticData);
            flxDetailBody.add(flxTitleDetails, flxApplicableAppsDetails, flxDescription, flxCodeDetails, flxLastModifiedOn, flxLastModifiedBy, flxContent);
            var flxEditBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEditBody",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "93dp",
                "width": "100%"
            }, {}, {});
            flxEditBody.setDefaultUnit(kony.flex.DP);
            var flxEditBodyWrapper = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "70%",
                "horizontalScrollIndicator": true,
                "id": "flxEditBodyWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxEditBodyWrapper.setDefaultUnit(kony.flex.DP);
            var flxFirstRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFirstRow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxFirstRow.setDefaultUnit(kony.flex.DP);
            var flxEditTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditTitle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "430dp"
            }, {}, {});
            flxEditTitle.setDefaultUnit(kony.flex.DP);
            var lblEditTitleHeading = new kony.ui.Label({
                "id": "lblEditTitleHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "Title",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHeadingCount = new kony.ui.Label({
                "id": "lblHeadingCount",
                "isVisible": false,
                "right": "5dp",
                "skin": "sknLbl78818A11px",
                "text": "0/50",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxTitle = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "tbxTitle",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.lblEnterHere\")",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "400dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var flxTitleError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxTitleError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75px",
                "width": "300px",
                "zIndex": 1
            }, {}, {});
            flxTitleError.setDefaultUnit(kony.flex.DP);
            var lblTitleError = new kony.ui.Label({
                "height": "15px",
                "id": "lblTitleError",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTitleErrorDesc = new kony.ui.Label({
                "height": "15px",
                "id": "lblTitleErrorDesc",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTermsAndConditions.titleBlankMessage\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTitleError.add(lblTitleError, lblTitleErrorDesc);
            flxEditTitle.add(lblEditTitleHeading, lblHeadingCount, tbxTitle, flxTitleError);
            var flxEditApplicableApps = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditApplicableApps",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "400dp"
            }, {}, {});
            flxEditApplicableApps.setDefaultUnit(kony.flex.DP);
            var lblEditApplicableAppsEdit = new kony.ui.Label({
                "id": "lblEditApplicableAppsEdit",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "Applicable Apps",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customListboxApps = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "customListboxApps",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "400dp",
                "overrides": {
                    "customListbox": {
                        "isVisible": true,
                        "left": "20dp",
                        "top": "30dp",
                        "width": "400dp"
                    },
                    "flxDropdown": {
                        "top": "5dp"
                    },
                    "flxListboxError": {
                        "isVisible": false
                    },
                    "flxSegmentList": {
                        "isVisible": false,
                        "width": "94%",
                        "zIndex": 1
                    },
                    "flxSelectedText": {
                        "width": "94%"
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "segList": {
                        "data": [{
                            "imgCheckBox": "checkboxnormal.png",
                            "lblDescription": "All"
                        }, {
                            "imgCheckBox": "checkboxnormal.png",
                            "lblDescription": "Retail banking native & responsive web"
                        }, {
                            "imgCheckBox": "checkboxnormal.png",
                            "lblDescription": "Micro-business banking"
                        }, {
                            "imgCheckBox": "checkboxnormal.png",
                            "lblDescription": "Small business banking"
                        }],
                        "width": "95%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditApplicableApps.add(lblEditApplicableAppsEdit, customListboxApps);
            flxFirstRow.add(flxEditTitle, flxEditApplicableApps);
            var flxEditDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "135dp",
                "id": "flxEditDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "100dp"
            }, {}, {});
            flxEditDescription.setDefaultUnit(kony.flex.DP);
            var lblEditDescriptionEdit = new kony.ui.Label({
                "id": "lblEditDescriptionEdit",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.description\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDescriptionCount = new kony.ui.Label({
                "id": "lblDescriptionCount",
                "isVisible": false,
                "right": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "0/250",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAreaDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextAreaFocus",
                "height": "100dp",
                "id": "txtAreaDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "maxTextLength": 250,
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.lblEnterHere\")",
                "right": "40dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextAreaPlaceholder"
            });
            flxEditDescription.add(lblEditDescriptionEdit, lblDescriptionCount, txtAreaDescription);
            var flxEditContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "280dp",
                "id": "flxEditContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxEditContent.setDefaultUnit(kony.flex.DP);
            var flxEditContentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditContentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxEditContentWrapper.setDefaultUnit(kony.flex.DP);
            var lblContentHeading = new kony.ui.Label({
                "id": "lblContentHeading",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl78818A11px",
                "text": "Content",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContentType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxContentType",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "210px",
                "zIndex": 1
            }, {}, {});
            flxContentType.setDefaultUnit(kony.flex.DP);
            var flxYes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxYes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxYes.setDefaultUnit(kony.flex.DP);
            var imgYes = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgYes",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxYes.add(imgYes);
            var lblYes = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblYes",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "Text",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxNo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxNo.setDefaultUnit(kony.flex.DP);
            var imgNo = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgNo",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNo.add(imgNo);
            var lblNo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNo",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "URL",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContentType.add(flxYes, lblYes, flxNo, lblNo);
            var flxURLTextBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxURLTextBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxURLTextBox.setDefaultUnit(kony.flex.DP);
            var tbxURLcontent = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "tbxURLcontent",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.lblEnterHere\")",
                "right": "40dp",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxURLTextBox.add(tbxURLcontent);
            var flxContentRichTxt = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "180px",
                "id": "flxContentRichTxt",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "10px",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxContentRichTxt.setDefaultUnit(kony.flex.DP);
            var rtxTnCEdit = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxTnCEdit",
                "isVisible": true,
                "left": "0px",
                "requestURLConfig": {
                    "URL": "richtext.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "right": "0dp",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxContentRichTxt.add(rtxTnCEdit);
            var flxContentError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxContentError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "7px",
                "width": "300px",
                "zIndex": 1
            }, {}, {});
            flxContentError.setDefaultUnit(kony.flex.DP);
            var lblContextErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblContextErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContextErrorMessage = new kony.ui.Label({
                "height": "15px",
                "id": "lblContextErrorMessage",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTermsAndConditions.contentBlankMessage\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContentError.add(lblContextErrorIcon, lblContextErrorMessage);
            flxEditContentWrapper.add(lblContentHeading, flxContentType, flxURLTextBox, flxContentRichTxt, flxContentError);
            flxEditContent.add(flxEditContentWrapper);
            flxEditBodyWrapper.add(flxFirstRow, flxEditDescription, flxEditContent);
            var lblButtonSeperator = new kony.ui.Label({
                "centerX": "50%",
                "height": "1px",
                "id": "lblButtonSeperator",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblSeparator",
                "text": "-",
                "top": "10dp",
                "width": "95%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCommonButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxCommonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCommonButtons.setDefaultUnit(kony.flex.DP);
            var flxCommonButtonsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCommonButtonsInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxCommonButtonsInner.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.buttonUpdate\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCommonButtonsInner.add(commonButtons);
            flxCommonButtons.add(flxCommonButtonsInner);
            flxEditBody.add(flxEditBodyWrapper, lblButtonSeperator, flxCommonButtons);
            flxDetailTermsAndConditionsWrapper.add(lblTandCHeading, lblTandCHeadingSeperator, flxViewEditButton, flxDetailBody, flxEditBody);
            flxDetailTermsAndConditions.add(flxDetailTermsAndConditionsWrapper);
            var flxTermsAndConditionsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "500dp",
                "id": "flxTermsAndConditionsList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxTermsAndConditionsList.setDefaultUnit(kony.flex.DP);
            var flxTermsAndConditionsListWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "40dp",
                "clipBounds": true,
                "id": "flxTermsAndConditionsListWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxTermsAndConditionsListWrapper.setDefaultUnit(kony.flex.DP);
            var flxTandCTable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "id": "flxTandCTable",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxTandCTable.setDefaultUnit(kony.flex.DP);
            var flxTandCList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "id": "flxTandCList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp"
            }, {}, {});
            flxTandCList.setDefaultUnit(kony.flex.DP);
            var flxTandCHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxTandCHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxTandCHeader.setDefaultUnit(kony.flex.DP);
            var flxTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "70px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_i87bd852f9f74801ae5ceff0e9ee81d1,
                "skin": "slFbox",
                "top": 0,
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxTitle.setDefaultUnit(kony.flex.DP);
            var lblTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTitle",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.Title\")",
                "top": 0,
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxTitle.add(lblTitle);
            var flxCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "34%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxCode.setDefaultUnit(kony.flex.DP);
            var lblCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CODE\")",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxCode.add(lblCode);
            var flxApplicableApps = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxApplicableApps",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "65%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_i88e2961592c4b338de61134a3117f51,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxApplicableApps.setDefaultUnit(kony.flex.DP);
            var lblApplicableApps = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblApplicableApps",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.ApplicableApps\")",
                "top": 0,
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var flxFilterStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxFilterStatus",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20px"
            }, {}, {});
            flxFilterStatus.setDefaultUnit(kony.flex.DP);
            var fontIconFilterStatus = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fontIconFilterStatus",
                "isVisible": true,
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxFilterStatus.add(fontIconFilterStatus);
            flxApplicableApps.add(lblApplicableApps, flxFilterStatus);
            var lblHeaderSeperator = new kony.ui.Label({
                "bottom": "1px",
                "centerX": "50%",
                "height": "1px",
                "id": "lblHeaderSeperator",
                "isVisible": true,
                "left": "35dp",
                "right": "35px",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTandCHeader.add(flxTitle, flxCode, flxApplicableApps, lblHeaderSeperator);
            var segTandC = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "fonticonActive": kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonEdit"),
                    "lblApplicableApps": "Retail Banking, Business Banking, Onboarding",
                    "lblCode": "Loans_Mortgage_ApplicantAgreement",
                    "lblSeparator": "Label",
                    "lblTitle": " DBX footer terms & conditions"
                }, {
                    "fonticonActive": kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonEdit"),
                    "lblApplicableApps": "Retail Banking, Business Banking, Onboarding",
                    "lblCode": "Loans_Mortgage_ApplicantAgreement",
                    "lblSeparator": "Label",
                    "lblTitle": " DBX footer terms & conditions"
                }, {
                    "fonticonActive": kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonEdit"),
                    "lblApplicableApps": "Retail Banking, Business Banking, Onboarding",
                    "lblCode": "Loans_Mortgage_ApplicantAgreement",
                    "lblSeparator": "Label",
                    "lblTitle": " DBX footer terms & conditions"
                }, {
                    "fonticonActive": kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonEdit"),
                    "lblApplicableApps": "Retail Banking, Business Banking, Onboarding",
                    "lblCode": "Loans_Mortgage_ApplicantAgreement",
                    "lblSeparator": "Label",
                    "lblTitle": " DBX footer terms & conditions"
                }, {
                    "fonticonActive": kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonEdit"),
                    "lblApplicableApps": "Retail Banking, Business Banking, Onboarding",
                    "lblCode": "Loans_Mortgage_ApplicantAgreement",
                    "lblSeparator": "Label",
                    "lblTitle": " DBX footer terms & conditions"
                }, {
                    "fonticonActive": kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonEdit"),
                    "lblApplicableApps": "Retail Banking, Business Banking, Onboarding",
                    "lblCode": "Loans_Mortgage_ApplicantAgreement",
                    "lblSeparator": "Label",
                    "lblTitle": " DBX footer terms & conditions"
                }, {
                    "fonticonActive": kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonEdit"),
                    "lblApplicableApps": "Retail Banking, Business Banking, Onboarding",
                    "lblCode": "Loans_Mortgage_ApplicantAgreement",
                    "lblSeparator": "Label",
                    "lblTitle": " DBX footer terms & conditions"
                }, {
                    "fonticonActive": kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonEdit"),
                    "lblApplicableApps": "Retail Banking, Business Banking, Onboarding",
                    "lblCode": "Loans_Mortgage_ApplicantAgreement",
                    "lblSeparator": "Label",
                    "lblTitle": " DBX footer terms & conditions"
                }, {
                    "fonticonActive": kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonEdit"),
                    "lblApplicableApps": "Retail Banking, Business Banking, Onboarding",
                    "lblCode": "Loans_Mortgage_ApplicantAgreement",
                    "lblSeparator": "Label",
                    "lblTitle": " DBX footer terms & conditions"
                }, {
                    "fonticonActive": kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonEdit"),
                    "lblApplicableApps": "Retail Banking, Business Banking, Onboarding",
                    "lblCode": "Loans_Mortgage_ApplicantAgreement",
                    "lblSeparator": "Label",
                    "lblTitle": " DBX footer terms & conditions"
                }],
                "groupCells": false,
                "height": "350dp",
                "id": "segTandC",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxTermsAndConditions",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "65dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxTermsAndConditions": "flxTermsAndConditions",
                    "flxTermsAndConditionsWrapper": "flxTermsAndConditionsWrapper",
                    "fonticonActive": "fonticonActive",
                    "lblApplicableApps": "lblApplicableApps",
                    "lblCode": "lblCode",
                    "lblSeparator": "lblSeparator",
                    "lblTitle": "lblTitle"
                },
                "width": "100%",
                "zIndex": 11
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "5px",
                "clipBounds": false,
                "id": "flxPagination",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox0d9c3974835234d",
                "top": 0,
                "width": "222px",
                "zIndex": 1
            }, {}, {});
            flxPagination.setDefaultUnit(kony.flex.DP);
            var flxPaginationWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": false,
                "height": "30px",
                "id": "flxPaginationWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox0d9c3974835234d",
                "top": 20,
                "width": "222px",
                "zIndex": 1
            }, {}, {});
            flxPaginationWrapper.setDefaultUnit(kony.flex.DP);
            var lbxPagination = new kony.ui.ListBox({
                "height": "30dp",
                "id": "lbxPagination",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Page 1 of 20"],
                    ["lb2", "Page 2 of 20"],
                    ["lb3", "Page 3 of 20"],
                    ["lb4", "Page 4 of 20"]
                ],
                "selectedKey": "lb1",
                "selectedKeyValue": ["lb1", "Page 1 of 20"],
                "skin": "sknlbxNobgNoBorderPagination",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var flxPaginationSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPaginationSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "2dp",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxPaginationSeperator.setDefaultUnit(kony.flex.DP);
            flxPaginationSeperator.add();
            var flxPrevious = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "32dp",
                "id": "flxPrevious",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "right": "3dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "32px",
                "zIndex": 1
            }, {}, {});
            flxPrevious.setDefaultUnit(kony.flex.DP);
            var lblIconPrevious = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconPrevious",
                "isVisible": true,
                "skin": "sknFontIconPrevNextDisable",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconPrevious\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPrevious.add(lblIconPrevious);
            var flxNext = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "32dp",
                "id": "flxNext",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "32px",
                "zIndex": 1
            }, {}, {});
            flxNext.setDefaultUnit(kony.flex.DP);
            var lblIconNext = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconNext",
                "isVisible": true,
                "skin": "sknFontIconPrevNextDisable",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconNext\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNext.add(lblIconNext);
            flxPaginationWrapper.add(lbxPagination, flxPaginationSeperator, flxPrevious, flxNext);
            flxPagination.add(flxPaginationWrapper);
            var flxApplicableAppsFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxApplicableAppsFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "647dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "200dp",
                "zIndex": 11
            }, {}, {});
            flxApplicableAppsFilter.setDefaultUnit(kony.flex.DP);
            var applicableAppsFilterMenu = new com.adminConsole.Products.productTypeFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "applicableAppsFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxChechboxOuter": {
                        "height": "100px"
                    },
                    "flxFilterSort": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblSeperator": {
                        "isVisible": false
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Retail Banking"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Business Banking"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Onboarding"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxApplicableAppsFilter.add(applicableAppsFilterMenu);
            var flxNoRecordsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxNoRecordsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoRecords = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoRecords",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecordsFound.add(rtxNoRecords);
            flxTandCList.add(flxTandCHeader, segTandC, flxPagination, flxApplicableAppsFilter, flxNoRecordsFound);
            flxTandCTable.add(flxTandCList);
            flxTermsAndConditionsListWrapper.add(flxTandCTable);
            flxTermsAndConditionsList.add(flxTermsAndConditionsListWrapper);
            flxScrollMainContent.add(flxNoTermsAndConditions, flxDetailTermsAndConditions, flxTermsAndConditionsList);
            flxRightPanel.add(flxMainHeader, flxMainSubHeader, flxHeaderDropdown, flxScrollMainContent);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxMain.add(flxLeftPannel, flxRightPanel, flxToastMessage, flxLoading);
            var flxTandCPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTandCPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxTandCPopUp.setDefaultUnit(kony.flex.DP);
            var tandCPopUp = new com.adminConsole.common.popUp1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "tandCPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "150px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesProceed\")"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Unsaved Changes"
                    },
                    "popUp1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure you want to navigate away from the current page?<br><br>\n\n\n\n\n\nThere are unsaved changes in the current page. By clicking on \"Yes, Proceed\", all the unsaved <br>\nchanges would be lost."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTandCPopUp.add(tandCPopUp);
            var flxTextToUrlPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTextToUrlPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxTextToUrlPopup.setDefaultUnit(kony.flex.DP);
            var textToUrlPopup = new com.adminConsole.common.popUp1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "textToUrlPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "150px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesProceed\")"
                    },
                    "flxPopupHeader": {
                        "width": "100%"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.UnsavedChanges\")"
                    },
                    "popUp1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.TextToUrlMessage\")",
                        "left": "20px",
                        "right": "20dp",
                        "width": "92%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTextToUrlPopup.add(textToUrlPopup);
            this.add(flxMain, flxTandCPopUp, flxTextToUrlPopup);
        };
        return [{
            "addWidgets": addWidgetsfrmTermsAndConditions,
            "enabledForIdleTimeout": true,
            "id": "frmTermsAndConditions",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_i84b41a94cd94117916c0bb1a757bdca(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_ga73a3b96d7646c9ad76a67a401444b0,
            "retainScrollPosition": false
        }]
    }
});