define([], function() {
    var repoMapping = {
        cardSummary: {
            model: "CardManagementObjService/cardSummary/Model",
            config: "CardManagementObjService/cardSummary/MF_Config",
            repository: "CardManagementObjService/cardSummary/Repository",
        },
        alert: {
            model: "AlertAndAlertTypes/alert/Model",
            config: "AlertAndAlertTypes/alert/MF_Config",
            repository: "AlertAndAlertTypes/alert/Repository",
        },
        PermissionObject: {
            model: "RolesAndPermissionsObjService/PermissionObject/Model",
            config: "RolesAndPermissionsObjService/PermissionObject/MF_Config",
            repository: "RolesAndPermissionsObjService/PermissionObject/Repository",
        },
        Browser: {
            model: "BrowserManagementObjService/Browser/Model",
            config: "BrowserManagementObjService/Browser/MF_Config",
            repository: "BrowserManagementObjService/Browser/Repository",
        },
        alertsubtype: {
            model: "AlertAndAlertTypes/alertsubtype/Model",
            config: "AlertAndAlertTypes/alertsubtype/MF_Config",
            repository: "AlertAndAlertTypes/alertsubtype/Repository",
        },
        bbcustomerservicelimit: {
            model: "BusinessBankingObjService/bbcustomerservicelimit/Model",
            config: "BusinessBankingObjService/bbcustomerservicelimit/MF_Config",
            repository: "BusinessBankingObjService/bbcustomerservicelimit/Repository",
        },
        LoansCustomer: {
            model: "LoansLocalServices/LoansCustomer/Model",
            config: "LoansLocalServices/LoansCustomer/MF_Config",
            repository: "LoansLocalServices/LoansCustomer/Repository",
        },
        CustomerCommunication: {
            model: "LoansLocalServices/CustomerCommunication/Model",
            config: "LoansLocalServices/CustomerCommunication/MF_Config",
            repository: "LoansLocalServices/CustomerCommunication/Repository",
        },
        QuerySection: {
            model: "LoansLocalServices/QuerySection/Model",
            config: "LoansLocalServices/QuerySection/MF_Config",
            repository: "",
        },
        Users: {
            model: "InternalusersObjService/Users/Model",
            config: "InternalusersObjService/Users/MF_Config",
            repository: "InternalusersObjService/Users/Repository",
        },
        transactionFeesEndUser: {
            model: "ManageLimitsAndFees/transactionFeesEndUser/Model",
            config: "ManageLimitsAndFees/transactionFeesEndUser/MF_Config",
            repository: "ManageLimitsAndFees/transactionFeesEndUser/Repository",
        },
        alertcategory: {
            model: "AlertAndAlertTypes/alertcategory/Model",
            config: "AlertAndAlertTypes/alertcategory/MF_Config",
            repository: "AlertAndAlertTypes/alertcategory/Repository",
        },
        QuestionDefinition: {
            model: "LoansLocalServices/QuestionDefinition/Model",
            config: "LoansLocalServices/QuestionDefinition/MF_Config",
            repository: "",
        },
        facility: {
            model: "LocationObjService/facility/Model",
            config: "LocationObjService/facility/MF_Config",
            repository: "",
        },
        company: {
            model: "BusinessBankingObjService/company/Model",
            config: "BusinessBankingObjService/company/MF_Config",
            repository: "BusinessBankingObjService/company/Repository",
        },
        campaign: {
            model: "CampaignManagementObjService/campaign/Model",
            config: "CampaignManagementObjService/campaign/MF_Config",
            repository: "CampaignManagementObjService/campaign/Repository",
        },
        userRole: {
            model: "RolesAndPermissionsObjService/userRole/Model",
            config: "RolesAndPermissionsObjService/userRole/MF_Config",
            repository: "",
        },
        CustomerProduct: {
            model: "CustomerManagementObjService/CustomerProduct/Model",
            config: "CustomerManagementObjService/CustomerProduct/MF_Config",
            repository: "CustomerManagementObjService/CustomerProduct/Repository",
        },
        manageSecurityImages: {
            model: "SecurityOpsObjService/manageSecurityImages/Model",
            config: "SecurityOpsObjService/manageSecurityImages/MF_Config",
            repository: "SecurityOpsObjService/manageSecurityImages/Repository",
        },
        eventsubtype: {
            model: "AlertAndAlertTypes/eventsubtype/Model",
            config: "AlertAndAlertTypes/eventsubtype/MF_Config",
            repository: "AlertAndAlertTypes/eventsubtype/Repository",
        },
        VehicleModels: {
            model: "LoansLocalServices/VehicleModels/Model",
            config: "LoansLocalServices/VehicleModels/MF_Config",
            repository: "",
        },
        QueryCoBorrower: {
            model: "LoansLocalServices/QueryCoBorrower/Model",
            config: "LoansLocalServices/QueryCoBorrower/MF_Config",
            repository: "LoansLocalServices/QueryCoBorrower/Repository",
        },
        product: {
            model: "BankProductManagment/product/Model",
            config: "BankProductManagment/product/MF_Config",
            repository: "BankProductManagment/product/Repository",
        },
        currency: {
            model: "LocationObjService/currency/Model",
            config: "LocationObjService/currency/MF_Config",
            repository: "",
        },
        managePermissions: {
            model: "RolesAndPermissionsObjService/managePermissions/Model",
            config: "RolesAndPermissionsObjService/managePermissions/MF_Config",
            repository: "RolesAndPermissionsObjService/managePermissions/Repository",
        },
        EligibilityCriteria: {
            model: "BusinessConfigObjService/EligibilityCriteria/Model",
            config: "BusinessConfigObjService/EligibilityCriteria/MF_Config",
            repository: "BusinessConfigObjService/EligibilityCriteria/Repository",
        },
        customertype: {
            model: "MasterDataObjService/customertype/Model",
            config: "MasterDataObjService/customertype/MF_Config",
            repository: "MasterDataObjService/customertype/Repository",
        },
        CustomerRequestSummary: {
            model: "DashboardObjService/CustomerRequestSummary/Model",
            config: "DashboardObjService/CustomerRequestSummary/MF_Config",
            repository: "DashboardObjService/CustomerRequestSummary/Repository",
        },
        CardRequests: {
            model: "AccountRequestsObjService/CardRequests/Model",
            config: "AccountRequestsObjService/CardRequests/MF_Config",
            repository: "AccountRequestsObjService/CardRequests/Repository",
        },
        leadnote: {
            model: "LeadAndApplicant/leadnote/Model",
            config: "LeadAndApplicant/leadnote/MF_Config",
            repository: "LeadAndApplicant/leadnote/Repository",
        },
        ProductTransaction: {
            model: "CustomerManagementObjService/ProductTransaction/Model",
            config: "CustomerManagementObjService/ProductTransaction/MF_Config",
            repository: "CustomerManagementObjService/ProductTransaction/Repository",
        },
        LoansConfigurationMasters: {
            model: "LoansLocalServices/LoansConfigurationMasters/Model",
            config: "LoansLocalServices/LoansConfigurationMasters/MF_Config",
            repository: "LoansLocalServices/LoansConfigurationMasters/Repository",
        },
        app: {
            model: "MasterDataObjService/app/Model",
            config: "MasterDataObjService/app/MF_Config",
            repository: "MasterDataObjService/app/Repository",
        },
        lead: {
            model: "LeadAndApplicant/lead/Model",
            config: "LeadAndApplicant/lead/MF_Config",
            repository: "LeadAndApplicant/lead/Repository",
        },
        leadconfiguration: {
            model: "LeadAndApplicant/leadconfiguration/Model",
            config: "LeadAndApplicant/leadconfiguration/MF_Config",
            repository: "LeadAndApplicant/leadconfiguration/Repository",
        },
        countrycode: {
            model: "MasterDataObjService/countrycode/Model",
            config: "MasterDataObjService/countrycode/MF_Config",
            repository: "MasterDataObjService/countrycode/Repository",
        },
        CustomerDpData: {
            model: "LoansLocalServices/CustomerDpData/Model",
            config: "LoansLocalServices/CustomerDpData/MF_Config",
            repository: "",
        },
        permissions_view: {
            model: "RolesAndPermissionsObjService/permissions_view/Model",
            config: "RolesAndPermissionsObjService/permissions_view/MF_Config",
            repository: "RolesAndPermissionsObjService/permissions_view/Repository",
        },
        TransactionReport: {
            model: "ReportsObjService/TransactionReport/Model",
            config: "ReportsObjService/TransactionReport/MF_Config",
            repository: "ReportsObjService/TransactionReport/Repository",
        },
        Customer: {
            model: "CustomerManagementObjService/Customer/Model",
            config: "CustomerManagementObjService/Customer/MF_Config",
            repository: "CustomerManagementObjService/Customer/Repository",
        },
        UserQuerySectionStatus: {
            model: "LoansLocalServices/UserQuerySectionStatus/Model",
            config: "LoansLocalServices/UserQuerySectionStatus/MF_Config",
            repository: "",
        },
        period: {
            model: "ManageLimitsAndFees/period/Model",
            config: "ManageLimitsAndFees/period/MF_Config",
            repository: "",
        },
        termsandconditions: {
            model: "TermsAndConditionsObjService/termsandconditions/Model",
            config: "TermsAndConditionsObjService/termsandconditions/MF_Config",
            repository: "TermsAndConditionsObjService/termsandconditions/Repository",
        },
        MessagesReport: {
            model: "ReportsObjService/MessagesReport/Model",
            config: "ReportsObjService/MessagesReport/MF_Config",
            repository: "ReportsObjService/MessagesReport/Repository",
        },
        OnboardingTermsAndConditions: {
            model: "BusinessConfigObjService/OnboardingTermsAndConditions/Model",
            config: "BusinessConfigObjService/OnboardingTermsAndConditions/MF_Config",
            repository: "BusinessConfigObjService/OnboardingTermsAndConditions/Repository",
        },
        periodLimitUserGroup: {
            model: "ManageLimitsAndFees/periodLimitUserGroup/Model",
            config: "ManageLimitsAndFees/periodLimitUserGroup/MF_Config",
            repository: "ManageLimitsAndFees/periodLimitUserGroup/Repository",
        },
        LocationObject: {
            model: "LocationObjService/LocationObject/Model",
            config: "LocationObjService/LocationObject/MF_Config",
            repository: "LocationObjService/LocationObject/Repository",
        },
        CustomerEntitlement: {
            model: "CustomerManagementObjService/CustomerEntitlement/Model",
            config: "CustomerManagementObjService/CustomerEntitlement/MF_Config",
            repository: "CustomerManagementObjService/CustomerEntitlement/Repository",
        },
        manageSecurityQuestions: {
            model: "SecurityOpsObjService/manageSecurityQuestions/Model",
            config: "SecurityOpsObjService/manageSecurityQuestions/MF_Config",
            repository: "SecurityOpsObjService/manageSecurityQuestions/Repository",
        },
        maplocation: {
            model: "ServerManagementObjService/maplocation/Model",
            config: "ServerManagementObjService/maplocation/MF_Config",
            repository: "ServerManagementObjService/maplocation/Repository",
        },
        OptionItem: {
            model: "LoansLocalServices/OptionItem/Model",
            config: "LoansLocalServices/OptionItem/MF_Config",
            repository: "",
        },
        alertType: {
            model: "AlertAndAlertTypes/alertType/Model",
            config: "AlertAndAlertTypes/alertType/MF_Config",
            repository: "AlertAndAlertTypes/alertType/Repository",
        },
        ValidateVIN: {
            model: "LoansLocalServices/ValidateVIN/Model",
            config: "LoansLocalServices/ValidateVIN/MF_Config",
            repository: "LoansLocalServices/ValidateVIN/Repository",
        },
        alertattribute: {
            model: "AlertAndAlertTypes/alertattribute/Model",
            config: "AlertAndAlertTypes/alertattribute/MF_Config",
            repository: "AlertAndAlertTypes/alertattribute/Repository",
        },
        IdType: {
            model: "CustomerManagementObjService/IdType/Model",
            config: "CustomerManagementObjService/IdType/MF_Config",
            repository: "CustomerManagementObjService/IdType/Repository",
        },
        CustomerAndCustomerGroup: {
            model: "CustomerManagementObjService/CustomerAndCustomerGroup/Model",
            config: "CustomerManagementObjService/CustomerAndCustomerGroup/MF_Config",
            repository: "CustomerManagementObjService/CustomerAndCustomerGroup/Repository",
        },
        LocationServicesObject: {
            model: "LocationObjService/LocationServicesObject/Model",
            config: "LocationObjService/LocationServicesObject/MF_Config",
            repository: "LocationObjService/LocationServicesObject/Repository",
        },
        GroupCustomers: {
            model: "CustomerGroupsAndEntitlObjSvc/GroupCustomers/Model",
            config: "CustomerGroupsAndEntitlObjSvc/GroupCustomers/MF_Config",
            repository: "CustomerGroupsAndEntitlObjSvc/GroupCustomers/Repository",
        },
        alertcondition: {
            model: "AlertAndAlertTypes/alertcondition/Model",
            config: "AlertAndAlertTypes/alertcondition/MF_Config",
            repository: "AlertAndAlertTypes/alertcondition/Repository",
        },
        OptionItemResponse: {
            model: "LoansLocalServices/OptionItemResponse/Model",
            config: "LoansLocalServices/OptionItemResponse/MF_Config",
            repository: "",
        },
        leadproduct: {
            model: "LeadAndApplicant/leadproduct/Model",
            config: "LeadAndApplicant/leadproduct/MF_Config",
            repository: "LeadAndApplicant/leadproduct/Repository",
        },
        DMSUser: {
            model: "DMSUserCreationObjService/DMSUser/Model",
            config: "DMSUserCreationObjService/DMSUser/MF_Config",
            repository: "DMSUserCreationObjService/DMSUser/Repository",
        },
        overallPaymentLimits: {
            model: "ManageLimitsAndFees/overallPaymentLimits/Model",
            config: "ManageLimitsAndFees/overallPaymentLimits/MF_Config",
            repository: "ManageLimitsAndFees/overallPaymentLimits/Repository",
        },
        periodLimitService: {
            model: "ManageLimitsAndFees/periodLimitService/Model",
            config: "ManageLimitsAndFees/periodLimitService/MF_Config",
            repository: "ManageLimitsAndFees/periodLimitService/Repository",
        },
        internalUsers_view: {
            model: "InternalusersObjService/internalUsers_view/Model",
            config: "InternalusersObjService/internalUsers_view/MF_Config",
            repository: "InternalusersObjService/internalUsers_view/Repository",
        },
        TransactionAndAuditLogs: {
            model: "AuditLogsObjSvc/TransactionAndAuditLogs/Model",
            config: "AuditLogsObjSvc/TransactionAndAuditLogs/MF_Config",
            repository: "AuditLogsObjSvc/TransactionAndAuditLogs/Repository",
        },
        alerthistory: {
            model: "CustomerManagementObjService/alerthistory/Model",
            config: "CustomerManagementObjService/alerthistory/MF_Config",
            repository: "CustomerManagementObjService/alerthistory/Repository",
        },
        identitymanagement: {
            model: "IdentityManagementObjService/identitymanagement/Model",
            config: "IdentityManagementObjService/identitymanagement/MF_Config",
            repository: "IdentityManagementObjService/identitymanagement/Repository",
        },
        termsAndConditions: {
            model: "StaticContentObjService/termsAndConditions/Model",
            config: "StaticContentObjService/termsAndConditions/MF_Config",
            repository: "StaticContentObjService/termsAndConditions/Repository",
        },
        CustomerActivity: {
            model: "CustomerManagementObjService/CustomerActivity/Model",
            config: "CustomerManagementObjService/CustomerActivity/MF_Config",
            repository: "CustomerManagementObjService/CustomerActivity/Repository",
        },
        privacyPolicy: {
            model: "StaticContentObjService/privacyPolicy/Model",
            config: "StaticContentObjService/privacyPolicy/MF_Config",
            repository: "StaticContentObjService/privacyPolicy/Repository",
        },
        DecisionResult: {
            model: "LoansLocalServices/DecisionResult/Model",
            config: "LoansLocalServices/DecisionResult/MF_Config",
            repository: "",
        },
        LoansConfigurations: {
            model: "LoansLocalServices/LoansConfigurations/Model",
            config: "LoansLocalServices/LoansConfigurations/MF_Config",
            repository: "",
        },
        ServiceAndServiceComm: {
            model: "CustServiceObjService/ServiceAndServiceComm/Model",
            config: "CustServiceObjService/ServiceAndServiceComm/MF_Config",
            repository: "CustServiceObjService/ServiceAndServiceComm/Repository",
        },
        GroupEntitlements: {
            model: "CustomerGroupsAndEntitlObjSvc/GroupEntitlements/Model",
            config: "CustomerGroupsAndEntitlObjSvc/GroupEntitlements/MF_Config",
            repository: "CustomerGroupsAndEntitlObjSvc/GroupEntitlements/Repository",
        },
        Disclaimer: {
            model: "LoansLocalServices/Disclaimer/Model",
            config: "LoansLocalServices/Disclaimer/MF_Config",
            repository: "",
        },
        Configuration: {
            model: "ConfigurationObjService/Configuration/Model",
            config: "ConfigurationObjService/Configuration/MF_Config",
            repository: "ConfigurationObjService/Configuration/Repository",
        },
        VehicleMakes: {
            model: "LoansLocalServices/VehicleMakes/Model",
            config: "LoansLocalServices/VehicleMakes/MF_Config",
            repository: "",
        },
        Applicant: {
            model: "CustomerManagementObjService/Applicant/Model",
            config: "CustomerManagementObjService/Applicant/MF_Config",
            repository: "CustomerManagementObjService/Applicant/Repository",
        },
        LocationsUsingCSV: {
            model: "LocationObjService/LocationsUsingCSV/Model",
            config: "LocationObjService/LocationsUsingCSV/MF_Config",
            repository: "LocationObjService/LocationsUsingCSV/Repository",
        },
        transferFeeGroup: {
            model: "ManageLimitsAndFees/transferFeeGroup/Model",
            config: "ManageLimitsAndFees/transferFeeGroup/MF_Config",
            repository: "ManageLimitsAndFees/transferFeeGroup/Repository",
        },
        periodLimitEndUser: {
            model: "ManageLimitsAndFees/periodLimitEndUser/Model",
            config: "ManageLimitsAndFees/periodLimitEndUser/MF_Config",
            repository: "ManageLimitsAndFees/periodLimitEndUser/Repository",
        },
        customer: {
            model: "BusinessBankingObjService/customer/Model",
            config: "BusinessBankingObjService/customer/MF_Config",
            repository: "BusinessBankingObjService/customer/Repository",
        },
        LoanProduct: {
            model: "LoansLocalServices/LoanProduct/Model",
            config: "LoansLocalServices/LoanProduct/MF_Config",
            repository: "",
        },
        GroupRecords: {
            model: "CustomerGroupsAndEntitlObjSvc/GroupRecords/Model",
            config: "CustomerGroupsAndEntitlObjSvc/GroupRecords/MF_Config",
            repository: "CustomerGroupsAndEntitlObjSvc/GroupRecords/Repository",
        },
        outageMessage: {
            model: "StaticContentObjService/outageMessage/Model",
            config: "StaticContentObjService/outageMessage/MF_Config",
            repository: "StaticContentObjService/outageMessage/Repository",
        },
        roleuser_view: {
            model: "RolesAndPermissionsObjService/roleuser_view/Model",
            config: "RolesAndPermissionsObjService/roleuser_view/MF_Config",
            repository: "",
        },
        mfaconfigandscenarios: {
            model: "MFAObjService/mfaconfigandscenarios/Model",
            config: "MFAObjService/mfaconfigandscenarios/MF_Config",
            repository: "MFAObjService/mfaconfigandscenarios/Repository",
        },
        region: {
            model: "InternalusersObjService/region/Model",
            config: "InternalusersObjService/region/MF_Config",
            repository: "InternalusersObjService/region/Repository",
        },
        locale: {
            model: "MasterDataObjService/locale/Model",
            config: "MasterDataObjService/locale/MF_Config",
            repository: "MasterDataObjService/locale/Repository",
        },
        alertchannels: {
            model: "AlertAndAlertTypes/alertchannels/Model",
            config: "AlertAndAlertTypes/alertchannels/MF_Config",
            repository: "AlertAndAlertTypes/alertchannels/Repository",
        },
        DataType: {
            model: "LoansLocalServices/DataType/Model",
            config: "LoansLocalServices/DataType/MF_Config",
            repository: "",
        },
        messageTemplate: {
            model: "CustServiceObjService/messageTemplate/Model",
            config: "CustServiceObjService/messageTemplate/MF_Config",
            repository: "CustServiceObjService/messageTemplate/Repository",
        },
        Note: {
            model: "CustomerManagementObjService/Note/Model",
            config: "CustomerManagementObjService/Note/MF_Config",
            repository: "CustomerManagementObjService/Note/Repository",
        },
        QuestionResponse: {
            model: "LoansLocalServices/QuestionResponse/Model",
            config: "LoansLocalServices/QuestionResponse/MF_Config",
            repository: "LoansLocalServices/QuestionResponse/Repository",
        },
        Branch: {
            model: "InternalusersObjService/Branch/Model",
            config: "InternalusersObjService/Branch/MF_Config",
            repository: "InternalusersObjService/Branch/Repository",
        },
        address: {
            model: "ServerManagementObjService/address/Model",
            config: "ServerManagementObjService/address/MF_Config",
            repository: "ServerManagementObjService/address/Repository",
        },
        createUser: {
            model: "InternalusersObjService/createUser/Model",
            config: "InternalusersObjService/createUser/MF_Config",
            repository: "",
        },
        status: {
            model: "RolesAndPermissionsObjService/status/Model",
            config: "RolesAndPermissionsObjService/status/MF_Config",
            repository: "",
        },
        server: {
            model: "ServerManagementObjService/server/Model",
            config: "ServerManagementObjService/server/MF_Config",
            repository: "ServerManagementObjService/server/Repository",
        },
        systemconfiguration: {
            model: "MasterDataObjService/systemconfiguration/Model",
            config: "MasterDataObjService/systemconfiguration/MF_Config",
            repository: "MasterDataObjService/systemconfiguration/Repository",
        },
        GetMakesModel: {
            model: "LoansLocalServices/GetMakesModel/Model",
            config: "LoansLocalServices/GetMakesModel/MF_Config",
            repository: "LoansLocalServices/GetMakesModel/Repository",
        },
        defaultcampaign: {
            model: "CampaignManagementObjService/defaultcampaign/Model",
            config: "CampaignManagementObjService/defaultcampaign/MF_Config",
            repository: "CampaignManagementObjService/defaultcampaign/Repository",
        },
        DecisionFailure: {
            model: "LoansLocalServices/DecisionFailure/Model",
            config: "LoansLocalServices/DecisionFailure/MF_Config",
            repository: "",
        },
        decision: {
            model: "DecisionManagement/decision/Model",
            config: "DecisionManagement/decision/MF_Config",
            repository: "DecisionManagement/decision/Repository",
        },
        CustomerNotification: {
            model: "CustomerManagementObjService/CustomerNotification/Model",
            config: "CustomerManagementObjService/CustomerNotification/MF_Config",
            repository: "CustomerManagementObjService/CustomerNotification/Repository",
        },
        CustomerContact: {
            model: "CustomerManagementObjService/CustomerContact/Model",
            config: "CustomerManagementObjService/CustomerContact/MF_Config",
            repository: "CustomerManagementObjService/CustomerContact/Repository",
        },
        email: {
            model: "EmailObjService/email/Model",
            config: "EmailObjService/email/MF_Config",
            repository: "",
        },
        DirectAndIndirectPermissionUsers: {
            model: "RolesAndPermissionsObjService/DirectAndIndirectPermissionUsers/Model",
            config: "RolesAndPermissionsObjService/DirectAndIndirectPermissionUsers/MF_Config",
            repository: "RolesAndPermissionsObjService/DirectAndIndirectPermissionUsers/Repository",
        },
        Group: {
            model: "CustomerGroupsAndEntitlObjSvc/Group/Model",
            config: "CustomerGroupsAndEntitlObjSvc/Group/MF_Config",
            repository: "CustomerGroupsAndEntitlObjSvc/Group/Repository",
        },
        city: {
            model: "InternalusersObjService/city/Model",
            config: "InternalusersObjService/city/MF_Config",
            repository: "",
        },
        OptionGroup: {
            model: "LoansLocalServices/OptionGroup/Model",
            config: "LoansLocalServices/OptionGroup/MF_Config",
            repository: "",
        },
        QueryResponse: {
            model: "LoansLocalServices/QueryResponse/Model",
            config: "LoansLocalServices/QueryResponse/MF_Config",
            repository: "LoansLocalServices/QueryResponse/Repository",
        },
        CustomerPrequalifyPackage: {
            model: "LoansLocalServices/CustomerPrequalifyPackage/Model",
            config: "LoansLocalServices/CustomerPrequalifyPackage/MF_Config",
            repository: "LoansLocalServices/CustomerPrequalifyPackage/Repository",
        },
        card: {
            model: "CardManagementObjService/card/Model",
            config: "CardManagementObjService/card/MF_Config",
            repository: "CardManagementObjService/card/Repository",
        },
        CustomerSecurityQuestions: {
            model: "CustomerManagementObjService/CustomerSecurityQuestions/Model",
            config: "CustomerManagementObjService/CustomerSecurityQuestions/MF_Config",
            repository: "CustomerManagementObjService/CustomerSecurityQuestions/Repository",
        },
        TravelNotification: {
            model: "AccountRequestsObjService/TravelNotification/Model",
            config: "AccountRequestsObjService/TravelNotification/MF_Config",
            repository: "AccountRequestsObjService/TravelNotification/Repository",
        },
        role: {
            model: "RolesAndPermissionsObjService/role/Model",
            config: "RolesAndPermissionsObjService/role/MF_Config",
            repository: "RolesAndPermissionsObjService/role/Repository",
        },
        userdirectpermission_view: {
            model: "RolesAndPermissionsObjService/userdirectpermission_view/Model",
            config: "RolesAndPermissionsObjService/userdirectpermission_view/MF_Config",
            repository: "",
        },
        validateTransactionLimit: {
            model: "ManageLimitsAndFees/validateTransactionLimit/Model",
            config: "ManageLimitsAndFees/validateTransactionLimit/MF_Config",
            repository: "ManageLimitsAndFees/validateTransactionLimit/Repository",
        },
        QuerySectionQuestion: {
            model: "LoansLocalServices/QuerySectionQuestion/Model",
            config: "LoansLocalServices/QuerySectionQuestion/MF_Config",
            repository: "",
        },
        country: {
            model: "InternalusersObjService/country/Model",
            config: "InternalusersObjService/country/MF_Config",
            repository: "",
        },
        frequentlyAskedQuestions: {
            model: "StaticContentObjService/frequentlyAskedQuestions/Model",
            config: "StaticContentObjService/frequentlyAskedQuestions/MF_Config",
            repository: "StaticContentObjService/frequentlyAskedQuestions/Repository",
        },
        dashboardalerts: {
            model: "DashboardObjService/dashboardalerts/Model",
            config: "DashboardObjService/dashboardalerts/MF_Config",
            repository: "DashboardObjService/dashboardalerts/Repository",
        },
        leadsummary: {
            model: "LeadAndApplicant/leadsummary/Model",
            config: "LeadAndApplicant/leadsummary/MF_Config",
            repository: "LeadAndApplicant/leadsummary/Repository",
        },
        role_view: {
            model: "RolesAndPermissionsObjService/role_view/Model",
            config: "RolesAndPermissionsObjService/role_view/MF_Config",
            repository: "RolesAndPermissionsObjService/role_view/Repository",
        },
        channelandscreen: {
            model: "CampaignManagementObjService/channelandscreen/Model",
            config: "CampaignManagementObjService/channelandscreen/MF_Config",
            repository: "CampaignManagementObjService/channelandscreen/Repository",
        },
        CustomerRequest: {
            model: "CustomerManagementObjService/CustomerRequest/Model",
            config: "CustomerManagementObjService/CustomerRequest/MF_Config",
            repository: "CustomerManagementObjService/CustomerRequest/Repository",
        },
        eventtype: {
            model: "AlertAndAlertTypes/eventtype/Model",
            config: "AlertAndAlertTypes/eventtype/MF_Config",
            repository: "AlertAndAlertTypes/eventtype/Repository",
        },
        ReportsInfo: {
            model: "ReportsObjService/ReportsInfo/Model",
            config: "ReportsObjService/ReportsInfo/MF_Config",
            repository: "ReportsObjService/ReportsInfo/Repository",
        },
        systemuser_view: {
            model: "RolesAndPermissionsObjService/systemuser_view/Model",
            config: "RolesAndPermissionsObjService/systemuser_view/MF_Config",
            repository: "",
        },
        accounttype: {
            model: "MasterDataObjService/accounttype/Model",
            config: "MasterDataObjService/accounttype/MF_Config",
            repository: "MasterDataObjService/accounttype/Repository",
        },
        LogView: {
            model: "AuditLogsObjSvc/LogView/Model",
            config: "AuditLogsObjSvc/LogView/MF_Config",
            repository: "AuditLogsObjSvc/LogView/Repository",
        },
        QueryDefinition: {
            model: "LoansLocalServices/QueryDefinition/Model",
            config: "LoansLocalServices/QueryDefinition/MF_Config",
            repository: "LoansLocalServices/QueryDefinition/Repository",
        },
        transferFeeService: {
            model: "ManageLimitsAndFees/transferFeeService/Model",
            config: "ManageLimitsAndFees/transferFeeService/MF_Config",
            repository: "ManageLimitsAndFees/transferFeeService/Repository",
        },
        CustomerGroup: {
            model: "CustomerManagementObjService/CustomerGroup/Model",
            config: "CustomerManagementObjService/CustomerGroup/MF_Config",
            repository: "CustomerManagementObjService/CustomerGroup/Repository",
        },
        service: {
            model: "StaticContentObjService/service/Model",
            config: "StaticContentObjService/service/MF_Config",
            repository: "StaticContentObjService/service/Repository",
        },
        VehicleTypes: {
            model: "LoansLocalServices/VehicleTypes/Model",
            config: "LoansLocalServices/VehicleTypes/MF_Config",
            repository: "",
        },
        internalUserProfile_view: {
            model: "InternalusersObjService/internalUserProfile_view/Model",
            config: "InternalusersObjService/internalUserProfile_view/MF_Config",
            repository: "InternalusersObjService/internalUserProfile_view/Repository",
        },
        LoanType: {
            model: "LoansLocalServices/LoanType/Model",
            config: "LoansLocalServices/LoanType/MF_Config",
            repository: "LoansLocalServices/LoanType/Repository",
        },
        AddressValidation: {
            model: "BusinessConfigObjService/AddressValidation/Model",
            config: "BusinessConfigObjService/AddressValidation/MF_Config",
            repository: "BusinessConfigObjService/AddressValidation/Repository",
        },
        CustomerDevice: {
            model: "CustomerManagementObjService/CustomerDevice/Model",
            config: "CustomerManagementObjService/CustomerDevice/MF_Config",
            repository: "CustomerManagementObjService/CustomerDevice/Repository",
        },
        alertPreference: {
            model: "AlertAndAlertTypes/alertPreference/Model",
            config: "AlertAndAlertTypes/alertPreference/MF_Config",
            repository: "AlertAndAlertTypes/alertPreference/Repository",
        },
        alertcontentfield: {
            model: "AlertAndAlertTypes/alertcontentfield/Model",
            config: "AlertAndAlertTypes/alertcontentfield/MF_Config",
            repository: "AlertAndAlertTypes/alertcontentfield/Repository",
        },
        rolepermission_view: {
            model: "RolesAndPermissionsObjService/rolepermission_view/Model",
            config: "RolesAndPermissionsObjService/rolepermission_view/MF_Config",
            repository: "",
        },
    };
    return repoMapping;
})