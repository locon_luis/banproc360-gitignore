define("PasswordAgeAndLockoutModule/frmPasswordAgeAndLockoutSettings", function() {
    return function(controller) {
        function addWidgetsfrmPasswordAgeAndLockoutSettings() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPanel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLeftMenuBackground",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "konydbxlogobignverted.png"
                    },
                    "imgBottomLogo": {
                        "src": "konydbxinverted.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPanel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxPasswordAgeAndLockoutSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPasswordAgeAndLockoutSettings",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxf5f6f8",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPasswordAgeAndLockoutSettings.setDefaultUnit(kony.flex.PX);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106px",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "clipBounds": true,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false,
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "top": "58px"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "height": "40px",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PasswordAgeAndLockout.Title\")",
                        "left": "35px"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxMainContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "750px",
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxViewSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "350px",
                "id": "flxViewSettings",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknPasswordAgeLockoutViewFlex",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxViewSettings.setDefaultUnit(kony.flex.DP);
            var flxViewPasswordAgeSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200px",
                "id": "flxViewPasswordAgeSettings",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxViewPasswordAgeSettings.setDefaultUnit(kony.flex.DP);
            var flxViewPasswordAgeSettingsTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "59px",
                "id": "flxViewPasswordAgeSettingsTop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxViewPasswordAgeSettingsTop.setDefaultUnit(kony.flex.DP);
            var lblViewPasswdAgeSettingsTitle = new kony.ui.Label({
                "height": "20px",
                "id": "lblViewPasswdAgeSettingsTitle",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLblSettingGroupNameLatoReg16px",
                "text": "Password Expiry Settings",
                "top": "20px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnUpdate = new kony.ui.Button({
                "centerY": "50%",
                "height": "25px",
                "id": "btnUpdate",
                "isVisible": true,
                "right": "20px",
                "skin": "sknbtnCancelUpdatePasswordLockout",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.edit\")",
                "width": "70px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewPasswordAgeSettingsTop.add(lblViewPasswdAgeSettingsTitle, btnUpdate);
            var flxViewPasswordAgeSettingsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "121px",
                "id": "flxViewPasswordAgeSettingsInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewPasswordAgeSettingsInner.setDefaultUnit(kony.flex.DP);
            var lblViewSeparatorPasswordAge = new kony.ui.Label({
                "height": "1px",
                "id": "lblViewSeparatorPasswordAge",
                "isVisible": true,
                "left": "20px",
                "right": "20px",
                "skin": "sknLblSeparator",
                "top": "0px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewPasswordAgeSettingsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxViewPasswordAgeSettingsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxViewPasswordAgeSettingsContainer.setDefaultUnit(kony.flex.DP);
            var flxViewPasswordAgeSettingsContainer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewPasswordAgeSettingsContainer1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxViewPasswordAgeSettingsContainer1.setDefaultUnit(kony.flex.DP);
            var flxViewPasswordValidity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewPasswordValidity",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxViewPasswordValidity.setDefaultUnit(kony.flex.PX);
            var lblViewPasswordValidityName = new kony.ui.Label({
                "height": "14px",
                "id": "lblViewPasswordValidityName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameView",
                "text": "PASSWORD EXPIRES AFTER",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewPasswordValidityVal = new kony.ui.Label({
                "height": "16px",
                "id": "lblViewPasswordValidityVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblViewPageLatoRegular14px",
                "text": "90 days",
                "top": "10px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewPasswordValidity.add(lblViewPasswordValidityName, lblViewPasswordValidityVal);
            var flxViewPasswordHistoryCount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewPasswordHistoryCount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "34%",
                "zIndex": 1
            }, {}, {});
            flxViewPasswordHistoryCount.setDefaultUnit(kony.flex.DP);
            var lblViewPasswordHistoryCountName = new kony.ui.Label({
                "height": "14px",
                "id": "lblViewPasswordHistoryCountName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameView",
                "text": "DO NOT ALLOW REPETITION OF",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewPasswordHistoryCountVal = new kony.ui.Label({
                "height": "16px",
                "id": "lblViewPasswordHistoryCountVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblViewPageLatoRegular14px",
                "text": "5 previous password(s)",
                "top": "10px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewPasswordHistoryCount.add(lblViewPasswordHistoryCountName, lblViewPasswordHistoryCountVal);
            var flxViewPasswordExpiryWarning = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewPasswordExpiryWarning",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxViewPasswordExpiryWarning.setDefaultUnit(kony.flex.DP);
            var lblViewPasswordExpiryWarningName = new kony.ui.Label({
                "height": "14px",
                "id": "lblViewPasswordExpiryWarningName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameView",
                "text": "PASSWORD EXPIRY WARNING REQUIRED",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewPasswordExpiryWarningVal = new kony.ui.Label({
                "height": "16px",
                "id": "lblViewPasswordExpiryWarningVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblViewPageLatoRegular14px",
                "text": "Yes",
                "top": "10px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewPasswordExpiryWarning.add(lblViewPasswordExpiryWarningName, lblViewPasswordExpiryWarningVal);
            flxViewPasswordAgeSettingsContainer1.add(flxViewPasswordValidity, flxViewPasswordHistoryCount, flxViewPasswordExpiryWarning);
            var flxViewPasswordExpiryWarningThreshold = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewPasswordExpiryWarningThreshold",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewPasswordExpiryWarningThreshold.setDefaultUnit(kony.flex.DP);
            var lblViewPasswordExpiryThresholdName = new kony.ui.Label({
                "height": "14px",
                "id": "lblViewPasswordExpiryThresholdName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameView",
                "text": "NOTIFY BEFORE",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewPasswordExpiryThresholdVal = new kony.ui.Label({
                "height": "16px",
                "id": "lblViewPasswordExpiryThresholdVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblViewPageLatoRegular14px",
                "text": "30 days",
                "top": "10px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewPasswordExpiryWarningThreshold.add(lblViewPasswordExpiryThresholdName, lblViewPasswordExpiryThresholdVal);
            flxViewPasswordAgeSettingsContainer.add(flxViewPasswordAgeSettingsContainer1, flxViewPasswordExpiryWarningThreshold);
            flxViewPasswordAgeSettingsInner.add(lblViewSeparatorPasswordAge, flxViewPasswordAgeSettingsContainer);
            flxViewPasswordAgeSettings.add(flxViewPasswordAgeSettingsTop, flxViewPasswordAgeSettingsInner);
            var flxViewAccountLockoutSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120px",
                "id": "flxViewAccountLockoutSettings",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "200px",
                "width": "100%"
            }, {}, {});
            flxViewAccountLockoutSettings.setDefaultUnit(kony.flex.DP);
            var lblViewAccountLockoutSettingsTitle = new kony.ui.Label({
                "height": "20px",
                "id": "lblViewAccountLockoutSettingsTitle",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLblSettingGroupNameLatoReg16px",
                "text": "Account Lock and Unlock Settings",
                "top": "20px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewAccountLockoutSettingsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "61px",
                "id": "flxViewAccountLockoutSettingsInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "19px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewAccountLockoutSettingsInner.setDefaultUnit(kony.flex.DP);
            var lblViewSeparatorAccountLockout = new kony.ui.Label({
                "height": "1px",
                "id": "lblViewSeparatorAccountLockout",
                "isVisible": true,
                "left": "20px",
                "right": "20px",
                "skin": "sknLblSeparator",
                "top": "0px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewAccountLockoutSettingsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewAccountLockoutSettingsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewAccountLockoutSettingsContainer.setDefaultUnit(kony.flex.DP);
            var flxViewAccountLockoutSettingsContainer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewAccountLockoutSettingsContainer1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px"
            }, {}, {});
            flxViewAccountLockoutSettingsContainer1.setDefaultUnit(kony.flex.DP);
            var flxViewAccountLockoutThreshold = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewAccountLockoutThreshold",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxViewAccountLockoutThreshold.setDefaultUnit(kony.flex.PX);
            var lblAccountLockoutThresholdSetting = new kony.ui.Label({
                "height": "14px",
                "id": "lblAccountLockoutThresholdSetting",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameView",
                "text": "LOCK AFTER",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAccountLockoutThresholdVal = new kony.ui.Label({
                "height": "16px",
                "id": "lblAccountLockoutThresholdVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblViewPageLatoRegular14px",
                "text": "3 attempts",
                "top": "10px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAccountLockoutThreshold.add(lblAccountLockoutThresholdSetting, lblAccountLockoutThresholdVal);
            var flxViewAccountLockoutTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewAccountLockoutTime",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "34%",
                "zIndex": 1
            }, {}, {});
            flxViewAccountLockoutTime.setDefaultUnit(kony.flex.DP);
            var lblViewAccountLockoutTimeName = new kony.ui.Label({
                "height": "14px",
                "id": "lblViewAccountLockoutTimeName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameView",
                "text": "AUTO UNLOCK AFTER",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewAccountLockoutTimeVal = new kony.ui.Label({
                "height": "16px",
                "id": "lblViewAccountLockoutTimeVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblViewPageLatoRegular14px",
                "text": "5 minute(s)",
                "top": "10px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAccountLockoutTime.add(lblViewAccountLockoutTimeName, lblViewAccountLockoutTimeVal);
            var flxViewActivationAndRecoveryEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxViewActivationAndRecoveryEmail",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxViewActivationAndRecoveryEmail.setDefaultUnit(kony.flex.DP);
            var lblViewActivationAndRecoveryEmailName = new kony.ui.Label({
                "height": "14px",
                "id": "lblViewActivationAndRecoveryEmailName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameView",
                "text": "EMAIL LINK EXPIRES AFTER",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewActivationAndRecoveryEmailVal = new kony.ui.Label({
                "height": "16px",
                "id": "lblViewActivationAndRecoveryEmailVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblViewPageLatoRegular14px",
                "text": "3 minute(s)",
                "top": "10px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewActivationAndRecoveryEmail.add(lblViewActivationAndRecoveryEmailName, lblViewActivationAndRecoveryEmailVal);
            flxViewAccountLockoutSettingsContainer1.add(flxViewAccountLockoutThreshold, flxViewAccountLockoutTime, flxViewActivationAndRecoveryEmail);
            flxViewAccountLockoutSettingsContainer.add(flxViewAccountLockoutSettingsContainer1);
            flxViewAccountLockoutSettingsInner.add(lblViewSeparatorAccountLockout, flxViewAccountLockoutSettingsContainer);
            flxViewAccountLockoutSettings.add(lblViewAccountLockoutSettingsTitle, flxViewAccountLockoutSettingsInner);
            flxViewSettings.add(flxViewPasswordAgeSettings, flxViewAccountLockoutSettings);
            var flxUpdateSettings = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "540px",
                "id": "flxUpdateSettings",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffoptemplateop3px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxUpdateSettings.setDefaultUnit(kony.flex.DP);
            var flxEditContainerScroll = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "460dp",
                "horizontalScrollIndicator": true,
                "id": "flxEditContainerScroll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxEditContainerScroll.setDefaultUnit(kony.flex.DP);
            var flxUpdatePasswordAgeSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "280px",
                "id": "flxUpdatePasswordAgeSettings",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxUpdatePasswordAgeSettings.setDefaultUnit(kony.flex.DP);
            var lblUpdatePasswdAgeSettingsTitle = new kony.ui.Label({
                "height": "20px",
                "id": "lblUpdatePasswdAgeSettingsTitle",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLblSettingGroupNameLatoReg16px",
                "text": "Password Expiry Settings",
                "top": "20px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUpdatePasswordAgeSettingsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "220px",
                "id": "flxUpdatePasswordAgeSettingsInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "19px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordAgeSettingsInner.setDefaultUnit(kony.flex.DP);
            var flxAgeSettingsSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxAgeSettingsSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxConfigurationBundlesFillLightGrey",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAgeSettingsSeparator.setDefaultUnit(kony.flex.DP);
            flxAgeSettingsSeparator.add();
            var flxUpdatePasswordAgeSettingsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200px",
                "id": "flxUpdatePasswordAgeSettingsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordAgeSettingsContainer.setDefaultUnit(kony.flex.DP);
            var flxUpdatePasswordAgeSettingsContainer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxUpdatePasswordAgeSettingsContainer1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxUpdatePasswordAgeSettingsContainer1.setDefaultUnit(kony.flex.DP);
            var flxUpdatePasswordValidity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxUpdatePasswordValidity",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordValidity.setDefaultUnit(kony.flex.PX);
            var lblUpdatePasswordValidityName = new kony.ui.Label({
                "height": "16px",
                "id": "lblUpdatePasswordValidityName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameUpdate",
                "text": "Password expires after",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUpdatePasswordValidityVal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxUpdatePasswordValidityVal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknUpdatePasswordSettingsNormal",
                "top": "9px",
                "width": "140px",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordValidityVal.setDefaultUnit(kony.flex.DP);
            var tbxUpdatePasswordValidityVal = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "38px",
                "id": "tbxUpdatePasswordValidityVal",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 3,
                "placeholder": "Number",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxUpdatePasswordValidityUnit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUpdatePasswordValidityUnit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80px",
                "isModalContainer": false,
                "skin": "sknFlxEAEBEFunits",
                "top": "0px",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordValidityUnit.setDefaultUnit(kony.flex.DP);
            var lblUpdatePasswordValidityUnitVal = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "16px",
                "id": "lblUpdatePasswordValidityUnitVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknIcommonPasswordLockoutUnits",
                "text": "day(s)",
                "top": "0px",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpdatePasswordValidityUnit.add(lblUpdatePasswordValidityUnitVal);
            flxUpdatePasswordValidityVal.add(tbxUpdatePasswordValidityVal, flxUpdatePasswordValidityUnit);
            var flxPasswordValidityError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxPasswordValidityError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "280px",
                "zIndex": 1
            }, {}, {});
            flxPasswordValidityError.setDefaultUnit(kony.flex.DP);
            var lblPasswordValidityErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblPasswordValidityErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPasswordValidityError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblPasswordValidityError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Value should be between 1 and 999",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPasswordValidityError.add(lblPasswordValidityErrorIcon, lblPasswordValidityError);
            flxUpdatePasswordValidity.add(lblUpdatePasswordValidityName, flxUpdatePasswordValidityVal, flxPasswordValidityError);
            var flxUpdatePasswordHistoryCount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxUpdatePasswordHistoryCount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "34%",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordHistoryCount.setDefaultUnit(kony.flex.DP);
            var lblUpdatePasswordHistoryCountName = new kony.ui.Label({
                "height": "16px",
                "id": "lblUpdatePasswordHistoryCountName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameUpdate",
                "text": "Do not allow repetition of",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUpdatePasswordHistoryCountVal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxUpdatePasswordHistoryCountVal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxeaebefop1px",
                "top": "9px",
                "width": "240px",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordHistoryCountVal.setDefaultUnit(kony.flex.DP);
            var tbxUpdatePasswordHistoryCountVal = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "38px",
                "id": "tbxUpdatePasswordHistoryCountVal",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 1,
                "placeholder": "Number",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxUpdatePasswordHistoryCountUnit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUpdatePasswordHistoryCountUnit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80px",
                "isModalContainer": false,
                "skin": "sknFlxEAEBEFunits",
                "top": "0px",
                "width": "160px",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordHistoryCountUnit.setDefaultUnit(kony.flex.DP);
            var lblUpdatePasswordHistoryCountUnitVal = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "16px",
                "id": "lblUpdatePasswordHistoryCountUnitVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknIcommonPasswordLockoutUnits",
                "text": "previous password(s)",
                "top": "0px",
                "width": "140px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpdatePasswordHistoryCountUnit.add(lblUpdatePasswordHistoryCountUnitVal);
            flxUpdatePasswordHistoryCountVal.add(tbxUpdatePasswordHistoryCountVal, flxUpdatePasswordHistoryCountUnit);
            var flxPasswordHistoryCountError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxPasswordHistoryCountError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "280px",
                "zIndex": 1
            }, {}, {});
            flxPasswordHistoryCountError.setDefaultUnit(kony.flex.DP);
            var lblPasswordHistoryCountErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblPasswordHistoryCountErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPasswordHistoryCountError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblPasswordHistoryCountError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Value should be between 1 and 9",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPasswordHistoryCountError.add(lblPasswordHistoryCountErrorIcon, lblPasswordHistoryCountError);
            flxUpdatePasswordHistoryCount.add(lblUpdatePasswordHistoryCountName, flxUpdatePasswordHistoryCountVal, flxPasswordHistoryCountError);
            var flxUpdatePasswordExpiryWarning = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxUpdatePasswordExpiryWarning",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordExpiryWarning.setDefaultUnit(kony.flex.DP);
            var lblUpdatePasswordExpiryWarningName = new kony.ui.Label({
                "height": "16px",
                "id": "lblUpdatePasswordExpiryWarningName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameUpdate",
                "text": "Password expiry warning required",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxExpiryWarningRadioButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxExpiryWarningRadioButton",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxExpiryWarningRadioButton.setDefaultUnit(kony.flex.DP);
            var flxExpiryWarningRadioYes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxExpiryWarningRadioYes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxExpiryWarningRadioYes.setDefaultUnit(kony.flex.DP);
            var imgExpiryWarningRadioYes = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgExpiryWarningRadioYes",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxExpiryWarningRadioYes.add(imgExpiryWarningRadioYes);
            var lblExpiryWarningRadioYes = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExpiryWarningRadioYes",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "YES",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxExpiryWarningRadioNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxExpiryWarningRadioNo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxExpiryWarningRadioNo.setDefaultUnit(kony.flex.DP);
            var imgExpiryWarningRadioNo = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgExpiryWarningRadioNo",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxExpiryWarningRadioNo.add(imgExpiryWarningRadioNo);
            var lblExpiryWarningRadioNo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExpiryWarningRadioNo",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "NO",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxExpiryWarningRadioButton.add(flxExpiryWarningRadioYes, lblExpiryWarningRadioYes, flxExpiryWarningRadioNo, lblExpiryWarningRadioNo);
            flxUpdatePasswordExpiryWarning.add(lblUpdatePasswordExpiryWarningName, flxExpiryWarningRadioButton);
            flxUpdatePasswordAgeSettingsContainer1.add(flxUpdatePasswordValidity, flxUpdatePasswordHistoryCount, flxUpdatePasswordExpiryWarning);
            var flxUpdatePasswordExpiryWarningThreshold = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxUpdatePasswordExpiryWarningThreshold",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordExpiryWarningThreshold.setDefaultUnit(kony.flex.DP);
            var lblUpdatePasswordExpiryThresholdSettingName = new kony.ui.Label({
                "height": "16px",
                "id": "lblUpdatePasswordExpiryThresholdSettingName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameUpdate",
                "text": "Notify before",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUpdatePasswordExpiryWarningThresholdVal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxUpdatePasswordExpiryWarningThresholdVal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxeaebefop1px",
                "top": "9px",
                "width": "140px",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordExpiryWarningThresholdVal.setDefaultUnit(kony.flex.DP);
            var tbxUpdatePasswordExpiryThresholdVal = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "38px",
                "id": "tbxUpdatePasswordExpiryThresholdVal",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 2,
                "placeholder": "Number",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxUpdatePasswordExpiryThresholdUnit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUpdatePasswordExpiryThresholdUnit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80px",
                "isModalContainer": false,
                "skin": "sknFlxEAEBEFunits",
                "top": "0px",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordExpiryThresholdUnit.setDefaultUnit(kony.flex.DP);
            var lblUpdatePasswordExpiryThresholdUnitVal = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "16px",
                "id": "lblUpdatePasswordExpiryThresholdUnitVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknIcommonPasswordLockoutUnits",
                "text": "day(s)",
                "top": "0px",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpdatePasswordExpiryThresholdUnit.add(lblUpdatePasswordExpiryThresholdUnitVal);
            flxUpdatePasswordExpiryWarningThresholdVal.add(tbxUpdatePasswordExpiryThresholdVal, flxUpdatePasswordExpiryThresholdUnit);
            var flxUpdatePasswordExpiryWarningThresholdError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxUpdatePasswordExpiryWarningThresholdError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "280px",
                "zIndex": 1
            }, {}, {});
            flxUpdatePasswordExpiryWarningThresholdError.setDefaultUnit(kony.flex.DP);
            var lblUpdatePasswordExpiryWarningThresholdErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblUpdatePasswordExpiryWarningThresholdErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUpdatePasswordExpiryWarningThresholdError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblUpdatePasswordExpiryWarningThresholdError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Value should be between 1 and 99",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpdatePasswordExpiryWarningThresholdError.add(lblUpdatePasswordExpiryWarningThresholdErrorIcon, lblUpdatePasswordExpiryWarningThresholdError);
            flxUpdatePasswordExpiryWarningThreshold.add(lblUpdatePasswordExpiryThresholdSettingName, flxUpdatePasswordExpiryWarningThresholdVal, flxUpdatePasswordExpiryWarningThresholdError);
            flxUpdatePasswordAgeSettingsContainer.add(flxUpdatePasswordAgeSettingsContainer1, flxUpdatePasswordExpiryWarningThreshold);
            flxUpdatePasswordAgeSettingsInner.add(flxAgeSettingsSeparator, flxUpdatePasswordAgeSettingsContainer);
            flxUpdatePasswordAgeSettings.add(lblUpdatePasswdAgeSettingsTitle, flxUpdatePasswordAgeSettingsInner);
            var flxUpdateAccountLockoutSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "180px",
                "id": "flxUpdateAccountLockoutSettings",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "280px",
                "width": "100%"
            }, {}, {});
            flxUpdateAccountLockoutSettings.setDefaultUnit(kony.flex.DP);
            var lblUpdateAccountLockoutSettingsTitle = new kony.ui.Label({
                "height": "20px",
                "id": "lblUpdateAccountLockoutSettingsTitle",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLblSettingGroupNameLatoReg16px",
                "text": "Account Lock and Unlock Settings",
                "top": "20px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUpdateAccountLockoutSettingsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120px",
                "id": "flxUpdateAccountLockoutSettingsInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "19px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUpdateAccountLockoutSettingsInner.setDefaultUnit(kony.flex.DP);
            var flxUpdateSeparatorAccountLockout = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxUpdateSeparatorAccountLockout",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxConfigurationBundlesFillLightGrey",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxUpdateSeparatorAccountLockout.setDefaultUnit(kony.flex.DP);
            flxUpdateSeparatorAccountLockout.add();
            var flxUpdateAccountLockoutSettingsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxUpdateAccountLockoutSettingsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxUpdateAccountLockoutSettingsContainer.setDefaultUnit(kony.flex.DP);
            var flxUpdateAccountLockoutSettingsContainer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxUpdateAccountLockoutSettingsContainer1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxUpdateAccountLockoutSettingsContainer1.setDefaultUnit(kony.flex.DP);
            var flxUpdateAccountLockoutThreshold = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxUpdateAccountLockoutThreshold",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxUpdateAccountLockoutThreshold.setDefaultUnit(kony.flex.PX);
            var lblUpdateAccountLockoutThresholdName = new kony.ui.Label({
                "height": "16px",
                "id": "lblUpdateAccountLockoutThresholdName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameUpdate",
                "text": "Lock after",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUpdateAccountLockoutThresholdVal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxUpdateAccountLockoutThresholdVal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxeaebefop1px",
                "top": "10px",
                "width": "160px",
                "zIndex": 1
            }, {}, {});
            flxUpdateAccountLockoutThresholdVal.setDefaultUnit(kony.flex.DP);
            var tbxUpdateAccountLockoutThresholdVal = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "38px",
                "id": "tbxUpdateAccountLockoutThresholdVal",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 2,
                "placeholder": "Number",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxUpdateAccountLockoutThresholdUnit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxUpdateAccountLockoutThresholdUnit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80px",
                "isModalContainer": false,
                "skin": "sknFlxEAEBEFunits",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxUpdateAccountLockoutThresholdUnit.setDefaultUnit(kony.flex.DP);
            var lblUpdateAccountLockoutThresholdUnitVal = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "16px",
                "id": "lblUpdateAccountLockoutThresholdUnitVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknIcommonPasswordLockoutUnits",
                "text": "attempt(s)",
                "top": "0px",
                "width": "60px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpdateAccountLockoutThresholdUnit.add(lblUpdateAccountLockoutThresholdUnitVal);
            flxUpdateAccountLockoutThresholdVal.add(tbxUpdateAccountLockoutThresholdVal, flxUpdateAccountLockoutThresholdUnit);
            var flxUpdateAccountLockoutThresholdError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxUpdateAccountLockoutThresholdError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "280px",
                "zIndex": 1
            }, {}, {});
            flxUpdateAccountLockoutThresholdError.setDefaultUnit(kony.flex.DP);
            var lblUpdateAccountLockoutThresholdErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblUpdateAccountLockoutThresholdErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUpdateAccountLockoutThresholdError = new kony.ui.Label({
                "height": "15px",
                "id": "lblUpdateAccountLockoutThresholdError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "text": "Value should be between 1 and 99",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpdateAccountLockoutThresholdError.add(lblUpdateAccountLockoutThresholdErrorIcon, lblUpdateAccountLockoutThresholdError);
            flxUpdateAccountLockoutThreshold.add(lblUpdateAccountLockoutThresholdName, flxUpdateAccountLockoutThresholdVal, flxUpdateAccountLockoutThresholdError);
            var flxUpdateAccountLockoutTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxUpdateAccountLockoutTime",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "34%",
                "zIndex": 1
            }, {}, {});
            flxUpdateAccountLockoutTime.setDefaultUnit(kony.flex.DP);
            var lblUpdateAccountLockoutTimeName = new kony.ui.Label({
                "height": "16px",
                "id": "lblUpdateAccountLockoutTimeName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameUpdate",
                "text": "Auto unlock after",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUpdateAccountLockoutTimeVal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxUpdateAccountLockoutTimeVal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxeaebefop1px",
                "top": "10px",
                "width": "160px",
                "zIndex": 1
            }, {}, {});
            flxUpdateAccountLockoutTimeVal.setDefaultUnit(kony.flex.DP);
            var tbxUpdateAccountLockoutTimeVal = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "38px",
                "id": "tbxUpdateAccountLockoutTimeVal",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 4,
                "placeholder": "Number",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxUpdateAccountLockoutTimeUnit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxUpdateAccountLockoutTimeUnit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80px",
                "isModalContainer": false,
                "skin": "sknFlxEAEBEFunits",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxUpdateAccountLockoutTimeUnit.setDefaultUnit(kony.flex.DP);
            var lblUpdateAccountLockoutTimeUnitVal = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "16px",
                "id": "lblUpdateAccountLockoutTimeUnitVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknIcommonPasswordLockoutUnits",
                "text": "minute(s)",
                "top": "0px",
                "width": "60px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpdateAccountLockoutTimeUnit.add(lblUpdateAccountLockoutTimeUnitVal);
            flxUpdateAccountLockoutTimeVal.add(tbxUpdateAccountLockoutTimeVal, flxUpdateAccountLockoutTimeUnit);
            var flxUpdateAccountLockoutTimeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxUpdateAccountLockoutTimeError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "280px",
                "zIndex": 1
            }, {}, {});
            flxUpdateAccountLockoutTimeError.setDefaultUnit(kony.flex.DP);
            var lblUpdateAccountLockoutTimeErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblUpdateAccountLockoutTimeErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUpdateAccountLockoutTimeError = new kony.ui.Label({
                "height": "15px",
                "id": "lblUpdateAccountLockoutTimeError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "text": "Value should be between 1 and 9999",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpdateAccountLockoutTimeError.add(lblUpdateAccountLockoutTimeErrorIcon, lblUpdateAccountLockoutTimeError);
            flxUpdateAccountLockoutTime.add(lblUpdateAccountLockoutTimeName, flxUpdateAccountLockoutTimeVal, flxUpdateAccountLockoutTimeError);
            var flxUpdateActivationAndRecoveryEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxUpdateActivationAndRecoveryEmail",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxUpdateActivationAndRecoveryEmail.setDefaultUnit(kony.flex.DP);
            var lblUpdateActivationAndRecoveryEmailName = new kony.ui.Label({
                "id": "lblUpdateActivationAndRecoveryEmailName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknSettingsNameUpdate",
                "text": "Email link expires after",
                "top": "0px",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUpdateActivationAndRecoveryEmailVal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxUpdateActivationAndRecoveryEmailVal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxeaebefop1px",
                "top": "10px",
                "width": "160px",
                "zIndex": 1
            }, {}, {});
            flxUpdateActivationAndRecoveryEmailVal.setDefaultUnit(kony.flex.DP);
            var tbxUpdateActivationAndRecoveryEmailVal = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "38px",
                "id": "tbxUpdateActivationAndRecoveryEmailVal",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 4,
                "placeholder": "Number",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxUpdateActivationAndRecoveryEmailUnit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxUpdateActivationAndRecoveryEmailUnit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80px",
                "isModalContainer": false,
                "skin": "sknFlxEAEBEFunits",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxUpdateActivationAndRecoveryEmailUnit.setDefaultUnit(kony.flex.DP);
            var lblUpdateActivationAndRecoveryEmailUnitVal = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "16px",
                "id": "lblUpdateActivationAndRecoveryEmailUnitVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknIcommonPasswordLockoutUnits",
                "text": "minute(s)",
                "top": "0px",
                "width": "54px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpdateActivationAndRecoveryEmailUnit.add(lblUpdateActivationAndRecoveryEmailUnitVal);
            flxUpdateActivationAndRecoveryEmailVal.add(tbxUpdateActivationAndRecoveryEmailVal, flxUpdateActivationAndRecoveryEmailUnit);
            var flxUpdateActivationAndRecoveryEmailError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxUpdateActivationAndRecoveryEmailError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "280px",
                "zIndex": 1
            }, {}, {});
            flxUpdateActivationAndRecoveryEmailError.setDefaultUnit(kony.flex.DP);
            var lblUpdateActivationAndRecoveryEmailErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblUpdateActivationAndRecoveryEmailErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUpdateActivationAndRecoveryEmailError = new kony.ui.Label({
                "height": "15px",
                "id": "lblUpdateActivationAndRecoveryEmailError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "text": "Value should be between 1 and 9999",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpdateActivationAndRecoveryEmailError.add(lblUpdateActivationAndRecoveryEmailErrorIcon, lblUpdateActivationAndRecoveryEmailError);
            flxUpdateActivationAndRecoveryEmail.add(lblUpdateActivationAndRecoveryEmailName, flxUpdateActivationAndRecoveryEmailVal, flxUpdateActivationAndRecoveryEmailError);
            flxUpdateAccountLockoutSettingsContainer1.add(flxUpdateAccountLockoutThreshold, flxUpdateAccountLockoutTime, flxUpdateActivationAndRecoveryEmail);
            flxUpdateAccountLockoutSettingsContainer.add(flxUpdateAccountLockoutSettingsContainer1);
            flxUpdateAccountLockoutSettingsInner.add(flxUpdateSeparatorAccountLockout, flxUpdateAccountLockoutSettingsContainer);
            flxUpdateAccountLockoutSettings.add(lblUpdateAccountLockoutSettingsTitle, flxUpdateAccountLockoutSettingsInner);
            flxEditContainerScroll.add(flxUpdatePasswordAgeSettings, flxUpdateAccountLockoutSettings);
            var flxUpdateButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxUpdateButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUpdateButtons.setDefaultUnit(kony.flex.DP);
            var lblUpdateSeparatorAccountLockoutSetting = new kony.ui.Label({
                "height": "1px",
                "id": "lblUpdateSeparatorAccountLockoutSetting",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "skin": "sknLblSeparator",
                "top": "0px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "20px",
                "width": "223px",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var btnCancelUpdate = new kony.ui.Button({
                "height": "40px",
                "id": "btnCancelUpdate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknbtnCancelUpdatePasswordLockout",
                "text": "CANCEL",
                "top": "0px",
                "width": "104px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnDoUpdate = new kony.ui.Button({
                "height": "40dp",
                "id": "btnDoUpdate",
                "isVisible": true,
                "left": "15px",
                "skin": "sknBlueBtnUpdate",
                "text": "UPDATE",
                "top": "0dp",
                "width": "104px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxButtons.add(btnCancelUpdate, btnDoUpdate);
            flxUpdateButtons.add(lblUpdateSeparatorAccountLockoutSetting, flxButtons);
            flxUpdateSettings.add(flxEditContainerScroll, flxUpdateButtons);
            flxMainContent.add(flxViewSettings, flxUpdateSettings);
            flxPasswordAgeAndLockoutSettings.add(flxMainHeader, flxMainContent);
            flxRightPanel.add(flxPasswordAgeAndLockoutSettings);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxMain.add(flxLeftPanel, flxRightPanel, flxHeaderDropdown, flxToastMessage, flxLoading);
            var flxPasswordAgeLockoutCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPasswordAgeLockoutCancel",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPasswordAgeLockoutCancel.setDefaultUnit(kony.flex.DP);
            var popUpDelete = new com.adminConsole.common.popUp1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUpDelete",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "150px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.YES_CANCEL\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Cancel_changes\")"
                    },
                    "popUp1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.cancel_mfa_configs_message\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPasswordAgeLockoutCancel.add(popUpDelete);
            this.add(flxMain, flxPasswordAgeLockoutCancel);
        };
        return [{
            "addWidgets": addWidgetsfrmPasswordAgeAndLockoutSettings,
            "enabledForIdleTimeout": true,
            "id": "frmPasswordAgeAndLockoutSettings",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_h85264f728ae411ca654adb83a558d6a(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});