define("flxNotificationsOuter", function() {
    return function(controller) {
        var flxNotificationsOuter = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxNotificationsOuter",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxNotificationsOuter.setDefaultUnit(kony.flex.DP);
        var flxNotificationRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxNotificationRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxNotificationRow.setDefaultUnit(kony.flex.DP);
        var lblNotificationId = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblNotificationId",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "1234567",
            "top": "15dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDetails = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblDetails",
            "isVisible": true,
            "left": "21%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Travel Request",
            "top": "15dp",
            "width": "13%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSelectedCards = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblSelectedCards",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "abc Credit account **** **** **** 1234",
            "top": "15dp",
            "width": "37%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatusCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxStatusCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "79%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "13%",
            "zIndex": 1
        }, {}, {});
        flxStatusCont.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "In progress",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatusCont.add(lblIconStatus, lblStatus);
        var flxIconCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxIconCont",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "95%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "25dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxIconCont.setDefaultUnit(kony.flex.DP);
        var lblIconAction = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconAction",
            "isVisible": true,
            "skin": "sknIconCancelNotification28px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxIconCont.add(lblIconAction);
        flxNotificationRow.add(lblNotificationId, lblDetails, lblSelectedCards, flxStatusCont, flxIconCont);
        var flxRowLine = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxRowLine",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSeparatorCsr",
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxRowLine.setDefaultUnit(kony.flex.DP);
        flxRowLine.add();
        flxNotificationsOuter.add(flxNotificationRow, flxRowLine);
        return flxNotificationsOuter;
    }
})