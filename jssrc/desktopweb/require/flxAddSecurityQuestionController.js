define("userflxAddSecurityQuestionController", {
    //Type your controller code here 
    activateSwitch: function() {
        this.view.switchToggle.selectedIndex = 1;
    }
});
define("flxAddSecurityQuestionControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onBeginEditing defined for txtAddSecurityQuestion **/
    AS_TextArea_j11d35e595f541edac1298f2e3dc1794: function AS_TextArea_j11d35e595f541edac1298f2e3dc1794(eventobject, context) {
        var self = this;
        this.activateSwitch();
    }
});
define("flxAddSecurityQuestionController", ["userflxAddSecurityQuestionController", "flxAddSecurityQuestionControllerActions"], function() {
    var controller = require("userflxAddSecurityQuestionController");
    var controllerActions = ["flxAddSecurityQuestionControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
