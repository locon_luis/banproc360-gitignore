define("userflxCustomerCardsListController", {
    hide: function() {
        this.executeOnParent("toggleVisibility");
    }
});
define("flxCustomerCardsListControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("flxCustomerCardsListController", ["userflxCustomerCardsListController", "flxCustomerCardsListControllerActions"], function() {
    var controller = require("userflxCustomerCardsListController");
    var controllerActions = ["flxCustomerCardsListControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
