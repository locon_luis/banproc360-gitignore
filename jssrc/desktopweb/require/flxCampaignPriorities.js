define("flxCampaignPriorities", function() {
    return function(controller) {
        var flxCampaignPriorities = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCampaignPriorities",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxCampaignPriorities.setDefaultUnit(kony.flex.DP);
        var lblCampaign = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblCampaign",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
            "top": "15dp",
            "width": "53%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPriority = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblPriority",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCampaignPriorities.add(lblCampaign, lblPriority);
        return flxCampaignPriorities;
    }
})