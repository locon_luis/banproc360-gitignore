define(['ModelManager'], function(ModelManager) {
    function ConfigurationsManager() {
        kony.mvc.Business.Delegator.call(this);
    }
    inheritsFrom(ConfigurationsManager, kony.mvc.Business.Delegator);
    ConfigurationsManager.prototype.initializeBusinessController = function() {};
    ConfigurationsManager.prototype.addEligibilityCriterias = function(context, onSuccess, onError) {
        ModelManager.invoke('EligibilityCriteria', 'addEligibilityCriteria', context, onSuccess, onError);
    };
    ConfigurationsManager.prototype.getEligibilityCriteria = function(context, onSuccess, onError) {
        ModelManager.invoke('EligibilityCriteria', 'getEligibilityCriteria', {}, onSuccess, onError);
    };
    ConfigurationsManager.prototype.deleteEligibilityCriteria = function(context, onSuccess, onError) {
        ModelManager.invoke('EligibilityCriteria', 'deleteEligibilityCriteria', {
            "criteriaID": context.id
        }, onSuccess, onError);
    };
    ConfigurationsManager.prototype.updateEligibilityCriteria = function(context, onSuccess, onError) {
        ModelManager.invoke('EligibilityCriteria', 'editEligibilityCriteria', context, onSuccess, onError);
    };
    ConfigurationsManager.prototype.fetchAllBundles = function(context, onSuccess, onError) {
        ModelManager.invoke('Configuration', 'fetchBundles', {}, onSuccess, onError);
    };
    ConfigurationsManager.prototype.fetchAllConfigurations = function(context, onSuccess, onError) {
        ModelManager.invoke('Configuration', 'fetchConfigurations', {}, onSuccess, onError);
    };
    ConfigurationsManager.prototype.fetchConfigurations = function(bundleId, onSuccess, onError) {
        ModelManager.invoke('Configuration', 'fetchConfigurations', bundleId, onSuccess, onError);
    };
    ConfigurationsManager.prototype.manageBundleAndConfigurations = function(bundle, onSuccess, onError) {
        ModelManager.invoke('Configuration', 'manageBundleAndConfigurations', bundle, onSuccess, onError);
    };
    ConfigurationsManager.prototype.deleteBundleAndConfigurations = function(id, onSuccess, onError) {
        ModelManager.invoke('Configuration', 'deleteBundleAndConfigurations', id, onSuccess, onError);
    };
    ConfigurationsManager.prototype.fetchTermsAndConds = function(context, onSuccess, onError) {
        ModelManager.invoke('OnboardingTermsAndConditions', 'getOnboardingTermsAndConditions', context, onSuccess, onError);
    };
    return ConfigurationsManager;
});