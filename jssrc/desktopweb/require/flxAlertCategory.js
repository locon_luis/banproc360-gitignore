define("flxAlertCategory", function() {
    return function(controller) {
        var flxAlertCategory = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertCategory",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBottomBordere1e5ed",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxAlertCategory.setDefaultUnit(kony.flex.DP);
        var lblAlertCategory = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblAlertCategory",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold485c7512px",
            "text": "Label",
            "top": "20px",
            "width": "60%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLblLatoReg12px333333"
        });
        var lblArrow = new kony.ui.Label({
            "bottom": "20px",
            "height": "15px",
            "id": "lblArrow",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblcursor",
            "text": "",
            "top": "20px",
            "width": "30px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAlertCategory.add(lblAlertCategory, lblArrow);
        return flxAlertCategory;
    }
})