define("flxCustMangTransctionHistory", function() {
    return function(controller) {
        var flxCustMangTransctionHistory = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxCustMangTransctionHistory",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "skin": "sknCursor"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustMangTransctionHistory.setDefaultUnit(kony.flex.DP);
        var flxCustMangRequestHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxCustMangRequestHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxCustMangRequestHeader.setDefaultUnit(kony.flex.DP);
        var flxRecurrIcon = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxRecurrIcon",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "13px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b2ed7f8b5b004c0b98b0250f16718382,
            "skin": "slFbox",
            "width": "15dp",
            "zIndex": 2
        }, {}, {});
        flxRecurrIcon.setDefaultUnit(kony.flex.DP);
        var lblLimitsIcon = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblLimitsIcon",
            "isVisible": true,
            "right": "0px",
            "skin": "sknFontIconLimits13Px",
            "text": "",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRecurrIcon.add(lblLimitsIcon);
        var flxFirstColoum = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxFirstColoum",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "43px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b2ed7f8b5b004c0b98b0250f16718382,
            "skin": "slFbox",
            "width": "10%",
            "zIndex": 2
        }, {}, {});
        flxFirstColoum.setDefaultUnit(kony.flex.DP);
        var lblRefNo = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRefNo",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "RI123456",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFirstColoum.add(lblRefNo);
        var lblDateAndTime = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDateAndTime",
            "isVisible": true,
            "left": "15%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "01/01/2019 10:30",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTransctionDescription = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTransctionDescription",
            "isVisible": true,
            "left": "30%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Payment",
            "width": "27%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblType",
            "isVisible": true,
            "left": "57%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Fund transfer",
            "width": "13%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxAmountOriginal = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxAmountOriginal",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "70%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b2ed7f8b5b004c0b98b0250f16718382,
            "skin": "slFbox",
            "width": "15%",
            "zIndex": 2
        }, {}, {});
        flxAmountOriginal.setDefaultUnit(kony.flex.DP);
        var lblAmountOriginalSign = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAmountOriginalSign",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAmountOriginalSymbol = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAmountOriginalSymbol",
            "isVisible": true,
            "left": "-7px",
            "skin": "sknIcomoonCurrencySymbol",
            "text": "",
            "width": "18px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAmountOriginal = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAmountOriginal",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "100,000.00",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAmountOriginal.add(lblAmountOriginalSign, lblAmountOriginalSymbol, lblAmountOriginal);
        var flxAmountConverted = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxAmountConverted",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "85%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b2ed7f8b5b004c0b98b0250f16718382,
            "skin": "slFbox",
            "width": "15%",
            "zIndex": 2
        }, {}, {});
        flxAmountConverted.setDefaultUnit(kony.flex.DP);
        var lblAmountConvertedSign = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAmountConvertedSign",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAmountConvertedSymbol = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAmountConvertedSymbol",
            "isVisible": true,
            "left": "-7px",
            "skin": "sknIcomoonCurrencySymbol",
            "text": "",
            "width": "18px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAmountConverted = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAmountConverted",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "990,000.00",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAmountConverted.add(lblAmountConvertedSign, lblAmountConvertedSymbol, lblAmountConverted);
        flxCustMangRequestHeader.add(flxRecurrIcon, flxFirstColoum, lblDateAndTime, lblTransctionDescription, lblType, flxAmountOriginal, flxAmountConverted);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "35px",
            "right": "35px",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustMangTransctionHistory.add(flxCustMangRequestHeader, lblSeperator);
        return flxCustMangTransctionHistory;
    }
})