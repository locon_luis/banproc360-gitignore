define("flxAdminConsoleLogList", function() {
    return function(controller) {
        var flxAdminConsoleLogList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAdminConsoleLogList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxAdminConsoleLogList.setDefaultUnit(kony.flex.DP);
        var flxAdminConsoleLogWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "17dp",
            "clipBounds": true,
            "id": "flxAdminConsoleLogWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0",
            "width": "1200dp"
        }, {}, {});
        flxAdminConsoleLogWrapper.setDefaultUnit(kony.flex.DP);
        var lblEvent = new kony.ui.Label({
            "id": "lblEvent",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "EVENT",
            "top": "17px",
            "width": "8%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUserName = new kony.ui.Label({
            "id": "lblUserName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "USERNAME",
            "top": "17px",
            "width": "9%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUserRole = new kony.ui.Label({
            "id": "lblUserRole",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "USER ROLE",
            "top": "17px",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblModuleName = new kony.ui.Label({
            "id": "lblModuleName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "MODULE NAME",
            "top": "17px",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "17px",
            "width": "9%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var imgStatus = new kony.ui.Image2({
            "height": "8px",
            "id": "imgStatus",
            "isVisible": false,
            "left": "0dp",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "8px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "id": "lblStatus",
            "isVisible": true,
            "left": "18px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Active",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconStatus = new kony.ui.Label({
            "height": "15dp",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon13pxGreen",
            "text": "",
            "top": "1dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(imgStatus, lblStatus, lblIconStatus);
        var lblDateAndTime = new kony.ui.Label({
            "id": "lblDateAndTime",
            "isVisible": true,
            "left": "0px",
            "right": "2%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "DATE & TIME",
            "top": "17px",
            "width": "11%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "id": "lblDescription",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "DESCRIPTION",
            "top": "17px",
            "width": "28%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAdminConsoleLogWrapper.add(lblEvent, lblUserName, lblUserRole, lblModuleName, flxStatus, lblDateAndTime, lblDescription);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblSeperator",
            "text": ".",
            "width": "1184px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAdminConsoleLogList.add(flxAdminConsoleLogWrapper, lblSeperator);
        return flxAdminConsoleLogList;
    }
})