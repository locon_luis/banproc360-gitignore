define("flxAddRole", function() {
    return function(controller) {
        var flxAddRole = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddRole",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {});
        flxAddRole.setDefaultUnit(kony.flex.DP);
        var flxAddWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": 20,
            "skin": "sknflxffffffOp100Border5e90cbRadius3Px",
            "top": "10dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "blocksHover"
        });
        flxAddWrapper.setDefaultUnit(kony.flex.DP);
        var lblRoleName = new kony.ui.Label({
            "id": "lblRoleName",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "CSR Agent",
            "top": "15px",
            "width": "120px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnAdd = new kony.ui.Button({
            "height": "20px",
            "id": "btnAdd",
            "isVisible": true,
            "onClick": controller.AS_Button_fa163d0f68044aaaa7f73ec7e3863105,
            "right": 20,
            "skin": "sknBtnLatoRegular11abeb12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknBtnLatoRegular1682b212px"
        });
        var rtxRoleDescription = new kony.ui.RichText({
            "bottom": "15dp",
            "id": "rtxRoleDescription",
            "isVisible": true,
            "left": "20dp",
            "right": 40,
            "skin": "sknrtxLato485c7514px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "top": "40dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAddWrapper.add(lblRoleName, btnAdd, rtxRoleDescription);
        flxAddRole.add(flxAddWrapper);
        return flxAddRole;
    }
})