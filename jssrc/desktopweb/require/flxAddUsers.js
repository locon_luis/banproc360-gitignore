define("flxAddUsers", function() {
    return function(controller) {
        var flxAddUsers = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80dp",
            "id": "flxAddUsers",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {});
        flxAddUsers.setDefaultUnit(kony.flex.DP);
        var flxAddUsersWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "70px",
            "id": "flxAddUsersWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": 20,
            "skin": "sknflxffffffOp100Border5e90cbRadius3Px",
            "top": "10dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "blocksHover"
        });
        flxAddUsersWrapper.setDefaultUnit(kony.flex.DP);
        var lblFullName = new kony.ui.Label({
            "id": "lblFullName",
            "isVisible": true,
            "left": "20px",
            "right": "55px",
            "skin": "sknlbl485c7514px",
            "text": "John Doe",
            "top": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxxUsernameWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxxUsernameWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "38dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxxUsernameWrapper.setDefaultUnit(kony.flex.DP);
        var lblUsername = new kony.ui.Label({
            "id": "lblUsername",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLatoRegular93a5bc12px",
            "text": "USERNAME",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUserIdValue = new kony.ui.Label({
            "id": "lblUserIdValue",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoRegular485c75Font12px",
            "text": "TDU34256987",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxxUsernameWrapper.add(lblUsername, lblUserIdValue);
        var btnAdd = new kony.ui.Button({
            "id": "btnAdd",
            "isVisible": true,
            "onClick": controller.AS_Button_i5f0e1b1d621469bb17b0e21177ee705,
            "right": 20,
            "skin": "sknBtnLatoRegular11abeb12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknBtnLatoRegular1682b212px"
        });
        flxAddUsersWrapper.add(lblFullName, flxxUsernameWrapper, btnAdd);
        flxAddUsers.add(flxAddUsersWrapper);
        return flxAddUsers;
    }
})