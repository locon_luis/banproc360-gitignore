define([], function() {
    function ReportsManagementModule_BusinessController() {
        kony.mvc.Business.Delegator.call(this);
    }
    inheritsFrom(ReportsManagementModule_BusinessController, kony.mvc.Business.Delegator);
    ReportsManagementModule_BusinessController.prototype.initializeBusinessController = function() {};
    /**
     * @name getReportsInfo
     * @member ReportsManagementModule.businessController
     * @param {} payload
     * @param (response:{csrNames : [{name : string, id : string}], opstatus : number, category : [{name : string, id : string}], httpStatusCode : number, httpresponse : {headers : string, url : string, responsecode : number}})=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    ReportsManagementModule_BusinessController.prototype.getReportsInfo = function(payload, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ReportsManager").businessController.getReportsInfo(payload, onSuccess, onError);
    };
    /**
     * @name getMessagesReport
     * @member ReportsManagementModule.businessController
     * @param {user_ID : string, startDate : string, endDate : string, category : string, csrName : string} getMessagesReportJSON
     * @param (response:{opstatus : number, messages : [{name : string, value : string}], threads : [{name : string, value : string}], httpStatusCode : number, httpresponse : {headers : string, url : string, responsecode : number}})=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    ReportsManagementModule_BusinessController.prototype.getMessagesReport = function(getMessagesReportJSON, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ReportsManager").businessController.getMessagesReport(getMessagesReportJSON, onSuccess, onError);
    };
    /**
     * @name getTransactionalReport
     * @member ReportsManagementModule.businessController
     * @param {startDate : string, endDate : string} getTransactionalReportJSON
     * @param (response:{records : [], httpresponse : {headers : string, url : string, responsecode : number}, opstatus : number})=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    ReportsManagementModule_BusinessController.prototype.getTransactionalReport = function(getTransactionalReportJSON, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ReportsManager").businessController.getTransactionalReport(getTransactionalReportJSON, onSuccess, onError);
    };
    ReportsManagementModule_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return ReportsManagementModule_BusinessController;
});