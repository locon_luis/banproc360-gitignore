define("ReportsManagementModule/userfrmReportsManagementController", {
    selectedCsrId: "",
    csrNameBeingTyped: false,
    willUpdateUI: function(context) {
        this.updateLeftMenu(context);
        if (context) {
            if (context.LoadingScreen) {
                if (context.LoadingScreen.focus) kony.adminConsole.utils.showProgressBar(this.view);
                else kony.adminConsole.utils.hideProgressBar(this.view);
            } else if (context.reportsInfo) {
                this.setReportsInfo(context.reportsInfo);
                this.showReportTabs();
            } else if (context.messagesReport) {
                this.setMessagesReport(context.messagesReport);
            } else if (context.transactionalReport) {
                this.setTransactionalReport(context.transactionalReport);
            } else if (context.toast == "error") {
                this.view.toastMessage.showErrorToastMessage(context.message, this);
            }
        }
    },
    reportManagementPreshow: function() {
        this.hideAll();
        this.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
        this.setDefaultWidgetsData();
        this.setFlowActions();
        this.showReportTabs();
        this.view.forceLayout();
        this.view.flxMain.height = kony.os.deviceInfo().screenHeight + "px";
        this.view.usersReport.lblLog.text = "\ue914";
        this.view.usersReport.lblLog.skin = "sknIcon60px";
        this.view.transactionReport.lblLog.text = "\ue913";
        this.view.transactionReport.lblLog.skin = "sknIcon60px";
        this.view.messagesReport.lblLog.text = "\ue912";
        this.view.messagesReport.lblLog.skin = "sknIcon60px";
    },
    setDefaultWidgetsData: function() {
        //Header
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmReportsManagement.titleReports");
        //Breadcrumb
        this.view.breadcrumbs.btnBackToMain.text = "REPORTS";
        //Users reports
        this.view.datePickerUsers.resetData = "Today";
        //Messages
        this.view.flxSuggestions.setVisibility(false);
    },
    hideAll: function() {
        //hide all function
        this.view.search2.flxSuggestionsClose.setVisibility(false);
        this.view.flxBreadCrumbs.setVisibility(false);
        this.view.flxReportsTabs.setVisibility(false);
        this.view.flxMainContainer.setVisibility(false);
        this.view.flxUsersReport.setVisibility(false);
        this.view.flxTransactionsReport.setVisibility(false);
        this.view.flxMessagesReport.setVisibility(false);
        this.view.search.flxCategory.setVisibility(false);
        this.view.search.flxCSR.setVisibility(false);
        this.view.search1.flxCategory.setVisibility(false);
        this.view.search1.flxCSR.setVisibility(false);
    },
    setFlowActions: function() {
        var scopeObj = this;
        this.view.flxCloseCal1.onClick = function() {
            scopeObj.view.datePickerTransactions.resetData = "Today";
            scopeObj.view.datePickerTransactions.rangeType = "Today";
            scopeObj.view.datePickerTransactions.value = scopeObj.getTodaysFormattedDate() + " - " + scopeObj.getTodaysFormattedDate();
            scopeObj.view.flxCloseCal1.setVisibility(false);
            scopeObj.view.forceLayout();
        };
        this.view.flxCloseCal2.onClick = function() {
            scopeObj.view.datePickerMessages.resetData = "Today";
            scopeObj.view.datePickerMessages.rangeType = "Today";
            scopeObj.view.datePickerMessages.value = scopeObj.getTodaysFormattedDate() + " - " + scopeObj.getTodaysFormattedDate();
            scopeObj.view.flxCloseCal2.setVisibility(false);
            scopeObj.view.forceLayout();
        };
        this.view.flxCloseCal3.onClick = function() {
            scopeObj.view.datePickerUsers.resetData = "Today";
            scopeObj.view.datePickerUsers.rangeType = "Today";
            scopeObj.view.datePickerUsers.value = scopeObj.getTodaysFormattedDate() + " - " + scopeObj.getTodaysFormattedDate();
            scopeObj.view.flxCloseCal3.setVisibility(false);
            scopeObj.view.forceLayout();
        };
        this.view.datePickerTransactions.event = function() {
            scopeObj.view.flxCloseCal1.setVisibility(true);
            scopeObj.view.forceLayout();
        };
        this.view.datePickerMessages.event = function() {
            scopeObj.view.flxCloseCal2.setVisibility(true);
            scopeObj.view.forceLayout();
        };
        this.view.datePickerUsers.event = function() {
            scopeObj.view.flxCloseCal3.setVisibility(true);
            scopeObj.view.forceLayout();
        };
        //Tabs onclick
        this.view.usersReport.onClick = function() {
            scopeObj.setDataForUsersReport();
            scopeObj.showUsersReport();
        };
        this.view.transactionReport.onClick = function() {
            scopeObj.showTransactionReport();
        };
        this.view.messagesReport.onClick = function() {
            scopeObj.showMessagesReport();
        };
        //Bread crumb onclick
        this.view.breadcrumbs.btnBackToMain.onClick = function() {
            scopeObj.showReportTabs();
        };
        //CSR tbx onkeyup
        this.view.search2.tbxCSR.onKeyUp = function() {
            scopeObj.loadPageData();
            scopeObj.searchCSR();
            scopeObj.view.forceLayout();
            scopeObj.csrNameBeingTyped = true;
            if (scopeObj.view.search2.tbxCSR.text.trim() == "") {
                scopeObj.view.search2.btnAdd.skin = "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px";
                scopeObj.view.search2.btnAdd.hoverSkin = "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px";
                scopeObj.view.search2.btnAdd.focusSkin = "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px";
            } else {
                scopeObj.view.search2.btnAdd.skin = "sknBtnAddDisabled";
                scopeObj.view.search2.btnAdd.hoverSkin = "sknBtnAddDisabled";
                scopeObj.view.search2.btnAdd.focusSkin = "sknBtnAddDisabled";
            }
        };
        //Suggestions onhover
        this.view.flxSuggestions.onHover = function(widget, context) {
            if (context.eventType === constants.ONHOVER_MOUSE_ENTER);
            else if (context.eventType === constants.ONHOVER_MOUSE_MOVE);
            else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                widget.setVisibility(false);
            }
        };
        //suggestions onRowClick
        this.view.segUserSuggestion.onRowClick = function() {
            scopeObj.view.search2.tbxCSR.text = scopeObj.view.segUserSuggestion.selecteditems[0].lblName;
            scopeObj.selectedCsrId = scopeObj.view.segUserSuggestion.selecteditems[0].id;
            scopeObj.view.search2.flxSuggestionsClose.setVisibility(true);
            scopeObj.view.flxSuggestions.setVisibility(false);
            scopeObj.csrNameBeingTyped = false;
            scopeObj.view.search2.btnAdd.skin = "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px";
            scopeObj.view.search2.btnAdd.hoverSkin = "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px";
            scopeObj.view.search2.btnAdd.focusSkin = "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px";
            scopeObj.view.forceLayout();
        };
        //Suggestions on img close
        this.view.search2.flxSuggestionsClose.onClick = function() {
            scopeObj.view.search2.tbxCSR.text = "";
            scopeObj.selectedCsrId = "";
            scopeObj.view.search2.flxSuggestionsClose.setVisibility(false);
            scopeObj.csrNameBeingTyped = false;
            scopeObj.view.search2.btnAdd.skin = "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px";
            scopeObj.view.search2.btnAdd.hoverSkin = "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px";
            scopeObj.view.search2.btnAdd.focusSkin = "sknbtnffffffLatoBolda5abc412Px485c751pxBorderRadius20px";
            scopeObj.view.forceLayout();
        };
        this.view.search2.btnAdd.onClick = function() {
            scopeObj.getMessagesReport();
        };
        this.view.search1.btnAdd.onClick = function() {
            scopeObj.getTransactionalReport();
        };
        this.view.search2.flxDownload.onClick = function() {
            if (scopeObj.csrNameBeingTyped === false) {
                var authToken = KNYMobileFabric.currentClaimToken;
                var mfURL = KNYMobileFabric.mainRef.config.reportingsvc.session.split("/services")[0];
                var downloadURL = mfURL + "/services/data/v1/ReportsObjService/operations/MessagesReport/exportMessagesReport?authToken=" + authToken;
                var startDate = "",
                    endDate = "";
                var rangeType = scopeObj.view.datePickerMessages.value;
                startDate = rangeType.substring(0, rangeType.indexOf(" - "));
                endDate = rangeType.substring(rangeType.indexOf(" - ") + 3);
                var category = scopeObj.view.search2.lstCategory.selectedKeyValue;
                downloadURL = downloadURL + "&startDate=" + startDate;
                downloadURL = downloadURL + "&endDate=" + endDate;
                downloadURL = downloadURL + "&categoryId=" + category[0];
                downloadURL = downloadURL + "&categoryName=" + category[1];
                downloadURL = downloadURL + "&csrid=" + scopeObj.selectedCsrId;
                downloadURL = downloadURL + "&csrName=" + scopeObj.view.search2.tbxCSR.text;
                downloadURL = downloadURL + "&offset=" + new Date().getTimezoneOffset();
                var encodedURI = encodeURI(downloadURL);
                var downloadLink = document.createElement("a");
                downloadLink.href = encodedURI;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        };
        this.view.search1.flxDownload.onClick = function() {
            var authToken = KNYMobileFabric.currentClaimToken;
            var mfURL = KNYMobileFabric.mainRef.config.reportingsvc.session.split("/services")[0];
            var downloadURL = mfURL + "/services/data/v1/ReportsObjService/operations/TransactionReport/exportTransactionReport?authToken=" + authToken;
            var startDate = "",
                endDate = "";
            var rangeType = scopeObj.view.datePickerTransactions.value;
            startDate = rangeType.substring(0, rangeType.indexOf(" - "));
            endDate = rangeType.substring(rangeType.indexOf(" - ") + 3);
            downloadURL = downloadURL + "&startDate=" + startDate;
            downloadURL = downloadURL + "&endDate=" + endDate;
            var encodedURI = encodeURI(downloadURL);
            var downloadLink = document.createElement("a");
            downloadLink.href = encodedURI;
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        };
    },
    showReportTabs: function() {
        this.hideAll();
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmReportsManagement.titleReports");
        this.view.search2.tbxCSR.text = "";
        this.view.richtextNoResult.setVisibility(false);
        this.view.segUserSuggestion.setVisibility(true);
        this.view.flxReportsTabs.setVisibility(true);
        this.view.forceLayout();
    },
    setReportsInfo: function(reportsInfoJSON) {
        kony.print("Inside setReportsInfo() of frmReportsController");
        var scopeObj = this;
        scopeObj.sortBy = scopeObj.getObjectSorter("name");
        scopeObj.loadPageData = function() {
            var searchResult = reportsInfoJSON.csrNames.filter(scopeObj.searchFilter).sort(scopeObj.sortBy.sortData);
            scopeObj.records = reportsInfoJSON.csrNames.filter(scopeObj.searchFilter).length;
            if (scopeObj.records === 0) {
                scopeObj.view.richtextNoResult.setVisibility(true);
                scopeObj.view.segUserSuggestion.setVisibility(false);
                scopeObj.view.richtextNoResult.text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found") + scopeObj.view.search2.tbxCSR.text + kony.i18n.getLocalizedString("i18n.frmCSRController._Try_with_another_keyword");
            } else {
                scopeObj.view.richtextNoResult.setVisibility(false);
                scopeObj.view.segUserSuggestion.setVisibility(true);
                scopeObj.setCsrNames(searchResult);
            }
        };
        scopeObj.loadPageData();
        scopeObj.setCategory(reportsInfoJSON.category);
    },
    setCategory: function(categories) {
        kony.print("categories: " + JSON.stringify(categories));
        var category = [];
        category.push(["Select category", "Select All"]);
        for (var i = 0; i < Object.keys(categories).length; ++i) {
            category.push([categories[i].id, categories[i].name]);
        }
        this.view.search2.lstCategory.masterData = category;
        this.view.forceLayout();
    },
    setCsrNames: function(csrNames) {
        kony.print("csrNames: " + JSON.stringify(csrNames));
        var scopeObj = this;
        var dataMap = {
            "flxReportsMangSuggestions": "flxReportsMangSuggestions",
            "lblName": "lblName",
            "id": "id"
        };
        this.view.segUserSuggestion.widgetDataMap = dataMap;
        this.view.segUserSuggestion.data = [];
        var segCsrNames = this.view.segUserSuggestion.data;
        for (var j = 0; j < Object.keys(csrNames).length; ++j) {
            var toAdd = {
                "flxReportsMangSuggestions": csrNames[j].name,
                "lblName": csrNames[j].name,
                "template": "flxReportsMangSuggestions",
                "id": csrNames[j].id
            };
            segCsrNames.push(toAdd);
        }
        this.view.segUserSuggestion.setData(segCsrNames);
        this.view.forceLayout();
    },
    setMessagesReport: function(messagesReportJSON) {
        kony.print("Inside setMessagesReport() of frmReportsManagementController");
        var scopeObj = this;
        // Messages
        kony.print("messagesReportJSON.messages: " + JSON.stringify(messagesReportJSON.messages));
        var messagesDataMap = {
            "flxFirstColumn": "flxFirstColumn",
            "flxlastColoumn": "flxlastColoumn",
            "flxMessage": "flxMessage",
            "flxReportsMangMessages": "flxReportsMangMessages",
            "lblDescription": "lblDescription",
            "lblSeperator": "lblSeperator",
            "lblValue": "lblValue"
        };
        scopeObj.view.segMessages.widgetDataMap = messagesDataMap;
        scopeObj.view.segMessages.data = [];
        var messages = scopeObj.view.segMessages.data;
        for (var i = 0; i < Object.keys(messagesReportJSON.messages).length; ++i) {
            var message = {
                "flxFirstColumn": "flxFirstColumn",
                "flxlastColoumn": "flxlastColoumn",
                "flxMessage": "flxMessage",
                "flxReportsMangMessages": "flxReportsMangMessages",
                "lblDescription": messagesReportJSON.messages[i].name,
                "lblSeperator": "lblSeperator",
                "lblValue": messagesReportJSON.messages[i].value,
                "template": "flxReportsMangMessages"
            };
            messages.push(message);
        }
        scopeObj.view.segMessages.setData(messages);
        // Threads
        kony.print("messagesReportJSON.threads: " + JSON.stringify(messagesReportJSON.threads));
        var threadsDataMap = {
            "flxFirstColumn": "flxFirstColumn",
            "flxlastColoumn": "flxlastColoumn",
            "flxMessage": "flxMessage",
            "flxReportsMangMessages": "flxReportsMangMessages",
            "lblDescription": "lblDescription",
            "lblSeperator": "lblSeperator",
            "lblValue": "lblValue"
        };
        scopeObj.view.segThreads.widgetDataMap = threadsDataMap;
        scopeObj.view.segThreads.data = [];
        var threads = scopeObj.view.segThreads.data;
        for (var j = 0; j < Object.keys(messagesReportJSON.threads).length; ++j) {
            var thread = {
                "flxFirstColumn": "flxFirstColumn",
                "flxlastColoumn": "flxlastColoumn",
                "flxMessage": "flxMessage",
                "flxReportsMangMessages": "flxReportsMangMessages",
                "lblDescription": messagesReportJSON.threads[j].name,
                "lblSeperator": "lblSeperator",
                "lblValue": messagesReportJSON.threads[j].value,
                "template": "flxReportsMangMessages"
            };
            threads.push(thread);
        }
        scopeObj.view.segThreads.setData(threads);
        kony.adminConsole.utils.hideProgressBar(scopeObj.view);
        this.view.forceLayout();
    },
    setTransactionalReport: function(transactionalReportJSON) {
        var scopeObj = this;
        kony.print("transactionalReportJSON.messages: " + JSON.stringify(transactionalReportJSON.messages));
        var data = scopeObj.modifyData(transactionalReportJSON);
        if (!(Object.keys(data).length)) {
            this.view.flxNoResultsFound.setVisibility(true);
        } else {
            this.view.flxNoResultsFound.setVisibility(false);
        }
        var transactionalDataMap = {
            //       "flxShadow":"flxShadow",
            "flxBottomSpace": "flxBottomSpace",
            "flxHeader": "flxHeader",
            "flxMobile": "flxMobile",
            "flxMobileValue": "flxMobileValue",
            "flxMobileVolume": "flxMobileVolume",
            "flxOnline": "flxOnline",
            "flxOnlineValue": "flxOnlineValue",
            "flxOnlineVolume": "flxOnlineVolume",
            "flxTotal": "flxTotal",
            "flxTotalValue": "flxTotalValue",
            "flxTotalVolume": "flxTotalVolume",
            "flxTransaction": "flxTransaction",
            "flxTransactionReports": "flxTransactionReports",
            "flxTransactionType": "flxTransactionType",
            "FlxTransactionValue": "FlxTransactionValue",
            "flxTransactionVolume": "flxTransactionVolume",
            "flxValue": "flxValue",
            "flxVolume": "flxVolume",
            "lblMobile": "lblMobile",
            "lblMobileSymbol": "lblMobileSymbol",
            "lblMobileValue": "lblMobileValue",
            "lblMobileVolume": "lblMobileVolume",
            "lblOnline": "lblOnline",
            "lblOnlineSymbol": "lblOnlineSymbol",
            "lblOnlineValue": "lblOnlineValue",
            "lblOnlineVolume": "lblOnlineVolume",
            "lblTotal": "lblTotal",
            "lblTotalSymbol": "lblTotalSymbol",
            "lblTotalValue": "lblTotalValue",
            "lblTotalVolume": "lblTotalVolume",
            "lblTransactionType": "lblTransactionType",
            "lblTransactionVolume": "lblTransactionVolume",
            "lblValue": "lblValue"
        };
        scopeObj.view.segTransactionReports.widgetDataMap = transactionalDataMap;
        scopeObj.view.segTransactionReports.data = [];
        var reportsList = scopeObj.view.segTransactionReports.data;
        for (var i = 0; i < Object.keys(data).length; ++i) {
            var totalValue = data[i].mobileValue === undefined ? data[i].onlineValue : (data[i].onlineValue === undefined ? data[i].mobileValue : (parseFloat(data[i].mobileValue) + parseFloat(data[i].onlineValue)).toFixed(2));
            var totalVolume = data[i].mobileVolume === undefined ? data[i].onlineVolume : (data[i].onlineVolume === undefined ? data[i].mobileVolume : parseInt(data[i].mobileVolume) + parseInt(data[i].onlineVolume));
            var report = {
                //         "flxShadow":"flxShadow",
                "flxBottomSpace": "flxBottomSpace",
                "flxHeader": "flxHeader",
                "flxMobile": "flxMobile",
                "flxMobileValue": "flxMobileValue",
                "flxMobileVolume": "flxMobileVolume",
                "flxOnline": "flxOnline",
                "flxOnlineValue": "flxOnlineValue",
                "flxOnlineVolume": "flxOnlineVolume",
                "flxTotal": "flxTotal",
                "flxTotalValue": "flxTotalValue",
                "flxTotalVolume": "flxTotalVolume",
                "flxTransaction": "flxTransaction",
                "flxTransactionReports": "flxTransactionReports",
                "flxTransactionType": "flxTransactionType",
                "FlxTransactionValue": "FlxTransactionValue",
                "flxTransactionVolume": "flxTransactionVolume",
                "flxValue": "flxValue",
                "flxVolume": "flxVolume",
                "lblMobile": "MOBILE",
                "lblMobileSymbol": this.defaultCurrencyCode(),
                "lblMobileValue": data[i].mobileValue ? data[i].mobileValue : "0",
                "lblMobileVolume": data[i].mobileVolume ? "# " + data[i].mobileVolume : "# 0",
                "lblOnline": "ONLINE",
                "lblOnlineSymbol": this.defaultCurrencyCode(),
                "lblOnlineValue": data[i].onlineValue ? data[i].onlineValue : "0",
                "lblOnlineVolume": data[i].onlineVolume ? "# " + data[i].onlineVolume : "# 0",
                "lblTotal": "TOTAL",
                "lblTotalSymbol": this.defaultCurrencyCode(),
                "lblTotalValue": totalValue ? totalValue : "0",
                "lblTotalVolume": "# " + totalVolume,
                "lblTransactionType": data[i].serviceName,
                "lblTransactionVolume": "VOLUME",
                "lblValue": "VALUE",
                "template": "flxTransactionReports"
            };
            reportsList.push(report);
        }
        scopeObj.view.segTransactionReports.setData(reportsList);
        kony.adminConsole.utils.hideProgressBar(scopeObj.view);
        this.view.forceLayout();
    },
    modifyData: function(reportsList) {
        var reportsData = JSON.parse(JSON.stringify(reportsList));
        var data = [];
        var row = {};
        var list = [];
        for (var i = 0; i < reportsData.length; i++) {
            if (list.indexOf(i) != -1) continue;
            row = {};
            row.serviceName = reportsData[i].serviceName;
            if (!row.serviceName) {
                continue;
            }
            if (reportsData[i].channel.toUpperCase() == "MOBILE APP") {
                row.mobileValue = reportsData[i].value;
                row.mobileVolume = reportsData[i].volume;
            } else if (reportsData[i].channel.toUpperCase() == "ONLINE BANKING") {
                row.onlineValue = reportsData[i].value;
                row.onlineVolume = reportsData[i].volume;
            }
            inner: for (var j = i + 1; j < reportsData.length; j++) {
                if (reportsData[i].serviceName == reportsData[j].serviceName) {
                    list.push(j);
                    if (reportsData[j].channel.toUpperCase() == "MOBILE APP") {
                        row.mobileValue = reportsData[j].value;
                        row.mobileVolume = reportsData[j].volume;
                    } else if (reportsData[j].channel.toUpperCase() == "ONLINE BANKING") {
                        row.onlineValue = reportsData[j].value;
                        row.onlineVolume = reportsData[j].volume;
                    }
                    break inner;
                }
            }
            data.push(row);
        }
        return data;
    },
    showUsersReport: function() {
        this.hideAll();
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmReportsManagement.titleViewReports");
        this.view.breadcrumbs.lblCurrentScreen.text = "USERS REPORT";
        this.view.flxMainContainer.setVisibility(true);
        this.view.flxBreadCrumbs.setVisibility(true);
        this.view.flxUsersReport.setVisibility(true);
        this.view.flxCloseCal3.setVisibility(false);
        this.view.datePickerUsers.resetData = "Today";
        this.view.datePickerUsers.rangeType = "Today";
        this.view.datePickerUsers.value = this.getTodaysFormattedDate() + " - " + this.getTodaysFormattedDate();
        this.view.forceLayout();
    },
    showTransactionReport: function() {
        this.hideAll();
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmReportsManagement.titleViewReports");
        this.view.breadcrumbs.lblCurrentScreen.text = "TRANSACTIONS REPORT";
        this.view.flxMainContainer.setVisibility(true);
        this.view.flxBreadCrumbs.setVisibility(true);
        this.view.flxTransactionsReport.setVisibility(true);
        this.view.flxCloseCal1.setVisibility(false);
        this.view.datePickerTransactions.resetData = "Today";
        this.view.datePickerTransactions.rangeType = "Today";
        this.view.datePickerTransactions.value = this.getTodaysFormattedDate() + " - " + this.getTodaysFormattedDate();
        kony.adminConsole.utils.showProgressBar(this.view);
        this.presenter.getTransactionalReport({
            "startDate": this.getTodaysFormattedDate(),
            "endDate": this.getTodaysFormattedDate()
        }); //{"startDate":startDate, "endDate":startDate}
        this.view.forceLayout();
    },
    showMessagesReport: function() {
        var scopeObj = this;
        this.hideAll();
        this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmReportsManagement.titleViewReports");
        this.view.breadcrumbs.lblCurrentScreen.text = "MESSAGES REPORT";
        this.view.flxMainContainer.setVisibility(true);
        this.view.flxBreadCrumbs.setVisibility(true);
        this.view.flxMessagesReport.setVisibility(true);
        this.view.flxCloseCal2.setVisibility(false);
        this.view.datePickerMessages.resetData = "Today";
        this.view.datePickerMessages.rangeType = "Today";
        this.view.datePickerMessages.value = this.getTodaysFormattedDate() + " - " + this.getTodaysFormattedDate();
        this.view.search2.lstCategory.selectedKey = "Select category";
        kony.adminConsole.utils.showProgressBar(this.view);
        var getMessagesReportJSON = {
            "user_ID": kony.mvc.MDAApplication.getSharedInstance().appContext.userID,
            "startDate": scopeObj.getTodaysFormattedDate() + "00:00:00",
            "endDate": scopeObj.getTodaysFormattedDate() + "23:59:59",
            "category": "Select category",
            "csrName": ""
        };
        this.presenter.getMessagesReport(getMessagesReportJSON);
        this.view.forceLayout();
    },
    searchCSR: function() {
        this.view.flxSuggestions.setVisibility(true);
        this.view.forceLayout();
    },
    setDataForUsersReport: function() {
        this.view.segListing.widgetDataMap = {
            "flxFirstColumn": "flxFirstColumn",
            "flxlastColoumn": "flxlastColoumn",
            "flxOptions": "flxOptions",
            "flxUsersReport": "flxUsersReport",
            "flxUsersReportWrapper": "flxUsersReportWrapper",
            "imgOptions": "imgOptions",
            "lblDescription": "lblDescription",
            "lblMobile": "lblMobile",
            "lblOnline": "lblOnline",
            "lblSeperator": "lblSeperator",
            "lblTotal": "lblTotal"
        };
        this.view.segListing.setData([{
            "flxFirstColumn": "flxFirstColumn",
            "flxlastColoumn": "flxlastColoumn",
            "flxOptions": "flxOptions",
            "flxUsersReport": "flxUsersReport",
            "flxUsersReportWrapper": "flxUsersReportWrapper",
            "imgOptions": "imgOptions",
            "lblDescription": "Users enrolled",
            "lblMobile": "2000",
            "lblOnline": "3000",
            "lblSeperator": "lblSeperator",
            "lblTotal": "5000",
            "template": "flxUsersReport"
        }, {
            "flxFirstColumn": "flxFirstColumn",
            "flxlastColoumn": "flxlastColoumn",
            "flxOptions": "flxOptions",
            "flxUsersReport": "flxUsersReport",
            "flxUsersReportWrapper": "flxUsersReportWrapper",
            "imgOptions": "imgOptions",
            "lblDescription": "Users with the app installed",
            "lblMobile": "2000",
            "lblOnline": "3000",
            "lblSeperator": "lblSeperator",
            "lblTotal": "5000",
            "template": "flxUsersReport"
        }, {
            "flxFirstColumn": "flxFirstColumn",
            "flxlastColoumn": "flxlastColoumn",
            "flxOptions": "flxOptions",
            "flxUsersReport": "flxUsersReport",
            "flxUsersReportWrapper": "flxUsersReportWrapper",
            "imgOptions": "imgOptions",
            "lblDescription": "New enrolments",
            "lblMobile": "120",
            "lblOnline": "120",
            "lblSeperator": "lblSeperator",
            "lblTotal": "240",
            "template": "flxUsersReport"
        }, {
            "flxFirstColumn": "flxFirstColumn",
            "flxlastColoumn": "flxlastColoumn",
            "flxOptions": "flxOptions",
            "flxUsersReport": "flxUsersReport",
            "flxUsersReportWrapper": "flxUsersReportWrapper",
            "imgOptions": "imgOptions",
            "lblDescription": "New installs",
            "lblMobile": "300",
            "lblOnline": "N/A",
            "lblSeperator": "lblSeperator",
            "lblTotal": "300",
            "template": "flxUsersReport"
        }]);
    },
    getMessagesReport: function() {
        kony.print("Inside getMessagesReport() of frmReportsManagementController");
        var scopeObj = this;
        if (scopeObj.csrNameBeingTyped === false) {
            var user_ID = kony.mvc.MDAApplication.getSharedInstance().appContext.userID;
            var rangeType = scopeObj.view.datePickerMessages.value;
            var startDate = rangeType.substring(0, rangeType.indexOf(" - ")) + " 00:00:00";
            var endDate = rangeType.substring(rangeType.indexOf(" - ") + 3) + " 23:59:59";
            var category = this.view.search2.lstCategory.selectedKey;
            var csrName = scopeObj.selectedCsrId;
            var getMessagesReportJSON = {
                "user_ID": user_ID,
                "startDate": startDate,
                "endDate": endDate,
                "category": category,
                "csrName": csrName
            };
            kony.adminConsole.utils.showProgressBar(this.view);
            scopeObj.presenter.getMessagesReport(getMessagesReportJSON);
        }
    },
    getTransactionalReport: function() {
        kony.print("Inside getTransactionalReport() of frmReportsManagementController");
        var scopeObj = this;
        var user_ID = kony.mvc.MDAApplication.getSharedInstance().appContext.userID;
        var startDate = "",
            endDate = "";
        var rangeType = scopeObj.view.datePickerTransactions.value;
        startDate = rangeType.substring(0, rangeType.indexOf(" - "));
        endDate = rangeType.substring(rangeType.indexOf(" - ") + 3);
        var getTransactionalReportJSON = {
            "startDate": startDate,
            "endDate": endDate
        };
        kony.adminConsole.utils.showProgressBar(this.view);
        scopeObj.presenter.getTransactionalReport(getTransactionalReportJSON);
    },
    searchFilter: function(serviceData) {
        var scopeObj = this;
        var searchText = scopeObj.view.search2.tbxCSR.text;
        if (typeof searchText === "string" && searchText.length > 0) {
            return (serviceData.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
        } else {
            return true;
        }
    },
});
define("ReportsManagementModule/frmReportsManagementControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmReportsManagement **/
    AS_Form_e480b4ce691f4ca0a279c18a8ecc78ae: function AS_Form_e480b4ce691f4ca0a279c18a8ecc78ae(eventobject) {
        var self = this;
        this.reportManagementPreshow();
    },
    /** onDeviceBack defined for frmReportsManagement **/
    AS_Form_a5380e6ae74142b7b1584e46088490a4: function AS_Form_a5380e6ae74142b7b1584e46088490a4(eventobject) {
        var self = this;
        //registered
        this.onBrowserBack();
    }
});
define("ReportsManagementModule/frmReportsManagementController", ["ReportsManagementModule/userfrmReportsManagementController", "ReportsManagementModule/frmReportsManagementControllerActions"], function() {
    var controller = require("ReportsManagementModule/userfrmReportsManagementController");
    var controllerActions = ["ReportsManagementModule/frmReportsManagementControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
