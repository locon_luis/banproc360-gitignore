define(['ErrorInterceptor', 'ErrorIsNetworkDown'], function(ErrorInterceptor, isNetworkDown) {
    function ReportsManagement_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }
    inheritsFrom(ReportsManagement_PresentationController, kony.mvc.Presentation.BasePresenter);
    ReportsManagement_PresentationController.prototype.initializePresentationController = function() {
        var self = this;
        ErrorInterceptor.wrap(this, 'businessController').match(function(on) {
            return [
                on(isNetworkDown).do(function() {
                    self.presentUserInterface('frmReportsManagement', {
                        NetworkDownMessage: {}
                    });
                })
            ];
        });
    };
    ReportsManagement_PresentationController.prototype.showLoadingScreen = function() {
        this.presentUserInterface('frmReportsManagement', {
            LoadingScreen: {
                focus: true
            }
        });
    };
    ReportsManagement_PresentationController.prototype.hideLoadingScreen = function() {
        this.presentUserInterface('frmReportsManagement', {
            LoadingScreen: {
                focus: false
            }
        });
    };
    /**
     * @name showReportTabs
     * @member ReportsManagementModule.presentationController
     * 
     */
    ReportsManagement_PresentationController.prototype.showReportTabs = function() {
        kony.print("Inside showReportTabs() of ReportsManagement_PresentationController");
        var self = this;

        function successCallback(response) {
            var context = {
                "reportsInfo": response
            };
            self.presentUserInterface('frmReportsManagement', context);
            self.hideLoadingScreen();
        }

        function failureCallback(error) {
            var context = {
                "toast": "error",
                "message": ErrorInterceptor.errorMessage(error)
            };
            self.presentUserInterface('frmReportsManagement', context);
            self.hideLoadingScreen();
        }
        this.businessController.getReportsInfo({}, successCallback, failureCallback);
        self.showLoadingScreen();
    };
    /**
     * @name getMessagesReport
     * @member ReportsManagementModule.presentationController
     * @param {user_ID : string, startDate : string, endDate : string, category : string, csrName : string} getMessagesReportJSON
     */
    ReportsManagement_PresentationController.prototype.getMessagesReport = function(getMessagesReportJSON) {
        kony.print("Inside getMessagesReport() of ReportsManagement_PresentationController");
        var self = this;

        function successCallback(response) {
            var context = {
                "messagesReport": response
            };
            self.presentUserInterface('frmReportsManagement', context);
            self.hideLoadingScreen();
        }

        function failureCallback(error) {
            var context = {
                "toast": "error",
                "message": ErrorInterceptor.errorMessage(error)
            };
            self.presentUserInterface('frmReportsManagement', context);
            self.hideLoadingScreen();
        }
        this.businessController.getMessagesReport(getMessagesReportJSON, successCallback, failureCallback);
        self.showLoadingScreen();
    };
    /**
     * @name getTransactionalReport
     * @member ReportsManagementModule.presentationController
     * @param {startDate : string, endDate : string} getTransactionalReportJSON
     */
    ReportsManagement_PresentationController.prototype.getTransactionalReport = function(getTransactionalReportJSON) {
        kony.print("Inside getMessagesReport() of ReportsManagement_PresentationController");
        var self = this;

        function successCallback(response) {
            var context = {
                "transactionalReport": response.records
            };
            self.presentUserInterface('frmReportsManagement', context);
            self.hideLoadingScreen();
        }

        function failureCallback(error) {
            self.hideLoadingScreen();
            var context = {
                "toast": "error",
                "message": ErrorInterceptor.errorMessage(error)
            };
            self.presentUserInterface('frmReportsManagement', context);
        }
        this.businessController.getTransactionalReport(getTransactionalReportJSON, successCallback, failureCallback);
        self.showLoadingScreen();
    };
    return ReportsManagement_PresentationController;
});