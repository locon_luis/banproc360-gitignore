define("flxCustomerAdminLog", function() {
    return function(controller) {
        var flxCustomerAdminLog = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxCustomerAdminLog",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustomerAdminLog.setDefaultUnit(kony.flex.DP);
        var flxCustomerActivityLogWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "50px",
            "id": "flxCustomerActivityLogWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0"
        }, {}, {});
        flxCustomerActivityLogWrapper.setDefaultUnit(kony.flex.DP);
        var lblAdminName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAdminName",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "module name",
            "width": "16.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRole = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRole",
            "isVisible": true,
            "right": 0,
            "skin": "sknlblLatoBold35475f14px",
            "text": "create",
            "top": "0%",
            "width": "15.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActivityType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblActivityType",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoBold35475f14px",
            "text": "bryanvn",
            "width": "12.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDescription",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoBold35475f14px",
            "text": "bryanvn",
            "width": "36.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDateAndTime = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDateAndTime",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoBold35475f14px",
            "text": "+91-1234-567",
            "width": "17.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustomerActivityLogWrapper.add(lblAdminName, lblRole, lblActivityType, lblDescription, lblDateAndTime);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "35px",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustomerAdminLog.add(flxCustomerActivityLogWrapper, lblSeperator);
        return flxCustomerAdminLog;
    }
})