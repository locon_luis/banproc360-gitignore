define("flxRolesHeader", function() {
    return function(controller) {
        var flxRolesHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxRolesHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxRolesHeader.setDefaultUnit(kony.flex.DP);
        var flxRoleHeaderName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRoleHeaderName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b38a27ee386d4c37b766c0efa1f60e79,
            "skin": "slFbox",
            "top": "0dp",
            "width": "13.85%",
            "zIndex": 1
        }, {}, {});
        flxRoleHeaderName.setDefaultUnit(kony.flex.DP);
        var lblRoleHeaderName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRoleHeaderName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
            "width": "40px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var fontIconSortName = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconSortName",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxRoleHeaderName.add(lblRoleHeaderName, fontIconSortName);
        var flxRoleHeaderDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRoleHeaderDescription",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "18%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_cf3f3c408ddc4ae4bf29ab1c34d05ab2,
            "skin": "slFbox",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, {}, {});
        flxRoleHeaderDescription.setDefaultUnit(kony.flex.DP);
        var lblRoleHeaderDescription = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRoleHeaderDescription",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
            "width": "90px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRoleHeaderDescription.add(lblRoleHeaderDescription);
        var flxRoleHeaderValidTill = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRoleHeaderValidTill",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "49%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_g4688ec253184083986ccc70f4693a11,
            "skin": "slFbox",
            "top": "0dp",
            "width": "12%",
            "zIndex": 1
        }, {}, {});
        flxRoleHeaderValidTill.setDefaultUnit(kony.flex.DP);
        var lblRoleHeaderValidTill = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRoleHeaderValidTill",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.VALIDTILL\")",
            "width": "65px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconSortValidTill = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconSortValidTill",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxRoleHeaderValidTill.add(lblRoleHeaderValidTill, fontIconSortValidTill);
        var flxRoleHeaderUsers = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRoleHeaderUsers",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "61%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_d8f18db98c474121bcbaf5d4837bed47,
            "skin": "slFbox",
            "top": "0dp",
            "width": "8%",
            "zIndex": 1
        }, {}, {});
        flxRoleHeaderUsers.setDefaultUnit(kony.flex.DP);
        var lblRoleHeaderUsers = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRoleHeaderUsers",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.USERS\")",
            "width": "40px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var fontIconSortUser = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconSortUser",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxRoleHeaderUsers.add(lblRoleHeaderUsers, fontIconSortUser);
        var flxRoleHeaderPermissions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRoleHeaderPermissions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "71%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_jb0ea3031a2549f584c7552a6d5e6158,
            "skin": "slFbox",
            "top": "0dp",
            "width": "11.26%",
            "zIndex": 1
        }, {}, {});
        flxRoleHeaderPermissions.setDefaultUnit(kony.flex.DP);
        var lblRoleHeaderPermissions = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRoleHeaderPermissions",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.PERMISSIONS\")",
            "width": "80px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var fontIconSortPermissions = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconSortPermissions",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxRoleHeaderPermissions.add(lblRoleHeaderPermissions, fontIconSortPermissions);
        var flxRoleHeaderStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRoleHeaderStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "86%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ecdcf65e7b694394b7e864fedc016f61,
            "skin": "slFbox",
            "top": "0dp",
            "width": "8%",
            "zIndex": 1
        }, {}, {});
        flxRoleHeaderStatus.setDefaultUnit(kony.flex.DP);
        var lblRoleHeaderStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRoleHeaderStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var fontIconFilterStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconFilterStatus",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxRoleHeaderStatus.add(lblRoleHeaderStatus, fontIconFilterStatus);
        var lblRoleHeaderSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblRoleHeaderSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblTableHeaderLine",
            "text": ".",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRolesHeader.add(flxRoleHeaderName, flxRoleHeaderDescription, flxRoleHeaderValidTill, flxRoleHeaderUsers, flxRoleHeaderPermissions, flxRoleHeaderStatus, lblRoleHeaderSeperator);
        return flxRolesHeader;
    }
})