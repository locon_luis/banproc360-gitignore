define("userflxFAQController", {
    hide: function() {
        this.executeOnParent("toggleVisibility");
    }
});
define("flxFAQControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxCheckBox **/
    AS_FlexContainer_he74d109b82345c28531e6248229c526: function AS_FlexContainer_he74d109b82345c28531e6248229c526(eventobject, context) {
        var self = this;
        this.executeOnParent("showSelectedRowServices");
    },
    /** onClick defined for flxArrow **/
    AS_FlexContainer_i5d724a10e2a41fbb1e6158cf0d4ea6c: function AS_FlexContainer_i5d724a10e2a41fbb1e6158cf0d4ea6c(eventobject, context) {
        var self = this;
        this.executeOnParent("showSelectedRowServices");
    },
    /** onClick defined for flxOptions **/
    AS_FlexContainer_df66c45f5f48483c9c93eba47d8f6bd6: function AS_FlexContainer_df66c45f5f48483c9c93eba47d8f6bd6(eventobject, context) {
        var self = this;
        this.executeOnParent("toggleVisibility");
    }
});
define("flxFAQController", ["userflxFAQController", "flxFAQControllerActions"], function() {
    var controller = require("userflxFAQController");
    var controllerActions = ["flxFAQControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
