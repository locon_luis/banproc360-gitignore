define("flxImageUploadListHeader", function() {
    return function(controller) {
        var flxImageUploadListHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65px",
            "id": "flxImageUploadListHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "skin": "sknflxffffffop0f6eee665387143"
        }, {}, {});
        flxImageUploadListHeader.setDefaultUnit(kony.flex.DP);
        var flxName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35px",
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "skin": "slFbox0h43cfab67a134d",
            "top": "0dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxName.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "IMAGE",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgSortName = new kony.ui.Image2({
            "centerY": "50%",
            "height": "8px",
            "id": "imgSortName",
            "isVisible": false,
            "left": "10dp",
            "skin": "slImage0e430dcc62b754e",
            "src": "sorting3x.png",
            "top": "0px",
            "width": "13px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxName.add(lblName, imgSortName);
        var flxHeaderStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "right": "90px",
            "skin": "sknCursor",
            "top": "0dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxHeaderStatus.setDefaultUnit(kony.flex.DP);
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "STATUS",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgSortStatus = new kony.ui.Image2({
            "centerY": "50%",
            "height": "9dp",
            "id": "imgSortStatus",
            "isVisible": false,
            "left": "0dp",
            "skin": "slImage0e430dcc62b754e",
            "src": "sorting3x.png",
            "top": "0dp",
            "width": "8dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconFilterStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconFilterStatus",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknSortIconHover15px"
        });
        flxHeaderStatus.add(lblStatus, imgSortStatus, fontIconFilterStatus);
        var lblHeaderSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblHeaderSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblTableHeaderLine",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxImageUploadListHeader.add(flxName, flxHeaderStatus, lblHeaderSeperator);
        return flxImageUploadListHeader;
    }
})