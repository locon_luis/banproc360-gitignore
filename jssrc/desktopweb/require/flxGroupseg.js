define("flxGroupseg", function() {
    return function(controller) {
        var flxGroupseg = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGroupseg",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxGroupseg.setDefaultUnit(kony.flex.DP);
        var flxSegMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegMain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, {}, {});
        flxSegMain.setDefaultUnit(kony.flex.DP);
        var flxGroupsegmain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGroupsegmain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxGroupsegmain.setDefaultUnit(kony.flex.DP);
        var lblGroupName = new kony.ui.Label({
            "id": "lblGroupName",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Platinum Customers",
            "top": "10px",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblHeaderGroupDescription = new kony.ui.Label({
            "id": "lblHeaderGroupDescription",
            "isVisible": true,
            "left": "18.50%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur..Lorem ipsum dolor sit amet, consectetur.",
            "top": "10px",
            "width": "41%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblGroupValidTill = new kony.ui.Label({
            "id": "lblGroupValidTill",
            "isVisible": false,
            "left": "5.70%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "01/12/2018",
            "top": "10px",
            "width": "10.80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblGroupCustomers = new kony.ui.Label({
            "id": "lblGroupCustomers",
            "isVisible": true,
            "left": "61.70%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "25",
            "top": "10px",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblGroupEntitlements = new kony.ui.Label({
            "id": "lblGroupEntitlements",
            "isVisible": true,
            "left": "73.50%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "20",
            "top": "10px",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "5.50%",
            "skin": "slFbox",
            "top": "7px",
            "width": "8.20%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblGroupStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblGroupStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconGroupStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconGroupStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblGroupStatus, fontIconGroupStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_d33034b1b41641c09fcfbd750f938ead,
            "right": "2.20%",
            "skin": "sknFlxBorffffff1pxRound",
            "top": "7px",
            "width": "25px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var fontIconOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "49%",
            "id": "fontIconOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(fontIconOptions);
        flxGroupsegmain.add(lblGroupName, lblHeaderGroupDescription, lblGroupValidTill, lblGroupCustomers, lblGroupEntitlements, flxStatus, flxOptions);
        flxSegMain.add(flxGroupsegmain);
        var lblSeparator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "top": "15px",
            "width": "100%",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxGroupseg.add(flxSegMain, lblSeparator);
        return flxGroupseg;
    }
})