define("flxViewConfigureCSR", function() {
    return function(controller) {
        var flxViewConfigureCSR = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewConfigureCSR",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxViewConfigureCSR.setDefaultUnit(kony.flex.DP);
        var flxViewConfigureRowCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxViewConfigureRowCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknFlxffffffCursorPointer"
        });
        flxViewConfigureRowCont.setDefaultUnit(kony.flex.DP);
        var flxExpandArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxExpandArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "15dp",
            "zIndex": 1
        }, {}, {});
        flxExpandArrow.setDefaultUnit(kony.flex.DP);
        var lblIconArrow = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconArrow",
            "isVisible": true,
            "skin": "sknfontIconDescRightArrow14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsRight\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxExpandArrow.add(lblIconArrow);
        var lblName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblName",
            "isVisible": true,
            "left": "50dp",
            "skin": "sknLblLato485c7513px",
            "text": "Account Transfers",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLine = new kony.ui.Label({
            "height": "1dp",
            "id": "lblLine",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "top": 0,
            "width": "100%",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxEnableToggle = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxEnableToggle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "13%",
            "zIndex": 2
        }, {}, {});
        flxEnableToggle.setDefaultUnit(kony.flex.DP);
        var lblEnable = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblEnable",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLblLato485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.ViewConfigureCSR.Enable\")",
            "top": "12dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxToggleSwitch = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxToggleSwitch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "80dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "28.50%",
            "width": "40dp",
            "zIndex": 1
        }, {}, {});
        flxToggleSwitch.setDefaultUnit(kony.flex.DP);
        var switchToggle = new kony.ui.Switch({
            "height": "25px",
            "id": "switchToggle",
            "isVisible": true,
            "left": "0dp",
            "leftSideText": "ON",
            "right": 0,
            "rightSideText": "OFF",
            "selectedIndex": 1,
            "skin": "sknSwitchServiceManagement",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxToggleSwitch.add(switchToggle);
        flxEnableToggle.add(lblEnable, flxToggleSwitch);
        var flxEnableTick = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxEnableTick",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "10%",
            "zIndex": 2
        }, {}, {});
        flxEnableTick.setDefaultUnit(kony.flex.DP);
        var lblIconGreenTick = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconGreenTick",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknIconGreen16px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEnabled = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblEnabled",
            "isVisible": true,
            "left": "45dp",
            "skin": "sknLblLato485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.ViewConfigureCSR.Enabled\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxEnableTick.add(lblIconGreenTick, lblEnabled);
        flxViewConfigureRowCont.add(flxExpandArrow, lblName, lblLine, flxEnableToggle, flxEnableTick);
        var flxViewConfigureDesc = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewConfigureDesc",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "50dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxViewConfigureDesc.setDefaultUnit(kony.flex.DP);
        var lblLine2 = new kony.ui.Label({
            "height": "1dp",
            "id": "lblLine2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblHeadingDesc = new kony.ui.Label({
            "id": "lblHeadingDesc",
            "isVisible": true,
            "left": "50dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var rtxDescription = new kony.ui.RichText({
            "bottom": "20dp",
            "id": "rtxDescription",
            "isVisible": true,
            "left": "50dp",
            "right": "100dp",
            "skin": "sknrtxLato485c7514px",
            "text": "RichText",
            "top": "40dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewConfigureDesc.add(lblLine2, lblHeadingDesc, rtxDescription);
        flxViewConfigureCSR.add(flxViewConfigureRowCont, flxViewConfigureDesc);
        return flxViewConfigureCSR;
    }
})