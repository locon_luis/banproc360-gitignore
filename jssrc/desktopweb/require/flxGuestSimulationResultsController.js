define("userflxGuestSimulationResultsController", {
    showApplyOnHover: function(rowIndex) {
        this.executeOnParent("showApplyButtonOnHoveredResult", rowIndex);
    },
    showLoanApplications: function() {
        this.executeOnParent("createLoanOnClickOfApply");
    }
});
define("flxGuestSimulationResultsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for lblApply **/
    AS_Label_b794caa48e554f7daec958a878cef5f0: function AS_Label_b794caa48e554f7daec958a878cef5f0(eventobject, x, y, context) {
        var self = this;
        this.showLoanApplications();
    },
    /** onHover defined for flxGuestSimulationResults **/
    AS_FlexContainer_icfe73a5fdd84d76b1f8e626e01565c4: function AS_FlexContainer_icfe73a5fdd84d76b1f8e626e01565c4(eventobject, context) {
        var self = this;
        this.showApplyOnHover(context.rowIndex);
    }
});
define("flxGuestSimulationResultsController", ["userflxGuestSimulationResultsController", "flxGuestSimulationResultsControllerActions"], function() {
    var controller = require("userflxGuestSimulationResultsController");
    var controllerActions = ["flxGuestSimulationResultsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
