define([],function(){

  return{
    setSkinForInfoTabs: function(btnWidget) {
      var widgetArray = [this.view.btnTabName1,
                         this.view.btnTabName2,
                         this.view.btnTabName3,
                         this.view.btnTabName4,
                         this.view.btnTabName5,
                         this.view.btnTabName6,
                         this.view.btnTabName7,
                         this.view.btnTabName8];

      require('TabUtil_FormExtn').tabUtilButtonFunction(widgetArray, btnWidget);
    },
    setFlowActions: function(){
      var scopeObj = this;
      var CustomerManagementPresentation = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CustomerManagementModule").presentationController;
      this.view.btnTabName1.onClick = function(){
        scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName1);
        CustomerManagementPresentation.navigateToContactsTab();
      };
      this.view.btnTabName2.onClick = function(){
        scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName2);
        CustomerManagementPresentation.navigateToAccountsTab();
      };
      this.view.btnTabName3.onClick = function(){
        scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName3);
        CustomerManagementPresentation.navigateToHelpCenterTab();
      };
      this.view.btnTabName4.onClick = function(){
        scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName4);
        CustomerManagementPresentation.navigateToRolesTab();
      };
      this.view.btnTabName5.onClick = function(){
        scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName5);
        CustomerManagementPresentation.navigateToEntitlementsTab();
      };
      this.view.btnTabName6.onClick = function(){
        scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName6);
        CustomerManagementPresentation.navigateToActivityHistoryTab();
      };
      this.view.btnTabName7.onClick = function(){
        scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName7);
        CustomerManagementPresentation.navigateToAlertHistoryTab();
      };
      this.view.btnTabName8.onClick = function(){
        scopeObj.setSkinForInfoTabs(scopeObj.view.btnTabName8);          
        CustomerManagementPresentation.navigateToDeviceInfoTab();
      };
    }
  };
});