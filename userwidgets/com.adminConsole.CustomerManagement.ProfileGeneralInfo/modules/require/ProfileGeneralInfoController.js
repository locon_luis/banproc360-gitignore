define(['AdminConsoleDateTimeUtilities','DateTimeUtils_FormExtn','AdminConsoleCommonUtilities'],function (AdminConsoleDateTimeUtilities, DateTimeUtils_FormExtn,AdminConsoleCommonUtilities){
return {
  NumberOfFlags: 3,
  setBasicInformation: function (CustomerBasicInfo, formInstance) {
    this.view.row1.btnLink2.info = { "emailSentCtr": 0 };
    this.view.alertMessage.setVisibility(false);
    if (!this.view.flxGmInfoDetailWrapper.isVisible) {
      this.toggleGeneralInfoTab();
    }

    if (CustomerBasicInfo.target === "EditScreen") {
      this.setDataForGeneralInformationEditScreen(CustomerBasicInfo, formInstance);
    } else {
      this.setDataForGeneralInformationScreen(CustomerBasicInfo, formInstance);
      this.view.alertMessage.setGeneralInformationAlertMessage(formInstance, formInstance.presenter.getCurrentCustomerLockedOnInfo(),
                                                               formInstance.presenter.getCurrentCustomerRequestAndNotificationCount());
    }
  },

  toggleGeneralInfoTab: function () {
    if (this.view.flxGmInfoDetailWrapper.isVisible) {
      this.view.flxGmInfoDetailWrapper.setVisibility(false);
      this.view.fonticonrightarrow.text = "";
      this.view.forceLayout();
    } else {
      this.view.flxGmInfoDetailWrapper.setVisibility(true);
      this.view.fonticonrightarrow.text = "";
      this.view.forceLayout();
    }
  },

  setDataForGeneralInformationEditScreen: function (basicInfo, formInstance) {
    this.fillSalutation();
    var NormalSkin = "sknlstbxNormal0f9abd8e88aa64a";
    //set edit data
    this.view.EditGeneralInfo.lblError1.setVisibility(false);
    this.view.EditGeneralInfo.lblError2.setVisibility(false);
    this.view.EditGeneralInfo.lblError3.setVisibility(false);
    this.view.EditGeneralInfo.lblError4.setVisibility(false);
    this.view.EditGeneralInfo.lstboxDetails1.skin = NormalSkin;
    this.view.EditGeneralInfo.lstboxDetails2.skin = NormalSkin;
    this.view.EditGeneralInfo.lstboxDetails3.skin = NormalSkin;
    this.view.EditGeneralInfo.lstboxDetails4.skin = NormalSkin;

    this.view.EditGeneralInfo.lstboxDetails1.selectedKey = basicInfo.customer.Salutation ? basicInfo.customer.Salutation : "lbl1";
    this.view.EditGeneralInfo.lstboxDetails2.selectedKey = basicInfo.customer.CustomerStatus_id ? basicInfo.customer.CustomerStatus_id : "lbl1";
    this.view.EditGeneralInfo.lstboxDetails3.selectedKey = basicInfo.customer.MaritalStatus_id ? basicInfo.customer.MaritalStatus_id : "lbl1";
    this.view.EditGeneralInfo.lstboxDetails4.selectedKey = basicInfo.customer.EmployementStatus_id ? basicInfo.customer.EmployementStatus_id : "lbl1";
    var statusKeys = basicInfo.customer.CustomerFlag_ids ? basicInfo.customer.CustomerFlag_ids.split(",") : basicInfo.customer.CustomerFlag_ids;
    var i;
    for (i = 1; i <= this.NumberOfFlags; i++) {
      this.view.EditGeneralInfo["imgFlag" + i].src = formInstance.AdminConsoleCommonUtils.checkbox;
    }
    this.view.EditGeneralInfo.flxSelectFlags.info = {};
    var initialFlagList = [];
    if (statusKeys) {
      for (i = 1; i <= this.NumberOfFlags; i++) {
        for (var j = 0; j < statusKeys.length; j++) {
          if (this.view.EditGeneralInfo["imgFlag" + i].info.Key === statusKeys[j].trim()) {
            this.view.EditGeneralInfo["imgFlag" + i].src = formInstance.AdminConsoleCommonUtils.checkboxSelected;
            initialFlagList.push(this.view.EditGeneralInfo["imgFlag" + i].info.Key);
          }
        }
      }
    }
    this.view.EditGeneralInfo.flxSelectFlags.info = {
      "initialFlagList": initialFlagList
    };

    // Turn of the switch if e-agreement is not required
    if (basicInfo.customer.isEAgreementRequired === "0" || basicInfo.customer.CustomerType_id === "TYPE_ID_RETAIL") {
      this.view.EditGeneralInfo.flxDetails7.setVisibility(false);
    } else {
      this.view.EditGeneralInfo.flxDetails7.setVisibility(true);
    }
    // Set e-agreement status
    if (basicInfo.customer.isEagreementSigned === "false") {
      this.view.EditGeneralInfo.switchToggle.selectedIndex = 1;
    } else {
      this.view.EditGeneralInfo.switchToggle.selectedIndex = 0;
    }
    //Turn off marital status edit if not a retail user
    if (basicInfo.customer.CustomerType_id === "TYPE_ID_RETAIL") {
      this.view.EditGeneralInfo.flxDetails3.setVisibility(true);
    } else {
      this.view.EditGeneralInfo.flxDetails3.setVisibility(false);
    }
    this.view.EditGeneralInfo.flxDetails3.info = {
      "CustomerType_id": basicInfo.customer.CustomerType_id
    };

    this.showEditGeneralInformationScreen(formInstance);
    this.view.EditGeneralInfo.flxDetails2.setVisibility(false);
    this.view.forceLayout();
  },
  setDataForGeneralInformationScreen: function (basicInfo, formInstance) {
    var self=this;
    kony.store.setItem("Customer_id", basicInfo.customer.Customer_id);
    if (basicInfo.customer.CustomerType_id) {
      kony.store.setItem("CustomerType_id", basicInfo.customer.CustomerType_id);
    }
    var hasLoans = formInstance.presenter.getCurrentCustomerType() === "TYPE_ID_PROSPECT" || formInstance.presenter.getCurrentCustomerType() === "TYPE_ID_RETAIL";
    this.view.dashboardCommonTab.btnLoans.setVisibility(hasLoans);
    this.view.dashboardCommonTab.btnDeposits.setVisibility(false);
    this.view.dashboardCommonTab.btnBanking.left = "150px";
    formInstance.view.CSRAssistToolTip.right = "130px";
    this.isAssistConsented = basicInfo.customer.IsAssistConsented;
    //Set Customer risk status
    this.view.generalInfoHeader.setRiskStatus(basicInfo.customer.CustomerFlag);

    //OLB access and enrolment
    this.view.row1.lblData2.info = { "IsOlbAllowed": basicInfo.customer.IsOlbAllowed, "IsEnrolledForOlb": basicInfo.customer.IsEnrolledForOlb };
    this.view.generalInfoHeader.flxDefaultSearchHeader.info = { "CustomerType_id": basicInfo.customer.CustomerType_id };
    //Set customer status
    var customerStatus = basicInfo.customer.OLBCustomerFlags.Status;
    this.view.generalInfoHeader.lblStatus.info = { "value": customerStatus };
    this.setLockStatus(customerStatus.toUpperCase(), formInstance);

    if (customerStatus.toUpperCase() === "LOCKED") {
      var lockedOnDate = DateTimeUtils_FormExtn.getDateInstanceFromDBDateTime(basicInfo.customer.OLBCustomerFlags.LockedOn);
      var unlockedOnDate = new Date(lockedOnDate.getTime() + (parseInt(basicInfo.configuration.value) * 60 * 1000));
      formInstance.presenter.setCurrentCustomerLockedOnInfo({
        lockedOn: AdminConsoleDateTimeUtilities.getFormattedTimeFromDateInstance(lockedOnDate, AdminConsoleDateTimeUtilities.CLIENT_DATETIME_FORMAT1),
        unlockedOn: AdminConsoleDateTimeUtilities.getFormattedTimeFromDateInstance(unlockedOnDate, AdminConsoleDateTimeUtilities.CLIENT_DATETIME_FORMAT1)
      });
      this.view.alertMessage.setGeneralInformationAlertMessage(formInstance, formInstance.presenter.getCurrentCustomerLockedOnInfo(),
                                                               formInstance.presenter.getCurrentCustomerRequestAndNotificationCount());
    }
    this.computeandSetToolTipHeight(formInstance);
    //adding the customer name into appcontext to be retrieved in the loans page
    kony.mvc.MDAApplication.getSharedInstance().appContext.searchCustomerName = basicInfo.customer.Name;
    this.view.generalInfoHeader.setCustomerNameandTag(basicInfo.customer);
    this.view.generalInfoHeader.lblCustomerName.info =
      { "FirstName": basicInfo.customer.FirstName, "MiddleName": basicInfo.customer.MiddleName, "LastName": basicInfo.customer.LastName };

    this.view.row3.btnLink1.setVisibility(false);
    this.view.row3.lblData1.setVisibility(true);

    if (basicInfo.customer.CustomerType_id === "TYPE_ID_PROSPECT") {
      //Applicant flow
      var applicantAddrArr=basicInfo.customer.Addresses?basicInfo.customer.Addresses:[];
      var finalAddress = "";
      for(var i=0;i<applicantAddrArr.length;i++){
        if(applicantAddrArr[i].isPrimary){
          if (applicantAddrArr[i].AddressLine1) {
            finalAddress += applicantAddrArr[i].AddressLine1 + ", ";
          }
          if (applicantAddrArr[i].AddressLine2) {
            finalAddress += applicantAddrArr[i].AddressLine2 + "<br>";
          }
          finalAddress += applicantAddrArr[i].CityName + ", " + applicantAddrArr[i].RegionName + ", " +
            applicantAddrArr[i].CountryName + " " + applicantAddrArr[i].ZipCode;
        }
      }
      //Fill data for applicant
      //Row 1
      this.view.row1.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerCare.lblPhoneNumber");
      this.view.row1.lblData1.text = basicInfo.customer.Customer_id ? basicInfo.customer.PrimaryPhoneNumber : "Not Available";

      this.view.row1.lblHeading2.text = kony.i18n.getLocalizedString("i18n.LeadManagement.segmentheaderEmail");
      this.view.row1.lblData2.text = basicInfo.customer.Customer_id ? basicInfo.customer.PrimaryEmailAddress : "Not Available";

      this.view.row1.lblHeading3.text = kony.i18n.getLocalizedString("i18n.View.ADDRESS");
      this.view.row1.lblData3.text = finalAddress!=="" ? finalAddress : "Not Available";
      this.view.row1.lblData3.setVisibility(true);
      this.view.row1.btnLink3.setVisibility(false);
      this.view.row2.setVisibility(false);
      this.view.row3.setVisibility(false);
      this.view.row4.setVisibility(false);
      if(basicInfo.customer.isCustomerAccessiable === "false"){
        this.view.dashboardCommonTab.btnLoans.setVisibility(false);
        this.view.dashboardCommonTab.btnDeposits.setVisibility(false);
      }else{
        this.view.dashboardCommonTab.btnLoans.setVisibility(true);
        this.view.dashboardCommonTab.btnDeposits.setVisibility(false);
      }

    } else {
      // Customer flow
      this.view.row2.setVisibility(true);
      this.view.row3.setVisibility(true);
      this.view.row4.setVisibility(true);
      this.view.dashboardCommonTab.btnLoans.setVisibility(true);
      this.view.dashboardCommonTab.btnDeposits.setVisibility(false);
      this.view.flxGeneralInfoEditButton.setVisibility(true);

      this.view.row1.lblHeading1.text = kony.i18n.getLocalizedString("i18n.permission.USERNAME");
      this.view.row1.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.MOBILE_ONLINE_BAKING_ACCESS");
      this.view.row1.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.RESET_PASSWORD");
      this.view.row1.lblData1.text = basicInfo.customer.Username ? basicInfo.customer.Username : "N/A";
      this.view.row1.btnLink3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SendLink");
      this.customerId = basicInfo.customer.Customer_id;
      if (basicInfo.customer.CustomerType_id === "TYPE_ID_RETAIL") {
        //Display Retail customer data
        this.displayRetailUserData(basicInfo, formInstance);
        formInstance.view.breadcrumbs.btnBackToMain.onClick = function () {
          var sourceFormDetails = formInstance.presenter.sourceFormNavigatedFrom();
          if(sourceFormDetails.data) {
            if(sourceFormDetails.data.leadId) {
              var leadPayload = {
                "leadId": sourceFormDetails.data.leadId,
                "statusIds": sourceFormDetails.data.statusId
              };
              formInstance.presenter.navigateToLeadDetailsScreen(leadPayload);
            }
            else {
              formInstance.presenter.navigateToLeadScreen();
            }
          }
          else {
            formInstance.presenter.displayCustomerMangementSearchForm();
          }
        };

      } else {
        // Business user profile
        this.displayBusinessUser(basicInfo, formInstance);
        formInstance.view.breadcrumbs.btnPreviousPage.onClick = function () {
          var companyPayload = { "id": self.view.row3.btnLink1.info.id };
          formInstance.presenter.navigateToCompanyDetailsScreen(companyPayload);
        };
      }
    }
    this.showGeneralInformationScreen(formInstance);

  },
  displayRetailUserData: function(basicInfo, formInstance){
    // Check if the current logged-in interuser has access to the current customer
    if(basicInfo.customer.isCustomerAccessiable === "true"){
      // Has access
      this.view.row2.setVisibility(true);
      this.view.row3.setVisibility(true);
      this.view.row4.setVisibility(true);
      this.view.row2.lblData1.text = basicInfo.customer.Gender ? basicInfo.customer.Gender : "N/A";
      this.view.row2.lblData2.text = basicInfo.customer.DateOfBirth ? formInstance.getLocaleDate(basicInfo.customer.DateOfBirth) : "N/A";
      this.view.row2.lblData3.text = basicInfo.customer.MaritalStatus_name ? basicInfo.customer.MaritalStatus_name : "N/A";
      this.view.row3.lblData1.text = basicInfo.customer.SSN ? formInstance.AdminConsoleCommonUtils.maskSSN(basicInfo.customer.SSN) : "N/A";
      this.view.row3.lblData2.text = basicInfo.customer.EmployementStatus_name ? basicInfo.customer.EmployementStatus_name : "N/A";
      this.view.row3.lblData3.text = basicInfo.customer.IsStaffMember === "true" ?
        kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.EMPLOYEE") :
      kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.NON_EMPLOYEE");
      this.view.row4.lblData1.text = basicInfo.customer.Customer_id;
      this.view.row4.lblData2.text = basicInfo.customer.CustomerSince ? formInstance.getLocaleDate(basicInfo.customer.CustomerSince) : "N/A";
      this.view.row4.lblData3.text = formInstance.AdminConsoleCommonUtils.getValidString(basicInfo.customer.Branch_code) + ", " + formInstance.AdminConsoleCommonUtils.getValidString(basicInfo.customer.Branch_name);
      this.view.row2.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.GENDER");
      this.view.row2.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DATE_OF_BIRTH");
      this.view.row2.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.MARITAL_STATUS");
      this.view.row3.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SSN_HEADER");
      this.view.row3.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.EMPLOYEMENT_STATUS");
      this.view.row3.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.EMPLOYEE_NON_EMPLOYEE");
      this.view.row4.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOMER_ID");
      this.view.row4.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOMER_SINCE");
      this.view.row4.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.BRANCH_CODE_NAME");
    }else{
      //Does not have access
      this.setCustomerBasicDataForNoAccessProfile(basicInfo, formInstance);
    }
  },
  displayBusinessUser: function(basicInfo, formInstance){
    var eagreementStatus = null;
    if (basicInfo.customer.isEAgreementRequired === "0") {
      eagreementStatus = kony.i18n.getLocalizedString("i18n.customermanagement.eSignNotRequired");
    } else if (basicInfo.customer.isEagreementSigned === "true") {
      eagreementStatus = kony.i18n.getLocalizedString("i18n.customermanagement.eSigned");
    } else if (basicInfo.customer.isEagreementSigned === "false") {
      eagreementStatus = kony.i18n.getLocalizedString("i18n.customermanagement.NotSigned");
    }

    this.setBreadCrumbsText([kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SEARCHCUSTOMERS"), 
    basicInfo.customer.organisation_name.toUpperCase(), this.view.generalInfoHeader.lblCustomerName.text.toUpperCase()], formInstance);
    this.view.row3.btnLink1.info = { "id": basicInfo.customer.organisation_id };
    
    if(basicInfo.customer.isCustomerAccessiable === "true"){
      //Has access
      this.view.row3.btnLink1.setVisibility(true);
      this.view.row3.lblData1.setVisibility(false);
      this.view.row3.btnLink1.skin = "sknBtnFont11ABEBSz13px";

      this.view.row2.lblData1.text = basicInfo.customer.DateOfBirth ? formInstance.getLocaleDate(basicInfo.customer.DateOfBirth) : "N/A";
      this.view.row2.lblData2.text = basicInfo.customer.SSN ? formInstance.AdminConsoleCommonUtils.maskSSN(basicInfo.customer.SSN) : "N/A";
      this.view.row2.lblData3.text = basicInfo.customer.IsStaffMember === "true" ?
        kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.EMPLOYEE") :
      kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.NON_EMPLOYEE");
      this.view.row3.btnLink1.text = basicInfo.customer.organisation_name ? basicInfo.customer.organisation_name : "N/A";
      this.view.row3.lblData2.text = basicInfo.customer.Customer_Role ? basicInfo.customer.Customer_Role : "N/A";
      this.view.row3.lblData3.text = formInstance.AdminConsoleCommonUtils.getValidString(basicInfo.customer.Branch_code) + ", " + formInstance.AdminConsoleCommonUtils.getValidString(basicInfo.customer.Branch_name);
      this.view.row4.lblData1.text = basicInfo.customer.Customer_id;
      this.view.row4.lblData2.text = basicInfo.customer.CustomerSince ? formInstance.getLocaleDate(basicInfo.customer.CustomerSince) : "N/A";
      this.view.row4.lblData3.text = eagreementStatus ? eagreementStatus : "N/A";
      this.view.row2.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DATE_OF_BIRTH");
      this.view.row2.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SSN_HEADER");
      this.view.row2.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.EMPLOYEE_NON_EMPLOYEE");
      this.view.row3.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Profile_COMPANY");
      this.view.row3.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Profile_ROLE");
      this.view.row3.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.BRANCH_CODE_NAME");
      this.view.row4.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOMER_ID");
      this.view.row4.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOMER_SINCE");
      this.view.row4.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Profile_Eagreement");
    }else{
      //Does not have access
      this.setCustomerBasicDataForNoAccessProfile(basicInfo, formInstance);
    }

  },
  setCustomerBasicDataForNoAccessProfile: function(basicInfo, formInstance){
     //Hide last three rows
    this.view.row2.setVisibility(false);
    this.view.row3.setVisibility(false);
    this.view.row4.setVisibility(false);
    
    //Hide Loans and Deposits tabs
    this.view.dashboardCommonTab.btnLoans.setVisibility(false);
    this.view.dashboardCommonTab.btnDeposits.setVisibility(false);
    
    this.view.row1.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.EMPLOYEE_NON_EMPLOYEE");
    this.view.row1.lblData2.setVisibility(true);
    this.view.row1.btnLink2.setVisibility(false);
    this.view.row1.lblData2.text = basicInfo.customer.IsStaffMember === "true" ?
      kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.EMPLOYEE") :
      kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.NON_EMPLOYEE");

  },
  setLockStatus: function (status, formInstance) {
    var customerType = null;
    this.view.generalInfoHeader.lblStatus.info = { "value": status };
    if (this.view.generalInfoHeader.flxDefaultSearchHeader.info && this.view.generalInfoHeader.flxDefaultSearchHeader.info.CustomerType_id) {
      customerType = this.view.generalInfoHeader.flxDefaultSearchHeader.info.CustomerType_id;
    }
    if (status === "LOCKED") {
      this.handleLockedUserLockStatus(customerType, formInstance);
    } else if (status === "SUSPENDED") {
      this.handleSuspendedUserLockStatus(customerType, formInstance);
    } else if (status === "NEW") {
      this.handleNewUserLockStatus(customerType, formInstance);
    } else {
      this.handleActiveUserLockStatus(customerType, formInstance);
    }
    this.setEnrollmentAccessandStatus(status, formInstance);
    formInstance.view.forceLayout();
  },
  setEnrollmentAccessandStatus: function (status, formInstance) {
    this.checkandHideResetPassword(status);
    if(formInstance.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "false"){
      return;
    }
    if (this.view.row1.btnLink2.info) {
      this.view.row1.btnLink2.info.emailSentCtr++;
    } else {
      this.view.row1.btnLink2.info = { "emailSentCtr": 1 };
    }
    if (!status) {
      status = this.view.generalInfoHeader.lblStatus.info.value;
    }
    this.view.row1.lblData2.text = "";

    if (status === "ACTIVE") {
      this.view.row1.lblData2.text += kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Enrolled");
      this.view.row1.lblData2.setVisibility(true);
      this.view.row1.btnLink2.setVisibility(false);
      this.view.row1.lblData2.onHover = null;

    } else if (status === "LOCKED" || status === "SUSPENDED") {

      if (this.view.row1.btnLink2.info && this.view.row1.btnLink2.info.emailSentCtr > 0
          && this.view.row1.btnLink2.info.emailSentCtr < 3) {
        this.view.row1.lblData2.text += kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.resendEnrollmentLink");
      } else {
        this.view.row1.lblData2.text += kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.sendEnrollmentLink");
      }
      this.view.row1.lblData2.setVisibility(true);
      this.view.row1.btnLink2.setVisibility(false);

      this.view.row1.lblData2.onHover = function (widget, context) {
        if (context.eventType === constants.ONHOVER_MOUSE_ENTER) {
          formInstance.view.EnrolToolTip.setVisibility(true);
          formInstance.view.forceLayout();
        } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
          formInstance.view.EnrolToolTip.setVisibility(false);
          formInstance.view.forceLayout();
        }
      };
    } else if (status === "NEW") {
      if (this.view.row1.btnLink2.info && this.view.row1.btnLink2.info.emailSentCtr > 0
          && this.view.row1.btnLink2.info.emailSentCtr < 3) {
        this.view.row1.btnLink2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.resendEnrollmentLink");
        this.view.row1.lblData2.setVisibility(false);
        this.view.row1.btnLink2.setVisibility(true);

      } else {
        this.view.row1.btnLink2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.sendEnrollmentLink");
        this.view.row1.lblData2.setVisibility(false);
        this.view.row1.btnLink2.setVisibility(true);
      }
      this.view.row1.lblData2.onHover = null;
    }
  },
  checkandHideResetPassword: function (status) {
    if (status === "NEW") {
      this.view.row1.flxColumn3.setVisibility(false);
    } else {
      this.view.row1.flxColumn3.setVisibility(true);
    }
  },
  handleLockedUserLockStatus: function (customerType, formInstance) {
    this.view.generalInfoHeader.handleLockedUserStatus(customerType, formInstance);
    this.view.row1.btnLink3.setVisibility(true);
    this.view.row1.lblData3.setVisibility(false);
    this.view.row1.lblData3.onHover = null;
  },

  handleSuspendedUserLockStatus: function (customerType, formInstance) {
    this.view.generalInfoHeader.handleSuspendedUserStatus(customerType, formInstance);
    this.view.row1.lblData3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SendLink");
    this.view.row1.lblData3.setVisibility(true);
    this.view.row1.btnLink3.setVisibility(false);
    this.view.row1.lblData3.onHover = function (widget, context) {
      if (context.eventType === constants.ONHOVER_MOUSE_ENTER) {
        formInstance.view.ResetPasswordToolTip.setVisibility(true);
        formInstance.view.forceLayout();
      } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
        formInstance.view.ResetPasswordToolTip.setVisibility(false);
        formInstance.view.forceLayout();
      }
    };
  },

  handleNewUserLockStatus: function (customerType, formInstance) {
    this.view.generalInfoHeader.handleNewUserStatus(customerType, formInstance);
    this.view.row1.lblData3.onHover = null;
  },

  handleActiveUserLockStatus: function (customerType, formInstance) {
    this.view.generalInfoHeader.handleActiveUserStatus(customerType, this.isAssistConsented, formInstance);
    this.view.row1.btnLink3.setVisibility(true);
    this.view.row1.lblData3.setVisibility(false);
    this.view.row1.lblData3.onHover = null;
  },
  fillSalutation: function () {
    var data = [];
    data.push(["lbl1", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Select_Salutation")]);
    data.push([kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Mr"), kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Mr")]);
    data.push(["Mrs.", "Mrs."]);
    data.push(["Dr.", "Dr."]);
    data.push(["Ms.", "Ms."]);
    this.view.EditGeneralInfo.lstboxDetails1.masterData = data;
    this.view.EditGeneralInfo.lstboxDetails1.selectedKey = "lbl1";
  },
  computeandSetToolTipHeight: function (formInstance) {
    var defaultEnrolEmailHeight = 210;
    var defaultResetPasswordHeight = 210;
    var heightToAdd = 0;
    if (this.view.alertMessage.isVisible) {
      heightToAdd += parseInt(this.view.alertMessage.height.slice(0, 2)) + 15;
    } else {
      heightToAdd += 15;
    }
    if (this.view.generalInfoHeader.flxRiskStatus.isVisible) {
      heightToAdd += parseInt(this.view.generalInfoHeader.flxRiskStatus.height.slice(0, 2));
    }
    if (this.view.flxTabWrapper.isVisible) {
      heightToAdd += parseInt(this.view.flxTabWrapper.height.slice(0, 2));
    }

    formInstance.view.EnrolToolTip.top = defaultEnrolEmailHeight + heightToAdd + "px";
    formInstance.view.EnrolToolTip.right = "420px";
    formInstance.view.ResetPasswordToolTip.top = defaultResetPasswordHeight + heightToAdd + "px";
  },
  showGeneralInformationScreen: function (formInstance) {
    formInstance.view.flxMainContent.setVisibility(true);
    formInstance.view.flxGeneralInformationWrapper.setVisibility(true);
    formInstance.view.flxGeneralInfoWrapper.flxEditGeneralInformation.setVisibility(false);
    formInstance.view.flxGeneralInfoWrapper.flxViewGeneralInformation.setVisibility(true);
    formInstance.view.flxOtherInfoWrapper.setVisibility(true);
    formInstance.view.btnNotes.setVisibility(true);
    var custType = formInstance.presenter.getCurrentCustomerType();
    if (custType === "TYPE_ID_PROSPECT") {
      formInstance.view.flxOtherInfoWrapper.setVisibility(false);
      this.view.flxGeneralInfoEditButton.setVisibility(false);
      this.view.generalInfoHeader.flxActionButtons.setVisibility(false);
    } else {
      formInstance.view.flxOtherInfoWrapper.setVisibility(true);
      this.view.generalInfoHeader.flxActionButtons.setVisibility(true);
      if(formInstance.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "true"){
        this.view.flxGeneralInfoEditButton.setVisibility(true);
      }else{
        this.view.flxGeneralInfoEditButton.setVisibility(false);
      }      
    }

    var sourceFormDetails = formInstance.presenter.sourceFormNavigatedFrom();
    if(sourceFormDetails.data && sourceFormDetails.data.breadcrumbValue) {
      this.setBreadCrumbsText([sourceFormDetails.data.breadcrumbValue, this.view.generalInfoHeader.lblCustomerName.text.toUpperCase()], formInstance);          
    }
    else {
      if (custType === "TYPE_ID_MICRO_BUSINESS" || custType === "TYPE_ID_SMALL_BUSINESS") {
        formInstance.view.flxBreadCrumbs.setVisibility(true);
      }
      else {
        this.setBreadCrumbsText([kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SEARCHCUSTOMERS"), this.view.generalInfoHeader.lblCustomerName.text.toUpperCase()], formInstance);
      }
    }
  },
  setBreadCrumbsText: function (textArray, formInstance) {
    if (textArray.length === 2) {
      formInstance.view.breadcrumbs.btnBackToMain.text = textArray[0];
      formInstance.view.breadcrumbs.btnPreviousPage.setVisibility(false);
      formInstance.view.breadcrumbs.fontIconBreadcrumbsRight2.setVisibility(false);
      formInstance.view.breadcrumbs.lblCurrentScreen.text = textArray[1];
    } else if (textArray.length === 3) {
      formInstance.view.breadcrumbs.btnBackToMain.text = textArray[0];
      formInstance.view.breadcrumbs.btnPreviousPage.setVisibility(true);
      formInstance.view.breadcrumbs.fontIconBreadcrumbsRight2.setVisibility(true);
      formInstance.view.breadcrumbs.btnPreviousPage.text = textArray[1];
      formInstance.view.breadcrumbs.lblCurrentScreen.text = textArray[2];
    }
    formInstance.customNavigateBack = null;
    formInstance.view.flxBreadCrumbs.setVisibility(true);
  },

  showEditGeneralInformationScreen: function (formInstance) {
    formInstance.view.flxBreadCrumbs.setVisibility(true);
    formInstance.view.flxMainContent.setVisibility(true);
    formInstance.view.flxGeneralInfoWrapper.flxEditGeneralInformation.setVisibility(true);
    formInstance.view.flxGeneralInfoWrapper.flxViewGeneralInformation.setVisibility(false);
    formInstance.view.flxOtherInfoWrapper.setVisibility(false);
  },
  setFlowActionsForGeneralInformationComponent: function (formInstance) {
    var scopeObj = this;
    formInstance.view.breadcrumbs.btnBackToMain.onClick = function () {
      formInstance.presenter.getSearchInputs();
    };
    this.view.row3.btnLink1.onClick = function () {
      var payload = { "id": scopeObj.view.row3.btnLink1.info.id };
      formInstance.presenter.navigateToCompanyDetailsScreen(payload);
    };
    this.view.flxGMHeader.onClick = function () {
      scopeObj.toggleGeneralInfoTab();
    };
    this.view.flxGeneralInfoEditButton.onClick = function () {
      var customerTypeId=formInstance.presenter.getCurrentCustomerType();
      if(customerTypeId==="TYPE_ID_SMALL_BUSINESS"||customerTypeId==="TYPE_ID_MICRO_BUSINESS"){
        var custBasicInfo=formInstance.presenter.getCurrentCustomerDetails();
        var statusKeys = custBasicInfo.CustomerFlag_ids ? custBasicInfo.CustomerFlag_ids.split(",") : custBasicInfo.CustomerFlag_ids;
        var editCustomerInputs={
          "id": custBasicInfo.Customer_id,
          "DateOfBirth": custBasicInfo.DateOfBirth,
          "DrivingLicenseNumber": custBasicInfo.DrivingLicenseNumber,
          "FirstName":custBasicInfo.FirstName,
          "LastName": custBasicInfo.LastName,
          "MiddleName":custBasicInfo.MiddleName,
          "Ssn": custBasicInfo.SSN,
          "UserName": custBasicInfo.Username,
          "role_name": custBasicInfo.Customer_Role,
          "status": custBasicInfo.CustomerStatus_id,
          "Email": custBasicInfo.PrimaryEmailAddress,
          "Phone": custBasicInfo.PrimaryPhoneNumber,
          "customerFlags":statusKeys,
          "employementStatus":custBasicInfo.EmployementStatus_id,
          "isEAgreementRequired":custBasicInfo.isEAgreementRequired,
          "isEagreementSigned":custBasicInfo.isEagreementSigned,
          "Salutation":custBasicInfo.Salutation,
          "company": {
            "UserName": custBasicInfo.Username,
            "id": custBasicInfo.organisation_id,
            "TypeId": customerTypeId
          }
        };
        formInstance.presenter.showCompaniesCustomerEdit(editCustomerInputs);
      }
      else
        formInstance.presenter.getStatusGroup();
    };
    this.view.editButtons.btnSave.onClick = function () {
      if (scopeObj.validateBasicEditInfo()) {
        var salutation = scopeObj.view.EditGeneralInfo.lstboxDetails1.selectedKeyValue[1];
        var MaritalStatusID = scopeObj.view.EditGeneralInfo.lstboxDetails3.selectedKey;
        var EmployementStatusID = scopeObj.view.EditGeneralInfo.lstboxDetails4.selectedKey;
        var eagreementStatus = scopeObj.view.EditGeneralInfo.switchToggle.selectedIndex === 1 ? "0" : "1";
        var listOfRemovedRisks = [],
            listOfAddedRisks = [],
            selectedFlags = [];

        for (var i = 1; i <= scopeObj.NumberOfFlags; i++) {
          if (scopeObj.view.EditGeneralInfo["imgFlag" + i].src === formInstance.AdminConsoleCommonUtils.checkboxSelected) {
            selectedFlags.push(scopeObj.view.EditGeneralInfo["imgFlag" + i].info.Key);
          }
        }
        var initialFlagList = scopeObj.view.EditGeneralInfo.flxSelectFlags.info.initialFlagList;

        var calculateList = function (a1, a2) {
          return a1.filter(function (x) {
            var result = false;
            if (a2.indexOf(x) < 0) result = true;
            return result;
          });
        };

        listOfAddedRisks = calculateList(selectedFlags, initialFlagList);
        listOfRemovedRisks = calculateList(initialFlagList, selectedFlags);
        var editParams = {
          "Customer_id": formInstance.presenter.getCurrentCustomerDetails().Customer_id,
          "Salutation": scopeObj.view.EditGeneralInfo.lstboxDetails1.selectedKey === "lbl1" ? null : salutation,
          "EmployementStatus_id": EmployementStatusID === "lbl1" ? null : EmployementStatusID,
          "eagreementStatus": eagreementStatus,
          "listOfRemovedRisks": listOfRemovedRisks,
          "listOfAddedRisks": listOfAddedRisks
        };
        if (scopeObj.view.EditGeneralInfo.flxDetails3.info && scopeObj.view.EditGeneralInfo.flxDetails3.info.CustomerType_id === "TYPE_ID_RETAIL") {
          editParams["MaritalStatus_id"] = MaritalStatusID;
        }
        formInstance.presenter.editCustomerBasicInfo(editParams);
      }
    };
    this.view.EditGeneralInfo.lstboxDetails1.onSelection = function () {
      scopeObj.view.EditGeneralInfo.lstboxDetails1.skin = "sknlstbxNormal0f9abd8e88aa64a";
      scopeObj.view.EditGeneralInfo.lblError1.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    this.view.EditGeneralInfo.lstboxDetails2.onSelection = function () {
      scopeObj.view.EditGeneralInfo.lstboxDetails2.skin = "sknlstbxNormal0f9abd8e88aa64a";
      scopeObj.view.EditGeneralInfo.lblError2.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    this.view.EditGeneralInfo.lstboxDetails3.onSelection = function () {
      scopeObj.view.EditGeneralInfo.lstboxDetails3.skin = "sknlstbxNormal0f9abd8e88aa64a";
      scopeObj.view.EditGeneralInfo.lblError3.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    this.view.EditGeneralInfo.lstboxDetails4.onSelection = function () {
      scopeObj.view.EditGeneralInfo.lstboxDetails4.skin = "sknlstbxNormal0f9abd8e88aa64a";
      scopeObj.view.EditGeneralInfo.lblError4.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    this.view.backToGeneralInformation.btnBack.onClick = function () {
      scopeObj.showGeneralInformationScreen(formInstance);
      scopeObj.view.forceLayout();
    };
    this.view.editButtons.btnCancel.onClick = function () {
      scopeObj.showGeneralInformationScreen(formInstance);
      scopeObj.view.forceLayout();
    };
    this.view.backToGeneralInformation.flxBack.onClick = function () {
      scopeObj.showGeneralInformationScreen(formInstance);
      scopeObj.view.forceLayout();
    };
    this.view.EditGeneralInfo.flxFlag1.onClick = function () {
      if (scopeObj.view.EditGeneralInfo.imgFlag1.src === formInstance.AdminConsoleCommonUtils.checkbox) {
        scopeObj.view.EditGeneralInfo.imgFlag1.src = formInstance.AdminConsoleCommonUtils.checkboxSelected;
      } else {
        scopeObj.view.EditGeneralInfo.imgFlag1.src = formInstance.AdminConsoleCommonUtils.checkbox;
      }
    };
    this.view.EditGeneralInfo.flxFlag2.onClick = function () {
      if (scopeObj.view.EditGeneralInfo.imgFlag2.src === formInstance.AdminConsoleCommonUtils.checkbox) {
        scopeObj.view.EditGeneralInfo.imgFlag2.src = formInstance.AdminConsoleCommonUtils.checkboxSelected;
      } else {
        scopeObj.view.EditGeneralInfo.imgFlag2.src = formInstance.AdminConsoleCommonUtils.checkbox;
      }
    };
    this.view.EditGeneralInfo.flxFlag3.onClick = function () {
      if (scopeObj.view.EditGeneralInfo.imgFlag3.src === formInstance.AdminConsoleCommonUtils.checkbox) {
        scopeObj.view.EditGeneralInfo.imgFlag3.src = formInstance.AdminConsoleCommonUtils.checkboxSelected;
      } else {
        scopeObj.view.EditGeneralInfo.imgFlag3.src = formInstance.AdminConsoleCommonUtils.checkbox;
      }
    };
    this.view.row1.btnLink2.onClick = function () {
      //set emails
      var emails = (formInstance.presenter.getCurrentCustomerContactInfo()).Emails;
      var emailLst = [];
      for (var i = 0; i < emails.length; i++) {
        if (emails[i].isPrimary === "true") {
          emailLst.push([emails[i].id, emails[i].Value + "    (Primary email)"]);
        } else {
          emailLst.push([emails[i].id, emails[i].Value]);
        }
      }
      formInstance.view.lstboxEmails.masterData = emailLst;
      //open confirm
      var confirmAction = function () {
        var email = (formInstance.view.lstboxEmails.selectedKeyValue[1]);
        email = (email.replace("(Primary email)", " ")).trim();
        formInstance.presenter.enrollACustomer({
          "Customer_id": formInstance.presenter.getCurrentCustomerDetails().Customer_id,
          "Customer_username": formInstance.presenter.getCurrentCustomerDetails().Username,
          "Customer_Email": email
        });
      };
      var cancelAction = function () { };
      formInstance.AdminConsoleCommonUtils.openEmailConfirm({
        header: kony.i18n.getLocalizedString("i18n.frmCustomers.SendEnrollHeader"),
        message: kony.i18n.getLocalizedString("i18n.frmCustomers.Selectenroll"),
        confirmAction: confirmAction,
        cancelMsg: kony.i18n.getLocalizedString("i18n.frmCustomers.CANCEL"),
        cancelAction: cancelAction,
        confirmMsg: kony.i18n.getLocalizedString("i18n.frmCustomers.Send"),

      }, formInstance);
    };

    this.view.row1.btnLink3.onClick = function () {
      var confirmAction = function () {
        formInstance.presenter.sendResetPasswordLink({
          "customerUsername": formInstance.presenter.getCurrentCustomerDetails().Username,
        });
      };
      var cancelAction = function () { };
      formInstance.AdminConsoleCommonUtils.openConfirm({
        header: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.resetPasswordConfirmationHeader"),
        message: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.resetPasswordConfirmation"),
        confirmAction: confirmAction,
        cancelMsg: kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS"),
        cancelAction: cancelAction,
        confirmMsg: 'YES, RESET',
      }, formInstance);
    };

  },
  processAndFillStatusForEdit: function (StatusGroup, formInstance) {
    if (StatusGroup.length > 0) {

      this.fillEmploymentStatus(StatusGroup.filter(function (x) {
        return x.Type_id === "STID_EMPLOYMENTSTATUS";
      }));
      this.fillCustomerFlags(StatusGroup.filter(function (x) {
        return x.Type_id === "STID_CUSTOMERFLAGS";
      }));
      this.fillMaritalStatus(StatusGroup.filter(function (x) {
        return x.Type_id === "STID_MARITALSTATUS";
      }));
      this.fillCustomerStatus(StatusGroup.filter(function (x) {
        return x.Type_id === "STID_CUSTOMERSTATUS";
      }));
      var presentationController = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CustomerManagementModule").presentationController;
      var basicInfo = presentationController.getCurrentCustomerBasicInfo();
      basicInfo.target = "EditScreen";
      this.setBasicInformation(basicInfo,formInstance);
    }
  },
  fillEmploymentStatus: function (StatusGroup) {
    var data = [];
    data.push(["lbl1", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Select_a_Status")]);
    for (var i = 0; i < StatusGroup.length; i++) {
      data.push([StatusGroup[i].id, StatusGroup[i].Description]);
    }
    this.view.EditGeneralInfo.lstboxDetails4.masterData = data;
  },
  fillCustomerFlags: function (StatusGroup) {
    for (var i = 1; i <= this.NumberOfFlags; i++) {
      this.view.EditGeneralInfo["lblFlag" + i].text = StatusGroup[i - 1].Description;
      this.view.EditGeneralInfo["imgFlag" + i].info = {
        "Key": StatusGroup[i - 1].id
      };
    }
  },
  fillMaritalStatus: function (StatusGroup) {
    var data = [];
    data.push(["lbl1", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Select_a_Status")]);
    for (var i = 0; i < StatusGroup.length; i++) {
      data.push([StatusGroup[i].id, StatusGroup[i].Description]);
    }
    this.view.EditGeneralInfo.lstboxDetails3.masterData = data;
  },
  fillCustomerStatus: function (StatusGroup) {
    var data = [];
    data.push(["lbl1", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Select_a_Status")]);
    data.push(["SID_CUS_ACTIVE", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Active")]);
    data.push(["SID_CUS_SUSPENDED", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Suspended")]);
    this.view.EditGeneralInfo.lstboxDetails2.masterData = data;
  },
  validateBasicEditInfo: function () {
    var flag = 0;
    var ErrorSkin = "sknlbxeb30173px";
    var NormalSkin = "sknlstbxNormal0f9abd8e88aa64a";
    //Valication for marital status
    if (this.view.EditGeneralInfo.flxDetails3.info && this.view.EditGeneralInfo.flxDetails3.info.CustomerType_id === "TYPE_ID_RETAIL") {
      if (this.view.EditGeneralInfo.lstboxDetails3.selectedKey === "lbl1") {
        flag += 1;
        this.view.EditGeneralInfo.lstboxDetails3.skin = ErrorSkin;
        this.view.EditGeneralInfo.lblError3.setVisibility(true);
      } else {
        this.view.EditGeneralInfo.lstboxDetails3.skin = NormalSkin;
        this.view.EditGeneralInfo.lblError3.setVisibility(false);
      }
    }
    if (flag === 0) {
      return true;
    }
    return false;
  },
  changeSelectedTabColour: function (selectedWidget) {
    var normalSkin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
    var selectedSkin = "sknBord7d9e0Rounded3px485c75";
    var tabs = [
      this.view.dashboardCommonTab.btnProfile,
      this.view.dashboardCommonTab.btnLoans,
      this.view.dashboardCommonTab.btnDeposits,
      this.view.dashboardCommonTab.btnBanking
    ];
    for (var i = 0; i < tabs.length; i++) {
      tabs[i].skin = normalSkin;
    }
    selectedWidget.skin = selectedSkin;
  },
}
});