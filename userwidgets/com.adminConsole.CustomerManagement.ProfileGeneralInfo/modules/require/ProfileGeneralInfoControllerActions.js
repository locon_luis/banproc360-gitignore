define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnProfile **/
    AS_Button_b232e3445bda4e7795fdb6e62534234c: function AS_Button_b232e3445bda4e7795fdb6e62534234c(eventobject) {
        var self = this;
        this.selectedTab(0);
    },
    /** onClick defined for btnLoans **/
    AS_Button_d00e6be0d3134fb7a3d0707efd4ec5e7: function AS_Button_d00e6be0d3134fb7a3d0707efd4ec5e7(eventobject) {
        var self = this;
        this.selectedTab(1);
    },
    /** onClick defined for btnBanking **/
    AS_Button_ie6cc81a8c23486c885f5ddc529b1939: function AS_Button_ie6cc81a8c23486c885f5ddc529b1939(eventobject) {
        var self = this;
        this.selectedTab(2);
    },
});