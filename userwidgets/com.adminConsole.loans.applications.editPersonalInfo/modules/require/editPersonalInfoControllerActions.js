define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onHover defined for flxDropDownAdmin2 **/
    AS_FlexContainer_c78c440541d84bae81d685519f067550: function AS_FlexContainer_c78c440541d84bae81d685519f067550(eventobject, context) {
        var self = this;
        var scopeObj = this;
        var isHover = context.event !== "mouseout";
        scopeObj.view.lblDobTip.setVisibility(isHover);
        scopeObj.view.lblDobError.setVisibility(false);
        scopeObj.view.flxDropDownAdmin2.skin = "sknflxffffffoptemplateop3px";
    }
});