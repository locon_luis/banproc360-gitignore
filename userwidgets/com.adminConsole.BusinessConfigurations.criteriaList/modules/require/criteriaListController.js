define(function() {
	
  return {
    mouseYCoordinate:0,
    flowActions:function(){
      this.view.segCriteria.onHover=this.saveScreenY;
      this.view.flxSelectOptions.onHover = this.onHoverEventCallback;
    },
    onHoverEventCallback:function(widget, context) {
    var scopeObj = this;
    var widGetId = widget.id;
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER||context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      scopeObj.view[widGetId].setVisibility(true);
    } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      scopeObj.view[widGetId].setVisibility(false);
    }
  },
    onClickOptions:function(){
      var selItems = this.view.segCriteria.selectedItems[0];
    var hgtValue;
    var usersindex = this.view.segCriteria.selectedIndex;
    this.usersSectionindex=usersindex[0];
    //var paginationIndex = (this.getNumPerPage()) * (this.currentPage - 1);
    this.usersRowIndex = this.view.segCriteria.selectedRowIndex[1];
    //this.usersRowIndex = this.view.segCriteria.selectedRowIndex[1] + paginationIndex;
    this.gblselIndex = this.view.segCriteria.selectedIndex[1];
    var clckd_selectedRowIndex = this.view.segCriteria.selectedRowIndex[1];
    var flexLeft = this.view.segCriteria.clonedTemplates[clckd_selectedRowIndex].flxOptions.frame.x;
    this.view.flxSelectOptions.left=parseInt((flexLeft-160),10) + "px";
    if (selItems.lblCriteriaStatus.text === kony.i18n.getLocalizedString("i18n.secureimage.Active")) {
      this.gblsegRoles = clckd_selectedRowIndex;
      hgtValue = (((clckd_selectedRowIndex + 1) * 50)+65)-this.view.segCriteria.contentOffsetMeasured.y;
      kony.print("hgtValue in roles------" + hgtValue);
      this.view.flxSelectOptions.top = this.mouseYCoordinate-148+ "px";
      this.view.lblOption1.text = kony.i18n.getLocalizedString("i18n.roles.Edit");
      this.view.lblOption3.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Delete");
      this.view.lblOption2.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate");
      this.view.fonticonDeactive.text = "\ue91c";
      this.view.fonticonDelete.text = "\ue91b";
      this.view.flxSelectOptions.frame.height=105;
      this.fixContextualMenu(this.mouseYCoordinate-148);
    } else {
      this.gblsegRoles = clckd_selectedRowIndex;
      hgtValue = (((clckd_selectedRowIndex + 1) * 50)+65)-this.view.segCriteria.contentOffsetMeasured.y;
      kony.print("hgtValue in permissions------" + hgtValue);
      this.view.flxSelectOptions.top = this.mouseYCoordinate-148 + "px";
      this.view.lblOption1.text = kony.i18n.getLocalizedString("i18n.roles.Edit");
      this.view.lblOption3.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Delete");
      this.view.lblOption2.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Activate");
      this.view.fonticonDeactive.text = "";
      this.view.flxSelectOptions.frame.height=105;
      this.fixContextualMenu(this.mouseYCoordinate-148);
    }
    kony.application.getCurrentForm().flxCriteriaStatusFilter.setVisibility(false);
    if (this.view.flxSelectOptions.isVisible===true){
      this.view.flxSelectOptions.isVisible = false;
    }
    else{
      this.view.flxSelectOptions.isVisible = true;
    }
    //this.fixContextualMenu(hgtValue);
    this.view.forceLayout();
  },
  fixContextualMenu:function(heightVal){
    if(((this.view.flxSelectOptions.frame.height+heightVal)>(this.view.segCriteria.frame.height+50))&&this.view.flxSelectOptions.frame.height<this.view.segCriteria.frame.height){
        this.view.flxSelectOptions.top=((heightVal-this.view.flxSelectOptions.frame.height)-39)+"px";
    }
    else{
        this.view.flxSelectOptions.top=(heightVal)+"px";
    }
    this.view.forceLayout();
  },
    saveScreenY:function(widget,context){
        this.mouseYCoordinate=context.screenY;
  }
  };
});