define(function() {

	return {
      
      preShow : function() {
      var scopeObj = this ;
      scopeObj.setFlowActions();
    },

    setFlowActions : function() {
      var scopeObj = this ;

      scopeObj.view.lblChannelArrow.onClick = function(eventobject) {
        var toCollapse = scopeObj.view.lblChannelArrow.text === "\ue915" ;
        scopeObj.view.lblChannelArrow.text = toCollapse ? "\ue922" : "\ue915" ;
        scopeObj.view.flxChannelDetails.setVisibility(!toCollapse);
      };
    }

	};
});