define(function() {

  function setCSRAssistStatus(status, formInstance, self) {

    self.view.flxCSRAssist.setVisibility(true);
    if (status === "true") {
      self.view.flxCSRAssist.skin = "sknflxCSRAssistBlue";
      self.view.flxCSRAssist.hoverSkin = "sknflxCSRAssistBlueHover";
      self.view.lblCSRAssist.skin = "sknlblCSRAssist";
      self.view.fonticonCSRAssist.text = "\ue93b";
      self.view.fonticonCSRAssist.skin = "sknIcon13pxWhite";
      self.view.flxCSRAssist.onClick = function() {
        formInstance.view.CSRAssist.openCSRAssistWindow(formInstance, "OLB");
      }
      self.view.flxCSRAssist.onHover = function () { };
    } else {
      self.view.lblCSRAssist.skin = "sknLblCsrAssistDisable";
      self.view.flxCSRAssist.skin = "sknflxCSRAssistGray";
      self.view.fonticonCSRAssist.text = "\ue93b";
      self.view.fonticonCSRAssist.skin = "sknIconCsrDisable";
      self.view.flxCSRAssist.hoverSkin = "sknflxCSRAssistGrayHover";
      self.view.flxCSRAssist.onClick = function () { };
      self.view.flxCSRAssist.onHover = function (widget, context) {
        if (context.eventType === constants.ONHOVER_MOUSE_ENTER) {
          kony.application.getCurrentForm().CSRAssistToolTip.setVisibility(true);
          kony.application.getCurrentForm().forceLayout();
        } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
          kony.application.getCurrentForm().CSRAssistToolTip.setVisibility(false);
          kony.application.getCurrentForm().forceLayout();
        }
      };
    }
  }

  function handleLockedUserStatus(customerType,formInstance){
    var self = this;
    self.view.fonticonActive.setVisibility(true);
    self.view.lblStatus.setVisibility(true);
    self.view.lblStatus.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Locked");
    self.view.lblStatus.skin = "sknlblCustMngLocked";
    self.view.fonticonActive.text = "";
    self.view.fonticonActive.skin = "sknfontIconLock";
    formInstance.view.flxSelectOptions.flxSuspend.setVisibility(false);
    formInstance.view.flxSelectOptions.isVisible  = false;
    if ((customerType && customerType === "TYPE_ID_RETAIL") || formInstance.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "false") {
      setCSRAssistStatus("false",formInstance, self);
    } else {
      self.view.flxCSRAssist.setVisibility(false);
      hideUpgradeOption(formInstance);
    }
    if(formInstance.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "false"){
      hideUpgradeOption(formInstance);
    }
  }

  function handleSuspendedUserStatus(customerType,formInstance){
    var self = this;
    self.view.fonticonActive.setVisibility(true);
    self.view.lblStatus.setVisibility(true);
    self.view.lblStatus.text = kony.i18n.getLocalizedString("i18n.frmCustomers.Suspended");
    self.view.lblStatus.skin = "sknlblLatoeab55d12px";
    self.view.fonticonActive.text = "";
    self.view.fonticonActive.skin = "sknFontIconSuspend";
    formInstance.view.flxSelectOptions.flxSuspend.setVisibility(true);
    formInstance.view.flxSelectOptions.lblOption1.text = kony.i18n.getLocalizedString("i18n.permission.Activate");
    formInstance.view.flxSelectOptions.fonticonEdit.text = "";
    enableUpgradeOption(formInstance);
    formInstance.view.flxSelectOptions.isVisible  = false;
    if ((customerType && customerType === "TYPE_ID_RETAIL") || formInstance.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "false") {
      setCSRAssistStatus("false",formInstance, self);
    } else {
      self.view.flxCSRAssist.setVisibility(false);
      hideUpgradeOption(formInstance);
    }
    if(formInstance.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "false"){
      hideUpgradeOption(formInstance);
    }
  }

  function handleNewUserStatus(customerType,formInstance){
    var self = this;
    self.view.fonticonActive.setVisibility(true);
    self.view.lblStatus.setVisibility(true);
    self.view.lblStatus.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.New");
    self.view.lblStatus.skin = "sknlblLato5bc06cBold14px";
    self.view.fonticonActive.text = "";
    self.view.fonticonActive.skin = "sknFontIconActivate";
    formInstance.view.flxSelectOptions.flxSuspend.setVisibility(true);
    formInstance.view.flxSelectOptions.lblOption1.text = kony.i18n.getLocalizedString("i18n.permission.Suspend");
     formInstance.view.flxSelectOptions.fonticonEdit.text = "";
    formInstance.view.flxSelectOptions.isVisible  = false;
    enableUpgradeOption(formInstance);
    if ((customerType && customerType === "TYPE_ID_RETAIL") || formInstance.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "false") {
      setCSRAssistStatus("false",formInstance, self);
    } else {
      self.view.flxCSRAssist.setVisibility(false);
      hideUpgradeOption(formInstance);
    } 
    if(formInstance.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "false"){
      hideUpgradeOption(formInstance);
    }
  }
  function handleActiveUserStatus(customerType, isAssistConsented,formInstance){
    var self = this;
    self.view.fonticonActive.setVisibility(true);
    self.view.lblStatus.setVisibility(true);
    self.view.lblStatus.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Active");
    self.view.lblStatus.skin = "sknlblLato5bc06cBold14px";
    self.view.fonticonActive.text = "";
    self.view.fonticonActive.skin = "sknFontIconActivate";
    formInstance.view.flxSelectOptions.flxSuspend.setVisibility(true);
    formInstance.view.flxSelectOptions.isVisible  = false;
    formInstance.view.flxSelectOptions.lblOption1.text = kony.i18n.getLocalizedString("i18n.permission.Suspend");
    formInstance.view.flxSelectOptions.fonticonEdit.text = "";
    enableUpgradeOption(formInstance);
    if (customerType && customerType === "TYPE_ID_RETAIL") {
      if(formInstance.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "false"){
        //Disable CSR assist and upgrade option if the user does not have access to the customer
        setCSRAssistStatus("false",formInstance, self);
        hideUpgradeOption(formInstance);
      }else{
        setCSRAssistStatus(isAssistConsented,formInstance, self);
      }
      
    } else {
      self.view.flxCSRAssist.setVisibility(false);
      hideUpgradeOption(formInstance);
    }
  }

  function enableUpgradeOption(formInstance){
    formInstance.view.flxSelectOptions.flxUpgrade.setVisibility(true);
    formInstance.view.flxSelectOptions.width="220px";
  }

  function hideUpgradeOption(formInstance){
    formInstance.view.flxSelectOptions.width="180px";
    formInstance.view.flxSelectOptions.flxUpgrade.setVisibility(false);
  }
  function setCustomerNameandTag(customer) {
    this.view.lblCustomerName.text = customer.Name.trim().length!==0?customer.Name:"Not Available";
    this.view.flxRetailTag.setVisibility(false);
    this.view.flxSmallBusinessTag.setVisibility(false);
    this.view.flxMicroBusinessTag.setVisibility(false);
    this.view.flxApplicantTag.setVisibility(false);

    if (customer.CustomerType_id === "TYPE_ID_MICRO_BUSINESS") {
      this.view.flxMicroBusinessTag.setVisibility(true);
    } else if (customer.CustomerType_id === "TYPE_ID_SMALL_BUSINESS") {
      this.view.flxSmallBusinessTag.setVisibility(true);
    } else if (customer.CustomerType_id === "TYPE_ID_PROSPECT") {
      this.view.flxApplicantTag.setVisibility(true);
    }else {
      this.view.flxRetailTag.setVisibility(true);
    }
  }

  function setRiskStatus(CustomerFlag){
    this.view.fonticonDefaulter.setVisibility(false);
    this.view.lblDefaulter.setVisibility(false);
    this.view.fonticonFraud.setVisibility(false);
    this.view.lblFraud.setVisibility(false);
    this.view.fonticonRisk.setVisibility(false);
    this.view.lblRisk.setVisibility(false);

    if (CustomerFlag) {
      this.view.flxRiskStatus.setVisibility(true);
      var customerFlags = CustomerFlag.split(",");
      for (var i = 0; i < customerFlags.length; i++) {
        if (customerFlags[i].trim().toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Defaulter").toUpperCase()) {
          this.view.fonticonDefaulter.setVisibility(true);
          this.view.lblDefaulter.setVisibility(true);
        }
        if (customerFlags[i].trim().toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Fraud_Detected").toUpperCase()) {
          this.view.fonticonFraud.setVisibility(true);
          this.view.lblFraud.setVisibility(true);
        }
        if (customerFlags[i].trim().toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.High_Risk").toUpperCase()) {
          this.view.fonticonRisk.setVisibility(true);
          this.view.lblRisk.setVisibility(true);
        }
      }
    } else {
      this.view.flxRiskStatus.setVisibility(false);
    }
  }

  function setDefaultHeaderData(formInstance){
    formInstance.view.CSRAssist.setVisibility(false);
    formInstance.view.CSRAssistToolTip.setVisibility(false);
    formInstance.view.CSRAssistToolTip.zIndex = 100;
    formInstance.view.flxSelectOptions.isVisible  = false;
    this.view.flxCSRAssist.setVisibility(true);
    initializeFlowActions(formInstance, this);
  }

  function initializeFlowActions(formInstance, scopeObj){

    formInstance.view.flxSelectOptions.flxSuspend.onClick = function () {
      var locklbl = formInstance.view.flxSelectOptions.lblOption1.text;
      var currentStatus = locklbl.toUpperCase();

      var confirmAction = function () {
        formInstance.presenter.updateDBPUserStatus({
          "customerUsername": formInstance.presenter.getCurrentCustomerDetails().Username,
          "status": currentStatus === "ACTIVATE" ? "ACTIVE" : "SUSPENDED"
        });
      };
      var cancelAction = function () { };
      var message = "", message2 = "";
      if (locklbl.toLowerCase() === "activate") {
        message = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Are_you_sure_to_Activate_the_customer");
        message2 = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Customer_can_perform");
      } else if (locklbl.toLowerCase() === "suspend") {
        message = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Are_you_sure_to_Suspend_the_customer");
        message2 = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Customer_can_not_perform");
      }

      formInstance.AdminConsoleCommonUtils.openConfirm({
        header: locklbl + ' Customer',
        message: message + ' "' + scopeObj.view.lblCustomerName.text + '"?<br><br>' + message2,
        confirmAction: confirmAction,
        cancelMsg: kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS"),
        cancelAction: cancelAction,
        confirmMsg: 'YES, ' + (locklbl.toUpperCase()),

      }, formInstance);
    }
    formInstance.view.flxSelectOptions.onHover = function (widget, context) {
      if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
       formInstance.view.flxSelectOptions.isVisible  = false;
      }
    }
    formInstance.view.flxSelectOptions.flxUpgrade.onClick = function () {
      formInstance.view.flxSelectOptions.isVisible  = false;
      formInstance.presenter.showUpgrdageUserScreen(formInstance.view.breadcrumbs.lblCurrentScreen.text);
    }
    scopeObj.view.flxOptions.onClick = function () {
      if(formInstance.view.flxSelectOptions.isVisible) {
        formInstance.view.flxSelectOptions.isVisible  = false;
      }
      else  {
        formInstance.view.flxSelectOptions.isVisible  = true;
      }
    }
  }

	return {
    setDefaultHeaderData: setDefaultHeaderData,
    handleLockedUserStatus: handleLockedUserStatus,
    handleSuspendedUserStatus: handleSuspendedUserStatus,
    handleNewUserStatus: handleNewUserStatus,
    handleActiveUserStatus: handleActiveUserStatus,
    setCustomerNameandTag: setCustomerNameandTag,    
    setRiskStatus: setRiskStatus
	};
});