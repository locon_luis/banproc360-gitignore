define({
  currentPage:1,
  recordsSize:20,
  gblsegRoles:0,
  roleId:"0",
  mouseYCoordinate:0,
  roleDetails:{},
  selectedArrowArray : [],
  rolesPreshow : function(){
    kony.adminConsole.utils.showProgressBar(this.view);
    this.skipTop = 0;
    this.skipBottom=0;
    this.end =0;
    this.endTop=0;
    this.view.flxMain.height=kony.os.deviceInfo().screenHeight+"px";
    var roleModule=kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("RoleModule");
    this.view.mainHeader.lblUserName.text=kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
    this.view.popUp.btnPopUpCancel.setVisibility(false);
    this.view.popUp.btnPopUpDelete.text=kony.i18n.getLocalizedString("i18n.SecurityQuestions.OK");
    this.view.popUp.lblPopUpMainMessage.text=kony.i18n.getLocalizedString("i18n.frmRolesController.Cannot_Create_Role_Details");
    this.view.popUp.rtxPopUpDisclaimer.text=kony.i18n.getLocalizedString("i18n.frmRolesController.select_at_least_one_permission");
    this.view.popUp.btnPopUpDelete.skin="slButtonGlossBlue0b7ec2606d7d543";
    this.view.popUp.btnPopUpDelete.focusSkin="slButtonGlossBlue0b7ec2606d7d543";
    this.view.popUp.btnPopUpDelete.hoverSkin="slButtonGlossBlue0b7ec2606d7d543";
    this.view.popUp.flxPopUpTopColor.skin="sknFlxee6565Op100NoBorder";
    this.tabUtilLabelFunction([this.view.lblTabName1,
                               this.view.lblTabName2],this.view.lblTabName1);
    this.selectedArrowArray=[this.view.fontIconImgSelected1,this.view.fontIconImgSelected3,this.view.lblIconCustomerAccessSelected,
                            this.view.lblIconSysPermisionsSelected];
    this.view.lblRoleDescriptionSize.setVisibility(false);
    this.view.lblRoleNameSize.setVisibility(false);
    this.view.rtxAvailabletxt.setVisibility(false);
    this.view.btnAddAll.setVisibility(false);
    this.view.btnRemoveAll.setVisibility(false);
    this.view.flxRoleStatusFilter.setVisibility(false);
    this.setFlowActions();
    this.showRoles();
    //this.view.flxPermissions.setVisibility(false);
    this.view.flxRolesBreadCrumb.setVisibility(false);
    this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.permission.ROLES");
    this.view.breadcrumbs.btnBackToMain.setVisibility(true);
    this.view.breadcrumbs.fontIconBreadcrumbsRight.setVisibility(false);
    this.view.breadcrumbs.btnPreviousPage.setVisibility(false);
    this.view.breadcrumbs.fontIconBreadcrumbsRight2.setVisibility(false);
    this.view.breadcrumbs.lblCurrentScreen.setVisibility(false);
    this.view.flxToastMessage.setVisibility(false);
    //this.setSelectedOptionsSegmentData();
    //this.setRolesSegmentData();
      var screenHeight = kony.os.deviceInfo().screenHeight;
    this.view.flxViewPermissions.height = screenHeight-170 +"px";
    this.setHeaderText();
    this.clearRoleDefaults();

  },
  callBackTimer:function()
  {

  },

  shouldUpdateUI: function (viewModel) {
    return viewModel !== undefined && viewModel !== null;
  },
  clearRoleDefaults: function() {
    this.view.tbxRoleNameValue.skin="skntbxLato35475f14px";
    this.view.flxNoRoleNameError.setVisibility(false);
    this.view.flxRolesBreadCrumb.setVisibility(false);
    this.currentPage = 1;
    this.view.subHeader.tbxSearchBox.text = "";
    this.view.subHeader.flxClearSearchImage.setVisibility(false);
    this.view.mainHeader.lblHeading.text=kony.i18n.getLocalizedString("i18n.leftmenu.Roles");
    //this.view.lbxPagination.selectedKey = this.currentPage;
  },
  willUpdateUI: function (roleModel) {
    this.updateLeftMenu(roleModel);
    if (roleModel.context  === "viewRoles"||roleModel.context  === kony.i18n.getLocalizedString("i18n.frmRolesController.update")||roleModel.context  === "create") {
      this.clearRoleDefaults();
      this.sortBy = this.getObjectSorter('role_Name');
      this.determineSortFontIcon(this.sortBy,"role_Name",this.view.fontIconSortName);
      this.resetSortFontIcons();
      this.loadPageData = function(){
        this.showRoles();
        this.view.flxSegmentPermmissions.setVisibility(true);
        this.view.flxRolesHeader.setVisibility(true);
        this.setRolesSegmentData(roleModel.fetchRoleList.filter(this.searchFilter).sort(this.sortBy.sortData));
        //           if(this.nextPageDisabled){
        //               this.view.flxNext.hoverSkin ="sknDisableCursor";
        //               this.view.fontIconImgNext.skin="sknFontIconPrevNextDisable";
        //             }else{
        //               this.view.flxNext.hoverSkin ="sknCursor";
        //               this.view.fontIconImgNext.skin= "sknFontIconPrevNextPage";
        //             }
        //             if(this.prevPageDisabled){
        //               this.view.flxPrevious.hoverSkin ="sknDisableCursor";
        //               this.view.fontIconImgPrevious.skin="sknFontIconPrevNextDisable";
        //             }else{
        //               this.view.flxPrevious.hoverSkin ="sknCursor";
        //               this.view.fontIconImgPrevious.skin="sknFontIconPrevNextPage";
        //             } 
        kony.adminConsole.utils.hideProgressBar(this.view);
        if((this.fromEditRoles === true && roleModel.status === true) || (this.fromCreateRole === true && roleModel.status === true))
          this.showSuccessMessage();
        else if((this.fromEditRoles === true && roleModel.status === false) || (this.fromCreateRole === true && roleModel.status === false))
          this.showErrorMessage();
      };
      this.loadPageData();
    }
    else if (roleModel.context === "updateRole" || roleModel.context === "createRole") {
      this.allPermissionsData = this.parseAllPermissionsData(roleModel.fetchRoleUpdates.fetchActivePermissions);
      this.orgRolePermissions = this.parseRolePermissionsData(roleModel.fetchRoleUpdates.fetchRolePermissions);
      this.orgRoleUsers = this.parseRoleUsersData(roleModel.fetchRoleUpdates.fetchRoleUsers);
      this.allUsersData = this.parseAllUserData(roleModel.fetchRoleUpdates.fetchActiveUsers);
      this.rolePermissions = this.parseRolePermissionsData(roleModel.fetchRoleUpdates.fetchRolePermissions);
      this.roleUsers = this.parseRoleUsersData(roleModel.fetchRoleUpdates.fetchRoleUsers);
      this.view.addCustomerRolesOption.segAddOptions.info = {"orgCustomerRoles":this.parseAllCustomerRoles(roleModel.fetchRoleUpdates.fetchAllCustomerRoles),
                                                                  "segData" :this.parseAllCustomerRoles(roleModel.fetchRoleUpdates.fetchAllCustomerRoles) };
      this.view.addCustomerRolesOption.segSelectedOptions.info = {"orgCustomerRoles":this.parseCustomerRolesForUserRole(roleModel.fetchRoleUpdates.fetchCustomerRoleForUserRole),
                                                                  "segData" :this.parseCustomerRolesForUserRole(roleModel.fetchRoleUpdates.fetchCustomerRoleForUserRole) };
      kony.adminConsole.utils.hideProgressBar(this.view);

      this.view.breadcrumbs.fontIconBreadcrumbsRight.setVisibility(true);
      if(roleModel.context === "updateRole"){
        for(var i=0;i<roleModel.fetchRoleList.length;i++){
          if(roleModel.fetchRoleList[i].role_id===this.roleId)
            this.view.breadcrumbs.lblCurrentScreen.text = roleModel.fetchRoleList[i].role_Name.toUpperCase();
        }
      }
      else {
        this.view.breadcrumbs.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.frmPermissionsController.CREATE");
        this.view.mainHeader.lblHeading.text=kony.i18n.getLocalizedString("i18n.frmRolesController.Add_Roles");
      }
      this.view.breadcrumbs.lblCurrentScreen.setVisibility(true);
      if(this.addNewRolePath !==true){
        this.addNewRolePath = false;
        this.showAddNewRoles();
      }
      if(this.directUsers === true){
        this.view.mainHeader.lblHeading.text =kony.i18n.getLocalizedString("i18n.frmRolesController.Edit_Roles");
        this.directUsers = false;
        this.addNewRolePath = false;
        this.showAddUsers();
      }
      if(this.directPermissions === true){
        this.view.mainHeader.lblHeading.text =kony.i18n.getLocalizedString("i18n.frmRolesController.Edit_Roles");
        //this.showAddPermissions();
        this.showAddCustomerRoles();
        this.addNewRolePath = false;
        this.directPermissions = false;
      }

    }
    else if(roleModel.context === "fetchRoleDetails"){
      this.roleDetailsObj = roleModel.fetchRoleDetails ;
      this.view.flxRolesBreadCrumb.setVisibility(true);
      this.view.mainHeader.lblHeading.text=kony.i18n.getLocalizedString("i18n.frmRolesController.View_Roles");
      var segIndex = this.view.segPermissions.selectedRowIndex[1];
      var segData = this.view.segPermissions.data[segIndex];
      this.view.breadcrumbs.lblCurrentScreen.text=this.roleDetailsObj.roleDetails.roleName.toUpperCase();
      this.view.breadcrumbs.lblCurrentScreen.setVisibility(true);
      this.view.breadcrumbs.fontIconBreadcrumbsRight.setVisibility(true);
      this.view.lblViewValue1.text = this.roleDetailsObj.roleDetails.roleName ;
      this.view.rtxViewDescription.text = this.roleDetailsObj.roleDetails.roleDesc ;
      this.view.fontIconViewValue2.skin = segData.fontIconStatusImg.skin;
      if(this.roleDetailsObj.roleDetails.roleStatus === "SID_ACTIVE"){
        this.view.lblViewValue2.text =  kony.i18n.getLocalizedString("i18n.secureimage.Active") ;
        this.view.lblViewValue2.skin = "sknlblLato5bc06cBold14px" ;
      }else{
        this.view.lblViewValue2.text =  kony.i18n.getLocalizedString("i18n.secureimage.Inactive") ;
        this.view.lblViewValue2.skin = "sknlblLatoDeactive" ;
      }
      this.showViewPermissionSegmentAndHeader();
      kony.adminConsole.utils.hideProgressBar(this.view);
    } else if(roleModel.context === "fetchCompositePermissions"){
      this.setDataToCSRAssistSegment(roleModel.fetchCompositePermissions.CompositePermissions)
    }else if(roleModel.toast){
      kony.adminConsole.utils.hideProgressBar(this.view);
      if(roleModel.toast === "Success"){
        this.view.toastMessage.showToastMessage(roleModel.message,this);
      }else{
        this.view.toastMessage.showErrorToastMessage (roleModel.message,this);
      }
    }
  },
  parseRoleUsersData : function(roleUsers){
    var self=this;
    var data = roleUsers.map(function(roleUsers) {
      var fullname = roleUsers.FirstName+" "+(roleUsers.MiddleName===null ? "" : roleUsers.MiddleName) + " " + roleUsers.LastName ;
      return{
        "fontIconClose":{"text":"","tooltip":kony.i18n.getLocalizedString("i18n.frmRolesController.remove_user")}, 
        "lblOption": fullname ,
        "userId": roleUsers.User_id,
        "sourceData": {
          "btnAdd": kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add"),
          "lblFullName": fullname,
          "lblUserIdValue":{
            "text" : roleUsers.User_id,
            "isVisible" : false
          },
          "lblUsername": roleUsers.Username,
          "template":"flxAddUsers",
          "userId":roleUsers.User_id},
        "flxClose":{"isVisible":false,"onClick":self.unSelectedOption},
        "flxAddOptionWrapper":{
          "onHover":self.onHoverEventCallback

        }
      };

    });
    return data;
  },
  parseAllPermissionsData: function(rolePermissions){
    var self = this;
    var data = rolePermissions.map(function(rolePermissions) {
      return{
        "btnAdd": {
          "text":kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add"),
          "onClick":self.addPermissionstoRole
        },
        "lblPermissionsName": rolePermissions.Permission_Name,
        "rtxPermissionDescription": rolePermissions.Permission_Desc,
        "template":"flxAddPermissions",
        "permissionId":rolePermissions.Permission_id
      };
    });
    return data;
  },
  parseRolePermissionsData : function(rolePermissions) {
    var self=this;
    var data = rolePermissions.map(function(rolePermissions) {
      return{
        "fontIconClose":{"text":"","tooltip":kony.i18n.getLocalizedString("i18n.frmRolesController.remove_permission")}, 
        "lblOption": rolePermissions.Permission_Name,
        "permissionId": rolePermissions.Permission_id,
        "sourceData": {
          "btnAdd": {
            "text":kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add"),
            "onClick":self.addPermissionstoRole
          },
          "lblPermissionsName": rolePermissions.Permission_Name,
          "rtxPermissionDescription": rolePermissions.Permission_Description,
          "template":"flxAddPermissions",
          "permissionId":rolePermissions.Permission_id
        },
        "flxClose":{"isVisible":false,"onClick":function(){self.showRemovePermissionPopup(1);}},
        "flxAddOptionWrapper":{
          "onHover":self.onHoverEventCallback

        }

      };
    });
    return data;
  },
  parseAllUserData : function(userData) {
    var data = userData.map(function(userData) {
      var fullname = userData.FirstName+" "+(userData.MiddleName===null ? "": userData.MiddleName) + " " + userData.LastName;
      return {
        "btnAdd": kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add"),
        "lblFullName": fullname,
        "lblUserIdValue":{
          "text" : userData.UserID,
          "isVisible" : false
        },
        "lblUsername": userData.Username,
        "template":"flxAddUsers",
        "userId":userData.UserID
      };
    });
    return data;
  },
  parseCustomerRolesForUserRole: function(data){
    var self = this;
    var segData = data.map(function(record){
      return{
         "fontIconClose":{"text":"","tooltip":kony.i18n.getLocalizedString("i18n.frmRolesController.remove_permission")}, 
         "lblOption": {"text": self.AdminConsoleCommonUtils.getTruncatedString(record.CustomerRole_Name, 35, 30),
                       "tooltip":record.CustomerRole_Name,
                       },
         "flxClose":{"isVisible":false,"onClick":function(){self.showRemoveCustomerRolePopup(1);}
                    },
         "customerRoleId":record.CustomerRole_id,
         "customerRoleName":record.CustomerRole_Name,
         "userRoleId":record.InternalRole_id,
         "customerRoleDescription":record.CustomerRole_Description,
         "flxAddOptionWrapper":{
         "onHover":self.showCloseOnHoverCallback
        },
      };
    }); 
    return segData;
  },
  parseAllCustomerRoles : function(data){
    var self =this;
    var segData = data.map(function(record){
      return {
       "btnAdd": {
            "text":kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add"),
            "onClick": self.addSelectedCustomerRole
          },
          "lblPermissionsName": record.Name,
          "rtxPermissionDescription": record.Description,
          "template":"flxAddPermissions",
          "customerRoleDescription":record.Description,
          "customerRoleId":record.id,
          "customerRoleName":record.Name,
          "userRoleId":"",
      };
    });
    return segData;
  },
  //   gotoPage : function(){
  //     this.currentPage = this.view.lbxPagination.selectedKey;
  //     this.loadPageData();
  //     this.view.lbxPagination.selectedKey = this.currentPage;
  //   },
  //   nextPage: function () {
  //     if (this.nextPageDisabled) {
  //       return;
  //     }
  //     this.currentPage++;
  //     this.view.lbxPagination.selectedKey = this.currentPage;
  //     this.loadPageData();
  //   },
  //   prevPage: function () {
  //     if (this.prevPageDisabled) {
  //       return;
  //     }
  //     this.currentPage--;
  //     this.view.lbxPagination.selectedKey = this.currentPage;
  //     this.loadPageData();
  //   },
  searchData:function(){
    this.loadPageData();
  },
  searchFilter: function (Role) {
    var searchText = this.view.subHeader.tbxSearchBox.text;
    if(typeof searchText === 'string' && searchText.length >0){
      return Role.role_Name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
    }else{
      return true;
    }
  },

  rolesPostShow : function(){
    this.namesFlag = true;
    this.userCountFlag = true;
    this.permissionFlag = true;
    this.statusFlag = true;
  },
  //scrollHeightSetting
  setScrollHeight :function(opt){
    var screenHeight = kony.os.deviceInfo().screenHeight;
    var scrollHeight;
    if(opt===1){
      //scrollHeight= screenHeight-this.view.flxMainHeader.height;
      scrollHeight= screenHeight-106;
    }
    else{
      //scrollHeight= screenHeight-this.view.flxMainHeader.height-this.view.flxMainSubHeader.height;
      scrollHeight= screenHeight-106-63;    
    }
    this.view.flxScrollMainContent.height=scrollHeight+"px";
    this.view.flxAddMainContainer.height = scrollHeight -70 + "px";
    this.view.flxSegRoles.height = screenHeight - 250 + "px";
  },


  //hide functions
  hideAll : function(){
    this.view.flxRoleStatusFilter.setVisibility(false);
    this.view.flxViews.setVisibility(false);
    this.view.flxPermissions.setVisibility(false);
    this.view.flxRolesBreadCrumb.setVisibility(false);
    this.view.flxDeactivatePermission.setVisibility(false);
    this.view.flxSelectOptions.setVisibility(false);
    this.hideViews();
  },
  hideViews : function(){
    this.view.fontIconImgViewDescription.text="";
    this.view.fontIconImgViewDescription.skin = "sknfontIconDescRightArrow14px";
    this.view.rtxViewDescription.setVisibility(false);
    this.view.flxAddMainContainer.setVisibility(false);
    this.view.flxViewPermissions.setVisibility(false);
    this.hideOptions();
  },
  hideOptions : function(){
    this.view.flxAddPermissionDetails.setVisibility(false);
    this.view.flxAddRoleDetails.setVisibility(false);
    this.view.flxAddOptionsContainer.setVisibility(false);
    this.view.flxAddCustomerRoles.setVisibility(false);
  },
  hideMainHeaderButtons : function(){
    this.view.mainHeader.btnAddNewOption.setVisibility(false);
    this.view.mainHeader.btnDropdownList.setVisibility(false);
  },
  hideMainSubHeader : function(){
    this.view.flxMainSubHeader.setVisibility(false);
    this.setScrollHeight(1);
  },
  hideAllOptionsButtonImages : function(){
    this.view.fontIconImgSelected1.setVisibility(false);
    this.view.fontIconImgSelected2.setVisibility(false);
    this.view.fontIconImgSelected3.setVisibility(false);
    this.view.fontIconImgSelected4.setVisibility(false);
  },
  togglePermissionSuboptions: function(isTrue){
    if(isTrue){  // for permissions tab
      this.view.flxAddPermissions.height = "105dp";
      this.view.lblIconDropPermissions.text = "\ue920";
      this.view.lblIconDropPermissions.skin = "sknIcon12pxBlack";
      this.view.flxSubPermissions.setVisibility(true);
    }else{ // for users tab
      this.view.flxAddPermissions.height = "45dp";
      this.view.lblIconDropPermissions.text = "\ue906";
      this.view.lblIconDropPermissions.skin = "sknicon15pxBlack";
      this.view.flxSubPermissions.setVisibility(false);
    }
  },
  //show functions
  setHeaderText : function(){
    this.view.mainHeader.lblHeading.text=kony.i18n.getLocalizedString("i18n.leftmenu.Roles");
    this.view.mainHeader.btnAddNewOption.text=kony.i18n.getLocalizedString("i18n.roles.ADDNEWROLES");
  },
  toggleRtxVisibility : function(){
    if(this.view.rtxViewDescription.isVisible===true){
      this.view.fontIconImgViewDescription.text = "";//font-icon value for desc arrow
      this.view.fontIconImgViewDescription.skin = "sknfontIconDescRightArrow14px"; 
      this.view.rtxViewDescription.setVisibility(false);
    }
    else{
      this.view.fontIconImgViewDescription.text ="";//font-icon value for down arrow
      this.view.fontIconImgViewDescription.skin = "sknfontIconDescDownArrow12px";    
      this.view.rtxViewDescription.setVisibility(true);   
    }
  },
  showMainHeaderButtons : function(){
    this.view.mainHeader.btnAddNewOption.setVisibility(true);
    this.view.mainHeader.btnDropdownList.setVisibility(true);
    this.view.mainHeader.btnDropdownList.text = kony.i18n.getLocalizedString("i18n.mainHeader.DOWNLOADLIST");
  },
  showRoles: function(){
    this.hideAll();
    this.setScrollHeight(2);
    this.view.tbxRoleNameValue.skin="skntbxLato35475f14px";
    this.view.txtRoleDescription.skin = "skntxtAreaLato35475f14Px";
    this.view.flxNoRoleNameError.setVisibility(false);
    this.view.flxNoRoleDescriptionError.setVisibility(false);
    this.view.flxPermissions.setVisibility(true);
    this.view.flxMainSubHeader.setVisibility(true);
    this.view.flxRolesBreadCrumb.setVisibility(true);
    this.view.mainHeader.lblHeading.text=kony.i18n.getLocalizedString("i18n.leftmenu.Roles");
    this.showMainHeaderButtons();
  },
  toggleSegmentMenuPopup : function(){
    var popMenu=this.view.flxSelectOptions;
    if(popMenu.visibility===false){
      popMenu.setVisibility(true);
    }
    else{
      popMenu.setVisibility(false);
    }
  },
  toggleDeactivatePopup : function(){
    if(this.view.flxDeactivatePermission.visibility===false){
      this.view.flxDeactivatePermission.setVisibility(true);
    }
    else{
      this.view.flxDeactivatePermission.setVisibility(false);
    }
  },
  showAddNewPermissions : function(){
    this.hideAll();
    this.hideOptions();
    this.hideMainHeaderButtons();
    this.hideMainSubHeader();
    this.tabUtilVerticleArrowVisibilityFunction(this.selectedArrowArray,this.view.fontIconImgSelected1);
    
    this.view.flxViews.setVisibility(true);
    this.view.flxAddMainContainer.setVisibility(true);
    this.view.flxAddPermissionDetails.setVisibility(true);
    this.view.flxRolesBreadCrumb.setVisibility(true);
    this.view.lblAddOptionsHeading.text=kony.i18n.getLocalizedString("i18n.frmRolesController.DETAILS");


  },
  navigateToAddNewRoleForm : function(){
    this.addNewRolePath=true;
    this.hideAll();
    this.hideOptions();
    this.hideMainHeaderButtons();
    this.hideMainSubHeader();
    this.togglePermissionSuboptions(false);
    this.view.flxRolesBreadCrumb.setVisibility(true);
    this.view.breadcrumbs.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.frmPermissionsController.CREATE");
    this.view.mainHeader.lblHeading.text=kony.i18n.getLocalizedString("i18n.frmRolesController.Add_Roles");
    this.view.breadcrumbs.fontIconBreadcrumbsRight.setVisibility(true);
    this.view.breadcrumbs.lblCurrentScreen.setVisibility(true);
    this.view.breadcrumbs.btnPreviousPage.setVisibility(false);
    this.view.breadcrumbs.fontIconBreadcrumbsRight2.setVisibility(false);

    this.tabUtilVerticleArrowVisibilityFunction(this.selectedArrowArray,this.view.fontIconImgSelected1);
    this.view.flxViews.setVisibility(true);
    this.view.flxAddMainContainer.setVisibility(true);
    this.view.tbxRoleNameValue.text="";
    this.view.tbxRoleNameValue.skin="skntbxLato35475f14px";
    this.view.flxNoRoleNameError.setVisibility(false);
    this.view.flxNoRoleDescriptionError.isVisible = false;
    this.view.txtRoleDescription.skin = "skntxtAreaLato35475f14Px"; 
    this.view.switchStatus.selectedIndex=0;	
    this.view.txtRoleDescription.text=""; 
    this.view.flxAddRoleDetails.setVisibility(true);
    this.view.flxValidity.setVisibility(false);
    var that=this;
    this.view.lblAddOptionsHeading.text=kony.i18n.getLocalizedString("i18n.frmRolesController.DETAILS");
    var widgetArray = [this.view.btnAddPermissions,this.view.btnAddUsers,this.view.btnOptionDetails];
    this.tabUtilVerticleButtonFunction(widgetArray,this.view.btnOptionDetails);
    //this.setHeaderText();
    this.view.btnOptionDetails.onClick = function() {
      that.showNewRoleTab();
    };
  },
  showAddNewRoles : function(){
    this.hideAll();
    this.hideOptions();
    this.hideMainHeaderButtons();
    this.hideMainSubHeader();
    this.togglePermissionSuboptions(false);
    this.tabUtilVerticleArrowVisibilityFunction(this.selectedArrowArray,this.view.fontIconImgSelected1);
    this.view.flxViews.setVisibility(true);
    this.view.flxAddMainContainer.setVisibility(true);
    this.fillRoleData();
    this.view.flxRolesBreadCrumb.setVisibility(true);
    this.view.lblAddOptionsHeading.text=kony.i18n.getLocalizedString("i18n.frmRolesController.DETAILS");
    var widgetArray = [this.view.btnAddPermissions,this.view.btnAddUsers,this.view.btnOptionDetails];
    this.tabUtilVerticleButtonFunction(widgetArray,this.view.btnOptionDetails);
    this.view.forceLayout();
  },
  updateRoleData:function(updateData){
    kony.adminConsole.utils.showProgressBar(this.view);
    this.presenter.UpdateRoleDetails(updateData);
    this.view.toastMessage.showToastMessage(kony.i18n.getLocalizedString("i18n.frmRolesController.Role_Updated_Successfully"),this);
  },
  getRolePermissions: function(role_id){
    var roleNameObj={"role_id":role_id};
    kony.adminConsole.utils.showProgressBar(this.view);
    this.presenter.fetchRolePermissions(this,roleNameObj);
  },
  getAllUpdateRoles: function(id){
    kony.adminConsole.utils.showProgressBar(this.view);
    this.presenter.fetchAllUpdateRoles(this,id);
  },
  getActiveUsers: function(){
    kony.adminConsole.utils.showProgressBar(this.view);
    this.presenter.fetchActiveUsers(this);
  },
  getAllActivePermissionsAndAllActiveUsers: function(){
    kony.adminConsole.utils.showProgressBar(this.view);
    this.presenter.fetchAllActiveUsersAndAllActivePermissions(this);
  },
  getRoleData: function(role_id){
    var roleNameObj={"role_id":role_id};
    kony.adminConsole.utils.showProgressBar(this.view);
    this.presenter.fetchUpdateRoleData(this,roleNameObj);
  },
  getActivePermissions: function(){
    kony.adminConsole.utils.showProgressBar(this.view);
    this.presenter.fetchActivePermissions(this);
  },
  getRoleUsers: function(role_id){
    var roleNameObj={"role_id":role_id};
    kony.adminConsole.utils.showProgressBar(this.view);
    this.presenter.fetchRoleUsers(this,roleNameObj);
  },
  fillRoleData: function(){
    var scopeObj=this;
    var segIndex = scopeObj.view.segPermissions.selectedRowIndex[1];
    var selectedData = scopeObj.view.segPermissions.data[segIndex];
    this.view.lblAddOptionsHeading.text=kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_ROLES");
    scopeObj.view.mainHeader.lblHeading.text=kony.i18n.getLocalizedString("i18n.frmRolesController.Edit_Roles");
    this.view.breadcrumbs.lblCurrentScreen.text = selectedData.lblRoleName.toUpperCase();
    this.view.breadcrumbs.fontIconBreadcrumbsRight.setVisibility(true);
    this.view.breadcrumbs.lblCurrentScreen.setVisibility(true);
    this.view.tbxRoleNameValue.text=this.roleDetails.lblRoleName;
    this.view.txtRoleDescription.text=this.roleDetails.lblDescription;
    //this.view.calValidEndDate.date=selectedData.lblValidTillDate;
    this.view.btnSave.text=kony.i18n.getLocalizedString("i18n.permission.SAVE");
    if(selectedData.lblRoleStatus.text===kony.i18n.getLocalizedString("i18n.secureimage.Active"))
      this.view.switchStatus.selectedIndex=0;
    else
      this.view.switchStatus.selectedIndex=1;
    this.view.flxAddRoleDetails.setVisibility(true);
    this.view.flxValidity.setVisibility(false);
    this.view.forceLayout();
  },
  showNewRoleTab: function(data)
  {
    this.tabUtilVerticleArrowVisibilityFunction(this.selectedArrowArray,this.view.fontIconImgSelected1);
    var widgetArray = [this.view.btnAddPermissions,this.view.btnAddUsers,this.view.btnOptionDetails];
    this.tabUtilVerticleButtonFunction(widgetArray,this.view.btnOptionDetails);
    this.togglePermissionSuboptions(false);
    this.view.flxAddRoleDetails.setVisibility(true);
    this.view.flxAddPermissionDetails.setVisibility(false);
    this.view.flxAddOptionsContainer.setVisibility(false);
    this.view.flxAddCustomerRoles.setVisibility(false);
  },
  createRoleData: function(data)
  {
    kony.adminConsole.utils.showProgressBar(this.view);
    this.presenter.createRoleDetails(this,data);
    this.view.toastMessage.showToastMessage(kony.i18n.getLocalizedString("i18n.frmRolesController.Role_created_Successfully"),this);
    this.addNewRolePath = false;
  },
  saveRoleData: function(data)
  {
    kony.adminConsole.utils.showProgressBar(this.view);
    this.presenter.UpdateRoleDetails(this,data);
  }, 
  showAddPermissions : function(){
    this.hideAll();
    this.hideOptions();
    this.hideMainHeaderButtons();
    this.togglePermissionSuboptions(true);
    this.view.tbxSearchBox.text = "";
    var widgetArray = [this.view.btnAddPermissions,this.view.btnAddUsers,this.view.btnOptionDetails];
    this.tabUtilVerticleButtonFunction(widgetArray,this.view.btnAddPermissions);
    var widgetArray2 = [this.view.btnAddSysPermissions,this.view.btnAddCustomerAccess];
    this.tabUtilVerticleButtonFunction(widgetArray2,this.view.btnAddSysPermissions);
    this.tabUtilVerticleArrowVisibilityFunction(this.selectedArrowArray,this.view.lblIconSysPermisionsSelected);
    this.view.flxViews.setVisibility(true);
    this.hideMainSubHeader();
    this.view.flxAddMainContainer.setVisibility(true);
    this.view.lblAddOptionsHeading.text=kony.i18n.getLocalizedString("i18n.users.ADDPERMISSIONS");
    this.view.lblSelectedOption.text=kony.i18n.getLocalizedString("i18n.roles.SelectedPermissions");
    this.view.lblAvailableOptionsHeading.text=kony.i18n.getLocalizedString("i18n.roles.AvailablePermissions");
    this.view.flxAddOptionsContainer.setVisibility(true);
    this.view.rtxAvailableOptionsMessage.setVisibility(false);
    this.view.rtxSelectedOptionsMessage.setVisibility(false);
    this.view.flxRolesBreadCrumb.setVisibility(true);
    this.view.flxClearSearch.setVisibility(false);
    this.view.btnNext.setVisibility(true);
    this.setSelectedOptionsSegmentData();
    //this.getAllUpdateRoles(this.roleId);
    this.setAddPermissionsSegmentData();
  },
  showAddUsers : function(){
    this.hideAll();
    this.hideOptions();
    this.hideMainHeaderButtons();
    this.togglePermissionSuboptions(false);
    this.view.tbxSearchBox.text = "";
    this.tabUtilVerticleArrowVisibilityFunction(this.selectedArrowArray,this.view.fontIconImgSelected3);
    var widgetArray = [this.view.btnAddPermissions,this.view.btnAddUsers,this.view.btnOptionDetails];
    this.tabUtilVerticleButtonFunction(widgetArray,this.view.btnAddUsers);
    this.view.flxViews.setVisibility(true);
    this.hideMainSubHeader();
    this.view.flxAddMainContainer.setVisibility(true);
    this.view.lblAddOptionsHeading.text=kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_USERS");
    this.view.lblSelectedOption.text=kony.i18n.getLocalizedString("i18n.permission.SelectedUsers");
    this.view.lblAvailableOptionsHeading.text=kony.i18n.getLocalizedString("i18n.permission.AvailableUsers");
    this.view.flxAddOptionsContainer.setVisibility(true);
    this.view.rtxAvailableOptionsMessage.setVisibility(false);
    this.view.rtxSelectedOptionsMessage.setVisibility(false);
    this.view.flxRolesBreadCrumb.setVisibility(true);
    this.view.btnNext.setVisibility(false);
    this.view.flxClearSearch.setVisibility(false);
    this.setSelectedOptionsSegmentData();
    this.setAddUsersSegmentData();
  },
  showAddRoles : function(){
    this.hideAll();
    this.hideOptions();
    this.hideMainHeaderButtons();
    this.togglePermissionSuboptions(false);
    this.tabUtilVerticleArrowVisibilityFunction(this.selectedArrowArray,this.view.fontIconImgSelected4);
    var widgetArray = [this.view.btnAddRoles,this.view.btnAddUsers,this.view.btnOptionDetails];
    this.tabUtilVerticleButtonFunction(widgetArray,this.view.btnAddRoles);
    this.view.flxViews.setVisibility(true);
    this.hideMainSubHeader();
    this.view.flxAddMainContainer.setVisibility(true);
    this.view.lblAddOptionsHeading.text=kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_ROLES");
    this.view.lblSelectedOption.text=kony.i18n.getLocalizedString("i18n.permission.SelectedRoles");
    this.view.lblAvailableOptionsHeading.text=kony.i18n.getLocalizedString("i18n.permission.AvailableRoles");
    this.view.flxAddOptionsContainer.setVisibility(true);
    this.view.btnNext.setVisibility(true);
    this.view.flxRolesBreadCrumb.setVisibility(true);
    this.setSelectedOptionsSegmentData();
    this.setAddRolesSegmentData();
  },
  showViewPermissionSegmentAndHeader : function(){
    this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.permission.ROLES");
    this.view.flxMainSubHeader.setVisibility(false);
    this.view.flxPermissions.setVisibility(false);
    this.view.flxRolesBreadCrumb.setVisibility(true);
    this.view.flxViews.setVisibility(true);
    this.view.flxAddMainContainer.setVisibility(false);
    this.view.flxViewPermissions.setVisibility(true);  
    this.tabUtilLabelFunction([this.view.lblTabName1,
                               this.view.lblTabName2],this.view.lblTabName1);
    this.subTabsButtonUtilFunction([this.view.tabs.btnTab1,this.view.tabs.btnTab2],this.view.tabs.btnTab1);
    this.view.flxPermissionsHeader.setVisibility(true);
    this.view.flxUsersHeader.setVisibility(false);
    this.view.flxCustomerRoleHeader.setVisibility(false);
    this.view.mainHeader.btnAddNewOption.setVisibility(false);
    this.view.mainHeader.btnDropdownList.setVisibility(false);
    this.view.flxViewSegmentAndHeaders.setVisibility(true);
    this.view.flxViewConfigureCsrCont.setVisibility(false);
    this.view.flxViewDetailsSubTabs.setVisibility(true);
    this.view.flxViewSegmentAndHeaders.top = "105dp";
    this.setViewPermissionSegmentData();
    this.view.forceLayout();
  },
  showViewUsersSegmentAndHeader : function(){
    this.tabUtilLabelFunction([this.view.lblTabName1,
                               this.view.lblTabName2],this.view.lblTabName2);
    this.view.flxPermissionsHeader.setVisibility(false);
    this.view.flxUsersHeader.setVisibility(true);
    this.view.flxCustomerRoleHeader.setVisibility(false);
    this.view.mainHeader.btnAddNewOption.setVisibility(false);
    this.view.mainHeader.btnDropdownList.setVisibility(false);
    this.view.flxViewSegmentAndHeaders.setVisibility(true);
    this.view.flxViewConfigureCsrCont.setVisibility(false);
    this.view.flxViewDetailsSubTabs.setVisibility(false);
    this.view.flxViewSegmentAndHeaders.top = "45dp";
    this.setViewUsersSegmentData();
  },
  downloadCSV:function() {
    kony.print("Inside downloadCSV() of frmRolesController");
    var scopeObj = this;

    var authToken = KNYMobileFabric.currentClaimToken;
    var mfURL = KNYMobileFabric.mainRef.config.reportingsvc.session.split("/services")[0];
    var downloadURL = mfURL + "/services/data/v1/RolesAndPermissionsObjService/operations/role_view/downloadRolesList?authToken=" + authToken ;

    if(scopeObj.view.subHeader.tbxSearchBox.text !== "") {
      downloadURL = downloadURL + "&searchText=" + scopeObj.view.subHeader.tbxSearchBox.text;
    }

    var downloadRolesFilterJSON = scopeObj.view.mainHeader.btnDropdownList.info;

    if(downloadRolesFilterJSON !== undefined && downloadRolesFilterJSON.selectedStatusList !== undefined) {
      var status = "&status=" + downloadRolesFilterJSON.selectedStatusList;
      downloadURL = downloadURL + status;
    }

    var encodedURI = encodeURI(downloadURL);
    var downloadLink = document.createElement("a");
    downloadLink.href = encodedURI;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  },

  setFlowActions : function(){
    var scopeObj=this;
    this.view.segPermissions.onScroll=function(){
      scopeObj.contextualMenuOff();
    };
    this.view.flxClearSearch.onClick = function(){
      scopeObj.view.flxSearchContainer.skin = "sknflxd5d9ddop100"; 
      var area = scopeObj.view.lblAddOptionsHeading.text;
      scopeObj.view.tbxSearchBox.text = "";
      scopeObj.view.segAddOptions.setVisibility(true);
      scopeObj.view.rtxAvailableOptionsMessage.setVisibility(false);
      scopeObj.view.flxClearSearch.setVisibility(false);
      if(area === kony.i18n.getLocalizedString("i18n.users.ADDPERMISSIONS")) {
        scopeObj.setAddPermissionsSegmentData();
      }else if(area === kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_USERS")){
        scopeObj.setAddUsersSegmentData();
      }else if(area === kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_ROLES")){
        scopeObj.setAddPermissionsSegmentData();
      }
    };
    this.view.mainHeader.btnDropdownList.onClick = function(){
      if(scopeObj.view.flxNoResultFound.isVisible === false) {
        scopeObj.downloadCSV();
      }
    };
    this.view.txtRoleDescription.onEndEditing = function(){
      if(scopeObj.view.lblRoleDescriptionSize.isVisible){
        scopeObj.view.lblRoleDescriptionSize.setVisibility(false);
      }
      scopeObj.roleDetails.lblDescription = scopeObj.view.txtRoleDescription.text;
    };
    this.view.txtRoleDescription.onKeyUp = function(){
      scopeObj.view.flxNoRoleDescriptionError.isVisible = false;
      scopeObj.view.txtRoleDescription.skin = "skntxtAreaLato35475f14Px";

      if(scopeObj.view.txtRoleDescription.text.length===0)
      {
        scopeObj.view.lblRoleDescriptionSize.setVisibility(false);
      }
      else
      {
        scopeObj.view.lblRoleDescriptionSize.setVisibility(true);
        scopeObj.view.lblRoleDescriptionSize.text=scopeObj.view.txtRoleDescription.text.length+"/300";
      }
      scopeObj.view.forceLayout();
    };
    this.view.tbxSearchBox.onTouchStart = function(){
      scopeObj.view.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search"; 
    };
    this.view.tbxSearchBox.onEndEditing = function(){
      scopeObj.view.flxSearchContainer.skin = "sknflxd5d9ddop100"; 
    };
    this.view.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.tbxSearchBox.text === ""){
        scopeObj.view.segAddOptions.setVisibility(true);
        scopeObj.view.flxClearSearch.setVisibility(false);
        scopeObj.view.rtxAvailableOptionsMessage.setVisibility(false);
      }
      else{
        scopeObj.view.flxClearSearch.setVisibility(true);
        scopeObj.view.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search"; 
      } 
      scopeObj.searchFromList();
    };
    this.view.subHeader.tbxSearchBox.onTouchStart = function(){
      scopeObj.view.subHeader.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
    };
    this.view.subHeader.tbxSearchBox.onEndEditing = function(){
      scopeObj.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100"; 
    }; 
    this.view.subHeader.tbxSearchBox.onKeyUp = function() {
      scopeObj.currentPage = 1;
      //scopeObj.view.lbxPagination.selectedKey = scopeObj.currentPage;
      if (scopeObj.view.subHeader.tbxSearchBox.text === "") {
        //scopeObj.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100"; 
        scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
      }
      else {
        scopeObj.view.subHeader.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
        scopeObj.view.subHeader.flxClearSearchImage.setVisibility(true);
      } 
      scopeObj.loadPageData();
    };

    this.view.subHeader.flxClearSearchImage.onClick=function(){
      scopeObj.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100"; 
      scopeObj.view.subHeader.tbxSearchBox.text="";
      scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
      scopeObj.loadPageData();
    };
    this.view.subHeader.lbxPageNumbers.onSelection = function(){
      scopeObj.view.flxCheckBoxOuter.isVisible = false;
      scopeObj.view.subHeader.lbxPageNumbers.isVisible = false;
      scopeObj.view.flxSelectOptions.isVisible = false;
      scopeObj.loadPageData();
    };
    //     this.view.lbxPagination.onSelection = function(){
    //         scopeObj.gotoPage();
    //     };
    this.view.flxViewDescription.onClick=function(){
      scopeObj.toggleRtxVisibility();
    };
    this.view.flxViewEditButton.onClick=function(){
      scopeObj.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.permission.ROLES");
      kony.adminConsole.utils.showProgressBar(this.view);
      console.log("obj", scopeObj.roleDetailObj);
      scopeObj.roleId = scopeObj.roleDetailsObj.roleDetails.roleId ;
      var segIndex = scopeObj.view.segPermissions.selectedRowIndex[1];
      var selectedData = scopeObj.view.segPermissions.data[segIndex];
      scopeObj.roleDetails.lblRoleName = selectedData.lblRoleName; 
      scopeObj.roleDetails.lblDescription = selectedData.lblDescription; 
      scopeObj.getRoleData(scopeObj.roleId);
    }; 
    this.view.tbxRoleNameValue.onKeyUp=function(){
      scopeObj.view.tbxRoleNameValue.skin="skntbxLato35475f14px";
      scopeObj.view.flxNoRoleNameError.setVisibility(false); 
      if(scopeObj.view.tbxRoleNameValue.text.length===0)
      {
        scopeObj.view.lblRoleNameSize.setVisibility(false);
      }
      else
      {
        scopeObj.view.lblRoleNameSize.text=scopeObj.view.tbxRoleNameValue.text.length+"/25";
        scopeObj.view.lblRoleNameSize.setVisibility(true);
      }
      scopeObj.roleDetails.lblRoleName = scopeObj.view.tbxRoleNameValue.text;
      scopeObj.view.forceLayout();
    };
    this.view.popUp.btnPopUpDelete.onClick= function(){
      scopeObj.view.flxErrorPopup.setVisibility(false);
    };
    this.view.popUpDeactivate.btnPopUpDelete.onClick= function(){
      scopeObj.DeactivatePermission();
    };
    this.view.popUpDeactivate.btnPopUpCancel.onClick= function(){
      scopeObj.view.flxDeactivatePermission.setVisibility(false);
    };
    this.view.popUpDeactivate.flxPopUpClose.onClick= function(){
      scopeObj.view.flxDeactivatePermission.setVisibility(false);
    };
    this.view.flxOption2.onClick=function(){
      scopeObj.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.permission.ROLES");
      kony.adminConsole.utils.showProgressBar(this.view);
      console.log("obj", scopeObj.roleDetailObj);
      var segIndex = scopeObj.view.segPermissions.selectedRowIndex[1];
      var selectedData = scopeObj.view.segPermissions.data[segIndex];
      scopeObj.roleDetails.lblRoleName = selectedData.lblRoleName; 
      scopeObj.roleDetails.lblDescription = selectedData.lblDescription; 
      scopeObj.getRoleData(scopeObj.roleId);
    };
    this.view.flxOption4.onClick=function(){
      var roleName = scopeObj.roleData[scopeObj.gblselIndex].role_Name;
      scopeObj.onClickActiveDeactive(roleName); 
    };
    this.view.btnRoles.onClick=function(){
      scopeObj.directUsers = true;
      scopeObj.addNewRolePath = true;
      var segIndex = scopeObj.view.segPermissions.selectedRowIndex[1];
      var selectedData = scopeObj.view.segPermissions.data[segIndex];
      scopeObj.roleDetails.lblRoleName = selectedData.lblRoleName; 
      scopeObj.roleDetails.lblDescription = selectedData.lblDescription; 
      scopeObj.getRoleData(scopeObj.roleId);
      scopeObj.fillRoleData();
      scopeObj.showAddUsers();
    };
    this.view.btnPermissions.onClick=function(){
      scopeObj.directPermissions = true;
      scopeObj.addNewRolePath = true;
      var segIndex = scopeObj.view.segPermissions.selectedRowIndex[1];
      var selectedData = scopeObj.view.segPermissions.data[segIndex];
      scopeObj.roleDetails.lblRoleName = selectedData.lblRoleName; 
      scopeObj.roleDetails.lblDescription = selectedData.lblDescription; 
      scopeObj.getRoleData(scopeObj.roleId);
      scopeObj.fillRoleData();
      scopeObj.showAddCustomerRoles();
    };
    this.view.btnOptionDetails.onClick= function(){
      scopeObj.savedEditedDataInTextBox();
      scopeObj.showAddNewRoles();
    };
    this.view.btnAddPermissions.onClick= function(){
      if(scopeObj.validateRoleName()){
       // scopeObj.showAddPermissions();
        scopeObj.showAddCustomerRoles();
      }
      else
        scopeObj.errorRoleName();
    };
    this.view.btnAddUsers.onClick= function(){
      if(scopeObj.validateRoleName()){
        scopeObj.showAddUsers();
      }
      else
        scopeObj.errorRoleName();
    };
    this.view.btnAddRoles.onClick= function(){
      scopeObj.showAddRoles();
    };
    this.view.lblTabName1.onTouchEnd= function(){
      scopeObj.tabUtilLabelFunction([scopeObj.view.lblTabName1,
                                     scopeObj.view.lblTabName2],scopeObj.view.lblTabName1);
      scopeObj.showViewPermissionSegmentAndHeader();
    };
    this.view.lblTabName2.onTouchEnd= function(){
      scopeObj.tabUtilLabelFunction([scopeObj.view.lblTabName1,
                                     scopeObj.view.lblTabName2],scopeObj.view.lblTabName2);
      scopeObj.showViewUsersSegmentAndHeader();
    };
    this.view.mainHeader.btnAddNewOption.onClick=function(){
      scopeObj.navigateToAddNewRoleForm();
      scopeObj.getAllActivePermissionsAndAllActiveUsers();
    };
    this.view.breadcrumbs.btnBackToMain.onClick= function(){
      scopeObj.view.mainHeader.lblHeading.text=kony.i18n.getLocalizedString("i18n.leftmenu.Roles");
      scopeObj.showRoles();
    };
    this.view.btnCancel.onClick= function(){
      scopeObj.showRoles();
    };
    this.view.btnAddRoleCancel.onClick= function(){
      scopeObj.view.tbxRoleNameValue.skin="skntbxLato35475f14px";
      scopeObj.view.flxNoRoleNameError.setVisibility(false);
      scopeObj.presenter.fetchRoleList();
      kony.adminConsole.utils.showProgressBar(this.view);
      scopeObj.showRoles();
    };
    this.view.btnAddPermissionCancel.onClick= function(){
      scopeObj.showRoles();
    };
    this.view.btnAddRoleNext.onClick= function(){
      if(scopeObj.validateRoleName()){
       // scopeObj.showAddPermissions();
        scopeObj.showAddCustomerRoles();
      }
      else
        scopeObj.errorRoleName();
    };
    this.view.btnAddPermissionNext.onClick= function(){
      if(scopeObj.validateRoleName()){
        scopeObj.showAddRoles();
      }
      else
        scopeObj.errorRoleName();

    };
    this.view.btnNext.onClick= function(){
      var from=scopeObj.view.lblAddOptionsHeading.text;
      if(scopeObj.validateRoleName()){
        if(from===kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_ROLES")){
          scopeObj.showAddUsers();
        }
        else if(from===kony.i18n.getLocalizedString("i18n.users.ADDPERMISSIONS")){
          scopeObj.showAddUsers();   
        }
        else if(from===kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_USERS")){
          //code to gofrom add users
        }
      }
      else
        scopeObj.errorRoleName();
    };
    //     this.view.tbxSearchBox.onKeyUp(function() {
    //       scopeObj.searchFromList();
    //     });
    this.view.popUp.flxPopUpClose.onClick= function(){
      scopeObj.view.flxErrorPopup.setVisibility(false);
    };
    this.view.btnSave.onClick= function(){
      if(scopeObj.validateRoleName()){
        if(scopeObj.rolePermissions.length >= 1){
          kony.adminConsole.utils.showProgressBar(scopeObj.view);
          scopeObj.prepareUpdateRoleRequest(scopeObj.roleId);
        }
        else{
          scopeObj.view.flxErrorPopup.setVisibility(true);
        }
      }
      else
        scopeObj.errorRoleName();
    };
    //     this.view.flxPrevious.onTouchStart=function(){
    //        scopeObj.prevPage();
    //      };
    //     this.view.flxNext.onTouchStart=function(){
    //       scopeObj.nextPage();
    //     };
    //     this.view.btnAddRoleSave.onClick= function(){
    //          if(scopeObj.validateRoleName()){
    //            if(scopeObj.rolePermissions.length>=1){
    //             kony.adminConsole.utils.showProgressBar(scopeObj.view);
    //           scopeObj.prepareUpdateRoleRequest(scopeObj.roleId);
    //            }
    //            else{
    //       scopeObj.view.flxErrorPopup.setVisibility(true);
    //            }
    //       }
    //       else
    //         scopeObj.errorRoleName();
    //     };
    this.view.btnAddPermissionSave.onClick= function(){
      scopeObj.showRoles();
    };
    scopeObj.permissionSorter = scopeObj.getObjectSorter('lblPermissionName.info.value');
    scopeObj.determineSortFontIcon(scopeObj.permissionSorter,'lblPermissionName.info.value',scopeObj.view.fontImgViewPermissionNameSort);
    this.view.flxViewPermissionName.onClick = function(){
      scopeObj.permissionSorter.column("lblPermissionName.info.value");
      scopeObj.determineSortFontIcon(scopeObj.permissionSorter,'lblPermissionName.info.value',scopeObj.view.fontImgViewPermissionNameSort);
      var dataSet=scopeObj.view.segViewSegment.data;
      if(dataSet.length!==0){
        dataSet[0].lblSeperator.isVisible=true;
        dataSet=dataSet.sort(scopeObj.permissionSorter.sortData);
        dataSet[0].lblSeperator.isVisible=false;
      }
      scopeObj.view.segViewSegment.setData(dataSet);
    };
    scopeObj.userSorter = scopeObj.getObjectSorter('lblViewFullName');
    scopeObj.setUserSortIcons = function(){
      var setImageSrc = function(imgWidgetID, objColumnName){
        scopeObj.determineSortFontIcon(scopeObj.userSorter, objColumnName,scopeObj.view[imgWidgetID]);
      };
      setImageSrc('fontImgViewUsersNameSort','lblViewFullName');
      setImageSrc('fontImgViewUsersUsernameSort','lblViewUsername');
      setImageSrc('fontImgSortEmail','lblViewEmailId');
      setImageSrc('fontImgSortRole','lblViewUpdatedBy');
      setImageSrc('fontImgSortPermissions','lblViewUpdatedDate');
    };
    scopeObj.setUserSortIcons();
    this.view.flxViewUsersFullName.onClick = function(){
      scopeObj.userSorter.column("lblViewFullName");
      scopeObj.setUserSortIcons();
      scopeObj.view.segViewSegment.setData(scopeObj.sortSegData(scopeObj.userSorter));
    };
    this.view.flxViewUsersUsername.onClick = function(){
      scopeObj.userSorter.column("lblViewUsername");
      scopeObj.setUserSortIcons();
      scopeObj.view.segViewSegment.setData(scopeObj.sortSegData(scopeObj.userSorter));
    };
    this.view.flxViewUsersEmailId.onClick = function(){
      scopeObj.userSorter.column("lblViewEmailId");
      scopeObj.setUserSortIcons();
      scopeObj.view.segViewSegment.setData(scopeObj.sortSegData(scopeObj.userSorter));
    };
    this.view.flxViewUsersUpdatedBy.onClick = function(){
      scopeObj.userSorter.column("lblViewUpdatedBy");
      scopeObj.setUserSortIcons();
      scopeObj.view.segViewSegment.setData(scopeObj.sortSegData(scopeObj.userSorter));
    };
    this.view.flxViewUsersUpdatedOn.onClick = function(){
      scopeObj.userSorter.column("lblViewUpdatedDate");
      scopeObj.setUserSortIcons();
      scopeObj.view.segViewSegment.setData(scopeObj.sortSegData(scopeObj.userSorter));
    };
    this.view.segPermissions.onHover=this.saveScreenY;
    this.view.btnAddAll.onClick=function(){
      scopeObj.selectAllRecords();
    };
    this.view.btnRemoveAll.onClick=function(){
      scopeObj.showRemovePermissionPopup(2);
    };
    this.view.statusFilterMenu.segStatusFilterDropdown.onRowClick = function(){
      scopeObj.performStatusFilter();
    };
    this.view.segPermissions.onHover=this.saveScreenY;
    this.view.flxSelectOptions.onHover = this.onDropDownsHoverCallback;
    this.view.flxRoleStatusFilter.onHover = this.onDropDownsHoverCallback;
    /*
        this.view..onClick= function(){
            scopeObj.();
        };
        */
    this.view.tbxRoleNameValue.onEndEditing = function(){
      scopeObj.view.lblRoleNameSize.setVisibility(false);
    };
    this.view.flxRoleHeaderName.onClick=function(){
      scopeObj.sortBy.column("role_Name");
      scopeObj.resetSortFontIcons();
      scopeObj.currentPage=1;
      //scopeObj.view.lbxPagination.selectedKey = scopeObj.currentPage; 
      scopeObj.loadPageData();
    };
    this.view.flxRoleHeaderUsers.onClick=function(){
      scopeObj.sortBy.column("Users_Count");
      scopeObj.resetSortFontIcons();
      scopeObj.currentPage=1;
      //      scopeObj.view.lbxPagination.selectedKey = scopeObj.currentPage;
      scopeObj.loadPageData();
    };
    this.view.flxRoleHeaderPermissions.onClick=function(){
      scopeObj.sortBy.column("permission_Count");
      scopeObj.resetSortFontIcons();
      scopeObj.currentPage=1;
      //      scopeObj.view.lbxPagination.selectedKey = scopeObj.currentPage;
      scopeObj.loadPageData();
    };
    this.view.flxRoleHeaderStatus.onClick= function () {
      scopeObj.view.flxSelectOptions.setVisibility(false);
      scopeObj.view.flxRoleStatusFilter.setVisibility(!scopeObj.view.flxRoleStatusFilter.isVisible);
      var flxLeft = scopeObj.view.flxRoleHeaderStatus.frame.x;
      scopeObj.view.flxRoleStatusFilter.left = (flxLeft-10)+"px"; 
    };
    this.view.viewConfigureCSRAssist.backToPageHeader.btnBack.onClick = function(){
      scopeObj.view.flxViewSegmentAndHeaders.setVisibility(true);
      scopeObj.view.flxViewConfigureCsrCont.setVisibility(false);
    };
    this.view.btnAddCustomerAccess.onClick  = function(){
      scopeObj.showAddCustomerRoles();
    };
    this.view.btnAddSysPermissions.onClick = function(){
      if(scopeObj.validateRoleName()){
        scopeObj.showAddPermissions();
      }
      else
        scopeObj.errorRoleName();
    };
    this.view.addCustomerRolesOption.tbxSearchBox.onTouchStart = function(){
      scopeObj.view.addCustomerRolesOption.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
    };
    this.view.addCustomerRolesOption.tbxSearchBox.onEndEditing = function(){
      scopeObj.view.addCustomerRolesOption.flxSearchContainer.skin = "sknflxd5d9ddop100";
    };
    this.view.addCustomerRolesOption.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.addCustomerRolesOption.tbxSearchBox.text === ""){
        scopeObj.view.addCustomerRolesOption.flxClearSearchImage.setVisibility(false);
      }else{
        scopeObj.view.addCustomerRolesOption.flxClearSearchImage.setVisibility(true);
        scopeObj.view.addCustomerRolesOption.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
      }
      scopeObj.searchCustomerRolesList();
    };
    this.view.addCustomerRolesOption.flxClearSearchImage.onClick = function(){
      scopeObj.view.addCustomerRolesOption.tbxSearchBox.text = "";
      scopeObj.view.addCustomerRolesOption.flxClearSearchImage.setVisibility(false);
      scopeObj.view.addCustomerRolesOption.flxSearchContainer.skin = "sknflxd5d9ddop100";
      scopeObj.searchCustomerRolesList();
    };
    this.view.commonButtons.btnCancel.onClick = function(){
      scopeObj.showRoles();
    };
    this.view.commonButtons.btnNext.onClick = function(){
      if(scopeObj.validateRoleName()){
       scopeObj.showAddPermissions();   
      }
      else
        scopeObj.errorRoleName();
    };
    this.view.commonButtons.btnSave.onClick = function(){
      scopeObj.view.btnSave.onClick();
    };
    this.view.addCustomerRolesOption.btnSelectAll.onClick= function(){
      scopeObj.addAllCustomerRoles();
    };
    this.view.addCustomerRolesOption.btnRemoveAll.onClick= function(){
      scopeObj.showRemoveCustomerRolePopup(2);
    };
    this.view.tabs.btnTab1.onClick = function(){
       scopeObj.showViewPermissionSegmentAndHeader();
    };
    this.view.tabs.btnTab2.onClick = function(){
      scopeObj.viewCustomerRolesForARole();
    };
    this.view.flxViewCustRoleName.onClick = function(){
      scopeObj.custRoleSorter.column("lblCustomerRoleName");
      scopeObj.determineSortFontIcon(scopeObj.custRoleSorter,'lblCustomerRoleName',scopeObj.view.lblIconViewCustRoleNameSort);
      var dataSet=scopeObj.view.segViewSegment.data;
      if(dataSet.length!==0){
        dataSet[0].lblCustomerRoleSeperator.isVisible=true;
        dataSet=dataSet.sort(scopeObj.custRoleSorter.sortData);
        dataSet[0].lblCustomerRoleSeperator.isVisible=false;
      }
      scopeObj.view.segViewSegment.setData(dataSet);
    };
  },
  resetSortFontIcons:function(){
    this.determineSortFontIcon(this.sortBy,'permission_Count',this.view.fontIconSortPermissions);
    this.determineSortFontIcon(this.sortBy,'Users_Count',this.view.fontIconSortUser);
    this.determineSortFontIcon(this.sortBy,'role_Name',this.view.fontIconSortName);
  },
  onDropDownsHoverCallback:function(widget,context){
    var self=this;
    var widgetId = widget.id;

    if (context.eventType === constants.ONHOVER_MOUSE_ENTER) {
      self.view[widgetId].setVisibility(true);
    } else if (context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      self.view[widgetId].setVisibility(true);
    } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      self.view[widgetId].setVisibility(false);
    }
    self.view.forceLayout();
  },
  saveScreenY:function(widget,context){
    this.mouseYCoordinate=context.screenY;
    // kony.print(this.mouseYCoordinate);
  },
  sortSegData :function(sortBy){
    var dataSet;
    dataSet=this.view.segViewSegment.data;
    if(dataSet.length!==0){
      dataSet[0].lblViewSeperator.isVisible=true;
      dataSet=dataSet.sort(sortBy.sortData);
      dataSet[0].lblViewSeperator.isVisible=false;
    }
    return dataSet;
  },
  showSuccessMessage : function() {
    if(this.fromEditRoles === true){
      this.view.lbltoastMessage.text = kony.i18n.getLocalizedString("i18n.frmRolesController.Role_successfully_Edited");
      kony.timer.schedule("mytimer", this.callBackTimer, 2, false);
      this.view.flxToastMessage.setVisibility(true);
      this.view.forceLayout();
      this.fromEditRoles = false;
    }
    else if(this.fromCreateRole === true) {
      this.view.lbltoastMessage.text = kony.i18n.getLocalizedString("i18n.frmRolesController.Role_successfully_created");
      kony.timer.schedule("mytimer", this.callBackTimer, 2, false);
      this.view.flxToastMessage.setVisibility(true);
      this.view.forceLayout();
      this.fromCreateRole = false;

    }
  } ,
  setRolesSegmentData : function(response,isFilter){
    var self = this;
    //response=response.concat(response).concat(response).concat(response).concat(response).concat(response).concat(response).concat(response);
    // response=response.concat(response).concat(response).concat(response).concat(response).concat(response).concat(response).concat(response);

    if(response.length === 0){
      self.view.rtxSearchMesg.text=kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found")+"\""+self.view.subHeader.tbxSearchBox.text+"\""+kony.i18n.getLocalizedString("i18n.frmPoliciesController.Try_with_another_keyword");
      self.view.flxNoResultFound.setVisibility(true);
      self.view.flxNoResultFound.top="0px";
      self.view.flxPermissionsContainer.height="43%";
      self.view.flxSegRoles.setVisibility(false);
      self.view.segPermissions.setData(response);
      self.view.flxRolesHeader.setVisibility(false);	
      //self.view.flxPagination.setVisibility(false);
    }else{
      self.view.flxNoResultFound.setVisibility(false);
      self.view.flxRolesHeader.setVisibility(true);
      self.view.flxSegRoles.setVisibility(true);
      self.view.flxPermissionsContainer.height="100%";
      var dataMap={
        "flxRoleHeaderDescription": "flxRoleHeaderDescription",
        "flxRoleHeaderName": "flxRoleHeaderName",
        "flxRoleHeaderPermissions": "flxRoleHeaderPermissions",
        "flxRoleHeaderStatus": "flxRoleHeaderStatus",
        "flxRoleHeaderUsers": "flxRoleHeaderUsers",
        "flxRoleHeaderValidTill": "flxRoleHeaderValidTill",
        "flxRolesHeader": "flxRolesHeader",
        "fontIconSortName": "fontIconSortName",
        "fontIconSortPermissions": "fontIconSortPermissions",
        "fontIconFilterStatus": "fontIconFilterStatus",
        "fontIconSortUser": "fontIconSortUser",
        "fontIconSortValidTill": "fontIconSortValidTill",
        "lblRoleHeaderDescription": "lblRoleHeaderDescription",
        "lblRoleHeaderName": "lblRoleHeaderName",
        "lblRoleHeaderPermissions": "lblRoleHeaderPermissions",
        "lblRoleHeaderSeperator": "lblRoleHeaderSeperator",
        "lblRoleHeaderStatus": "lblRoleHeaderStatus",
        "lblRoleHeaderUsers": "lblRoleHeaderUsers",
        "lblRoleHeaderValidTill": "lblRoleHeaderValidTill",
        "flxOptions": "flxOptions",
        "flxRoles": "flxRoles",
        "flxStatus": "flxStatus",
        "lblIconImgOptions": "lblIconImgOptions",
        "fontIconStatusImg": "fontIconStatusImg",
        "lblDescription": "lblDescription",
        "lblHeaderSeperator":"lblHeaderSeperator",
        "lblNoOfUsers": "lblNoOfUsers",
        "lblPermissions": "lblPermissions",
        "lblRoleName": "lblRoleName",
        "lblRoleStatus": "lblRoleStatus",
        "lblSeperator": "lblSeperator",
        //"lblValidTillDate": "lblValidTillDate"
      };
      var sortIconFor = function(column){
        return self.determineSortIconForSeg(self.sortBy,column);
      };

      var data = [];
      //       [{
      //           "fontIconSortName": self.determineSortIconForSeg(self.sortBy,'role_Name'),
      //           "fontIconSortPermissions": self.determineSortIconForSeg(self.sortBy,'permission_Count'),
      //           "fontIconFilterStatus": {"text":"\ue916","skin":"sknIcon15px"},
      //           "fontIconSortUser": self.determineSortIconForSeg(self.sortBy,'Users_Count'),
      //           "fontIconSortValidTill": '',
      //           "lblHeaderSeperator": "-",
      //           "lblRoleHeaderDescription": {"text":kony.i18n.getLocalizedString("i18n.View.DESCRIPTION"),"skin":"sknlblLato5d6c7f12px","hoverSkin":undefined},
      //           "lblRoleHeaderName": kony.i18n.getLocalizedString("i18n.permission.NAME"),
      //           "lblRoleHeaderPermissions": kony.i18n.getLocalizedString("i18n.roles.PERMISSIONS"),
      //           "lblRoleHeaderSeperator": "-",
      //           "lblRoleHeaderStatus": kony.i18n.getLocalizedString("i18n.roles.STATUS"),
      //           "lblRoleHeaderUsers": kony.i18n.getLocalizedString("i18n.users.Users"),
      //           "lblRoleHeaderValidTill": kony.i18n.getLocalizedString("i18n.Group.ValidTill"),
      //           "template": "flxRolesHeader",
      //           "flxRoleHeaderName": {
      //             "onClick": function () {
      //               self.sortBy.column("role_Name");
      //               self.currentPage=1;
      //               self.view.lbxPagination.selectedKey = self.currentPage; 
      //               self.loadPageData();
      //             }
      //           },
      //           "flxRoleHeaderUsers": {
      //             "onClick": function () {
      //               self.sortBy.column("Users_Count");
      //               self.currentPage=1;
      //               self.view.lbxPagination.selectedKey = self.currentPage;
      //               self.loadPageData();
      //             }
      //           },
      //           "flxRoleHeaderPermissions": {
      //             "onClick": function () {
      //               self.sortBy.column("permission_Count");
      //               self.currentPage=1;
      //               self.view.lbxPagination.selectedKey = self.currentPage;
      //               self.loadPageData();
      //             }
      //           },
      //           "flxRoleHeaderStatus": {
      //             "onClick": function () {
      //               self.view.flxRoleStatusFilter.setVisibility(!self.view.flxRoleStatusFilter.isVisible);
      //             }
      //           }
      //         },
      //         []
      //       ]
      //     ];
      var statusList=[];
      if (typeof response !== 'undefined') {
        data = response.map(function(roleViewData) {
          if(!statusList.contains(roleViewData.Status_Desc))
            statusList.push(roleViewData.Status_Desc);
          return {
            "roleId": roleViewData.role_id,
            "statusId":roleViewData.Status_id,
            "lblIconImgOptions": {"text":"\ue91f"},
            "fontIconStatusImg": roleViewData.Status_Desc === kony.i18n.getLocalizedString("i18n.secureimage.Active")?{"skin":"sknFontIconActivate"} : {"skin":"sknfontIconInactive"},
            "lblDescription": roleViewData.role_Desc,
            "lblNoOfUsers": roleViewData.Users_Count,
            "lblPermissions": roleViewData.permission_Count,
            "lblRoleName": roleViewData.role_Name,
            "lblRoleStatus": roleViewData.Status_Desc === kony.i18n.getLocalizedString("i18n.secureimage.Active")?{"text":roleViewData.Status_Desc,"skin":"sknlblLato5bc06cBold14px"}: {"text":roleViewData.Status_Desc,"skin":"sknlblLatoDeactive"},                
            "lblSeperator": "-",
            "template":"flxRoles",
            "flxOptions": {
              "onClick": function () {
                kony.print(kony.i18n.getLocalizedString("i18n.frmCSRController.accounts_EDIT_Message"));
                self.roleId = roleViewData.role_id;
                self.statusId = roleViewData.Status_id;
                self.onClickOptions();
              }
            },
            "flxRoles" : {
              "onClick" : function () {
                self.fetchRoleDetails(roleViewData.role_id,roleViewData.role_Name,
                                      roleViewData.role_Desc,roleViewData.Status_id);
              }
            }
          };

        });
      }
      if(!isFilter){
        self.roleData = response;
        self.SetStatusFilterData(statusList);
      }
      this.view.segPermissions.widgetDataMap=dataMap;
      //this.view.segPermissions.setData(data);   
      this.rolesData = data;
      data[0].lblSeperator.isVisible = false;
      this.view.segPermissions.setData(data);

      //self.view.flxPagination.setVisibility(true);
    }
    this.view.forceLayout();
  },
  contextualMenuOff: function(context) {
    this.view.flxSelectOptions.isVisible = false;
  },
  fixContextualMenu:function(heightVal){
    if(((this.view.flxSelectOptions.frame.height+heightVal)>(this.view.segPermissions.frame.height+50))&&this.view.flxSelectOptions.frame.height<this.view.segPermissions.frame.height){
      this.view.flxSelectOptions.top=((heightVal-this.view.flxSelectOptions.frame.height)-39)+"px";
    }
    else{
      this.view.flxSelectOptions.top=(heightVal)+"px";
    }
    this.view.forceLayout();
  }, 


  SetStatusFilterData:function(segData){
    var self = this;
    var widgetMap = {
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var data = segData.map(function(segData){
      return{
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "lblDescription": segData,
        "imgCheckBox":{
          "src":"checkbox.png"
        }
      };
    });

    self.view.statusFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
    self.view.statusFilterMenu.segStatusFilterDropdown.setData(data);
    var indices = [];
    for(index = 0; index < data.length; index++){
      indices.push(index);
    }
    self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices = [[0,indices]];
    //self.view.flxRoleStatusFilter.setVisibility(true);

  },
  //   getNumPerPage: function () {
  //     return this.view.subHeader.lbxPageNumbers.selectedKeyValue
  //     ? this.view.subHeader.lbxPageNumbers.selectedKeyValue[1]
  //   : "10";
  //   },
  //   assignPageList: function (pageData) {
  //     var selectedPage = Number(this.view.lbxPagination.selectedKey) || 1;
  //     this.view.lbxPagination.masterData = pageData;
  //     this.view.lbxPagination.selectedKey = selectedPage;
  //   },
  addPermissionstoRole: function() {
    var self=this;
    this.view.rtxSelectedOptionsMessage.setVisibility(false);
    this.view.btnRemoveAll.setVisibility(true);
    var sourceSegment = kony.application.getCurrentForm().segAddOptions;
    var selected = sourceSegment.selectedItems[0];
    var targetSegment = kony.application.getCurrentForm().segSelectedOptions;
    var toAdd = {
      "fontIconClose":{"text":"","tooltip":kony.i18n.getLocalizedString("i18n.frmRolesController.remove_permission")}, 
      "lblOption": "" + selected.lblPermissionsName,
      "permissionId": selected.permissionId,
      "sourceData": selected,
      "flxClose":{"isVisible":false,"onClick":function(){self.showRemovePermissionPopup(1);}},
      "flxAddOptionWrapper":{
        "onHover":self.onHoverEventCallback

      }
    };
    targetSegment.data.push(toAdd);
    targetSegment.setData(targetSegment.data);
    sourceSegment.data.remove(selected);
    if( sourceSegment.data.length===0){
      this.view.rtxAvailableOptionsMessage.text=kony.i18n.getLocalizedString("i18n.frmPermissionsController.No_Permissions_Available");
      this.view.rtxAvailableOptionsMessage.setVisibility(true);
      this.view.btnAddAll.setVisibility(false);
      targetSegment.setVisibility(false);
    }
    else{
      this.view.rtxAvailableOptionsMessage.setVisibility(false);
      this.view.btnAddAll.setVisibility(true);
      targetSegment.setVisibility(true);
    }
    sourceSegment.setData(sourceSegment.data);
    this.rolePermissions = targetSegment.data;
    this.showHidePlaceHolder();
    kony.application.getCurrentForm().forceLayout();
  },
  addUserstoRole: function(){
    var self=this;
    this.view.rtxSelectedOptionsMessage.setVisibility(false);
    this.view.btnRemoveAll.setVisibility(true);
    var sourceSegment = kony.application.getCurrentForm().segAddOptions;
    var selected = sourceSegment.selectedItems[0];
    var targetSegment = kony.application.getCurrentForm().segSelectedOptions;
    var toAdd={
      "fontIconClose":{"text":"","tooltip":kony.i18n.getLocalizedString("i18n.frmRolesController.remove_user")}, 
      "lblOption": selected.lblFullName,
      "userId": "" + selected.userId,
      "sourceData": selected,
      "flxClose":{"isVisible":false,"onClick":self.unSelectedOption},
      "flxAddOptionWrapper":{
        "onHover":self.onHoverEventCallback

      }
    };
    targetSegment.data.push(toAdd);
    targetSegment.setData(targetSegment.data);
    sourceSegment.data.remove(selected);
    if( sourceSegment.data.length===0){
      this.view.rtxAvailableOptionsMessage.text=kony.i18n.getLocalizedString("i18n.frmPermissionsController.No_Users_Available");
      this.view.rtxAvailableOptionsMessage.setVisibility(true);
      this.view.btnAddAll.setVisibility(false);
      targetSegment.setVisibility(false);
    }
    else{
      this.view.rtxAvailableOptionsMessage.setVisibility(false);
      this.view.btnAddAll.setVisibility(true);
      targetSegment.setVisibility(true);
    }
    sourceSegment.setData(sourceSegment.data);
    this.roleUsers = targetSegment.data;
    this.showHidePlaceHolder();
    kony.application.getCurrentForm().forceLayout();
  },
  showHidePlaceHolder : function(){
    if (this.view.lblAddOptionsHeading.text == kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_ROLES")){
      this.view.rtxSelectedOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmRolesController.select_a_permission");
    }else if (this.view.lblAddOptionsHeading.text == kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_USERS")){
      this.view.rtxSelectedOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmPermissionsController.Click_Add_to_select_a_user");
    }
    if(this.view.segSelectedOptions.data.length <= 0){
      this.view.rtxSelectedOptionsMessage.text=kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_to_select_the_item");
      this.view.rtxSelectedOptionsMessage.isVisible = true;
    }else{
      this.view.rtxSelectedOptionsMessage.isVisible = false;
    }

  },
  unSelectedOption: function() {
    this.view.rtxAvailableOptionsMessage.setVisibility(false);
    var sourceSegment = kony.application.getCurrentForm().segSelectedOptions;
    var selectedRow = sourceSegment.selectedRowIndex[1];
    var selected = sourceSegment.data[selectedRow];
    var targetSegment = kony.application.getCurrentForm().segAddOptions;
    /*var toAdd={
                                                "imgClose": "close_blue.png",
                                                "lblOption": ""+lblOptionText,
            "userId": "" + userId,
            "sourceData": selected
                                };*/
    targetSegment.data.push(selected.sourceData);
    targetSegment.setData(targetSegment.data);
    sourceSegment.data.remove(selected);
    if( sourceSegment.data.length===0){
      this.view.rtxSelectedOptionsMessage.text=kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_to_select_the_item");
      this.view.rtxSelectedOptionsMessage.setVisibility(true);
      this.view.btnRemoveAll.setVisibility(false);
    }
    else{
      this.view.rtxSelectedOptionsMessage.setVisibility(false);
      this.view.btnRemoveAll.setVisibility(true);
    }
    sourceSegment.setData(sourceSegment.data);
    this.view.flxClearSearch.onClick();
    this.view.segAddOptions.setVisibility(targetSegment.data.length!==0);
    this.view.segSelectedOptions.setVisibility(sourceSegment.data.length!==0);
    this.view.btnAddAll.setVisibility(targetSegment.data.length!==0);
    this.view.btnremoveAll.setVisibility(sourceSegment.data.length!==0);
    this.showHidePlaceHolder();
    kony.application.getCurrentForm().forceLayout();
  },
  searchFromList: function() {
    var searchKey = this.view.tbxSearchBox.text.toLowerCase();
    var area = this.view.lblAddOptionsHeading.text;
    if(searchKey === ""){
      this.view.flxClearSearch.setVisibility(false);
      this.view.segAddOptions.setVisibility(true);
      this.view.rtxAvailableOptionsMessage.setVisibility(false);
    }
    else{
      this.view.flxClearSearch.setVisibility(true);
    }
    if(area === kony.i18n.getLocalizedString("i18n.users.ADDPERMISSIONS")) {
      if(searchKey.length > 0) {
        this.setAddPermissionsSegmentDataAfterSearch(searchKey);
      } else {
        this.setAddPermissionsSegmentData();
      }
    } else if (area === kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_USERS")) {
      if(searchKey.length > 0) {
        this.setAddUsersSegmentDataAfterSearch(searchKey);
      } else {
        this.setAddUsersSegmentData();
      }
    }
  },
  prepareUpdateRoleRequest: function(roleId) {
    var rolePermissions = this.rolePermissions.slice(0);
    var orgRolePermissions = this.orgRolePermissions.slice(0);
    var orgRoleUsers = this.orgRoleUsers.slice(0);
    var roleUsers = this.roleUsers.slice(0);
    var roleName = this.view.tbxRoleNameValue.text.trim();
    var roleUserIds = [];
    var rolePermissionIds = [];
    var orgRoleUserIds = [];
    var orgRolePermissionIds = [];
    var roleUserIdsAdded = [];
    var rolePermissionIdsAdded = [];
    var roleUserIdsRemoved = [];
    var rolePermissionIdsRemoved = [];
    var request = {};
    for(var i = 0; i < rolePermissions.length; i++) {
      rolePermissionIds[rolePermissionIds.length] = "" + rolePermissions[i].permissionId;
    }
    for(var i = 0; i < roleUsers.length; i++) {
      roleUserIds[roleUserIds.length] = "" + roleUsers[i].userId;
    }
    for(var i = 0; i < orgRolePermissions.length; i++) {
      orgRolePermissionIds[orgRolePermissionIds.length] = "" + orgRolePermissions[i].permissionId;
    }
    for(var i = 0; i < orgRoleUsers.length; i++) {
      orgRoleUserIds[orgRoleUserIds.length] = "" + orgRoleUsers[i].userId;
    }
    roleUserIdsRemoved = this.updatedIdUsersPermissions(orgRoleUserIds,roleUserIds);
    rolePermissionIdsRemoved = this.updatedIdUsersPermissions(orgRolePermissionIds,rolePermissionIds);
    roleUserIdsAdded = this.updatedIdUsersPermissions(roleUserIds,orgRoleUserIds);
    rolePermissionIdsAdded  = this.updatedIdUsersPermissions(rolePermissionIds,orgRolePermissionIds);
    
    if(this.addNewRolePath === true){
      this.fromCreateRole = true;
      this.fromEditRoles = false;
      request = {
        "Role_Name" : this.roleDetails.lblRoleName,
        "Role_Desc" : this.roleDetails.lblDescription,
        "Status_id" : this.view.switchStatus.selectedIndex === 1 ?"SID_INACTIVE":"SID_ACTIVE",
        "system_user" : kony.mvc.MDAApplication.getSharedInstance().appContext.userID,
        "Permission_ids" : rolePermissionIdsAdded,
        "User_ids" : roleUserIdsAdded,
        "AddedRoles": this.getUpdatedCustomerRolesList().AddedRoles,
        "RemovedRoles": []
      };
      this.createRoleData(request);
    }

    else{
      this.fromCreateRole = false;
      this.fromEditRoles = true;
      request = {
        "User_id": kony.mvc.MDAApplication.getSharedInstance().appContext.userID,
        "Role_Details": {
          "id": roleId,
          "Name": roleName,
          "Description": this.view.txtRoleDescription.text.trim(),
          "Status_id": this.view.switchStatus.selectedIndex === 1 ?"SID_INACTIVE":"SID_ACTIVE"
        },
        "AssignedTo": {
          "permissionList": rolePermissionIdsAdded,
          "usersList": roleUserIdsAdded
        },
        "RemovedFrom": {
          "permissionList": rolePermissionIdsRemoved,
          "usersList": roleUserIdsRemoved
        },
        "AddedRoles":this.getUpdatedCustomerRolesList().AddedRoles,
        "RemovedRoles": this.getUpdatedCustomerRolesList().RemovedRoles
      };
      this.updateRoleData(request);
    }     
  },
  setAddPermissionsSegmentData : function(){
    var dataMap={
      "btnAdd": "btnAdd",
      "flxAddPermissions": "flxAddPermissions",
      "flxAddWrapper": "flxAddWrapper",
      "lblPermissionsName": "lblPermissionsName",
      "rtxPermissionDescription": "rtxPermissionDescription"
    };
    this.view.segAddOptions.widgetDataMap=dataMap;
    var data = this.allPermissionsData.slice(0);
    var removalIds = [];
    for(var i = 0; i < this.rolePermissions.length; i++) {
      removalIds[removalIds.length] = this.rolePermissions[i].permissionId;
    }
    i = 0;
    while (i < data.length) {
      if (removalIds.indexOf(data[i].permissionId) > -1) {
        data.remove(data[i]);
      } else {
        i++;
      }
    }
    this.view.segAddOptions.setVisibility(true);
    this.view.segAddOptions.setData(data);
    if(data.length === 0 ){
      this.view.btnAddAll.setVisibility(false);
      this.view.segAddOptions.setVisibility(false);
      this.view.rtxAvailableOptionsMessage.text =kony.i18n.getLocalizedString("i18n.frmPermissionsController.No_Permissions_Available");
      this.view.rtxAvailableOptionsMessage.setVisibility(true);
    }
    this.view.btnAddAll.setVisibility(this.view.segAddOptions.data.length!==0);
    this.view.forceLayout();
  },
  setAddPermissionsSegmentDataAfterSearch : function(searchKey) {
    var dataMap={
      "btnAdd": "btnAdd",
      "flxAddPermissions": "flxAddPermissions",
      "flxAddWrapper": "flxAddWrapper",
      "lblPermissionsName": "lblPermissionsName",
      "rtxPermissionDescription": "rtxPermissionDescription"
    };
    var data = this.allPermissionsData.slice(0);
    var removalIds = [];
    for(var i = 0; i < this.rolePermissions.length; i++) {
      removalIds[removalIds.length] = this.rolePermissions[i].permissionId;
    }
    i = 0;
    while (i < data.length) {
      if (removalIds.indexOf(data[i].permissionId) > -1  || data[i].lblPermissionsName.toLowerCase().indexOf(searchKey) === -1) {
        data.remove(data[i]);
      } else {
        i++;
      }
    }
    if(data.length > 0){
      this.view.segAddOptions.setVisibility(true);
      this.view.rtxAvailableOptionsMessage.text =kony.i18n.getLocalizedString("i18n.frmPermissionsController.No_Users_Available");
      this.view.rtxAvailableOptionsMessage.setVisibility(false);
    }else{
      this.view.segAddOptions.setVisibility(false);
      this.view.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found")+"\""+searchKey+"\""+kony.i18n.getLocalizedString("i18n.frmPoliciesController.Try_with_another_keyword");
      this.view.rtxAvailableOptionsMessage.setVisibility(true);
    } 
    this.view.segAddOptions.setData(data);
    this.view.btnAddAll.setVisibility(this.view.segAddOptions.data.length!==0);
    this.view.forceLayout(); 
  },
  setAddUsersSegmentData : function() {
    var dataMap={
      "btnAdd": "btnAdd",
      "flxAddUsers": "flxAddUsers",
      "flxAddUsersWrapper": "flxAddUsersWrapper",
      "flxxUsernameWrapper": "flxxUsernameWrapper",
      "lblFullName": "lblFullName",
      "lblUserIdValue": "lblUserIdValue",
      "lblUsername": "lblUsername"
    };
    this.view.segAddOptions.widgetDataMap=dataMap;
    var data = this.allUsersData.slice(0);
    var removalIds = [];
    for(var i = 0; i < this.roleUsers.length; i++) {
      removalIds[removalIds.length] = this.roleUsers[i].userId;
    }
    i = 0;
    while (i < data.length) {
      if (removalIds.indexOf(data[i].userId) > -1) {
        data.remove(data[i]);
      } else {
        i++;
      }
    }
    this.view.segAddOptions.setVisibility(true);
    this.view.segAddOptions.setData(data);
    if(data.length === 0 ){
      this.view.btnAddAll.setVisibility(false);
      this.view.segAddOptions.setVisibility(false);
      this.view.rtxAvailableOptionsMessage.text =kony.i18n.getLocalizedString("i18n.frmPermissionsController.No_Users_Available");
      this.view.rtxAvailableOptionsMessage.setVisibility(true);
    }
    this.view.btnAddAll.setVisibility(this.view.segAddOptions.data.length!==0);
    this.view.forceLayout();
  },
  setAddUsersSegmentDataAfterSearch : function(searchKey) {
    var dataMap={
      "btnAdd": "btnAdd",
      "flxAddUsers": "flxAddUsers",
      "flxAddUsersWrapper": "flxAddUsersWrapper",
      "flxxUsernameWrapper": "flxxUsernameWrapper",
      "lblFullName": "lblFullName",
      "lblUserIdValue": "lblUserIdValue",
      "lblUsername": "lblUsername"
    };
    var data = this.allUsersData.slice(0);
    var removalIds = [];
    for(var i = 0; i < this.roleUsers.length; i++) {
      removalIds[removalIds.length] = this.roleUsers[i].userId;
    }
    i = 0;
    while (i < data.length) {
      if (removalIds.indexOf(data[i].userId) > -1 || data[i].lblFullName.toLowerCase().indexOf(searchKey) === -1) {
        data.remove(data[i]);
      } else {
        i++;
      }
    }
    if(data.length > 0){
      this.view.segAddOptions.setVisibility(true);
      this.view.rtxAvailableOptionsMessage.text =kony.i18n.getLocalizedString("i18n.frmPermissionsController.No_Users_Available");
      this.view.rtxAvailableOptionsMessage.setVisibility(false);
    }else{
      this.view.segAddOptions.setVisibility(false);
      this.view.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found")+"\""+searchKey+"\""+kony.i18n.getLocalizedString("i18n.frmPoliciesController.Try_with_another_keyword");
      this.view.rtxAvailableOptionsMessage.setVisibility(true);
    } 
    this.view.segAddOptions.widgetDataMap=dataMap;
    this.view.segAddOptions.setData(data);
    this.view.forceLayout();
  },
  setAddRolesSegmentData : function(){
    var dataMap={
      "btnAdd": "btnAdd",
      "flxAddUsers": "flxAddUsers",
      "flxAddUsersWrapper": "flxAddUsersWrapper",
      "flxxUsernameWrapper": "flxxUsernameWrapper",
      "lblFullName": "lblFullName",
      "lblUserIdValue": "lblUserIdValue",
      "lblUsername": "lblUsername"
    };
    var data=[];
    this.view.segAddOptions.widgetDataMap=dataMap;
    this.view.segAddOptions.setData(data);
    this.view.forceLayout();
  },
  setViewUsersSegmentData : function(){
    var users = this.roleDetailsObj.roleUsers ;
    var self = this;
    var data = users.map(function(user) {
      var fullname = user.FirstName + " " + (user.MiddleName===null ? "" : user.MiddleName) + " " + user.LastName;
      return {
        "template": "flxViewUsers",
        "lblViewEmailId": user.Email,
        "lblViewFullName": fullname,
        "lblViewSeperator": {"isVisible":true,"text":"-"},
        "lblViewUpdatedBy": user.UpdatedBy,
        "lblViewUpdatedDate": self.getLocaleDateAndTime(self.getDateInstanceFromDBDateTime(user.LastModifiedTimeStamp)),
        "lblViewUpdatedTime": "",
        "lblViewUsername": user.Username
      };
    });
    if(data){
      var dataMap={
        "flxViewUsers": "flxViewUsers",
        "lblViewEmailId": "lblViewEmailId",
        "lblViewFullName": "lblViewFullName",
        "lblViewSeperator": "lblViewSeperator",
        "lblViewUpdatedBy": "lblViewUpdatedBy",
        "lblViewUpdatedDate": "lblViewUpdatedDate",
        "lblViewUpdatedTime": "lblViewUpdatedTime",
        "lblViewUsername": "lblViewUsername"
      };
      this.view.segViewSegment.widgetDataMap=dataMap;
      this.view.segViewSegment.setData(data.sort(this.userSorter.sortData));
      if (data.length !== 0) {
        data[0].lblViewSeperator.isVisible=false;
        this.view.segViewSegment.setData(data);
        this.view.flxUsersHeader.setVisibility(true);
        this.view.rtxAvailabletxt.setVisibility(false);
      } else {
        this.view.flxUsersHeader.setVisibility(false);
        this.view.rtxAvailabletxt.setVisibility(true);
        this.view.rtxAvailabletxt.text = kony.i18n.getLocalizedString("i18n.frmPermissionsController.No_Users_Available");
      }
      this.view.forceLayout();
    }
  },
  setViewPermissionSegmentData : function(){
    var self  = this;
    var rolePermissions = this.roleDetailsObj.rolePermissions;
    var data = rolePermissions.map(function(permission) {

      var actionIcon = "";
      if(permission.Permission_isComposite === "true"){
        actionIcon= self.getActionItem();
      }
      return {
        "lblDescription": permission.Permission_Description,
        "lblPermissionName": {"text":self.AdminConsoleCommonUtils.getTruncatedString(permission.Permission_Name,28,26),
                              "tooltip":permission.Permission_Name,
                              "info":{"value":permission.Permission_Name}
                             },
        "flxActionIcon": {
          "isVisible": permission.Permission_isComposite === "true" ? true : false,
          "onClick": function () {
            self.navigateToConfigureViewCSRPerm();
          }
        },
        "lblIconAction": actionIcon,
        "Permission_id": permission.Permission_id,
        "Role_id": permission.Role_id,
        "lblSeperator": {
          "isVisible": true,
          "text": "-"
        },
        "template": "flxViewPermissions",
      };
    });
    var dataMap={
      "flxViewPermissions": "flxViewPermissions",
      "lblDescription": "lblDescription",
      "lblPermissionName": "lblPermissionName",
      "lblSeperator": "lblSeperator",
      "flxActionIcon": "flxActionIcon",
      "lblIconAction": "lblIconAction",
      "Permission_id": "Permission_id",
      "Role_id": "Role_id"
    };
    this.view.segViewSegment.widgetDataMap=dataMap;
    this.view.segViewSegment.setData(data.sort(this.permissionSorter.sortData));    
    if (data.length !== 0) {
      data[0].lblSeperator.isVisible=false;
      this.view.segViewSegment.setData(data);
      this.view.rtxAvailabletxt.setVisibility(false);
    } else {
      this.view.rtxAvailabletxt.setVisibility(true);
      this.view.rtxAvailabletxt.text = kony.i18n.getLocalizedString("i18n.frmPermissionsController.No_Permissions_Available");
    }
    this.view.forceLayout();
  },
  setSelectedOptionsSegmentData : function(){
    var data;
    var dataMap={
      "flxAddOptionWrapper": "flxAddOptionWrapper",
      "flxClose": "flxClose",
      "flxOptionAdded": "flxOptionAdded",
      "fontIconClose": "fontIconClose",
      "lblOption": "lblOption"
    };
    var area = this.view.lblAddOptionsHeading.text;
    if(area === kony.i18n.getLocalizedString("i18n.users.ADDPERMISSIONS")) {
      data = this.rolePermissions;
    } else if (area === kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_USERS")) {
      data = this.roleUsers;
    }
    this.view.rtxSelectedOptionsMessage.setVisibility(false);
    this.view.btnRemoveAll.setVisibility(true);
    if(data > 0){
      this.view.segSelectedOptions.setVisibility(true);
    }  
    this.view.segSelectedOptions.widgetDataMap=dataMap;
    this.view.segSelectedOptions.setData(data);
    this.view.btnRemoveAll.setVisibility(this.view.segSelectedOptions.data.length!==0);
    this.showHidePlaceHolder();
    this.view.forceLayout();
  },



  onClickOptions:function()
  {
    var selItems = this.view.segPermissions.selectedItems[0];
    //  this.roleId = id;
    //  this.statusId=statusId;
    this.view.flxRoleStatusFilter.setVisibility(false);
    var clckd_selectedRowIndex = this.view.segPermissions.selectedRowIndex[1];
    kony.print("clckd_selectedRowIndex----"+JSON.stringify(clckd_selectedRowIndex));    

    kony.print("selItems roles- skin---"+JSON.stringify(selItems));   


    this.gblselIndex = this.view.segPermissions.selectedIndex[1];

    if(selItems.lblRoleStatus.text === kony.i18n.getLocalizedString("i18n.secureimage.Active")){

      //this.view.flxSelectOptions.setVisibility(true);  

      if(this.view.flxSelectOptions.isVisible === true)
      {

        kony.print("this.gblsegRoles-- after setting the value-  if visible true-"+this.gblselIndex);

        kony.print("this.clckd_selectedRowIndex-- after setting the value-  if visible true-"+clckd_selectedRowIndex);

        if(clckd_selectedRowIndex === this.gblselIndex)
        {

          this.view.segPermissions.setDataAt({
            "lblIconImgOptions": selItems.lblIconImgOptions,
            "fontIconStatusImg": selItems.fontIconStatusImg,
            "lblDescription": selItems.lblDescription,
            "lblNoOfUsers": selItems.lblNoOfUsers,
            "lblPermissions": selItems.lblPermissions,
            "lblRoleName": selItems.lblRoleName,
            "lblRoleStatus": selItems.lblRoleStatus,
            "lblSeperator": selItems.lblSeperator,
            //"lblValidTillDate": selItems.lblValidTillDate,
            "template":"flxRoles"
          },this.gblselIndex);

          this.view.btnRoles.text = kony.i18n.getLocalizedString("i18n.leftmenu.Users");
          this.view.btnRoles.setVisibility(true);
          this.view.btnPermissions.text = kony.i18n.getLocalizedString("i18n.users.Permissions");
          this.view.btnPermissions.setVisibility(true);
          this.view.lblDescription.setVisibility(true);
          this.view.flxSeperator.setVisibility(true);
          this.view.lblOption2.text = kony.i18n.getLocalizedString("i18n.roles.Edit");
          this.view.lblOption4.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate");
          this.view.fontIconDeactivate.text = "";
          this.view.flxSelectOptions.isVisible = false;  //flxTranspSknNormal
          this.view.forceLayout();


        }


      }
      else if(this.view.flxSelectOptions.isVisible === false)
      {


        this.gblsegRoles = clckd_selectedRowIndex;
        var hgtValue = (((clckd_selectedRowIndex + 1) * 50)+65);
        this.view.flxSelectOptions.top = this.mouseYCoordinate-148 +"px";

        this.view.segPermissions.setDataAt({
          "lblIconImgOptions":  selItems.lblIconImgOptions,
          "fontIconStatusImg": selItems.fontIconStatusImg,
          "lblDescription": selItems.lblDescription,
          "lblNoOfUsers": selItems.lblNoOfUsers,
          "lblPermissions": selItems.lblPermissions,
          "lblRoleName": selItems.lblRoleName,
          "lblRoleStatus": selItems.lblRoleStatus,
          "lblSeperator": selItems.lblSeperator,
          //"lblValidTillDate": selItems.lblValidTillDate,
          "template":"flxRoles"
        },this.gblselIndex);


        this.view.btnRoles.text = kony.i18n.getLocalizedString("i18n.leftmenu.Users");
        this.view.btnRoles.setVisibility(true);
        this.view.btnPermissions.text = kony.i18n.getLocalizedString("i18n.users.Permissions");
        this.view.btnPermissions.setVisibility(true);
        this.view.lblDescription.setVisibility(true);
        this.view.flxSeperator.setVisibility(true);
        this.view.lblOption2.text = kony.i18n.getLocalizedString("i18n.roles.Edit");
        this.view.lblOption4.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate");
        this.view.fontIconDeactivate.text = "";





        this.view.flxSelectOptions.isVisible = true;
        this.view.forceLayout();
        this.fixContextualMenu(this.mouseYCoordinate-148);
      }


    }else{



      if(this.view.flxSelectOptions.isVisible === true)
      {

        if(clckd_selectedRowIndex === this.gblsegRoles)
        {
          this.view.flxSelectOptions.isVisible = false;  //flxTranspSknNormal
          this.view.forceLayout();



          this.view.segPermissions.setDataAt({
            "lblIconImgOptions":  selItems.lblIconImgOptions,
            "fontIconStatusImg": selItems.fontIconStatusImg,
            "lblDescription": selItems.lblDescription,
            "lblNoOfUsers": selItems.lblNoOfUsers,
            "lblPermissions": selItems.lblPermissions,
            "lblRoleName": selItems.lblRoleName,
            "lblRoleStatus": selItems.lblRoleStatus,
            "lblSeperator": selItems.lblSeperator,
            //"lblValidTillDate": selItems.lblValidTillDate,
            "template":"flxRoles"
          },this.gblselIndex);


          this.view.btnRoles.text = kony.i18n.getLocalizedString("i18n.leftmenu.Users");
          this.view.btnRoles.setVisibility(false);
          this.view.btnPermissions.text = kony.i18n.getLocalizedString("i18n.users.Permissions");
          this.view.btnPermissions.setVisibility(false);
          this.view.lblDescription.setVisibility(false);
          this.view.flxSeperator.setVisibility(false);
          this.view.lblOption2.text = kony.i18n.getLocalizedString("i18n.roles.Edit");
          this.view.lblOption4.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Activate");
          this.view.fontIconDeactivate.text = "";


        }                  
      }
      else if(this.view.flxSelectOptions.isVisible === false)
      {

        this.gblsegRoles = clckd_selectedRowIndex;
        var hgtValue = (((clckd_selectedRowIndex + 1) * 50)+65);
        kony.print("hgtValue in permissions------"+hgtValue);
        this.view.flxSelectOptions.top = this.mouseYCoordinate-148+"px";

        this.view.segPermissions.setDataAt({
          "lblIconImgOptions":  selItems.lblIconImgOptions,
          "fontIconStatusImg": selItems.fontIconStatusImg,
          "lblDescription": selItems.lblDescription,
          "lblNoOfUsers": selItems.lblNoOfUsers,
          "lblPermissions": selItems.lblPermissions,
          "lblRoleName": selItems.lblRoleName,
          "lblRoleStatus": selItems.lblRoleStatus,
          "lblSeperator": selItems.lblSeperator,
          //"lblValidTillDate": selItems.lblValidTillDate,
          "template":"flxRoles"
        },this.gblselIndex);

        this.view.btnRoles.text = kony.i18n.getLocalizedString("i18n.leftmenu.Users");
        this.view.btnRoles.setVisibility(false);
        this.view.btnPermissions.text = kony.i18n.getLocalizedString("i18n.users.Permissions");
        this.view.btnPermissions.setVisibility(false);
        this.view.lblDescription.setVisibility(false);
        this.view.flxSeperator.setVisibility(false);
        this.view.lblOption2.text = kony.i18n.getLocalizedString("i18n.roles.Edit");
        this.view.lblOption4.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Activate");
        this.view.fontIconDeactivate.text = "";
        this.view.flxSelectOptions.isVisible = true;   
        this.view.forceLayout();
        this.fixContextualMenu(this.mouseYCoordinate-148);
      }
    }
  },
  onClickActiveDeactive:function(roleName){
    var self =this;
    if(this.view.lblOption4.text === kony.i18n.getLocalizedString("i18n.SecurityQuestions.Activate")){
      this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.frmRolesController.Yes__Activate");
      this.view.flxSelectOptions.setVisibility(false);
      this.DeactivatePermission();
    }
    else  if(this.view.lblOption4.text === kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate")){
      this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmRolesController.Deactivate_Role");
      this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmRolesController.deactivate_Role_popup")+roleName+kony.i18n.getLocalizedString("i18n.frmRolesController.deactivate_Role_popupContent");
      this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesDeactivate");
      this.view.flxDeactivatePermission.setVisibility(true);
      this.view.flxSelectOptions.setVisibility(false);
    }
    this.view.popUpDeactivate.btnPopUpDelete.onClick= function(){
      self.DeactivatePermission();
    };
  },

  DeactivatePermission:function(){
    var user_id=kony.mvc.MDAApplication.getSharedInstance().appContext.userID;
    if(this.view.popUpDeactivate.btnPopUpDelete.text === kony.i18n.getLocalizedString("i18n.frmGroupsController.Yes__Deactivate"))
    {
      this.view.flxDeactivatePermission.setVisibility(false);
      kony.adminConsole.utils.showProgressBar(this.view);
      this.presenter.changeStatusOf(this.roleId, this.statusId,user_id);
      //this.view.toastMessage.showToastMessage(kony.i18n.getLocalizedString("i18n.frmRolesController.Role_successfully_deactivated"),this);
    }
    else
    {
      this.view.flxDeactivatePermission.setVisibility(false);
      kony.adminConsole.utils.showProgressBar(this.view);
      this.presenter.changeStatusOf(this.roleId, this.statusId,user_id);
      //this.view.toastMessage.showToastMessage(kony.i18n.getLocalizedString("i18n.frmRolesController.Role_successfully_activated"),this);
    }

  },
  callBackTimer : function() 
  {
    kony.timer.cancel("mytimer");
    this.view.flxToastMessage.setVisibility(false);
  },
  createNewUserData: function(data){
    this.activeUsers=data.map(this.newUserData);
  },
  newUserData: function(userData){
    return{
      "btnAdd": kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add"),
      "lblFullName": userData.fullName,
      "lblUserIdValue": userData.user_id,
      "lblUsername": userData.userName,
      "template":"flxAddUsers"
    };
  },
  createNewPermData:function(data){
    this.activePermissions=data.map(this.newPermData);
  },
  newPermData: function(PermData){
    return{
      "btnAdd": {
        "text":kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add"),
        "onClick":this.addPermissionstoRole
      },
      "lblPermissionsName": PermData.permissionName,
      "rtxPermissionDescription": PermData.permissionDesc,
      "template":"flxAddPermissions"

    };
  },
  fetchRoleDetails: function(roleId,roleName,roleDesc,roleStatus){
    var roleObj={"roleId":roleId,"roleName":roleName,"roleDesc":roleDesc,"roleStatus":roleStatus};
    kony.adminConsole.utils.showProgressBar(this.view);
    this.presenter.fetchRoleDetails(this,roleObj);
  },
  updatedIdUsersPermissions: function(a1,a2) {
    return a1.filter(function(x) {
      if(a2.indexOf(x) >= 0) return false;
      else return true;
    }); 
  },
  validateRoleName:function(){
    var returnValue;
    if(this.view.tbxRoleNameValue.text.trim()==="")
    {
      this.view.lblNoRoleNameError.text=kony.i18n.getLocalizedString("i18n.frmRolesController.Role_Name_cannot_be_empty");
      this.view.tbxRoleNameValue.skin="skinredbg";
      this.view.flxNoRoleNameError.setVisibility(true);
      return false;
    }
    else if(this.view.tbxRoleNameValue.text.trim().length<5)
    {
      this.view.tbxRoleNameValue.skin="skinredbg";
      this.view.flxNoRoleNameError.setVisibility(true);
      this.view.lblNoRoleNameError.text=kony.i18n.getLocalizedString("i18n.frmRolesController.Role_Name_Min_Limit");
      return false;
    }
    else{
      this.view.tbxRoleNameValue.skin="skntbxLato35475f14px";
      this.view.flxNoRoleNameError.setVisibility(false);
      returnValue  = true;
    }
    if(this.view.txtRoleDescription.text === "" || this.view.txtRoleDescription.text.trim() ===""){
      this.view.lblNoRoleDescriptionError.text = kony.i18n.getLocalizedString("i18n.frmRolesController.Role_Description_Cannot_Be_empty");
      this.view.txtRoleDescription.skin = "skinredbg";
      this.view.flxNoRoleDescriptionError.isVisible = true;
      return false;
    }else if(this.view.txtRoleDescription.text.trim().length<5){
      this.view.lblNoRoleDescriptionError.text = kony.i18n.getLocalizedString("i18n.frmRolesController.Role_Description_min_limit");
      this.view.txtRoleDescription.skin = "skinredbg";
      this.view.flxNoRoleDescriptionError.isVisible = true;
      return false;
    }else if(this.view.txtRoleDescription.text.trim().length>300){
      this.view.lblNoRoleDescriptionError.text = kony.i18n.getLocalizedString("i18n.frmRolesController.Role_Description_max_limit");
      this.view.txtRoleDescription.skin = "skinredbg";
      this.view.flxNoRoleDescriptionError.isVisible = true;
      return false;
    }
    else{
      this.view.txtRoleDescription.skin="skntbxLato35475f14px";
      this.view.flxNoRoleDescriptionError.setVisibility(false);
      returnValue  = true;
    }
    return returnValue;
  },
  errorRoleName: function(){
    this.tabUtilVerticleArrowVisibilityFunction(this.selectedArrowArray,this.view.fontIconImgSelected1);
    this.view.flxAddPermissionDetails.setVisibility(false);
    this.view.flxAddRoleDetails.setVisibility(false);
    this.view.flxAddOptionsContainer.setVisibility(false);
    this.view.flxAddCustomerRoles.setVisibility(false);
    this.view.flxAddMainContainer.setVisibility(true);
    var widgetArray = [this.view.btnAddPermissions,this.view.btnAddUsers,this.view.btnOptionDetails];
    this.tabUtilVerticleButtonFunction(widgetArray,this.view.btnOptionDetails);
    this.view.flxAddRoleDetails.setVisibility(true);
    this.view.flxValidity.setVisibility(false);
    this.view.forceLayout(); 
  },
  selectAllRecords: function(){
    var self = this;
    var toAdd;
    var availableRecords = this.view.segAddOptions.data;
    var data = this.view.segSelectedOptions.data;
    this.view.btnRemoveAll.setVisibility(true);
    this.view.rtxSelectedOptionsMessage.setVisibility(false);

    for (var i = 0; i < availableRecords.length; i++) {
      if(availableRecords[i].lblPermissionsName){
        self.view.rtxAvailableOptionsMessage.text=kony.i18n.getLocalizedString("i18n.frmPermissionsController.No_Permissions_Available");
        toAdd = {
          "fontIconClose":{"text":"","tooltip":kony.i18n.getLocalizedString("i18n.frmRolesController.remove_permission")}, 
          "lblOption": "" + availableRecords[i].lblPermissionsName,
          "permissionId": availableRecords[i].permissionId,
          "sourceData": availableRecords[i],
          "flxClose":{"isVisible":false,"onClick":function(){self.showRemovePermissionPopup(1);}},
          "flxAddOptionWrapper":{
            "onHover":self.onHoverEventCallback

          }
        };
      }
      else{
        self.view.rtxAvailableOptionsMessage.text=kony.i18n.getLocalizedString("i18n.frmPermissionsController.No_Users_Available");
        toAdd = {
          "fontIconClose":{"text":"","tooltip":kony.i18n.getLocalizedString("i18n.frmRolesController.remove_user")},
          "lblOption": availableRecords[i].lblFullName,
          "userId": "" + availableRecords[i].userId,
          "sourceData": availableRecords[i],
          "flxClose":{"isVisible":false,"onClick":self.unSelectedOption},
          "flxAddOptionWrapper":{
            "onHover":self.onHoverEventCallback

          }
        };
      }
      data.push(toAdd);
    }
    if(availableRecords[0].lblPermissionsName){
      this.rolePermissions = data;
    }else{
      this.roleUsers = data;
    }
    this.view.segAddOptions.removeAll();
    this.view.btnAddAll.setVisibility(false);
    if(data.length > 0){
      this.view.segSelectedOptions.isVisible = true;
    }
    this.view.segSelectedOptions.setData(data);
    this.view.rtxAvailableOptionsMessage.setVisibility(true);
    this.view.forceLayout();
  },
  unselectAllRecords: function(){
    var self = this;
    var toAddData;
    var selRecords = this.view.segSelectedOptions.data;
    var  availableRecords = this.view.segAddOptions.data;
    this.view.btnAddAll.setVisibility(true);
    this.view.segAddOptions.setVisibility(true);
    for(var i=0; i<selRecords.length;i++ )
    {
      if(selRecords[i].sourceData.lblFullName){
        toAddData = {
          "btnAdd": kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add"),
          "lblFullName": selRecords[i].sourceData.lblFullName,
          "lblUserIdValue": selRecords[i].sourceData.lblUserIdValue,
          "lblUsername": selRecords[i].sourceData.lblUsername,
          "template":"flxAddUsers",
          "userId":selRecords[i].sourceData.UserId
        };
      }
      else{
        toAddData={
          "btnAdd": {
            "text":kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add"),
            "onClick":self.addPermissionstoRole
          },
          "lblPermissionsName":  selRecords[i].sourceData.lblPermissionsName,
          "rtxPermissionDescription": selRecords[i].sourceData.rtxPermissionDescription,
          "template":"flxAddPermissions",
          "permissionId": selRecords[i].sourceData.permissionId
        };
      }
      availableRecords.push(toAddData);
    }
    if(selRecords[0].sourceData.lblFullName){
      this.roleUsers = [];
    }else{
      this.rolePermissions = [];
    }
    this.view.segAddOptions.setData(availableRecords);
    this.view.rtxAvailableOptionsMessage.setVisibility(availableRecords.length === 0);
    this.view.flxClearSearch.onClick();
    //remove data from right
    this.view.segSelectedOptions.removeAll();
    this.view.btnRemoveAll.setVisibility(false);
    this.view.rtxSelectedOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmPermissionsController.ADD_to_select_the_item");
    this.view.rtxSelectedOptionsMessage.setVisibility(true);
  },
  onHoverEventCallback:function(widget,context){
    var self=this;
    var rowData = self.view.segSelectedOptions.data[context.rowIndex];
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      //rowData.flxAddOptionWrapper.skin = "sknFlxSegRowHover11abeb"; 
      if(rowData.flxClose.isVisible===false){ 
        rowData.flxClose.isVisible = true;
        if(rowData.permissionId){ //in case of permission show popup
          rowData.flxClose.onClick = function(){self.showRemovePermissionPopup(1);};
        }else{
          rowData.flxClose.onClick = self.unSelectedOption;
        }
        self.view.segSelectedOptions.setDataAt(rowData, context.rowIndex, context.sectionIndex);
      }
    }  else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      // rowData.flxAddOptionWrapper.skin = "sknflxffffffop0e"; 
      rowData.flxClose.isVisible = false;
      rowData.flxClose.onClick = {};
      self.view.segSelectedOptions.setDataAt(rowData, context.rowIndex, context.sectionIndex);
    }
    for(var i=0;i<self.view.segSelectedOptions.data.length;i++){
      rowData=self.view.segSelectedOptions.data[i];
      if(i!==context.rowIndex){ 
        rowData.flxClose.isVisible = false;
        rowData.flxClose.onClick = {};
        self.view.segSelectedOptions.setDataAt(rowData, i , i);
      }
    }
    self.view.forceLayout();

  },
  performStatusFilter: function () {
    var self = this;
    var selStatus = [];
    var selInd;
    var dataToShow = [];
    var allData = self.roleData;
    var segStatusData = self.view.statusFilterMenu.segStatusFilterDropdown.data;
    var indices = self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices;
    if (indices !== null) { //show selected indices data
      selInd = indices[0][1];
      var statuses = "";
      for(var i=0;i<selInd.length;i++){
        selStatus.push(self.view.statusFilterMenu.segStatusFilterDropdown.data[selInd[i]].lblDescription);
        statuses = statuses + self.view.statusFilterMenu.segStatusFilterDropdown.data[selInd[i]].lblDescription + "_";
      }
      if(self.view.mainHeader.btnDropdownList.info === undefined) {
        self.view.mainHeader.btnDropdownList.info = {};
      }
      self.view.mainHeader.btnDropdownList.info.selectedStatusList = statuses.substring(0, statuses.length-1);

      self.view.flxNoResultFound.setVisibility(false);
      self.view.flxPermissionsContainer.height="100%";
      self.view.flxRolesHeader.setVisibility(true);
      self.view.flxSegRoles.setVisibility(true);

      //self.view.flxPagination.setVisibility(true);
      if (selInd.length === segStatusData.length) { //all are selected
        self.setRolesSegmentData(self.roleData,true);
      } else {
        dataToShow = allData.filter(function(rec){
          if(selStatus.indexOf(rec.Status_Desc) >= 0){
            return rec;
          }
        });
        if (dataToShow.length > 0) {
          self.setRolesSegmentData(dataToShow,true);
        } else {
          self.view.rtxSearchMesg.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
          self.view.flxNoResultFound.setVisibility(true);
          self.view.flxNoResultFound.top="60px";
          self.view.flxPermissionsContainer.height="53%";
          self.view.flxRolesHeader.setVisibility(true);
          self.view.flxSegRoles.setVisibility(false);
          //         self.view.flxPagination.setVisibility(false);
          self.view.segPermissions.setData([]);
        }
      }
    } else {
      self.view.rtxSearchMesg.text = kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
      self.view.flxNoResultFound.setVisibility(true);
      self.view.flxNoResultFound.top="60px";
      self.view.flxPermissionsContainer.height="53%";
      self.view.flxRolesHeader.setVisibility(true);
      self.view.flxSegRoles.setVisibility(false);
      //         self.view.flxPagination.setVisibility(false);
      self.view.segPermissions.setData([]);
    }
  },
  savedEditedDataInTextBox : function(){
    var scopeObj = this;
    scopeObj.view.tbxRoleNameValue.text=scopeObj.roleDetails.lblRoleName;
    scopeObj.view.txtRoleDescription.text=scopeObj.roleDetails.lblDescription;
  },
  setDataToCSRAssistSegment: function (data) {
    var scopeObj = this;
    var widgetMap = {
      "id": "id",
      "isEnabled": "isEnabled",
      "Permission_id": "Permission_id",
      "lblIconArrow": "lblIconArrow",
      "lblName": "lblName",
      "lblLine": "lblLine",
      "flxEnableToggle": "flxEnableToggle",
      "lblEnable": "lblEnable",
      "switchToggle": "switchToggle",
      "flxEnableTick": "flxEnableTick",
      "lblIconGreenTick": "lblIconGreenTick",
      "lblEnabled": "lblEnabled",
      "flxViewConfigureDesc": "flxViewConfigureDesc",
      "lblLine2": "lblLine2",
      "lblHeadingDesc": "lblHeadingDesc",
      "rtxDescription": "rtxDescription",
    };
    var index = scopeObj.view.segViewSegment.selectedRowIndex;
    var rowData;
    if (index) {
      rowData = scopeObj.view.segViewSegment.data[index[1]];
    }
    var segData = data.map(function (record) {
      return {
        "id": record.id,
        "isEnabled": record.isEnabled,
        "Permission_id": record.Permission_id,
        "lblIconArrow": {
          "skin": "sknfontIconDescRightArrow14px",
          "text": "\ue922"
        },
        "lblName": record.Name,
        "rtxDescription": record.Description,
        "flxEnableToggle": {
          "isVisible": rowData.lblIconAction.text === "\ue952" ? true : false
        },
        "switchToggle": {
          "selectedIndex": record.isEnabled === "true" ? 0 : 1,
          "onSlide": function () {
            scopeObj.updateRoleCompositePermission();
          }
        },
        "flxEnableTick": {
          "isVisible": (rowData.lblIconAction.text === "\ue948" && record.isEnabled === "true") ? true : false
        },
        "flxViewConfigureDesc": {
          "isVisible": false
        },
        "lblLine": "-",
        "lblEnable": kony.i18n.getLocalizedString("i18n.userwidgetmodel.ViewConfigureCSR.Enable"),					
        "lblIconGreenTick": {
          "text": "\ue94f"
        },
        "lblEnabled": kony.i18n.getLocalizedString("i18n.userwidgetmodel.ViewConfigureCSR.Enabled"),	
        "lblLine2": "-",
        "lblHeadingDesc": kony.i18n.getLocalizedString("i18n.permission.DESCRIPTION"),
      };
    });
    kony.adminConsole.utils.hideProgressBar(scopeObj.view);
    scopeObj.view.viewConfigureCSRAssist.segViewConfigureCSR.widgetDataMap = widgetMap;
    scopeObj.view.viewConfigureCSRAssist.segViewConfigureCSR.setData(segData);
    scopeObj.view.forceLayout();
  },
  getActionItem: function () {
    var self = this;
    var rolePermissions = self.roleDetailsObj.rolePermissions;
    var iconValue = {"text":"\ue948", "skin":"sknEyeIcon30px"};
    var roleNames = rolePermissions.map(function (perm) {
      return (perm.Permission_Name).toLowerCase();
    });
    if (roleNames.indexOf("updateroles") >= 0) {
      iconValue = {"text":"\ue952", "skin":"sknIcon20px","tooltip":kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Configure")}; //configure-icon
    } else if (roleNames.indexOf("viewroles") >= 0) {
      iconValue = {"text":"\ue948", "skin":"sknEyeIcon30px","tooltip":kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.View")}; //view-icon
    }
    return iconValue;
  },
  navigateToConfigureViewCSRPerm: function () {
    var self = this;
    var index = self.view.segViewSegment.selectedRowIndex;
    var data;
    if (index) {
      data = self.view.segViewSegment.data[index[1]];
    }
    var inputParam = {
      "Role_id": data.Role_id,
      "Permission_id": data.Permission_id
    };
    self.view.flxViewSegmentAndHeaders.setVisibility(false);
    self.view.flxViewConfigureCsrCont.setVisibility(true);
    kony.adminConsole.utils.showProgressBar(this.view);
    self.presenter.fetchCompositePermissions(inputParam);
  },
  updateRoleCompositePermission : function(){
    var self = this;
    var inputReq ={
      "roleId":"",
      "addedCompositePermissions":[],
      "removedCompositePermissions":[]
    };
    var opt = "";
    var rowData;
    var roleSegIndex = self.view.segPermissions.selectedRowIndex;
    var index = self.view.viewConfigureCSRAssist.segViewConfigureCSR.selectedRowIndex;
    if(index && roleSegIndex){
      rowData = self.view.viewConfigureCSRAssist.segViewConfigureCSR.data[index[1]];
      var roleData = self.view.segPermissions.data[roleSegIndex[1]];
      inputReq.roleId = roleData.roleId;
      if(rowData.switchToggle.selectedIndex === 1){
        inputReq.removedCompositePermissions.push(rowData.id);
        opt = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Remove");
      }else{
        inputReq.addedCompositePermissions.push(rowData.id);
        opt = kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add");
      }
      self.presenter.updateRoleCompositePermissions(inputReq,opt);
    }
  },
  /*show add-remove customer roles screen*/
  showAddCustomerRoles : function(){
    var self = this;
    self.hideAll();
    self.hideOptions();
    self.hideMainHeaderButtons();
    self.togglePermissionSuboptions(true);
    self.view.addCustomerRolesOption.tbxSearchBox.text = "";
    self.view.addCustomerRolesOption.flxClearSearchImage.setVisibility(false);
    var widgetArray = [self.view.btnAddPermissions,self.view.btnAddUsers,self.view.btnOptionDetails];
    self.tabUtilVerticleButtonFunction(widgetArray,self.view.btnAddPermissions);
    var widgetArray2 = [self.view.btnAddSysPermissions,self.view.btnAddCustomerAccess];
    self.tabUtilVerticleButtonFunction(widgetArray2,self.view.btnAddCustomerAccess);
    self.tabUtilVerticleArrowVisibilityFunction(self.selectedArrowArray,self.view.lblIconCustomerAccessSelected);
    self.view.flxViews.setVisibility(true);
    self.hideMainSubHeader();
    self.view.flxAddMainContainer.setVisibility(true);
    self.view.flxAddCustomerRoles.setVisibility(true);
    self.view.flxRolesBreadCrumb.setVisibility(true);
    self.setAvailableCustomerRoles();
    self.setSelectedCustomerRoles();
    self.view.forceLayout();
  },
  /* map available customer roles data to segment */
  setAvailableCustomerRoles: function(){
    var widgetMap = {
       "btnAdd":"btnAdd" ,
          "lblPermissionsName": "lblPermissionsName",
          "rtxPermissionDescription": "rtxPermissionDescription",
          "flxAddPermissions":"flxAddPermissions",
          "customerRoleDescription":"customerRoleDescription",
          "customerRoleId":"customerRoleId",
          "customerRoleName":"customerRoleName",
      };
    var availableData = this.view.addCustomerRolesOption.segAddOptions.info.segData;
    var selectedData = this.view.addCustomerRolesOption.segSelectedOptions.info.segData;
    //remove already added customer roles if-any
    for(var i=0;i<selectedData.length;i++){
		for(var j=0;j<availableData.length;j++){
			if(availableData[j].customerRoleId === selectedData[i].customerRoleId){
				availableData.splice(j,1);
				break;
			}
		}
	}
    this.view.addCustomerRolesOption.segAddOptions.widgetDataMap =widgetMap; 
    this.view.addCustomerRolesOption.segAddOptions.setData(availableData);
    this.view.addCustomerRolesOption.rtxAvailableOptionsMessage.text = "No customer roles available.";
    this.showHideSegmentNoResult();
    this.view.forceLayout();
  },
   /* map assigned customer roles data to segment */
  setSelectedCustomerRoles : function(){
    var widgetMap = {
         "fontIconClose":"fontIconClose", 
         "lblOption": "lblOption",
         "flxClose":"flxClose",
         "customerRoleId":"customerRoleId",
         "customerRoleName":"customerRoleName",
         "userRoleId":"userRoleId",
         "customerRoleDescription":"customerRoleDescription",
         "flxAddOptionWrapper":"flxAddOptionWrapper"
      };
    var segData = this.view.addCustomerRolesOption.segSelectedOptions.info.segData;
    this.view.addCustomerRolesOption.segSelectedOptions.widgetDataMap = widgetMap;
    this.view.addCustomerRolesOption.segSelectedOptions.setData(segData);
    this.showHideSegmentNoResult();
    this.view.forceLayout();
  },
  /* search for customer roles in the available section */
  searchCustomerRolesList: function(){
    var segData = this.view.addCustomerRolesOption.segAddOptions.info.segData;
    var searchText = this.view.addCustomerRolesOption.tbxSearchBox.text;
    var searchResult = [];
    var searchData = function (records) {
        return records.customerRoleName.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
     };
    searchResult = segData.filter(searchData);
    if(searchResult.length > 0){
      this.view.addCustomerRolesOption.segAddOptions.setData(searchResult);
      this.view.addCustomerRolesOption.segAddOptions.setVisibility(true);
      this.view.addCustomerRolesOption.rtxAvailableOptionsMessage.setVisibility(false);
      this.view.addCustomerRolesOption.btnSelectAll.setVisibility(true);
    } else{
      this.view.addCustomerRolesOption.segAddOptions.setData([]);
      this.view.addCustomerRolesOption.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found")+"\""+searchText+
        											"\""+kony.i18n.getLocalizedString("i18n.frmPoliciesController.Try_with_another_keyword");
      this.view.addCustomerRolesOption.segAddOptions.setVisibility(false);
      this.view.addCustomerRolesOption.rtxAvailableOptionsMessage.setVisibility(true);
      this.view.addCustomerRolesOption.btnSelectAll.setVisibility(false);
    }
    this.view.forceLayout();
  },
  /* map available row data for selected row */
  mapCustomerRoleAvailableToSelected : function(selectedRowData){
    var self = this;
    return {
            "fontIconClose":{"text":"","tooltip":kony.i18n.getLocalizedString("i18n.frmRolesController.remove_permission")}, 
            "lblOption": {"text": this.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.customerRoleName, 35, 30),
                          "tooltip":selectedRowData.customerRoleName,
                         },
            "flxClose":{"isVisible":false,"onClick":function(){self.showRemoveCustomerRolePopup(1);}},
            "customerRoleId":selectedRowData.customerRoleId,
            "customerRoleName":selectedRowData.customerRoleName,
            "userRoleId":"",
            "customerRoleDescription":selectedRowData.customerRoleDescription,
            "flxAddOptionWrapper":{
              "onHover":self.showCloseOnHoverCallback
            }
    };
  },
  /* map selected row data for available row */
  mapCustomerRoleSelectedToAvailable : function(selectedRowData){
    var self = this;
    return {
          "btnAdd": {
            "text":kony.i18n.getLocalizedString("i18n.frmPermissionsController.Add"),
            "onClick": self.addSelectedCustomerRole
          },
          "lblPermissionsName": selectedRowData.customerRoleName,
          "rtxPermissionDescription": selectedRowData.customerRoleDescription,
          "template":"flxAddPermissions",
          "customerRoleDescription":selectedRowData.customerRoleDescription,
          "customerRoleId":selectedRowData.customerRoleId,
          "customerRoleName":selectedRowData.customerRoleName,
          "userRoleId":selectedRowData.userRoleId
        };
  },
  /* add customer role to selected section and remove from available section*/
  addSelectedCustomerRole : function(){
    var selectedRow = this.view.addCustomerRolesOption.segAddOptions.selectedRowIndex[1];
    var selectedRowData = this.view.addCustomerRolesOption.segAddOptions.data[selectedRow];
    var rowDataToAdd = this.mapCustomerRoleAvailableToSelected(selectedRowData);
    this.view.addCustomerRolesOption.segSelectedOptions.addDataAt(rowDataToAdd, 0);
    this.view.addCustomerRolesOption.segSelectedOptions.info.segData = this.view.addCustomerRolesOption.segSelectedOptions.data;
    this.view.addCustomerRolesOption.segAddOptions.info.segData = (this.view.addCustomerRolesOption.segAddOptions.info.segData).filter(function (record) {
      if (record.customerRoleId !== selectedRowData.customerRoleId) {
        return record;
      }
    }); 
    this.view.addCustomerRolesOption.segAddOptions.removeAt(selectedRow);
    this.view.addCustomerRolesOption.rtxAvailableOptionsMessage.text = "No customer roles available.";
    this.showHideSegmentNoResult();
    this.view.forceLayout();
  },
  /* remove selected customer role from selected section and add in available section */
  removeSelectedCustomerRole : function(){
    var selectedRow = this.view.addCustomerRolesOption.segSelectedOptions.selectedRowIndex[1];
    var selectedRowData = this.view.addCustomerRolesOption.segSelectedOptions.data[selectedRow];
    var availableData = this.view.addCustomerRolesOption.segAddOptions.info.segData;
    this.view.addCustomerRolesOption.flxClearSearchImage.onClick();
    var rowDataToAdd = this.mapCustomerRoleSelectedToAvailable(selectedRowData);
    this.view.addCustomerRolesOption.segAddOptions.addDataAt(rowDataToAdd, 0);
    availableData.push(rowDataToAdd);
    this.view.addCustomerRolesOption.segAddOptions.info.segData = availableData;
    this.view.addCustomerRolesOption.segSelectedOptions.info.segData = (this.view.addCustomerRolesOption.segSelectedOptions.info.segData).filter(function (record) {
      if (record.customerRoleId !== selectedRowData.customerRoleId) {
        return record;
      }
    });
    this.view.addCustomerRolesOption.segSelectedOptions.removeAt(selectedRow);
    this.showHideSegmentNoResult();
    
    this.view.forceLayout();
  },
  /* hide/show segments or no result found screen based on data*/
  showHideSegmentNoResult: function(){
    // for available options 
    if(this.view.addCustomerRolesOption.segAddOptions.data.length > 0){
      this.view.addCustomerRolesOption.segAddOptions.setVisibility(true);
      this.view.addCustomerRolesOption.rtxAvailableOptionsMessage.setVisibility(false);
      this.view.addCustomerRolesOption.btnSelectAll.setVisibility(true);
    } else {
      this.view.addCustomerRolesOption.segAddOptions.setVisibility(false);
      this.view.addCustomerRolesOption.rtxAvailableOptionsMessage.setVisibility(true);
      this.view.addCustomerRolesOption.btnSelectAll.setVisibility(false);
    }
    // for selected options
    if(this.view.addCustomerRolesOption.segSelectedOptions.data.length > 0){
      this.view.addCustomerRolesOption.rtxSelectedOptionsMessage.setVisibility(false);
      this.view.addCustomerRolesOption.segSelectedOptions.setVisibility(true);
      this.view.addCustomerRolesOption.btnRemoveAll.setVisibility(true);
    } else {
      this.view.addCustomerRolesOption.rtxSelectedOptionsMessage.setVisibility(true);
      this.view.addCustomerRolesOption.segSelectedOptions.setVisibility(false);
      this.view.addCustomerRolesOption.btnRemoveAll.setVisibility(false);
    } 

  },
  /* add all the customer roles shown in segment*/
  addAllCustomerRoles : function(){
    var segAvailableData = this.view.addCustomerRolesOption.segAddOptions.data;
    var segSelectedData = this.view.addCustomerRolesOption.segSelectedOptions.data;
    var rowDataToAdd = segAvailableData.map(this.mapCustomerRoleAvailableToSelected);
    var searchText = this.view.addCustomerRolesOption.tbxSearchBox.text;
    this.view.addCustomerRolesOption.segSelectedOptions.setData(segSelectedData.concat(rowDataToAdd));
    this.view.addCustomerRolesOption.segSelectedOptions.info.segData = this.view.addCustomerRolesOption.segSelectedOptions.data;
    if(segAvailableData.length === this.view.addCustomerRolesOption.segAddOptions.info.segData.length){
      this.view.addCustomerRolesOption.segAddOptions.removeAll();
      this.view.addCustomerRolesOption.segAddOptions.info.segData = [];
      this.view.addCustomerRolesOption.rtxAvailableOptionsMessage.text = "No customer roles available.";
    } else{ //in case of text present in search
      while(segAvailableData.length > 0) {
        this.view.addCustomerRolesOption.segAddOptions.info.segData = (this.view.addCustomerRolesOption.segAddOptions.info.segData).filter(function(record) {
          if (segAvailableData[0].customerRoleId !== record.customerRoleId) {
            return record;
          }
        });
        this.view.addCustomerRolesOption.segAddOptions.removeAt(0);
      }
      this.view.addCustomerRolesOption.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found")+"\""+
        searchText+"\""+kony.i18n.getLocalizedString("i18n.frmPoliciesController.Try_with_another_keyword");
    }
    this.showHideSegmentNoResult();
    this.view.forceLayout();
  },
  /* remove all customer roles from selected section*/
  removeAllcustomerRoles : function(){
    var selectedData = this.view.addCustomerRolesOption.segSelectedOptions.data;
    var availableData = this.view.addCustomerRolesOption.segAddOptions.info.segData;
    this.view.addCustomerRolesOption.flxClearSearchImage.onClick();
    var rowDataToAdd = selectedData.map(this.mapCustomerRoleSelectedToAvailable);
    this.view.addCustomerRolesOption.segAddOptions.setData(availableData.concat(rowDataToAdd));
    this.view.addCustomerRolesOption.segAddOptions.info.segData = availableData.concat(rowDataToAdd);
    this.view.addCustomerRolesOption.segSelectedOptions.removeAll();
    this.view.addCustomerRolesOption.segSelectedOptions.info.segData = [];
    this.showHideSegmentNoResult();
    this.view.forceLayout();
  },
  /* get added and removed customer roles
     @return: { "AddedRoles":[],"RemovedRoles"[] }
  */
  getUpdatedCustomerRolesList : function(){
    var orginalList = this.view.addCustomerRolesOption.segSelectedOptions.info.orgCustomerRoles;
    var modifiedList = this.view.addCustomerRolesOption.segSelectedOptions.info.segData;
    var orginalListID =[],modifiedListID =[];
    orginalListID = orginalList.map(function(rec){
      return rec.customerRoleId;
    });
    modifiedListID = modifiedList.map(function(rec){
      return rec.customerRoleId;
    });
    var addedId = this.updatedIdUsersPermissions(modifiedListID,orginalListID);
    var removedId = this.updatedIdUsersPermissions(orginalListID,modifiedListID);
    return {
      "AddedRoles":addedId,
      "RemovedRoles":removedId
    };
  },
  /* show confirmation popup on removing customer roles*/
  showRemoveCustomerRolePopup : function(context){
    var self =this;
    this.view.flxDeactivatePermission.setVisibility(true);
    this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmRoles.RevokePermissionsHeading");
    this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmRoles.revokePermissionsMsg");
    this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesProceed");
    //button proceed on click
    this.view.popUpDeactivate.btnPopUpDelete.onClick = function(){
      if(context === 1){ // remove one customer role
        self.removeSelectedCustomerRole();
      }else if(context === 2){  // remove all customer roles
        self.removeAllcustomerRoles();
      }
      self.view.flxDeactivatePermission.setVisibility(false);
    };
  },
  /* show confirmation popup on removing system permissions*/
  showRemovePermissionPopup : function(context){
    var self =this;
    this.view.flxDeactivatePermission.setVisibility(true);
    this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmRoles.RevokePermissionsHeading");
    this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmRoles.revokePermissionsMsg");
    this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesProceed");
    //button proceed on click
    this.view.popUpDeactivate.btnPopUpDelete.onClick = function(){
      self.view.flxDeactivatePermission.setVisibility(false);
      if(context === 1){ // remove one permission
        self.unSelectedOption();
      }else if(context === 2){  // remove all permissions
        self.unselectAllRecords();
      }
      
      self.view.forceLayout();
    }; 
  },
  /* show close on segment row hover for selected segment section*/
  showCloseOnHoverCallback : function(widget,context){
    var self=this;
    var rowData = self.view.addCustomerRolesOption.segSelectedOptions.data[context.rowIndex];
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      if(rowData.flxClose.isVisible === false){ 
        rowData.flxClose.isVisible = true;
        self.view.addCustomerRolesOption.segSelectedOptions.setDataAt(rowData, context.rowIndex);
      }
    } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      if(rowData.flxClose.isVisible === true){
        rowData.flxClose.isVisible = false;
        self.view.addCustomerRolesOption.segSelectedOptions.setDataAt(rowData, context.rowIndex);
      }
    }
    self.view.forceLayout();
  },
  viewCustomerRolesForARole : function(){
    this.tabUtilLabelFunction([this.view.lblTabName1,this.view.lblTabName2],this.view.lblTabName1);
    this.subTabsButtonUtilFunction([this.view.tabs.btnTab1,this.view.tabs.btnTab2],this.view.tabs.btnTab2);
    this.view.flxPermissionsHeader.setVisibility(false);
    this.view.flxUsersHeader.setVisibility(false);
    this.view.flxCustomerRoleHeader.setVisibility(true);
    this.view.mainHeader.btnAddNewOption.setVisibility(false);
    this.view.mainHeader.btnDropdownList.setVisibility(false);
    this.view.flxViewSegmentAndHeaders.setVisibility(true);
    this.view.flxViewConfigureCsrCont.setVisibility(false);
    this.custRoleSorter = this.getObjectSorter("lblCustomerRoleName");
    this.determineSortFontIcon(this.custRoleSorter,'lblCustomerRoleName',this.view.lblIconViewCustRoleNameSort);
    var widgetMap = {
      "customerRoleId":"customerRoleId",
      "flxVewCustomerRoles":"flxVewCustomerRoles",
      "lblCustomerRoleName":"lblCustomerRoleName",
      "lblCustomerRoleDescription":"lblCustomerRoleDescription",
      "lblCustomerRoleSeperator":"lblCustomerRoleSeperator"
    };
    var customerRole = this.roleDetailsObj.roleCustomerRoles;
    var segData = customerRole.map(function(record){
      return {
        "lblCustomerRoleName":record.CustomerRole_Name,
        "lblCustomerRoleDescription":record.CustomerRole_Description,
        "lblCustomerRoleSeperator":{"isVisible" : true,
                                    "text":"-"},
        "customerRoleId" :record.CustomerRole_id,
        "template":"flxVewCustomerRoles",
      };
    });
    this.view.segViewSegment.widgetDataMap = widgetMap;
    var sortedData = segData.sort(this.custRoleSorter.sortData);
    if(sortedData.length > 0)
      sortedData[0].lblCustomerRoleSeperator.isVisible=false;
    this.view.segViewSegment.setData(sortedData);
    this.view.flxCustomerRoleHeader.setVisibility(segData.length > 0);
    this.view.rtxAvailabletxt.setVisibility(segData.length <= 0);  
    this.view.rtxAvailabletxt.text =  kony.i18n.getLocalizedString("i18n.frmRoles.NoCustomerRolesAvailable");
    this.view.forceLayout();
  }
  
});
