define({
  preShowActions: function() {
    this.view.mainHeader.lblHeading.text=kony.i18n.getLocalizedString("i18n.leftmenu.businessconfigurations");
    this.view.mainHeader.flxButtons.setVisibility(false);
    this.view.flxNoConfigurations.height=kony.os.deviceInfo().screenHeight-150+"px";
    this.view.flxLeftOptions.height=kony.os.deviceInfo().screenHeight-160+"px";
    this.view.flxLeftOptions.setVisibility(false);
    this.view.flxNoConfigurations.setVisibility(false);
    this.view.flxConfigurationsList.height=kony.os.deviceInfo().screenHeight-160+"px";
    this.view.noStaticData.height=kony.os.deviceInfo().screenHeight-160+"px";
    this.view.txtRoleDescription.text="";
    this.view.flxMainHeader.setVisibility(true);
    this.view.flxScrollMainContent.setVisibility(true);
    this.view.flxToastMessage.setVisibility(false);
    this.view.SwitchToggleStatus.selectedIndex = 0;
    this.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
    this.setFlowActions();
  },
  willUpdateUI:function(viewModel){
    if(typeof viewModel.LoadingScreen!=='undefined'){
      if(viewModel.LoadingScreen.focus===true) {
        kony.adminConsole.utils.showProgressBar(this.view);
      } else {
        kony.adminConsole.utils.hideProgressBar(this.view);
      }
    }

    if (viewModel) {
      this.updateLeftMenu(viewModel);
      if(viewModel.toast){
      if(viewModel.toast.status === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SUCCESS")){
        this.view.toastMessage.showToastMessage(viewModel.toast.message,this);
      }else{
        this.view.toastMessage.showErrorToastMessage (viewModel.toast.message,this);
      }
    }
      if(viewModel.criteriaList!==undefined){
        this.view.flxLeftOptions.setVisibility(true);
    	this.view.flxNoConfigurations.setVisibility(true);
        this.criteriaData=viewModel.criteriaList;
        this.hideNoResultsFound();
        this.showConfigurationsList();
       	this.view.forceLayout();
      }
    }
  },
  showConfigurationsList: function(){
     if(this.criteriaData.length <= 0){
          this.view.noStaticData.setVisibility(true);
          this.view.flxConfigurationsList.setVisibility(false);
        }
        else{
          this.view.noStaticData.setVisibility(false);
          this.view.flxConfigurationsList.setVisibility(true);
          this.setCriteriaSegmentData(this.criteriaData,false);
        }
  },
  setFlowActions: function(){
    var scopeObj=this;
    this.view.criteriaList.flxCriteriaStatus.onClick = function(){
      scopeObj.view.flxCriteriaStatusFilter.left=scopeObj.view.criteriaList.flxCriteriaStatus.frame.x-44+"px";
      scopeObj.view.criteriaList.flxSelectOptions.setVisibility(false);
      if(scopeObj.view.flxCriteriaStatusFilter.isVisible===false)
      	scopeObj.view.flxCriteriaStatusFilter.setVisibility(true);
      else
         scopeObj.view.flxCriteriaStatusFilter.setVisibility(false);
    };
    this.view.criteriaList.flxDeactivate.onClick = function() {
      if (scopeObj.view.criteriaList.lblOption2.text === kony.i18n.getLocalizedString("i18n.SecurityQuestions.Deactivate")) scopeObj.showDeactivate();
      else scopeObj.showActivate();
    };
    this.view.criteriaList.flxDelete.onClick = function() {
      scopeObj.showDeleteCriteria();
    };
    this.view.criteriaList.flxEdit.onClick = function() {
       scopeObj.isEdit = true;
       scopeObj.editCriteria();
    };
    this.view.popUpDeactivate.btnPopUpCancel.onClick = function() {
      scopeObj.view.flxDeactivateCriteria.setVisibility(false);
      scopeObj.view.criteriaList.flxSelectOptions.setVisibility(false);
    };
    this.view.popUpDeactivate.btnPopUpDelete.onClick = function() {
      scopeObj.deactivateCriteria();
      scopeObj.view.flxDeactivateCriteria.setVisibility(false);
      scopeObj.view.criteriaList.flxSelectOptions.setVisibility(false);
    };
    this.view.popUp.btnPopUpCancel.onClick = function() {
      scopeObj.view.flxDeleteCriteria.setVisibility(false);
      scopeObj.view.criteriaList.flxSelectOptions.setVisibility(false);
    };
    this.view.popUpDeactivate.flxPopUpClose.onClick = function() {
      scopeObj.view.flxDeactivateCriteria.setVisibility(false);
      scopeObj.view.criteriaList.flxSelectOptions.setVisibility(false);
    };
    this.view.popUp.flxPopUpClose.onClick = function() {
      scopeObj.view.flxDeleteCriteria.setVisibility(false);
      scopeObj.view.criteriaList.flxSelectOptions.setVisibility(false);
    };
    this.view.popUp.btnPopUpDelete.onClick = function() {
      scopeObj.view.flxDeleteCriteria.setVisibility(false);
      scopeObj.view.criteriaList.flxSelectOptions.setVisibility(false);
      scopeObj.deleteCriteria();
    };
    this.view.statusFilterMenu.segStatusFilterDropdown.onRowClick=function(){
      scopeObj.performStatusFilter();
    };
    this.view.noStaticData.btnAddStaticContent.onClick = function() {
      scopeObj.isEdit = false;
      scopeObj.view.SwitchToggleStatus.selectedIndex = 0;
      scopeObj.view.flxNoCriteriaError.isVisible = false;
      scopeObj.view.lblAddCriteria.text=kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.addNewCriteria");
      scopeObj.view.flxAddEligibityCriteria.setVisibility(true);
      scopeObj.view.lblRoleDescriptionSize.isVisible = false;
       scopeObj.view.lblRoleDescriptionSize.text=scopeObj.view.txtRoleDescription.text.trim().length+"/200";
    };
    this.view.flxAddButton.onClick = function(){
      scopeObj.isEdit = false;
      scopeObj.view.flxNoCriteriaError.isVisible = false;
      scopeObj.view.txtRoleDescription.text="";
      scopeObj.view.lblAddCriteria.text=kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.addNewCriteria");
      scopeObj.view.SwitchToggleStatus.selectedIndex = 0;
      scopeObj.view.flxAddEligibityCriteria.setVisibility(true);
      scopeObj.view.criteriaList.flxSelectOptions.setVisibility(false);
      scopeObj.view.lblRoleDescriptionSize.isVisible = false;
       scopeObj.view.lblRoleDescriptionSize.text=scopeObj.view.txtRoleDescription.text.trim().length+"/200";
    };
    this.view.flxEligibilityClose.onClick = function(){
      scopeObj.view.flxAddEligibityCriteria.setVisibility(false);
      if(scopeObj.view.noStaticData.isVisible===true)
        scopeObj.view.flxConfigurationsList.setVisibility(false);
      else
        scopeObj.view.flxConfigurationsList.setVisibility(true);
    };
    this.view.btnCancel.onClick = function(){
      scopeObj.view.flxEligibilityClose.onClick();
    };
    this.view.btnsave.onClick = function() {
      var context, statusId;
      if(scopeObj.view.txtRoleDescription.text.trim().length===0){
        scopeObj.view.lblNoCriteriaError.text =  kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.criteriaCannotBeBlank");
        scopeObj.view.flxNoCriteriaError.isVisible = true;
      }
      else if(scopeObj.view.txtRoleDescription.text.trim().length < 5){
        scopeObj.view.lblNoCriteriaError.text =  kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.CriteriaMinDescriptionLength");
        scopeObj.view.flxNoCriteriaError.isVisible = true;
      }
      else if(scopeObj.isEdit === false){
        scopeObj.view.flxNoCriteriaError.isVisible = false;
      scopeObj.view.flxAddEligibityCriteria.setVisibility(false);
      scopeObj.view.noStaticData.setVisibility(false);
      scopeObj.view.flxConfigurationsList.setVisibility(true);
      scopeObj.view.criteriaList.flxSelectOptions.setVisibility(false);
      statusId = scopeObj.view.SwitchToggleStatus.selectedIndex === 1?"SID_INACTIVE":"SID_ACTIVE";
      context = {
        "description":scopeObj.view.txtRoleDescription.text,
        "status_id":statusId};
      scopeObj.presenter.addEligibilityCriteria(context);
      } 
      else{
         var selItems = scopeObj.view.criteriaList.segCriteria.selectedRowIndex[1];
         scopeObj.view.flxNoCriteriaError.isVisible = false;
      scopeObj.view.flxAddEligibityCriteria.setVisibility(false);
      scopeObj.view.criteriaList.flxSelectOptions.setVisibility(false);
      statusId = scopeObj.view.SwitchToggleStatus.selectedIndex === 1?"SID_INACTIVE":"SID_ACTIVE";
        context = {
          "criteriaID":scopeObj.view.criteriaList.segCriteria.data[selItems].lblCriteriaDesc.info,
          "description":scopeObj.view.txtRoleDescription.text,
          "status_id":statusId
        };
        scopeObj.presenter.updateEligibilityCriteria(context);
      }
    };
     this.view.txtRoleDescription.onKeyUp=function(){
      if(scopeObj.view.txtRoleDescription.text.trim().length===0)
      {
        scopeObj.view.lblRoleDescriptionSize.setVisibility(false);
      }
      else
      {
        scopeObj.view.lblRoleDescriptionSize.text=scopeObj.view.txtRoleDescription.text.trim().length+"/200";
        scopeObj.view.lblRoleDescriptionSize.setVisibility(true);
      }
      scopeObj.view.forceLayout();
    };
    this.view.configLeftMenu.flxHeader1.onTouchStart = function(){
      scopeObj.presenter.fetchEligibilityCriteria();
    };
    this.view.flxCriteriaStatusFilter.onHover = scopeObj.onHoverEventCallback;
  },
  onHoverEventCallback:function(widget, context) {
    var scopeObj = this;
    var widGetId = widget.id;
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER||context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      scopeObj.view[widGetId].setVisibility(true);
    } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      scopeObj.view[widGetId].setVisibility(false);
    }
  },

  showDeactivate: function(opt) {
    this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.Deactivate_Criteria");
    this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.deactivate_Criteria_MessageContent");
    this.view.popUpDeactivate.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.NO__LEAVE_AS_IT_IS");
    this.view.popUpDeactivate.btnPopUpCancel.right=180+"px";
    this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesDeactivate");
    this.view.flxDeactivateCriteria.setVisibility(true);
    this.view.forceLayout();
  },
  showActivate: function(opt) {
    this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.Activate_Criteria");
    this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.Are_you_sure_to_Activate_Criteria");
    this.view.popUpDeactivate.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.NO__LEAVE_AS_IT_IS");
    this.view.popUpDeactivate.btnPopUpCancel.right=160+"px";
    this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.frmPermissionsController.YES__ACTIVATE");
    this.view.flxDeactivateCriteria.setVisibility(true);

    this.view.forceLayout();
  },
  showDeleteCriteria: function(opt) {
    this.view.popUp.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.Delete_Criteria");
    this.view.popUp.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.Are_you_sure_to_delete_Criteria");
    this.view.popUp.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.NO__LEAVE_AS_IT_IS");
    this.view.popUp.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesDelete");
    this.view.toastMessage.lbltoastMessage.text = kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.Delete_Criteria_successfully");
    this.view.flxDeleteCriteria.setVisibility(true);
    this.view.forceLayout();
  },
  deleteCriteria: function(){
    var selIndex = this.view.criteriaList.segCriteria.selectedRowIndex[1];
    this.presenter.deleteEligibilityCriteria(this.view.criteriaList.segCriteria.data[selIndex].lblCriteriaDesc.info);
  },
  deactivateCriteria: function(){
    var selIndex = this.view.criteriaList.segCriteria.selectedRowIndex[1];
    var criteriaStatusData={"criteriaID":this.view.criteriaList.segCriteria.data[selIndex].lblCriteriaDesc.info};
    if(this.view.criteriaList.segCriteria.data[selIndex].lblCriteriaStatus.text===kony.i18n.getLocalizedString("i18n.secureimage.Active"))
      criteriaStatusData.status_id="SID_INACTIVE";
    else
      criteriaStatusData.status_id="SID_ACTIVE";
    this.presenter.updateEligibilityCriteriaStatus(criteriaStatusData);
  },
  editCriteria: function(){
    this.view.lblAddCriteria.text=kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.editCriteria");
    var selItems = this.view.criteriaList.segCriteria.selectedRowIndex[1];
    this.view.txtRoleDescription.text=this.view.criteriaList.segCriteria.data[selItems].lblCriteriaDesc.text;
    if(this.view.criteriaList.segCriteria.data[selItems].lblCriteriaStatus.text===kony.i18n.getLocalizedString("i18n.secureimage.Active"))
      this.view.SwitchToggleStatus.selectedIndex = 0;
    else
      this.view.SwitchToggleStatus.selectedIndex = 1;
    this.view.txtRoleDescription.onKeyUp();
    this.view.flxAddEligibityCriteria.setVisibility(true);
  },
  showNoResultsFound: function() {
    this.view.flxLeftOptions.height=240+"px";
    this.view.flxConfigurationsList.height=240+"px";
    this.view.criteriaList.setVisibility(false);
    this.view.rtxNoRecords.text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found") ;
    this.view.rtxNoRecords.setVisibility(true);
    this.view.flxNoRecordsFound.setVisibility(true);
  },
  hideNoResultsFound: function() {
    this.view.flxLeftOptions.height=kony.os.deviceInfo().screenHeight-160+"px";
    this.view.flxConfigurationsList.height=kony.os.deviceInfo().screenHeight-160+"px";
    this.view.criteriaList.setVisibility(true);
    this.view.rtxNoRecords.setVisibility(false);
    this.view.flxNoRecordsFound.setVisibility(false);
  },
  setCriteriaSegmentData:function(resData,isFilter){
    var self = this;
    var data = [];
    this.view.criteriaList.lblCriteriaDesc.text = kony.i18n.getLocalizedString("i18n.frmBusinessConfigurations.eligibilityCriterias");
    this.view.criteriaList.lblStatus.text = kony.i18n.getLocalizedString("i18n.roles.STATUS");
    var dataMap = {
      "flxOptions":"flxOptions",
        "lblOptions": "lblOptions",
        "fonticonActive":"fonticonActive",
        "flxStatus": "flxStatus",
        "lblCriteriaDesc": "lblCriteriaDesc",
        "lblCriteriaStatus": "lblCriteriaStatus",
        "lblSeparator": "lblSeparator",
    };
    if (resData) {
    data = resData.map(self.toCriteriaSegment.bind(self));
    }
    else if(resData==[])
      resData.map(this.toCriteriaSegment.bind(self));
    var statusFilter=[];
    this.allCriteriasList=data;
    for(var i=0;i<data.length;i++){
      if(!statusFilter.contains(data[i].lblCriteriaStatus.text))
        statusFilter.push(data[i].lblCriteriaStatus.text);
    }
    if(!isFilter){
      self.setCriteriaStatusFilterData(statusFilter);
    }
    this.view.criteriaList.segCriteria.widgetDataMap = dataMap;
    this.view.criteriaList.segCriteria.setData(data);
    this.view.flxConfigurationsList.setVisibility(true);
    this.view.forceLayout();
  },
  toCriteriaSegment: function(criteriaData) {
    var statusText = "",
      statusSkin = "",
      statusImgSkin = "";
    if (criteriaData.Status_id === "SID_ACTIVE") {
      statusText = kony.i18n.getLocalizedString("i18n.secureimage.Active");
      statusSkin = "sknlblLato5bc06cBold14px";
      statusImgSkin = "sknFontIconActivate";
    } else if (criteriaData.Status_id === "SID_INACTIVE") {
      statusText = kony.i18n.getLocalizedString("i18n.secureimage.Inactive");
      statusSkin = "sknlblLatocacacaBold12px";
      statusImgSkin = "sknfontIconInactive";
    }
    return {
      lblOptions: {
        "text":"\ue91f",
        "skin": "sknFontIconOptionMenu"
      },
      fonticonActive: {
        "skin":statusImgSkin
      },
      lblCriteriaDesc: {"text":criteriaData.Description,"info":criteriaData.id},
      lblSeparator: ".",
      lblCriteriaStatus: {
        text: statusText,
        skin: statusSkin
      },
      template: "flxCriterias"
    };
  },
  setCriteriaStatusFilterData:function(data){
    var self=this;
    var widgetMap = {
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var filterData = data.map(function(data){
      return{
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "lblDescription": data,
        "imgCheckBox":{
          "src":"checkbox.png"
        }
      };
    });
    self.view.statusFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
    self.view.statusFilterMenu.segStatusFilterDropdown.setData(filterData);
    var indices = [];
    for(var index = 0; index < filterData.length; index++){
      indices.push(index);
    }
    self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices = [[0,indices]];
  },
  performStatusFilter: function () {
    var self = this;
    var selStatus = [];
    var selInd;
    var dataToShow = [];
    var allData = self.criteriaData;
    var segStatusData = self.view.statusFilterMenu.segStatusFilterDropdown.data;
    var indices = self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices;
    if (indices !== null) { //show selected indices data
      selInd = indices[0][1];
      for(var i=0;i<selInd.length;i++){
        selStatus.push(self.view.statusFilterMenu.segStatusFilterDropdown.data[selInd[i]].lblDescription);
      }
      if (selInd.length === segStatusData.length) { //all are selected
        self.inFilter=false;
        self.hideNoResultsFound();
        self.setCriteriaSegmentData(self.criteriaData,true);
      } else {
        dataToShow = allData.filter(function(rec){
          if(selStatus.indexOf(rec.Status_id==="SID_ACTIVE"?
                                kony.i18n.getLocalizedString("i18n.secureimage.Active"):
                                kony.i18n.getLocalizedString("i18n.secureimage.Inactive")) >=0 ){
            return rec;
          }
        });
        if (dataToShow.length > 0) {
          self.hideNoResultsFound();
          self.setCriteriaSegmentData(dataToShow,true);
        } else {
          self.view.criteriaList.segCriteria.setData([]);
        }
      }
    }
    else {
      self.view.rtxNoRecords.text=kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
      self.view.rtxNoRecords.setVisibility(true);
      self.view.flxNoRecordsFound.setVisibility(true);
      var data=[];
      self.view.criteriaList.segCriteria.setData(data);
    }
    self.view.forceLayout();
  },

 });
