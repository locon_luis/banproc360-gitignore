define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxAccordianContainer **/
    AS_FlexContainer_id7174e469c340a9babab1502d7f39d3: function AS_FlexContainer_id7174e469c340a9babab1502d7f39d3(eventobject, context) {
        var self = this;
        this.executeOnParent("showSelectedRow");
    },
    /** onClick defined for flxOptions **/
    AS_FlexContainer_e3d80f752794438ba79bb85da5e279ee: function AS_FlexContainer_e3d80f752794438ba79bb85da5e279ee(eventobject, context) {
        var self = this;
        this.executeOnParent("toggleVisibility");
    },
    /** onClick defined for flxSegMFAScenarioSelected **/
    AS_FlexContainer_b45d3862195a422bb5415a98e861261f: function AS_FlexContainer_b45d3862195a422bb5415a98e861261f(eventobject, context) {
        var self = this;
        this.executeOnParent("showSelectedRow");
    }
});