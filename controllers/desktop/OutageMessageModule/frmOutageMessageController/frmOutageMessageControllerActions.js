define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmOutageMessage **/
    AS_Form_f99c5cffa0864cb8a65d17ad1e600555: function AS_Form_f99c5cffa0864cb8a65d17ad1e600555(eventobject) {
        var self = this;
        this.preShowActions();
    },
    /** onDeviceBack defined for frmOutageMessage **/
    AS_Form_fcb2e4c3e55a4a96bf1354b6caf7e017: function AS_Form_fcb2e4c3e55a4a96bf1354b6caf7e017(eventobject) {
        var self = this;
        //registered
        this.onBrowserBack();
    }
});