define({
  willUpdateUI: function (context) {
    if (context) {
      this.updateLeftMenu(context);

      if (context.LoadingScreen) {
        if (context.LoadingScreen.focus) {
          kony.adminConsole.utils.showProgressBar(this.view);
        } else {
          kony.adminConsole.utils.hideProgressBar(this.view);
        }
      } else if (context.toastModel) {
        if (context.toastModel.status === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SUCCESS")) {
          this.view.toastMessage.showToastMessage(context.toastModel.message, this);
        } else {
          this.view.toastMessage.showErrorToastMessage(context.toastModel.message, this);
        }
      } else if (context.CustomerBasicInfo) {
        this.view.flxGeneralInfoWrapper.setBasicInformation(context.CustomerBasicInfo, this);

      } else if (context.CustomerGroups) {
        this.view.tabs.setSkinForInfoTabs(this.view.tabs.btnTabName4);
        this.unloadOnScrollEvent();
        this.resetAllSortImagesGroups();
        if (context.CustomerGroups.target === "InfoScreen") {
          this.setDataForGroupsSegment(context.CustomerGroups.AssignedGroups);
        } else {
          this.setDataForGroupsEditScreen(context.CustomerGroups);
        }

      } else if (context.UpdateDBPUserStatus) {
        this.view.flxGeneralInfoWrapper.setLockStatus(context.UpdateDBPUserStatus.status.toUpperCase(), this);

      } else if (context.enrollACustomer) {
        this.view.flxGeneralInfoWrapper.setEnrollmentAccessandStatus();

      } else if (context.StatusGroup) {
        this.view.flxGeneralInfoWrapper.processAndFillStatusForEdit(context.StatusGroup, this);

      } else if (context.CustomerNotes) {
        this.view.Notes.displayNotes(this, context.CustomerNotes);

      } else if (context.OnlineBankingLogin) {
        this.view.CSRAssist.onlineBankingLogin(context.OnlineBankingLogin, this);

      }
    }
  },
  CustomerProfileRolesPreshow: function () {
    this.view.tabs.setSkinForInfoTabs(this.view.tabs.btnTabName4);
    this.view.Notes.setDefaultNotesData(this);
    var screenHeight = kony.os.deviceInfo().screenHeight;
    this.view.flxMainContent.height = screenHeight - 135 + "px";
    this.view.flxGeneralInfoWrapper.changeSelectedTabColour(this.view.flxGeneralInfoWrapper.dashboardCommonTab.btnProfile);
    this.view.flxGeneralInfoWrapper.generalInfoHeader.setDefaultHeaderData(this);
    this.view.flxGeneralInfoWrapper.setFlowActionsForGeneralInformationComponent(this);
    this.AdminConsoleCommonUtils.setVisibility(this.view.groupsEditButtons.btnNext, false);
    this.setFlowActions();
  },
  unloadOnScrollEvent: function () {
    document.getElementById("frmCustomerProfileRoles_flxMainContent").onscroll = function () { };
  },
  resetAllSortImagesGroups: function () {
    if (this.view.fonticonSortGroupName.text !== kony.adminConsole.utils.fonticons.SORTABLE_IMAGE) {
      this.view.fonticonSortGroupName.text = kony.adminConsole.utils.fonticons.SORTABLE_IMAGE;
      this.view.fonticonSortGroupName.skin = kony.adminConsole.utils.fonticons.SORTABLE_SKIN;
    }
  },
  clearSearchResultsEditGroup: function () {
    this.view.addAndRemoveGroups.tbxSearchBox.text = "";
    this.view.addAndRemoveGroups.flxClearSearchImage.setVisibility(false);
    var searchParameters = [{
      "searchKey": "lblName",
      "searchValue": ""
    }];
    this.search(this.view.addAndRemoveGroups.segAddOptions, searchParameters, this.view.addAndRemoveGroups.rtxAvailableOptionsMessage,
      this.view.flxOtherInfoWrapper, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ALL"), null);

    if (this.view.addAndRemoveGroups.rtxAvailableOptionsMessage.isVisible) {
      this.view.addAndRemoveGroups.btnSelectAll.setVisibility(false);

    } else {
      this.view.addAndRemoveGroups.btnSelectAll.setVisibility(true);
    }
  },
  showGroupsScreen: function () {
    this.view.flxGroupsWrapper.setVisibility(true);
    this.view.flxGroupsListing.setVisibility(true);
    this.view.flxGroupsEdit.setVisibility(false);
    this.view.forceLayout();
    this.view.flxMainContent.scrollToWidget(this.view.flxOtherInfoWrapper);
  },
  showGroupsEditScreen: function () {
    this.view.flxGroupsWrapper.setVisibility(true);
    this.view.flxGroupsListing.setVisibility(false);
    this.view.flxGroupsEdit.setVisibility(true);
    this.view.addAndRemoveGroups.rtxAvailableOptionsMessage.setVisibility(false);
    this.view.addAndRemoveGroups.btnSelectAll.setVisibility(true);
    this.view.addAndRemoveGroups.rtxSelectedOptionsMessage.setVisibility(false);
    this.view.addAndRemoveGroups.btnRemoveAll.setVisibility(true);
    this.view.forceLayout();
    this.view.flxMainContent.scrollToWidget(this.view.flxOtherInfoWrapper);
    this.view.addAndRemoveGroups.tbxSearchBox.text = "";
    this.view.addAndRemoveGroups.flxClearSearchImage.setVisibility(false);
  },
  setFlowActions: function () {
    var scopeObj = this;
    this.view.addAndRemoveGroups.tbxSearchBox.onKeyUp = function () {
      var searchParameters = [{
        "searchKey": "lblName",
        "searchValue": scopeObj.view.addAndRemoveGroups.tbxSearchBox.text
      }];
      scopeObj.view.addAndRemoveGroups.flxClearSearchImage.setVisibility(true);
      scopeObj.view.addAndRemoveGroups.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.No_results_found_with_parameters") + scopeObj.view.addAndRemoveGroups.tbxSearchBox.text + kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Try_with_another_keyword");
      scopeObj.search(scopeObj.view.addAndRemoveGroups.segAddOptions, searchParameters, scopeObj.view.addAndRemoveGroups.rtxAvailableOptionsMessage,
        scopeObj.view.flxOtherInfoWrapper, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ALL"), null);

      if (scopeObj.view.addAndRemoveGroups.rtxAvailableOptionsMessage.isVisible) {
        scopeObj.view.addAndRemoveGroups.btnSelectAll.setVisibility(false);
      } else {
        scopeObj.view.addAndRemoveGroups.btnSelectAll.setVisibility(true);
      }
    };
    this.view.addAndRemoveGroups.flxClearSearchImage.onClick = function () {
      scopeObj.clearSearchResultsEditGroup();
    };
    //sorting on groups
    this.view.flxGroupName.onClick = function () {
      scopeObj.AdminConsoleCommonUtils.sort(scopeObj.view.segGroups, "lblGroupName", scopeObj.view.fonticonSortGroupName, scopeObj.view.flxOtherInfoWrapper, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ALL"), null);
    };
    this.view.addAndRemoveGroups.btnSelectAll.onClick = function () {
      scopeObj.selectAllGroups();
    };
    this.view.addAndRemoveGroups.btnRemoveAll.onClick = function () {
      scopeObj.removeAllSelectedGroups();
    };
    this.view.backToGroupsListing.btnBack.onClick = function () {
      scopeObj.showGroupsScreen();
    };
    this.view.groupsEditButtons.btnCancel.onClick = function () {
      scopeObj.showGroupsScreen();
    };
    this.view.groupsEditButtons.btnSave.onClick = function () {
      var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
      var listOfAddedGroups = scopeObj.getAllAddedGroups();
      var listOfRemovedGroups = scopeObj.getAllRemovedGroups();
      scopeObj.presenter.editCustomerGroups({
        "Customer_id": id,
        "listOfAddedGroups": listOfAddedGroups,
        "listOfRemovedGroups": listOfRemovedGroups
      });

    };
    this.view.btnGroupsEdit.onClick = function () {
      var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
      var target = "EditScreen";
      scopeObj.presenter.getCustomerGroups({
        "customerID": id
      }, target);
    };
  },
  setDataForGroupsSegment: function (CustomerGroups) {
    var self = this;
    //Determine the type of the customer
    var customerType = this.view.flxGeneralInfoWrapper.generalInfoHeader.flxDefaultSearchHeader.info ?
      (this.view.flxGeneralInfoWrapper.generalInfoHeader.flxDefaultSearchHeader.info.CustomerType_id ? this.view.flxGeneralInfoWrapper.generalInfoHeader.flxDefaultSearchHeader.info.CustomerType_id : "TYPE_ID_RETAIL")
      : "TYPE_ID_RETAIL";
    var dataMap = {
      "flxCustomerMangGroup": "flxCustomerMangGroup",
      "flxDelete": "flxDelete",
      "flxGroupNameAndDescription": "flxGroupNameAndDescription",
      "flxSeperator": "flxSeperator",
      "fonticonDelete": "fonticonDelete",
      "lblGroupName": "lblGroupName",
      "lblSeperator": "lblSeperator",
      "rtxDescription": "rtxDescription"
    };
    var data = [];
    var toAdd;
    if (CustomerGroups.length > 0) {

      if (customerType === "TYPE_ID_MICRO_BUSINESS" || customerType === "TYPE_ID_SMALL_BUSINESS" || 
            this.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "false") {
        this.view.btnGroupsEdit.setVisibility(false);
      } else {
        this.view.btnGroupsEdit.setVisibility(true);
      }
      for (var i = 0; i < CustomerGroups.length; i++) {
        toAdd = {

          "flxCustomerMangGroup": "flxCustomerMangGroup",
          "flxDelete": {
            "onClick": function () {
              self.deleteGroup();
            },
            "isVisible": (customerType === "TYPE_ID_MICRO_BUSINESS" || customerType === "TYPE_ID_SMALL_BUSINESS"
              || this.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "false") ? false : true
          },
          "flxGroupNameAndDescription": "flxGroupNameAndDescription",
          "flxSeperator": "flxSeperator",
          "fonticonDelete": {
            "text": ""
          },
          "lblGroupName": CustomerGroups[i].Group_name,
          "lblSeperator": ".",
          "rtxDescription": CustomerGroups[i].Group_Desc,
          "template": "flxCustomerMangGroup",
          "id": CustomerGroups[i].Group_id
        };
        data.push(toAdd);
      }
      this.view.segGroups.widgetDataMap = dataMap;
      this.view.segGroups.setData(data);
      this.view.segGroups.info = {
        "data": data,
        "searchAndSortData": data
      };
      this.view.segGroups.setVisibility(true);
      this.view.lblGroupsHeaderSeperator.setVisibility(true);
      this.view.flxGroupsSegmentHeader.setVisibility(true);
      this.view.rtxMsgGroups.setVisibility(false);
    } else {
      this.view.segGroups.setVisibility(false);
      this.view.lblGroupsHeaderSeperator.setVisibility(false);
      this.view.flxGroupsSegmentHeader.setVisibility(false);
      this.view.rtxMsgGroups.setVisibility(true);

    }
    this.showGroupsScreen();
    this.view.forceLayout();
    this.view.flxMainContent.scrollToWidget(this.view.flxOtherInfoWrapper);
  },
  deleteGroup: function () {
    var self = this;
    var confirmAction = function () {
      var groupid = self.view.segGroups.selecteditems[0].id;
      var id = self.presenter.getCurrentCustomerDetails().Customer_id;
      self.presenter.editCustomerGroups({
        "Customer_id": id,
        "listOfAddedGroups": [],
        "listOfRemovedGroups": [groupid]
      });
    };
    var cancelAction = function () { };

    this.AdminConsoleCommonUtils.openConfirm({
      header: 'Remove group',
      message: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Remove_customer_from_group") + (self.view.segGroups.selecteditems[0].lblGroupName) + kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Remove_customer_from_group_Messagecontent"),
      confirmAction: confirmAction,
      cancelMsg: kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS"),
      cancelAction: cancelAction,
      confirmMsg: 'YES, REMOVE',

    }, this);
  },
  checkSizeAndToggleVisibilityForAddGroups: function () {
    if (this.view.addAndRemoveGroups.segAddOptions.data.length === 0) {
      this.view.addAndRemoveGroups.rtxAvailableOptionsMessage.setVisibility(true);
      this.view.addAndRemoveGroups.segAddOptions.setVisibility(false);
      this.view.addAndRemoveGroups.btnSelectAll.setVisibility(false);
      this.view.addAndRemoveGroups.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.No_available_records");
    } else {
      this.view.addAndRemoveGroups.rtxAvailableOptionsMessage.setVisibility(false);
      this.view.addAndRemoveGroups.segAddOptions.setVisibility(true);
      this.view.addAndRemoveGroups.btnSelectAll.setVisibility(true);
    }
  },
  checkSizeAndToggleVisibilityForRemoveGroups: function () {
    if (this.view.addAndRemoveGroups.segSelectedOptions.data.length === 0) {
      this.view.addAndRemoveGroups.rtxSelectedOptionsMessage.setVisibility(true);
      this.view.addAndRemoveGroups.segSelectedOptions.setVisibility(false);
      this.view.addAndRemoveGroups.btnRemoveAll.setVisibility(false);
      this.view.addAndRemoveGroups.rtxSelectedOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.No_available_records");
    } else {
      this.view.addAndRemoveGroups.rtxSelectedOptionsMessage.setVisibility(false);
      this.view.addAndRemoveGroups.segSelectedOptions.setVisibility(true);
      this.view.addAndRemoveGroups.btnRemoveAll.setVisibility(true);
    }
  },
  setDataForGroupsEditScreen: function (CustomerGroups) {
    var availableGroups = CustomerGroups.AllGroups;

    for (var i = 0; i < CustomerGroups.AssignedGroups.length; i++) {
      availableGroups = availableGroups.filter(function (el) {
        return el.id !== CustomerGroups.AssignedGroups[i].Group_id;
      });
    }

    var segData1 = availableGroups.map(this.mappingAvailableGroupsData);
    this.setAvailableGroupsSegmentData(segData1);

    var segData2 = CustomerGroups.AssignedGroups.map(this.mappingSelectedGroupsData);
    this.setSelectedGroupsSegmentData(segData2);
    this.showGroupsEditScreen();
  },
  removeAllSelectedGroups: function () {
    //add data to left seg
    var self = this;
    var selectedGroupsData = this.view.addAndRemoveGroups.segSelectedOptions.data;
    var availableGroupsData = this.view.addAndRemoveGroups.segAddOptions.data;
    this.clearSearchResultsEditGroup();
    for (var i = 0; i < selectedGroupsData.length; i++) {
      var toAddData = {
        "btnAdd": {
          "text": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
          "onClick": function () {
            self.addGroupToSegment();
          }
        },
        "template": "flxAddOption",
        "lblName": "" + selectedGroupsData[i].lblOption,
        "rtxDescription": "" + selectedGroupsData[i].hiddenDescription,
        "id": selectedGroupsData[i].id
      };
      availableGroupsData.push(toAddData);

    }
    this.view.addAndRemoveGroups.segAddOptions.setData(availableGroupsData);
    this.view.addAndRemoveGroups.segAddOptions.info = {
      "data": availableGroupsData
    };
    this.checkSizeAndToggleVisibilityForAddGroups();
    //remove data from right
    this.view.addAndRemoveGroups.segSelectedOptions.removeAll();
    this.checkSizeAndToggleVisibilityForRemoveGroups();
  },
  removeSelectedGroup: function () {
    var rowIndex = this.view.addAndRemoveGroups.segSelectedOptions.selectedIndex[1];
    //add row to right
    var self = this;
    this.clearSearchResultsEditGroup();
    var selectedGroupsData = this.view.addAndRemoveGroups.segSelectedOptions.data;
    var availableGroupsData = this.view.addAndRemoveGroups.segAddOptions.data;
    var toAddData = {
      "btnAdd": {
        "text": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
        "onClick": function () {
          self.addGroupToSegment();
        }
      },
      "template": "flxAddOption",
      "lblName": "" + selectedGroupsData[rowIndex].lblOption,
      "rtxDescription": "" + selectedGroupsData[rowIndex].hiddenDescription,
      "id": selectedGroupsData[rowIndex].id
    };
    availableGroupsData.push(toAddData);
    this.view.addAndRemoveGroups.segAddOptions.setData(availableGroupsData);
    this.view.addAndRemoveGroups.segAddOptions.info = {
      "data": availableGroupsData
    };
    this.checkSizeAndToggleVisibilityForAddGroups();
    //check if right segment is empty and display rtx
    this.view.addAndRemoveGroups.segSelectedOptions.removeAt(rowIndex);
    this.checkSizeAndToggleVisibilityForRemoveGroups();
    this.view.forceLayout();
  },
  addGroupToSegment: function () {
    var self = this;
    var selectedItemData = this.view.addAndRemoveGroups.segAddOptions.selectedItems[0];
    var toAdd = {
      "flxClose": {
        "onClick": function () {
          self.removeSelectedGroup();
        }
      },
      "fontIconClose": "",
      "lblOption": "" + selectedItemData.lblName,
      "hiddenDescription": "" + selectedItemData.rtxDescription,
      "template": "flxOptionAdded",
      "id": selectedItemData.id
    };
    var data = this.view.addAndRemoveGroups.segSelectedOptions.data;
    data.push(toAdd);

    var rowIndex = this.view.addAndRemoveGroups.segAddOptions.selectedIndex[1];
    var id = self.view.addAndRemoveGroups.segAddOptions.data[rowIndex].id;
    this.view.addAndRemoveGroups.segAddOptions.removeAt(rowIndex);
    this.checkSizeAndToggleVisibilityForAddGroups();
    this.view.addAndRemoveGroups.segAddOptions.info.data = this.view.addAndRemoveGroups.segAddOptions.info.data.filter(function (el) {
      return el.id !== id;
    });
    this.view.addAndRemoveGroups.segSelectedOptions.setData(data);
    this.checkSizeAndToggleVisibilityForRemoveGroups();
    this.view.forceLayout();
  },

  selectAllGroups: function () {
    var self = this;
    var groupsToAddData = this.view.addAndRemoveGroups.segAddOptions.info.searchAndSortData || this.view.addAndRemoveGroups.segAddOptions.info.data;
    var data = this.view.addAndRemoveGroups.segSelectedOptions.data;
    for (var i = 0; i < groupsToAddData.length; i++) {
      var toAdd = {
        "flxClose": {
          "onClick": function () {
            self.removeSelectedGroup();
          }
        },
        "fontIconClose": "",
        "lblOption": "" + groupsToAddData[i].lblName,
        "hiddenDescription": "" + groupsToAddData[i].rtxDescription,
        "template": "flxOptionAdded",
        "id": groupsToAddData[i].id
      };
      data.push(toAdd);
    }

    var newData = this.view.addAndRemoveGroups.segAddOptions.info.data;
    for (var i = 0; i < groupsToAddData.length; i++) {
      newData = newData.filter(function (x) {
        return x.id !== groupsToAddData[i].id;
      });
    }
    this.view.addAndRemoveGroups.segAddOptions.setData(newData);
    this.view.addAndRemoveGroups.segAddOptions.info = {
      "data": newData
    };
    this.checkSizeAndToggleVisibilityForAddGroups();
    this.view.addAndRemoveGroups.segSelectedOptions.setData(data);
    this.checkSizeAndToggleVisibilityForRemoveGroups();
    this.clearSearchResultsEditGroup();
    this.view.forceLayout();
  },
  mappingSelectedGroupsData: function (data) {
    var self = this;
    return {
      "flxClose": {
        "onClick": function () {
          self.removeSelectedGroup();
        }
      },
      "fontIconClose": "",
      "lblOption": "" + data.Group_name,
      "hiddenDescription": "" + data.Group_Desc,
      "id": data.Group_id,
      "template": "flxOptionAdded"
    };
  },
  mappingAvailableGroupsData: function (data) {
    var self = this;
    return {
      "btnAdd": {
        "text": kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
        "onClick": function () {
          self.addGroupToSegment();
        }
      },
      "flxAdd": "flxAdd",
      "flxAddOption": "flxAddOption",
      "template": "flxAddOption",
      "lblName": data.Name,
      "rtxDescription": data.Description,
      "id": data.id
    };
  },
  setAvailableGroupsSegmentData: function (data) {
    var dataMap = {
      btnAdd: "btnAdd",
      flxAdd: "flxAdd",
      flxAddOption: "flxAddOption",
      lblName: "lblName",
      rtxDescription: "rtxDescription"
    };
    this.view.addAndRemoveGroups.segAddOptions.widgetDataMap = dataMap;
    this.view.addAndRemoveGroups.segAddOptions.setData(data);
    this.view.addAndRemoveGroups.segAddOptions.info = {
      "data": data
    };
    this.checkSizeAndToggleVisibilityForAddGroups();
    this.view.forceLayout();
  },
  setSelectedGroupsSegmentData: function (assignedGroups) {
    var dataMap = {
      flxAddOptionWrapper: "flxAddOptionWrapper",
      flxClose: "flxClose",
      flxOptionAdded: "flxOptionAdded",
      fontIconClose: "fontIconClose",
      lblOption: "lblOption"
    };
    var data = assignedGroups || [];
    this.view.addAndRemoveGroups.segSelectedOptions.widgetDataMap = dataMap;
    this.view.addAndRemoveGroups.segSelectedOptions.setData(data);
    this.checkSizeAndToggleVisibilityForRemoveGroups();
    this.view.forceLayout();
  },
  getAllAddedGroups: function () {
    var CustomerGroups = this.presenter.getCurrentCustomerGroups();

    var originalGroupIds = CustomerGroups.AssignedGroups.map(function (g) {
      return g.Group_id;
    });
    var Group_ids = this.view.addAndRemoveGroups.segSelectedOptions.data.map(function (g) {
      return g.id;
    });
    return this.updatedCollection(Group_ids, originalGroupIds);
  },
  getAllRemovedGroups: function () {
    var CustomerGroups = this.presenter.getCurrentCustomerGroups();

    var originalGroupIds = CustomerGroups.AssignedGroups.map(function (g) {
      return g.Group_id;
    });
    var Group_ids = this.view.addAndRemoveGroups.segSelectedOptions.data.map(function (g) {
      return g.id;
    });
    return this.updatedCollection(originalGroupIds, Group_ids);
  },
  updatedCollection: function (a1, a2) {
    return a1.filter(function (x) {
      var result = false;
      if (a2.indexOf(x) < 0) result = true;
      return result;
    });
  },


});