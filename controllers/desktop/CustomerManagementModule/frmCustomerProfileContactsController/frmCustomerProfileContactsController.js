define({
    contactInfoCount: 3,
    PhoneNumbersList: [kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Home"),kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Work"),
                  kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Mobile") ],
    EmailTypesList: [kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Personal"),kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Work"),
                  kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Other") ],
    willUpdateUI: function (context) {
        if (context) {
            this.updateLeftMenu(context);

            if (context.LoadingScreen) {
                if (context.LoadingScreen.focus) {
                    kony.adminConsole.utils.showProgressBar(this.view);
                } else {
                    kony.adminConsole.utils.hideProgressBar(this.view);
                }
            } else if (context.toastModel) {
                if (context.toastModel.status === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SUCCESS")) {
                    this.view.toastMessage.showToastMessage(context.toastModel.message, this);
                } else {
                    this.view.toastMessage.showErrorToastMessage(context.toastModel.message, this);
                }
            } else if (context.CustomerBasicInfo) {
                this.view.flxGeneralInfoWrapper.setBasicInformation(context.CustomerBasicInfo, this);
                if (context.CustomerBasicInfo.customer.CustomerType_id !== "TYPE_ID_PROSPECT" && 
                    context.CustomerBasicInfo.target != "EditScreen") {
                    this.presenter.getCustomerContactInfo({ "customerID": this.presenter.getCurrentCustomerDetails().Customer_id }, "InfoScreen");
                    this.presenter.getCitiesStatesAndCountries();
                }
                
            } else if (context.CustomerContactInfo) {
                if (context.CustomerContactInfo.target === "InfoScreen") {
                    this.setDataForContactScreen(context.CustomerContactInfo);
                } else {
                    this.setDataForContactEditScreen(context.CustomerContactInfo);
                }
            } else if (context.enrollACustomer) {
                this.view.flxGeneralInfoWrapper.setEnrollmentAccessandStatus();

            } else if (context.StatusGroup) {
                this.view.flxGeneralInfoWrapper.processAndFillStatusForEdit(context.StatusGroup, this);

            } else if (context.UpdateDBPUserStatus) {
                this.view.flxGeneralInfoWrapper.setLockStatus(context.UpdateDBPUserStatus.status.toUpperCase(), this);

            } else if (context.CustomerNotes) {
                this.view.Notes.displayNotes(this, context.CustomerNotes);

            } else if (context.AddressModel) {
                this.populateAllListBoxes(context.AddressModel);

            } else if (context.targetAddressModel) {
                this.populateTargetedListBoxes(context.targetAddressModel);

            } else if (context.OnlineBankingLogin) {
                this.view.CSRAssist.onlineBankingLogin(context.OnlineBankingLogin, this);

            }
        }
    },
    CustomerProfileContactsPreshow: function () {
        this.AdminConsoleCommonUtils.setVisibility(this.view.EditAddress.lblAddressLine1Size, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.EditAddress.lblAddressLine2Size, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.EditAddress.lblZipcodeSize, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.EditAddress1.lblAddressLine1Size, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.EditAddress1.lblAddressLine2Size, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.EditAddress1.lblZipcodeSize, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.EditAddress2.lblAddressLine1Size, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.EditAddress2.lblAddressLine2Size, false);
        this.AdminConsoleCommonUtils.setVisibility(this.view.EditAddress2.lblZipcodeSize, false);
        this.view.tabs.setSkinForInfoTabs(this.view.tabs.btnTabName1);
        this.view.Notes.setDefaultNotesData(this);
        var screenHeight = kony.os.deviceInfo().screenHeight;
        this.view.flxMainContent.height = screenHeight - 135 + "px";
        this.view.flxGeneralInfoWrapper.changeSelectedTabColour(this.view.flxGeneralInfoWrapper.dashboardCommonTab.btnProfile);
        this.view.flxGeneralInfoWrapper.generalInfoHeader.setDefaultHeaderData(this);
        this.view.flxGeneralInfoWrapper.setFlowActionsForGeneralInformationComponent(this);
        this.setFlowActions();
    },

    showContactsViewScreen: function () {
        this.view.flxEditContactDetails.setVisibility(false);
        this.view.flxContactDetails.setVisibility(true);
    },
    setFlowActions: function () {
        var scopeObj = this;
        this.view.EditAddress.txtfldAddressLine1.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress.txtfldAddressLine1);
            scopeObj.view.EditAddress.lblErrorAddressLine1.setVisibility(false);
            scopeObj.view.EditAddress.lblAddressLine1Size.setVisibility(true);
            scopeObj.view.EditAddress.lblAddressLine1Size.text = scopeObj.view.EditAddress.txtfldAddressLine1.text.length + "/100";
            scopeObj.view.forceLayout();
        };
        this.view.EditAddress.txtfldAddressLine1.onEndEditing = function () {
            scopeObj.view.EditAddress.lblAddressLine1Size.setVisibility(false);
        }
        this.view.EditAddress.txtfldAddressLine2.onKeyUp = function () {
            scopeObj.view.EditAddress.lblAddressLine2Size.setVisibility(true);
            scopeObj.view.EditAddress.lblAddressLine2Size.text = scopeObj.view.EditAddress.txtfldAddressLine2.text.length + "/100";
            scopeObj.view.forceLayout();
        };
        this.view.EditAddress.txtfldAddressLine2.onEndEditing = function () {
            scopeObj.view.EditAddress.lblAddressLine2Size.setVisibility(false);
        }
        this.view.EditAddress.txtfldZipCode.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress.txtfldZipCode);
            scopeObj.view.EditAddress.lblErrorZipcode.setVisibility(false);
            scopeObj.view.EditAddress.lblZipcodeSize.text = scopeObj.view.EditAddress.txtfldZipCode.text.length + "/20";
            scopeObj.view.forceLayout();
        };
        this.view.EditAddress.txtfldZipCode.onEndEditing = function () {
            scopeObj.view.EditAddress.lblZipcodeSize.setVisibility(false);
        }
        this.view.EditAddress.lstboxCity.onSelection = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress.lstboxCity);
            scopeObj.view.EditAddress.lblErrorCity.setVisibility(false);
        };
        this.view.EditAddress.lstboxstate.onSelection = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress.lstboxstate);
            scopeObj.view.EditAddress.lblErrorState.setVisibility(false);
            scopeObj.presenter.getSpecifiedCitiesAndStates(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.state"), scopeObj.view.EditAddress.lstboxstate.selectedKey, 0);
        };
        this.view.EditAddress.lstboxCountry.onSelection = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress.lstboxCountry);
            scopeObj.view.EditAddress.lblErrorCountry.setVisibility(false);
            scopeObj.presenter.getSpecifiedCitiesAndStates(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.country"), scopeObj.view.EditAddress.lstboxCountry.selectedKey, 0);
        };
        this.view.EditAddress1.txtfldAddressLine1.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress1.txtfldAddressLine1);
            scopeObj.view.EditAddress1.lblErrorAddressLine1.setVisibility(false);
            scopeObj.view.EditAddress1.lblAddressLine1Size.setVisibility(true);
            scopeObj.view.EditAddress1.lblAddressLine1Size.text = scopeObj.view.EditAddress1.txtfldAddressLine1.text.length + "/100";
            scopeObj.view.forceLayout();
        };
        this.view.EditAddress1.txtfldAddressLine1.onEndEditing = function () {
            scopeObj.view.EditAddress1.lblAddressLine1Size.setVisibility(false);
        }
        this.view.EditAddress1.txtfldAddressLine2.onKeyUp = function () {
            scopeObj.view.EditAddress1.lblAddressLine2Size.setVisibility(true);
            scopeObj.view.EditAddress1.lblAddressLine2Size.text = scopeObj.view.EditAddress1.txtfldAddressLine2.text.length + "/100";
            scopeObj.view.forceLayout();
        };
        this.view.EditAddress1.txtfldAddressLine2.onEndEditing = function () {
            scopeObj.view.EditAddress1.lblAddressLine2Size.setVisibility(false);
        }
        this.view.EditAddress1.txtfldZipCode.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress1.txtfldZipCode);
            scopeObj.view.EditAddress1.lblErrorZipcode.setVisibility(false);
            scopeObj.view.EditAddress1.lblZipcodeSize.text = scopeObj.view.EditAddress1.txtfldZipCode.text.length + "/20";
            scopeObj.view.forceLayout();
        };
        this.view.EditAddress1.txtfldZipCode.onEndEditing = function () {
            scopeObj.view.EditAddress1.lblZipcodeSize.setVisibility(false);
        }
        this.view.EditAddress1.lstboxCity.onSelection = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress1.lstboxCity);
            scopeObj.view.EditAddress1.lblErrorCity.setVisibility(false);
        };
        this.view.EditAddress1.lstboxstate.onSelection = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress1.lstboxstate);
            scopeObj.view.EditAddress1.lblErrorState.setVisibility(false);
            scopeObj.presenter.getSpecifiedCitiesAndStates(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.state"), scopeObj.view.EditAddress1.lstboxstate.selectedKey, 1);
        };
        this.view.EditAddress1.lstboxCountry.onSelection = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress1.lstboxCountry);
            scopeObj.view.EditAddress1.lblErrorCountry.setVisibility(false);
            scopeObj.presenter.getSpecifiedCitiesAndStates(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.country"), scopeObj.view.EditAddress1.lstboxCountry.selectedKey, 1);
        };
        this.view.EditAddress2.txtfldAddressLine1.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress2.txtfldAddressLine1);
            scopeObj.view.EditAddress2.lblErrorAddressLine1.setVisibility(false);
            scopeObj.view.EditAddress2.lblAddressLine1Size.setVisibility(true);
            scopeObj.view.EditAddress2.lblAddressLine1Size.text = scopeObj.view.EditAddress2.txtfldAddressLine1.text.length + "/100";
            scopeObj.view.forceLayout();
        };
        this.view.EditAddress2.txtfldAddressLine1.onEndEditing = function () {
            scopeObj.view.EditAddress2.lblAddressLine1Size.setVisibility(false);
        }
        this.view.EditAddress2.txtfldAddressLine2.onKeyUp = function () {
            scopeObj.view.EditAddress2.lblAddressLine2Size.setVisibility(true);
            scopeObj.view.EditAddress2.lblAddressLine2Size.text = scopeObj.view.EditAddress2.txtfldAddressLine2.text.length + "/100";
            scopeObj.view.forceLayout();
        };
        this.view.EditAddress2.txtfldAddressLine2.onEndEditing = function () {
            scopeObj.view.EditAddress2.lblAddressLine2Size.setVisibility(false);
        }
        this.view.EditAddress2.txtfldZipCode.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress2.txtfldZipCode);
            scopeObj.view.EditAddress2.lblErrorZipcode.setVisibility(false);
            scopeObj.view.EditAddress2.lblZipcodeSize.text = scopeObj.view.EditAddress2.txtfldZipCode.text.length + "/20";
            scopeObj.view.forceLayout();
        };
        this.view.EditAddress2.txtfldZipCode.onEndEditing = function () {
            scopeObj.view.EditAddress2.lblZipcodeSize.setVisibility(false);
        }
        this.view.EditAddress2.lstboxCity.onSelection = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress2.lstboxCity);
            scopeObj.view.EditAddress2.lblErrorCity.setVisibility(false);
        };
        this.view.EditAddress2.lstboxstate.onSelection = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress2.lstboxstate);
            scopeObj.view.EditAddress2.lblErrorState.setVisibility(false);
            scopeObj.presenter.getSpecifiedCitiesAndStates(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.state"), scopeObj.view.EditAddress2.lstboxstate.selectedKey, 2);
        };
        this.view.EditAddress2.lstboxCountry.onSelection = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditAddress2.lstboxCountry);
            scopeObj.view.EditAddress2.lblErrorCountry.setVisibility(false);
            scopeObj.presenter.getSpecifiedCitiesAndStates(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.country"), scopeObj.view.EditAddress2.lstboxCountry.selectedKey, 2);
        };
        this.view.backToPageHeader.btnBack.onClick = function () {
            scopeObj.view.flxGeneralInfoWrapper.showGeneralInformationScreen(scopeObj);
            scopeObj.showContactsViewScreen();
            scopeObj.view.forceLayout();
        };
        this.view.EditEmailId.txtfldContactNum1.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditEmailId.txtfldContactNum1);
            scopeObj.view.EditEmailId.lblErrorContact1.setVisibility(false);
        };
        this.view.EditEmailId.txtfldContactNum2.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditEmailId.txtfldContactNum2);
            scopeObj.view.EditEmailId.lblErrorContact2.setVisibility(false);
        };
        this.view.EditEmailId.txtfldContactNum3.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditEmailId.txtfldContactNum3);
            scopeObj.view.EditEmailId.lblErrorContact3.setVisibility(false);
        };
        this.view.EditContact.contactNumber1.txtContactNumber.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditContact.contactNumber1.txtContactNumber);
            scopeObj.view.EditContact.contactNumber1.hideErrorMsg(2);
        };
        this.view.EditContact.contactNumber2.txtContactNumber.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditContact.contactNumber2.txtContactNumber);
            scopeObj.view.EditContact.contactNumber2.hideErrorMsg(2);
        };
        this.view.EditContact.contactNumber3.txtContactNumber.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditContact.contactNumber3.txtContactNumber);
            scopeObj.view.EditContact.contactNumber3.hideErrorMsg(2);
        };
        this.view.EditContact.contactNumber1.txtISDCode.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditContact.contactNumber1.txtISDCode);
            scopeObj.view.EditContact.contactNumber1.flxError.setVisibility(false);
            scopeObj.view.EditContact.contactNumber1.hideErrorMsg(1);
        };
        this.view.EditContact.contactNumber2.txtISDCode.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditContact.contactNumber2.txtISDCode);
            scopeObj.view.EditContact.contactNumber2.flxError.setVisibility(false);
            scopeObj.view.EditContact.contactNumber2.hideErrorMsg(1);
        };
        this.view.EditContact.contactNumber3.txtISDCode.onKeyUp = function () {
            scopeObj.AdminConsoleCommonUtils.showNoError(scopeObj.view.EditContact.contactNumber3.txtISDCode);
            scopeObj.view.EditContact.contactNumber3.flxError.setVisibility(false);
            scopeObj.view.EditContact.contactNumber3.hideErrorMsg(1);
        };
        this.view.EditContact.contactNumber1.txtISDCode.onEndEditing = function () {
            scopeObj.view.EditContact.contactNumber1.txtISDCode.text =
                scopeObj.view.EditContact.contactNumber1.addingPlus(scopeObj.view.EditContact.contactNumber1.txtISDCode.text);
        };
        this.view.EditContact.contactNumber2.txtISDCode.onEndEditing = function () {
            scopeObj.view.EditContact.contactNumber2.txtISDCode.text =
                scopeObj.view.EditContact.contactNumber2.addingPlus(scopeObj.view.EditContact.contactNumber2.txtISDCode.text);
        };
        this.view.EditContact.contactNumber3.txtISDCode.onEndEditing = function () {
            scopeObj.view.EditContact.contactNumber3.txtISDCode.text =
                scopeObj.view.EditContact.contactNumber3.addingPlus(scopeObj.view.EditContact.contactNumber3.txtISDCode.text);
        };
        this.view.ContactEmail.btnAdd.onClick = function () {
            //service call to fetch address details
            var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
            var target = "EditScreen";
            scopeObj.presenter.getCustomerContactInfo({
                "customerID": id
            }, target, "Email");
        };
        this.view.ContactEmail.btnEdit.onClick = function () {
            //service call to fetch address details
            var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
            var target = "EditScreen";
            scopeObj.presenter.getCustomerContactInfo({
                "customerID": id
            }, target, "Email");
        };
        this.view.editEmailIdButtons.btnSave.onClick = function () {

            if (scopeObj.validateCommunicationDetails("EditEmailId")) {
                var EmailIds = [];
                for (var i = 1; i <= scopeObj.contactInfoCount; i++) {
                    if (scopeObj.isCommunicationEntered("EditEmailId", i)) {

                        EmailIds.push({
                            value: scopeObj.view.EditEmailId["txtfldContactNum" + i].text,
                            isPrimary: scopeObj.view.EditEmailId["imgRadio" + i].src === scopeObj.AdminConsoleCommonUtils.radioSelected ? "1" : "0",
                            Extension: scopeObj.view.EditEmailId["lblContactType" + i].selectedKey
                        });
                        if (scopeObj.view.EditEmailId["txtfldContactNum" + i].info && scopeObj.view.EditEmailId["txtfldContactNum" + i].info.Key)
                            EmailIds[i - 1].id = scopeObj.view.EditEmailId["txtfldContactNum" + i].info.Key;
                    }
                }
                scopeObj.presenter.editCustomerContactInfo({
                    "Customer_id": scopeObj.presenter.getCurrentCustomerDetails().Customer_id,
                    "EmailIds": EmailIds

                });
                scopeObj.view.flxContactDetails.setVisibility(true);
                scopeObj.view.flxEditContactDetails.setVisibility(false);

            }
        };
        this.view.ContactNum.btnAdd.onClick = function () {
            //service call to fetch address details
            var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
            var target = "EditScreen";
            scopeObj.presenter.getCustomerContactInfo({
                "customerID": id
            }, target, "Phone");
        };
        this.view.ContactNum.btnEdit.onClick = function () {
            //service call to fetch address details
            var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
            var target = "EditScreen";
            scopeObj.presenter.getCustomerContactInfo({
                "customerID": id
            }, target, "Phone");
        };
        this.view.editContactButtons.btnSave.onClick = function () {
            if (scopeObj.validateCommunicationDetails("EditContact")) {
                var PhoneNumbers = [];
                for (var i = 1; i <= scopeObj.contactInfoCount; i++) {
                    if (scopeObj.isCommunicationEntered("EditContact", i)) {
                        PhoneNumbers.push({
                            value: scopeObj.view.EditContact["contactNumber" + i].txtISDCode.text + "-" + scopeObj.view.EditContact["contactNumber" + i].txtContactNumber.text,
                            isPrimary: scopeObj.view.EditContact["imgRadio" + i].src === scopeObj.AdminConsoleCommonUtils.radioSelected ? "1" : "0",
                            Extension: scopeObj.view.EditContact["lblContactType" + i].selectedKey
                        });
                        if (scopeObj.view.EditContact["contactNumber" + i].txtContactNumber.info && scopeObj.view.EditContact["contactNumber" + i].txtContactNumber.info.Key)
                            PhoneNumbers[i - 1].id = scopeObj.view.EditContact["contactNumber" + i].txtContactNumber.info.Key;
                    }
                }
                scopeObj.presenter.editCustomerContactInfo({
                    "Customer_id": scopeObj.presenter.getCurrentCustomerDetails().Customer_id,
                    "PhoneNumbers": PhoneNumbers

                });
                scopeObj.view.flxContactDetails.setVisibility(true);
                scopeObj.view.flxEditContactDetails.setVisibility(false);
            }
        };
        this.view.addressEditButtons.btnCancel.onClick = function () {
            scopeObj.view.flxGeneralInfoWrapper.showGeneralInformationScreen(scopeObj);
            scopeObj.showContactsViewScreen();
            scopeObj.view.forceLayout();
        };
        this.view.backToPageHeader.flxBack.onClick = function () {
            scopeObj.view.flxContactDetails.setVisibility(true);
            scopeObj.view.flxEditContactDetails.setVisibility(false);
        };

        this.view.editContactButtons.btnCancel.onClick = function () {
            scopeObj.view.flxContactDetails.setVisibility(true);
            scopeObj.view.flxEditContactDetails.setVisibility(false);
        };
        this.view.editEmailIdButtons.btnCancel.onClick = function () {
            scopeObj.view.flxContactDetails.setVisibility(true);
            scopeObj.view.flxEditContactDetails.setVisibility(false);
        };
        this.view.ContactPrefTimeMethod.btnEdit.onClick = function () {
            //service call to fetch details
            var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
            var target = "EditScreen";
            scopeObj.presenter.getCustomerContactInfo({
                "customerID": id
            }, target, "PrefTimeMethod");

        };
        this.view.Address.btnEdit.onClick = function () {
            //service call to fetch address details
            var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
            var target = "EditScreen";
            scopeObj.presenter.getCustomerContactInfo({
                "customerID": id
            }, target, "Address");
        };
        this.view.Address.btnAdd.onClick = function () {
            //service call to fetch address details
            var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
            var target = "EditScreen";
            scopeObj.presenter.getCustomerContactInfo({
                "customerID": id
            }, target, "Address");
        };

        this.view.EditAddress.flxRadio1.onClick = function () {
            var selectedBtn = scopeObj.view.EditAddress.imgRadio1;
            var allBtns = [scopeObj.view.EditAddress.imgRadio1, scopeObj.view.EditAddress1.imgRadio1, scopeObj.view.EditAddress2.imgRadio1];
            scopeObj.selectRadioButtons(selectedBtn, allBtns);
        };
        this.view.EditAddress1.flxRadio1.onClick = function () {
            var selectedBtn = scopeObj.view.EditAddress1.imgRadio1;
            var allBtns = [scopeObj.view.EditAddress.imgRadio1, scopeObj.view.EditAddress1.imgRadio1, scopeObj.view.EditAddress2.imgRadio1];
            scopeObj.selectRadioButtons(selectedBtn, allBtns);
        };
        this.view.EditAddress2.flxRadio1.onClick = function () {
            var selectedBtn = scopeObj.view.EditAddress2.imgRadio1;
            var allBtns = [scopeObj.view.EditAddress.imgRadio1, scopeObj.view.EditAddress1.imgRadio1, scopeObj.view.EditAddress2.imgRadio1];
            scopeObj.selectRadioButtons(selectedBtn, allBtns);
        };
        this.view.addressEditButtons.btnSave.onClick = function () {
            if (scopeObj.validateEditOrAddAddress()) {
                var Addresses = [];
                for (var i = 0; i < scopeObj.contactInfoCount; i++) {
                    if (scopeObj.isAddressEntered(i)) {
                        var count = (i === 0) ? "" : i;
                        Addresses.push({
                            Addr_type: scopeObj.view["EditAddress" + count].lblAddressType.selectedKey,
                            isPrimary: scopeObj.view["EditAddress" + count].imgRadio1.src === scopeObj.AdminConsoleCommonUtils.radioSelected ? "1" : "0",
                            addrLine1: scopeObj.view["EditAddress" + count].txtfldAddressLine1.text,
                            addrLine2: scopeObj.view["EditAddress" + count].txtfldAddressLine2.text ? scopeObj.view["EditAddress" + count].txtfldAddressLine2.text : "",
                            City_id: scopeObj.view["EditAddress" + count].lstboxCity.selectedKey,
                            Region_id: scopeObj.view["EditAddress" + count].lstboxstate.selectedKey,
                            ZipCode: scopeObj.view["EditAddress" + count].txtfldZipCode.text,
                        });
                        if (scopeObj.view["EditAddress" + count].flxViewContact.info.Address_id)
                            Addresses[i].Addr_id = scopeObj.view["EditAddress" + count].flxViewContact.info.Address_id;
                    }
                }
                scopeObj.presenter.editCustomerContactInfo({
                    "Customer_id": scopeObj.presenter.getCurrentCustomerDetails().Customer_id,
                    "Addresses": Addresses

                });
                scopeObj.view.flxContactDetails.setVisibility(true);
                scopeObj.view.flxEditContactDetails.setVisibility(false);
            }
        };

        this.view.Address.btnDeleteAddress1.onClick = function () {
            var confirmAction = function () {
                var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
                scopeObj.presenter.editCustomerContactInfo({
                    "Customer_id": id,
                    "deleteAddressID": scopeObj.view.Address.btnDeleteAddress1.info.Value
                });
            };
            var cancelAction = function () { };
            scopeObj.AdminConsoleCommonUtils.openConfirm({
                header: 'Delete Address',
                message: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Delete_address_message_content"),
                confirmAction: confirmAction,
                cancelMsg: kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS"),
                cancelAction: cancelAction,
                confirmMsg: 'YES, DELETE',

            }, scopeObj);
        };
        this.view.Address.btnDeleteAddress2.onClick = function () {
            var confirmAction = function () {
                var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
                scopeObj.presenter.editCustomerContactInfo({
                    "Customer_id": id,
                    "deleteAddressID": scopeObj.view.Address.btnDeleteAddress2.info.Value

                });
            };
            var cancelAction = function () { };
            scopeObj.AdminConsoleCommonUtils.openConfirm({
                header: 'Delete Address',
                message: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Delete_address_message_content"),
                confirmAction: confirmAction,
                cancelMsg: kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS"),
                cancelAction: cancelAction,
                confirmMsg: 'YES, DELETE',

            }, scopeObj);
        };
        this.view.ContactNum.btnDeleteAdditionalDetails1.onClick = function () {
            var confirmAction = function () {
                var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
                scopeObj.presenter.editCustomerContactInfo({
                    "Customer_id": id,
                    "deleteCommunicationID": scopeObj.view.ContactNum.btnDeleteAdditionalDetails1.info.Value

                });
            };
            var cancelAction = function () { };
            scopeObj.AdminConsoleCommonUtils.openConfirm({
                header: 'Delete Phone number',
                message: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Delete_Phone_message_content"),
                confirmAction: confirmAction,
                cancelMsg: kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS"),
                cancelAction: cancelAction,
                confirmMsg: 'YES, DELETE',

            }, scopeObj);
        };
        this.view.ContactNum.btnDeleteAdditionalDetails2.onClick = function () {
            var confirmAction = function () {
                var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
                scopeObj.presenter.editCustomerContactInfo({
                    "Customer_id": id,
                    "deleteCommunicationID": scopeObj.view.ContactNum.btnDeleteAdditionalDetails2.info.Value

                });
            };
            var cancelAction = function () { };
            scopeObj.AdminConsoleCommonUtils.openConfirm({
                header: 'Delete Phone number',
                message: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Delete_Phone_message_content"),
                confirmAction: confirmAction,
                cancelMsg: kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS"),
                cancelAction: cancelAction,
                confirmMsg: 'YES, DELETE',

            }, scopeObj);
        };
        this.view.ContactEmail.btnDeleteAdditionalDetails1.onClick = function () {
            var confirmAction = function () {
                var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
                scopeObj.presenter.editCustomerContactInfo({
                    "Customer_id": id,
                    "deleteCommunicationID": scopeObj.view.ContactEmail.btnDeleteAdditionalDetails1.info.Value

                });
            };
            var cancelAction = function () { };
            scopeObj.AdminConsoleCommonUtils.openConfirm({
                header: 'Delete email',
                message: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Delete_email_message_content"),
                confirmAction: confirmAction,
                cancelMsg: kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS"),
                cancelAction: cancelAction,
                confirmMsg: 'YES, DELETE',

            }, scopeObj);
        };
        this.view.ContactEmail.btnDeleteAdditionalDetails2.onClick = function () {
            var confirmAction = function () {
                var id = scopeObj.presenter.getCurrentCustomerDetails().Customer_id;
                scopeObj.presenter.editCustomerContactInfo({
                    "Customer_id": id,
                    "deleteCommunicationID": scopeObj.view.ContactEmail.btnDeleteAdditionalDetails2.info.Value

                });
            };
            var cancelAction = function () { };
            scopeObj.AdminConsoleCommonUtils.openConfirm({
                header: 'Delete email',
                message: kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Delete_email_message_content"),
                confirmAction: confirmAction,
                cancelMsg: kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS"),
                cancelAction: cancelAction,
                confirmMsg: 'YES, DELETE',

            }, scopeObj);
        };
        this.view.flxCheckBox1.onClick = function () {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.AdminConsoleCommonUtils.togglePreferredCheckbox(scopeObj.view.imgCheckBox1);
            scopeObj.togglePreferredTimeByCall();
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        this.view.flxCheckBox2.onClick = function () {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.AdminConsoleCommonUtils.togglePreferredCheckbox(scopeObj.view.imgCheckBox2);
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };

        this.view.flxTimeCheckBox1.onClick = function () {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.AdminConsoleCommonUtils.togglePreferredCheckbox(scopeObj.view.imgTimeCheckBox1);
            scopeObj.toggleAnyTime(scopeObj.view.imgTimeCheckBox1);
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        this.view.flxTimeCheckBox2.onClick = function () {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.AdminConsoleCommonUtils.togglePreferredCheckbox(scopeObj.view.imgTimeCheckBox2);
            scopeObj.toggleAnyTime(scopeObj.view.imgTimeCheckBox2);
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        this.view.flxTimeCheckBox3.onClick = function () {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.AdminConsoleCommonUtils.togglePreferredCheckbox(scopeObj.view.imgTimeCheckBox3);
            scopeObj.toggleAnyTime(scopeObj.view.imgTimeCheckBox3);
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        this.view.flxTimeCheckBox4.onClick = function () {
            scopeObj.AdminConsoleCommonUtils.storeScrollHeight(scopeObj.view.flxMainContent);
            scopeObj.AdminConsoleCommonUtils.togglePreferredCheckbox(scopeObj.view.imgTimeCheckBox4);
            scopeObj.toggleAnyTime(scopeObj.view.imgTimeCheckBox4);
            scopeObj.AdminConsoleCommonUtils.scrollToDefaultHeight(scopeObj.view.flxMainContent);
        };
        this.view.editTimeButtons.btnCancel.onClick = function () {
            scopeObj.view.flxContactDetails.setVisibility(true);
            scopeObj.view.flxEditContactDetails.setVisibility(false);
        };
        this.view.editTimeButtons.btnSave.onClick = function () {
            scopeObj.view.flxContactDetails.setVisibility(true);
            scopeObj.view.flxEditContactDetails.setVisibility(false);

            var PreferredMethod = "";
            for (var i = 1; i <= 2; i++) {
                if (scopeObj.view["imgCheckBox" + i].src === scopeObj.AdminConsoleCommonUtils.checkboxSelected) {
                    if (PreferredMethod !== "") {
                        PreferredMethod += ", " + scopeObj.view["lblCheckBox" + i].text;
                    } else {
                        PreferredMethod += scopeObj.view["lblCheckBox" + i].text;
                    }
                }
            }

            var PreferredTime = "";
            if (scopeObj.view.imgCheckBox1.src === scopeObj.AdminConsoleCommonUtils.checkboxSelected) {
                for (var i = 1; i <= 4; i++) {
                    if (scopeObj.view["imgTimeCheckBox" + i].src === scopeObj.AdminConsoleCommonUtils.checkboxSelected)
                        if (PreferredTime !== "") {
                            PreferredTime += ", " + scopeObj.view["lblPreferredTime" + i].text;
                        }
                        else {
                            PreferredTime += scopeObj.view["lblPreferredTime" + i].text;
                        }
                }
            }

            scopeObj.presenter.editCustomerContactInfo({
                "Customer_id": scopeObj.presenter.getCurrentCustomerDetails().Customer_id,
                "PreferredContactMethod": PreferredMethod,
                "PreferredContactTime": PreferredTime
            });
        };
    },
    setDataForContactScreen: function (contactInfo) {
        this.EmptyCommunication();
        this.view.ContactPrefTimeMethod.btnEdit.setVisibility(true);
        this.view.Address.btnAdd.setVisibility(true);
        this.view.Address.btnEdit.setVisibility(true);
        this.view.ContactNum.btnAdd.setVisibility(true);
        this.view.ContactNum.btnEdit.setVisibility(true);
        this.view.ContactEmail.btnAdd.setVisibility(true);
        this.view.ContactEmail.btnEdit.setVisibility(true);
        //Populate address data
        var finalAddress, addressCount = 1,
            i;
        if (contactInfo.Addresses.length === this.contactInfoCount) {
            this.view.Address.btnAdd.setVisibility(false);
        } else {
            this.view.Address.btnAdd.setVisibility(true);
        }

        if (contactInfo.ContactNumbers.length === this.contactInfoCount) {
            this.view.ContactNum.btnAdd.setVisibility(false);
        } else {
            this.view.ContactNum.btnAdd.setVisibility(true);
        }

        if (contactInfo.Emails.length === this.contactInfoCount) {
            this.view.ContactEmail.btnAdd.setVisibility(false)
        }
        else {
            this.view.ContactEmail.btnAdd.setVisibility(true);
        }

        for (i = 0; i < this.contactInfoCount && i < contactInfo.Addresses.length; i++) {
            finalAddress = "";
            if (contactInfo.Addresses[i].AddressLine1) {
                finalAddress += contactInfo.Addresses[i].AddressLine1 + ", ";
            }
            if (contactInfo.Addresses[i].AddressLine2) {
                finalAddress += contactInfo.Addresses[i].AddressLine2 + "<br>";
            }
            finalAddress += contactInfo.Addresses[i].CityName + ", " + contactInfo.Addresses[i].RegionName + ", " +
                contactInfo.Addresses[i].CountryName + " " + contactInfo.Addresses[i].ZipCode;
            var AddressType;
            switch (contactInfo.Addresses[i].AddressType) {
                case "ADR_TYPE_HOME":
                    AddressType = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Home");
                    break;
                case "ADR_TYPE_WORK":
                    AddressType = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Work");
                    break;
                case "ADR_TYPE_OTHER":
                    AddressType = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Other");
                    break;
            }
            if (contactInfo.Addresses[i].isPrimary === "true") {
                this.view.Address.lblAddressPrimary.text = AddressType;
                this.view.Address.rtxAddressPrimary.text = finalAddress;

            } else if (addressCount < 3) {
                this.view.Address["lblAddressAdditional" + addressCount].text = AddressType;
                this.view.Address["rtxAddressAdditional" + addressCount].text = finalAddress;
                this.view.Address["lblAddressAdditional" + addressCount].setVisibility(true);
                this.view.Address["btnDeleteAddress" + addressCount].info = {
                    "Key": "deleteAddressID",
                    "Value": contactInfo.Addresses[i].Address_id
                };
                this.view.Address["btnDeleteAddress" + addressCount].setVisibility(true);
                this.view.forceLayout();
                this.view.Address["flxSeprator" + addressCount].height = this.view.Address["rtxAddressAdditional" + addressCount].frame.height + "px";
                this.view.Address["flxSeprator" + addressCount].setVisibility(true);
                this.view.Address["btnDeleteAddress" + addressCount].top = (this.view.Address["rtxAddressAdditional" + addressCount].frame.height / 2) + 30 - 17.5 + "px";
                addressCount++;
            }

        }
        // Populate contact numbers data
        var contactCount = 1;

        for (i = 0; i < this.contactInfoCount && i < contactInfo.ContactNumbers.length; i++) {
            var contactTypeText = contactInfo.ContactNumbers[i].Extension;
            if (!this.PhoneNumbersList.contains(contactInfo.ContactNumbers[i].Extension)){
                contactTypeText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Other");
            }

            var phoneNumber = contactInfo.ContactNumbers[i].Value;
            if(phoneNumber && phoneNumber.endsWith("-")){
                phoneNumber = phoneNumber.slice(0, phoneNumber.length-1);
            }

            if (contactInfo.ContactNumbers[i].isPrimary === "true") {
                this.view.ContactNum.lblPrimaryKey.text = contactTypeText;
                this.view.ContactNum.lblPrimaryValue.text = phoneNumber;

            } else if (contactCount < 3) {
                this.view.ContactNum["lblAdditionalDetailsKey" + contactCount].text = contactTypeText;
                this.view.ContactNum["lblAdditionalDetailsValue" + contactCount].text = phoneNumber;
                this.view.ContactNum["lblAdditionalDetailsKey" + contactCount].setVisibility(true);
                this.view.ContactNum["btnDeleteAdditionalDetails" + contactCount].info = {
                    "Key": "deleteCommunicationID",
                    "Value": contactInfo.ContactNumbers[i].id
                };
                this.view.ContactNum["btnDeleteAdditionalDetails" + contactCount].setVisibility(true);
                contactCount++;
            }
        }
        //Populate email address data
        var emailCount = 1;
        this.view.ContactEmail.lblDetailsHeader.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Email_IDs");
        var emails = [];
        var primaryEmail = "";
        for (i = 0; i < this.contactInfoCount && i < contactInfo.Emails.length; i++) {
            emails.push(contactInfo.Emails[i].Value);
            var emailTypeText = contactInfo.Emails[i].Extension;
            if (!this.EmailTypesList.contains(contactInfo.Emails[i].Extension)){
                emailTypeText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Other");
            }
            if (contactInfo.Emails[i].isPrimary === "true") {
                this.view.ContactEmail.lblPrimaryKey.text = emailTypeText;
                this.view.ContactEmail.lblPrimaryValue.text = contactInfo.Emails[i].Value;
                primaryEmail = contactInfo.Emails[i].Value;
            } else if (emailCount < 3) {
                this.view.ContactEmail["lblAdditionalDetailsKey" + emailCount].text = emailTypeText;
                this.view.ContactEmail["lblAdditionalDetailsValue" + emailCount].text = contactInfo.Emails[i].Value;
                this.view.ContactEmail["lblAdditionalDetailsKey" + emailCount].setVisibility(true);
                this.view.ContactEmail["btnDeleteAdditionalDetails" + emailCount].info = {
                    "Key": "deleteCommunicationID",
                    "Value": contactInfo.Emails[i].id
                };
                this.view.ContactEmail["btnDeleteAdditionalDetails" + emailCount].setVisibility(true);
                emailCount++;
            }
        }
        this.view.ContactPrefTimeMethod.lblDetailsHeader.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Communication");
        this.view.ContactPrefTimeMethod.lblPrimaryKey.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Preferred_Contact_Method");
        this.view.ContactPrefTimeMethod.lblPrimaryValue.text = contactInfo.PreferredTimeAndMethod.PreferredContactMethod ?
            contactInfo.PreferredTimeAndMethod.PreferredContactMethod : "N/A";
        this.view.ContactPrefTimeMethod.lblAdditionalDetailsKey1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Preferred_Contact_Time");
        this.view.ContactPrefTimeMethod.btnDeleteAdditionalDetails1.setVisibility(false);
        this.view.ContactPrefTimeMethod.lblAdditionalDetailsValue1.text = contactInfo.PreferredTimeAndMethod.PreferredContactTime ?
            contactInfo.PreferredTimeAndMethod.PreferredContactTime : "N/A";

        if(this.presenter.getCurrentCustomerDetails().isCustomerAccessiable === "false"){
            //Hide edit and add buttons
            this.view.ContactPrefTimeMethod.btnEdit.setVisibility(false);
            this.view.Address.btnAdd.setVisibility(false);
            this.view.Address.btnEdit.setVisibility(false);
            this.view.ContactNum.btnAdd.setVisibility(false);
            this.view.ContactNum.btnEdit.setVisibility(false);
            this.view.ContactEmail.btnAdd.setVisibility(false);
            this.view.ContactEmail.btnEdit.setVisibility(false);

            //Hide delete buttons
            this.view.Address.btnDeleteAddress1.setVisibility(false);
            this.view.Address.btnDeleteAddress2.setVisibility(false);
            this.view.ContactNum.btnDeleteAdditionalDetails1.setVisibility(false);
            this.view.ContactNum.btnDeleteAdditionalDetails2.setVisibility(false);
            this.view.ContactEmail.btnDeleteAdditionalDetails1.setVisibility(false);
            this.view.ContactEmail.btnDeleteAdditionalDetails2.setVisibility(false);
        }
        this.showContactScreen();
        this.view.forceLayout();
    },
    EmptyCommunication: function (contactInfo) {
        //address
        var addressCount = 2, i;
        this.view.Address.rtxAddressPrimary.text = "N/A";
        for (i = 1; i <= addressCount; i++) {
            this.view.Address["rtxAddressAdditional" + i].text = "";
            this.view.Address["lblAddressAdditional" + i].setVisibility(false);
            this.view.Address["flxSeprator" + i].setVisibility(false);
            this.view.Address["btnDeleteAddress" + i].setVisibility(false);
        }
        //contact numbers
        var contactCount = 2;
        this.view.ContactNum.lblPrimaryValue.text = "N/A";
        for (i = 1; i <= contactCount; i++) {
            this.view.ContactNum["lblAdditionalDetailsValue" + i].text = "";
            this.view.ContactNum["lblAdditionalDetailsKey" + i].setVisibility(false);
            this.view.ContactNum["btnDeleteAdditionalDetails" + i].setVisibility(false);
        }
        //email
        var emailCount = 2;
        this.view.ContactEmail.lblPrimaryValue.text = "N/A";
        for (i = 1; i <= emailCount; i++) {
            this.view.ContactEmail["lblAdditionalDetailsValue" + i].text = "";
            this.view.ContactEmail["lblAdditionalDetailsKey" + i].setVisibility(false);
            this.view.ContactEmail["btnDeleteAdditionalDetails" + i].setVisibility(false);
        }
        this.showContactScreen();
    },
    showContactScreen: function () {
        this.view.flxContactWrapper.setVisibility(true);
        this.view.flxContactDetails.setVisibility(true);
        this.view.flxEditContactDetails.setVisibility(false);
        // this.setSkinForInfoTabs(this.view.tabs.btnTabName1); TODO: add this to tabs controller
    },
    setAddressListBoxToDefault: function (target) {
        var data = [];
        data.push(["ADR_TYPE_HOME", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Home")]);
        data.push(["ADR_TYPE_WORK", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Work")]);
        data.push(["ADR_TYPE_OTHER", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Other")]);
        this.view["EditAddress" + target].lblAddressType.masterData = data;
        this.view["EditAddress" + target].lblAddressType.selectedKey = "ADR_TYPE_HOME";
    },
    resetStateAndCityListbox: function (target) {
        var cities = [], states = [];
        cities.push(["lbl1", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Select_a_City")]);
        states.push(["lbl1", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Select_a_State")]);
        this.view["EditAddress" + target].lstboxCity.masterData = cities;
        this.view["EditAddress" + target].lstboxstate.masterData = states;
    },
    hideAllContactEditScreens: function () {
        this.view.flxEditContactAddress.setVisibility(false);
        this.view.flxEditContactNo.setVisibility(false);
        this.view.flxEditEmailId.setVisibility(false);
        this.view.flxEditTime.setVisibility(false);
    },
    resetAddressValidation: function () {
        for (var i = 0; i < this.contactInfoCount; i++) {
            var count = (i === 0) ? "" : i;
            this.AdminConsoleCommonUtils.showNoError(this.view["EditAddress" + count].txtfldAddressLine1);
            this.AdminConsoleCommonUtils.showNoError(this.view["EditAddress" + count].txtfldZipCode);
            this.AdminConsoleCommonUtils.showNoError(this.view["EditAddress" + count].lstboxCity);
            this.AdminConsoleCommonUtils.showNoError(this.view["EditAddress" + count].lstboxstate);
            this.AdminConsoleCommonUtils.showNoError(this.view["EditAddress" + count].lstboxCountry);
            this.view["EditAddress" + count].flxViewContact.info = {};
            this.view["EditAddress" + count].lblErrorAddressLine1.setVisibility(false);
            this.view["EditAddress" + count].lblErrorZipcode.setVisibility(false);
            this.view["EditAddress" + count].lblErrorCity.setVisibility(false);
            this.view["EditAddress" + count].lblErrorState.setVisibility(false);
            this.view["EditAddress" + count].lblErrorCountry.setVisibility(false);
        }
    },
    setListBoxToDefault: function (context, target) {
        var data = [];
        if (context === "EditEmailId") {
            data.push([kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Personal"), kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Personal")]);
            data.push([kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Other"), kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Other")]);
        }
        else {
            data.push([kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Home"), kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Home")]);
            data.push([kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Mobile"), kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Mobile")]);
        }
        data.push([kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Work"), kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Work")]);
        
        this.view["" + context]["lblContactType" + target].masterData = data;
        var defaultValue = (context === "EditEmailId") ? kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Personal")
            : kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Home");
        this.view["" + context]["lblContactType" + target].selectedKey = defaultValue;

        this.AdminConsoleCommonUtils.showNoError(this.view["" + context]["txtfldContactNum" + target]);
    },
    setDataForContactEditScreen: function (contactInfo) {
        var i, count, maxIterations, inter;
        this.resetAddressValidation();
        if (contactInfo.subTarget === "Address") {
            //Populate Address details
            for (i = 0; i < this.contactInfoCount; i++) {
                count = (i === 0) ? "" : i;
                this.setAddressListBoxToDefault(count);
                this.view["EditAddress" + count].lblContactHeader.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Address");
                this.view["EditAddress" + count].imgRadio1.src = this.AdminConsoleCommonUtils.radioNotSelected;
                this.view["EditAddress" + count].txtfldAddressLine1.text = "";
                this.view["EditAddress" + count].txtfldAddressLine2.text = "";
                this.view["EditAddress" + count].lstboxCity.selectedKey = "lbl1";
                this.view["EditAddress" + count].lstboxstate.selectedKey = "lbl1";
                this.view["EditAddress" + count].lstboxCountry.selectedKey = "lbl1";
                this.view["EditAddress" + count].txtfldZipCode.text = "";
                this.view["EditAddress" + count].flxViewContact.info = {};
                this.resetStateAndCityListbox(count);
            }

            maxIterations = contactInfo.Addresses.length > this.contactInfoCount ?
                this.contactInfoCount : contactInfo.Addresses.length;

            for (i = 0; i < maxIterations; i++) {
                count = (i === 0) ? "" : i;
                if (contactInfo.Addresses[i].isPrimary === "true") {
                    this.view["EditAddress" + count].imgRadio1.src = this.AdminConsoleCommonUtils.radioSelected;
                }
                else {
                    this.view["EditAddress" + count].imgRadio1.src = this.AdminConsoleCommonUtils.radioNotSelected;
                }

                this.view["EditAddress" + count].flxViewContact.info = {
                    "Address_id": contactInfo.Addresses[i].Address_id
                };
                this.presenter.getSpecifiedCitiesAndStates(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.country"), contactInfo.Addresses[i].Country_id, i);
                this.presenter.getSpecifiedCitiesAndStates(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.state"), contactInfo.Addresses[i].Region_id, i);

                this.view["EditAddress" + count].lblAddressType.selectedKey = contactInfo.Addresses[i].AddressType;
                this.view["EditAddress" + count].txtfldAddressLine1.text = contactInfo.Addresses[i].AddressLine1;
                this.view["EditAddress" + count].txtfldAddressLine2.text = contactInfo.Addresses[i].AddressLine2 ? contactInfo.Addresses[i].AddressLine2 : "";
                this.view["EditAddress" + count].lstboxCity.selectedKey = contactInfo.Addresses[i].City_id;
                this.view["EditAddress" + count].lstboxstate.selectedKey = contactInfo.Addresses[i].Region_id;
                this.view["EditAddress" + count].lstboxCountry.selectedKey = contactInfo.Addresses[i].Country_id;
                this.view["EditAddress" + count].txtfldZipCode.text = contactInfo.Addresses[i].ZipCode;
            }

            this.view.flxContactDetails.setVisibility(false);
            this.view.flxEditContactDetails.setVisibility(true);
            this.hideAllContactEditScreens();
            this.view.flxEditContactAddress.setVisibility(true);
        }

        else if (contactInfo.subTarget === "Phone") {
            //Populate contact details
            this.view.EditContact.lblContactHeader.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Contact_numbers");

            for (i = 1; i <= this.contactInfoCount; i++) {
                this.view.EditContact["contactNumber" + i].txtContactNumber.info = {};
                this.view.EditContact["contactNumber" + i].txtContactNumber.text = "";
                this.view.EditContact["contactNumber" + i].txtISDCode.text = "";
                this.view.EditContact["lblErrorContact" + i].setVisibility(false);
                this.view.EditContact["imgRadio" + i].src = this.AdminConsoleCommonUtils.radioNotSelected;
                this.setListBoxToDefault("EditContact", i);
            }

            maxIterations = contactInfo.ContactNumbers.length > this.contactInfoCount ?
                this.contactInfoCount : contactInfo.ContactNumbers.length;

            for (i = 1; i <= maxIterations; i++) {
                var delimeter = contactInfo.ContactNumbers[i - 1].Value.indexOf("-");
                var strLen = contactInfo.ContactNumbers[i - 1].Value.length;
                if ((contactInfo.ContactNumbers[i - 1].Value.substr(delimeter + 1, strLen)).indexOf("-") > -1) {
                    inter = (contactInfo.ContactNumbers[i - 1].Value.substr(delimeter + 1, strLen)).indexOf("-");
                } else {
                    inter = strLen;
                }
                this.view.EditContact["contactNumber" + i].txtISDCode.text = contactInfo.ContactNumbers[i - 1].Value.substr(0, delimeter);
                this.view.EditContact["contactNumber" + i].txtContactNumber.text = contactInfo.ContactNumbers[i - 1].Value.substr(delimeter + 1, inter);
                this.view.EditContact["contactNumber" + i].txtContactNumber.info = {
                    Key: contactInfo.ContactNumbers[i - 1].id
                };
                if (contactInfo.ContactNumbers[i - 1].isPrimary === "true") {
                    this.view.EditContact["imgRadio" + i].src = this.AdminConsoleCommonUtils.radioSelected;
                }

                if(contactInfo.ContactNumbers[i - 1].Extension === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Personal"))
                    this.view.EditContact["lblContactType" + i].selectedKey = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Home");
                else if (!this.view.EditContact["lblContactType" + i].masterData.map(function(ele){return ele[0]}).contains(contactInfo.ContactNumbers[i - 1].Extension))
                    this.view.EditContact["lblContactType" + i].selectedKey = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Other");
                else
                    this.view.EditContact["lblContactType" + i].selectedKey = contactInfo.ContactNumbers[i - 1].Extension;

            }

            this.view.flxContactDetails.setVisibility(false);
            this.view.flxEditContactDetails.setVisibility(true);
            this.hideAllContactEditScreens();
            this.view.EditContact.contactNumber1.isVisible = true;
            this.view.EditContact.contactNumber2.isVisible = true;
            this.view.EditContact.contactNumber3.isVisible = true;
            this.view.EditContact.contactNumber1.flxError.isVisible = false;
            this.view.EditContact.contactNumber2.flxError.isVisible = false;
            this.view.EditContact.contactNumber3.flxError.isVisible = false;
            this.view.EditContact.txtfldContactNum1.isVisible = false;
            this.view.EditContact.txtfldContactNum2.isVisible = false;
            this.view.EditContact.txtfldContactNum3.isVisible = false;
            this.view.EditContact.lblErrorContact1.isVisible = false;
            this.view.EditContact.lblErrorContact2.isVisible = false;
            this.view.EditContact.lblErrorContact3.isVisible = false;
            this.view.flxEditContactNo.setVisibility(true);
        }

        else if (contactInfo.subTarget === "Email") {
            //Populate email information
            this.view.EditEmailId.lblContactHeader.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Email_IDs");

            for (i = 1; i <= this.contactInfoCount; i++) {
                this.view.EditEmailId["txtfldContactNum" + i].info = {};
                this.view.EditEmailId["txtfldContactNum" + i].text = "";
                this.view.EditEmailId["lblErrorContact" + i].setVisibility(false);
                this.view.EditEmailId["imgRadio" + i].src = this.AdminConsoleCommonUtils.radioNotSelected;
                this.setListBoxToDefault("EditEmailId", i);
            }

            maxIterations = contactInfo.Emails.length > this.contactInfoCount ?
                this.contactInfoCount : contactInfo.Emails.length;

            for (i = 1; i <= maxIterations; i++) {
                this.view.EditEmailId["txtfldContactNum" + i].text = contactInfo.Emails[i - 1].Value;
                this.view.EditEmailId["txtfldContactNum" + i].info = {
                    Key: contactInfo.Emails[i - 1].id
                };
                if (contactInfo.Emails[i - 1].isPrimary === "true") {
                    this.view.EditEmailId["imgRadio" + i].src = this.AdminConsoleCommonUtils.radioSelected;
                }
                if (!this.view.EditEmailId["lblContactType" + i].masterData.map(function(ele){return ele[0]}).contains(contactInfo.Emails[i - 1].Extension))
                    this.view.EditEmailId["lblContactType" + i].selectedKey = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Other");
                else
                    this.view.EditEmailId["lblContactType" + i].selectedKey = contactInfo.Emails[i - 1].Extension;
            }

            this.view.flxContactDetails.setVisibility(false);
            this.view.flxEditContactDetails.setVisibility(true);
            this.hideAllContactEditScreens();
            this.view.EditContact.contactNumber1.isVisible = false;
            this.view.EditContact.contactNumber2.isVisible = false;
            this.view.EditContact.contactNumber3.isVisible = false;
            this.view.EditContact.contactNumber1.flxError.isVisible = false;
            this.view.EditContact.contactNumber2.flxError.isVisible = false;
            this.view.EditContact.contactNumber3.flxError.isVisible = false;
            this.view.EditContact.txtfldContactNum1.isVisible = true;
            this.view.EditContact.txtfldContactNum2.isVisible = true;
            this.view.EditContact.txtfldContactNum3.isVisible = true;
            this.view.EditContact.lblErrorContact1.isVisible = false;
            this.view.EditContact.lblErrorContact2.isVisible = false;
            this.view.EditContact.lblErrorContact3.isVisible = false;
            this.view.flxEditEmailId.setVisibility(true);
        }

        else if (contactInfo.subTarget === "PrefTimeMethod") {
            //Populate Time and contact information
            for (i = 1; i <= 2; i++) {
                this.view["imgCheckBox" + i].src = this.AdminConsoleCommonUtils.checkbox;
            }
            var contactMethod = [];
            if (contactInfo.PreferredTimeAndMethod.PreferredContactMethod) {
                contactMethod = contactInfo.PreferredTimeAndMethod.PreferredContactMethod.indexOf(",") >= 0 ?
                    contactInfo.PreferredTimeAndMethod.PreferredContactMethod.split(",") : [contactInfo.PreferredTimeAndMethod.PreferredContactMethod];
            }
            for (i = 0; i < contactMethod.length; i++) {
                if (contactMethod[i].trim().toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CALL").toUpperCase()) {
                    this.view.imgCheckBox1.src = this.AdminConsoleCommonUtils.checkboxSelected;
                } else if (contactMethod[i].trim().toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.EMAIL").toUpperCase()) {
                    this.view.imgCheckBox2.src = this.AdminConsoleCommonUtils.checkboxSelected;
                }
            }

            for (i = 1; i <= 4; i++) {
                this.view["imgTimeCheckBox" + i].src = this.AdminConsoleCommonUtils.checkbox;
            }
            if (this.view.imgCheckBox1.src === this.AdminConsoleCommonUtils.checkboxSelected) {
                var contactTime = [];
                if (contactInfo.PreferredTimeAndMethod.PreferredContactTime) {
                    contactTime = contactInfo.PreferredTimeAndMethod.PreferredContactTime.indexOf(",") >= 0 ?
                        contactInfo.PreferredTimeAndMethod.PreferredContactTime.split(",") : [contactInfo.PreferredTimeAndMethod.PreferredContactTime];
                }
                for (i = 0; i < contactTime.length; i++) {
                    if (contactTime[i].trim().toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Morning").toUpperCase()) {
                        this.view.imgTimeCheckBox1.src = this.AdminConsoleCommonUtils.checkboxSelected;
                    } else if (contactTime[i].trim().toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Afternoon").toUpperCase()) {
                        this.view.imgTimeCheckBox2.src = this.AdminConsoleCommonUtils.checkboxSelected;
                    } else if (contactTime[i].trim().toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Evening").toUpperCase()) {
                        this.view.imgTimeCheckBox4.src = this.AdminConsoleCommonUtils.checkboxSelected;
                    } else if (contactTime[i].trim().toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Anytime").toUpperCase()) {
                        this.view.imgTimeCheckBox3.src = this.AdminConsoleCommonUtils.checkboxSelected;
                    }
                }
                this.view.flxContactRow2.setVisibility(true);
            } else {
                this.view.flxContactRow2.setVisibility(false);
            }
            //Set visibility
            this.view.flxContactDetails.setVisibility(false);
            this.view.flxEditContactDetails.setVisibility(true);
            this.hideAllContactEditScreens();
            this.view.flxEditTime.setVisibility(true);
        }
        this.view.forceLayout();
        this.view.flxMainContent.scrollToWidget(this.view.flxOtherInfoWrapper);
    },
    populateAllListBoxes: function (AddressModel) {
        var cities = [], states = [], countries = [];
        cities.push(["lbl1", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Select_a_City")]);
        states.push(["lbl1", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Select_a_State")]);
        countries.push(["lbl1", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Select_a_Country")]);
        var i;
        for (i = 0; i < AddressModel.countries.length; i++) {
            countries.push([AddressModel.countries[i].id, AddressModel.countries[i].Name]);
        }

        for (i = 0; i < this.contactInfoCount; i++) {
            var count = (i === 0) ? "" : i;
            this.view["EditAddress" + count].lstboxCity.masterData = cities;
            this.view["EditAddress" + count].lstboxstate.masterData = states;
            this.view["EditAddress" + count].lstboxCountry.masterData = countries;
        }
        this.view.forceLayout();
    },
    populateTargetedListBoxes: function (targetAddressModel) {
        var count = (targetAddressModel.target === 0) ? "" : targetAddressModel.target;

        if (targetAddressModel.states) {
            this.view["EditAddress" + count].lstboxstate.masterData = targetAddressModel.states;
        }
        if (targetAddressModel.cities) {
            this.view["EditAddress" + count].lstboxCity.masterData = targetAddressModel.cities;
        }
        this.view.forceLayout();
    },
    validateEditOrAddAddress: function () {
        var flag = 0;
        for (var i = 0; i < this.contactInfoCount; i++) {
            if (this.isAddressEntered(i)) {
                flag += this.validateTargetedAddress(i);
            }
        }
        if (flag === 0)
            return true;
        return false;
    },
    validateTargetedAddress: function (target) {
        var count = (target === 0) ? "" : target;
        var flag = 0;
        if (this.view["EditAddress" + count].txtfldAddressLine1.text === "") {
            this.AdminConsoleCommonUtils.showError(this.view["EditAddress" + count].txtfldAddressLine1);
            this.view["EditAddress" + count].lblErrorAddressLine1.setVisibility(true);
            flag++;
        }
        if (this.view["EditAddress" + count].txtfldZipCode.text === "") {
            this.AdminConsoleCommonUtils.showError(this.view["EditAddress" + count].txtfldZipCode);
            this.view["EditAddress" + count].lblErrorZipcode.setVisibility(true);
            flag++;
        }
        if (this.view["EditAddress" + count].lstboxCity.selectedKey === "lbl1") {
            this.AdminConsoleCommonUtils.showError(this.view["EditAddress" + count].lstboxCity);
            this.view["EditAddress" + count].lblErrorCity.setVisibility(true);
            flag++;
        }
        if (this.view["EditAddress" + count].lstboxstate.selectedKey === "lbl1") {
            this.AdminConsoleCommonUtils.showError(this.view["EditAddress" + count].lstboxstate);
            this.view["EditAddress" + count].lblErrorState.setVisibility(true);
            flag++;
        }
        if (this.view["EditAddress" + count].lstboxCountry.selectedKey === "lbl1") {
            this.AdminConsoleCommonUtils.showError(this.view["EditAddress" + count].lstboxCountry);
            this.view["EditAddress" + count].lblErrorCountry.setVisibility(true);
            flag++;
        }
        return flag;
    },
    isAddressEntered: function (target) {
        var count = (target === 0) ? "" : target;
        if (this.view["EditAddress" + count].txtfldAddressLine1.text !== "") return true;
        if (this.view["EditAddress" + count].txtfldAddressLine2.text !== "") return true;
        if (this.view["EditAddress" + count].lstboxCity.selectedKey !== "lbl1") return true;
        if (this.view["EditAddress" + count].lstboxstate.selectedKey !== "lbl1") return true;
        if (this.view["EditAddress" + count].lstboxCountry.selectedKey !== "lbl1") return true;
        if (this.view["EditAddress" + count].txtfldZipCode.text !== "") return true;
        if (this.view["EditAddress" + count].imgRadio1.src === this.AdminConsoleCommonUtils.radioSelected) return true;
        if (this.view["EditAddress" + count].flxViewContact.info.Address_id) return true;

        return false;
    },
    validateCommunicationDetails: function (Type) {
        var flag = 0;
        for (var i = 1; i <= this.contactInfoCount; i++) {
            if (this.isCommunicationEntered(Type, i)) {
                flag += this.validateTargetedCommunication(Type, i);
            }
        }
        if (flag === 0)
            return true;
        return false;
    },
    isCommunicationEntered: function (Type, target) {
        if (Type === "EditContact") {
            if (this.view["" + Type]["contactNumber" + target].txtContactNumber.text !== "") return true;
            if (this.view["" + Type]["imgRadio" + target].src === this.AdminConsoleCommonUtils.radioSelected) return true;
            if (this.view["" + Type]["contactNumber" + target].txtContactNumber.info.Key) return true;
        } else {
            if (this.view["" + Type]["txtfldContactNum" + target].text !== "") return true;
            if (this.view["" + Type]["imgRadio" + target].src === this.AdminConsoleCommonUtils.radioSelected) return true;
            if (this.view["" + Type]["txtfldContactNum" + target].info.Key) return true;
        }
        return false;
    },
    validateTargetedCommunication: function (Type, target) {
        var flag = 0;
        var emailReg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var phoneReg = /^\d{10}$/;
        var ISD = /^\+(\d{1,3}|\d{1,3})$/;
        if (Type === "EditContact") {
            if (!(phoneReg.test(this.view["" + Type]["contactNumber" + target].txtContactNumber.text))) {
                this.AdminConsoleCommonUtils.showError(this.view["" + Type]["contactNumber" + target].txtContactNumber);
                this.view.EditContact["lblErrorContact" + target].setVisibility(true);
                this.view.EditContact["lblErrorContact" + target].text = "Invalid contact number";
                flag++;
            }
            if (this.view["" + Type]["contactNumber" + target].txtContactNumber.text === "") {
                this.view["" + Type]["lblErrorContact" + target].setVisibility(true);
                this.view["" + Type]["lblErrorContact" + target].text = "Contact number cannot be empty";
                this.AdminConsoleCommonUtils.showError(this.view["" + Type]["contactNumber" + target].txtContactNumber);
                flag++;
            }
            if ((!(ISD.test(this.view["" + Type]["contactNumber" + target].txtISDCode.text))) || this.view["" + Type]["contactNumber" + target].txtISDCode.text.length > 4) {
                this.AdminConsoleCommonUtils.showError(this.view["" + Type]["contactNumber" + target].txtISDCode);
                this.view.EditContact["lblErrorContact" + target].setVisibility(true);
                this.view.EditContact["lblErrorContact" + target].text = "Invalid ISD code";
                flag++;
            }
            if (this.view["" + Type]["contactNumber" + target].txtISDCode.text === "") {
                this.AdminConsoleCommonUtils.showError(this.view["" + Type]["contactNumber" + target].txtISDCode);
                this.view.EditContact["lblErrorContact" + target].setVisibility(true);
                this.view.EditContact["lblErrorContact" + target].text = "ISD code cannot be empty";
                flag++;
            }

        } else if (Type === "EditEmailId") {
            if (!(emailReg.test(this.view["" + Type]["txtfldContactNum" + target].text))) {
                this.AdminConsoleCommonUtils.showError(this.view["" + Type]["txtfldContactNum" + target]);
                this.view.EditEmailId["lblErrorContact" + target].setVisibility(true);
                this.view.EditEmailId["lblErrorContact" + target].text = "Invalid email address";
                flag++;
            }
            if (this.view["" + Type]["txtfldContactNum" + target].text === "") {
                this.view["" + Type]["lblErrorContact" + target].setVisibility(true);
                this.view["" + Type]["lblErrorContact" + target].text = "Email address cannot be empty";
                this.AdminConsoleCommonUtils.showError(this.view["" + Type]["txtfldContactNum" + target]);
                flag++;
            }
        }
        return flag;
    },
    togglePreferredTimeByCall: function () {

        if (this.view.imgCheckBox1.src === this.AdminConsoleCommonUtils.checkboxSelected) {
            this.view.imgTimeCheckBox1.src = this.AdminConsoleCommonUtils.checkbox;
            this.view.imgTimeCheckBox2.src = this.AdminConsoleCommonUtils.checkbox;
            this.view.imgTimeCheckBox3.src = this.AdminConsoleCommonUtils.checkbox;
            this.view.imgTimeCheckBox4.src = this.AdminConsoleCommonUtils.checkbox;
            this.view.flxContactRow2.setVisibility(true);
        } else {
            this.view.imgTimeCheckBox1.src = this.AdminConsoleCommonUtils.checkbox;
            this.view.imgTimeCheckBox2.src = this.AdminConsoleCommonUtils.checkbox;
            this.view.imgTimeCheckBox3.src = this.AdminConsoleCommonUtils.checkbox;
            this.view.imgTimeCheckBox4.src = this.AdminConsoleCommonUtils.checkbox;
            this.view.flxContactRow2.setVisibility(false);
        }
    },
    toggleAnyTime: function (widget) {

        if (widget === this.view.imgTimeCheckBox1 || widget === this.view.imgTimeCheckBox2 || widget === this.view.imgTimeCheckBox3) {
            if (this.view.imgTimeCheckBox1.src === this.AdminConsoleCommonUtils.checkboxSelected && this.view.imgTimeCheckBox2.src === this.AdminConsoleCommonUtils.checkboxSelected && this.view.imgTimeCheckBox3.src === this.AdminConsoleCommonUtils.checkboxSelected) {
                this.view.imgTimeCheckBox4.src = this.AdminConsoleCommonUtils.checkboxSelected;

            } else {
                this.view.imgTimeCheckBox4.src = this.AdminConsoleCommonUtils.checkbox;
            }
        } else if (widget === this.view.imgTimeCheckBox4) {
            if (this.view.imgTimeCheckBox4.src === this.AdminConsoleCommonUtils.checkboxSelected) {
                this.view.imgTimeCheckBox1.src = this.AdminConsoleCommonUtils.checkboxSelected;
                this.view.imgTimeCheckBox2.src = this.AdminConsoleCommonUtils.checkboxSelected;
                this.view.imgTimeCheckBox3.src = this.AdminConsoleCommonUtils.checkboxSelected;

            } else {
                this.view.imgTimeCheckBox1.src = this.AdminConsoleCommonUtils.checkbox;
                this.view.imgTimeCheckBox2.src = this.AdminConsoleCommonUtils.checkbox;
                this.view.imgTimeCheckBox3.src = this.AdminConsoleCommonUtils.checkbox;

            }
        }
    },
    selectRadioButtons: function (selectedButton, buttonsArray) {
        for (var i = 0; i < buttonsArray.length; i++) {
            buttonsArray[i].src = this.AdminConsoleCommonUtils.radioNotSelected;
        }
        selectedButton.src = this.AdminConsoleCommonUtils.radioSelected;
    },
    
});