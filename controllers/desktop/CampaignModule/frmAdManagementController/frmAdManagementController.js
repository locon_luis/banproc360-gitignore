define({
  campaignData : null,
  allPossibleFilterData : [],
  statusfilterIndices : [],
  selectedStatus : null,
  isDefault : false,
  defaultCampaignDetails : {},

  campaignsPreShow: function(){
    this.view.mainHeader.btnAddNewOption.hoverSkin = "sknBtn163C5DLatoRegularffffff13pxRad28px";
    this.view.mainHeader.btnDropdownList.skin = "sknBtn006CCALatoRegularffffff13pxRad28px";
    this.view.mainHeader.btnDropdownList.hoverSkin = "sknBtn004F93LatoRegularffffff13pxRad28px";
    this.view.popUp.btnPopUpDelete.skin = "sknBtn006CCALatoRegularffffff13pxRad28px";
    this.view.popUp.btnPopUpDelete.hoverSkin = "sknBtn004F93LatoRegularffffff13pxRad28px";
    this.view.lblCampaignsInfo.text=kony.i18n.getLocalizedString("i18n.frmAdManagement.campaignsRunningInfo");
    this.view.flxListing.setVisibility(true);
    this.view.flxToastMessage.setVisibility(false);
    this.view.flxViewDetails.setVisibility(false);
    this.setFlowActions();
    this.view.mainHeader.flxButtons.setVisibility(true);
    this.view.flxViewDetails.height = (kony.os.deviceInfo().screenHeight - 126) + "px";
    this.view.flxFirstLine.isVisible = true;
    this.view.flxSecondLine.isVisible = true;
    this.view.flxDefaultAdCampaign.isVisible = false;
  },

  willUpdateUI: function (context) {
    var scopeObj = this;
    if(context){
      this.updateLeftMenu(context);
      if(context.progressBar) {
        if(context.progressBar.show === "success")
          kony.adminConsole.utils.showProgressBar(this.view);
        else
          kony.adminConsole.utils.hideProgressBar(this.view);
      }
      if(context.fetchCampaigns){
        scopeObj.fetchedCampaignsResponse(context);
      }
      if(context.fetchURLandGroups){
        scopeObj.checkURLResponse(context);
      }
      if(context.updatedCampaignStatus){
        scopeObj.showToastMsg(context);
      }
      if(context.fetchDefaultCampaign){
        scopeObj.checkGetDefaultCampaignResponse(context);
      }
      if(context && context.action === "createOrUpdateStatus") {
        scopeObj.showToastMsg({"updatedCampaignStatus" : context.status, "toastMessage" : context.msg});
      }
    }
  },

  checkURLResponse: function (context){
    // For populating URLs Data in flxViewDetails
    // Data coming from 'getCampaignSpecificationsAndGroups' Service
    var scopeObj = this;

    if(context.fetchURLandGroups === "success") {
      var URLdata = context.campaignSpecifications;
      scopeObj.populateChannelData(URLdata);
      scopeObj.setDefaultSettingsForViewDetails();//Initial Settings
      var customerRolesData = context.campaignGroups;
      scopeObj.populateTargetCustomerRoles(customerRolesData);
    }
    else {
      scopeObj.view.flxViewDetails.setVisibility(false);
      scopeObj.view.flxListing.setVisibility(true);
      scopeObj.presenter.fetchCampaigns();
      scopeObj.view.toastMessage.showErrorToastMessage("Error in fetching Campaigns", scopeObj);
    }
    scopeObj.view.forceLayout();

  },

  checkGetDefaultCampaignResponse: function(context) {
    var scopeObj = this;
    if(context.fetchDefaultCampaign === "success") {
      var defaultDescription = context.description;
      var defaultURLSpecifications = context.specifications;
      scopeObj.view.lblDefaultDescription.text = defaultDescription;
      scopeObj.populateChannelData(defaultURLSpecifications);
      scopeObj.setDefaultSettingsForViewDetails();//Initial Settings
      scopeObj.defaultCampaignDetails =  {"description" : defaultDescription, "specifications" : defaultURLSpecifications};
    }
    else {
      scopeObj.view.toastMessage.showErrorToastMessage("Error in fetching Campaigns", scopeObj);
    }
    scopeObj.view.forceLayout();
  },

  setFlowActions: function() {
    var scopeObj = this;

    this.view.flxCampaignNameHeader.onClick = function(){
      scopeObj.sortCampaignList("name");
    };

    this.view.flxCampaignPriorityHeader.onClick = function(){
      scopeObj.sortCampaignList("priority");
    };

    this.view.flxCampaignStartDateTimeHeader.onClick = function(){
      scopeObj.sortCampaignList("startDateTime");
    };

    this.view.flxCampaignEndDateTimeHeader.onClick = function(){
      scopeObj.sortCampaignList("endDateTime");
    };

    this.view.subHeader.tbxSearchBox.onTouchStart = function(){
      scopeObj.view.subHeader.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
    };

    this.view.subHeader.tbxSearchBox.onEndEditing = function(){
      scopeObj.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100"; 
    }; 

    this.view.subHeader.tbxSearchBox.onKeyUp = function() {
      scopeObj.searchCampaigns(scopeObj.view.subHeader.tbxSearchBox.text);
    };

    this.view.subHeader.flxClearSearchImage.onClick=function(){
      scopeObj.searchCampaigns("");
    };

    this.view.flxCampaignStatusHeader.onClick = function() {
      scopeObj.showStatusFilterMenu();
    };

    this.view.statusFilterMenu.segStatusFilterDropdown.onRowClick = function() {
      var index = scopeObj.view.statusFilterMenu.segStatusFilterDropdown.selectedRowIndex[1];
      scopeObj.showFilterdCampaignsList(index);
    };

    this.view.segCampaigns.onRowClick = function() {
      var selItem = scopeObj.view.segCampaigns.selectedItems[0];
      scopeObj.displayDetails(selItem);
    };

    this.view.mainHeader.btnAddNewOption.onClick = function() {
      scopeObj.displayDefaultAdDetails();
    };

    this.view.flxOptions.onClick = function() {
      scopeObj.toggleContextualMenu("viewDetails");
    };

    this.view.lblDescriptionTag.onTouchEnd = function(){
      if(scopeObj.view.lblIconRightArrow.isVisible===true){
        scopeObj.view.flxDescriptionBody.setVisibility(true);
        scopeObj.view.lblIconRightArrow.isVisible = false;
        scopeObj.view.lblIconDownArrow.isVisible = true;
        scopeObj.view.forceLayout();
      }
      else if(scopeObj.view.lblIconDownArrow.isVisible===true){
        scopeObj.view.flxDescriptionBody.setVisibility(false);
        scopeObj.view.lblIconDownArrow.isVisible = false;
        scopeObj.view.lblIconRightArrow.isVisible = true;
        scopeObj.view.forceLayout();
      }
    };
    // To Show Description (By Default it would be hidden)
    this.view.lblIconRightArrow.onTouchEnd = function(){
      if(scopeObj.view.lblIconRightArrow.isVisible===true){
        scopeObj.view.flxDescriptionBody.setVisibility(true);
        scopeObj.view.lblIconRightArrow.isVisible = false;
        scopeObj.view.lblIconDownArrow.isVisible = true;
        scopeObj.view.forceLayout();
      }
    };

    //To Hide Description
    this.view.lblIconDownArrow.onTouchEnd = function(){
      if(scopeObj.view.lblIconDownArrow.isVisible===true){
        scopeObj.view.flxDescriptionBody.setVisibility(false);
        scopeObj.view.lblIconDownArrow.isVisible = false;
        scopeObj.view.lblIconRightArrow.isVisible = true;
        scopeObj.view.forceLayout();
      }
    };

    // When Tab2 (Target Customer Roles) is pressed.
    this.view.flxTab2.onClick = function(){
      if(scopeObj.view.flxUnderline2.isVisible===false){
        scopeObj.view.flxUnderline2.isVisible = true;
        scopeObj.view.flxUnderline1.isVisible = false;
        scopeObj.view.lblTargetCustomerRoles.skin = "sknlblLatoBold485c7512px";
        scopeObj.view.lblAdSpecifications.skin = "sknlblLatoRegular485c7512px";
        scopeObj.view.flxTargetCustomerRolesData.isVisible = true;
        scopeObj.view.flxChannelsDataFlowVertical.isVisible = false;
      }
    };

    // When Tab1 (Ad Specifications) is pressed.
    this.view.flxTab1.onClick = function(){
      if(scopeObj.view.flxUnderline1.isVisible===false){
        scopeObj.view.flxUnderline1.isVisible = true;
        scopeObj.view.flxUnderline2.isVisible = false;
        scopeObj.view.lblAdSpecifications.skin = "sknlblLatoBold485c7512px";
        scopeObj.view.lblTargetCustomerRoles.skin = "sknlblLatoRegular485c7512px";
        scopeObj.view.flxChannelsDataFlowVertical.isVisible = true;
        scopeObj.view.flxTargetCustomerRolesData.isVisible = false;
      }
    }; 

    this.view.flxClose.onClick = function(){
      scopeObj.view.flxViewAdPreview.setVisibility(false);
      scopeObj.view.richTextAdImage.text = "";
    };

    this.view.breadcrumbs.btnBackToMain.onClick = function(){
      scopeObj.presenter.fetchCampaigns();
    };

    // When Cursor is Hovering over Tab1 and it is not selected label skin has to change
    this.view.flxTab1.onHover = scopeObj.onHoverEventCallbackTab1;
    // When Cursor is Hovering over Tab2 and it is not selected label skin has to change
    this.view.flxTab2.onHover = scopeObj.onHoverEventCallbackTab2;

    scopeObj.view.mainHeader.btnDropdownList.onClick = function() {
      kony.adminConsole.utils.showProgressBar(scopeObj.view);
      scopeObj.presenter.getCampaignConfigurations();
    };

    this.view.selectOptions.flxOption1.onClick = function() {
      scopeObj.statusContextualMenuOptionOnSelect(scopeObj.view.selectOptions.lblOption1.text);
    };

    this.view.selectOptions.flxOption2.onClick = function() {
      scopeObj.statusContextualMenuOptionOnSelect(scopeObj.view.selectOptions.lblOption2.text);
    };

    this.view.selectOptions.flxOption3.onClick = function() {
      scopeObj.statusContextualMenuOptionOnSelect(scopeObj.view.selectOptions.lblOption3.text);
    };

    this.view.popUp.btnPopUpCancel.onClick = function() {
      scopeObj.view.flxAdManagementPopUp.setVisibility(false);
    };
    this.view.popUp.btnPopUpDelete.onClick = function() {
      scopeObj.updateCampaignStatus();
      scopeObj.view.flxAdManagementPopUp.setVisibility(false);
    };

    this.view.popUp.flxPopUpClose.onClick = function() {
      scopeObj.view.flxAdManagementPopUp.setVisibility(false);
    };
  },

  fetchedCampaignsResponse : function(context) {
    var scopeObj = this;

    if(context.fetchCampaigns === "success") {
      scopeObj.showCampaignList(context.campaignsList);
      this.campaignData = this.view.segCampaigns.data;
      scopeObj.sortBy = this.getObjectSorter("endDateTime");
      // setting the sort icon of endDateTime as descending order
      scopeObj.sortBy.inAscendingOrder = false;
      scopeObj.resetSortFontIcons();
      scopeObj.setStatusFilterMenu();
      this.campaignSpecification = [];
      this.isDefault = false;
    }
    else {
      scopeObj.view.toastMessage.showErrorToastMessage("Error in fetching Campaigns", scopeObj);
    }
    scopeObj.view.forceLayout();
  },

  showCampaignList: function(data){
    this.view.flxListing.setVisibility(true);
    this.view.flxViewDetails.setVisibility(false);
    this.view.mainHeader.flxButtons.setVisibility(true);
    this.view.flxBreadCrumbs.setVisibility(false);
    this.view.flxHeaderSeperator.setVisibility(true);
    this.view.subHeader.tbxSearchBox.text="";
    this.view.subHeader.flxClearSearchImage.setVisibility(false);
    this.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100"; 
    this.view.flxSelectOptions.setVisibility(false);
    var segData = data.map(this.mappingCampaignsdata);
    this.setCampaignsSegmentData(segData);
  },

  mappingCampaignsdata: function(data){
    var statusIcon, status, trimCharacters;
    if (this.view.flxListingMainContainer.frame.width > 1000)
      trimCharacters = 40;
    else
      trimCharacters = 25;
    status = this.getCampaignStatus(data.statusId, data.startDateTime, data.endDateTime);
    statusIcon = this.getCampaignStatusIcon(status);
    return {
      name: data.name,
      priority: data.statusId === "SID_TERMINATED" || status === "Completed" ? "0" : data.priority,
      startDateTime: data.startDateTime,
      endDateTime: data.endDateTime,
      lblCampaignsName: {
        text: this.trimSegText(data.name, trimCharacters),
        tooltip: data.name.length > trimCharacters ? data.name : "",
      },
      lblPriority: {
        text : data.statusId === "SID_TERMINATED" || status === "Completed"? "N/A" : data.priority > 9 ? data.priority : "0" + data.priority
      },
      lblStartDateTime: {
        text : data.startDateTime.substr(5,2) + "/" +  data.startDateTime.substr(8,2) + "/" + data.startDateTime.substr(0,4)
        //this.getFormatDate(data.startDateTime)
      },
      lblEndDateTime: {
        text : data.endDateTime.substr(5,2) + "/" +  data.endDateTime.substr(8,2) + "/" + data.endDateTime.substr(0,4)
        //this.getFormatDate(data.endDateTime)
      },
      lblStatus: {
        text : status
      },
      fontIconStatusImg: {
        text : statusIcon,
        skin : this.getCampaignStatusIconSkin(status)
      },
      flxOptions: {
        isVisible : data.statusId === "SID_TERMINATED" || status === "Completed"? false : true,
        onClick : this.toggleContextualMenu
      },
      lblIconOptions: {
        text : ""
      },
      flxDropdown: {
        onClick : this.showCampaignDescription,
      },
      fonticonArrow: {
        text : "\ue922",
        skin : "sknfontIconDescRightArrow14px"
      },
      flxCampaignExpand: {
        isVisible : false
      },
      lblDescriptionHeader: {
        text : kony.i18n.getLocalizedString("i18n.ConfigurationBundles.descriptionInCaps")
      },
      lblDescription: {
        text : data.description
      },
      lblSeparator: {
        text : ".",
        skin : "sknlblSeperator"
      },
      campaignId : data.id,
    };
  },

  setCampaignsSegmentData: function(data){
    var self = this;
    if(data.length === 0){
      self.showNoResultsFound(kony.i18n.getLocalizedString("i18n.frmLogs.rtxNorecords"));
    }
    else{
      self.view.flxNoResultFound.setVisibility(false);
      self.view.flxSegCampaigns.setVisibility(true);
      var dataMap = {
        flxCampaigns: "flxCampaigns",
        flxCampaignCollapse: "flxCampaignCollapse",
        lblCampaignsName: "lblCampaignsName",
        lblPriority: "lblPriority",
        lblStartDateTime: "lblStartDateTime",
        lblEndDateTime: "lblEndDateTime",
        flxStatus: "flxStatus",
        lblStatus: "lblStatus",
        fontIconStatusImg: "fontIconStatusImg",
        flxOptions: "flxOptions",
        lblIconOptions: "lblIconOptions",
        flxDropdown: "flxDropdown",
        fonticonArrow: "fonticonArrow",
        flxCampaignExpand: "flxCampaignExpand",
        lblDescriptionHeader: "lblDescriptionHeader",
        lblDescription: "lblDescription",
        lblSeparator: "lblSeparator",
      };
      self.view.segCampaigns.widgetDataMap = dataMap;
      self.view.segCampaigns.setData(data);
    }
    self.view.forceLayout();
  },

  trimSegText: function(text,len) {
    var final_text = text;
    if (text.length > len) final_text = text.substr(0, len) + "...";
    return final_text;
  },

  getFormatDate: function(dateTime) {
    var time = dateTime.substr(11);
    var hour = time.substr(0, time.indexOf(":"));
    var ampm = (hour < 12 || hour === 0) ? " AM" : " PM";
    hour = hour % 12 || 12;
    var minute = time.substr(time.indexOf(":"), 3);
    hour = hour < 10 ? "0" + hour : hour;
    time = hour +  minute + ampm;
    var date = dateTime.substr(5,2) + "/" +  dateTime.substr(8,2) + "/" + dateTime.substr(0,4) + " "; 
    return  (date + time);
  },

  getCampaignStatus: function(statusId, startDateTime, endDateTime) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    var startDT = new Date(startDateTime.substring(0,10).replace(/-/g,"/"));
    startDT.setHours(0, 0, 0, 0);
    var endDT = new Date(endDateTime.substring(0,10).replace(/-/g,"/"));
    endDT.setHours(0, 0, 0, 0);
    if(statusId === "SID_TERMINATED")
      return "Terminated";
    else if(statusId==="SID_PAUSED") {
      if(today>endDT)
        return "Completed";
      else
        return "Paused";
    } 
    else if(today>=startDT && today<=endDT)
      return "Active";
    else if(today<startDT)
      return "Scheduled";
    else if(today>endDT)
      return "Completed";
  },

  getCampaignStatusIcon: function(status){
    if(status==="Active")
      return "\ue921";
    else if(status==="Scheduled")
      return "\ue94a";
    else if(status==="Completed")
      return "\ue904";
    else if(status === "Terminated")
      return "\ue905";
    else if(status ==="Paused")
      return "\ue91d";
  },

  getCampaignStatusIconSkin: function(status){
    if(status==="Active")
      return "sknFontIconActivate";
    else if(status==="Completed")
      return "sknFontIconCompleted";
    else if(status==="Scheduled")
      return "sknFontIconScheduled";
    else if(status === "Terminated")
      return "sknFontIconTerminate";
    else if(status ==="Paused")
      return "sknFontIconPause";
    else
      return "sknFontIconOptionMenuRow";
  },

  showCampaignDescription: function(){
    var self = this;
    var index = self.view.segCampaigns.selectedRowIndex[1];
    var selItems = self.view.segCampaigns.selectedItems[0];
    if(selItems.fonticonArrow.text==="\ue922"){
      selItems.fonticonArrow.text = "\ue915";
      selItems.fonticonArrow.skin = "sknfontIconDescDownArrow12px";
      selItems.flxCampaignExpand.isVisible=true;
      self.view.segCampaigns.setDataAt(selItems, index);
    }
    else{
      selItems.fonticonArrow.text = "\ue922";
      selItems.fonticonArrow.skin = "sknfontIconDescRightArrow14px";
      selItems.flxCampaignExpand.isVisible=false;
      self.view.segCampaigns.setDataAt(selItems, index);
    }
    self.view.forceLayout();
  },

  showNoResultsFound: function(text){
    var self = this;
    self.view.flxNoResultFound.setVisibility(true);
    self.view.flxSegCampaigns.setVisibility(false);
    self.view.rtxSearchMesg.text=text;
    self.view.forceLayout();
  },

  toggleContextualMenu: function(type){
    var self = this;

    var height = 0;
    var left = 0;
    if(type==="viewDetails"){
      //showing contextual menu for View details page
      height = "164px";
      left = (this.view.flxTopDetails.frame.width - 116) + "px";
      self.view.selectOptions.flxSelectOptionsInner.top = "-1px";
      if(self.view.flxDefaultAdCampaign.isVisible===true)
        var contextualWidgetHeight = self.setContextualMenuItem("Default"); 
      else
        var contextualWidgetHeight = self.setContextualMenuItem(this.view.lblCampaignStatus.text); 
      self.view.selectOptions.flxArrowImage.setVisibility(true);
    }
    else {
      //showing contextual menu in segment of listing page
      var index = self.view.segCampaigns.selectedRowIndex[1];
      var templateArray = self.view.segCampaigns.clonedTemplates;
      var contextualWidgetHeight = self.setContextualMenuItem(templateArray[index].flxStatus.lblStatus.text);
      for (var i = 0; i < index; i++) {
        height += templateArray[i].flxCampaigns.frame.height;
      }
      self.view.selectOptions.flxArrowImage.setVisibility(true);
      self.view.selectOptions.flxSelectOptionsInner.top = "-1px";
      var segmentWidget = this.view.segCampaigns;
      var contextualWidget = this.view.flxSelectOptions;
      height = height + 50 - segmentWidget.contentOffsetMeasured.y;
      if (height + contextualWidgetHeight > segmentWidget.frame.height) {
        height = height - contextualWidgetHeight -30;
        self.view.selectOptions.flxArrowImage.setVisibility(false);
        self.view.selectOptions.flxSelectOptionsInner.top = "0px";
      }
      height = height + 220 + "px";
      left = templateArray[index].flxOptions.frame.x + 35 - 100 + "px";
    }
    if ((self.view.flxSelectOptions.isVisible === false) || (self.view.flxSelectOptions.isVisible === true && self.view.flxSelectOptions.top !== height)) {
      self.view.flxSelectOptions.top = height;
      self.view.flxSelectOptions.left = left;
      self.view.flxSelectOptions.setVisibility(true);
      self.view.flxSelectOptions.onHover = self.onHoverEventCallback;
    }
    else {
      self.view.flxSelectOptions.setVisibility(false);
    }
    self.view.forceLayout();
  },

  onHoverEventCallback : function(widget, context) {
    var scopeObj = this;

    var widGetId = widget.id;
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      scopeObj.view[widGetId].setVisibility(true);
    }
    else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      scopeObj.view[widGetId].setVisibility(false);
    }
  },

  searchCampaigns : function (text) {
    var self = this;
    if (text === "") {
      self.view.subHeader.tbxSearchBox.text="";
      self.view.subHeader.flxClearSearchImage.setVisibility(false);
      self.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100"; 
    }
    else {
      self.view.subHeader.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
      self.view.subHeader.flxClearSearchImage.setVisibility(true);
    }
    var segData = this.campaignData.filter(this.searchFilter);
    // to apply filter on searched Data
    segData = this.setStatusFilterOnSearch(segData);
    if(this.sortBy){
      this.setCampaignsSegmentData(segData.sort(this.sortBy.sortData));
    }
    else{
      this.setCampaignsSegmentData(segData);
    }
  },

  searchFilter : function (data) {
    var searchText = this.view.subHeader.tbxSearchBox.text;
    if(typeof searchText === 'string' && searchText.length > 0){
      return data.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
    }else{
      return true;
    }
  },

  setStatusFilterOnSearch: function (segData) {
    var data=[];
    for(var i=0; i<this.allPossibleFilterData.length; i++){
      if(this.statusfilterIndices[this.allPossibleFilterData[i]]){
        var x=[]
        for(var j=0; j<segData.length; j++)
          if(segData[j].lblStatus.text === this.allPossibleFilterData[i])
            x.push(segData[j]);
        data=data.concat(x);
      }
    }
    return data;
  },

  sortCampaignList : function (sortColumn) {
    var self = this;
    var segData = self.view.segCampaigns.data;
    var passSegData = [], x = [];

    self.sortBy.column(sortColumn);
    var sortOrder = self.sortBy.inAscendingOrder;
    segData = segData.sort(this.sortBy.sortData);

    // campaign name is again alphabetically sorted for same value of the coulum
    if(sortColumn !== "name"){
      self.sortBy.column("name");
      for(var i=0; i<segData.length-1; i++){
        if(segData[i][sortColumn] === segData[i+1][sortColumn])
          x.push(segData[i]);
        else{
          x.push(segData[i]);
          passSegData = passSegData.concat(x.sort(this.sortBy.sortData));
          x = [];
        }
      }
      x.push(segData[i]);
      passSegData = passSegData.concat(x.sort(this.sortBy.sortData));
      self.sortBy.columnName = sortColumn;
      self.sortBy.inAscendingOrder = sortOrder;
    }
    else
      passSegData = segData;
    self.resetSortFontIcons();
    self.setCampaignsSegmentData(passSegData);
  },

  resetSortFontIcons : function() {
    this.determineSortFontIcon(this.sortBy,'name',this.view.fontIconSortCampaignName);
    this.determineSortFontIcon(this.sortBy,'priority',this.view.fontIconSortCampaignPriority);
    this.determineSortFontIcon(this.sortBy,'startDateTime',this.view.fontIconSortCampaignStartDateTime);
    this.determineSortFontIcon(this.sortBy,'endDateTime',this.view.fontIconSortCampaignEndDateTime);
  },

  setStatusFilterMenu : function() {
    var segCampaignData = this.campaignData;
    var segStatusFilterData = [];
    this.allPossibleFilterData = ["Active","Paused","Completed","Scheduled","Terminated",];
    this.statusfilterIndices = [];
    // Global variable statusfilterIndices is set according to the data present in the segment.
    for(var i=0; i<this.allPossibleFilterData.length; i++){
      for(var j=0; j<segCampaignData.length; j++){
        if(segCampaignData[j].lblStatus.text === this.allPossibleFilterData[i]){
          this.statusfilterIndices[this.allPossibleFilterData[i]] = true;
          break;
        }
      }   
    }
    var widgetMap = {
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    this.view.statusFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
  },

  showStatusFilterMenu : function() {
    var segCampaignData = this.campaignData.filter(this.searchFilter).sort(this.sortBy.sortData);
    var segStatusFilterData = [], indices = [];

    for(var i=0, k=0; i<this.allPossibleFilterData.length; i++){
      for(var j=0; j<segCampaignData.length; j++){
        if(segCampaignData[j].lblStatus.text === this.allPossibleFilterData[i]){
          segStatusFilterData.push({
            lblDescription : this.allPossibleFilterData[i],
            imgCheckBox:{
              "src":"checkboxselected.png"
            }
          });
          if(this.statusfilterIndices[this.allPossibleFilterData[i]])
            indices.push(k++);
          else
            k++;
          break;
        }
      }   
    }

    this.view.statusFilterMenu.segStatusFilterDropdown.setData(segStatusFilterData);
    this.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices = [[0, indices]];
    this.view.flxCampaignStatusFilter.setVisibility(true);
    this.view.flxCampaignStatusFilter.onHover = this.onHoverEventCallback;
    //StatusHeader left + StatusHeader witdth - StatusFilter width
    this.view.flxCampaignStatusFilter.left=this.view.flxCampaignStatusHeader.frame.x + 76 - 130 + "px";
    this.view.forceLayout();
  },

  showFilterdCampaignsList : function(index) {
    var segStatusFilterData = this.view.statusFilterMenu.segStatusFilterDropdown.data;
    var segCampaignData = [], x = [], indices = [];
    var currentSegCampaignData = this.campaignData.filter(this.searchFilter).sort(this.sortBy.sortData);
    for(var i=0; i<segStatusFilterData.length; i++){
      x = [];
      if(index !== i && this.statusfilterIndices[segStatusFilterData[i].lblDescription]){
        for(var j=0; j<currentSegCampaignData.length; j++)
          if(currentSegCampaignData[j].lblStatus.text === segStatusFilterData[i].lblDescription)
            x.push(currentSegCampaignData[j]);
        indices.push(i);
      }
      else if(index == i && this.statusfilterIndices[segStatusFilterData[i].lblDescription] === false){
        for(var j=0; j<currentSegCampaignData.length; j++)
          if(currentSegCampaignData[j].lblStatus.text === segStatusFilterData[i].lblDescription)
            x.push(currentSegCampaignData[j]);
        indices.push(i);
        this.statusfilterIndices[segStatusFilterData[i].lblDescription] = true;
      }
      else
        this.statusfilterIndices[segStatusFilterData[i].lblDescription] = false;
      segCampaignData = segCampaignData.concat(x);
    }
    this.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices = [[0, indices]];
    this.setCampaignsSegmentData(segCampaignData.sort(this.sortBy.sortData));
    this.view.forceLayout();
  },

  setContextualMenuItem: function(status) {
    if (status === "Default"){
      this.view.selectOptions.flxOption1.setVisibility(true);
      this.view.selectOptions.fontIconOption1.text = kony.i18n.getLocalizedString("i18n.decisionManagement.editIcon");
      this.view.selectOptions.fontIconOption1.skin = this.getCampaignStatusIconSkin("Edit");
      this.view.selectOptions.lblOption1.text = kony.i18n.getLocalizedString("i18n.LeadManagement.contextualMenuEdit");
      this.view.selectOptions.flxOption2.setVisibility(false);
      this.view.selectOptions.fontIconOption2.text = "";
      this.view.selectOptions.lblOption2.text = "";
      this.view.selectOptions.flxOption3.setVisibility(false);
      this.view.selectOptions.fontIconOption3.text = "";
      this.view.selectOptions.lblOption3.text = "";
      return 46;
    }
    else if (status === "Active") {
      this.view.selectOptions.flxOption1.setVisibility(true);
      this.view.selectOptions.fontIconOption1.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.PauseIcon");
      this.view.selectOptions.fontIconOption1.skin = this.getCampaignStatusIconSkin("Paused");
      this.view.selectOptions.lblOption1.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.Pause");
      this.view.selectOptions.flxOption2.setVisibility(true);
      this.view.selectOptions.fontIconOption2.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.TerminateIcon");
      this.view.selectOptions.fontIconOption2.skin = this.getCampaignStatusIconSkin("Terminated");
      this.view.selectOptions.lblOption2.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.Terminate");
      this.view.selectOptions.flxOption3.setVisibility(false);
      this.view.selectOptions.fontIconOption3.text = "";
      this.view.selectOptions.lblOption3.text = "";
      return 81;
    } /*else if (status === "Completed") {
      this.view.selectOptions.flxOption1.setVisibility(true);
      this.view.selectOptions.fontIconOption1.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.RestartIcon");
      this.view.selectOptions.fontIconOption1.skin = this.getCampaignStatusIconSkin("Restart");
      this.view.selectOptions.lblOption1.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.Restart");
      this.view.selectOptions.flxOption2.setVisibility(false);
      this.view.selectOptions.fontIconOption2.text = "";
      this.view.selectOptions.lblOption2.text = "";
      this.view.selectOptions.flxOption3.setVisibility(false);
      this.view.selectOptions.fontIconOption3.text = "";
      this.view.selectOptions.lblOption3.text = "";
      return 46;
    }*/ else if (status === "Paused") {
      this.view.selectOptions.flxOption1.setVisibility(true);
      this.view.selectOptions.fontIconOption1.text = kony.i18n.getLocalizedString("i18n.decisionManagement.editIcon");
      this.view.selectOptions.fontIconOption1.skin = this.getCampaignStatusIconSkin("Edit");
      this.view.selectOptions.lblOption1.text = kony.i18n.getLocalizedString("i18n.LeadManagement.contextualMenuEdit");
      this.view.selectOptions.flxOption2.setVisibility(true);
      this.view.selectOptions.fontIconOption2.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.ResumeIcon");
      this.view.selectOptions.fontIconOption2.skin = this.getCampaignStatusIconSkin("Paused");
      this.view.selectOptions.lblOption2.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.Resume");
      this.view.selectOptions.flxOption3.setVisibility(true);
      this.view.selectOptions.fontIconOption3.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.TerminateIcon");
      this.view.selectOptions.fontIconOption3.skin = this.getCampaignStatusIconSkin("Terminated");
      this.view.selectOptions.lblOption3.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.Terminate");
      return 116;
    } else if (status === "Scheduled") {
      this.view.selectOptions.flxOption1.setVisibility(true);
      this.view.selectOptions.fontIconOption1.text = kony.i18n.getLocalizedString("i18n.decisionManagement.editIcon");
      this.view.selectOptions.fontIconOption1.skin = this.getCampaignStatusIconSkin("Edit");
      this.view.selectOptions.lblOption1.text = kony.i18n.getLocalizedString("i18n.LeadManagement.contextualMenuEdit");
      this.view.selectOptions.flxOption2.setVisibility(true);
      this.view.selectOptions.fontIconOption2.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.PauseIcon");
      this.view.selectOptions.fontIconOption2.skin = this.getCampaignStatusIconSkin("Paused");
      this.view.selectOptions.lblOption2.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.Pause");
      this.view.selectOptions.flxOption3.setVisibility(true);
      this.view.selectOptions.fontIconOption3.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.TerminateIcon");
      this.view.selectOptions.fontIconOption3.skin = this.getCampaignStatusIconSkin("Terminated");
      this.view.selectOptions.lblOption3.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.Terminate");
      return 116;
    }
  },

  statusContextualMenuOptionOnSelect: function(status) {
    this.selectedStatus = status;
    if(status === kony.i18n.getLocalizedString("i18n.LeadManagement.contextualMenuEdit")){
      //edit functionality in contextual Menu
      if(this.isDefault){
        this.defaultCampaignDetails.name = kony.i18n.getLocalizedString("i18n.frmAdManagement.DefaultAdCampaign");
        this.presenter.editDefaultCampaign(this.defaultCampaignDetails);
      }
      else
        this.updateCampaign();
    }
    /* else if(status === kony.i18n.getLocalizedString("i18n.frmAdManagement.Restart")){
      //in contextual Menu functionality for restart campaign 
      this.updateCampaignStatus();
    }*/
    else
      this.setCampaignPopUp(status);
  },

  createStatusIdToUpdateStatus: function(status) {
    if(status === kony.i18n.getLocalizedString("i18n.frmAdManagement.Terminate"))
      return "SID_TERMINATED";
    else if(status === kony.i18n.getLocalizedString("i18n.frmAdManagement.Pause"))
      return "SID_PAUSED";
    else if(status === kony.i18n.getLocalizedString("i18n.frmAdManagement.Resume")||status === kony.i18n.getLocalizedString("i18n.frmAdManagement.Restart"))
      return "SID_SCHEDULED_ACTIVE_COMPLETED";
  },

  setCampaignPopUp: function(status) {
    if(status === kony.i18n.getLocalizedString("i18n.frmAdManagement.Pause")){
      this.view.popUp.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.popUpHeaderPauseCampaign");
      this.view.popUp.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.popUpBodyPauseCampaign");
    }
    else if(status === kony.i18n.getLocalizedString("i18n.frmAdManagement.Resume")){
      this.view.popUp.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.popUpHeaderResumeCampaign");
      this.view.popUp.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.popUpBodyResumeCampaign");
    }
    else if(status === kony.i18n.getLocalizedString("i18n.frmAdManagement.Terminate")){
      this.view.popUp.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.popUpHeaderTerminateCampaign");
      this.view.popUp.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.popUpBodyTerminateCampaign");
    }
    this.view.flxAdManagementPopUp.setVisibility(true);
    this.view.popUp.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.PopUp.NoLeaveAsIS");
    this.view.popUp.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesProceed");
    this.view.forceLayout();
  },

  updateCampaignStatus: function() {
    var campaignId = this.view.segCampaigns.selectedRowItems[0].campaignId;
    var context = {
      "id" : campaignId,
      "statusId" : this.createStatusIdToUpdateStatus(this.selectedStatus),
    };
    this.presenter.updateCampaignStatus(context);
  },

  updateCampaign: function() {
    var campaignObj = this.view.segCampaigns.selectedRowItems[0];
    var startDate = campaignObj.startDateTime.substr(5,2) + "-" +  campaignObj.startDateTime.substr(8,2) + "-" + campaignObj.startDateTime.substr(0,4);
    var endDate = campaignObj.endDateTime.substr(5,2) + "-" +  campaignObj.endDateTime.substr(8,2) + "-" + campaignObj.endDateTime.substr(0,4);
    var context = {
      "id" : campaignObj.campaignId,
      "name" : campaignObj.name,
      "description" : campaignObj.lblDescription.text,
      "priority" : campaignObj.priority.toString(),
      "startDate" : startDate, 
      "endDate" : endDate,
    };
    this.presenter.getEditCampaignDetails(context);
  },

  changeVisibility: function(){
    let change = this.view.flxDefaultAdCampaign.isVisible === true ? true : false;

    this.view.NativeMobileChannelScreen1.setVisibility(!change);
    this.view.segPreLoginURLs.setVisibility(!change);
    this.view.NativeMobileChannelScreen2.setVisibility(!change);
    this.view.segAccDashURLs.setVisibility(!change);
    this.view.NativeMobileChannelScreen3.setVisibility(!change);
    this.view.segPostLoginURLs.setVisibility(!change);
    this.view.NativeTabletChannelScreen1.setVisibility(!change);
    this.view.segTabPreLoginURLs.setVisibility(!change);
    this.view.NativeTabletChannelScreen2.setVisibility(!change);
    this.view.segTabAccDashboardURLs.setVisibility(!change);
    this.view.WebAppChannelScreen1.setVisibility(!change);
    this.view.segWebAppAccDashboardURLs.setVisibility(!change);
    this.view.WebAppChannelScreen2.setVisibility(!change);
    this.view.segWebAppNewAccURLs.setVisibility(!change);

    this.view.MobileDefaultScreen1.setVisibility(change);
    this.view.segDefaultPreLoginURLs.setVisibility(change);
    this.view.MobileDefaultScreen2.setVisibility(change);
    this.view.segDefaultAccDashURLs.setVisibility(change);
    this.view.MobileDefaultScreen3.setVisibility(change);
    this.view.segDefaultPostLoginURLs.setVisibility(change);
    this.view.TabletDefaultScreen1.setVisibility(change);
    this.view.segDefaultTabPreLoginURLs.setVisibility(change);
    this.view.TabletDefaultScreen2.setVisibility(change);
    this.view.segDefaultTabAccDashboardURLs.setVisibility(change);
    this.view.WebDefaultScreen1.setVisibility(change);
    this.view.segDefaultWebAppAccDashboardURLs.setVisibility(change);
    this.view.WebDefaultScreen2.setVisibility(change);
    this.view.segDefaultWebAppNewAccURLs.setVisibility(change);
  },

  setDefaultSettingsForViewDetails: function(){
    //Initially TAB1 will be highlighted
    this.view.flxUnderline1.isVisible = true;
    this.view.flxUnderline2.isVisible = false;
    this.view.lblAdSpecifications.skin = "sknlblLatoBold485c7512px";
    this.view.lblTargetCustomerRoles.skin = "sknlblLatoRegular485c7512px";
    this.view.flxChannelsDataFlowVertical.isVisible = true;
    this.view.flxTargetCustomerRolesData.isVisible = false;

    this.view.mainHeader.flxButtons.setVisibility(false);
    this.view.flxHeaderSeperator.setVisibility(false);

    this.view.flxViewDetails.isVisible = true;
    this.view.flxListing.isVisible = false;

    this.view.lblIconRightArrow.isVisible = true;
    this.view.lblIconDownArrow.isVisible = false;
    this.view.flxSelectOptions.setVisibility(false);
  },

  setBreadCrumb: function(campaign_name){
    this.view.flxBreadCrumbs.setVisibility(true);
    this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.AdCampaignListCAPS");
    this.view.breadcrumbs.lblCurrentScreen.text = campaign_name.toUpperCase();
  },

  //Function to be called on onRowClick of Segment on Page 1
  displayDetails: function(campaignDetailsTop){
    this.setDefaultSettingsForViewDetails();
    this.setBreadCrumb(campaignDetailsTop.name);
    this.view.flxFirstLine.isVisible = true;
    this.view.flxSecondLine.isVisible = true;
    this.view.flxDefaultAdCampaign.isVisible = false;
    this.view.flxCampaignStatus.setVisibility(true);
    this.view.flxDescriptionBody.setVisibility(false);
    this.view.flxTab2.setVisibility(true);
    this.view.flxStatus.setVisibility(true);
    this.view.lblCampaignNameSelected.text = campaignDetailsTop.name;
    this.view.lblStartDate.text = campaignDetailsTop.lblStartDateTime.text;
    this.view.lblEndDate.text = campaignDetailsTop.lblEndDateTime.text;
    this.view.lblPriority.text = campaignDetailsTop.lblPriority.text;
    this.view.lblDescription.text = campaignDetailsTop.lblDescription.text;
    this.view.lblCampaignStatus.text = campaignDetailsTop.lblStatus.text;
    if(campaignDetailsTop.lblStatus.text=="Terminated"||campaignDetailsTop.lblStatus.text=="Completed")
      this.view.flxOptions.setVisibility(false);
    else
      this.view.flxOptions.setVisibility(true);
    this.view.lblFontIconStatusImg.text = this.getCampaignStatusIcon(campaignDetailsTop.lblStatus.text);
    this.view.lblFontIconStatusImg.skin = this.getCampaignStatusIconSkin(campaignDetailsTop.lblStatus.text);
    this.changeVisibility();

    this.presenter.fetchCampaignURLandGroups(campaignDetailsTop.campaignId);
    this.view.forceLayout();
  },

  //Function to be called when user Clicks on "Default Ad Campaign"
  displayDefaultAdDetails: function(){
    this.setDefaultSettingsForViewDetails();
    this.setBreadCrumb(kony.i18n.getLocalizedString("i18n.frmAdManagement.DefaultCampaignCAPS"));
    this.isDefault = true;
    this.view.flxFirstLine.isVisible = false;
    this.view.flxSecondLine.isVisible = false;
    this.view.flxDefaultAdCampaign.isVisible = true;
    this.view.flxCampaignStatus.setVisibility(true);
    this.view.flxStatus.setVisibility(false);
    this.view.flxTab2.setVisibility(true);
    this.populateTargetCustomerRoles([{"groupName":"All Customer Roles", "groupId":"All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019."}]);
    this.view.lblCampaignNameSelected.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.DefaultAdCampaign");
    this.changeVisibility();
    this.presenter.getDefaultCampaign();
    this.view.forceLayout();
  },

  //To change the skin of Label in Tab1 when cursor hovers over it.
  onHoverEventCallbackTab1: function(widget, context){
    var scopeObj = this;
    // This means the tab is not selected.
    if(scopeObj.view.flxUnderline1.isVisible===false){
      if (context.eventType === constants.ONHOVER_MOUSE_ENTER||context.eventType === constants.ONHOVER_MOUSE_MOVE) {
        this.view.lblAdSpecifications.skin = "sknLblUtilHover30353612pxReg";
      }
      else if(context.eventType === constants.ONHOVER_MOUSE_LEAVE){
        this.view.lblAdSpecifications.skin = "sknlblLatoRegular485c7512px";
      }
    }
  },

  //To change the skin of Label in Tab2 when cursor hovers over it.
  onHoverEventCallbackTab2: function(widget, context){
    var scopeObj = this;
    // This means the tab is not selected.
    if(scopeObj.view.flxUnderline2.isVisible===false){
      if (context.eventType === constants.ONHOVER_MOUSE_ENTER||context.eventType === constants.ONHOVER_MOUSE_MOVE) {
        scopeObj.view.lblTargetCustomerRoles.skin = "sknLblUtilHover30353612pxReg";
      } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
        scopeObj.view.lblTargetCustomerRoles.skin = "sknlblLatoRegular485c7512px";
      } 
    }
  },

  setDataForAdPreview: function(segName){
    var scopeObj = this;
    if(segName=="segPreLoginURLs"){
      scopeObj.view.lblChannelName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.NativeMobileApp");
      scopeObj.view.lblScreenName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.PreLoginScreen");
    }
    else if(segName=="segAccDashURLs"){
      scopeObj.view.lblChannelName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.NativeMobileApp");
      scopeObj.view.lblScreenName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.AccountDashboard");
    }
    else if(segName=="segPostLoginURLs"){
      scopeObj.view.lblChannelName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.NativeMobileApp");
      scopeObj.view.lblScreenName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.PostLoginFullScreenInterstitialAd");
    }
    else if(segName=="segTabPreLoginURLs"){
      scopeObj.view.lblChannelName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.NativeTabletApp");
      scopeObj.view.lblScreenName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.PreLoginScreen");
    }
    else if(segName=="segTabAccDashboardURLs"){
      scopeObj.view.lblChannelName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.NativeTabletApp");
      scopeObj.view.lblScreenName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.AccountDashboard");
    }
    else if(segName=="segWebAppAccDashboardURLs"){
      scopeObj.view.lblChannelName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.WebApp");
      scopeObj.view.lblScreenName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.AccountDashboard");
    }
    else if(segName=="segWebAppNewAccURLs"){
      scopeObj.view.lblChannelName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.WebApp");
      scopeObj.view.lblScreenName.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.ApplyforNewAccMod");
    }
  },

  displayImage: function(link,resolution,segName){
    var scopeObj = this;
    var img = new Image();
    img.onload = function(){
      scopeObj.setDataForAdPreview(segName);
      scopeObj.view.richTextAdImage.text = "<img src=\""+link+"\" alt=\"Image\">";
      scopeObj.view.flxImage.width = img.width;
      scopeObj.view.flxImage.height = img.height;
      var resWidth = resolution.substring(0,resolution.indexOf("x"));
      var resHeight = resolution.substring(resolution.indexOf("x")+1,resolution.length);
      scopeObj.view.flxBorder.width = parseInt(resWidth) + 20 + "px";
      scopeObj.view.flxBorder.height = parseInt(resHeight) + 20 + "px";
      scopeObj.view.flxViewAdPreview.setVisibility(true);  
      scopeObj.view.forceLayout(); 
    };
    img.src = link;
  },

  openSourceURL: function(widgetId, index){
    let segName = widgetId.containerId;
    let lblName = widgetId.children[0];
    let data = eval("this.view." + segName +".selectedRowItems");
    var link = data[0].lblImgSourceURL.tooltip ==="" ? data[0].lblImgSourceURL.text :data[0].lblImgSourceURL.tooltip; 
    this.displayImage(link,data[0].lblResolution.text,segName);
    //window.open(link);
  },

  openTargetURL: function(widgetId, index){
    let segName = widgetId.containerId;
    let lblName = widgetId.children[0];
    let data = eval("this.view." + segName +".selectedRowItems");
    var link = data[0].lblTargetURL.tooltip ==="" ? data[0].lblTargetURL.text :data[0].lblTargetURL.tooltip; 
    //this.displayImage(link,data[0].lblResolution.text);
    window.open(link);
  },

  // For enabling onClick, tooltip and trimText of URL.
  mappingCampaignsURLData: function(data) {
    var scopeObj = this;
    let breakPoint;
	let URLlength = "imageIndex" in data ? 28 : 42;
    if(data.imageScale[1]!='x'){
      if(data.imageScale=="640")
        breakPoint = kony.i18n.getLocalizedString("i18n.frmAdManagement.Mobile");
      else if(data.imageScale=="1024")
        breakPoint = kony.i18n.getLocalizedString("i18n.frmAdManagement.Tablet");
      else if(data.imageScale=="1366")
        breakPoint = kony.i18n.getLocalizedString("i18n.frmAdManagement.Desktop");
    }
    return {
      lblResolution: {
        text: data.imageResolution
      },
      flxImgSourceURL: {
        onClick: data.imageURL !== "" ? this.openSourceURL : undefined,
        skin: data.imageURL !== "" ? "sknFlxffffffCursorPointer" : "slFbox"
      },
      flxImgTargetURL: {
        onClick: data.destinationURL !== "" ? this.openTargetURL : undefined,
        skin: data.destinationURL !== "" ? "sknFlxffffffCursorPointer" : "slFbox"
      },
      lblImgSourceURL: {
        text: this.trimSegText(data.imageURL, URLlength),
        tooltip: data.imageURL.length > URLlength ? data.imageURL : ""
      },
      lblTargetURL: {
        text: this.trimSegText(data.destinationURL, URLlength),
        tooltip: data.destinationURL.length > URLlength ? data.destinationURL : ""
      },
      lblImageContainerNameScale: {
        text: data.imageScale[1]=='x' ? "Carousel Image_" + data.imageIndex + " - " + data.imageScale : breakPoint
      }
    };
  },

  // NATIVE MOBILE APP
  populateMobilePreLogin: function(mobilePreLogin,dataMapForSegmentURL){
    let mobilePreLoginData = mobilePreLogin.map(this.mappingCampaignsURLData);
    if(this.view.flxDefaultAdCampaign.isVisible===true){
      this.view.segDefaultPreLoginURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segDefaultPreLoginURLs.setData(mobilePreLoginData);
    }
    else{
      this.view.segPreLoginURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segPreLoginURLs.setData(mobilePreLoginData); 
    }
  },
  populateMobileAccountDashboard: function(mobileAccDash, dataMapForSegmentURL){
    let mobileAccDashData = mobileAccDash.map(this.mappingCampaignsURLData);
    if(this.view.flxDefaultAdCampaign.isVisible===true){
      this.view.segDefaultAccDashURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segDefaultAccDashURLs.setData(mobileAccDashData);
    }
    else{
      this.view.segAccDashURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segAccDashURLs.setData(mobileAccDashData);
    }
  },
  populateMobilePostLogin: function(mobilePostLogin, dataMapForSegmentURL){
    let mobilePostLoginData = mobilePostLogin.map(this.mappingCampaignsURLData);
    if(this.view.flxDefaultAdCampaign.isVisible===true){
      this.view.segDefaultPostLoginURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segDefaultPostLoginURLs.setData(mobilePostLoginData);
    }
    else{
      this.view.segPostLoginURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segPostLoginURLs.setData(mobilePostLoginData);
    }
  },
  populateMobileData: function(mobileData,dataMapForSegmentURL){
    this.view.flxMobileChannelFreeFrom.setVisibility(true);
    this.view.flxPreLoginMobileParent.setVisibility(true);
    this.view.flxAccountDashboardMobileParent.setVisibility(true);
    this.view.flxPostLoginMobileParent.setVisibility(true);

    let isPreLoginPresent = "PRE_LOGIN" in mobileData ? true : false;
    let isAccDashPresent = "ACCOUNT_DASHBOARD" in mobileData ? true : false;
    let isPostLoginPresent = "POST_LOGIN" in mobileData ? true : false;

    if(isPreLoginPresent===false && isAccDashPresent===false && isPostLoginPresent===false)
      this.view.flxMobileChannelFreeFrom.setVisibility(false);
    else if(isPreLoginPresent && isAccDashPresent && isPostLoginPresent){
      let mobilePreLogin = mobileData.PRE_LOGIN;
      let mobileAccDash = mobileData.ACCOUNT_DASHBOARD;
      let mobilePostLogin = mobileData.POST_LOGIN;
      if(mobilePreLogin.length===0 && mobileAccDash.length===0 && mobilePostLogin.length===0)
        this.view.flxMobileChannelFreeFrom.setVisibility(false);
      else{
        this.populateMobilePreLogin(mobilePreLogin, dataMapForSegmentURL);
        this.populateMobileAccountDashboard(mobileAccDash, dataMapForSegmentURL);
        this.populateMobilePostLogin(mobilePostLogin, dataMapForSegmentURL);
      }
    }
    else{
      // Native Mobile App: Pre Login Screen
      if(isPreLoginPresent){
        let mobilePreLogin = mobileData.PRE_LOGIN;
        if(mobilePreLogin.length===0)
          this.view.flxPreLoginMobileParent.setVisibility(false);
        else
          this.populateMobilePreLogin(mobilePreLogin, dataMapForSegmentURL);
      }
      else
        this.view.flxPreLoginMobileParent.setVisibility(false);

      // Native Mobile App : Account Dashboard
      if(isAccDashPresent){
        let mobileAccDash = mobileData.ACCOUNT_DASHBOARD;
        if(mobileAccDash.length===0)
          this.view.flxAccountDashboardMobileParent.setVisibility(false);
        else
          this.populateMobileAccountDashboard(mobileAccDash, dataMapForSegmentURL);
      }
      else
        this.view.flxAccountDashboardMobileParent.setVisibility(false);

      // Native Mobile App: Post Login Screen
      if(isPostLoginPresent){
        let mobilePostLogin = mobileData.POST_LOGIN;
        if(mobilePostLogin.length===0)
          this.view.flxPostLoginMobileParent.setVisibility(false);
        else
          this.populateMobilePostLogin(mobilePostLogin, dataMapForSegmentURL);
      }
      else
        this.view.flxPostLoginMobileParent.setVisibility(false);
    }
    this.view.forceLayout();
  },

  // NATIVE TABLET APP
  populateTabletPreLogin: function(tabletPreLogin, dataMapForSegmentURL){
    let tabletPreLoginData = tabletPreLogin.map(this.mappingCampaignsURLData);
    if(this.view.flxDefaultAdCampaign.isVisible===true){
      this.view.segDefaultTabPreLoginURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segDefaultTabPreLoginURLs.setData(tabletPreLoginData);
    }
    else{
      this.view.segTabPreLoginURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segTabPreLoginURLs.setData(tabletPreLoginData);
    }
  },
  populateTabletAccountDashboard: function(tabletAccDash, dataMapForSegmentURL){
    let tabletAccDashData = tabletAccDash.map(this.mappingCampaignsURLData);
    if(this.view.flxDefaultAdCampaign.isVisible===true){
      this.view.segDefaultTabAccDashboardURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segDefaultTabAccDashboardURLs.setData(tabletAccDashData);
    }
    else{
      this.view.segTabAccDashboardURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segTabAccDashboardURLs.setData(tabletAccDashData);
    }
  },
  populateTabletData: function(tabletData,dataMapForSegmentURL){
    this.view.flxTabletChannelFreeForm.setVisibility(true);
    this.view.flxPreLoginTabletParent.setVisibility(true);
    this.view.flxAccDashboardTabletParent.setVisibility(true);

    let isPreLoginPresent = "PRE_LOGIN" in tabletData ? true : false;
    let isAccDashPresent = "ACCOUNT_DASHBOARD" in tabletData ? true : false;

    if(isPreLoginPresent===false && isAccDashPresent===false)
      this.view.flxTabletChannelFreeForm.setVisibility(false);
    else if(isPreLoginPresent && isAccDashPresent){
      let tabletPreLogin = tabletData.PRE_LOGIN;
      let tabletAccDash = tabletData.ACCOUNT_DASHBOARD;
      if(tabletPreLogin.length===0 && tabletAccDash.length===0)
        this.view.flxTabletChannelFreeForm.setVisibility(false);
      else{
        this.populateTabletPreLogin(tabletPreLogin, dataMapForSegmentURL);
        this.populateTabletAccountDashboard(tabletAccDash, dataMapForSegmentURL);
      }
    }
    else{
      // Native Tablet App: Pre Login Screen
      if(isPreLoginPresent){
        let tabletPreLogin = tabletData.PRE_LOGIN;
        if(tabletPreLogin.length===0)
          this.view.flxPreLoginTabletParent.setVisibility(false);
        else
          this.populateTabletPreLogin(tabletPreLogin, dataMapForSegmentURL);
      }
      else
        this.view.flxPreLoginTabletParent.setVisibility(false);

      //Native Tablet App: Account Dashboard Screen
      if(isAccDashPresent){
        let tabletAccDash = tabletData.ACCOUNT_DASHBOARD;
        if(tabletAccDash.length===0)
          this.view.flxAccDashboardTabletParent.setVisibility(false);
        else
          this.populateTabletAccountDashboard(tabletAccDash, dataMapForSegmentURL);
      }
      else
        this.view.flxAccDashboardTabletParent.setVisibility(false);
    }
    this.view.forceLayout();
  },

  // NATIVE WEB APP
  populateWebAccountDeashboard: function(webAccDash, dataMapForSegmentURL){
    let webAccDashData = webAccDash.map(this.mappingCampaignsURLData);
    if(this.view.flxDefaultAdCampaign.isVisible===true){
      this.view.segDefaultWebAppAccDashboardURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segDefaultWebAppAccDashboardURLs.setData(webAccDashData);
    }
    else{
      this.view.segWebAppAccDashboardURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segWebAppAccDashboardURLs.setData(webAccDashData);
    }
  },
  populateApplyForNewAccountModule: function(webNewAccModule, dataMapForSegmentURL){
    let webNewAccModuleData = webNewAccModule.map(this.mappingCampaignsURLData);
    if(this.view.flxDefaultAdCampaign.isVisible===true){
      this.view.segDefaultWebAppNewAccURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segDefaultWebAppNewAccURLs.setData(webNewAccModuleData);
    }
    else{
      this.view.segWebAppNewAccURLs.widgetDataMap = dataMapForSegmentURL;
      this.view.segWebAppNewAccURLs.setData(webNewAccModuleData);
    }
  },
  populateWebData: function(webData,dataMapForSegmentURL){
    this.view.flxWebAppChannelFreeForm.setVisibility(true);
    this.view.flxAccDashboardWebAppParent.setVisibility(true);
    this.view.flxApplyForNewAccDashWebAppParent.setVisibility(true);

    let isAccountDashboardPresent = "ACCOUNT_DASHBOARD" in webData ? true : false;
    let isApplyForNewAccModulePresent = "APPLY_FOR_NEW_ACCOUNT" in webData ? true : false;

    if(isAccountDashboardPresent===false && isApplyForNewAccModulePresent===false)
      this.view.flxWebAppChannelFreeForm.setVisibility(false);
    else if(isAccountDashboardPresent && isApplyForNewAccModulePresent){
      let webAccDash = webData.ACCOUNT_DASHBOARD;
      let webNewAccModule = webData.APPLY_FOR_NEW_ACCOUNT;
      if(webAccDash.length===0 && webNewAccModule.length===0)
        this.view.flxWebAppChannelFreeForm.setVisibility(false);
      else{
        this.populateWebAccountDeashboard(webAccDash, dataMapForSegmentURL);
        this.populateApplyForNewAccountModule(webNewAccModule, dataMapForSegmentURL);
      }
    }
    else{
      // Native Web App: Account Dashboard Screen
      if(isAccountDashboardPresent){
        let webAccDash = webData.ACCOUNT_DASHBOARD;
        if(webAccDash.length===0)
          this.view.flxAccDashboardWebAppParent.setVisibility(false);
        else
          this.populateWebAccountDeashboard(webAccDash, dataMapForSegmentURL);
      }
      else
        this.view.flxAccDashboardWebAppParent.setVisibility(false);

      // Native Web App: Apply for New Account Module
      if(isApplyForNewAccModulePresent){
        let webNewAccModule = webData.APPLY_FOR_NEW_ACCOUNT;
        if(webNewAccModule.length===0)
          this.view.flxApplyForNewAccDashWebAppParent.setVisibility(false);
        else
          this.populateApplyForNewAccountModule(webNewAccModule, dataMapForSegmentURL);
      }
      else 
        this.view.flxApplyForNewAccDashWebAppParent.setVisibility(false); 


    }
    this.view.forceLayout();
  },

  /** 
  * Function for Populating URL Data in Channels' Segments.
  * Called in willUpdateUI
  **/
  populateChannelData: function(URLdata){
    var dataMapForSegmentURL = {"lblResolution":"lblResolution",
                                "lblImgSourceURL":"lblImgSourceURL",
                                "lblTargetURL":"lblTargetURL",
                                "flxImgSourceURL": "flxImgSourceURL",
                                "flxImgTargetURL": "flxImgTargetURL",
                                "lblImageContainerNameScale": "lblImageContainerNameScale"};
    if("MOBILE" in URLdata){
      var mobileData = URLdata.MOBILE;
      this.populateMobileData(mobileData,dataMapForSegmentURL);
    }
    else
      this.view.flxMobileChannelFreeFrom.setVisibility(false);
    if("TABLET" in URLdata){
      var tabletData = URLdata.TABLET;
      this.populateTabletData(tabletData,dataMapForSegmentURL);
    }
    else
      this.view.flxTabletChannelFreeForm.setVisibility(false);
    if("WEB" in URLdata){
      var webData = URLdata.WEB;
      this.populateWebData(webData,dataMapForSegmentURL); 
    }
    else
      this.view.flxWebAppChannelFreeForm.setVisibility(false);
    this.view.forceLayout();
  },

  populateTargetCustomerRoles: function(customerData){
    this.view.segTargetCustomerRoles.widgetDataMap = {"lblRoleName":"groupName",
                                                      "lblRoleDescription":"groupId"};
    this.view.segTargetCustomerRoles.setData(customerData);
  },

  showToastMsg : function(context){
    if (context.updatedCampaignStatus === "success") {
      this.view.toastMessage.showToastMessage(context.toastMessage, this);
    } 
    else {
      this.view.toastMessage.showErrorToastMessage(context.toastMessage, this);
    }
  },

});