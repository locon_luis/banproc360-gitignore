define({ 

  isCreate : false,
  isDefaultCampaign : false,
  isEdit : false,
  campaignSpecifications : {},
  defaultCampaignObject : {},
  channelNameMapper : {
    "MOBILE" : kony.i18n.getLocalizedString("i18n.frmAdManagement.NativeMobileApp"),
    "WEB" : kony.i18n.getLocalizedString("i18n.frmAdManagement.WebApp")
  },
  moduleNameMapper : {
    "PRE_LOGIN" : kony.i18n.getLocalizedString("i18n.frmAdManagement.PreLoginScreen"),
    "POST_LOGIN" : kony.i18n.getLocalizedString("i18n.frmAdManagement.PostLoginFullScreenInterstitialAd"),
    "ACCOUNT_DASHBOARD" : kony.i18n.getLocalizedString("i18n.frmAdManagement.AccountDashboard"),
    "APPLY_FOR_NEW_ACCOUNT" : kony.i18n.getLocalizedString("i18n.frmAdManagement.ApplyforNewAcc")
  },
  breakpointMapper : {
    "640" : kony.i18n.getLocalizedString("i18n.frmAdManagement.Mobile"),
    "1024" : kony.i18n.getLocalizedString("i18n.frmAdManagement.Tablet"),
    "1366" : kony.i18n.getLocalizedString("i18n.frmAdManagement.Desktop")
  },
  availableCustomerGroups : [],
  groupsToAdd : [],
  groupsToDelete : [],
  hasValidBasicDetails : false,
  hasValidSpecifiactions : false,
  previousPriority : "",
  campaignData : {},
  existingCustomerGroups : [],
  urlRegex : /\b(https|ftp):(\/\/|\\\\)[^\s]+\b$/,

  willUpdateUI: function (context) {
    var scopeObj = this;
    if(context) {
      this.updateLeftMenu(context);
      if(context.action && context.action === "createCampaign") {
        scopeObj.isCreate = true;
        scopeObj.isEdit = false;
        scopeObj.isDefaultCampaign = false;
        scopeObj.campaignSpecifications = {"channels" : context.channels};
        scopeObj.resetAllFields();
        scopeObj.view.breadcrumbs.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.CreateCampaignCAPS");
      }
      if(context.action && context.action === "editCampaign") {
        scopeObj.isCreate = false;
        scopeObj.isEdit = true;
        scopeObj.isDefaultCampaign = false;
        if(context.hasSpecifications) {
          scopeObj.campaignSpecifications = context.campaignConfigurations ;
        }
        scopeObj.campaignData = context.campaignData ;
        scopeObj.resetAllFields();
        scopeObj.setEditCampaignData();
        scopeObj.view.breadcrumbs.lblCurrentScreen.text = context.campaignData.name.toUpperCase();
      }
      if(context.action && context.action === "defaultCampaign") {
        scopeObj.isCreate = false;
        scopeObj.isEdit = false;
        scopeObj.isDefaultCampaign = true;
        scopeObj.setDefaultCampaignSpecifications(context.defaultCampaignObject);
        scopeObj.resetAllFields();
        scopeObj.view.breadcrumbs.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.DefaultCampaignCAPS");
      }
      if(context.action && context.action === "CampaignPriorityList") {
        scopeObj.showCampaignsList(context.campaignList);
      }
      if(context.action && context.action === "checkPriority") {
        scopeObj.hasExistingPriority(context.campaignList);
      }
      if(context.action && context.action === "getCustomerGroups") {
        scopeObj.formatCustomerRolesData(context.customerRoles);
        scopeObj.showCustomerRoles();
        scopeObj.setLeftNavSkins(false, false, true);        
      }
      if (context.action && context.action === "ErrorOccured") {
        kony.adminConsole.utils.hideProgressBar(scopeObj.view);
        scopeObj.view.toastMessage.showErrorToastMessage("Something went wrong. Please try again.", scopeObj);
      }
      if (context.toastModel) {
        if (context.toastModel.status === "SUCCESS") {
          scopeObj.view.toastMessage.showToastMessage(context.toastModel.message, scopeObj);
          kony.adminConsole.utils.hideProgressBar(scopeObj.view);
        } else {
          scopeObj.view.toastMessage.showErrorToastMessage(context.toastModel.message, scopeObj);
          kony.adminConsole.utils.hideProgressBar(scopeObj.view);
        }
      }
      if (context.LoadingScreen) {
        if (context.LoadingScreen.focus)
          kony.adminConsole.utils.showProgressBar(scopeObj.view);
        else
          kony.adminConsole.utils.hideProgressBar(scopeObj.view);
      }
    }
    scopeObj.view.forceLayout();
  },

  frmCampaignDetailsPreShow : function() {
    var scopeObj = this ;
    scopeObj.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
    var containerHeight = kony.os.deviceInfo().screenHeight - 145;
    scopeObj.view.flxContainer.height = containerHeight +"dp";
    scopeObj.view.flxDetails.height = containerHeight +"dp";
    scopeObj.view.flxContent.height = (containerHeight - 80) +"dp";
    scopeObj.view.flxAddOptionsSegment.height = (containerHeight - 195) + "dp";
    scopeObj.view.flxSegSelectedOptions.height = (containerHeight - 130) + "dp";
    scopeObj.view.commonButtons.btnSave.skin = "sknBtnFFFFFFLatoBold006CCA13pxKA";
    scopeObj.view.commonButtons.btnSave.hoverSkin = "sknBtnFFFFFFLatoBold006CCA13pxKA";
    scopeObj.view.flxToastMessage.setVisibility(false);
    scopeObj.setFlowActions();
    scopeObj.closePopup();
  },

  frmCampaignDetailsPostShow : function(){
    var scopeObj = this ;
    scopeObj.AdminConsoleCommonUtils.restrictTextFieldToNumeric("frmAdDetails_txtPriority");
    scopeObj.setLeftNavSkins(true, false, false);
    if(scopeObj.isEdit){
      setTimeout(function() {
        scopeObj.view.customStartDate.value = scopeObj.campaignData.startDate;
        scopeObj.view.customStartDate.resetData = scopeObj.campaignData.startDate;
        scopeObj.view.customEndDate.value = scopeObj.campaignData.endDate;
        scopeObj.view.customEndDate.resetData = scopeObj.campaignData.endDate;
      }, 100);
    } else {
      scopeObj.view.customStartDate.value = "";
      scopeObj.view.customEndDate.value = "";
    }
  },

  setFlowActions: function() {
    var scopeObj = this;

    scopeObj.view.tbxSearchBox.onKeyUp = function() {
      scopeObj.view.flxClearSearch.setVisibility(true);
      scopeObj.showCustomerRoles();
    };

    scopeObj.view.tbxSearchBox.onTouchStart = function() {
      scopeObj.view.flxClearSearch.setVisibility(true);
    };

    scopeObj.view.tbxSearchBox.onTouchEnd = function() {
      scopeObj.view.flxClearSearch.setVisibility(false);
    };

    scopeObj.view.flxClearSearch.onClick = function(){
      scopeObj.view.tbxSearchBox.text = "";
      scopeObj.showCustomerRoles();
    };

    scopeObj.view.tbxSearchBox.onTouchStart = function() {
      scopeObj.view.flxClearSearch.setVisibility(true);
    };

    scopeObj.view.breadcrumbs.btnBackToMain.onClick = function() {
      scopeObj.presenter.fetchCampaigns();
    };

    scopeObj.view.commonButtons.btnCancel.onClick = function() {
      scopeObj.presenter.fetchCampaigns();
    };

    scopeObj.view.lblAdDetailsHeader.onClick = function() {
      scopeObj.setLeftNavSkins(true, false, false);
    };

    scopeObj.view.lblAdSpecificationsHeader.onClick = function() {
      scopeObj.validateBasicDetails();
    };

    scopeObj.view.lblTargetCustomersHeader.onClick = function() {
      scopeObj.validateSpecifications();
    };

    scopeObj.view.txtAdName.onKeyUp = function() {
      scopeObj.view.txtAdName.skin = "txtD7d9e0";
      scopeObj.view.AdNameError.setVisibility(false);
      var textLen = scopeObj.view.txtAdName.text.length ;
      scopeObj.view.lblAdNameSize.text = textLen + "/" + scopeObj.view.txtAdName.maxtextlength;
      scopeObj.view.lblAdNameSize.setVisibility(true);
      scopeObj.view.flxBasicDetails.forceLayout();
    };

    scopeObj.view.txtAdName.onTouchStart = function() {
      var textLen = scopeObj.view.txtAdName.text.length ;
      scopeObj.view.lblAdNameSize.text = textLen + "/" + scopeObj.view.txtAdName.maxtextlength;
      scopeObj.view.lblAdNameSize.setVisibility(true);
      scopeObj.view.flxAdName.forceLayout();
    };

    scopeObj.view.txtAdName.onEndEditing = function(){
      scopeObj.view.lblAdNameSize.setVisibility(false);
    };

    scopeObj.view.txtDescription.onKeyUp = function() {
      scopeObj.view.txtDescription.skin = "skntxtAread7d9e0";
      scopeObj.view.AdDescriptionError.setVisibility(false);
      var textLen = scopeObj.view.txtDescription.text.length ;
      scopeObj.view.lblDescriptionSize.text = textLen + "/" + scopeObj.view.txtDescription.maxtextlength;
      scopeObj.view.lblDescriptionSize.setVisibility(true);
      scopeObj.view.flxBasicDetails.forceLayout();
    };

    scopeObj.view.txtDescription.onTouchStart = function() {
      var textLen = scopeObj.view.txtDescription.text.length ;
      scopeObj.view.lblDescriptionSize.text = textLen + "/" + scopeObj.view.txtDescription.maxtextlength;
      scopeObj.view.lblDescriptionSize.setVisibility(true);
      scopeObj.view.flxDescription.forceLayout();
    };

    scopeObj.view.txtDescription.onEndEditing = function(){
      scopeObj.view.lblDescriptionSize.setVisibility(false);
    };

    scopeObj.view.txtPriority.onKeyUp = function() {
      scopeObj.view.txtPriority.skin = "txtD7d9e0";
      scopeObj.view.PriorityError.setVisibility(false);
      scopeObj.view.flxBasicDetails.forceLayout();
    };

    scopeObj.view.txtPriority.onTextChange = function() {
      var priority = scopeObj.view.txtPriority.text.trim();
      if(priority !== "" && !scopeObj.isDefaultCampaign && (parseInt(priority) >= 1 && parseInt(priority) <= 99)) {
        scopeObj.presenter.fetchCampaignPriorities(false);
      }
    };

    scopeObj.view.btnPriorityList.onClick = function() {
      scopeObj.presenter.fetchCampaignPriorities(true);
    };

    scopeObj.view.flxStartDate.onTouchStart = function(){
      scopeObj.view.flxStartDate.skin = "sknFlxCalendar";
      scopeObj.view.StartDateError.setVisibility(false);
      scopeObj.view.forceLayout();
    };

    scopeObj.view.flxEndDate.onTouchStart = function(){
      scopeObj.view.flxEndDate.skin = "sknFlxCalendar";
      scopeObj.view.EndDateError.setVisibility(false);
      scopeObj.view.forceLayout();
    };

    scopeObj.view.lblPopupClose.onClick = function() {
      scopeObj.closePopup();
    };

    scopeObj.view.popUp.btnPopUpCancel.onClick = function() {
      if(scopeObj.view.popUp.lblPopUpMainMessage.text === kony.i18n.getLocalizedString("i18n.frmAdManagement.ExistingPriorityConflictHeader")){
        scopeObj.view.txtPriority.text = scopeObj.previousPriority;
      }
      scopeObj.closePopup();
    };

    scopeObj.view.popUp.btnPopUpDelete.onClick = function() {
      if(scopeObj.view.popUp.lblPopUpMainMessage.text === kony.i18n.getLocalizedString("i18n.frmAdManagement.ExistingPriorityConflictHeader")){
        scopeObj.previousPriority = scopeObj.view.txtPriority.text;
      }
      scopeObj.closePopup();
    };

    scopeObj.view.popUp.flxPopUpClose.onClick = function() {
      scopeObj.closePopup();
    };

    scopeObj.view.btnAddAll.onClick = function(){
      scopeObj.addAllGroups();
    };

    scopeObj.view.btnRemoveAll.onClick = function(){
      scopeObj.deleteAllGroups();
    };

    scopeObj.view.flxClose.onClick = function() {
      scopeObj.closePopup();
    };

    scopeObj.view.commonButtons.btnSave.onClick = function(){
      if(scopeObj.view.flxBasicDetails.isVisible) {
        scopeObj.validateBasicDetails();
      } else if(scopeObj.view.flxAdSpecifications.isVisible) {
        scopeObj.validateSpecifications();
      } else if(scopeObj.view.segSelectedOptions.data.length === 0 && !scopeObj.isDefaultCampaign){
        scopeObj.showPopupMessgae(true,kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorCampaignCustomersMandatory"),
                                  kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorCampaignCustomersMandatoryMsg"));
      } else {
        scopeObj.updateJSON();
      }
    };

    scopeObj.view.lblCampaignHeader.onClick = function(){
      scopeObj.sortPriorityList('name');
    };

    scopeObj.view.lblCampaignSort.onClick = function(){
      scopeObj.sortPriorityList('name');
    };

    scopeObj.view.lblPriorityHeader.onClick = function(){
      scopeObj.sortPriorityList('priority');
    };

    scopeObj.view.lblPrioritySort.onClick = function(){
      scopeObj.sortPriorityList('priority');
    };

  },

  resetAllFields : function() {
    var scopeObj = this ;
    scopeObj.view.txtAdName.text = scopeObj.isDefaultCampaign ? scopeObj.defaultCampaignObject.name : "";
    scopeObj.view.txtDescription.text = scopeObj.isDefaultCampaign ? scopeObj.defaultCampaignObject.description : "";
    scopeObj.view.txtPriority.text = scopeObj.isDefaultCampaign ? "00" : "";
    scopeObj.view.lblPriorityInfoText.text = scopeObj.isDefaultCampaign ? kony.i18n.getLocalizedString("i18n.frmAdManagement.DefaultCampaignPriority") : kony.i18n.getLocalizedString("i18n.frmAdManagement.PriorityInfoText");
    scopeObj.view.tbxSearchBox.text = "";
    scopeObj.previousPriority = "";
    scopeObj.groupsToAdd = [];
    scopeObj.groupsToDelete = [];
    scopeObj.resetSkins();
    scopeObj.view.segSelectedOptions.removeAll();
    scopeObj.view.segAddOptions.removeAll();
    scopeObj.hasSelectedCustomersData();
    scopeObj.hasAvailableCustomersData();
    scopeObj.view.txtAdName.setEnabled(!scopeObj.isDefaultCampaign);
    scopeObj.view.txtPriority.setEnabled(!scopeObj.isDefaultCampaign);
    scopeObj.view.txtPriority.secureTextEntry = scopeObj.isDefaultCampaign;
    scopeObj.view.flxDateContainer.setVisibility(!scopeObj.isDefaultCampaign);
    scopeObj.view.rtxDefaultDateInfo.setVisibility(scopeObj.isDefaultCampaign);
    scopeObj.constructAdSpecifications();
  },

  resetSkins : function() {
    var scopeObj = this ;
    scopeObj.view.txtAdName.skin = scopeObj.isDefaultCampaign ? "txtD7d9e0disabledf3f3f3" : "txtD7d9e0";
    scopeObj.view.AdNameError.setVisibility(false);
    scopeObj.view.txtDescription.skin = "skntxtAread7d9e0";
    scopeObj.view.AdDescriptionError.setVisibility(false);
    scopeObj.view.txtPriority.skin = scopeObj.isDefaultCampaign ? "txtD7d9e0disabledf3f3f3" : "txtD7d9e0";
    scopeObj.view.PriorityError.setVisibility(false);
    scopeObj.view.flxStartDate.skin = "sknFlxCalendar";
    scopeObj.view.flxEndDate.skin = "sknFlxCalendar";
    scopeObj.view.StartDateError.setVisibility(false);
    scopeObj.view.EndDateError.setVisibility(false);
  },

  validateBasicDetails : function() {
    var scopeObj = this;
    scopeObj.resetSkins();
    scopeObj.hasValidBasicDetails = true;
    if(scopeObj.view.txtDescription.text.trim() === "") {
      scopeObj.view.txtDescription.skin = "sknTxtError" ;
      scopeObj.view.AdDescriptionError.setVisibility(true);
      scopeObj.hasValidBasicDetails = false;
    }
    if(!scopeObj.isDefaultCampaign){
      if(scopeObj.view.txtAdName.text.trim() === "") {
        scopeObj.view.txtAdName.skin = "skinredbg" ;
        scopeObj.view.AdNameError.setVisibility(true);
        scopeObj.hasValidBasicDetails = false;
      }
      var priority = scopeObj.view.txtPriority.text.trim();
      if(priority === "") {
        scopeObj.view.txtPriority.skin = "skinredbg" ;
        scopeObj.view.PriorityError.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorPriority");
        scopeObj.view.PriorityError.setVisibility(true);
        scopeObj.hasValidBasicDetails = false;
      } else if(parseInt(priority) < 1 || parseInt(priority) > 99) {
        scopeObj.view.txtPriority.skin = "skinredbg" ;
        scopeObj.view.PriorityError.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorPriorityRange");
        scopeObj.view.PriorityError.setVisibility(true);
        scopeObj.hasValidBasicDetails = false;
      }
      var startDate = scopeObj.view.customStartDate.value.trim().replace(/-/g,"/");
      var isValidateDates = scopeObj.isCreate || (scopeObj.isEdit && startDate !== scopeObj.campaignData.startDate.replace(/-/g,"/") );
      var maxStartDate = new Date().setDate(new Date().getDate()+30) ;
      if(startDate === "" ) {
        scopeObj.view.StartDateError.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorStartDate");
        scopeObj.view.StartDateError.setVisibility(true);
        scopeObj.view.flxStartDate.skin = "sknFlxCalendarError";
        scopeObj.hasValidBasicDetails = false;
      } else if(isValidateDates && scopeObj.isInValidDate(new Date(),new Date(startDate)) ) {
        scopeObj.view.StartDateError.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorStartDateLessThanToday");
        scopeObj.view.StartDateError.setVisibility(true);
        scopeObj.view.flxStartDate.skin = "sknFlxCalendarError";
        scopeObj.hasValidBasicDetails = false;
      } else if(isValidateDates && scopeObj.isInValidDate(new Date(startDate),new Date(maxStartDate))){
        scopeObj.view.StartDateError.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorEndDateLessThan30Days");
        scopeObj.view.StartDateError.setVisibility(true);
        scopeObj.view.flxStartDate.skin = "sknFlxCalendarError";
        scopeObj.hasValidBasicDetails = false;
      }
      var endDate = scopeObj.view.customEndDate.value.trim().replace(/-/g,"/");
      if(endDate === "" ) {
        scopeObj.view.EndDateError.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorEndDate");
        scopeObj.view.EndDateError.setVisibility(true);
        scopeObj.view.flxEndDate.skin = "sknFlxCalendarError";
        scopeObj.hasValidBasicDetails = false;
      } else if(scopeObj.isInValidDate(new Date(startDate),new Date(endDate), true)){
        scopeObj.view.EndDateError.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorEndDateLessThanStartDate");
        scopeObj.view.EndDateError.setVisibility(true);
        scopeObj.view.flxEndDate.skin = "sknFlxCalendarError";
        scopeObj.hasValidBasicDetails = false;
      }
    }
    if(scopeObj.hasValidBasicDetails){
      scopeObj.setLeftNavSkins(false, true, false);
    }
  },

  isInValidDate: function(date1, date2, isEndDateValidation ){
    var isInValid = false;
    if (isNaN(date1.getDate()) || isNaN(date2.getDate())) {
      isInValid = true;
    } 
    date1.setHours(0, 0, 0, 0);
    date2.setHours(0, 0, 0, 0);
    if(date1 - date2 > 0 || (isEndDateValidation && date1 - date2 === 0)) {
      isInValid = true;
    }
    return isInValid;
  },

  closePopup : function() {
    var scopeObj = this;
    scopeObj.view.flxConsentPopup.setVisibility(false);
    scopeObj.view.flxCampaignList.setVisibility(false);
    scopeObj.view.flxDisplayImage.setVisibility(false);
    scopeObj.view.popUp.setVisibility(false);
  },

  setLeftNavSkins : function(screen1,screen2,screen3){
    var scopeObj = this;
    var btnText = scopeObj.isCreate ? kony.i18n.getLocalizedString("i18n.frmAdManagement.CreateCAPS") : kony.i18n.getLocalizedString("i18n.frmAdManagement.UpdateCAPS");
    scopeObj.view.fontIconImgSelected1.setVisibility(screen1);
    scopeObj.view.fontIconImgSelected2.setVisibility(screen2);
    scopeObj.view.fontIconImgSelected3.setVisibility(screen3);
    scopeObj.view.flxBasicDetails.setVisibility(screen1);
    scopeObj.view.flxAdSpecifications.setVisibility(screen2);
    scopeObj.view.flxTargetCustomers.setVisibility(screen3);
    scopeObj.view.lblAdDetailsHeader.skin = screen1 ? "sknLblLatoBold12px485C75" : "sknLbl485C75LatoRegular12Px" ;
    scopeObj.view.lblAdDetailsHeader.hoverSkin = screen1 ? "sknlblBold485c7512pxHoverCursor" : "sknlbl485c7512pxHoverCursor" ;
    scopeObj.view.lblAdSpecificationsHeader.skin = screen2 ? "sknLblLatoBold12px485C75" : "sknLbl485C75LatoRegular12Px" ;
    scopeObj.view.lblAdSpecificationsHeader.hoverSkin = screen2 ? "sknlblBold485c7512pxHoverCursor" : "sknlbl485c7512pxHoverCursor" ;
    scopeObj.view.lblTargetCustomersHeader.skin = screen3 ? "sknLblLatoBold12px485C75" : "sknLbl485C75LatoRegular12Px" ;
    scopeObj.view.lblTargetCustomersHeader.hoverSkin = screen3 ? "sknlblBold485c7512pxHoverCursor" : "sknlbl485c7512pxHoverCursor" ;
    scopeObj.view.commonButtons.btnSave.text = screen3 ? btnText : kony.i18n.getLocalizedString("i18n.frmAdManagement.Next") ;
    scopeObj.view.flxAvailableOptions.setVisibility(!scopeObj.isDefaultCampaign);
    scopeObj.view.flxSelectedOptions.setVisibility(!scopeObj.isDefaultCampaign);
    scopeObj.view.rtxdefaultCampaignCustomerMsg.setVisibility(scopeObj.isDefaultCampaign);
    scopeObj.view.forceLayout();
  },

  constructAdSpecifications : function() {
    var scopeObj = this;
    var isWeb = false;
    scopeObj.view.flxAdSpecifications.removeAll();
    scopeObj.existingCustomerGroups = [];
    var specificationsArray = scopeObj.isDefaultCampaign ? scopeObj.defaultCampaignObject.specifications : scopeObj.campaignSpecifications;
    var channelTemplate = "" ;
    for(var key in scopeObj.channelNameMapper){
      for(var i = 0; i<specificationsArray.channels.length; i++) {
        var channelObj = specificationsArray.channels[i] ;
        if(channelObj.channelId === key){
          isWeb = key === kony.i18n.getLocalizedString("i18n.frmAdManagement.WebCAPS");
          channelTemplate = new com.adminConsole.adManagement.channelTemplate({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "channelTemplate"+channelObj.channelId.split('_').join(""),
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25dp",
            "width": "100%",
            "overrides": {
              "lblChannelName" : {
                "text" : scopeObj.channelNameMapper[channelObj.channelId]
              },
              "lblChannelArrow" :  {
                "onClick" : function(eventobject) {
                  scopeObj.showOrHideChannel(eventobject);
                }
              }
            }
          }, {
            "overrides": {}
          }, {
            "overrides": {}
          });
          var screensArray = channelObj.screens ;
          scopeObj.constructScreens(screensArray, channelTemplate, isWeb);
          scopeObj.view.flxAdSpecifications.add(channelTemplate);
        }
      }
    }

  },

  constructScreens : function(screensArray, channelTemplate, isWeb){
    var scopeObj = this;
    for(var key in scopeObj.moduleNameMapper){
      for(var i = 0; i<screensArray.length; i++) {
        var screenObj = screensArray[i] ;
        if(screenObj.screenId === key){
          var moduleName = scopeObj.moduleNameMapper[screenObj.screenId] ? scopeObj.moduleNameMapper[screenObj.screenId] : "";
          var screenTemplate = new com.adminConsole.adManagement.moduleTemplate({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "moduleTemplate" + screenObj.screenId.split('_').join(""),
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "overrides": {
              "lblModuleName" : {
                "text" : moduleName.toUpperCase()
              },
              "switchModule" : {
                "isVisible" : !scopeObj.isDefaultCampaign,
                "onSlide": function(eventobject){
                  scopeObj.showOrHideScreens(eventobject);
                }
              }
            }
          }, {
            "overrides": {}
          }, {
            "overrides": {}
          });
          scopeObj.constructResolutions(screenObj.resolutions, screenTemplate, isWeb);
          channelTemplate.flxChannelDetails.add(screenTemplate);
        }
      }
    }
  },

  constructResolutions : function(resolutions, screenTemplate, isWeb){
    var scopeObj = this;
    var imageid ;
    var imgscale ;
    var corouselText = "";
    for(var i = 0; i<resolutions.length; i++) {
      imageid = resolutions[i].imageResolution ;
      imageid += resolutions[i].imageIndex ? resolutions[i].imageIndex : "";
      imageid += resolutions[i].imageScale ? resolutions[i].imageScale : "";
      imgscale = resolutions[i].imageScale ? (isWeb ? scopeObj.breakpointMapper[resolutions[i].imageScale] : resolutions[i].imageScale) : "";
      corouselText = (resolutions[i].imageIndex ? resolutions[i].imageIndex : "") + " - " + (imgscale ? imgscale : "");
      var imageSourceURL = new com.adminConsole.adManagement.imageSourceURL({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "imageSourceURL"+imageid,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_DEFAULT,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "overrides": {
          "btnImgVerify" : {
            "onClick" : function(eventobject) {
              scopeObj.verifyImageSource(eventobject);
            }
          },
          "btnTargetVerify" : {
            "onClick" : function(eventobject) {
              scopeObj.verifyTargetSource(eventobject);
            }
          },
          "lblResolutionValue" : {
            "text" : resolutions[i].imageResolution
          },
          "txtImgSource" : {
            "text" : scopeObj.isDefaultCampaign ? resolutions[i].imageURL : ""
          },
          "txtTargetSource" : {
            "text" : scopeObj.isDefaultCampaign ? resolutions[i].destinationURL : ""
          },
          "lblImageIndex" : {
            "isVisible" : scopeObj.isDefaultCampaign,
            "text" : kony.i18n.getLocalizedString("i18n.frmAdManagement.ImageSourceURLDefault") + corouselText
          },
          "lblImgSource" : {
            "isVisible" : !scopeObj.isDefaultCampaign,
          }
        }
      }, {
        "overrides": {}
      }, {
        "overrides": {
          "txtImgSource" : {
            "onKeyUp" : function(eventobject){
              scopeObj.hideErrorMsg(eventobject);
            }
          },
          "txtTargetSource" : {
            "onKeyUp" : function(eventobject){
              scopeObj.hideTargetErrorMsg(eventobject);
            },
            "onEndEditing" : function(eventobject){
              var targetURL = eventobject.text;
              if(targetURL.indexOf("www.") === 0){
                eventobject.text = "https://" + targetURL;
              }
            }
          }
        }
      });
      screenTemplate.flxModuleDetails.add(imageSourceURL) ;
    }
  },

  hideErrorMsg : function(eventobject){
    var scopeObj = this;
    var parentflx = eventobject.parent.parent;
    eventobject.skin = "txtD7d9e0";
    parentflx.flxInfo.setVisibility(true);
    parentflx.imageSrcError.setVisibility(false);
  },
  
  hideTargetErrorMsg : function(eventobject){
    var scopeObj = this;
    var parentflx = eventobject.parent.parent;
    eventobject.skin = "txtD7d9e0";
    parentflx.imgTargetError.setVisibility(false);
  },

  showOrHideChannel : function(eventobject) {
    var scopeObj = this;
    var channelId = eventobject.parent.parent.id ;
    var toCollapse = scopeObj.view[channelId].lblChannelArrow.text === "\ue915";
    scopeObj.view[channelId].lblChannelArrow.text = toCollapse ? "\ue922" : "\ue915";
    scopeObj.view[channelId].flxChannelDetails.setVisibility(!toCollapse);
  },

  showOrHideScreens : function(eventobject) {
    eventobject.parent.parent.flxModuleDetails.setVisibility(eventobject.selectedIndex === 0);
    var screenTemplate = eventobject.parent.parent ;
    for(var i = 1; i<screenTemplate.flxModuleDetails.children.length;i++){
      screenTemplate[screenTemplate.flxModuleDetails.children[i]].txtImgSource.skin = "txtD7d9e0";
      screenTemplate[screenTemplate.flxModuleDetails.children[i]].txtImgSource.text = "" ;
      screenTemplate[screenTemplate.flxModuleDetails.children[i]].txtTargetSource.text = "" ;
      screenTemplate[screenTemplate.flxModuleDetails.children[i]].imgTargetError.setVisibility(false);
      screenTemplate[screenTemplate.flxModuleDetails.children[i]].imageSrcError.setVisibility(false);
      screenTemplate[screenTemplate.flxModuleDetails.children[i]].flxInfo.setVisibility(true);
    }
    eventobject.parent.parent.flxModuleDetails.setVisibility(eventobject.selectedIndex === 0);
  },

  verifyImageSource : function(eventobject) {
    var scopeObj = this;
    var imageURL = eventobject.parent.txtImgSource.text;
    var module = eventobject.parent.parent.parent.parent.parent ;
    var moduleId = module.id.replace("moduleTemplate","");
    var moduleName = "";
    for(var key in scopeObj.moduleNameMapper){
      if(moduleId === key.split("_").join("")){
        moduleName = scopeObj.moduleNameMapper[key];
      }
    }
    scopeObj.view.lblChannelName.text = module.parent.parent.lblChannelName.text;
    scopeObj.view.lblScreenName.text = moduleName;
    if(imageURL.trim() !== "") {
      var img = new Image();
      img.onload = function() {
        var resolutions = eventobject.parent.parent.flxInfo.lblResolutionValue.text.split("x");
        scopeObj.view.richTextAdImage.text = "<img src=\"" + imageURL + "\" alt=\"Image\">";
        scopeObj.view.flxImage.width = img.width;
        scopeObj.view.flxImage.height = img.height;
        scopeObj.view.flxBorder.width = parseInt(resolutions[0]) + 20 + "px";
        scopeObj.view.flxBorder.height = parseInt(resolutions[1]) + 20 + "px";
        scopeObj.view.forceLayout();
      };
      img.src = imageURL;
      scopeObj.view.flxConsentPopup.setVisibility(true);
      scopeObj.view.flxDisplayImage.setVisibility(true);
    }
  },

  verifyTargetSource : function(eventobject){
    var targetURL = eventobject.parent.txtTargetSource.text;
    if(targetURL.trim() !== "") {
      kony.application.openURL(targetURL);
    }
  },

  showCampaignsList : function(list) {
    var scopeObj = this;
    scopeObj.view.segCampaigns.widgetDataMap = {
      "lblPriority": "priority",
      "lblCampaign": "name",
    };
    scopeObj.view.segCampaigns.setData(list);
    scopeObj.view.flxConsentPopup.setVisibility(true);
    scopeObj.view.flxCampaignList.setVisibility(true);
  },

  showCustomerRoles : function(){
    var scopeObj = this;
    var searchText = scopeObj.view.tbxSearchBox.text.toUpperCase();
    var selectedDataList = [];
    var unSelectedDataList = [];
    var obj ;
    var groupId = "";
    for(var i=0; i<scopeObj.availableCustomerGroups.length;i++){
      obj = scopeObj.availableCustomerGroups[i];
      groupId = obj.Group_id;
      if(scopeObj.groupsToAdd.indexOf(groupId) !== -1 || 
         (scopeObj.groupsToDelete.indexOf(groupId) === -1 && scopeObj.existingCustomerGroups.indexOf(groupId)!==-1)){
        selectedDataList.push(obj);
      } else if(searchText === "" || obj.Group_Name.toUpperCase().indexOf(searchText) !== -1){
        unSelectedDataList.push(obj);
      }
    }
    scopeObj.view.segAddOptions.setData(unSelectedDataList);
    scopeObj.view.segSelectedOptions.setData(selectedDataList);
    scopeObj.hasAvailableCustomersData();
    scopeObj.hasSelectedCustomersData();
  },

  formatCustomerRolesData : function(customerGroups) {
    var scopeObj = this;
    scopeObj.availableCustomerGroups = [];
    for(var i=0;i<customerGroups.length;i++){
      var obj = customerGroups[i] ;
      if(obj.Status_id === "SID_ACTIVE") {
        obj.btnAdd = {
          "text" : kony.i18n.getLocalizedString("i18n.DragBox.ADD"),
          "onClick" : scopeObj.addGroup
        };
        obj.fontIconClose =  {
          "text" : "\ue929",
          "onClick" : scopeObj.removeGroup
        };
        obj.lblDesc = obj.Group_Desc;
        obj.lblName = obj.Group_Name;
        obj.lblOption = obj.Group_Name;
        scopeObj.availableCustomerGroups.push(obj);
      }
    }
  },

  hasSelectedCustomersData : function(){
    var scopeObj = this;
    var hasData = scopeObj.view.segSelectedOptions.data.length > 0;
    scopeObj.view.segSelectedOptions.setVisibility(hasData);
    scopeObj.view.lblNoSelectedCustomers.setVisibility(!hasData);
  },

  hasAvailableCustomersData : function(){
    var scopeObj = this;
    var hasData = scopeObj.view.segAddOptions.data.length > 0;
    scopeObj.view.segAddOptions.setVisibility(hasData);
    scopeObj.view.lblNoCustomersAvailable.setVisibility(!hasData);
  },

  addGroup : function(eventobject,context) {
    var scopeObj = this;
    var dataObj = scopeObj.view.segAddOptions.data[context.rowIndex];
    var dataLen = scopeObj.view.segSelectedOptions.data.length ;
    scopeObj.view.segSelectedOptions.addDataAt(dataObj, dataLen, context.sectionIndex);
    scopeObj.view.segAddOptions.removeAt(context.rowIndex, context.sectionIndex);
    scopeObj.hasSelectedCustomersData();
    scopeObj.hasAvailableCustomersData();
    scopeObj.updateGroups(dataObj.Group_id,true);
  },

  removeGroup : function(eventobject,context) {
    var scopeObj = this;
    var dataObj = scopeObj.view.segSelectedOptions.data[context.rowIndex];
    var dataLen = scopeObj.view.segAddOptions.data.length ;
    scopeObj.view.segAddOptions.addDataAt(dataObj, dataLen, context.sectionIndex);
    scopeObj.view.segSelectedOptions.removeAt(context.rowIndex, context.sectionIndex);
    scopeObj.hasSelectedCustomersData();
    scopeObj.hasAvailableCustomersData();
    scopeObj.updateGroups(dataObj.Group_id,false);
  },

  addAllGroups : function() {
    var scopeObj = this;
    var dataArray = scopeObj.view.segAddOptions.data;
    var selectedData = [];
    dataArray.forEach(function(dataObj){
      selectedData.push(dataObj);
      scopeObj.updateGroups(dataObj.Group_id,true);
    });
    scopeObj.view.segAddOptions.removeAll();
    scopeObj.view.segSelectedOptions.addAll(selectedData);
    scopeObj.hasSelectedCustomersData();
    scopeObj.hasAvailableCustomersData();
  },

  deleteAllGroups : function(){
    var scopeObj = this;
    var dataArray = scopeObj.view.segSelectedOptions.data;
    var unSelectedData = [];
    dataArray.forEach(function(dataObj){
      unSelectedData.push(dataObj);
      scopeObj.updateGroups(dataObj.Group_id,false);
    });
    scopeObj.view.segSelectedOptions.removeAll();
    scopeObj.view.segAddOptions.addAll(unSelectedData);
    scopeObj.hasSelectedCustomersData();
    scopeObj.hasAvailableCustomersData();
  },

  updateGroups : function(groupId, isAdd){
    var scopeObj = this;
    var isAddIndex = scopeObj.groupsToAdd.indexOf(groupId);
    var isDeleteIndex = scopeObj.groupsToDelete.indexOf(groupId);
    if(isAdd && scopeObj.existingCustomerGroups.indexOf(groupId) === -1) {
      scopeObj.groupsToAdd.push(groupId);
    } else if( !isAdd && scopeObj.existingCustomerGroups.indexOf(groupId) !== -1) {
      scopeObj.groupsToDelete.push(groupId);
    } 
    if(isAdd && isDeleteIndex !== -1){
      scopeObj.groupsToDelete.splice(isDeleteIndex,1);
    } 
    if( !isAdd && isAddIndex !== -1){
      scopeObj.groupsToAdd.splice(isAddIndex,1);
    }
  },

  updateJSON : function() {
    var scopeObj = this;
    var saveJSON = {
      "name" : scopeObj.view.txtAdName.text.trim(),
      "description": scopeObj.view.txtDescription.text.trim(),
      "priority" : parseInt(scopeObj.view.txtPriority.text.trim()),
      "startDateTime": scopeObj.view.customStartDate.value.replace(/-/g,"/"),
      "endDateTime": scopeObj.view.customEndDate.value.replace(/-/g,"/")
    };
    saveJSON.specifications = scopeObj.getSpecificationsJSON();
    if(scopeObj.isDefaultCampaign){
      scopeObj.presenter.updateDefaultCampaign(saveJSON);
    } else {
      saveJSON.groupsToBeAdded = scopeObj.groupsToAdd.map(function(obj){
        return { "groupId" : obj} ;
      });
      saveJSON.groupsToBeRemoved = scopeObj.groupsToDelete.map(function(obj){
        return { "groupId" : obj} ;
      });
      if(scopeObj.isCreate){
        scopeObj.presenter.createCampaign(saveJSON);
      } else {
        saveJSON.id = scopeObj.campaignData.id;
        scopeObj.presenter.updateCampaign(saveJSON);
      }
    }
  },

  getSpecificationsJSON : function(){
    var scopeObj = this;
    var json = [];
    var channelObj ;
    var screenObj ;
    var screenTemplate ;
    var resolutionObj ;
    var resoultionTemplate ;
    var imageid ;
    var specificationsArray = scopeObj.isDefaultCampaign ? scopeObj.defaultCampaignObject.specifications : scopeObj.campaignSpecifications;
    for (var i = 0; i < specificationsArray.channels.length; i++) {
      channelObj = specificationsArray.channels[i];
      for (var j = 0; j < channelObj.screens.length; j++) {
        screenObj =  channelObj.screens[j];
        screenTemplate = scopeObj.view["channelTemplate" + channelObj.channelId.split('_').join("")]["moduleTemplate" + screenObj.screenId.split('_').join("")];
        if(screenTemplate.switchModule.selectedIndex === 0){
          for (var k = 0; k < screenObj.resolutions.length; k++) {
            resolutionObj = screenObj.resolutions[k];
            imageid = resolutionObj.imageResolution ;
            imageid += resolutionObj.imageIndex ? resolutionObj.imageIndex : "";
            imageid += resolutionObj.imageScale ? resolutionObj.imageScale : "";
            resoultionTemplate = screenTemplate["imageSourceURL" + imageid] ;
            json.push({
              "channelId": channelObj.channelId,
              "screenId": screenObj.screenId,
              "imageResolution": resolutionObj.imageResolution,
              "imageURL": resoultionTemplate.txtImgSource.text.trim(),
              "destinationURL": resoultionTemplate.txtTargetSource.text.trim(),
              "imageIndex" : resolutionObj.imageIndex ? resolutionObj.imageIndex : "",
              "imageScale" : resolutionObj.imageScale ? resolutionObj.imageScale : ""
            });
          }
        }
      }
    }
    return json;
  },

  hasExistingPriority : function(list){
    var scopeObj = this;
    var priority = parseInt(scopeObj.view.txtPriority.text.trim());
    for(var i = 0; i<list.length; i++) {
      if(list[i].priority === priority) {
        scopeObj.showPopupMessgae(false,kony.i18n.getLocalizedString("i18n.frmAdManagement.ExistingPriorityConflictHeader"),
                                  kony.i18n.getLocalizedString("i18n.frmAdManagement.ExistingPriorityConflictInfo"));
        return ;
      }
    }
    scopeObj.isToCheckExistingPrioirity = false;
  },

  validateSpecifications: function(){
    var scopeObj = this;
    scopeObj.hasValidSpecifiactions = true ;
    var channelObj ;
    var channelTemplate;
    var screenObj ;
    var screenTemplate ;
    var resolutionObj ;
    var resolutionTempate ;
    var specificationsArray  = scopeObj.isDefaultCampaign ? scopeObj.defaultCampaignObject.specifications : scopeObj.campaignSpecifications;
    var hasNoModuleSelected = true;
    var validImagesTypes = ["JPG","GIF", "PNG"];
    var imgextensions ;
    var imgsrc ;
    var isValid ;
    var errormsg ;
    var imageid ;
    var isValidTargetSourceURL;
    var targeturl ;
    for (var i = 0; i < specificationsArray.channels.length; i++) {
      channelObj = specificationsArray.channels[i];
      channelTemplate = scopeObj.view["channelTemplate"+channelObj.channelId.split('_').join("")];
      for (var j = 0; j < channelObj.screens.length; j++) {
        screenObj =  channelObj.screens[j];
        screenTemplate = scopeObj.view["channelTemplate" + channelObj.channelId.split('_').join("")]["moduleTemplate" + screenObj.screenId.split('_').join("")];
        hasNoModuleSelected = hasNoModuleSelected && (screenTemplate.switchModule.selectedIndex === 1);
        if(screenTemplate.switchModule.selectedIndex === 0){
          for (var k = 0; k < screenObj.resolutions.length; k++) {
            errormsg = kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorImageSourceURL");
            resolutionObj = screenObj.resolutions[k];
            imageid = resolutionObj.imageResolution ;
            imageid += resolutionObj.imageIndex ? resolutionObj.imageIndex : "";
            imageid += resolutionObj.imageScale ? resolutionObj.imageScale : "";
            resolutionTempate = screenTemplate["imageSourceURL"+imageid] ;
            imgsrc = resolutionTempate.txtImgSource.text.trim();
            isValid = imgsrc !== "" ;
            if(isValid){
              imgextensions = imgsrc.split(".");
              isValid = (validImagesTypes.indexOf(imgextensions[imgextensions.length-1].toUpperCase()) !== -1) ;
              errormsg = isValid ? errormsg : kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorImageExtension");
            }
            isValidTargetSourceURL = true;
            targeturl = resolutionTempate.txtTargetSource.text.trim().toLowerCase();
            if(targeturl !== "") {
              if(targeturl.indexOf("http:") === 0) {
                resolutionTempate.imgTargetError.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorTargetURLHttp");
                isValidTargetSourceURL = false;
              } else if(!scopeObj.urlRegex.test(targeturl)){
                resolutionTempate.imgTargetError.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmAdManagement.ErrorTargetURL");
                isValidTargetSourceURL = false;
              }
            }
            resolutionTempate.imgTargetError.setVisibility(!isValidTargetSourceURL);
            resolutionTempate.txtTargetSource.skin = isValidTargetSourceURL ? "txtD7d9e0" : "skinredbg";
            resolutionTempate.txtImgSource.skin = isValid ? "txtD7d9e0" : "skinredbg";
            resolutionTempate.flxInfo.setVisibility(isValid);
            resolutionTempate.imageSrcError.setVisibility(!isValid);
            resolutionTempate.imageSrcError.lblErrorText.text = errormsg;
            scopeObj.hasValidSpecifiactions = scopeObj.hasValidSpecifiactions && isValid && isValidTargetSourceURL;
            channelTemplate.flxChannelDetails.setVisibility(isValid && isValidTargetSourceURL ? channelTemplate.flxChannelDetails.isVisible : true);
            channelTemplate.lblChannelArrow.text = isValid ? channelTemplate.lblChannelArrow.text : "\ue915";
          }
        }
      }
    }
    if(hasNoModuleSelected){
      scopeObj.showPopupMessgae(true,kony.i18n.getLocalizedString("i18n.frmAdManagement.RemoveImageHeader"),
                                kony.i18n.getLocalizedString("i18n.frmAdManagement.RemoveImageInfo"));
    } else if(scopeObj.hasValidSpecifiactions){
      if(scopeObj.isDefaultCampaign) {
        scopeObj.setLeftNavSkins(false, false, true);
      } else if(scopeObj.availableCustomerGroups.length > 0) {
        scopeObj.showCustomerRoles(); 
        scopeObj.setLeftNavSkins(false, false, true);      
      } else {
        scopeObj.presenter.fetchCustomerGroups();
      }
    }
    scopeObj.view.forceLayout();
  },

  sortPriorityList : function(sortColumn){
    var scopeObj = this;  
    var sortOrder = (scopeObj.sortBy && sortColumn === scopeObj.sortBy.columnName) ? !scopeObj.sortBy.inAscendingOrder : true;  
    scopeObj.sortBy = scopeObj.getObjectSorter(sortColumn);
    scopeObj.sortBy.inAscendingOrder = sortOrder;
    scopeObj.getObjectSorter().column(sortColumn);
    var data = scopeObj.view.segCampaigns.data.sort(scopeObj.sortBy.sortData);
    scopeObj.view.segCampaigns.setData(data);
    scopeObj.determineSortFontIcon(scopeObj.sortBy,'name',this.view.lblCampaignSort);
    scopeObj.determineSortFontIcon(scopeObj.sortBy,'priority',this.view.lblPrioritySort);
  },

  setEditCampaignData : function(){
    var scopeObj = this;
    scopeObj.view.txtAdName.text = scopeObj.campaignData.name;
    scopeObj.view.txtDescription.text = scopeObj.campaignData.description;
    scopeObj.view.txtPriority.text = scopeObj.campaignData.priority;
    scopeObj.view.customStartDate.value = scopeObj.campaignData.startDate;
    scopeObj.view.customStartDate.resetData = scopeObj.campaignData.startDate;
    scopeObj.view.customEndDate.value = scopeObj.campaignData.endDate;
    scopeObj.view.customEndDate.resetData = scopeObj.campaignData.endDate;
    // set specifications
    var campaignSpecifications = scopeObj.campaignData.campaignSpecifications;
    var channelTemplate ;
    var screenTemplate ;
    var resolutionObj ;
    var resolutionTempate ;
    var imageid;
    for (var channel in campaignSpecifications) {
      channelTemplate = scopeObj.view["channelTemplate" + channel.split('_').join("")];
      for (var screen in campaignSpecifications[channel]) {
        screenTemplate = channelTemplate["moduleTemplate" + screen.split('_').join("")];
        for(var i=0; i<campaignSpecifications[channel][screen].length; i++){
          resolutionObj = campaignSpecifications[channel][screen][i];
          imageid = resolutionObj.imageResolution;
          imageid += resolutionObj.imageIndex ? resolutionObj.imageIndex : "";
          imageid += resolutionObj.imageScale ? resolutionObj.imageScale : "";
          resolutionTempate = screenTemplate["imageSourceURL" + imageid];
          resolutionTempate.txtImgSource.text = resolutionObj.imageURL;
          resolutionTempate.txtTargetSource.text = resolutionObj.destinationURL;
        }
      }
    }
    // set customer groups
    scopeObj.existingCustomerGroups = scopeObj.campaignData.campaignGroups.map(function(obj){
      return obj.groupId ;
    });
    scopeObj.validateEditCampaignSpecificationsData();
  },

  validateEditCampaignSpecificationsData : function(){
    var scopeObj = this;
    var channelObj;
    var screenObj;
    var screenTemplate;
    var resolutionObj;
    var resolutionTempate;
    var hasScreenData = true;
    var imageid ;
    var specificationsArray = scopeObj.campaignSpecifications.channels;
    for (var i = 0; i < specificationsArray.length; i++) {
      channelObj = scopeObj.campaignSpecifications.channels[i];
      for (var j = 0; j < channelObj.screens.length; j++) {
        hasScreenData = true;
        screenObj = channelObj.screens[j];
        screenTemplate = scopeObj.view["channelTemplate" + channelObj.channelId.split('_').join("")]["moduleTemplate" + screenObj.screenId.split('_').join("")];
        for (var k = 0; k < screenObj.resolutions.length; k++) {
          resolutionObj = screenObj.resolutions[k];
          imageid = resolutionObj.imageResolution;
          imageid += resolutionObj.imageIndex ? resolutionObj.imageIndex : "";
          imageid += resolutionObj.imageScale ? resolutionObj.imageScale : "";
          resolutionTempate = screenTemplate["imageSourceURL" + imageid];
          hasScreenData = hasScreenData && resolutionTempate.txtImgSource.text !== "";
        }
        screenTemplate.switchModule.setVisibility(!hasScreenData);
        screenTemplate.switchModule.selectedIndex = hasScreenData ? 0 : 1;
        screenTemplate.flxModuleDetails.setVisibility(hasScreenData);
      }
    }
  },

  setDefaultCampaignSpecifications : function(defaultCampaignData){
    var scopeObj = this;
    scopeObj.view.flxAdSpecifications.removeAll();
    var channels = [];
    var screens = [];
    for (var channel in defaultCampaignData.specifications) {
      screens = [];
      for (var screen in defaultCampaignData.specifications[channel]) {
        screens.push({"screenId" : screen , "resolutions" : defaultCampaignData.specifications[channel][screen]});
      }
      channels.push({"channelId" : channel, "screens" : screens});
    }
    scopeObj.defaultCampaignObject = {"name" : defaultCampaignData.name,
                                      "description" : defaultCampaignData.description, 
                                      "specifications" : {"channels" : channels}
                                     };
  },

  showPopupMessgae : function(isError, header, message){
    var scopeObj = this;
    scopeObj.view.popUp.flxPopUpTopColor.skin = isError ? "sknFlxee6565Op100NoBorder" : "sknflxebb54cOp100";
    scopeObj.view.popUp.lblPopUpMainMessage.text = header;
    scopeObj.view.popUp.rtxPopUpDisclaimer.text = message;
    scopeObj.view.popUp.btnPopUpDelete.text = isError ?
      kony.i18n.getLocalizedString("i8n.navigation.okButton").toUpperCase() : kony.i18n.getLocalizedString("i18n.PopUp.YesProceed");
    scopeObj.view.popUp.btnPopUpCancel.setVisibility(!isError);
    scopeObj.view.flxConsentPopup.setVisibility(true);
    scopeObj.view.popUp.setVisibility(true);
  }

});