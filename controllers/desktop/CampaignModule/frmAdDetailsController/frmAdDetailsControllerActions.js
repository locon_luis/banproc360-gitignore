define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmAdDetails **/
    AS_Form_d4c237d41e654e8188a8b4bf4eb981f4: function AS_Form_d4c237d41e654e8188a8b4bf4eb981f4(eventobject) {
        var self = this;
        return self.frmCampaignDetailsPreShow.call(this);
    },
    /** postShow defined for frmAdDetails **/
    AS_Form_bf5188f919a741e0bb5187da73d26d40: function AS_Form_bf5188f919a741e0bb5187da73d26d40(eventobject) {
        var self = this;
        return self.frmCampaignDetailsPostShow.call(this);
    },
    /** onDeviceBack defined for frmAdDetails **/
    AS_Form_b0638d9f664d41828d64a669d1ca619c: function AS_Form_b0638d9f664d41828d64a669d1ca619c(eventobject) {
        var self = this;
        this.onBrowserBack();
    }
});