define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxCheckbox **/
    AS_FlexContainer_d70000f3a77f4206b1a348f40b406e56: function AS_FlexContainer_d70000f3a77f4206b1a348f40b406e56(eventobject, context) {
        this.toggleCheckbox2();
    },
    /** onClick defined for flxOptions **/
    AS_FlexContainer_b2c24ab86083483bae9d1d23854fe16f: function AS_FlexContainer_b2c24ab86083483bae9d1d23854fe16f(eventobject, context) {
        this.executeOnParent("toggleContextualMenu");
    }
});