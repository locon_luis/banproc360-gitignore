define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmPolicies **/
    AS_Form_ecab3926c81549e2a81f51c89c0e6017: function AS_Form_ecab3926c81549e2a81f51c89c0e6017(eventobject) {
        var self = this;
        this.policiesPreShow();
    },
    /** onDeviceBack defined for frmPolicies **/
    AS_Form_b9037ebbdeb34d98b93eacef4438dc4a: function AS_Form_b9037ebbdeb34d98b93eacef4438dc4a(eventobject) {
        var self = this;
        //registered
        this.onBrowserBack();
    }
});