define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgLogout **/
    AS_Image_ba3054180b9948c180685a109ad8fea8: function AS_Image_ba3054180b9948c180685a109ad8fea8(eventobject, x, y) {
        var self = this;
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.presentationController.doLogout();
    },
    /** preShow defined for frmDashboard **/
    AS_Form_a7392d1d72024b2c9dbf5c07011a7abe: function AS_Form_a7392d1d72024b2c9dbf5c07011a7abe(eventobject) {
        var self = this;
        this.formPreshow();
    },
    /** onDeviceBack defined for frmDashboard **/
    AS_Form_ga9181d33bfe402ab5e9a712972ef96c: function AS_Form_ga9181d33bfe402ab5e9a712972ef96c(eventobject) {
        var self = this;
        this.onBrowserBack();
    }
});