define({
  accountsData: [],
  assignedAccounts: [],
  comapnyDetails :null,
  customerDetails : null,
  typeConfig :{
    smallBusiness :"TYPE_ID_SMALL_BUSINESS",
    microBusiness :"TYPE_ID_MICRO_BUSINESS"
  },
  actionConfig:{
    create :"CREATE",
    edit:"EDIT"
  },
  tabClick : 1,
  serviceTypeConfig :{
    transactional:"SER_TYPE_TRNS"
  },
  action : "CREATE",
  usernameRulesAndPolicy :{
    usernamerules : null,
    usernamepolicy : null
  },
  willUpdateUI: function (customerCreateModel) {
    this.updateLeftMenu(customerCreateModel);
    if (customerCreateModel) {
      if (customerCreateModel.LoadingScreen) {
        if (customerCreateModel.LoadingScreen.focus)
          kony.adminConsole.utils.showProgressBar(this.view);
        else
          kony.adminConsole.utils.hideProgressBar(this.view);
      } else if(customerCreateModel.companyDetails){ //Create screen
        this.view.flxOFACError.setVisibility(false);
        this.comapnyDetails = customerCreateModel.companyDetails;
        this.action = this.actionConfig.create;
        this.view.breadcrumbs.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.frmCompanies.CREATE_CUSTOMER");
        this.view.breadcrumbs.btnBackToMain.text = this.comapnyDetails.companyName.toUpperCase();
        this.showCreateEditSpecificUI();
      } else if (customerCreateModel.accounts) {
        this.manageAccountsData(customerCreateModel.accounts);
      } else if (customerCreateModel.roles) {
        this.setRolesForLimits(customerCreateModel.roles);
      } else if (customerCreateModel.rolePermission) {
        this.setPermissionLimits(customerCreateModel.rolePermission);
      } else if(customerCreateModel.isUsernameAvailable){
        this.displayUsernameAvailability(customerCreateModel.isUsernameAvailable);
      } else if(customerCreateModel.OFACVerification) {
        this.onClickOfNextFromDetails(customerCreateModel.OFACVerification);
      } else if(customerCreateModel.editInputs){ //Edit screen
        this.action = this.actionConfig.edit;
        this.view.flxOFACError.setVisibility(false);
        this.customerDetails = customerCreateModel.editInputs;
        this.showCreateEditSpecificUI();
        this.view.verticalTabsCustomer.flxOption3.info = {"isFirstTime" : true};
        this.view.breadcrumbs.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EDIT_BUSINESS_USER");
        this.setCustomerDetailsForEdit(customerCreateModel.editInputs);
      } else if(customerCreateModel.customerAccounts){
        this.assignedAccounts = customerCreateModel.customerAccounts;
        var mappedSelectedData = this.mapSelectedSegmentData(customerCreateModel.customerAccounts);
        this.view.addAndRemoveAccounts.segSelectedOptions.info={"segData" : mappedSelectedData};
      } else if(customerCreateModel.customerLimits){
        this.setPermissionLimits(customerCreateModel.rolePermissionEdit);
        this.view.segLimits.info = {
        	"customerLimits": customerCreateModel.customerLimits,
        	"roleLimits": customerCreateModel.rolePermissionEdit
        };
        if (this.view.verticalTabsCustomer.flxOption3.info.isFirstTime) {
        	this.view.verticalTabsCustomer.flxOption3.info.isFirstTime = false;
        	this.setRolesLimitsForEdit();
        }
      } else if(customerCreateModel.usernameRulesAndPolicy) {
        this.setUsernameRulesAndPolicy(customerCreateModel.usernameRulesAndPolicy);
      } else if (customerCreateModel.toastMessage) {
        if (customerCreateModel.toastMessage.status === kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success")) {
          this.view.toastMessage.showToastMessage(customerCreateModel.toastMessage.message, this);
        } else if (customerCreateModel.toastMessage.status === kony.i18n.getLocalizedString("i18n.frmGroupsController.error")) {
          this.view.toastMessage.showErrorToastMessage(customerCreateModel.toastMessage.message, this);
        }
      }
    }
    this.view.forceLayout();
  },
  setFlowActions: function () {
    var scopeObj = this;
    this.view.verticalTabsCustomer.btnOption1.onClick = function () {
      scopeObj.tabClick = 1;
      if(scopeObj.view.flxCustomerAccounts.isVisible){
        var isValidAcc = scopeObj.checkAccountValidation();
        if(isValidAcc) scopeObj.showCustomerCreateScreen();
        else scopeObj.view.addAndRemoveAccounts.flxError.setVisibility(true);
      }
      if(scopeObj.view.flxCustomerRolesLimits.isVisible){
        var isValidLimits = scopeObj.validateLimitsScreen();
        if(isValidLimits) scopeObj.showCustomerCreateScreen();
      }
    };
    this.view.verticalTabsCustomer.btnOption2.onClick = function () {
      scopeObj.tabClick = 2;
      if(scopeObj.view.flxCustomerDetails.isVisible) {
        var isValidDetails = scopeObj.detailsScreenValidation();
        if(isValidDetails) {
          if(scopeObj.action === scopeObj.actionConfig.create){
          scopeObj.checkUsernameAvailability();
        }else{
          var param = scopeObj.OFACverification();
          scopeObj.presenter.OFACverification(param);
        }
        }
      }
      if(scopeObj.view.flxCustomerRolesLimits.isVisible){
        var isValidLimits = scopeObj.validateLimitsScreen();
        if(isValidLimits) scopeObj.showAccountsScreen();
      }
    };
    this.view.verticalTabsCustomer.btnOption3.onClick = function () {
      scopeObj.tabClick = 3;
      if(scopeObj.view.flxCustomerDetails.isVisible){
        var isValidDetails = scopeObj.detailsScreenValidation();
        if (isValidDetails && (scopeObj.action === scopeObj.actionConfig.create)) {
            scopeObj.checkUsernameAvailability();
        } else if(isValidDetails && (scopeObj.action === scopeObj.actionConfig.edit)){
            var param = scopeObj.OFACverification();
            scopeObj.presenter.OFACverification(param);
        }
      }
      if(scopeObj.view.flxCustomerAccounts.isVisible){
        var isValidAcc = scopeObj.checkAccountValidation();
        if(isValidAcc) {
          if (scopeObj.action === scopeObj.actionConfig.edit && scopeObj.view.verticalTabsCustomer.flxOption3.info.isFirstTime) {
              scopeObj.getCustomerLimits();
          } else if(scopeObj.action === scopeObj.actionConfig.edit) {
              scopeObj.setRolesLimitsForEdit();
          }
          scopeObj.showLimitsScreen();
        } else {
          scopeObj.view.addAndRemoveAccounts.flxError.setVisibility(true);
        }
      }
    };
    this.view.commonButtonsDetails.btnCancel.onClick = function () {
      var prevForm = kony.application.getPreviousForm();
      if(prevForm.id === "frmCompanies"){
        scopeObj.hideCustomerCreateScreen();
      } else{
        var custId = scopeObj.customerDetails.id;
        var param = {"Customer_id": custId};
        scopeObj.presenter.navigateToCustomerPersonal(param,"frmCustomerCreate");
      }
      
    };
    this.view.commonButtonsAccounts.btnCancel.onClick = function () {
      var prevForm = kony.application.getPreviousForm();
      if(prevForm.id === "frmCompanies"){
        scopeObj.hideCustomerCreateScreen();
      } else{
        var custId = scopeObj.customerDetails.id;
        var param = {"Customer_id": custId};
        scopeObj.presenter.navigateToCustomerPersonal(param,"frmCustomerCreate");
      }
    };
    this.view.commonButtonsRolesLimit.btnCancel.onClick = function () {
      var prevForm = kony.application.getPreviousForm();
      if(prevForm.id === "frmCompanies"){
        scopeObj.hideCustomerCreateScreen();
      } else{
        var custId = scopeObj.customerDetails.id;
        var param = {"Customer_id": custId};
        scopeObj.presenter.navigateToCustomerPersonal(param,"frmCustomerCreate");
      }
    };
    this.view.commonButtonsDetails.btnSave.onClick = function () {
      scopeObj.tabClick = 2;
      var isValid = scopeObj.detailsScreenValidation();
      if(isValid) {
        if(scopeObj.action === scopeObj.actionConfig.create){
          scopeObj.checkUsernameAvailability();
        }else{
          var param = scopeObj.OFACverification();
          scopeObj.presenter.OFACverification(param);
        }
      }
    };
    this.view.commonButtonsAccounts.btnSave.onClick = function(){
      scopeObj.tabClick = 3;
      var isValid = scopeObj.checkAccountValidation();
      scopeObj.showHideAddRemoveSegment();
      if (isValid) {
      	if (scopeObj.action === scopeObj.actionConfig.edit) {
      		if (scopeObj.view.verticalTabsCustomer.flxOption3.info.isFirstTime) {
      			scopeObj.getCustomerLimits();
      		} else {
      			scopeObj.setRolesLimitsForEdit();
      		}
      	}
      	scopeObj.showLimitsScreen();
      	scopeObj.view.addAndRemoveAccounts.flxError.setVisibility(false);
      } else {
      	scopeObj.view.addAndRemoveAccounts.flxError.setVisibility(true);
      }
    };
    this.view.commonButtonsRolesLimit.btnSave.onClick = function(){
      var isValid = scopeObj.validateLimitsScreen();
      if(isValid) {
        scopeObj.createCustomerRequest();
      }
    };
    this.view.addAndRemoveAccounts.tbxSearchBox.onKeyUp = function () {
      scopeObj.accountSearch(scopeObj.view.addAndRemoveAccounts.tbxSearchBox.text,scopeObj.typeConfig.microBusiness);
    };
    this.view.addAndRemoveAccounts.tbxSearchBox.onTouchStart = function () {
      scopeObj.view.addAndRemoveAccounts.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
    };
    this.view.addAndRemoveAccounts.tbxSearchBox.onEndEditing = function () {
      scopeObj.view.addAndRemoveAccounts.flxSearchContainer.skin = "sknflxd5d9ddop100";
    };
    this.view.addAndRemoveAccounts.tbxFilterSearch.onTouchStart = function(){
      if(scopeObj.view.addAndRemoveAccounts.flxSegSearchType.isVisible){
        scopeObj.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(false);
      }
    };
    this.view.addAndRemoveAccounts.tbxFilterSearch.onKeyUp = function(){
      if(scopeObj.view.addAndRemoveAccounts.tbxFilterSearch.text === ""){
        scopeObj.accountSearch(scopeObj.view.addAndRemoveAccounts.tbxFilterSearch.text,scopeObj.typeConfig.smallBusiness);
      }
    };
    this.view.addAndRemoveAccounts.btnRemoveAll.onClick = function(){
      scopeObj.resetAccounts();
    };
    this.view.lstBoxSelectRole.onSelection = function () {
      scopeObj.view.lstBoxSelectRole.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
      scopeObj.view.flxInlineError.setVisibility(false);
      if (scopeObj.action === scopeObj.actionConfig.edit) {
        scopeObj.showPopUpOnRoleChange();
        scopeObj.view.flxPopup.setVisibility(true);
        if (scopeObj.view.lstBoxSelectRole.selectedKey === scopeObj.view.segLimits.info.customerLimits.Group_id) {
          scopeObj.setRolesLimitsForEdit();
        } else {
          scopeObj.getPermissionsForSelectedRole();
        }
      } else {
        scopeObj.getPermissionsForSelectedRole();
      }
    };
    this.view.textBoxEntry11.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidationsForDetails(scopeObj.view.textBoxEntry11.tbxEnterValue, scopeObj.view.textBoxEntry11.flxInlineError, scopeObj.view.textBoxEntry11.flxEnterValue);
    };
    this.view.textBoxEntry13.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidationsForDetails(scopeObj.view.textBoxEntry13.tbxEnterValue, scopeObj.view.textBoxEntry13.flxInlineError, scopeObj.view.textBoxEntry13.flxEnterValue);
    };
    this.view.textBoxEntry21.tbxEnterValue.onKeyUp = function(){
      scopeObj.view.lblUsernameCheck.setVisibility(false);
      scopeObj.clearValidationsForDetails(scopeObj.view.textBoxEntry21.tbxEnterValue, scopeObj.view.textBoxEntry21.flxInlineError, scopeObj.view.textBoxEntry21.flxEnterValue);
    };
    this.view.textBoxEntry22.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidationsForDetails(scopeObj.view.textBoxEntry22.tbxEnterValue, scopeObj.view.textBoxEntry22.flxInlineError, scopeObj.view.textBoxEntry22.flxEnterValue);
    };
    this.view.textBoxEntry23.txtContactNumber.onKeyUp = function(){
      scopeObj.clearValidationsForDetails(scopeObj.view.textBoxEntry23.txtContactNumber, scopeObj.view.textBoxEntry23.flxError);
    };
    this.view.textBoxEntry23.txtISDCode.onKeyUp = function(){
      scopeObj.clearValidationsForDetails(scopeObj.view.textBoxEntry23.txtISDCode, scopeObj.view.textBoxEntry23.flxError);
      scopeObj.view.textBoxEntry23.txtISDCode.text = scopeObj.view.textBoxEntry23.addingPlus(scopeObj.view.textBoxEntry23.txtISDCode.text);
    };
    this.view.textBoxEntry31.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidationsForDetails(scopeObj.view.textBoxEntry31.tbxEnterValue, scopeObj.view.textBoxEntry31.flxInlineError, scopeObj.view.textBoxEntry31.flxEnterValue);
    };
    this.view.textBoxEntry32.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidationsForDetails(scopeObj.view.textBoxEntry32.tbxEnterValue, scopeObj.view.textBoxEntry32.flxInlineError, scopeObj.view.textBoxEntry32.flxEnterValue);
    };
    this.view.textBoxEntry21.btnCheck.onClick = function(){
      var isUsernameValid = scopeObj.userNameValidation();
      if(isUsernameValid){
        var usernameParam = {"UserName" :scopeObj.view.textBoxEntry21.tbxEnterValue.text};
        scopeObj.presenter.verifyUsername(usernameParam,null);
      }
    };
    this.view.customCalCustomerDOB.event = function(){
      scopeObj.view.flxCalendarDOB.skin = "sknflxffffffoptemplateop3px";
      scopeObj.view.textBoxEntry31.flxInlineError.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    this.view.addAndRemoveAccounts.flxSelectedType.onClick = function(){
      if(scopeObj.view.addAndRemoveAccounts.flxSegSearchType.isVisible) scopeObj.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(false);
      else scopeObj.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(true);
    };
    this.view.addAndRemoveAccounts.segSearchType.onRowClick = function(){
      scopeObj.dispalySelectedType();
    };
    this.view.addAndRemoveAccounts.flxSearchClick.onClick = function(){
      scopeObj.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(false);
      scopeObj.accountSearch(scopeObj.view.addAndRemoveAccounts.tbxFilterSearch.text,scopeObj.typeConfig.smallBusiness);
    };
    this.view.popUpRole.btnPopUpCancel.onClick = function(){
      scopeObj.view.lstBoxSelectRole.selectedKey = scopeObj.view.segLimits.info.customerLimits.Group_id;
      scopeObj.view.lstBoxSelectRole.onSelection();
      scopeObj.view.flxPopup.setVisibility(false);
    };
    this.view.popUpRole.btnPopUpDelete.onClick = function(){
      scopeObj.view.flxPopup.setVisibility(false);
    };
    this.view.popUpRole.flxPopUpClose.onClick = function(){
      scopeObj.view.popUpRole.btnPopUpCancel.onClick();
    };
    this.view.breadcrumbs.btnBackToMain.onClick = function(){
      scopeObj.hideCustomerCreateScreen();
    };
    this.view.lblRules.onClick = function() {
      scopeObj.rulesLabelClick();
    };
    this.view.flxFlag1.onClick = function(){
      scopeObj.selectRiskFlag(scopeObj.view.imgFlag1);
    };
    this.view.flxFlag2.onClick = function(){
      scopeObj.selectRiskFlag(scopeObj.view.imgFlag2);
    };
    this.view.flxFlag3.onClick = function(){
      scopeObj.selectRiskFlag(scopeObj.view.imgFlag3);
    };
  },
  createCustomerPreshow: function () {
    var self = this;
    self.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
    self.clearDataForDetails();
    self.clearDataForAccounts();
    self.clearDataForRolesLimits();
    self.showCustomerCreateScreen();
    self.view.flxUsernameRules.setVisibility(false);
    this.setFlowActions();
  },
  /*
   * callback function for accounts data from willUpdateUI
   */
  manageAccountsData : function(accounts){
    var type =(this.action === this.actionConfig.create) ? this.comapnyDetails.type : this.customerDetails.company.TypeId;
    if(type === this.typeConfig.microBusiness){
      this.view.flxHeading.setVisibility(true);
    }else{
      this.view.flxHeading.setVisibility(false);
    }
    this.accountsData = accounts;
    var mappedAvailData = this.mapAvailableSegmentData(accounts);
    this.view.addAndRemoveAccounts.segAddOptions.info = {"segData": mappedAvailData};
  },
  /*
	 * display customer create screen
	 */
  showCustomerCreateScreen: function () {
    var self = this;
    self.view.flxCustomerCreate.setVisibility(true);
    self.view.flxCustomerDetails.setVisibility(true);
    self.view.flxCustomerAccounts.setVisibility(false);
    self.view.flxCustomerRolesLimits.setVisibility(false);
    self.view.lblUsernameCheck.setVisibility(false);
    if(self.action === self.actionConfig.create){
      self.view.textBoxEntry21.tbxEnterValue.setEnabled(true);
      self.view.textBoxEntry32.tbxEnterValue.setEnabled(true);
      self.view.textBoxEntry21.btnCheck.setEnabled(true);
    }
    var widgetArray = [this.view.verticalTabsCustomer.btnOption1, this.view.verticalTabsCustomer.btnOption2, this.view.verticalTabsCustomer.btnOption3];
    self.tabUtilVerticleButtonFunction(widgetArray, this.view.verticalTabsCustomer.btnOption1);
    var widgetArray1 = [this.view.verticalTabsCustomer.lblSelected1,this.view.verticalTabsCustomer.lblSelected2,this.view.verticalTabsCustomer.lblSelected3];
    self.tabUtilVerticleArrowVisibilityFunction(widgetArray1,this.view.verticalTabsCustomer.lblSelected1);
    self.view.forceLayout();
  },
  /*
   * hides customer create screen when creation is cancelled
   */
  hideCustomerCreateScreen: function () {
    var self = this;
    self.clearDataForAccounts();
    self.clearDataForDetails();
    self.clearDataForRolesLimits();
    var orgId = "";
    if(self.action === self.actionConfig.create){
      orgId = self.comapnyDetails.companyID;
    } else {
      orgId = self.customerDetails.company.id;
    }
    var param = {"action":"createCustomer","id": orgId};
    self.presenter.hideCreatecompanyScreen(param,true,null);
  },
  /*
	 * display accounts tab screen in customer create screen
	 */
  showAccountsScreen: function () {
    var self = this;
    var accountsToShow = [];
    var tin = "",membership = "",type = "";
    kony.adminConsole.utils.showProgressBar(self.view);
    self.view.flxCustomerDetails.setVisibility(false);
    self.view.flxCustomerAccounts.setVisibility(true);
    self.view.flxCustomerRolesLimits.setVisibility(false);
    type = (self.action === self.actionConfig.create) ? self.comapnyDetails.type : self.customerDetails.company.TypeId;
    self.adjustAccountsUIBasedOnType(type);
    var widgetArray = [this.view.verticalTabsCustomer.btnOption1, this.view.verticalTabsCustomer.btnOption2, this.view.verticalTabsCustomer.btnOption3];
    self.tabUtilVerticleButtonFunction(widgetArray, this.view.verticalTabsCustomer.btnOption2);
    var widgetArray1 = [this.view.verticalTabsCustomer.lblSelected1,this.view.verticalTabsCustomer.lblSelected2,this.view.verticalTabsCustomer.lblSelected3];
    self.tabUtilVerticleArrowVisibilityFunction(widgetArray1,this.view.verticalTabsCustomer.lblSelected2);
    var data = self.view.addAndRemoveAccounts.segSelectedOptions.info.segData || [];
    self.view.addAndRemoveAccounts.segSelectedOptions.setData(data);
    //clearing search
    self.view.addAndRemoveAccounts.tbxSearchBox.text = "";
    accountsToShow = self.filterAlreadyAddedData(self.view.addAndRemoveAccounts.segAddOptions.info.segData);
    self.view.addAndRemoveAccounts.segAddOptions.info.segData = accountsToShow;
    self.view.addAndRemoveAccounts.segAddOptions.setData(accountsToShow);
    self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Search_to_see_available_accounts");
    //display tin,membership for microBusiness type
    if(type === self.typeConfig.microBusiness){
      if(data.length > 0){
        membership = data[0].membershipId;
        tin = data[0].tin;
      } else{
        membership = accountsToShow[0].membershipId;
        tin = accountsToShow[0].tin;
      }
      self.view.lblMembershipValue.text = membership ||"-";
      self.view.lblTinValue.text = tin || "-";
    }
    self.showHideAddRemoveSegment();
    self.view.forceLayout();
    kony.adminConsole.utils.hideProgressBar(self.view);
  },
  /*
	 * display set limits screen tab in customer create screen
	 */
  showLimitsScreen: function () {
    var self = this;
    self.view.flxCustomerDetails.setVisibility(false);
    self.view.flxCustomerAccounts.setVisibility(false);
    self.view.flxCustomerRolesLimits.setVisibility(true);
    if(self.action === self.actionConfig.create){
      self.view.commonButtonsRolesLimit.btnSave.text = kony.i18n.getLocalizedString("i18n.frmPermissionsController.CREATE");
    } else{
      self.view.commonButtonsRolesLimit.btnSave.text = kony.i18n.getLocalizedString("i18n.frmOutageMessageController.UPDATE");
    }
    var widgetArray = [this.view.verticalTabsCustomer.btnOption1, this.view.verticalTabsCustomer.btnOption2, this.view.verticalTabsCustomer.btnOption3];
    self.tabUtilVerticleButtonFunction(widgetArray, this.view.verticalTabsCustomer.btnOption3);
    var widgetArray1 = [this.view.verticalTabsCustomer.lblSelected1,this.view.verticalTabsCustomer.lblSelected2,this.view.verticalTabsCustomer.lblSelected3];
    self.tabUtilVerticleArrowVisibilityFunction(widgetArray1,this.view.verticalTabsCustomer.lblSelected3);
    self.view.forceLayout();
  },
  /*
   * clears all fields in create customer screen tabs
   */
  clearDataForDetails : function(){
    var self =this;
    self.clearValidationsForDetails();
    self.view.flxCustomerDetails.setVisibility(true);
    self.view.flxCustomerAccounts.setVisibility(false);
    self.view.flxCustomerRolesLimits.setVisibility(false);
    self.view.addAndRemoveAccounts.flxError.setVisibility(false);
    self.view.textBoxEntry11.tbxEnterValue.text = "";
    self.view.textBoxEntry12.tbxEnterValue.text = "";
    self.view.textBoxEntry13.tbxEnterValue.text = "";
    self.view.textBoxEntry21.tbxEnterValue.text = "";
    self.view.textBoxEntry22.tbxEnterValue.text = "";
    self.view.customCalCustomerDOB.value = "";
    self.view.customCalCustomerDOB.resetData = "Select Date";
    self.view.textBoxEntry32.tbxEnterValue.text = "";
    self.view.textBoxEntry33.tbxEnterValue.text = "";
    self.view.textBoxEntry23.txtContactNumber.text = "";
    self.view.textBoxEntry23.txtISDCode.text = "";
    self.view.customCalCustomerDOB.resetData = kony.i18n.getLocalizedString("i18n.frmLogsController.Select_Date");
    self.view.imgFlag1.src = self.AdminConsoleCommonUtils.checkbox;
    self.view.imgFlag2.src = self.AdminConsoleCommonUtils.checkbox;
    self.view.imgFlag3.src = self.AdminConsoleCommonUtils.checkbox;
    self.view.switchEagreement.selectedIndex = 1;
    self.view.forceLayout();
  },
  clearDataForAccounts : function(){
    var self = this;
    self.view.addAndRemoveAccounts.segSelectedOptions.info ={"segData":[]};
    self.view.addAndRemoveAccounts.tbxSearchBox.text = "";
    self.view.addAndRemoveAccounts.segAddOptions.setData([]);
    self.view.addAndRemoveAccounts.segSelectedOptions.setData([]);
    self.showHideAddRemoveSegment();
  },
  clearDataForRolesLimits : function(){
    var self =this;
    self.view.flxNoResults.setVisibility(true);
    self.view.flxSegLimitsContainer.setVisibility(false);
    self.view.lstBoxSelectRole.selectedKey = "select";
    self.view.segLimits.setData([]);
    self.view.forceLayout();
  },
  /*
	 * validate detail screen fields
	 */
  detailsScreenValidation: function () {
    var self = this;
    var isValid = true;
    //firstname
    if (self.view.textBoxEntry11.tbxEnterValue.text.trim() === "") {
      self.view.textBoxEntry11.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxEntry11.flxInlineError.setVisibility(true);
      self.view.textBoxEntry11.lblErrorText.text = kony.i18n.getLocalizedString("i18n.Common.frmGuestDashboard.FirstNameMissing");
      isValid = false;
    }
    //lastname
    if (self.view.textBoxEntry13.tbxEnterValue.text.trim() === "") {
      self.view.textBoxEntry13.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxEntry13.flxInlineError.setVisibility(true);
      self.view.textBoxEntry13.lblErrorText.text = kony.i18n.getLocalizedString("i18n.Common.frmGuestDashboard.LastNameMissing");
      isValid = false;
    }
    //username
    var usernameValid = self.userNameValidation();
    if (!usernameValid) {
      isValid = false;
    }
    //email-id
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (self.view.textBoxEntry22.tbxEnterValue.text.trim() === "") {
      self.view.textBoxEntry22.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxEntry22.flxInlineError.setVisibility(true);
      self.view.textBoxEntry22.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EmailId_cannot_be_empty");
      isValid = false;
    } else if (emailRegex.test(self.view.textBoxEntry22.tbxEnterValue.text.trim()) === false) {
      self.view.textBoxEntry22.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxEntry22.flxInlineError.setVisibility(true);
      self.view.textBoxEntry22.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EmailID_not_valid");
      isValid = false;
    }
    //contact num
    var phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    if (!self.view.textBoxEntry23.txtContactNumber.text || !self.view.textBoxEntry23.txtContactNumber.text.trim()) {
      self.view.textBoxEntry23.txtContactNumber.skin = "skinredbg";
      self.view.textBoxEntry23.flxError.left = "36.5%";
      self.view.textBoxEntry23.flxError.setVisibility(true);
      self.view.textBoxEntry23.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_cannot_be_empty");
      isValid = false;
    } else if (self.view.textBoxEntry23.txtContactNumber.text.trim().length > 15) {
      self.view.textBoxEntry23.txtContactNumber.skin = "skinredbg";
      self.view.textBoxEntry23.flxError.left = "36.5%";
      self.view.textBoxEntry23.flxError.setVisibility(true);
      self.view.textBoxEntry23.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_cannot_exceed");
      isValid = false;
    } else if (phoneRegex.test(self.view.textBoxEntry23.txtContactNumber.text) === false) {
      self.view.textBoxEntry23.txtContactNumber.skin = "skinredbg";
      self.view.textBoxEntry23.flxError.left = "36.5%";
      self.view.textBoxEntry23.flxError.setVisibility(true);
      self.view.textBoxEntry23.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_not_valid");
      isValid = false;
    }
    //ISD code
    var ISDRegex = /^\+(\d{1,3}|\d{1,3})$/;
    if ((!self.view.textBoxEntry23.txtISDCode.text) ||
        (!self.view.textBoxEntry23.txtISDCode.text.trim())||
        (self.view.textBoxEntry23.txtISDCode.text.trim().length > 4) ||
        (self.view.textBoxEntry23.txtISDCode.text === "+")||
        (ISDRegex.test(self.view.textBoxEntry23.txtISDCode.text) === false))
    {
      self.view.textBoxEntry23.txtISDCode.skin = "skinredbg";
      self.view.textBoxEntry23.flxError.left = "0%";
      self.view.textBoxEntry23.flxError.setVisibility(true);
      self.view.textBoxEntry23.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
      isValid = false;
    }
    var selDate = self.view.customCalCustomerDOB.value;
    var formatDate = selDate.split("/");
    var currDate = new Date();
    var otherDate = new Date(formatDate[2],formatDate[0]-1,formatDate[1]);
    //DOB
    if (self.view.customCalCustomerDOB.value === "") {
      self.view.flxCalendarDOB.skin = "sknFlxCalendarError";
      self.view.textBoxEntry31.flxInlineError.setVisibility(true);
      self.view.textBoxEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.DOB_cannot_be_empty");
      isValid = false;
    } else if(otherDate > currDate ){
      self.view.flxCalendarDOB.skin = "sknFlxCalendarError";
      self.view.textBoxEntry31.flxInlineError.setVisibility(true);
      self.view.textBoxEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Select_valid_DOB");
      isValid = false;
    }
    //SSN
    if (self.view.textBoxEntry32.tbxEnterValue.text.trim() === "") {
      self.view.textBoxEntry32.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxEntry32.flxInlineError.setVisibility(true);
      self.view.textBoxEntry32.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.SSN_cannot_be_empty");
      isValid = false;
    }
    self.view.forceLayout();
    return isValid;
  },
  /*
   * clears the inline error message and skins
   */
  clearValidationsForDetails : function(txtBoxPath, errFlexPath, flxBoxPath){
    var self = this;
    if(flxBoxPath){
      flxBoxPath.skin = "sknflxEnterValueNormal";
      if(errFlexPath) errFlexPath.setVisibility(false);
    }
    else if(txtBoxPath){
      txtBoxPath.skin = "skntbxLato35475f14px";
      if(errFlexPath) errFlexPath.setVisibility(false);
    } else{
      self.view.textBoxEntry11.flxEnterValue.skin = "sknflxEnterValueNormal";
      self.view.textBoxEntry13.flxEnterValue.skin = "sknflxEnterValueNormal";
      self.view.textBoxEntry21.flxEnterValue.skin = "sknflxEnterValueNormal";
      self.view.textBoxEntry22.flxEnterValue.skin = "sknflxEnterValueNormal";
      self.view.textBoxEntry23.txtContactNumber.skin = "skntbxLato35475f14px";
      self.view.textBoxEntry23.txtISDCode.skin = "skntbxLato35475f14px";
      self.view.textBoxEntry32.flxEnterValue.skin = "sknflxEnterValueNormal";
      self.view.flxCalendarDOB.skin = "sknflxffffffoptemplateop3px";

      self.view.textBoxEntry11.flxInlineError.setVisibility(false);
      self.view.textBoxEntry13.flxInlineError.setVisibility(false);
      self.view.textBoxEntry21.flxInlineError.setVisibility(false);
      self.view.textBoxEntry22.flxInlineError.setVisibility(false);
      self.view.textBoxEntry23.flxError.setVisibility(false);
      self.view.textBoxEntry31.flxInlineError.setVisibility(false);
      self.view.textBoxEntry32.flxInlineError.setVisibility(false);
    }
    self.view.forceLayout();
  },
  clearValidationForRolesLimits : function(){
    var self = this;
    self.view.lstBoxSelectRole.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
    self.view.flxInlineError.setVisibility(false);
  },
  /*
	 * filters accounts data based on search text
	 */
  accountSearch: function (searchText,type) {
    var self = this;
    var sourceData =[],
        searchResult = [],searchType = "",selectedType = "";
    sourceData = self.view.addAndRemoveAccounts.segAddOptions.info.segData;
    if (searchText) {
      searchResult = sourceData.filter(function (rec) {
        if(type === self.typeConfig.microBusiness && (rec.lblAccountNum.text.indexOf(searchText) >= 0)){
          return rec;
        }else if(type === self.typeConfig.smallBusiness){
          var searchTypeJson = {"membership id": rec.lblAccFieldValue2.text,
                      "tax number": rec.lblAccFieldValue3.text,
                      "account id" : rec.lblAccountNum.text};
            selectedType = self.view.addAndRemoveAccounts.lblSelSearchType.text;
            searchType = searchTypeJson[selectedType.toLowerCase()];
            if ((searchType.indexOf(searchText) >= 0)) {
              return rec;
            }
          }
      });
      var filterList = self.filterAlreadyAddedData(searchResult);
      self.view.addAndRemoveAccounts.segAddOptions.setData(filterList);
      self.view.addAndRemoveAccounts.segAddOptions.setVisibility((filterList.length > 0));
      self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.setVisibility((filterList.length <= 0));
      if(filterList.length > 0){
        self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Search_to_see_available_accounts");
      }else{
        self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmLogsController.No_results_found");
      }
    } else {
      var filterList1 = self.filterAlreadyAddedData(sourceData);
      self.view.addAndRemoveAccounts.segAddOptions.setData(filterList1);
      self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Search_to_see_available_accounts");
      self.showHideAddRemoveSegment();
    }
    self.view.forceLayout();
  },
  /*
	 * filters already added data from the search result
	 * data resulted from the search
	 */
  filterAlreadyAddedData: function (sourceData) {
    var self = this;
    var finalData = [],
        doesExsist = false;
    var searchDataLen = sourceData.length || 0;
    var selectedAccounts = self.view.addAndRemoveAccounts.segSelectedOptions.data || [];
    if (searchDataLen > 0) {
      finalData = sourceData.filter(function (rec) {
        doesExsist = false;
        for (var i = 0; i < selectedAccounts.length; i++) {
          if (selectedAccounts[i].accountId === rec.lblAccountNum.text) {
            doesExsist = true;
            break;
          }
        }
        if (!doesExsist) {
          return rec;
        }
      });
    }
    return finalData;
  },
  /*
	 * function to map data to available segment on search
	 * @param: result of search
	 */
  mapAvailableSegmentData: function (data) {
    var self = this;
    var result = [];
    var type = self.typeConfig.smallBusiness;
    type = (self.view.flxHeading.isVisible === true) ? self.typeConfig.microBusiness : self.typeConfig.smallBusiness;
    var widgetMap = {
      "lblAccountNum": "lblAccountNum",
      "lblAccFieldValue1": "lblAccFieldValue1",
      "lblAccFieldValue2": "lblAccFieldValue2",
      "lblAccFieldValue3": "lblAccFieldValue3",
      "lblAccountText": "lblAccountText",
      "lblAccFieldHeader1": "lblAccFieldHeader1",
      "lblAccFieldHeader2": "lblAccFieldHeader2",
      "lblAccFieldHeader3": "lblAccFieldHeader3",
      "btnAddAccount": "btnAddAccount",
      "lblLine": "lblLine",
      "flxAccField1": "flxAccField1",
      "flxAccField3": "flxAccField3",
      "flxAccField2": "flxAccField2",
      "flxAvailableAccounts": "flxAvailableAccounts",
      "name":"name",
      "holder":"holder",
      "membershipId":"membershipId",
      "tin":"tin"
    };
    self.view.addAndRemoveAccounts.segAddOptions.widgetDataMap = widgetMap;
    result = data.map(function (record) {
      return {
        "lblAccountNum": {
          "text": record.Account_id
        },
        "lblAccFieldValue1": {
          "text": record.Account_Type
        },
        "lblAccFieldValue2": (type === self.typeConfig.smallBusiness) ? {
        	"text": self.AdminConsoleCommonUtils.getTruncatedString(record.Membership_id, 13, 10),
        	"info": {
        		"value": record.Membership_id
        	},
        	"tooltip": record.Membership_id
        }
         : {
        	"text": self.AdminConsoleCommonUtils.getTruncatedString(record.accountName, 13, 10),
        	"info": {
        		"value": record.accountName
        	},
        	"tooltip": record.accountName
        },
        "lblAccFieldValue3": (type === self.typeConfig.smallBusiness) ? {
        	"text": self.AdminConsoleCommonUtils.getTruncatedString(record.Taxid, 10, 8),
        	"info": {
        		"value": record.Taxid
        	},
        	"tooltip": record.Taxid
        }
         : {
        	"text": self.AdminConsoleCommonUtils.getTruncatedString(record.username, 10, 8),
        	"info": {
        		"value": record.username
        	},
        	"tooltip": record.username
        },
        "lblAccountText": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ACCOUNTS"),
        "lblAccFieldHeader1": kony.i18n.getLocalizedString("i18n.Group.TYPE"),
        "lblAccFieldHeader2":(type === self.typeConfig.smallBusiness) ?
        						kony.i18n.getLocalizedString("i18n.frmCompanies.MEMBERSHIP_ID") : kony.i18n.getLocalizedString("i18n.permission.NAME"),
        "lblAccFieldHeader3":(type === self.typeConfig.smallBusiness) ?
        						kony.i18n.getLocalizedString("i18n.frmCompanies.TAXNUMBER") : kony.i18n.getLocalizedString("i18n.frmCompanies.HOLDER"),
        "btnAddAccount": {
          "onClick": self.addSelectedAccount
        },
        "name": record.accountName,
        "holder": record.username,
        "membershipId":record.Membership_id,
        "tin":record.Taxid,
        "lblLine":{"text": "-"},
        "template": "flxAvailableAccounts"
      };
    });
    return result;
  },
  /*
   * function to map data to selected segment
   * @param: customer assigned accounts
   * @return: mapped data for segment
	*/
  mapSelectedSegmentData : function(data){
    var self =this;
    var result = [];
    var widgetMap = {
      "flxClose": "flxClose",
      "fontIconClose": "fontIconClose",
      "lblOption": "lblOption",
      "accountId": "accountId",
      "membershipId": "membershipId",
      "tin": "tin",
      "name":"name",
      "holder":"holder",
      "accountType": "accountType",
      "flxOptionAdded": "flxOptionAdded"
    };
    self.view.addAndRemoveAccounts.segSelectedOptions.widgetDataMap = widgetMap;
    result = data.map(function(record){
      return {
        "flxClose": {
          "onClick": function () {
            self.removeSelectedAccount();
          },
          "isVisible":true
        },
        "fontIconClose": {
          "text": "\ue929",
          "tooltip": kony.i18n.getLocalizedString("i18n.frmCompanies.Remove_account")
        },
        "lblOption": {
          "text": "Account  " + self.AdminConsoleCommonUtils.getTruncatedString(record.Account_id, 15, 12),
          "tooltip": record.Account_id
        },
        "accountId": record.Account_id,
        "membershipId": record.Membership_id || "-",
        "tin": record.Taxid || "-",
        "name": record.accountName,
        "holder":record.username || "-",
        "accountType": record.Account_Type,
        "template": "flxOptionAdded"
      };
    });
    return result;
  },
  /*
	 * (Add Accounts) add left segment selected row to right segment
	 */
  addSelectedAccount: function () {
    var self = this;
    var selectedRowData = [];
    var type = self.typeConfig.smallBusiness;
    type = (self.view.flxHeading.isVisible === true) ? self.typeConfig.microBusiness : self.typeConfig.smallBusiness;
    var rowIndex = self.view.addAndRemoveAccounts.segAddOptions.selectedIndex[1];
    selectedRowData = self.view.addAndRemoveAccounts.segAddOptions.selectedRowItems[0];
    var widgetMap = {
      "flxClose": "flxClose",
      "fontIconClose": "fontIconClose",
      "lblOption": "lblOption",
      "accountId": "accountId",
      "membershipId": "membershipId",
      "tin": "tin",
      "name":"name",
      "holder":"holder",
      "accountType": "accountType",
      "flxOptionAdded": "flxOptionAdded"
    };
    var recordToRemove = {
      "flxClose": {
        "onClick": function () {
          self.removeSelectedAccount();
        }
      },
      "fontIconClose": {
        "text": "\ue929",
        "tooltip": kony.i18n.getLocalizedString("i18n.frmCompanies.Remove_account")
      },
      "lblOption": {
        "text": "Account  " + self.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.lblAccountNum.text, 15, 12),
        "tooltip": selectedRowData.lblAccountNum.text
      },
      "accountId": selectedRowData.lblAccountNum.text,
      "membershipId": (type === self.typeConfig.smallBusiness) ? selectedRowData.lblAccFieldValue2.info.value : selectedRowData.membershipId,
      "tin": (type === self.typeConfig.smallBusiness) ? selectedRowData.lblAccFieldValue3.info.value : selectedRowData.tin,
      "name":(type === self.typeConfig.smallBusiness) ?  selectedRowData.name : selectedRowData.lblAccFieldValue2.info.value,
      "holder":(type === self.typeConfig.smallBusiness) ? selectedRowData.holder : selectedRowData.lblAccFieldValue3.info.value,
      "accountType": selectedRowData.lblAccFieldValue1.text,
      "template": "flxOptionAdded"
    };
    self.assignedAccounts.push(recordToRemove);
    self.view.addAndRemoveAccounts.segSelectedOptions.widgetDataMap = widgetMap;
    if(self.view.addAndRemoveAccounts.segSelectedOptions.data.length === 0){
      self.view.addAndRemoveAccounts.segSelectedOptions.setData([recordToRemove]);
    }else{
      self.view.addAndRemoveAccounts.segSelectedOptions.addDataAt(recordToRemove, 0);
    }
    self.view.addAndRemoveAccounts.segAddOptions.removeAt(rowIndex);
    self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    self.view.addAndRemoveAccounts.segAddOptions.info.segData = self.view.addAndRemoveAccounts.segAddOptions.data;
    self.showHideAddRemoveSegment();
    self.view.forceLayout();
  },
  /*
	 * (Remove Accounts) add right segment selected row to left segment
	 */
  removeSelectedAccount: function () {
    var self = this;
    var selectedRowData = [],type = self.typeConfig.smallBusiness;
    type = (self.view.flxHeading.isVisible === true) ? self.typeConfig.microBusiness : self.typeConfig.smallBusiness;
    var rowIndex = self.view.addAndRemoveAccounts.segSelectedOptions.selectedIndex[1];
    selectedRowData = self.view.addAndRemoveAccounts.segSelectedOptions.selectedRowItems[0];
    var recordToAdd = {
      "lblAccountNum": {
        "text": selectedRowData.accountId
      },
      "lblAccFieldValue1": {
        "text": selectedRowData.accountType
      },
      "lblAccFieldValue2": (type === self.typeConfig.smallBusiness) ? {
      	"text": self.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.membershipId, 13, 10),
      	"info": {
      		"value": selectedRowData.membershipId
      	},
      	"tooltip": selectedRowData.membershipId
      }
       : {
      	"text": self.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.name, 13, 10),
      	"info": {
      		"value": selectedRowData.name
      	},
      	"tooltip": selectedRowData.name
      },
      "lblAccFieldValue3": (type === self.typeConfig.smallBusiness) ? {
      	"text": self.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.tin, 10, 8),
      	"info": {
      		"value": selectedRowData.tin
      	},
      	"tooltip": selectedRowData.tin
      }
       : {
      	"text": self.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.holder, 10, 8),
      	"info": {
      		"value": selectedRowData.holder
      	},
      	"tooltip": selectedRowData.holder
      },
      "lblAccountText": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ACCOUNTS"),
      "lblAccFieldHeader1": kony.i18n.getLocalizedString("i18n.Group.TYPE"),
      "lblAccFieldHeader2": (type === self.typeConfig.smallBusiness) ? kony.i18n.getLocalizedString("i18n.frmCompanies.MEMBERSHIP_ID"):
      													kony.i18n.getLocalizedString("i18n.permission.NAME"),
      "lblAccFieldHeader3": (type === self.typeConfig.smallBusiness) ? kony.i18n.getLocalizedString("i18n.frmCompanies.TAXNUMBER") :
      													kony.i18n.getLocalizedString("i18n.frmCompanies.HOLDER"),
      "btnAddAccount": {
        "onClick": self.addSelectedAccount
      },
      "name": selectedRowData.name,
      "holder": selectedRowData.holder,
      "membershipId":selectedRowData.membershipId,
      "tin":selectedRowData.tin,
      "lblLine":{"text": "-"},
      "template": "flxAvailableAccounts"
    };
    self.view.addAndRemoveAccounts.segAddOptions.addDataAt(recordToAdd, 0);
    self.view.addAndRemoveAccounts.segSelectedOptions.removeAt(rowIndex);
    self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    self.view.addAndRemoveAccounts.segAddOptions.info.segData = self.view.addAndRemoveAccounts.segAddOptions.data;
    self.showHideAddRemoveSegment();
    self.view.forceLayout();
  },
  /*
	 * reset the added accounts in selected accounts column
	 */
  resetAccounts: function () {
    var self = this;
    var selectedAcc = [],
        resetRec = [],
        exsistingData = [],
        dataToAdd = [];
    var type = self.typeConfig.smallBusiness;
    type = (self.view.flxHeading.isVisible === true) ? self.typeConfig.microBusiness : self.typeConfig.smallBusiness;
    selectedAcc = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    resetRec = selectedAcc.map(function (record) {
      return {
        "lblAccountNum": {
          "text": record.accountId
        },
        "lblAccFieldValue1": {
          "text": record.accountType
        },
        "lblAccFieldValue2": (type === self.typeConfig.smallBusiness) ? {
        	"text": self.AdminConsoleCommonUtils.getTruncatedString(record.membershipId, 13, 10),
        	"info": {
        		"value": record.membershipId
        	},
        	"tooltip": record.membershipId
        }
         : {
        	"text": self.AdminConsoleCommonUtils.getTruncatedString(record.name, 13, 10),
        	"info": {
        		"value": record.name
        	},
        	"tooltip": record.name
        },
        "lblAccFieldValue3": (type === self.typeConfig.smallBusiness) ? {
        	"text": self.AdminConsoleCommonUtils.getTruncatedString(record.tin, 10, 8),
        	"info": {
        		"value": record.tin
        	},
        	"tooltip": record.tin
        }
         : {
        	"text": self.AdminConsoleCommonUtils.getTruncatedString(record.holder, 10, 8),
        	"info": {
        		"value": record.holder
        	},
        	"tooltip": record.holder
        },
        "lblAccountText": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ACCOUNTS"),
        "lblAccFieldHeader1": kony.i18n.getLocalizedString("i18n.Group.TYPE"),
        "lblAccFieldHeader2": (type === self.typeConfig.smallBusiness) ? kony.i18n.getLocalizedString("i18n.frmCompanies.MEMBERSHIP_ID"):
        															kony.i18n.getLocalizedString("i18n.permission.NAME"),
        "lblAccFieldHeader3": (type === self.typeConfig.smallBusiness) ? kony.i18n.getLocalizedString("i18n.frmCompanies.TAXNUMBER") :
        															kony.i18n.getLocalizedString("i18n.frmCompanies.HOLDER"),
        "btnAddAccount": {
          "onClick": self.addSelectedAccount
        },
        "name": record.name,
        "holder": record.holder,
        "membershipId":record.membershipId,
        "tin":record.tin,
        "lblLine":{"text": "-"},
        "template": "flxAvailableAccounts"
      };
    });
    exsistingData = self.view.addAndRemoveAccounts.segAddOptions.data;
    dataToAdd = exsistingData.concat(resetRec);
    self.view.addAndRemoveAccounts.segAddOptions.setData(dataToAdd);
    self.view.addAndRemoveAccounts.segAddOptions.info.segData = self.view.addAndRemoveAccounts.segAddOptions.data;
    self.view.addAndRemoveAccounts.segSelectedOptions.setData([]);
    self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = [];
    self.showHideAddRemoveSegment();
    self.view.forceLayout();
  },
  /*
	 * validation to check atleast one account is assigned
	 */
  checkAccountValidation: function () {
    var self = this;
    var selectedAcc = [];
    selectedAcc = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    return (selectedAcc.length > 0);
  },
  /*
	 * hide/show segment or noResultsFound in accounts screen
	 */
  showHideAddRemoveSegment: function () {
    var self = this;
    var availableSegData = [],
        selectedSegData = [];
    availableSegData = self.view.addAndRemoveAccounts.segAddOptions.data;
    selectedSegData = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    self.view.addAndRemoveAccounts.segAddOptions.setVisibility(availableSegData.length > 0);
    self.view.addAndRemoveAccounts.segSelectedOptions.setVisibility(selectedSegData.length > 0);
    self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.setVisibility(availableSegData.length <= 0);
    self.view.addAndRemoveAccounts.rtxSelectedOptionsMessage.setVisibility(selectedSegData.length <= 0);
    self.view.addAndRemoveAccounts.btnRemoveAll.setVisibility(selectedSegData.length > 0);
    //validation -atleast one account should be selected
    if (selectedSegData.length > 0) {
      self.view.commonButtonsAccounts.btnSave.skin = "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px";
      self.view.commonButtonsAccounts.btnSave.hoverSkin= "sknBtn1f81b2LatoBoldffffff12pxBg0b70ccRadius28Px";
      self.view.commonButtonsAccounts.btnSave.focusSkin= "sknBtn11abebLatoBoldffffff12Px11abebRadius20Px";
      self.view.commonButtonsAccounts.btnSave.setEnabled(true);
      self.view.addAndRemoveAccounts.flxError.setVisibility(false);
    } else {
      self.view.commonButtonsAccounts.btnSave.skin = "btnSkinGrey";
      self.view.commonButtonsAccounts.btnSave.hoverSkin= "btnSkinGrey";
      self.view.commonButtonsAccounts.btnSave.focusSkin= "btnSkinGrey";
      self.view.commonButtonsAccounts.btnSave.setEnabled(false);
    }
  },
  /*
	 * fill data for roles listbox
	 */
  setRolesForLimits: function (roles) {
    var self = this;
    var rolesData = [];
    rolesData = roles.reduce(
      function (list, record) {
        return list.concat([[record.id, record.Name]]);
      }, [["select", kony.i18n.getLocalizedString("i18n.frmCompanies.Select_a_role")]]);
    self.view.lstBoxSelectRole.masterData = rolesData;
    self.view.lstBoxSelectRole.selectedKey = "select";
  },
  /*
  	*Fill data for salutation
    */
    fillSalutation: function () {
    var data = [];
    data.push(["lbl1", kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Select_Salutation")]);
    data.push([kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Mr"), kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Mr")]);
    data.push(["Mrs.", "Mrs."]);
    data.push(["Dr.", "Dr."]);
    data.push(["Ms.", "Ms."]);
    this.view.lstBxSalutation.masterData = data;
    this.view.lstBxSalutation.selectedKey = "lbl1";
  },
    fillCustomerFlags: function () {
      var StatusGroup=[{Description: "Defaulter",Type_id: "STID_CUSTOMERFLAGS",id: "SID_DEFAULTER"},{Description: "Fraud Detected",Type_id: "STID_CUSTOMERFLAGS",id: "SID_FRAUDDETECTED"},{Description: "High Risk",Type_id: "STID_CUSTOMERFLAGS",id: "SID_HIGHRISK"}];
    for (var i = 1; i <=3; i++) {
      this.view["lblFlag" + i].text = StatusGroup[i - 1].Description;
      this.view["imgFlag" + i].info = {
        "Key": StatusGroup[i - 1].id
      };
    }
  },
  /*
	 * map permissions data to segment and display
	 */
  setPermissionLimits: function (data) {
    var self = this;
    var segData = [],transData = [];
    var widgetMap = {
      "id": "id",
      "lblPermissionName": "lblPermissionName",
      "tbxMaxDailyLimit": "tbxMaxDailyLimit",
      "tbxTransactionLimit": "tbxTransactionLimit",
      "lblDailyLimit": "lblDailyLimit",
      "lblTransactionLimit":"lblTransactionLimit",
      "flxLine": "flxLine",
      "flxLineVertical": "flxLineVertical",
      "flxDailyLimit":"flxDailyLimit",
      "flxTransactionLimit":"flxTransactionLimit",
      "lblCurrencyValue":"lblCurrencyValue",
      "lblTransactionCurrency":"lblTransactionCurrency",
      "flxErrorDailyLimit": "flxErrorDailyLimit",
      "flxErrorTransaction": "flxErrorTransaction",
      "lblErrorTextDaily": "lblErrorTextDaily",
      "lblErrorTextTransaction": "lblErrorTextTransaction",
      "lblErrorIconDaily":"lblErrorIconDaily",
      "lblErrorIconTransaction":"lblErrorIconTransaction",
      "flxLimitsCustomer": "flxLimitsCustomer"
    };
    transData = data.filter(function(rec){
      if(rec.Type_id === self.serviceTypeConfig.transactional){
        return rec;
      }
    })
    segData = transData.map(function (rec) {
      return {
        "id": rec.Service_id,
        "lblPermissionName": {"text":rec.Name},
        "flxDailyLimit": {"skin":"sknflxffffffop100dbdbe6Radius3px"},
        "tbxMaxDailyLimit": {"text":"",
                             "placeholder":kony.i18n.getLocalizedString("i18n.frmLogs.blSearchParam"),
                             "skin":"skntbxffffffNoBorderlato35475f14px",
                             "onKeyUp": self.clearErrorForLimit},
        "flxLine": {"isVisible":true},
        "flxTransactionLimit": {"skin":"sknflxffffffop100dbdbe6Radius3px"},
        "tbxTransactionLimit": {"text":"",
                                "placeholder":kony.i18n.getLocalizedString("i18n.frmLogs.blSearchParam"),
                                "skin":"skntbxffffffNoBorderlato35475f14px",
                                "onKeyUp": self.clearErrorForLimit},
        "flxLineVertical": {"isVisible":true},
        "lblCurrencyValue": {"isVisible":true},
        "lblTransactionCurrency": {"isVisible":true},
        "flxErrorDailyLimit": {"isVisible":false},
        "flxErrorTransaction": {"isVisible":false},
        "lblErrorIconDaily": {"isVisible":true},
        "lblErrorIconTransaction": {"isVisible":true},
        "lblErrorTextDaily": {"text":"Error"},
        "lblErrorTextTransaction":{"text":"Error"},
        "template": "flxLimitsCustomer",
        "MaxTransferLimit": rec.MaxTransferLimit,
        "MaxDailyLimit": rec.MaxDailyLimit,
        "MinTransferLimit": rec.MinTransferLimit
      };
    });
    self.view.segLimits.widgetDataMap = widgetMap;
    self.view.segLimits.setData(segData);
    self.view.flxSegLimitsContainer.setVisibility(segData.length > 0);
    if(self.view.lstBoxSelectRole.selectedKey === "select"){
      self.view.rtxNoResults.text = kony.i18n.getLocalizedString("i18n.frmCompanies.No_limits_found_Select_role_to_view_limits");
    }else{
      self.view.rtxNoResults.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Set_limit_not_applicable");
    }
    self.view.flxNoResults.setVisibility(segData.length <= 0);
    self.view.forceLayout();
  },
  /*
   * function to call presentationControler's to get permissions list
	 */
  getPermissionsForSelectedRole: function (id) {
    var self = this;
    var selectedItem = self.view.lstBoxSelectRole.selectedKey;
    if (selectedItem && selectedItem !== "select") {
      self.presenter.getPermissionsOfRole({
        "Group_id": selectedItem
      });
    } else if(selectedItem === "select"){
      self.setPermissionLimits([]);
    }
  },
  /*
   * validates limits entered for permissions
   */
  validateLimitsScreen : function(){
    var self =this;
    var isValid =true;
    var dailyLimitIndices =[],transLimitIndices =[];
    var selItem = self.view.lstBoxSelectRole.selectedKey;
    if(selItem === "select"){
      self.view.flxInlineError.setVisibility(true);
      self.view.lstBoxSelectRole.skin = "redListBxSkin";
      isValid = false;
    }
    isValid = self.validateDailyLimitValues();
    isValid = self.validateTransferLimitValues();
    for(var i=0; i< self.view.segLimits.data.length; i++){
      if(self.view.segLimits.data[i].flxErrorDailyLimit.isVisible)
        dailyLimitIndices.push(i);
      if(self.view.segLimits.data[i].flxErrorTransaction.isVisible)
        transLimitIndices.push(i);
    }
    self.setFocusToMinimumRow(dailyLimitIndices, transLimitIndices);
    self.view.forceLayout();
    return isValid;
  },
  /*
   * function to set scroll to a particular row when error is shown
   * @param: array of row indices of inline error textboxes
   */
  setFocusToMinimumRow : function(limitArr,tranArr){
    var ind = null;
    if(limitArr.length >0 && tranArr.length>0){
      ind = Math.min(limitArr[0],tranArr[0]);
    }else if(limitArr.length>0){
      ind = limitArr[0];
    }else if(tranArr.length>0){
      ind = tranArr[0];
    }
    this.view.segLimits.selectedRowIndex = [0,ind];
  },
  /*
   * function to validate daily limts entered in limts segment
   * @return : boolean
   */
  validateDailyLimitValues: function(){
    var self =this;
    var isValid = true, segData = [];
    segData = self.view.segLimits.data;
    if(segData.length > 0){
      var count = 0;
      segData.forEach(function(rec){
        if(rec.tbxMaxDailyLimit.text.trim() === ""){
          rec.flxDailyLimit.skin = "sknFlxCalendarError";
          rec.flxErrorDailyLimit.isVisible = true;
          rec.lblErrorTextDaily.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Amount_cannot_be_empty");
          isValid = false;
        }
        else if(!self.AdminConsoleCommonUtils.isValidAmount(rec.tbxMaxDailyLimit.text.trim())){
          rec.flxDailyLimit.skin = "sknFlxCalendarError";
          rec.flxErrorDailyLimit.isVisible = true;
          rec.lblErrorTextDaily.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Please_enter_valid_amount");
          isValid = false;
        }else{
          var amount = rec.tbxMaxDailyLimit.text.trim();
          amount = +amount;
          var maxDailyLimit = rec.MaxDailyLimit;
          maxDailyLimit = +maxDailyLimit;
          if(amount>maxDailyLimit){
          rec.flxDailyLimit.skin = "sknFlxCalendarError";
          rec.flxErrorDailyLimit.isVisible = true;
          rec.lblErrorTextDaily.text ="Bank allows max of $"+ maxDailyLimit;
          isValid = false;            
          }
        }
        if(!isValid){
          self.view.segLimits.setDataAt(rec, count);
        }
        count = count+1;
      });
    }
    return isValid;
  },
  /*
   * function to validate tranfer limts entered in limts segment
   * @return : boolean
   */
  validateTransferLimitValues : function(){
    var self =this;
    var isValid = true, segData =[];
    segData = self.view.segLimits.data;
    if(segData.length > 0){
      var count = 0;
      segData.forEach(function(rec){
        if(rec.tbxTransactionLimit.text.trim() === ""){
          rec.flxTransactionLimit.skin = "sknFlxCalendarError";
          rec.flxErrorTransaction.isVisible = true;
          rec.lblErrorTextTransaction.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Amount_cannot_be_empty");
          isValid = false;
        }
        else if(!self.AdminConsoleCommonUtils.isValidAmount(rec.tbxTransactionLimit.text.trim())){
          rec.flxTransactionLimit.skin = "sknFlxCalendarError";
          rec.flxErrorTransaction.isVisible = true;
          rec.lblErrorTextTransaction.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Please_enter_valid_amount");
          isValid = false;
        }else{
          var amountTran = rec.tbxTransactionLimit.text.trim();
          amountTran = +amountTran;
          var maxTransactionLimit = rec.MaxTransferLimit;
          maxTransactionLimit = +maxTransactionLimit;
          if(amountTran>maxTransactionLimit){
          rec.flxTransactionLimit.skin = "sknFlxCalendarError";
          rec.flxErrorTransaction.isVisible = true;
          rec.lblErrorTextTransaction.text ="Bank allows max of $" + maxTransactionLimit;
          isValid = false;            
          }
        }
        if(!isValid){
          self.view.segLimits.setDataAt(rec, count);
        }
        count = count+1;
      });
    }
    return isValid;
  },
  /*
   * clears inline errors for segLimits in roles and limit screen
   */
  clearErrorForLimit: function(event){
    var self =this;
    var currRowInd = 0, currRowData;
    var selectedRow = self.view.segLimits.selectedRowIndex;
    var currTbx = event.id;
    if(selectedRow){
      currRowInd = selectedRow[1];
      currRowData = self.view.segLimits.selectedRowItems[0];
      if(currTbx === "tbxMaxDailyLimit" && currRowData.flxDailyLimit.skin === "sknFlxCalendarError"){
        currRowData.flxDailyLimit.skin = "sknflxffffffop100dbdbe6Radius3px";
        currRowData.flxErrorDailyLimit.isVisible = false;
        self.view.segLimits.setDataAt(currRowData, currRowInd);
      }
      else if(currTbx === "tbxTransactionLimit" && currRowData.flxTransactionLimit.skin === "sknFlxCalendarError"){
        currRowData.flxTransactionLimit.skin = "sknflxffffffop100dbdbe6Radius3px";
        currRowData.flxErrorTransaction.isVisible = false;
        self.view.segLimits.setDataAt(currRowData, currRowInd);
      }
    }
    self.view.forceLayout();
  },
  /*
   * forming request payload for creating new  customer
   */
  createCustomerRequest : function(){
    var self = this;
    var selRoleId = self.view.lstBoxSelectRole.selectedKey;
    var segLimitData = self.view.segLimits.data || [];
    var serviceLimits = [],accounts =[];
    serviceLimits = segLimitData.map(function(rec){
      return {"Service_id":rec.id,
              "MaxDailyLimit":rec.tbxMaxDailyLimit.text,
              "MaxTransactionLimit":rec.tbxTransactionLimit.text};
    });
    accounts = (self.view.addAndRemoveAccounts.segSelectedOptions.data).map(function(rec){
           return {
             "id":rec.accountId,
             "Account_Name":rec.name
           };
    });
    var selDate = self.view.customCalCustomerDOB.value;
    var formatDate = selDate.split("/");
    var dob = formatDate[2] +"-"+formatDate[0]+"-"+formatDate[1];
    var inputParam = {
                      "FirstName": self.view.textBoxEntry11.tbxEnterValue.text,
                      "LastName":self.view.textBoxEntry13.tbxEnterValue.text,
                      "MiddleName": self.view.textBoxEntry12.tbxEnterValue.text,
                      "UserName": self.view.textBoxEntry21.tbxEnterValue.text,
                      "Email":self.view.textBoxEntry22.tbxEnterValue.text,
                      "Phone":self.view.textBoxEntry23.txtISDCode.text +"-"+self.view.textBoxEntry23.txtContactNumber.text,
                      "DateOfBirth":dob,
                      "Ssn":self.view.textBoxEntry32.tbxEnterValue.text,
                      "DrivingLicenseNumber":self.view.textBoxEntry33.tbxEnterValue.text,
                      "accounts": accounts,
                      "Role_id":selRoleId,
                      "services": serviceLimits,
                     };
    if(self.action === self.actionConfig.create){
      inputParam.Type_id = self.comapnyDetails.type;
      inputParam.Organization_id = self.comapnyDetails.companyID;
      self.presenter.createCustomer(inputParam,inputParam.Organization_id);
    }else{
      inputParam.id = self.customerDetails.id;
      var listOfRemovedRisks = [],
            listOfAddedRisks = [],
            selectedFlags = [];
      inputParam.RiskStatus={ListRemovedRisk:[],ListAddedRisk:[]};
      if(this.view.flxEagreementDetails.isVisible)
      	inputParam.isEagreementSigned=this.view.switchEagreement.selectedIndex===1?"false":"true";
      for (var i = 1; i <= 3; i++) {
          if (self.view["imgFlag" + i].src === self.AdminConsoleCommonUtils.checkboxSelected) {
            selectedFlags.push(self.view["imgFlag" + i].info.Key);
          }
        }
        var initialFlagList = self.view.flxSelectFlags.info.initialFlagList;

      var calculateList = function (a1, a2) {
        return a1.filter(function (x) {
          var result = false;
          if (a2.indexOf(x) < 0) result = true;
          return result;
        });
      };

      listOfAddedRisks = calculateList(selectedFlags, initialFlagList);
      listOfRemovedRisks = calculateList(initialFlagList, selectedFlags);
      inputParam.RiskStatus.ListRemovedRisk=listOfRemovedRisks;
      inputParam.RiskStatus.ListAddedRisk=listOfAddedRisks;
      inputParam.RiskStatus = JSON.stringify(inputParam.RiskStatus);
      var orgId = self.customerDetails.company.id;
      self.presenter.editCustomer(inputParam,orgId);
    }
  },
  /*
   * show the changes in accounts screen based on type
   */
  adjustAccountsUIBasedOnType : function(type){
    var self =this;
    if(type === self.typeConfig.smallBusiness){
      self.view.flxHeading.setVisibility(false);
      self.view.flxAccountsAddAndRemove.top = "0dp";
      self.view.addAndRemoveAccounts.flxFilteredSearch.setVisibility(true);
      self.view.addAndRemoveAccounts.flxSearchContainer.setVisibility(false);
      self.view.addAndRemoveAccounts.tbxSearchBox.placeholder = kony.i18n.getLocalizedString("i18n.frmCompanies.Search_by_AccountNo_TIN_MembershipID");
    } else{
      self.view.flxHeading.setVisibility(true);
      self.view.flxAccountsAddAndRemove.top = "40dp";
      self.view.addAndRemoveAccounts.flxFilteredSearch.setVisibility(false);
      self.view.addAndRemoveAccounts.flxSearchContainer.setVisibility(true);
      self.view.addAndRemoveAccounts.tbxSearchBox.placeholder = kony.i18n.getLocalizedString("i18n.frmCompanies.Search_accounts_by");
    }
    self.view.forceLayout();
  },
  /*
   * call to verify if username is availble and OFAC check
   */
  checkUsernameAvailability : function(){
    var self = this;
    var usernameParam = "",ofacParam = "";
    var usernameValid = self.userNameValidation();
    if(usernameValid){
      usernameParam = {"UserName" :self.view.textBoxEntry21.tbxEnterValue.text};
      ofacParam = self.OFACverification();
      self.presenter.verifyUsername(usernameParam,ofacParam);
    }
  },
  /*
   * display if username can be used or not
   */
  displayUsernameAvailability : function(response){
    var self = this;
    if(response.userAvailable){
      self.view.textBoxEntry21.flxInlineError.setVisibility(false);
      self.view.lblUsernameCheck.setVisibility(true);
    }else{
      self.view.lblUsernameCheck.setVisibility(false);
      self.view.textBoxEntry21.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmUsersController.Username_Not_Available");
      self.view.textBoxEntry21.flxInlineError.setVisibility(true);
    }
    self.view.forceLayout();
  },
  /*
   * call for OFAC verification
   * @return : request payload
   */
  OFACverification : function(){
    var self = this;
    var ssn = self.view.textBoxEntry32.tbxEnterValue.text;
    var dobValue = self.view.customCalCustomerDOB.value;
    var dob = dobValue.replace(/\//g,"-");  //mm/dd/yyyy to mm-dd-yyyy
    return {"DateOfBirth":dob,
            "Ssn":ssn};
  },
  /*
   * display the selected type for search
   */
  dispalySelectedType : function(){
    var self = this;
    var selType ="",curType = "",ind = "";
    var selRowData = self.view.addAndRemoveAccounts.segSearchType.selectedRowItems[0];
    ind = self.view.addAndRemoveAccounts.segSearchType.selectedRowIndex[1];
    selType = selRowData.lblValue;
    curType = self.view.addAndRemoveAccounts.lblSelSearchType.text;
    selRowData.lblValue = curType;
    self.view.addAndRemoveAccounts.segSearchType.setDataAt(selRowData, ind);
    self.view.addAndRemoveAccounts.lblSelSearchType.text = selType;
    self.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(false);
    self.view.forceLayout();
  },
  onClickOfNextFromDetails : function(OFACResponse){
    var self = this;
    var isValid = OFACResponse.canProceed;
      if(isValid && (self.tabClick === 2)) {
        self.view.flxOFACError.setVisibility(false);
        self.showAccountsScreen();
      } else if(isValid && (self.tabClick === 3)){
        self.view.flxOFACError.setVisibility(false);
        if (self.action === self.actionConfig.edit) {
          if (self.view.verticalTabsCustomer.flxOption3.info.isFirstTime) {
            self.getCustomerLimits();
          } else {
            self.setRolesLimitsForEdit();
          }
        }
        self.showLimitsScreen();
      } else if(!isValid){
        self.view.flxOFACError.setVisibility(true);
        self.view.alertMessage.skin = "sknRedBorder";
        self.view.alertMessage.flxLeftImage.skin = "sknRedFill";
        self.view.alertMessage.lblData.text = kony.i18n.getLocalizedString("i18n.frmCompanies.errorToast_verify_OFAC_fail");
      }
  },
 /*
  * fill data for edit customer screen
  */
  setCustomerDetailsForEdit : function(data){
    var self =this;
    self.editCustomerID = data.id;
    var dob = (data.DateOfBirth).split("-");
    var dateOfBirth = dob[1]+"/"+dob[2]+"/"+dob[0];
    var phone = data.Phone;
    var isd = "",phn ="";
    if(phone.indexOf("-")>= 0){
      phn = phone.split("-")[1];
      isd = phone.split("-")[0];
    } else {
      phn = phone;
    }
    //self.fillSalutation();
    self.fillCustomerFlags();
    //self.view.lstBxSalutation.selectedKey=data.Salutation?data.Salutation:"lbl1";
    self.view.textBoxEntry21.tbxEnterValue.setEnabled(false);
    self.view.textBoxEntry32.tbxEnterValue.setEnabled(false);
    self.view.textBoxEntry21.btnCheck.setEnabled(false);
    self.view.textBoxEntry11.tbxEnterValue.text = data.FirstName;
    self.view.textBoxEntry12.tbxEnterValue.text = data.MiddleName;
    self.view.textBoxEntry13.tbxEnterValue.text = data.LastName;
    self.view.textBoxEntry21.tbxEnterValue.text = data.UserName;
    self.view.textBoxEntry22.tbxEnterValue.text = data.Email;
    self.view.textBoxEntry32.tbxEnterValue.text = data.Ssn;
    self.view.textBoxEntry33.tbxEnterValue.text = data.DrivingLicenseNumber;
    self.view.customCalCustomerDOB.value = dateOfBirth;
    self.view.customCalCustomerDOB.resetData = self.getLocaleDate(dateOfBirth);
    self.view.textBoxEntry23.txtISDCode.text = isd;
    self.view.textBoxEntry23.txtContactNumber.text = phn;
    var initialFlagList = [];
    if (data.customerFlags) {
      for (var i = 1; i <= 3; i++) {
        for (var j = 0; j < data.customerFlags.length; j++) {
          if (self.view["imgFlag" + i].info.Key === data.customerFlags[j].trim()) {
            self.view["imgFlag" + i].src = self.AdminConsoleCommonUtils.checkboxSelected;
            initialFlagList.push(self.view["imgFlag" + i].info.Key);
          }
        }
      }
    }
    self.view.flxSelectFlags.info = {
      "initialFlagList": initialFlagList
    };
    if (data.isEAgreementRequired === "0") {
      self.view.flxEagreementDetails.setVisibility(false);
    } else {
      self.view.flxEagreementDetails.setVisibility(true);
    }
    // Set e-agreement status
    if (data.isEagreementSigned === "false") {
      self.view.switchEagreement.selectedIndex = 1;
    } else {
      self.view.switchEagreement.selectedIndex = 0;
    }
  },
  /*
   * call to fetch customer role and limits for editing
   */
  getCustomerLimits : function(){
    var self =this;
    var param = {"Username" : self.customerDetails.UserName};
    self.presenter.getCustomerRoleLimit(param);
  },
  /*
   * set roles and limits assigned to customer
   */
  setRolesLimitsForEdit : function(){
    var self = this;
    kony.adminConsole.utils.showProgressBar(self.view);
    var data = [];
    data = self.view.segLimits.info.customerLimits;
    var role = data.Group_id;
    self.view.lstBoxSelectRole.selectedKey = role;
    var limitsData = data.services.map(function(rec){
      var maxTranfer = self.getMaxLimitValuesToValidate(rec.Id,"MaxTransferLimit");
      var maxDaily = self.getMaxLimitValuesToValidate(rec.Id,"MaxDailyLimit");
      return {
        "id": rec.Id,
        "lblPermissionName": {"text":rec.Name},
        "flxDailyLimit": {"skin":"sknflxffffffop100dbdbe6Radius3px"},
        "tbxMaxDailyLimit": {"text":rec.MaxDailyLimit,
                             "placeholder":kony.i18n.getLocalizedString("i18n.frmLogs.blSearchParam"),
                             "skin":"skntbxffffffNoBorderlato35475f14px",
                             "onKeyUp": self.clearErrorForLimit},
        "flxLine": {"isVisible":true},
        "flxTransactionLimit": {"skin":"sknflxffffffop100dbdbe6Radius3px"},
        "tbxTransactionLimit": {"text":rec.MaxTransactionLimit,
                                "placeholder":kony.i18n.getLocalizedString("i18n.frmLogs.blSearchParam"),
                                "skin":"skntbxffffffNoBorderlato35475f14px",
                                "onKeyUp": self.clearErrorForLimit},
        "flxLineVertical": {"isVisible":true},
        "lblCurrencyValue": {"isVisible":true},
        "lblTransactionCurrency": {"isVisible":true},
        "flxErrorDailyLimit": {"isVisible":false},
        "flxErrorTransaction": {"isVisible":false},
        "lblErrorIconDaily": {"isVisible":true},
        "lblErrorIconTransaction": {"isVisible":true},
        "lblErrorTextDaily": {"text":"Error"},
        "lblErrorTextTransaction":{"text":"Error"},
        "template": "flxLimitsCustomer",
        "MaxTransferLimit": maxTranfer,
        "MaxDailyLimit": maxDaily,
        "MinTransferLimit": ""
      };
    });
    self.view.segLimits.setData(limitsData);
    self.view.segLimits.setVisibility(limitsData.length > 0);
    self.view.flxNoResults.setVisibility(limitsData.length <= 0);
    kony.adminConsole.utils.hideProgressBar(self.view);
    self.view.forceLayout();
  },
  /*
  * get max limits of particular permissions
  */
  getMaxLimitValuesToValidate : function(id,category){
    var self = this;
    var roleLimits = self.view.segLimits.info.roleLimits;
    var result= [],value = "";
    result = roleLimits.filter(function(rec){
      if(id === rec.Service_id){
        return rec;
      }
    });
    if(result[0]){
      if(category.toLowerCase().indexOf("transfer")>=0) value = result[0].MaxTransferLimit;
      else value = result[0].MaxDailyLimit;
    } else{
      value = "0";
    }
    return value;
  },
  /*
   * display role update popup
   */
  showPopUpOnRoleChange : function(){
    var self =this;
    var groupName = self.view.segLimits.info.customerLimits.Group_Name;
    self.view.popUpRole.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Update_role");
    self.view.popUpRole.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Are_you_sure_to_update")+
      " "+groupName+" "+kony.i18n.getLocalizedString("i18n.frmCompanies.role_type")+"\n"+
      kony.i18n.getLocalizedString("i18n.frmCompanies.Update_customer_role_type_popup_message");
  },
  /*
  * validations for username
  *@returns : true or false
  */
  userNameValidation : function(){
    var self = this;
    var isValid = true;
    if(self.action === self.actionConfig.create){
      var usernameRegexString = "^([a-zA-Z0-9sp]+)$";
      if(self.usernameRulesAndPolicy.usernamerules.symbolsAllowed) {
        var supportedSymbols = self.usernameRulesAndPolicy.usernamerules.supportedSymbols.replace(/,/g, "");
        usernameRegexString = usernameRegexString.replace(/sp/g, supportedSymbols);
      }
      else {
        usernameRegexString = usernameRegexString.replace(/sp/g, '');
      }
      var usernameRegex = new RegExp(usernameRegexString);
      if (self.view.textBoxEntry21.tbxEnterValue.text.trim() === "") {
        self.view.textBoxEntry21.flxEnterValue.skin = "sknflxEnterValueError";
        self.view.textBoxEntry21.flxInlineError.setVisibility(true);
        self.view.textBoxEntry21.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Username_cannot_be_empty");
        isValid = false;
      } else if(self.view.textBoxEntry21.tbxEnterValue.text.trim().length < self.usernameRulesAndPolicy.usernamerules.minLength){
        self.view.textBoxEntry21.flxEnterValue.skin = "sknflxEnterValueError";
        self.view.textBoxEntry21.flxInlineError.setVisibility(true);
        self.view.textBoxEntry21.lblErrorText.text = "Enter atleast " + self.usernameRulesAndPolicy.usernamerules.minLength + " characters";
        isValid = false;
      } else if(self.view.textBoxEntry21.tbxEnterValue.text.trim().length > self.usernameRulesAndPolicy.usernamerules.maxLength){
        self.view.textBoxEntry21.flxEnterValue.skin = "sknflxEnterValueError";
        self.view.textBoxEntry21.flxInlineError.setVisibility(true);
        self.view.textBoxEntry21.lblErrorText.text = "Enter max of " + self.usernameRulesAndPolicy.usernamerules.maxLength + " characters only";
        isValid = false;
      } else if(usernameRegex.test(self.view.textBoxEntry21.tbxEnterValue.text.trim()) === false){
        self.view.textBoxEntry21.flxEnterValue.skin = "sknflxEnterValueError";
        self.view.textBoxEntry21.flxInlineError.setVisibility(true);
        if(self.usernameRulesAndPolicy.usernamerules.symbolsAllowed) {
          self.view.textBoxEntry21.lblErrorText.text = "Only following special characters allowed: " + self.usernameRulesAndPolicy.usernamerules.supportedSymbols;
        }
        else {
          self.view.textBoxEntry21.lblErrorText.text = "No special characters allowed";
        }
        isValid = false;
      }
    }
    return isValid;
  },
  
  setUsernameRulesAndPolicy : function(usernameRulesAndPolicy) {
    var scopeObj = this;
    
    scopeObj.usernameRulesAndPolicy.usernamerules = usernameRulesAndPolicy.usernamerules;
    for(var i=0; i<usernameRulesAndPolicy.usernamepolicy.length; ++i) {
      if(usernameRulesAndPolicy.usernamepolicy[i].locale === "en-US") {
        scopeObj.usernameRulesAndPolicy.usernamepolicy = usernameRulesAndPolicy.usernamepolicy[i].content;
        break;
      }
    }
  },
    
  rulesLabelClick : function() {
    var scopeObj = this;
    
    if(this.view.flxUsernameRules.isVisible === false) {
      scopeObj.view.flxUsernameRules.setVisibility(true);
      scopeObj.view.rtxUsernameRules.text = scopeObj.usernameRulesAndPolicy.usernamepolicy;
    }
    else {
      scopeObj.view.flxUsernameRules.setVisibility(false);
    }
    
    this.view.forceLayout();
  },
  /*
   * function to display salutation and risk flags while editing customer
   */
  showCreateEditSpecificUI : function(){
    var self = this;
    if(self.action === self.actionConfig.create){
      self.view.flxSalutation.setVisibility(false);
      self.view.flxRow4.setVisibility(false);
    }else if(self.action === self.actionConfig.edit){
      //self.view.flxSalutation.setVisibility(true);
      self.view.flxRow4.setVisibility(true);
    }
    self.view.forceLayout();
  },
  /*
   * function to check/uncheck risk flags checkboxes
   * @param : image widget path
   */
  selectRiskFlag : function(widget){
    var self = this;
    if(widget.src === self.AdminConsoleCommonUtils.checkbox){
      widget.src = self.AdminConsoleCommonUtils.checkboxSelected;
    }else if(widget.src === self.AdminConsoleCommonUtils.checkboxSelected){
       widget.src = self.AdminConsoleCommonUtils.checkbox;
    }
    self.view.forceLayout();
  }
});
