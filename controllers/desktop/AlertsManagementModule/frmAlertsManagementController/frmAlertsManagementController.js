define({

  tab1: {},
  tab2: {},
  tab3: {},
  tab4: {},
  tabData: {},
  tab: [],
  defaultChannelList : ["SMS","Email","Push","Secure Message"],
  selectedChannelList : [],
  globalVR: [],
  recentTabData: {
    "header": {},
    "data": {},
    "tabName": ""
  },
  alertGroupActionConfig :{
    CREATE:"CREATE",
    EDIT:"EDIT"
  },
  reorderAlert: false,
  activateDeactivate: "",
  alertGroupScreenContext : 1,
  alertGroupCurrAction :"",
  reorderAlertIndex: 0,
  recentButton: {},
  recentButtonIndex: 0,
  radioNotSelected: "radio_notselected.png",
  radioSelected: "radio_selected.png",
  checkboxselected : "checkboxselected.png",
  checkboxnormal : "checkboxnormal.png",
  checkbox :"checkbox.png",
  sPermDeactivate : kony.i18n.getLocalizedString("i18n.permission.Deactivate"),
  sPermActivate: kony.i18n.getLocalizedString("i18n.permission.Activate"),
  sDeactivate : kony.i18n.getLocalizedString("i18n.frmSecurityQuestionsController.deactivate"),
  sActivate : kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.activate"),
  sSubscription : kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.subscription"),
  sDeactivateSubStatus : kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Deactive_subscription_status"),
  sDeactivateAlertStatus : kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Deactivate_alert_status"),
  sActiveSubStatus : kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Active_subscription_status"),
  sActivateAlertStatus : kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Activate_alert_status"),
  sNoLeaveAsIs : kony.i18n.getLocalizedString("i18n.frmAlertsManagement.NO_LEAVE_IT_AS_IT_IS"),
  sWriteDesc : kony.i18n.getLocalizedString("i18n.frmAlertsManagement.WriteDescription"),
  sYesActivate : kony.i18n.getLocalizedString("i18n.frmPermissionsController.YES__ACTIVATE"),
  sAlerts : kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Alerts"),
  sDeactivateRecord : kony.i18n.getLocalizedString("i18n.frmCustomerCareController.Deactivate_Record"),
  sActive : kony.i18n.getLocalizedString("i18n.secureimage.Active"),
  sInActive : kony.i18n.getLocalizedString("i18n.secureimage.Inactive"),
  sYesDeactivate : kony.i18n.getLocalizedString("i18n.PopUp.YesDeactivate"),
  sSuccess : kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"),
  sUpdateCaps: kony.i18n.getLocalizedString("i18n.frmAlertsManagement.UpdataCaps"),
  sAddName: kony.i18n.getLocalizedString("i18n.frmAlertsManagement.AddName"),
  sLang: kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Language"),
  sAlertContentTemplate: kony.i18n.getLocalizedString("i18n.frmAlertsManagement.AlertContentTemplate"),
  sNA : kony.i18n.getLocalizedString("i18n.frmAlertsManagement.NA"),
  sAll : kony.i18n.getLocalizedString("i18n.frmAlertsManagement.All"),
  sSelected: kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Selected"),
  sAlertCategoryInactive: kony.i18n.getLocalizedString("i18n.frmAlertsManagement.AlertCategoryInactive"),
  sAlertGroupInactive: kony.i18n.getLocalizedString("i18n.frmAlertsManagement.AlertGroupInactive"),
  selectedAlertId: null,
  cursorPoint:null,
  emailRtxData:null,
  supportedChannels:[],
  filterIndiciesArray: {
    "channel": [0, 1, 2],
    "status": [0, 1]
  },
  localeLanguages :[],
  searchFlag: false,
  searchResentTabData: [],
  willUpdateUI: function(viewModel) {
    this.updateLeftMenu(viewModel);
    if(!viewModel)
      return;
    if (viewModel.type) {
      this.assingTabValues(viewModel, viewModel.type);
    }
    if (viewModel.reset) {
      this.resetUI();
    }
    if (viewModel.categoryId) {
      this.tabData = viewModel.records;
      this.changeActiveButtonSkin(this.recentButton,this.tab[this.recentButtonIndex], "tab"+(this.recentButtonIndex+1),this.tabData);
      this.showAlertCategoryDetailScreen();
    }
    if(viewModel.action === "masterData"){
      this.setMasterData(viewModel);
    } else if(viewModel.subAlertDetails){
      this.setSubAlertsView(viewModel.subAlertDetails);
    } else if(viewModel.alertDetails && viewModel.action === "viewDetails"){
      this.setDataToAlertTypeDetailsScreen(viewModel.alertDetails);
      this.alertsJSON=viewModel.alertDetails.alertSubTypes;
      this.setSubAlertSegData();
      this.showAlertGroupDetailScreen();
      kony.adminConsole.utils.hideProgressBar(this.view);
    } else if(viewModel.alertDetails && viewModel.action === "edit"){
      this.showAddAlertGroupScreen();
      this.view.flxAlertTypesContextualMenu.setVisibility(false);
      this.fillAlertGroupScreenForEdit(viewModel.alertDetails);
    }else if(viewModel.variableRef){
      this.setVariableReferenceSegmentData(viewModel.variableRef.alertContentFields);
    }else if(viewModel.alertCodes){
      this.showAddSubAlert(viewModel.alertCodes.eventSubTypes);
    }else if(viewModel.alertDetails && viewModel.action === "statusChange"){
      this.activateDeactivateAlertGroup(viewModel.alertDetails);
      kony.adminConsole.utils.hideProgressBar(this.view);
    } else if(viewModel.progressBar){
      if (viewModel.progressBar.show) {
        kony.adminConsole.utils.showProgressBar(this.view);
      } else {
        kony.adminConsole.utils.hideProgressBar(this.view);
      }
    } else if(viewModel.toast){
      if(viewModel.toast.status === kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success")){
        this.view.toastMessage.showToastMessage(viewModel.toast.message,this);
      }else if(viewModel.toast.status === kony.i18n.getLocalizedString("i18n.frmGroupsController.error")){
        this.view.toastMessage.showErrorToastMessage (viewModel.toast.message,this);
      }
    }
  },
  setMasterData: function(viewModel){
    if(viewModel.appsList){
        this.setAppsUsersData(viewModel.appsList, "apps");
      }
      if(viewModel.usersList){
        this.setAppsUsersData(viewModel.usersList, "users");
      }
      if(viewModel.languagesList){
        this.setLanguages(viewModel.languagesList);
        this.localeLanguages = viewModel.languagesList;
      }
      if(viewModel.attributesList){
        this.setAttributes(viewModel.attributesList.alertAttributes);
      }
      if(viewModel.criteriaList){
        this.setCriteria(viewModel.criteriaList.alertConditions);
      }
      if(viewModel.alertGroupCodes){
        this.setAlertGroupCodes(viewModel.alertGroupCodes);
      }
  },
  formPreshow: function() {
    this.view.mainHeader.lblHeading.toolTip = "Alerts";
    this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.AlertsDefinition");
    this.view.subHeader.tbxSearchBox.placeholder = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Search_by_name");
    this.view.subHeader.tbxSearchBox.text = "";
    this.view.subHeader.flxClearSearchImage.setVisibility(false);
    this.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
    this.view.subHeader.flxSearchContainer.centerY = "50%";
    this.view.breadcrumbs.btnBackToMain.setVisibility(true);
    this.view.breadcrumbs.fontIconBreadcrumbsRight.setVisibility(false);
    this.view.breadcrumbs.btnPreviousPage.setVisibility(false);
    this.view.breadcrumbs.fontIconBreadcrumbsRight2.setVisibility(false);
    this.view.breadcrumbs.lblCurrentScreen.setVisibility(false);
    this.view.commonButtons.btnNext.setVisibility(false);
    this.view.contextualMenu1.lblHeader.setVisibility(false);
    this.view.contextualMenu1.btnLink1.setVisibility(false);
    this.view.contextualMenu1.btnLink2.setVisibility(false);
    this.view.contextualMenu1.flxOptionsSeperator.setVisibility(false);
    this.view.contextualMenu1.flxOption1.setVisibility(false);
    this.view.contextualMenu1.flxOption3.setVisibility(false);
    this.view.flxPreviewPopup.setVisibility(false);
    this.view.flxSubAlertCodeGrey.setVisibility(false);
    this.view.flxAddSubAlertPopUp.setVisibility(false);
    this.view.flxToastMessage.setVisibility(false);
    this.view.contextualMenu1.lblIconOption2.text = "\ue91e";
    this.view.lblIconOption2.text = "\ue91c";
    this.view.lblIconOption3.text = "\ue91b";
    this.view.contextualMenuAlertTypeDetail.lblIconOption2.text = "\ue91e";
    this.recentButton = this.view.btnTabName1;
    this.view.breadcrumbAlerts.skin = "sknFlxFFFFFF100O";
    this.setFlexHeight();
    this.setFlowActions();
    this.getMasterData();
    this.resetUI();
  },
  resetUI: function() {
    var scopeObj = this;
    scopeObj.view.statusFilterMenu.setVisibility(false);
    scopeObj.view.flxAlertsBreadCrumb.setVisibility(false);
    scopeObj.view.flxEditAlert.setVisibility(false);
    scopeObj.view.flxEditAlertMain.setVisibility(false);
    scopeObj.view.flxEditAlertInner.setVisibility(false);
    scopeObj.view.flxViewAlertData.setVisibility(false);
    scopeObj.view.flxAlertsMain.setVisibility(true);
    scopeObj.view.flxEditAlertCategoryScreen.setVisibility(false);
    scopeObj.view.flxAlertCategories.setVisibility(true);
    scopeObj.view.flxAlertTypeDetailsScreen.setVisibility(false);
    scopeObj.view.flxAlertTypes.setVisibility(false);
    scopeObj.view.flxSubAlerts.setVisibility(false);
    scopeObj.view.flxBreadcrumbAlerts.setVisibility(false);
    scopeObj.view.flxAlertCategoryDetailsScreen.setVisibility(true);
    scopeObj.view.statusFilterMenu.setVisibility(false);
    scopeObj.view.flxCategoryOptionsMenu.setVisibility(false);
    scopeObj.view.flxContextualMenu.setVisibility(false);
  },
  setFlexHeight: function() {
    var screenHeight = kony.os.deviceInfo().screenHeight;
    this.view.flxAlertConfiguration.height = screenHeight - 106 + "px";
    this.view.flxAlertsMain.height = this.view.flxMain.height;
    this.view.flxAlertTypeDetailsScreen.height = screenHeight - 126 - 90 - 10 + "px";
    this.view.flxAddAlertTypeScreen.height = screenHeight - 126 - 90 - 10 + "px";
    this.view.flxAlertBoxContainer.height=kony.os.deviceInfo().screenHeight-120+"px";
    this.view.flxSubAlerts.height=kony.os.deviceInfo().screenHeight-250+"px";
    this.view.flxViewSubAlert.height=kony.os.deviceInfo().screenHeight-250+"px";
    this.view.flxAlertCategoryDetailsScreen.height = screenHeight - 106 - 90 + "px";    
    this.view.flxEditAlertCategoryScreen.height = screenHeight - 126 - 90 - 10 + "px";
    this.view.forceLayout();
  },
  setSkinForChannelTabs: function (btnWidget) {
    this.view.btnSMS.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
    this.view.btnPushNoti.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
    this.view.btnEmail.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
    this.view.btnNotifCenter.skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
    btnWidget.skin = "sknbtnBgffffffLato485c75Radius3Px12Px";
  },
  onHoverEventCallback:function(widget, context) {
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER||context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      widget.setVisibility(true);
    } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      widget.setVisibility(false);
    }
  },
  showSwitchTogglePopup: function(status, type) {
    var scopeObj = this;
    var lblPopUpMainMessage;
    var rtxPopUpDisclaimer;
    var btnPopUpCancelRight;
    var btnPopUpDeleteText;

    if (status === scopeObj.sDeactivate) {
      if (type === scopeObj.sSubscription) {
        lblPopUpMainMessage = scopeObj.sDeactivateSubStatus;
        rtxPopUpDisclaimer = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.deactiveSubscriptionMessage");
        this.view.popUpDeactivate.btnPopUpCancel.text = scopeObj.sNoLeaveAsIs;
        btnPopUpCancelRight = "180px";
        btnPopUpDeleteText = scopeObj.sYesDeactivate;
      } else {
        lblPopUpMainMessage = scopeObj.sDeactivateAlertStatus;
        rtxPopUpDisclaimer = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.DeactivateAlertStatusMessage");
        this.view.popUpDeactivate.btnPopUpCancel.text = scopeObj.sNoLeaveAsIs;
        btnPopUpCancelRight = "180px";
        btnPopUpDeleteText = scopeObj.sYesDeactivate;
      }
    } else if (status === scopeObj.sActivate) {
      if (type === scopeObj.sSubscription) {
        lblPopUpMainMessage = scopeObj.sActiveSubStatus;
        rtxPopUpDisclaimer = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Active_subscription_status_Message");
        this.view.popUpDeactivate.btnPopUpCancel.text = scopeObj.sNoLeaveAsIs;
        btnPopUpCancelRight = "162px";
        btnPopUpDeleteText = scopeObj.sYesActivate;
      } else {
        lblPopUpMainMessage = scopeObj.sActivateAlertStatus;
        rtxPopUpDisclaimer = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Active_alert_status_Message");
        this.view.popUpDeactivate.btnPopUpCancel.text = scopeObj.sNoLeaveAsIs;
        btnPopUpCancelRight = "162px";
        btnPopUpDeleteText = scopeObj.sYesActivate;
      }
    }
    this.view.popUpDeactivate.lblPopUpMainMessage.text = lblPopUpMainMessage;
    this.view.popUpDeactivate.rtxPopUpDisclaimer.text = rtxPopUpDisclaimer;
    this.view.popUpDeactivate.btnPopUpCancel.text = scopeObj.sNoLeaveAsIs;
    this.view.popUpDeactivate.btnPopUpCancel.right = btnPopUpCancelRight;
    this.view.popUpDeactivate.btnPopUpDelete.text = btnPopUpDeleteText;
    this.view.flxDeleteAlert.setVisibility(true);
  },
  setOnClick : function(channelToAdd) {
    var scopeObj = this;

    channelToAdd.flxcbSupportedChannel.onClick = function() {
      if(channelToAdd.imgcbSupportedChannel.src === scopeObj.checkboxnormal) {
        channelToAdd.imgcbSupportedChannel.src = scopeObj.checkboxselected;
        scopeObj.selectedChannelList.push(channelToAdd.lblSupportedChannel.text);
        scopeObj.view.flxCategoryChannelError.setVisibility(false);
      }
      else {
        channelToAdd.imgcbSupportedChannel.src = scopeObj.checkboxnormal;
        var selectedChannelindex = scopeObj.selectedChannelList.indexOf(channelToAdd.lblSupportedChannel.text);
        if (selectedChannelindex > -1) {
          scopeObj.selectedChannelList.splice(selectedChannelindex, 1);
        }
      }
    };
  },
  setSupportedChannels: function(channels) {
    var scopeObj = this;
    scopeObj.selectedChannelList = scopeObj.supportedChannels;
    scopeObj.view.flxSupportedChannelEntries.removeAll();
    if (channels) {
      for (var i=0; i<channels.length; ++i) {
        var channelToAdd = new com.adminConsole.alerts.supportedChannel({
          "autogrowMode" : kony.flex.AUTOGROW_NONE,
          "clipBounds" : true,
          "id" : "channel" + i,
          "isVisible" : true,
          "layoutType" : kony.flex.FREE_FORM,
          "masterType" : constants.MASTER_TYPE_DEFAULT,
          "width": ((channels[i].channelDisplayName.length * 6) + 50)+"px",
        }, {}, {});
        channelToAdd.lblSupportedChannel.text = channels[i].channelDisplayName;
        if(channels[i].isChannelSupported === "true")
        {
          channelToAdd.imgcbSupportedChannel.src = scopeObj.checkboxselected;
        }else {
          channelToAdd.imgcbSupportedChannel.src = scopeObj.checkboxnormal;
        }
        scopeObj.setOnClick(channelToAdd);
        scopeObj.view.flxSupportedChannelEntries.add(channelToAdd);
      }
    }
    this.view.forceLayout();
  },

  setEditAlertCategoryData: function(){
    var scopeObj = this;
    scopeObj.view.EditAlertCategoryButtons.btnSave.text = scopeObj.sUpdateCaps;
    scopeObj.view.EditAlertCategoryButtons.flxRightButtons.width = "100dp";
    scopeObj.view.lblEditAlertCategoryName.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Edit") +" " + scopeObj.view.lblViewDetailsCategoryDisplayName.text;

    var accLevelAlerts = scopeObj.tabData.categoryDefintion.containsAccountLevelAlerts;
    var data = [{"selectedImg":scopeObj.radioSelected,"unselectedImg":scopeObj.radioNotSelected,
                 "src":(accLevelAlerts === "true" ? scopeObj.radioSelected : scopeObj.radioNotSelected),"value":"Yes","id":"Yes"},
                {"selectedImg":scopeObj.radioSelected,"unselectedImg":scopeObj.radioNotSelected,
                 "src":(accLevelAlerts === "false" ? scopeObj.radioSelected : scopeObj.radioNotSelected),"value":"No","id":"No"}];
    scopeObj.view.customCategoryRadioButtonType.setData(data);
    scopeObj.view.editAlertCategoryStatusSwitch.switchToggle.selectedIndex = 0;
    scopeObj.setSupportedChannels(scopeObj.tabData.categoryChannels);
    var langPref = scopeObj.tabData.displayPreferences;
    scopeObj.assignCategoryLanguagesSupported(langPref);
  },
  getCategoryLangSegDataMapJson : function(){
    var self = this;
    var currLocale = (kony.i18n.getCurrentLocale()).replace("_","-");
    var getName = self.getLanguageNameForCode(currLocale);
    return {
      "lblCategoryLanguage":{"text":getName,
                             "info":{"id":currLocale},
                             "isVisible":true,
                             "onClick":self.updateCategoryLanguagesBasedOnSelection},
      "lstBoxSelectLanguage":{"isVisible":false,
                              "onSelection":self.hideCategoryLangListBox,
                              "masterData":self.langToShow,
                              "selectedKey":currLocale,
                              "onHover":self.hideCategoryListBoxOnHover},
      "lblCategoryDisplayName":{"text":self.sAddName,
                                "isVisible":true,
                                "onClick":self.showCategoryLangTextBox},
      "tbxDisplayName":{"text":"",
                        "isVisible":false,
                        "onHover":self.onHoverHideCategoryTextBoxCallback,
                        "onKeyUp":self.addNewCategoryLangRowAtEnd},
      "lblCategoryDescription":{"text":self.sWriteDesc,
                                "isVisible":true,
                                "onClick":self.showCategoryLangTextBox},
      "lblIconDeleteLang":{"text":"\ue91b","skin":"sknIcon20px"},
      "flxDescription": {"isVisible" : false},
      "lblDescCount": {"isVisible":false},
      "tbxDescription":{"text":"",
                        //"onKeyUp" : self.onKeyUpCallBackSegLang,
                        "onHover":self.onHoverHideCategoryTextBoxCallback,
                        "onKeyUp":self.addNewCategoryLangRowAtEnd},
      "lblSeprator":"-",
      "flxCategoryLangDelete":{"isVisible":false, "onClick" : self.showCategoryDeleteLangPopup},
      "flxLanguageRowContainer":{"onHover": self.onHoverDisplayCategoryDelete},
      "template":"flxAlertsCategoryLanguageData"
    };
  },
  assignCategoryLanguagesSupported : function(displayPref){
    var self =this;
    var mapData =[];
    mapData = displayPref.map(function(rec){
      var rowData = self.getCategoryLangSegDataMapJson();
      rowData.lblCategoryLanguage.text = self.getLanguageNameForCode(rec.LanguageCode) || self.lang;
      rowData.lblCategoryLanguage.info.id = rec.LanguageCode;
      rowData.lstBoxSelectLanguage.selectedKey = rec.LanguageCode;
      rowData.lblCategoryDisplayName.text = rec.DisplayName;
      rowData.tbxDisplayName.text = rec.DisplayName;
      rowData.lblCategoryDescription.text = rec.Description;
      rowData.tbxDescription.text = rec.Description;
      return rowData;
    });
    self.view.segEditAlertCategoryLanguages.setData(mapData);
    self.view.segEditAlertCategoryLanguages.info = {"segData" : displayPref};
    if(mapData.length <= 0){
      self.addRowDataForCategoryLangSeg();
    } else{
      self.view.segEditAlertCategoryLanguages.selectedRowIndex = [0,mapData.length-1];
      self.addNewCategoryLangRowAtEnd();
    }
    self.view.forceLayout();
  },
  addRowDataForCategoryLangSeg : function(){
    var self = this;

    self.view.segEditAlertCategoryLanguages.setData([]);
    var widgetDataMap = {
      "lblCategoryLanguage":"lblCategoryLanguage",
      "lstBoxSelectLanguage":"lstBoxSelectLanguage",
      "lblCategoryDisplayName":"lblCategoryDisplayName",
      "tbxDisplayName":"tbxDisplayName",
      "lblCategoryDescription":"lblCategoryDescription",
      "tbxDescription":"tbxDescription",
      "lblSeprator":"lblSeprator",
      "flxDescription":"flxDescription",
      "lblDescCount":"lblDescCount",
      "flxCategoryLangDelete":"flxCategoryLangDelete",
      "lblIconDeleteLang":"lblIconDeleteLang",
      "flxLanguageRowContainer":"flxLanguageRowContainer",
      "flxAlertsCategoryLanguageData":"flxAlertsCategoryLanguageData"
    };
    self.view.segEditAlertCategoryLanguages.widgetDataMap = widgetDataMap;

    var segData = self.getCategoryLangSegDataMapJson();

    self.view.segEditAlertCategoryLanguages.setData([segData]);
    self.view.forceLayout();
  },
  /*
   * callback function to display languages listbox
   */
  showCategoryLangListBox :  function(event){
    var self = this;
    var ind = self.view.segEditAlertCategoryLanguages.selectedRowIndex[1];
    var rowData = self.view.segEditAlertCategoryLanguages.data[ind];
    rowData.lblCategoryLanguage.isVisible = false;
    rowData.lstBoxSelectLanguage.isVisible = true;
    self.view.segEditAlertCategoryLanguages.setDataAt(rowData, ind);
    self.view.forceLayout();
  },
  /*
   * hide the listbox on selection in languages segment
   */
  hideCategoryLangListBox : function(){
    var self = this;
    var ind = self.view.segEditAlertCategoryLanguages.selectedRowIndex[1];
    var rowData = self.view.segEditAlertCategoryLanguages.data[ind];
    var selectedValue = rowData.lstBoxSelectLanguage.selectedKeyValue;
    if(selectedValue[0] !== "select"){
      rowData.lblCategoryLanguage.text = selectedValue[1];
      rowData.lblCategoryLanguage.info.id = selectedValue[0];
      rowData.lblCategoryLanguage.isVisible = true;
      rowData.lstBoxSelectLanguage.isVisible = false;
      self.view.segEditAlertCategoryLanguages.setDataAt(rowData, ind);
    }
    self.view.forceLayout();
  },
  /*
   * callback function to display textbox on click of label
   */
  showCategoryLangTextBox : function(event){
    var self = this;
    var currWidgetId = event.id;
    var ind = self.view.segEditAlertCategoryLanguages.selectedRowIndex[1];
    var rowData = self.view.segEditAlertCategoryLanguages.data[ind];
    if(currWidgetId === "lblCategoryDisplayName"){
      rowData.tbxDisplayName.isVisible = true;
      rowData.lblCategoryDisplayName.isVisible = false;
    }else{
      rowData.flxDescription.isVisible = true;
      rowData.lblCategoryDescription.isVisible = false;
    }
    self.view.segEditAlertCategoryLanguages.setDataAt(rowData, ind);
    self.view.forceLayout();
  },
  /*
   * function to update languages listbox with the unselected languages list only
   */
  updateCategoryLanguagesBasedOnSelection: function() {
    var self = this;
    var assignedData = [], allLangId = [], diffList = [];
    var segData = self.view.segEditAlertCategoryLanguages.data;
    var currInd = self.view.segEditAlertCategoryLanguages.selectedRowIndex[1];
    var currRowData = self.view.segEditAlertCategoryLanguages.data[currInd];
    //get all assigned languges id's
    for (var i = 0; i < segData.length; i++) {
      if (segData[i].lblCategoryLanguage.info.id !== "" && i!== currInd ) assignedData.push(segData[i].lblCategoryLanguage.info.id);
    }
    //all exsisting language id's
    allLangId = self.langToShow.map(function(rec) {
      return rec[0];
    });
    //differentiate common and diff id's
    for (var j = 0; j < allLangId.length; j++) {
      if (assignedData.contains(allLangId[j])) {
        commonList.push(allLangId[j]);
      } else {
        diffList.push(allLangId[j]);
      }
    }
    var lstData = self.getListForListBox(diffList);
    currRowData.lstBoxSelectLanguage.masterData = lstData;
    if(currInd !== 0){
      currRowData.lblCategoryLanguage.isVisible = false;
      currRowData.lstBoxSelectLanguage.isVisible = true;
      currRowData.lstBoxSelectLanguage.selectedKey = lstData[0][0];
      self.view.segEditAlertCategoryLanguages.setDataAt(currRowData, currInd);
    }
  },
  /*
   * on hover callback for listbox - in case same language is selected from listbox again
   */
  hideCategoryListBoxOnHover : function(widget,context){
    var self = this;
    var ind = self.view.segEditAlertCategoryLanguages.selectedRowIndex[1];
    var rowData = self.view.segEditAlertCategoryLanguages.data[ind];
    var selectedLangId = rowData.lblCategoryLanguage.info.id;
    var listSelectedId = rowData.lstBoxSelectLanguage.selectedKey;
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      //do-nothing
    }else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      if(selectedLangId === listSelectedId){
        rowData.lblCategoryLanguage.isVisible = true;
        rowData.lstBoxSelectLanguage.isVisible = false;
        self.view.segEditAlertCategoryLanguages.setDataAt(rowData, ind);
      } else if(selectedLangId === ""){ // for first time selection
        rowData.lblCategoryLanguage.isVisible = true;
        rowData.lblCategoryLanguage.text = self.getLanguageNameForCode(listSelectedId);
        rowData.lblCategoryLanguage.info.id = listSelectedId;
        rowData.lblCategoryLanguage.isVisible = false;
        self.view.segEditAlertCategoryLanguages.setDataAt(rowData, ind);
      } else{
        rowData.lstBoxSelectLanguage.selectedKey = rowData.lblCategoryLanguage.info.id;
        rowData.lblCategoryLanguage.isVisible = true;
        rowData.lstBoxSelectLanguage.isVisible = false;
        self.view.segEditAlertCategoryLanguages.setDataAt(rowData, ind);
      }
    }
  },
  addNewCategoryLangRowAtEnd : function(event){
    var self = this;
    var segData = self.view.segEditAlertCategoryLanguages.data;
    var currInd = self.view.segEditAlertCategoryLanguages.selectedRowIndex[1];
    kony.store.setItem("keyNameCat", segData[currInd].tbxDisplayName.text);
    kony.store.setItem("keyDescriptionCat", segData[currInd].tbxDescription.text);
    if(segData.length === currInd+1 && segData.length !== self.langToShow.length){
      var newRow = self.getCategoryLangSegDataMapJson();
      newRow.lblCategoryLanguage.text = self.sLang;
      newRow.lblCategoryLanguage.info.id = "";
      newRow.lstBoxSelectLanguage.selectedKey = self.langToShow[0][0];
      self.view.segEditAlertCategoryLanguages.addDataAt(newRow, currInd+1);
      self.view.forceLayout();
    }
  },
  onHoverHideCategoryTextBoxCallback: function(widget, context) {
    var self = this;
    var ind = self.view.segEditAlertCategoryLanguages.selectedRowIndex[1];
    var rowData = self.view.segEditAlertCategoryLanguages.data[ind];
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      //do-nothing
    }else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      if (widget.id === "tbxDisplayName") {
        rowData.tbxDisplayName.isVisible = false;
        rowData.lblCategoryDisplayName.isVisible = true;
        if (rowData.tbxDisplayName.text === "" && rowData.lblCategoryDisplayName.text !== self.sAddName) {
          rowData.tbxDisplayName.text = kony.store.getItem("keyNameCat");
        }
        rowData.lblCategoryDisplayName.text = rowData.tbxDisplayName.text === "" ? self.sAddName: rowData.tbxDisplayName.text ;
      } else {
        rowData.flxDescription.isVisible = false;
        rowData.lblCategoryDescription.isVisible = true;
        if (rowData.tbxDescription.text === "" && rowData.lblCategoryDescription.text !== self.sWriteDesc) {
          rowData.tbxDescription.text = kony.store.getItem("keyDescriptionCat");
        }
        rowData.lblCategoryDescription.text = rowData.tbxDescription.text === ""? self.sWriteDesc: rowData.tbxDescription.text;
      }
      self.view.segEditAlertCategoryLanguages.setDataAt(rowData, ind);
      self.view.forceLayout();
    }
  },
  onHoverDisplayCategoryDelete : function(widget, context){
    var self = this;
    var ind = context.rowIndex;
    var rowData = self.view.segEditAlertCategoryLanguages.data[ind];
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      if(ind !== 0){
        if(rowData.flxCategoryLangDelete.isVisible === false){
          rowData.flxCategoryLangDelete.isVisible = true;
          self.view.segEditAlertCategoryLanguages.setDataAt(rowData, ind);
        }

      }
    }else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      if(rowData.flxCategoryLangDelete.isVisible === true){
        rowData.flxCategoryLangDelete.isVisible = false;
        self.view.segEditAlertCategoryLanguages.setDataAt(rowData, ind);
      }   
    }
    self.view.forceLayout();
  },
  /*
   * delete current row from the languages segment
   */
  onClickOfDeleteCategoryLang : function(){
    var self = this;
    var currInd = self.view.segEditAlertCategoryLanguages.selectedRowIndex[1];
    self.view.segEditAlertCategoryLanguages.removeAt(currInd);
    self.view.forceLayout();
  },
  showCategoryDeleteLangPopup : function(){
    var self = this;
    this.view.flxDeleteAlert.isVisible = true;
    this.view.popUpDeactivate.btnPopUpCancel.text = self.sNoLeaveAsIs;
    this.view.popUpDeactivate.btnPopUpCancel.right = "150px";
    this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.DeleteLanguage");
    this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.AreYouSureDeleteLanguage");
    this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesDelete");
    //overriding actions for alert type delete language
    this.view.popUpDeactivate.btnPopUpCancel.onClick = function(){
      self.view.flxDeleteAlert.setVisibility(false);
    };
    this.view.popUpDeactivate.flxPopUpClose.onClick = function(){
      self.view.flxDeleteAlert.setVisibility(false);
    };
    this.view.popUpDeactivate.btnPopUpDelete.onClick = function(){
      self.onClickOfDeleteCategoryLang();
      self.view.flxDeleteAlert.setVisibility(false);
    };
  },
  /*
   * create payload for activate/deactivate alert category
   */
  activateDeactivateAlertCategory : function(){
    var self = this;
    var reqParam = {
      "categoryCode": self.tabData.categoryDefintion.categoryCode
    };
    self.showCategoryStatusChangePopup(reqParam);
  },
  mapLangDesc : function(rec){
    return {
      "languageCode": rec.lblCategoryLanguage.info.id,
      "displayName": rec.lblCategoryDisplayName.text,
      "description": rec.lblCategoryDescription.text
    };
  },
  showCategoryStatusChangePopup: function(reqParam) {
    self = this;
    this.view.flxDeleteAlert.isVisible = true;
    this.view.popUpDeactivate.btnPopUpCancel.text = self.sNoLeaveAsIs;
    this.view.popUpDeactivate.btnPopUpCancel.right = "160px";
    if (this.view.contextualMenu1.lblOption4.text === self.sDeactivateRecord) {
      this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.DeactivateAlertStatus");
      this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.DeactivateAlertCategory");
      this.view.popUpDeactivate.btnPopUpDelete.text = self.sYesDeactivate;
      this.view.popUpDeactivate.btnPopUpCancel.right = "180px";
      reqParam.statusId = "SID_INACTIVE";
    } else {
      this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.ActivateAlertStatus");
      this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.ActivateAlertCategory");
      this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.YES_ACTIVATE");
      reqParam.statusId = "SID_ACTIVE";
    }
    this.view.forceLayout();
    this.view.popUpDeactivate.flxPopUpClose.onClick = function(){
      self.view.flxDeleteAlert.setVisibility(false);
    };
    this.view.popUpDeactivate.btnPopUpCancel.onClick = function(){
      self.view.flxDeleteAlert.setVisibility(false);
    };
    this.view.popUpDeactivate.btnPopUpDelete.onClick = function(){
      self.presenter.editAlertCategory(reqParam);
      self.view.flxDeleteAlert.setVisibility(false);
    };
  },
  /*
   * creates request param for create and edit alert group
   * @return: request Param
   */
  createAlertCategoryRequestParam :  function(){
    var self = this;
    var lang =  self.view.segEditAlertCategoryLanguages.data;
    var langPrefer = lang.map(self.mapLangDesc);

    var reqParam = {
      "categoryName":self.tabData.categoryDefintion.displayName,
      "categoryCode": self.tabData.categoryDefintion.categoryCode, 
      "channels":{},
      "containsAccountLevelAlerts": self.view.customCategoryRadioButtonType.selectedValue.id === "Yes" ? true:false,
      "statusId": "SID_ACTIVE",
      "addedDisplayPreferences": langPrefer,
      "removedDisplayPreferences": []
    };
    var channels = {};
    for(var i=0;i<self.tabData.categoryChannels.length;i++)
    {
      var flag = false;
      for (var j=0; j<self.selectedChannelList.length; j++)
      {
        if (self.selectedChannelList[j] === self.tabData.categoryChannels[i].channelDisplayName) {
          {

            channels[self.tabData.categoryChannels[i].channelID] = true;
            flag = true;
            break;
          }
        }
      }
      if(flag === false)
      {
        channels[self.tabData.categoryChannels[i].channelID] = false;
      }
    }
    reqParam.channels = channels;
    return reqParam;
  },
  editAlertCategory : function(){
    var self =this;
    var reqParam = self.editAlertCategoryRequestParam();
    self.presenter.editAlertCategory(reqParam);
  },
  /*
   * form request param for edit alert group
   * @return: edit request param
   */
  editAlertCategoryRequestParam : function(){
    var self =this;
    var initialParam = self.createAlertCategoryRequestParam();
    var langPref = self.getAddedRemovedCategoryLangList();
    initialParam.removedDisplayPreferences = langPref.removed;
    var addedLang = langPref.added.map(self.mapLangDesc);
    initialParam.addedDisplayPreferences = addedLang;
    return initialParam;
  },
  /*
   * function to get removed lang list
   * @return: removed id's list
   */
  getAddedRemovedCategoryLangList : function(){
    var self = this;
    var originalList = self.view.segEditAlertCategoryLanguages.info.segData;
    var newList = self.view.segEditAlertCategoryLanguages.data;
    var orgId = originalList.map(function(rec){
      return rec.LanguageCode;
    });
    var newId = newList.map(function(rec){
      return rec.lblCategoryLanguage.info.id;
    });
    var removed = self.getDiffOfArray(orgId, newId);
    var addList = newList.filter(function(rec){
      for(var i=0;i<newId.length;i++){
        if(newId[i] !== "" && newId[i] === rec.lblCategoryLanguage.info.id){
          return rec;
        }
      }
      return null;
    });
    return {"added":addList,"removed":removed};
  },
  setFlowActions: function() {
    var scopeObj = this;
    this.view.segSubAlerts.onHover=this.saveScreenY;
    this.view.flxSelectOptions.onHover = this.onHoverEventCallback;
    this.view.flxSubAlertContextualMenu.onHover=this.onHoverEventCallback;
    this.view.flxArrowTopMostPosition.onClick = function() {
      if(scopeObj.reorderAlertIndex !== -1)
      {
        scopeObj.moveTop(scopeObj.reorderAlertIndex);
      }  	
    };
    this.view.flxArrowTopPosition.onClick = function() {
      if(scopeObj.reorderAlertIndex !== -1)
      {
        scopeObj.moveUp(scopeObj.reorderAlertIndex);
      }  	
    };
    this.view.flxArrowBottomPosition.onClick = function() {
      if(scopeObj.reorderAlertIndex !== -1)
      {
        scopeObj.moveDown(scopeObj.reorderAlertIndex);
      }  	
    };
    this.view.flxArrowBottomMostPosition.onClick = function() {
      if(scopeObj.reorderAlertIndex !== -1)
      {
        scopeObj.moveBottom(scopeObj.reorderAlertIndex);
      }  	
    };	
    this.view.btnReorderAlerts.onClick = function(){
      scopeObj.view.flxSequencePopUp.setVisibility(true);
      scopeObj.reorderAlert = true;
      scopeObj.enableDisableAllArrows(1);
      scopeObj.view.lblReorderAlert.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.ReorderAlertGroups");
      var sequenceData = scopeObj.tabData.alertGroups.map(scopeObj.mappingAlertSegmentSequenceData.bind(scopeObj));
      scopeObj.setAlertSegmentSequenceData(sequenceData);
    };
    this.view.btnReorderCategories.onClick = function(){
      scopeObj.reorderAlert = false;
      scopeObj.view.flxSequencePopUp.setVisibility(true);
      scopeObj.enableDisableAllArrows(1);
      scopeObj.view.lblReorderAlert.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.ReorderCategory");
      var sequenceData = scopeObj.tab.map(scopeObj.mappingCategorySegmentSequenceData.bind(scopeObj));
      scopeObj.setCategorySegmentSequenceData(sequenceData);
    };

    this.view.flxPopupCloseSequence.onClick = function(){
      scopeObj.view.btnCancelSequencePopup.onClick();
    };
    this.view.btnCancelSequencePopup.onClick = function(){
      scopeObj.view.flxSequencePopUp.setVisibility(false);
    };
    this.view.btnUpdateSequencePopup.onClick = function(){
      //make an update sequence call
      scopeObj.view.flxSequencePopUp.setVisibility(false);
      scopeObj.updateSequence();
    };
    this.view.flxCategoryOptions.onClick = function() {
      var status = scopeObj.view.lblViewDetailsCategoryStatus.text.toLowerCase();
      scopeObj.view.flxCategoryOptionsMenu.setVisibility(scopeObj.view.flxCategoryOptionsMenu.isVisible === false);
      scopeObj.view.contextualMenu1.lblOption4.text = 
        status === kony.i18n.getLocalizedString("i18n.frmOutageMessageController.active") ?
        scopeObj.sPermDeactivate :scopeObj.sPermActivate;
      scopeObj.view.contextualMenu1.lblIconOption4.text = 
        status === kony.i18n.getLocalizedString("i18n.frmOutageMessageController.active") ?
        "\ue91c" : "\ue931";
      scopeObj.view.flxCategoryOptionsMenu.setVisibility(true);
    };
    this.view.flxCategoryOptionsMenu.onHover = scopeObj.onHoverEventCallback;
    this.view.contextualMenu1.flxOption2.onClick = function() {
      scopeObj.view.flxCategoryOptionsMenu.setVisibility(false);
      scopeObj.view.flxAlertCategoryDetailsScreen.setVisibility(false);
      scopeObj.view.flxEditAlertCategoryScreen.setVisibility(true);			
      scopeObj.setEditAlertCategoryData();	
    };
    this.view.contextualMenu1.flxOption4.onClick = function() {
      scopeObj.view.flxCategoryOptionsMenu.setVisibility(false);
      scopeObj.activateDeactivateAlertCategory();
    };
    this.view.EditAlertCategoryButtons.btnSave.onClick = function() {
      scopeObj.clearValidationsForEditAlertCategory();
      var isValid = scopeObj.validateEditAlertCategoryScreen();
      if (isValid) {
        scopeObj.editAlertCategory();
      }
    };
    this.view.EditAlertCategoryButtons.btnCancel.onClick = function() {
      scopeObj.showAlertCategoryDetailScreen();
    };
    this.view.segSubAlerts.onRowClick = function(){
      scopeObj.getSubAlertViewData();
      scopeObj.getVariableReference();
      scopeObj.toggleBreadcrumbButtons("subalert");
    };
    this.view.btnAddSubAlerts.onClick = function(){
      scopeObj.presenter.getAlertCodes({"eventTypeId":scopeObj.view.lblAlertTypeCodeValue.text});
      //       scopeObj.alertGroupCurrAction=scopeObj.alertGroupActionConfig.CREATE;
      //       scopeObj.showAddSubAlert();
    };
    this.view.flxEdit.onClick = function(){
      scopeObj.alertGroupCurrAction=scopeObj.alertGroupActionConfig.EDIT;
      scopeObj.showEditSubAlert();
    };
    this.view.flxDeactivate.onClick = function(){
      scopeObj.alertGroupCurrAction = scopeObj.alertGroupActionConfig.EDIT;
      if(scopeObj.view.lblDeactivate.text===scopeObj.sPermDeactivate)
        scopeObj.showDeactivateSubAlert();
      else
        scopeObj.activateDeactivateSubAlert("SID_ACTIVE");
    };
    this.view.flxAlertClose.onClick = function(){
      scopeObj.view.flxSubAlertCodeGrey.setVisibility(false);
      scopeObj.view.flxAddSubAlertPopUp.setVisibility(false);
    };
    this.view.btnCancel.onClick = function(){
      scopeObj.view.flxSubAlertCodeGrey.setVisibility(false);
      scopeObj.view.flxAddSubAlertPopUp.setVisibility(false);
    };
    this.view.btnsave.onClick = function(){
      if(scopeObj.validateFields()){
        if(scopeObj.toCheckAlertNameAvailability()&&scopeObj.checkAlertCode()){
          var alertData={
            "alertTypeCode":scopeObj.view.lblAlertTypeCodeValue.text,
            "name": scopeObj.view.txtbxSubAlertName.text,
            "code":scopeObj.view.lstbxSubAlertCode.selectedKeyValue[1],
            "description": scopeObj.view.txtAlertDescription.text,
            "statusId":scopeObj.view.AlertStatusSwitch.switchToggle.selectedIndex===1?"SID_INACTIVE":"SID_ACTIVE",
            "addedTemplates": [],
            "removedTemplates": []
          };
          scopeObj.createEditAlert(alertData);
        }
      }
    };
    this.view.flxEyeicon.onClick = function(){
      if(scopeObj.view.flxSubAlertContextualMenu.isVisible)
        scopeObj.view.flxSubAlertContextualMenu.setVisibility(false);
      scopeObj.setPreviewData();
    };
    this.view.btnPopUpCancel.onClick = function(){
      scopeObj.view.flxPreviewPopup.setVisibility(false);
    };
    this.view.flxPopUpClose.onClick = function(){
      scopeObj.view.flxPreviewPopup.setVisibility(false);
    };
    this.view.btnEmail.onClick = function(){
      scopeObj.view.lblPreviewTemplateBody.setVisibility(false);
      scopeObj.view.rtxViewer.setVisibility(true);
      if(scopeObj.view.flxViewTemplate.isVisible===true){
        scopeObj.view.lblPreviewSubHeader1.text=scopeObj.populateAlertPreview(scopeObj.view.lblEmailTitleValue.text);
        document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML =scopeObj.populateAlertPreview(scopeObj.emailRtxData);
      }else{
        scopeObj.view.lblPreviewSubHeader1.text=scopeObj.populateAlertPreview(scopeObj.view.tbxEmailSubject.text);
        document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML =scopeObj.populateAlertPreview(scopeObj.emailRtxData);
      }
      scopeObj.view.flxTemplatePreviewHeader.setVisibility(true);
      scopeObj.view.flxPopUpButtons.top="3px";
      scopeObj.setSkinForChannelTabs(scopeObj.view.btnEmail);
      scopeObj.view.forceLayout();
    };
    this.view.btnSMS.onClick = function(){
      if(scopeObj.view.flxViewTemplate.isVisible===true){
        scopeObj.view.lblPreviewTemplateBody.text=scopeObj.populateAlertPreview(scopeObj.view.ViewTemplateSMS.lblChannelMsg.text);
      }else{
        scopeObj.view.lblPreviewTemplateBody.text=scopeObj.populateAlertPreview(scopeObj.view.txtSMSMsg.text);
      }
      scopeObj.view.flxTemplatePreviewHeader.setVisibility(false);
      scopeObj.view.flxPopUpButtons.top="70px";
      scopeObj.view.lblPreviewTemplateBody.setVisibility(true);
      scopeObj.view.rtxViewer.setVisibility(false);
      scopeObj.setSkinForChannelTabs(scopeObj.view.btnSMS);
    };
    this.view.btnPushNoti.onClick = function(){
      if(scopeObj.view.flxViewTemplate.isVisible===true){
        scopeObj.view.lblPreviewSubHeader1.text=scopeObj.populateAlertPreview(scopeObj.view.ViewTemplatePush.lblTitleValue.text);
        scopeObj.view.lblPreviewTemplateBody.text=scopeObj.populateAlertPreview(scopeObj.view.ViewTemplatePush.lblChannelMsg.text);
      }else{
        scopeObj.view.lblPreviewSubHeader1.text=scopeObj.populateAlertPreview(scopeObj.view.tbxPushNotificationTitle.text);
        scopeObj.view.lblPreviewTemplateBody.text=scopeObj.populateAlertPreview(scopeObj.view.txtPushNotification.text);
      }
      scopeObj.view.flxTemplatePreviewHeader.setVisibility(true);
      scopeObj.view.flxPopUpButtons.top="3px";
      scopeObj.view.lblPreviewTemplateBody.setVisibility(true);
      scopeObj.view.rtxViewer.setVisibility(false);
      scopeObj.setSkinForChannelTabs(scopeObj.view.btnPushNoti);
    };
    this.view.btnNotifCenter.onClick = function(){
      if(scopeObj.view.flxViewTemplate.isVisible===true){
        scopeObj.view.lblPreviewSubHeader1.text=scopeObj.populateAlertPreview(scopeObj.view.ViewTemplateCenter.lblTitleValue.text);
        scopeObj.view.lblPreviewTemplateBody.text=scopeObj.populateAlertPreview(scopeObj.view.ViewTemplateCenter.lblChannelMsg.text);
      }else{
        scopeObj.view.lblPreviewSubHeader1.text=scopeObj.populateAlertPreview(scopeObj.view.tbxNotiCenterTitle.text);
        scopeObj.view.lblPreviewTemplateBody.text=scopeObj.populateAlertPreview(scopeObj.view.txtNotiCenterMsg.text);
      }
      scopeObj.view.flxTemplatePreviewHeader.setVisibility(true);
      scopeObj.view.flxPopUpButtons.top="3px";
      scopeObj.view.lblPreviewTemplateBody.setVisibility(true);
      scopeObj.view.rtxViewer.setVisibility(false);
      scopeObj.setSkinForChannelTabs(scopeObj.view.btnNotifCenter);
    };
    this.view.txtSMSMsg.onKeyUp = function(){
      if(scopeObj.view.txtSMSMsg.text.trim().length===0)
      {
        scopeObj.view.lblSMSMsgSize.setVisibility(false);
      }
      else
      {
        scopeObj.view.lblSMSMsgSize.text=scopeObj.view.txtSMSMsg.text.trim().length+"/300";
        scopeObj.view.lblSMSMsgSize.setVisibility(true);
      }
      scopeObj.view.forceLayout();
    };
    this.view.txtSMSMsg.onEndEditing = function(){
      if(scopeObj.view.lblSMSMsgSize.isVisible){
        scopeObj.view.lblSMSMsgSize.setVisibility(false);
      }
    };
    this.view.txtPushNotification.onKeyUp = function(){
      if(scopeObj.view.txtPushNotification.text.trim().length===0)
      {
        scopeObj.view.lblPushNotificationSize.setVisibility(false);
      }
      else
      {
        scopeObj.view.lblPushNotificationSize.text=scopeObj.view.txtPushNotification.text.trim().length+"/300";
        scopeObj.view.lblPushNotificationSize.setVisibility(true);
      }
      scopeObj.view.forceLayout();
    };
    this.view.txtPushNotification.onEndEditing = function(){
      if(scopeObj.view.lblPushNotificationSize.isVisible){
        scopeObj.view.lblPushNotificationSize.setVisibility(false);
      }
    };
    this.view.txtNotiCenterMsg.onKeyUp = function(){
      if(scopeObj.view.txtNotiCenterMsg.text.trim().length===0)
      {
        scopeObj.view.lblNotiCenterMsgSize.setVisibility(false);
      }
      else
      {
        scopeObj.view.lblNotiCenterMsgSize.text=scopeObj.view.txtNotiCenterMsg.text.trim().length+"/300";
        scopeObj.view.lblNotiCenterMsgSize.setVisibility(true);
      }
      scopeObj.view.forceLayout();
    };
    this.view.txtNotiCenterMsg.onEndEditing = function(){
      if(scopeObj.view.lblNotiCenterMsgSize.isVisible){
        scopeObj.view.lblNotiCenterMsgSize.setVisibility(false);
      }
    };
    this.view.tbxPushNotificationTitle.onKeyUp = function(){
      if(scopeObj.view.tbxPushNotificationTitle.text.trim().length===0)
      {
        scopeObj.view.lblPushNotificationSize.setVisibility(false);
      }
      else
      {
        scopeObj.view.flxNoPushNotification.setVisibility(false);
        scopeObj.view.lblPushNotificationSize.text=scopeObj.view.tbxPushNotificationTitle.text.trim().length+"/100";
        scopeObj.view.lblPushNotificationSize.setVisibility(true);
      }
      scopeObj.view.forceLayout();
    };
    this.view.tbxPushNotificationTitle.onEndEditing = function(){
      if(scopeObj.view.lblPushNotificationSize.isVisible){
        scopeObj.view.lblPushNotificationSize.setVisibility(false);
      }
    };
    this.view.tbxNotiCenterTitle.onKeyUp = function(){
      if(scopeObj.view.tbxNotiCenterTitle.text.trim().length===0)
      {
        scopeObj.view.lblNotiCenterMsgSize.setVisibility(false);
      }
      else
      {
        scopeObj.view.flxNoNotiCenterMsgError.setVisibility(false);
        scopeObj.view.lblNotiCenterMsgSize.text=scopeObj.view.tbxNotiCenterTitle.text.trim().length+"/100";
        scopeObj.view.lblNotiCenterMsgSize.setVisibility(true);
      }
      scopeObj.view.forceLayout();
    };
    this.view.tbxNotiCenterTitle.onEndEditing = function(){
      if(scopeObj.view.lblNotiCenterMsgSize.isVisible){
        scopeObj.view.lblNotiCenterMsgSize.setVisibility(false);
      }
    };
    this.view.tbxEmailSubject.onKeyUp = function(){
      if(scopeObj.view.tbxEmailSubject.text.trim().length===0)
      {
        scopeObj.view.lblSubjectSize.setVisibility(false);
      }
      else
      {
        scopeObj.view.flxNoEmailSubject.setVisibility(false);
        scopeObj.view.lblSubjectSize.text=scopeObj.view.tbxEmailSubject.text.trim().length+"/100";
        scopeObj.view.lblSubjectSize.setVisibility(true);
      }
      scopeObj.view.forceLayout();
    };
    this.view.tbxEmailSubject.onEndEditing = function(){
      if(scopeObj.view.lblSubjectSize.isVisible){
        scopeObj.view.lblSubjectSize.setVisibility(false);
      }
    };
    this.view.flxAddTemplateButton.onClick = function(){
      if(scopeObj.view.flxSubAlertContextualMenu.isVisible)
        scopeObj.view.flxSubAlertContextualMenu.setVisibility(false);
      scopeObj.addContentTemplate();
    };
    this.view.txtbxSubAlertName.onKeyUp = function(){
      scopeObj.view.txtbxSubAlertName.skin="skntbxLato35475f14px";
      scopeObj.view.flxErrorAlertName.setVisibility(false);
      if(scopeObj.view.txtbxSubAlertName.text.trim().length===0)
      {
        scopeObj.view.lblAlertNameCount.setVisibility(false);
      }
      else
      {
        scopeObj.view.lblAlertNameCount.text=scopeObj.view.txtbxSubAlertName.text.trim().length+"/50";
        scopeObj.view.lblAlertNameCount.setVisibility(true);
      }
      scopeObj.view.forceLayout();
    };
    this.view.txtbxSubAlertName.onEndEditing = function(){
      if(scopeObj.view.lblAlertNameCount.isVisible){
        scopeObj.view.lblAlertNameCount.setVisibility(false);
      }
    };
    this.view.txtAlertDescription.onKeyUp = function(){
      scopeObj.view.txtAlertDescription.skin="skntbxLato35475f14px";
      scopeObj.view.flxNoDescriptionError.setVisibility(false);
      if(scopeObj.view.txtAlertDescription.text.trim().length===0)
      {
        scopeObj.view.lblAlertDescriptionSize.setVisibility(false);
      }
      else
      {
        scopeObj.view.lblAlertDescriptionSize.text=scopeObj.view.txtAlertDescription.text.trim().length+"/500";
        scopeObj.view.lblAlertDescriptionSize.setVisibility(true);
      }
      scopeObj.view.forceLayout();
    };
    this.view.txtAlertDescription.onEndEditing = function(){
      if(scopeObj.view.lblAlertDescriptionSize.isVisible){
        scopeObj.view.lblAlertDescriptionSize.setVisibility(false);
      }
    };
    this.view.flxOptionEdit.onClick = function(){
      if(scopeObj.view.flxSubAlertContextualMenu.isVisible)
        scopeObj.view.flxSubAlertContextualMenu.setVisibility(false);
      scopeObj.editContentTemplate();
    };
    this.view.btnTemplateCancel.onClick = function(){
      scopeObj.view.lstBoxSubAlertResponseState.setEnabled(true);
      scopeObj.view.lstBoxSubAlertLanguages.setEnabled(true);
      scopeObj.view.lstBoxSubAlertResponseState.skin="sknlbxBgffffffBorderc1c9ceRadius3Px";
      scopeObj.view.lstBoxSubAlertLanguages.skin="sknlbxBgffffffBorderc1c9ceRadius3Px";
      scopeObj.setContentTemplateData();
      scopeObj.view.lblTemplateHeader.text=scopeObj.sAlertContentTemplate;
      scopeObj.view.lblContentBy.text="View Content By";//kony.i18n.getLocalizedString("i18n.frmAlertsManagement.viewContentBy");
      scopeObj.view.flxOptionsSubAlerts.setVisibility(true);
      scopeObj.view.flxAddTemplateButton.setVisibility(true);
      scopeObj.view.flxSaveTemplateButtons.setVisibility(false);
      scopeObj.view.flxAddTemplate.setVisibility(false);
    };
    this.view.btnTemplateSave.onClick = function(){
      if(scopeObj.validateTitles()){
        scopeObj.saveTemplate();
        scopeObj.view.lstBoxSubAlertResponseState.setEnabled(true);
        scopeObj.view.lstBoxSubAlertLanguages.setEnabled(true);
        scopeObj.view.lstBoxSubAlertResponseState.skin="sknlbxBgffffffBorderc1c9ceRadius3Px";
        scopeObj.view.lstBoxSubAlertLanguages.skin="sknlbxBgffffffBorderc1c9ceRadius3Px";
        scopeObj.view.lblTemplateHeader.text=scopeObj.sAlertContentTemplate;
        scopeObj.view.lblContentBy.text="View Content By";//kony.i18n.getLocalizedString("i18n.frmAlertsManagement.viewContentBy");
        scopeObj.view.flxOptionsSubAlerts.setVisibility(true);
        scopeObj.view.flxAddTemplateButton.setVisibility(true);
        scopeObj.view.flxSaveTemplateButtons.setVisibility(false);
        scopeObj.view.flxAddTemplate.setVisibility(false);
      }
    };
    this.view.flxOptionsSubAlerts.onClick = function(){
      if(scopeObj.view.flxSubAlertContextualMenu.isVisible){
        scopeObj.view.flxSubAlertContextualMenu.setVisibility(false);
      } else{
        scopeObj.showViewContextualMenu();
      }
    };
    this.view.flxEditSubAlertView.onClick = function(){
      scopeObj.alertGroupCurrAction=scopeObj.alertGroupActionConfig.EDIT;
      scopeObj.showEditSubAlert();
    };
    this.view.flxDeactivateAlert.onClick = function(){
      scopeObj.alertGroupCurrAction = scopeObj.alertGroupActionConfig.EDIT;
      if(scopeObj.view.lblDeactivateAlert.text===scopeObj.sPermDeactivate)
        scopeObj.showDeactivateSubAlert();
      else
        scopeObj.activateDeactivateSubAlert("SID_ACTIVE");
    };
    this.view.flxViewSubAlert.onScrollStart = function(){
      if(scopeObj.view.flxSubAlertContextualMenu.isVisible)
        scopeObj.view.flxSubAlertContextualMenu.setVisibility(false);
      if(scopeObj.view.flxVariableReferenceContainer.isVisible)
        scopeObj.view.flxVariableReferenceContainer.setVisibility(false);
    };
    this.view.lstBoxSubAlertResponseState.onSelection = function(){
      if(scopeObj.view.flxViewTemplate.isVisible===true||scopeObj.view.flxNoContentTemplate.isVisible===true)
        scopeObj.setChannelsTemplateData();
      else if(scopeObj.view.flxAddTemplate.isVisible===true&&scopeObj.alertGroupCurrAction === scopeObj.alertGroupActionConfig.CREATE){
        scopeObj.setListSelectedChannelsData();
      }
    };
    this.view.lstBoxSubAlertLanguages.onSelection = function(){
      if(scopeObj.view.lstBoxSubAlertLanguages.selectedKey!=="select"&&scopeObj.view.flxViewTemplate.isVisible===false){
        scopeObj.view.flxEyeicon.setVisibility(true);
        scopeObj.view.flxSaveTemplateButtons.setVisibility(true);
        scopeObj.view.flxAddTemplate.setVisibility(true);
      }else{
        scopeObj.view.flxEyeicon.setVisibility(false);
        scopeObj.view.flxSaveTemplateButtons.setVisibility(false);
        scopeObj.view.flxAddTemplate.setVisibility(false);
      }
      if(scopeObj.view.flxViewTemplate.isVisible===true||scopeObj.view.flxNoContentTemplate.isVisible===true)
        scopeObj.setChannelsTemplateData();
      else if(scopeObj.view.flxAddTemplate.isVisible===true&&scopeObj.alertGroupCurrAction === scopeObj.alertGroupActionConfig.CREATE){
        scopeObj.setListSelectedChannelsData();
      }
    };
    this.view.customListboxApps.flxSelectedText.onClick = function(){
      scopeObj.view.customListboxApps.flxSegmentList.setVisibility(scopeObj.view.customListboxApps.flxSegmentList.isVisible === false);
      scopeObj.view.customListboxUsertypes.flxSegmentList.setVisibility(false);
      scopeObj.clearValidationsForAddAlertGroup(3, scopeObj.view.customListboxApps.flxSelectedText, scopeObj.view.customListboxApps.flxListboxError);
    };
    this.view.customListboxUsertypes.flxSelectedText.onClick = function(){
      scopeObj.view.customListboxUsertypes.flxSegmentList.setVisibility(scopeObj.view.customListboxUsertypes.flxSegmentList.isVisible === false);
      scopeObj.view.customListboxApps.flxSegmentList.setVisibility(false);
      scopeObj.clearValidationsForAddAlertGroup(3, scopeObj.view.customListboxUsertypes.flxSelectedText, scopeObj.view.customListboxUsertypes.flxListboxError);
    };
    this.view.customListboxApps.flxCheckBox.onClick = function(){
      scopeObj.onClickOfSelectAll("apps");
    };
    this.view.customListboxUsertypes.flxCheckBox.onClick = function(){
      scopeObj.onClickOfSelectAll("users");
    };
    this.view.customListboxApps.segList.onRowClick = function(){
      scopeObj.onCustomListBoxRowClick("apps");
    };
    this.view.customListboxUsertypes.segList.onRowClick = function(){
      scopeObj.onCustomListBoxRowClick("users");
    };
    this.view.customListboxApps.flxDropdown.onClick = function(){
      scopeObj.view.customListboxApps.flxSegmentList.setVisibility(scopeObj.view.customListboxApps.flxSegmentList.isVisible === false);
    };
    this.view.customListboxUsertypes.flxDropdown.onClick = function(){
      scopeObj.view.customListboxUsertypes.flxSegmentList.setVisibility(scopeObj.view.customListboxUsertypes.flxSegmentList.isVisible === false);
    };

    this.view.flxOptions.onClick = function(){
      var top = (scopeObj.view.flxOptions.frame.y + 45) - scopeObj.view.flxAlertTypeDetailsScreen.contentOffsetMeasured.y;
      scopeObj.view.flxAlertTypesContextualMenu.top = top +"dp";
      scopeObj.view.flxAlertTypesContextualMenu.setVisibility(scopeObj.view.flxAlertTypesContextualMenu.isVisible === false);
      scopeObj.view.contextualMenuAlertTypeDetail.lblOption4.text = scopeObj.view.lblViewDetailsAlertTypeStatus.text.toLowerCase() === "active" ?
        scopeObj.sPermDeactivate :scopeObj.sPermActivate;
      scopeObj.view.contextualMenuAlertTypeDetail.lblIconOption4.text = scopeObj.view.lblViewDetailsAlertTypeStatus.text.toLowerCase() === "active" ?
        "\ue91c" : "\ue931";
    };
    this.view.flxAlertTypeDetailsScreen.onScrollStart = function(){
      scopeObj.view.flxAlertTypesContextualMenu.setVisibility(false);
    };
    this.view.flxAlertCategoryDetailsScreen.onScrollStart = function(){
      scopeObj.view.flxCategoryOptionsMenu.setVisibility(false);
    };
    this.view.contextualMenuAlertTypeDetail.flxOption2.onClick = function(){
      scopeObj.alertGroupCurrAction = scopeObj.alertGroupActionConfig.EDIT;
      scopeObj.alertGroupScreenContext = 2;
      var ind = scopeObj.view.segListing.selectedRowIndex[1];
      var alertGroupId = scopeObj.view.segListing.data[ind].lblAlertCode.info.id;
      scopeObj.presenter.getAlertTypeDetails({"AlertTypeId":alertGroupId},"edit");
      scopeObj.view.flxAlertTypesContextualMenu.setVisibility(false);
      scopeObj.view.breadcrumbAlerts.lblCurrentScreen.text =scopeObj.view.lblAlertTypeAlertNameValue.text.toUpperCase();
      scopeObj.initializeCreateData();
    };
    this.view.contextualMenuAlertTypeDetail.flxOption4.onClick = function(){
      scopeObj.alertGroupScreenContext = 2;
      scopeObj.presenter.getAlertTypeDetails({"AlertTypeId":scopeObj.view.lblAlertTypeCodeValue.text},"statusChange");
    };
    this.view.flxAlertTypesContextualMenu.onHover = scopeObj.onHoverEventCallback;
    this.view.addAlertTypeButtons.btnCancel.onClick = function(){
      scopeObj.clearValidationsForAddAlertGroup();
      scopeObj.view.customListboxApps.flxSegmentList.setVisibility(false);
      scopeObj.view.customListboxUsertypes.flxSegmentList.setVisibility(false);
      scopeObj.hideAddAlertGroupScreen();
    };
    this.view.addAlertTypeButtons.btnSave.onClick = function(){
      scopeObj.clearValidationsForAddAlertGroup();
      var isValid = scopeObj.validateAddAlertGroupScreen();
      scopeObj.view.customListboxApps.flxSegmentList.setVisibility(false);
      scopeObj.view.customListboxUsertypes.flxSegmentList.setVisibility(false);
      if(scopeObj.alertGroupCurrAction === scopeObj.alertGroupActionConfig.CREATE &&
         isValid){
        scopeObj.createNewAlertGroup();
        scopeObj.hideAddAlertGroupScreen();
      }else if(scopeObj.alertGroupCurrAction === scopeObj.alertGroupActionConfig.EDIT &&
               isValid){
        scopeObj.editAlertGroup();
        scopeObj.hideAddAlertGroupScreen();
      }
    };
    this.view.customListboxApps.btnOk.onClick = function(){
      scopeObj.view.customListboxApps.flxSegmentList.setVisibility(scopeObj.view.customListboxApps.flxSegmentList.isVisible === false);
    };
    this.view.customListboxUsertypes.btnOk.onClick = function(){
      scopeObj.view.customListboxUsertypes.flxSegmentList.setVisibility(scopeObj.view.customListboxUsertypes.flxSegmentList.isVisible === false);
    };
    this.view.imgCheckboxAttribute.onClick = function(){
      scopeObj.toggleAttributes();
    };
    this.view.lstBoxAddAlertTypeAttribute.onSelection = function(){
      scopeObj.clearValidationsForAddAlertGroup(2, scopeObj.view.lstBoxAddAlertTypeAttribute, scopeObj.view.flxAttributesError);
      scopeObj.changeUIForAttibuteType("attribute");
      scopeObj.view.forceLayout();
    };
    this.view.lstBoxAddAlertTypeCriteria.onSelection = function(){
      scopeObj.clearValidationsForAddAlertGroup(2, scopeObj.view.lstBoxAddAlertTypeCriteria, scopeObj.view.flxAddAlertTypeErrorCriteria);
      scopeObj.changeUIForAttibuteType("criteria");
      scopeObj.view.forceLayout();
    };
    this.view.btnAddAlerts.onClick = function(){
      scopeObj.alertGroupCurrAction = scopeObj.alertGroupActionConfig.CREATE;
      scopeObj.alertGroupScreenContext = 1;
      scopeObj.view.flxAlertTypesContextualMenu.setVisibility(false);
      scopeObj.view.breadcrumbAlerts.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.ADD_ALERT_TYPE");
      scopeObj.view.lblAddAlertTypeName.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Add_Alert_Type");
      scopeObj.initializeCreateData();
      scopeObj.showAddAlertGroupScreen();
    };
    this.view.segVariableReference.onRowClick = function() {
      var cursorPos;
      var strLength;
      var data = scopeObj.view.segVariableReference.data[scopeObj.view.segVariableReference.selectedIndex[1]];
      if(scopeObj.cursorPoint==="SMS/Text"){
        cursorPos = document.getElementById("frmAlertsManagement_txtSMSMsg").selectionStart;
        strLength = scopeObj.view.txtSMSMsg.text.length;
        scopeObj.view.txtSMSMsg.text = 
          scopeObj.view.txtSMSMsg.text.substring(0, cursorPos) +
          "[#]" + data.lbVariableReference.text + "[/#]" +
          scopeObj.view.txtSMSMsg.text.substring(cursorPos, strLength);
      }
      else if(scopeObj.cursorPoint==="PushNotification"){
        cursorPos = document.getElementById("frmAlertsManagement_txtPushNotification").selectionStart;
        strLength = scopeObj.view.txtPushNotification.text.length;
        scopeObj.view.txtPushNotification.text = 
          scopeObj.view.txtPushNotification.text.substring(0, cursorPos) +
          "[#]" + data.lbVariableReference.text + "[/#]" +
          scopeObj.view.txtPushNotification.text.substring(cursorPos, strLength);
      }
      else if(scopeObj.cursorPoint==="NotificationCenter"){
        cursorPos = document.getElementById("frmAlertsManagement_txtNotiCenterMsg").selectionStart;
        strLength = scopeObj.view.txtNotiCenterMsg.text.length;
        scopeObj.view.txtNotiCenterMsg.text = 
          scopeObj.view.txtNotiCenterMsg.text.substring(0, cursorPos) +
          "[#]" + data.lbVariableReference.text + "[/#]" +
          scopeObj.view.txtNotiCenterMsg.text.substring(cursorPos, strLength);
      }
      else if(scopeObj.cursorPoint==="Email"){
        document.getElementById("iframe_rtxEmailTemplate").contentWindow.document.execCommand("insertText", false, "[#]" + data.lbVariableReference.text + "[/#]");
        this.emailRtxData = document.getElementById("iframe_rtxEmailTemplate").contentWindow.document.getElementById("editor").innerHTML;
      }
    };
    this.view.alertSubscriptionSwitch.switchToggle.onSlide = function() {
      if (scopeObj.view.alertSubscriptionSwitch.switchToggle.selectedIndex === 1) 
        scopeObj.showSwitchTogglePopup(scopeObj.sDeactivate, scopeObj.sSubscription);
      else 
        scopeObj.showSwitchTogglePopup(scopeObj.sActivate, scopeObj.sSubscription);
    };
    this.view.customServiceStatusSwitch.switchToggle.onSlide = function() {
      if (scopeObj.view.customServiceStatusSwitch.switchToggle.selectedIndex === 1) 
        scopeObj.showSwitchTogglePopup(scopeObj.sDeactivate, "status");
      else 
        scopeObj.showSwitchTogglePopup(scopeObj.sActivate, "status");
    };
    this.view.btnTabName1.onClick = function() {
      scopeObj.presenter.fetchAlertCategory(scopeObj.tab1.alertcategory_id);
      scopeObj.recentButton = scopeObj.view.btnTabName1;
      scopeObj.recentButtonIndex = 0;
    };
    this.view.btnTabName2.onClick = function() {
      scopeObj.presenter.fetchAlertCategory(scopeObj.tab2.alertcategory_id);        
      scopeObj.recentButton = scopeObj.view.btnTabName2;
      scopeObj.recentButtonIndex = 1;
    };
    this.view.btnTabName3.onClick = function() {
      scopeObj.presenter.fetchAlertCategory(scopeObj.tab3.alertcategory_id);                
      scopeObj.recentButton = scopeObj.view.btnTabName3;
      scopeObj.recentButtonIndex = 2;
    };
    this.view.btnTabName4.onClick = function() {
      scopeObj.presenter.fetchAlertCategory(scopeObj.tab4.alertcategory_id);                
      scopeObj.recentButton = scopeObj.view.btnTabName4;
      scopeObj.recentButtonIndex = 3;
    };
    this.view.flxDescription.onClick = function() {
      scopeObj.changeLableStatus();
    };
    this.view.toastMessage.flxRightImage.onClick = function() {
      scopeObj.view.toastMessage.hideToastMessage(scopeObj);
    };

    this.view.btnEditAlertType.onClick = function() {
      scopeObj.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.alertsManagment.editAlertType");
      scopeObj.view.flxAlertsBreadCrumb.setVisibility(true);
      scopeObj.view.breadcrumbs.fontIconBreadcrumbsRight.setVisibility(false);
      scopeObj.view.breadcrumbs.btnPreviousPage.setVisibility(false);
      scopeObj.view.breadcrumbs.fontIconBreadcrumbsRight2.setVisibility(true);
      scopeObj.view.breadcrumbs.lblCurrentScreen.setVisibility(true);
      scopeObj.view.flxAlertsMain.setVisibility(false);
      scopeObj.view.flxEditAlert.setVisibility(true);
      scopeObj.view.flxEditAlertInner.setVisibility(false);
      scopeObj.view.flxEditAlertMain.setVisibility(true);
      scopeObj.populateAlertTypeEdit();
    };
    this.view.commonButtons.btnSave.onClick = function() {
      scopeObj.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.alertsManagment.viewAlert");
      if (scopeObj.view.flxEditAlertMain.isVisible === true) {
        if (scopeObj.showErrorAlertType()) {
          scopeObj.view.flxAlertsBreadCrumb.setVisibility(false);
          scopeObj.saveEditedAlertTypeData();
          scopeObj.view.flxEditAlert.setVisibility(false);
          scopeObj.view.flxViewAlertData.setVisibility(false);
          scopeObj.view.flxAlertsMain.setVisibility(true);
        }
      } else if (scopeObj.view.flxEditAlertInner.isVisible === true) {
        if (scopeObj.showErrorAlert()) {
          scopeObj.saveEditedAlertData();
          scopeObj.view.flxEditAlert.setVisibility(false);
          scopeObj.view.flxAlertsMain.setVisibility(false);
          scopeObj.view.flxViewAlertData.setVisibility(true);
        }
      }
    };
    this.view.commonButtons.btnCancel.onClick = function() {
      scopeObj.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.alertsManagment.viewAlert");
      if (scopeObj.view.flxEditAlertMain.isVisible === true) {
        scopeObj.view.flxAlertsBreadCrumb.setVisibility(false);
        scopeObj.view.flxEditAlert.setVisibility(false);
        scopeObj.view.flxAlertsMain.setVisibility(true);
      } else if (scopeObj.view.flxEditAlertInner.isVisible === true) {
        scopeObj.view.flxEditAlert.setVisibility(false);
        scopeObj.view.flxViewAlertData.setVisibility(true);
      }
    };
    this.view.btnViewAlertEdit.onClick = function() {
      scopeObj.view.flxViewAlertData.setVisibility(false);
      scopeObj.view.flxEditAlert.setVisibility(true);
      scopeObj.view.flxEditAlertMain.setVisibility(false);
      scopeObj.view.flxEditAlertInner.setVisibility(true);
      scopeObj.populateAlertData();
      scopeObj.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n,alertsManagment.editAlert");
    };
    this.view.btnViewAlertDelete.onClick = function() {
      scopeObj.setDeletePopupData();
      scopeObj.view.mainHeader.lblHeading.text = scopeObj.sAlerts;
    };
    this.view.popUpDeactivate.btnPopUpCancel.onClick = function() {
      scopeObj.cancelDeleteAlert();
    };
    this.view.popUpDeactivate.btnPopUpDelete.onClick = function() {
      scopeObj.deleteAlert();
    };
    this.view.popUpDeactivate.flxPopUpClose.onClick = function() {
      scopeObj.cancelDeleteAlert();
    };
    this.view.lblSMSVariableReference.onTouchStart = function() {
      scopeObj.cursorPoint="SMS/Text";
      scopeObj.view.flxVariableReferenceContainer.onHover = scopeObj.onHoverEventCallback;
      scopeObj.view.flxVariableReferenceContainer.left = scopeObj.view.lblSMSVariableReference.frame.x + 130 +"px";
      scopeObj.view.flxVariableReferenceContainer.top = (scopeObj.view.flxAddTemplate.frame.y+scopeObj.view.flxAddSMSTemplate.frame.y)-scopeObj.view.flxViewSubAlert.contentOffsetMeasured.y-79 +"px";
      if (scopeObj.view.flxVariableReferenceContainer.isVisible === false) {
        scopeObj.view.flxVariableReferenceContainer.setVisibility(true);
      } else if (scopeObj.view.flxVariableReferenceContainer.isVisible === true) {
        scopeObj.view.flxVariableReferenceContainer.setVisibility(false);
      }
      scopeObj.view.forceLayout();
    };
    this.view.lblPushNotiVariableReference.onTouchStart = function() {
      scopeObj.cursorPoint="PushNotification";
      scopeObj.view.flxVariableReferenceContainer.onHover = scopeObj.onHoverEventCallback;
      scopeObj.view.flxVariableReferenceContainer.left = scopeObj.view.lblPushNotiVariableReference.frame.x + 125 +"px";
      scopeObj.view.flxVariableReferenceContainer.top = (scopeObj.view.flxAddTemplate.frame.y+scopeObj.view.flxAddPushNotification.frame.y)-scopeObj.view.flxViewSubAlert.contentOffsetMeasured.y-79 +"px";
      if (scopeObj.view.flxVariableReferenceContainer.isVisible === false) {
        scopeObj.view.flxVariableReferenceContainer.setVisibility(true);
      } else if (scopeObj.view.flxVariableReferenceContainer.isVisible === true) {
        scopeObj.view.flxVariableReferenceContainer.setVisibility(false);
      }
      scopeObj.view.forceLayout();
    };
    this.view.lblNotiCenterVariableReference.onTouchStart = function() {
      scopeObj.cursorPoint="NotificationCenter";
      scopeObj.view.flxVariableReferenceContainer.onHover = scopeObj.onHoverEventCallback;
      scopeObj.view.flxVariableReferenceContainer.left = scopeObj.view.lblNotiCenterVariableReference.frame.x + 125 +"px";
      scopeObj.view.flxVariableReferenceContainer.top = (scopeObj.view.flxAddTemplate.frame.y+scopeObj.view.flxAddNotificationCenter.frame.y)-scopeObj.view.flxViewSubAlert.contentOffsetMeasured.y-79 +"px";
      if (scopeObj.view.flxVariableReferenceContainer.isVisible === false) {
        scopeObj.view.flxVariableReferenceContainer.setVisibility(true);
      } else if (scopeObj.view.flxVariableReferenceContainer.isVisible === true) {
        scopeObj.view.flxVariableReferenceContainer.setVisibility(false);
      }
      scopeObj.view.forceLayout();
    };
    this.view.lblEmailVariableReference.onTouchStart = function() {
      scopeObj.cursorPoint="Email";
      scopeObj.view.flxVariableReferenceContainer.onHover = scopeObj.onHoverEventCallback;
      scopeObj.view.flxVariableReferenceContainer.left = scopeObj.view.lblEmailVariableReference.frame.x + 125 +"px";
      scopeObj.view.flxVariableReferenceContainer.top = (scopeObj.view.flxAddTemplate.frame.y+scopeObj.view.flxAddEmailTemplate.frame.y)-scopeObj.view.flxViewSubAlert.contentOffsetMeasured.y-79 +"px";
      if (scopeObj.view.flxVariableReferenceContainer.isVisible === false) {
        scopeObj.view.flxVariableReferenceContainer.setVisibility(true);
      } else if (scopeObj.view.flxVariableReferenceContainer.isVisible === true) {
        scopeObj.view.flxVariableReferenceContainer.setVisibility(false);
      }
      scopeObj.view.forceLayout();
    };
    this.view.flxAlertPreviewClick.onClick = function() {
      if (scopeObj.populateAlertPreview()) {
        scopeObj.view.flxAlertPreview.setVisibility(true);
      }
    };
    this.view.flxAlertPreviewClose.onClick = function() {
      scopeObj.view.flxAlertPreview.setVisibility(false);
    };
    this.view.dataEntryAlertName.tbxData.onKeyUp = function(){
      scopeObj.view.dataEntryAlertName.lblTextCounter.setVisibility(true);
      scopeObj.view.dataEntryAlertName.lblTextCounter.text = scopeObj.view.dataEntryAlertName.tbxData.text.length + "/50";
      scopeObj.clearValidationsForAddAlertGroup(1, scopeObj.view.dataEntryAlertName.tbxData, scopeObj.view.dataEntryAlertName.flxError);
    };
    this.view.dataEntryAlertName.tbxData.onEndEditing = function(){
      scopeObj.view.dataEntryAlertName.lblTextCounter.setVisibility(false);
    };
    this.view.lstBoxAddAlertTypeCode.onSelection = function(){
      var selKey = scopeObj.view.lstBoxAddAlertTypeCode.selectedKey;
      var check = scopeObj.checkForAlertCodeExsist(selKey);
      if(!check && scopeObj.alertGroupCurrAction === scopeObj.alertGroupActionConfig.CREATE){
        scopeObj.view.lstBoxAddAlertTypeCode.skin = "redListBxSkin";
        scopeObj.view.flxAddAlertTypeCodeError.setVisibility(true);
        scopeObj.view.lblErrMsgAlertTypeCode.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Alert_group_code_already_exist");
      } else{
        scopeObj.view.lstBoxAddAlertTypeCode.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
        scopeObj.view.flxAddAlertTypeCodeError.setVisibility(false);
      }
    };
    this.view.dataEntryCode.tbxData.onKeyUp = function(){
      scopeObj.view.dataEntryCode.lblTextCounter.setVisibility(true);
      scopeObj.view.dataEntryCode.lblTextCounter.text = scopeObj.view.dataEntryCode.tbxData.text.length + "/20";
      scopeObj.clearValidationsForAddAlertGroup(1,scopeObj.view.dataEntryCode.tbxData,scopeObj.view.dataEntryCode.flxError);
    };
    this.view.dataEntryCode.tbxData.onEndEditing = function(){
      scopeObj.view.dataEntryCode.lblTextCounter.setVisibility(false);
    };
    this.view.tbxEditAlertMainName.onKeyUp = function() {
      scopeObj.view.flxNoEditAlertMainNameError.setVisibility(false);
      scopeObj.view.tbxEditAlertMainName.skin = "skntbxLato35475f14px";
      if (scopeObj.view.tbxEditAlertMainName.text.trim().length === 0) {
        scopeObj.view.lblAlertTypeNameCount.setVisibility(false);
      } else {
        scopeObj.view.lblAlertTypeNameCount.setVisibility(true);
        scopeObj.view.lblAlertTypeNameCount.skin = "sknlblLatoBold485c7512px";
        scopeObj.view.lblAlertTypeNameCount.text = scopeObj.view.tbxEditAlertMainName.text.length + "/30";
      }
    };
    this.view.txtareaEditAlertMainDescription.onKeyUp = function() {
      scopeObj.view.flxNoEditAlertMainDescriptionError.setVisibility(false);
      scopeObj.view.txtareaEditAlertMainDescription.skin = "skntxtAreaLato35475f14Px";
      if (scopeObj.view.txtareaEditAlertMainDescription.text.trim().length === 0) {
        scopeObj.view.lblAlertTypeDescriptionCount.setVisibility(false);
      } else {
        scopeObj.view.lblAlertTypeDescriptionCount.setVisibility(true);
        scopeObj.view.lblAlertTypeDescriptionCount.skin = "sknlblLatoBold485c7512px";
        scopeObj.view.lblAlertTypeDescriptionCount.text = scopeObj.view.txtareaEditAlertMainDescription.text.length + "/250";
      }
    };
    this.view.tbxEditAlertName.onKeyUp = function() {
      scopeObj.view.flxNoEditAlertNameError.setVisibility(false);
      scopeObj.view.tbxEditAlertName.skin = "skntbxLato35475f14px";
      if (scopeObj.view.tbxEditAlertName.text.trim().length === 0) {
        scopeObj.view.lblEditAlertNameCount.setVisibility(false);
      } else {
        scopeObj.view.lblEditAlertNameCount.setVisibility(true);
        scopeObj.view.lblEditAlertNameCount.skin = "sknlblLatoBold485c7512px";
        scopeObj.view.lblEditAlertNameCount.text = scopeObj.view.tbxEditAlertName.text.length + "/50";
      }
    };
    this.view.txtareaEditAlertDescription.onKeyUp = function() {
      scopeObj.view.flxNoEditAlertDescriptionError.setVisibility(false);
      scopeObj.view.txtareaEditAlertDescription.skin = "skntxtAreaLato35475f14Px";
      if (scopeObj.view.txtareaEditAlertDescription.text.trim().length === 0) {
        scopeObj.view.lblEditAlertDescriptionCount.setVisibility(false);
      } else {
        scopeObj.view.lblEditAlertDescriptionCount.setVisibility(true);
        scopeObj.view.lblEditAlertDescriptionCount.skin = "sknlblLatoBold485c7512px";
        scopeObj.view.lblEditAlertDescriptionCount.text = scopeObj.view.txtareaEditAlertDescription.text.length + "/250";
      }
    };
    this.view.txtareaEditAlertAlertContent.onKeyUp = function() {
      scopeObj.view.flxNoEditAlertAlertContentError.setVisibility(false);
      scopeObj.view.txtareaEditAlertAlertContent.skin = "skntxtAreaLato35475f14Px";
      if (scopeObj.view.txtareaEditAlertAlertContent.text.trim().length === 0) {
        scopeObj.view.lblEditAlertContentCount.setVisibility(false);
      } else {
        scopeObj.view.lblEditAlertContentCount.setVisibility(true);
        scopeObj.view.lblEditAlertContentCount.skin = "sknlblLatoBold485c7512px";
        scopeObj.view.lblEditAlertContentCount.text = scopeObj.view.txtareaEditAlertAlertContent.text.length + "/250";
      }
    };
    this.view.breadcrumbs.btnBackToMain.onClick = function() {
      scopeObj.view.flxAlertsBreadCrumb.setVisibility(false);
      scopeObj.view.flxViewAlertData.setVisibility(false);
      scopeObj.view.flxEditAlertMain.setVisibility(false);
      scopeObj.view.flxEditAlertInner.setVisibility(false);
      scopeObj.view.flxEditAlert.setVisibility(false);
      scopeObj.view.flxAlertsMain.setVisibility(true);
      scopeObj.view.mainHeader.lblHeading.text = scopeObj.sAlerts;
      scopeObj.presenter.fetchAlertAndAlertTypes(kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.fetch"));
    };
    this.view.breadcrumbs.btnPreviousPage.onClick = function() {
      scopeObj.view.flxAlertsBreadCrumb.setVisibility(false);
      scopeObj.view.flxViewAlertData.setVisibility(false);
      scopeObj.view.flxEditAlertMain.setVisibility(false);
      scopeObj.view.flxEditAlertInner.setVisibility(false);
      scopeObj.view.flxEditAlert.setVisibility(false);
      scopeObj.view.flxAlertsMain.setVisibility(true);
      scopeObj.presenter.fetchAlertAndAlertTypes(kony.i18n.getLocalizedString("i18n.frmPoliciesController.edit"));
      scopeObj.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Alerts");
    };
    this.view.flxOption1.onClick = function() {
      scopeObj.alertGroupCurrAction = scopeObj.alertGroupActionConfig.EDIT;
      scopeObj.alertGroupScreenContext = 2;
      scopeObj.view.flxContextualMenu.isVisible = false;
      scopeObj.toggleBreadcrumbButtons("alertgroup");
      scopeObj.view.flxAlertCategoryDetailsScreen.setVisibility(false);
      var ind = scopeObj.view.segListing.selectedRowIndex[1];
      var alertGroupId = scopeObj.view.segListing.data[ind].lblAlertCode.info.id;
      var alertGroupName = scopeObj.view.segListing.data[ind].lblAlertName;
      scopeObj.presenter.getAlertTypeDetails({"AlertTypeId":alertGroupId},"edit");
      scopeObj.view.flxAlertTypesContextualMenu.setVisibility(false);
      scopeObj.view.breadcrumbAlerts.lblCurrentScreen.text = alertGroupName.toUpperCase();
      scopeObj.initializeCreateData();
    };	
    this.view.flxOption2.onClick = function() {
      scopeObj.view.flxContextualMenu.isVisible = false;
      scopeObj.alertGroupScreenContext = 1;
      var ind = scopeObj.view.segListing.selectedRowIndex[1];
      var alertGroupId = scopeObj.view.segListing.data[ind].lblAlertCode.info.id;
      scopeObj.presenter.getAlertTypeDetails({"AlertTypeId":alertGroupId},"statusChange");
    };
    this.view.flxSmsCheckBox.onClick = function() {
      scopeObj.changingCheckBoxImg(0);
    };
    this.view.flxEmailCheckBox.onClick = function() {
      scopeObj.changingCheckBoxImg(1);
    };
    this.view.flxPushCheckBox.onClick = function() {
      scopeObj.changingCheckBoxImg(2);
    };
    this.view.flxAllCheckBox.onClick = function() {
      scopeObj.changingCheckBoxImg(3);
    };

    this.view.subHeader.tbxSearchBox.onKeyUp = function() {
      scopeObj.loadPageData(scopeObj.recentTabData.data.Alerts.map(scopeObj.mappingAlertSegmentData.bind(scopeObj)));
      if (scopeObj.records === 0) {
        var text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found") +
            "\"" + scopeObj.view.subHeader.tbxSearchBox.text + "\"" +
            kony.i18n.getLocalizedString("i18n.frmPoliciesController.Try_with_another_keyword");
        scopeObj.showNoResultsFound(text);
        scopeObj.view.forceLayout();
      } else {
        scopeObj.hideNoResultsFound();
        scopeObj.view.forceLayout();
      }
      if (scopeObj.view.subHeader.tbxSearchBox.text === "") {
        scopeObj.searchFlag = false;
        scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
      } else {
        scopeObj.searchFlag = true;
        scopeObj.view.subHeader.flxClearSearchImage.setVisibility(true);
      }
    };

    this.view.subHeader.flxClearSearchImage.onClick = function() {
      scopeObj.searchFlag = false;
      scopeObj.view.subHeader.tbxSearchBox.text = "";
      scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
      scopeObj.hideNoResultsFound();
      scopeObj.assingAlertsData(scopeObj.recentTabData.data.Alerts);
      scopeObj.filterIndiciesArray.channel = [0, 1, 2];
      scopeObj.filterIndiciesArray.status = [0, 1];
    };

    this.view.flxNameContainer.onClick = function() {
      scopeObj.sortBy.column("lblHeaderName");
      scopeObj.loadPageData(scopeObj.view.segListing.data);
      scopeObj.resetSortImages("lblHeaderName");
    };
    this.view.flxChannelContainer.onClick = function() {
      scopeObj.view.flxContextualMenu.isVisible = false;
      scopeObj.view.statusFilterMenu.onHover = scopeObj.onHoverEventCallback;
      scopeObj.setDatafilterMenu("channel");
      scopeObj.view.statusFilterMenu.isVisible = true;
    };

    this.view.flxStatusContainer.onClick = function() {
      scopeObj.view.flxContextualMenu.isVisible = false;
      scopeObj.view.statusFilterMenu.onHover = scopeObj.onHoverEventCallback;
      scopeObj.setDatafilterMenu("status");
      scopeObj.view.statusFilterMenu.isVisible = true;
    };
    this.view.lstBoxAddAlertTypeFrequency.onSelection = function(){
      scopeObj.clearValidationsForAddAlertGroup(2, scopeObj.view.lstBoxAddAlertTypeFrequency, scopeObj.view.flxAddAlertTypeErrorFrequency);
    };
    this.view.tbxAddAlertTypeFromValue.onKeyUp = function(){
      scopeObj.clearValidationsForAddAlertGroup(1, scopeObj.view.tbxAddAlertTypeFromValue, scopeObj.view.flxErrorFromValue);
    };
    this.view.tbxAddAlertTypeToValue.onKeyUp = function(){
      scopeObj.clearValidationsForAddAlertGroup(1, scopeObj.view.tbxAddAlertTypeToValue, scopeObj.view.flxAddAlertTypeErrorToValue);
    };
    this.view.segSequenceList.onRowClick = function(){
      scopeObj.selectCurrentRow();
    };
    this.view.breadcrumbAlerts.btnPreviousPage.onClick = function(){
      scopeObj.presenter.getAlertTypeDetails({
        "AlertTypeId":scopeObj.view.breadcrumbAlerts.btnPreviousPage.info.id
      },"viewDetails");
    };
    this.view.breadcrumbAlerts.btnBackToMain.onClick = function(){
      scopeObj.presenter.fetchAlertCategory(scopeObj.view.breadcrumbAlerts.btnBackToMain.info.id);
    };
  },
  /*
   * call to fetch all masterdata
   */
  getMasterData :  function(){
    var self = this;
    self.presenter.fetchMasterdata();
  },
  /*
   * To check if titles/subjects are filled when there is content in the description boxes
   */
  validateTitles : function(){
    var errorFlag=true;
    var emailEditorDocument = document.getElementById("iframe_rtxEmailTemplate").contentWindow.document;
    if(this.view.flxAddPushNotification.isVisible&&this.view.txtPushNotification.text!==""&&this.view.tbxPushNotificationTitle.text===""){
      errorFlag=false;
      this.view.lblNoPushNotificationError.text=kony.i18n.getLocalizedString("i18n.AlertsManagement.pushNotTitleEmpty");
      this.view.flxNoPushNotification.setVisibility(true);
    }else
      this.view.flxNoPushNotification.setVisibility(false);
    if(this.view.flxAddNotificationCenter.isVisible&&this.view.tbxNotiCenterTitle.text===""&&this.view.txtNotiCenterMsg.text!==""){
      errorFlag=false;
      this.view.lblNoNotiCenterError.text=kony.i18n.getLocalizedString("i18n.AlertsManagement.notifCenterTitleEmpty");
      this.view.flxNoNotiCenterMsgError.setVisibility(true);
    }else
      this.view.flxNoNotiCenterMsgError.setVisibility(false);
    if(this.view.flxAddEmailTemplate.isVisible&&this.view.tbxEmailSubject.text===""&&emailEditorDocument.getElementById("editor").innerHTML.length>0){      
      errorFlag=false;
      this.view.lblNoSubjectError.text=kony.i18n.getLocalizedString("i18n.AlertsManagement.emailSubjectEmpty");
      this.view.flxNoEmailSubject.setVisibility(true);
    }else
      this.view.flxNoEmailSubject.setVisibility(false);
    return errorFlag;
  },
  /*
   * show add/edit alert group screen
   */
  showAddAlertGroupScreen :  function(){
    this.clearValidationsForAddAlertGroup();
    this.view.flxAlertTypes.setVisibility(true);
    this.view.flxAddAlertTypeScreen.setVisibility(true);
    this.view.flxAlertTypeDetailsScreen.setVisibility(false);
    this.view.flxAlertCategories.setVisibility(false);
    this.view.flxSubAlerts.setVisibility(false);
    this.view.flxBreadcrumbAlerts.setVisibility(true);
    this.view.lstBoxAddAlertTypeCode.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
    this.view.lstBoxAddAlertTypeCode.setEnabled(true);
    this.view.breadcrumbAlerts.btnBackToMain.text = this.view.lblViewDetailsCategoryDisplayName.text.toUpperCase();
    this.toggleBreadcrumbButtons("alertgroup");
    this.view.flxAddAlertTypeScreen.setContentOffset({y:0,x:0});
    if(this.alertGroupCurrAction === this.alertGroupActionConfig.CREATE){
      this.view.addAlertTypeButtons.btnSave.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.SAVE");
    }else if(this.alertGroupCurrAction === this.alertGroupActionConfig.EDIT){
      this.view.addAlertTypeButtons.btnSave.text = this.sUpdateCaps;
    }
    this.view.forceLayout();
  },
  /*
   * hide add/edit alert group screen
   */
  hideAddAlertGroupScreen : function(){
    this.view.flxAlertTypes.setVisibility(false);
    this.view.flxAddAlertTypeScreen.setVisibility(false);
    this.view.flxAlertTypeDetailsScreen.setVisibility(false);
    this.view.flxAlertCategoryDetailsScreen.setVisibility(true);
    this.view.flxAlertCategories.setVisibility(true);
    this.view.flxBreadcrumbAlerts.setVisibility(false);
    this.view.breadcrumbAlerts.lblCurrentScreen.text =this.view.lblAlertTypeAlertNameValue.text.toUpperCase();
  },
  showAlertCategoryDetailScreen : function(){
    this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.AlertsDefinition");
    this.view.flxAlertCategoryDetailsScreen.setVisibility(true);
    this.view.flxAlertTypes.setVisibility(false);
    this.view.flxSubAlerts.setVisibility(false);
    this.view.flxEditAlertCategoryScreen.setVisibility(false);
    this.view.flxBreadcrumbAlerts.setVisibility(false);
    this.view.flxAlertCategoryDetailsScreen.setContentOffset({y:0,x:0});
    this.view.forceLayout();
  },
  /*
   * show alert group details screen
   */
  showAlertGroupDetailScreen :  function(){
    this.view.flxAlertTypes.setVisibility(true);
    this.view.flxAddAlertTypeScreen.setVisibility(false);
    this.view.flxAlertTypeDetailsScreen.setVisibility(true);
    this.view.flxAlertCategories.setVisibility(false);
    this.view.flxBreadcrumbAlerts.setVisibility(true);
    this.view.flxSubAlerts.setVisibility(false);
    this.view.breadcrumbAlerts.btnBackToMain.text = this.view.lblViewDetailsCategoryDisplayName.text.toUpperCase();
    this.view.breadcrumbAlerts.lblCurrentScreen.text =this.view.lblAlertTypeAlertNameValue.text.toUpperCase();
    this.toggleBreadcrumbButtons("alertgroup");
    this.view.forceLayout();
  },
  /*
   * clear add screen to defaults
   */
  clearAddAlertGroupScreen : function(){
    var self = this;
    this.view.dataEntryAlertName.tbxData.text = "";
    this.view.dataEntryAlertName.lblTextCounter.setVisibility(false);
    this.view.dataEntryAlertName.lblTextCounter.text = "0/50";
    this.view.dataEntryCode.tbxData.text = "";
    this.view.dataEntryCode.lblTextCounter.setVisibility(false);
    this.view.dataEntryCode.lblTextCounter.text = "0/20";
    this.view.addAlertTypeStatusSwitch.switchToggle.selectedIndex = 0;
    this.view.imgCheckboxAttribute.src = self.checkboxnormal;
    this.view.flxAddAlertTypeAddAttribute.setVisibility(false);
    this.view.lstBoxAddAlertTypeAttribute.selectedKey = "select";
    this.view.lstBoxAddAlertTypeCriteria.selectedKey = "select";
    this.view.tbxAddAlertTypeFromValue.text ="";
    this.view.tbxAddAlertTypeToValue.text = "";
    this.view.lblAddAlertTypeFromValue.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Value");
    this.view.flxAddAlertTypeFromValue.setVisibility(true);
    this.view.flxAddAlertTypeToValue.setVisibility(false);
    this.view.customListboxApps.flxSegmentList.setVisibility(false);
    this.view.customListboxUsertypes.flxSegmentList.setVisibility(false);
  },
  changeActiveButtonSkin: function(selectedButton, tabHeader, tabName, tabData) {
    this.tabUtilButtonFunction([this.view.btnTabName1,
                                this.view.btnTabName2,
                                this.view.btnTabName3,
                                this.view.btnTabName4], selectedButton);
    this.setDetailData(tabHeader, tabName,tabData);
    this.assingAlertsData(tabData);
    this.toggleBreadcrumbButtons();
  },
  assingTabValues: function(data, type) {
    var self = this;
    this.view.lblOption1.text = kony.i18n.getLocalizedString("i18n.permission.Edit");
    this.view.lblOption2.text = self.sDeactivateRecord;
    this.view.lblOption3.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Delete");
    this.view.imgOption3.src = "delete_2x.png";
    this.view.imgOption1.src = "img_edit_btn.png";
    this.tab =[];
    this.tabData = data.records;
    if (data.tabs.records.length >= 1) {
      this.view.btnTabName1.setVisibility(true);
      this.tab1 = data.tabs.records[0];
      this.tab.push(this.tab1);
      if(this.tab1.alertcategory_Name !== null && this.tab1.alertcategory_Name !== undefined)
      {
        this.view.btnTabName1.text = this.tab1.alertcategory_Name.toUpperCase();
        this.view.btnTabName2.setVisibility(false);
        this.view.btnTabName3.setVisibility(false);
        this.view.btnTabName4.setVisibility(false);
      }    
    }
    if(data.tabs.records.length >= 2)
    {
      this.view.btnTabName2.setVisibility(true);
      this.tab2 = data.tabs.records[1];   
      this.tab.push(this.tab2);
      if(this.tab2.alertcategory_Name !== null && this.tab2.alertcategory_Name !== undefined)
      {

        this.view.btnTabName2.text = this.tab2.alertcategory_Name.toUpperCase();
        this.view.btnTabName3.setVisibility(false);
        this.view.btnTabName4.setVisibility(false);	
      }
    }
    if(data.tabs.records.length >= 3)
    {
      this.view.btnTabName3.setVisibility(true);
      this.tab3 = data.tabs.records[2];
      this.tab.push(this.tab3);
      if(this.tab3.alertcategory_Name !== null && this.tab3.alertcategory_Name !== undefined)
      {      
        this.view.btnTabName3.text = this.tab3.alertcategory_Name.toUpperCase();
        this.view.btnTabName4.setVisibility(false);	
      }
    }
    if(data.tabs.records.length >= 4)
    {
      this.view.btnTabName4.setVisibility(true);
      this.tab4 = data.tabs.records[3];
      this.tab.push(this.tab4);
      if(this.tab4.alertcategory_Name !== null && this.tab4.alertcategory_Name !== undefined)
      {
        this.view.btnTabName4.text = this.tab4.alertcategory_Name.toUpperCase();
      }
    }
    this.view.flxAlertBoxContainer.setVisibility(true);
    this.view.btnReorderCategories.setVisibility(true);
    this.changeActiveButtonSkin(this.view.btnTabName1, this.tab1, "tab1",this.tabData);    
    //this.setVariableReferenceSegmentData(data.variableReference);
  },
  setDetailData: function(tabHeader, tabName, tabData) {
    this.view.lblViewDetailsCategoryDisplayName.text = tabData.categoryDefintion.displayName;
    this.view.lblCategoryCodeValue.text = tabData.categoryDefintion.categoryCode;
    this.view.lblCategoryChannelValue.text = "";
    var str = "";
    this.supportedChannels=[];
    for (var i = 0; i < tabData.categoryChannels.length; i++) {
      if (tabData.categoryChannels[i].isChannelSupported === "true") {
        str += tabData.categoryChannels[i].channelDisplayName + ", ";
        this.supportedChannels.push(tabData.categoryChannels[i].channelDisplayName);
      }
    }
    if (str.substr(str.length-2,2) == ", ") {
      str = str.substring(0,str.length-2);
    }
    this.view.lblCategoryChannelValue.text = str || this.sNA;
    this.view.lblCategoryAccountLevelValue.text = 
      (tabData.categoryDefintion.containsAccountLevelAlerts === "true") ?
      "Yes" : "No";
    if (tabData.categoryDefintion.categoryStatus === "SID_ACTIVE") {
      this.view.lblViewDetailsCategoryStatus.text = this.sActive;
      this.view.lblViewDetailsCategoryStatus.skin = "sknlblLato5bc06cBold14px";
      this.view.lblIconViewDetailsCategoryStatus.skin = "sknIcon13pxGreen";
      this.view.flxAlertCategoryMessage.setVisibility(false);
      this.view.flxAlertTypeMessage.setVisibility(false);
      this.view.flxAlertMessage.setVisibility(false);
    } else {
      this.view.lblViewDetailsCategoryStatus.text = this.sInActive;
      this.view.lblViewDetailsCategoryStatus.skin = "sknlblLatoDeactive";
      this.view.lblIconViewDetailsCategoryStatus.skin = "sknIcon13pxGray";
      this.view.flxAlertCategoryMessage.setVisibility(true);
      this.view.flxAlertTypeMessage.setVisibility(true);
      this.view.flxAlertMessage.setVisibility(true);
      this.view.alertCategoryMessage.lblData.text = this.sAlertCategoryInactive;
      this.view.alertTypeMessage.lblData.text = this.sAlertCategoryInactive;
      this.view.alertMessage.lblData.text = this.sAlertCategoryInactive;
    }
    tabHeader.alertcategory_status_id = tabData.categoryDefintion.categoryStatus;
    this.view.lblCategoryDescValue.text = tabData.categoryDefintion.categoryDescription;
    this.view.breadcrumbAlerts.btnBackToMain.info = {"id":tabData.categoryDefintion.categoryCode};
    this.recentTabData = {
      "header": tabHeader,
      "data": tabData,
      "tabName": tabName
    };
  },
  assingAlertsData: function(data) {
    var scopeObj = this;
    scopeObj.view.lblSortName.text = "?";
    var finalData = data.alertGroups.map(scopeObj.mappingAlertSegmentData.bind(scopeObj));
    scopeObj.setAlertSegmentdata(finalData);
  },    
  /** returns channel string based on input type
   * @param {object} data -  object describes which channel is active 
   * @param {string} data.IsSmsActive - "true"/"false" value
   * @param {string} data.IsEmailActive - "true"/"false" value
   * @param {string} data.IsPushActive - "true"/"false" value
   */
  getChannelString: function(data) {
    var channels = "";
    if (data.IsSmsActive === "true") {
      channels = channels + "SMS";
    }
    if (data.IsEmailActive === "true") {
      channels = channels !== "" ? 
        channels + kony.i18n.getLocalizedString("i18n.frmAlertsManagementController._Email") : "Email";
    }
    if (data.IsPushActive === "true") {
      channels = channels !== "" ? 
        channels + kony.i18n.getLocalizedString("i18n.frmAlertsManagementController._Push") :
      kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Push");
    }
    return channels;
  },
  /*
   * toggle additional attributes based on selection
   */
  toggleAttributes :  function(){
    var self = this;
    if(self.view.imgCheckboxAttribute.src === self.checkboxnormal){
      self.view.imgCheckboxAttribute.src = self.checkboxselected;
      self.view.flxAddAlertTypeAddAttribute.setVisibility(true);
    }else{
      self.view.imgCheckboxAttribute.src = self.checkboxnormal;
      self.view.flxAddAlertTypeAddAttribute.setVisibility(false);
    }
    self.view.forceLayout();
  },
  /*
   * function to populate attributes names in the listbox
   * @param: attribute list
   */
  setAttributes :  function(list){
    var self =this;
    var attributeList = [];
    attributeList = list.reduce(
      function (list, record) {
        if(record.id === "ALERT_ATTRIBUTE_FREQUENCY"){
          self.setFrequency(record.values);
        }
        return list.concat([[record.alertattribute_id, record.alertattribute_name]]);
      }, [["select", "Select"]]);
    self.view.lstBoxAddAlertTypeAttribute.masterData = attributeList;
    self.view.lstBoxAddAlertTypeAttribute.selectedKey = "select";
  },
  /*
   * function to populate criteria in the listbox
   * @param: criteria list
   */
  setCriteria :  function(list){
    var self =this;
    var criteriaList = [];
    criteriaList = list.reduce(
      function (list, record) {
        return list.concat([[record.id, record.Name]]);
      }, [["select", "Select"]]);
    self.view.lstBoxAddAlertTypeCriteria.masterData = criteriaList;
    self.view.lstBoxAddAlertTypeCriteria.selectedKey = "select";
  },
  /*
   * function to populate frequency in the listbox
   * @param: frequency values list
   */
  setFrequency :  function(list){
    var self =this;
    var frequencyList = [];
    frequencyList = list.reduce(
      function (list, record) {
        return list.concat([[record.alertattributelistvalues_id, record.alertattributelistvalues_name]]);
      }, [["select", "Select"]]);
    self.view.lstBoxAddAlertTypeFrequency.masterData = frequencyList;
    self.view.lstBoxAddAlertTypeFrequency.selectedKey = "select";
  },
  /*
   * function to populate languages in the listbox
   * @param: languages list
   */
  setLanguages : function(data){
    var self = this;
    var langList = [];
    langList = data.reduce(
      function (list, record) {
        return list.concat([[record.Code, record.Language]]);
      }, [["select", "Select"]]);
    self.listBoxDataLang = langList;
    self.langToShow = langList.splice(1,self.listBoxDataLang.length-1);
  },
  /*
   * function to populate alert group codes in the listbox
   * @param: alert group codes list
   */
  setAlertGroupCodes : function(data){
    var self = this;
    var codeList = [];
    codeList = data.eventTypes.reduce(
      function (list, record) {
        return list.concat([[record.id, record.id]]);
      }, [["select", "Select"]]);
    self.view.lstBoxAddAlertTypeCode.masterData = codeList;
    self.view.lstBoxAddAlertTypeCode.selectedKey = "select";
  },
  /*
   * initialize required data for add alert group
   */
  initializeCreateData : function(){
    var self =this;
    self.clearAddAlertGroupScreen();
    var data = [{"selectedImg":self.radioSelected,"unselectedImg":self.radioNotSelected,"src":self.radioNotSelected,"value":"Global Alert","id":"GA"},
                {"selectedImg":self.radioSelected,"unselectedImg":self.radioNotSelected,"src":self.radioNotSelected,"value":"User Alert","id":"UA"},];
    self.view.customRadioButtonType.setData(data);
    self.view.lstBoxAddAlertTypeCode.selectedKey = "select";
    self.view.customListboxApps.lblSelectedValue.text =self.sAll;
    self.view.customListboxUsertypes.lblSelectedValue.text = self.sAll;
    self.onClickOfSelectAll("apps");
    self.onClickOfSelectAll("users");
    if(self.alertGroupCurrAction === self.alertGroupActionConfig.CREATE)
      self.addDefaultRowDataForLangSeg();

  },
  mappingCategorySegmentSequenceData: function(data) {
    return {
      "id": data.alertcategory_id,
      "lblName": data.alertcategory_Name,
      "lblSeqNum": data.alertcategory_DisplaySequence,
      "flxAlertSequenceMap":{"skin":"sknbackGroundffffff100"},
      "template": "flxAlertSequenceMap",
    };
  },
  setCategorySegmentSequenceData: function(data) {

    this.view.segSequenceList.setVisibility(true);
    this.view.lblSubHeaderSequencePopUp.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.ReorderAlertCategory");
    var widgetMap = {
      "id":"id",
      "lblName": "lblName",
      "lblSeqNum": "lblSeqNum",
      "flxAlertSequenceMap" :"flxAlertSequenceMap"
    };
    this.sortBy = this.getObjectSorter("lblSeqNum");
    this.view.segSequenceList.widgetDataMap = widgetMap;
    //align position of buttons flex based on scrollbar visibility
    if(data.length > 6){
      this.view.flxAlertSequenceIcons.right = "37dp";
    }else{
      this.view.flxAlertSequenceIcons.right = "19dp";
    }
    if(data.length === 1) this.enableDisableAllArrows(0);
    this.view.segSequenceList.setData(data.sort(this.sortBy.sortData));
    this.view.flxSequencePopUp.forceLayout();
  }, 
  mappingAlertSegmentSequenceData: function(data) {
    return {
      "id": data.code,
      "lblName": data.name,
      "lblSeqNum": data.displaySequence,
      "flxAlertSequenceMap":{"skin":"sknbackGroundffffff100"},
      "template": "flxAlertSequenceMap",
    };
  },
  setAlertSegmentSequenceData: function(data) {
    if (data.length <= 0) {
      this.view.lblSubHeaderSequencePopUp.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.NoAssociatedAlerts");
      this.view.segSequenceList.setVisibility(false);
    } else {
      this.view.segSequenceList.setVisibility(true);
      this.view.lblSubHeaderSequencePopUp.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.ReorderAlertGroup");
    }
    var widgetMap = {
      "id":"id",
      "lblName": "lblName",
      "lblSeqNum": "lblSeqNum",
      "flxAlertSequenceMap" :"flxAlertSequenceMap"
    };
    this.sortBy = this.getObjectSorter("lblSeqNum");
    this.view.segSequenceList.widgetDataMap = widgetMap;
    //align position of buttons flex based on scrollbar visibility
    if(data.length > 6){
      this.view.flxAlertSequenceIcons.right = "37dp";
    }else{
      this.view.flxAlertSequenceIcons.right = "19dp";
    }
    if(data.length === 1) this.enableDisableAllArrows(0);
    this.view.segSequenceList.setData(data.sort(this.sortBy.sortData));
    this.view.flxSequencePopUp.forceLayout();
  }, 
  moveBottom :function(ind)
  {
    var self = this;
    var rowData = self.view.segSequenceList.data[ind];
    if(ind < self.view.segSequenceList.data.length - 1){
      self.view.segSequenceList.addDataAt(rowData, self.view.segSequenceList.data.length);
      self.view.segSequenceList.removeAt(ind);
      self.reorderAlertIndex = self.view.segSequenceList.data.length-1;
      self.disableArrowsSequencePopup(self.reorderAlertIndex);
    }
  },
  moveTop: function(ind)
  {
    var self = this;
    var rowData = self.view.segSequenceList.data[ind];
    if(ind >0){
      self.view.segSequenceList.addDataAt(rowData, 0);
      self.view.segSequenceList.removeAt(ind+1);
      self.reorderAlertIndex = 0;
      self.disableArrowsSequencePopup(self.reorderAlertIndex);
    }
  },
  moveDown: function(ind)
  {
    var self =this;
    var rowData = self.view.segSequenceList.data[ind];
    var rowData2 = self.view.segSequenceList.data[ind+1];
    if(ind < self.view.segSequenceList.data.length - 1)
    {
      self.view.segSequenceList.removeAt(ind+1);
      self.view.segSequenceList.removeAt(ind);
      self.view.segSequenceList.addDataAt(rowData2,ind);
      self.view.segSequenceList.addDataAt(rowData,ind+1);
      self.reorderAlertIndex = ind+1;
      self.disableArrowsSequencePopup(self.reorderAlertIndex);
    }
  },
  moveUp: function(ind)
  {
    var self = this;
    var rowData = self.view.segSequenceList.data[ind];
    if (ind > 0) {
      self.view.segSequenceList.removeAt(ind);
      self.view.segSequenceList.addDataAt(rowData, ind - 1);
      self.reorderAlertIndex = ind-1;
      self.disableArrowsSequencePopup(self.reorderAlertIndex);
    }
  },
  /*
   * function to disable arrows
   * @param: row position
   */
  disableArrowsSequencePopup : function(rowInd){
    var self = this;
    var segLength = self.view.segSequenceList.data.length;
    if(rowInd <= 0){
      self.enableDisableAllArrows(1);
      //disable skins
      self.view.flxArrowTopMostPosition.hoverSkin ="sknCursorDisabled";
      self.view.flxArrowTopPosition.hoverSkin ="sknCursorDisabled";
      self.view.lblArrowTopMostPosition.skin ="sknIconC0C0C020px";
      self.view.lblArrowTopPosition.skin="sknIconC0C0C014px";
    }else if(rowInd >= segLength-1){
      self.enableDisableAllArrows(1);
      //disable skins
      self.view.flxArrowBottomPosition.hoverSkin ="sknCursorDisabled";
      self.view.flxArrowBottomMostPosition.hoverSkin ="sknCursorDisabled";
      self.view.lblArrowBottomPosition.skin="sknIconC0C0C014px";
      self.view.lblArrowBottomMostPosition.skin="sknIconC0C0C020px";
    } else{
      self.enableDisableAllArrows(1);
    }
    self.view.forceLayout();
  },
  /*
   * function to enable all the arrow's skins
   * @param: status (0-disable,1-enable)
   */
  enableDisableAllArrows : function(status){
    var self =this;
    if(status === 1){ //enabled state
      self.view.flxArrowTopMostPosition.hoverSkin ="sknCursor";
      self.view.flxArrowTopPosition.hoverSkin ="sknCursor";
      self.view.flxArrowBottomPosition.hoverSkin ="sknCursor";
      self.view.flxArrowBottomMostPosition.hoverSkin ="sknCursor";

      self.view.lblArrowTopMostPosition.skin ="sknIcon20px";
      self.view.lblArrowTopPosition.skin="sknIcon00000014px";
      self.view.lblArrowBottomPosition.skin="sknIcon00000014px";
      self.view.lblArrowBottomMostPosition.skin="sknIcon20px";
    } else if(status === 0){   //disabled state
      self.view.flxArrowTopMostPosition.hoverSkin ="sknCursorDisabled";
      self.view.flxArrowTopPosition.hoverSkin ="sknCursorDisabled";
      self.view.flxArrowBottomPosition.hoverSkin ="sknCursorDisabled";
      self.view.flxArrowBottomMostPosition.hoverSkin ="sknCursorDisabled";

      self.view.lblArrowTopMostPosition.skin ="sknIconC0C0C020px";
      self.view.lblArrowTopPosition.skin="sknIconC0C0C014px";
      self.view.lblArrowBottomPosition.skin="sknIconC0C0C014px";
      self.view.lblArrowBottomMostPosition.skin="sknIconC0C0C020px";
    }
  },
  mappingAlertSegmentData: function(data) {
    var scopeObj = this;
    return {
      "alertSeq": data.displaySequence,
      "lblAlertName": data.name,
      "lblAlertCode": {"tooltip": data.code,
                       "info":{"id":data.code},
                       "text": scopeObj.AdminConsoleCommonUtils.getTruncatedString(data.code, 25, 22)},
      "lblAlertType": data.type,
      "lblAlertStatus": {
        "text": data.statusId === "SID_ACTIVE" ? scopeObj.sActive : scopeObj.sInActive,
        "skin": data.statusId === "SID_ACTIVE" ? "sknlblLato5bc06cBold14px" : "sknlblLatocacacaBold12px",
      },
      "lblOptions": {
        "text": "\ue91f",
        "skin": "sknFontIconOptionMenu"
      },
      "lblAlertIconStatus": {
        "text": "\ue921",
        "skin": data.statusId === "SID_ACTIVE" ? "sknIcon13pxGreen" : "sknIcon13pxGray",
      },
      "flxOptions": {
        "onClick": function() {
          scopeObj.toggleContextualMenu(60);
        }
      },
      "template": "flxAlertGroupManagement",
      "lblSeperator": "lblSeperator",            
    };
  },
  setAlertSegmentdata: function(data) {
    if (data.length <= 0) {
      this.showNoResultsFound("No associated alerts found");
    } else {
      this.hideNoResultsFound();
    }
    var scopeObj = this;
    var widgetMap = {
      "alertSeq":"alertSeq",
      "lblAlertName": "lblAlertName",	
      "lblAlertCode": "lblAlertCode",
      "lblAlertType": "lblAlertType",      
      "lblAlertStatus": "lblAlertStatus",
      "imgAlertStatus": "imgAlertStatus",
      "lblAlertIconStatus": "lblAlertIconStatus",
      "imgOptions": "imgOptions",
      "lblOptions": "lblOptions",
      "lblSeperator": "lblSeperator",
      "flxOptions": "flxOptions"
    };
    this.view.segListing.widgetDataMap = widgetMap;
    this.view.segListing.onRowClick = function() {
      var allData = scopeObj.view.segListing.data;
      var index = scopeObj.view.segListing.selectedRowIndex[1];
      var selectedRowData = allData[index];
      scopeObj.selectedAlertId = selectedRowData.lblAlertCode.info.id;
      scopeObj.view.breadcrumbAlerts.btnPreviousPage.info = {"id":selectedRowData.lblAlertCode.info.id};
      scopeObj.presenter.getAlertTypeDetails({
        "AlertTypeId": selectedRowData.lblAlertCode.info.id
      },"viewDetails");            
    };

    this.sortBy = this.getObjectSorter("alertSeq");
    this.view.segListing.setData(data.sort(this.sortBy.sortData));
    this.view.flxNoAlertGroups.setVisibility(data.length <= 0);
    this.view.flxReorderAlertBtn.setVisibility(data.length > 0);
    this.view.flxAlertListSegment.setVisibility(data.length > 0);
    this.view.flxAlertListSegHeader.setVisibility(data.length > 0);
    this.view.flxAlertListSegment.forceLayout();
    this.view.flxRightPanel.setVisibility(true);
  },
  contextualMenuOff: function(context) {
    this.view.flxContextualMenu.isVisible = false;
  },
  searchFilter: function(serviceData) {
    var searchText = this.view.subHeader.tbxSearchBox.text;
    if (typeof searchText === "string" && searchText.length > 0) {
      return (serviceData.lblHeaderName.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
    } else {
      return true;
    }
  },
  showNoResultsFound: function(text) {
    this.view.segListing.setVisibility(false);
    this.view.flxListingSegmentWrapper.setVisibility(true);
    this.view.rtxNoResultFound.text = text;
    this.view.flxAlertListSegHeader.setVisibility(false);
  },
  hideNoResultsFound: function() {
    this.view.segListing.setVisibility(true);
    this.view.flxListingSegmentWrapper.setVisibility(false);
    this.view.flxAlertListSegHeader.setVisibility(true);
  },
  changeLableStatus: function() {
    if (this.view.lblIconDescription.text === "\ue915") {
      this.view.lblDescriptionText.isVisible = false;
      this.view.flxSearch.top = "25px";
      this.view.lblIconDescription.text = "\ue922";
      this.view.lblIconDescription.skin = "sknfontIconDescRightArrow14px";
    } else {
      this.view.lblDescriptionText.isVisible = true;
      this.view.flxSearch.top = "0px";
      this.view.lblIconDescription.text = "\ue915";
      this.view.lblIconDescription.skin = "sknfontIconDescDownArrow12px";
    }
  },
  cancelDeleteAlert: function() {
    var scopeObj = this;
    this.view.flxDeleteAlert.setVisibility(false);
    if (scopeObj.view.popUpDeactivate.lblPopUpMainMessage.text === scopeObj.sDeactivateSubStatus) {
      scopeObj.view.alertSubscriptionSwitch.switchToggle.selectedIndex = 0;
    } else if (scopeObj.view.popUpDeactivate.lblPopUpMainMessage.text === scopeObj.sActiveSubStatus) {
      scopeObj.view.alertSubscriptionSwitch.switchToggle.selectedIndex = 1;
    } else if (scopeObj.view.popUpDeactivate.lblPopUpMainMessage.text === scopeObj.sDeactivateAlertStatus) {
      this.view.customServiceStatusSwitch.switchToggle.selectedIndex = 0;
    } else if (scopeObj.view.popUpDeactivate.lblPopUpMainMessage.text === scopeObj.sActivateAlertStatus) {
      this.view.customServiceStatusSwitch.switchToggle.selectedIndex = 1;
    }
  },
  resetSortImages: function(field) {
    var self = this;
    self.determineSortFontIcon(self.sortBy, field, this.view.lblSortName);
  },
  deleteAlert: function() {
    var self  = this;
    kony.print("Inside deleteAlert() of frmAlertsManagementControler");
    this.view.flxDeleteAlert.setVisibility(false);
    var flxEditAlertVisibility = this.view.flxEditAlertInner.isVisible;
    var popUpMainMessageText = this.view.popUpDeactivate.lblPopUpMainMessage.text;
    var deactivateAlertStatusi18Value = self.sDeactivateAlertStatus;
    var activateAlertStatusi18Value = self.sActivateAlertStatus;
    if ( flxEditAlertVisibility === false && (popUpMainMessageText === deactivateAlertStatusi18Value || popUpMainMessageText === activateAlertStatusi18Value)) {
      this.view.flxDeleteAlert.isVisible = true;
      this.changeStatus(this.view.popUpDeactivate.lblPopUpMainMessage.text);
    } else if (popUpMainMessageText === self.sDeactivateSubStatus) {
      this.view.alertSubscriptionSwitch.switchToggle.selectedIndex = 1;
    } else if (popUpMainMessageText === self.sActiveSubStatus) {
      this.view.alertSubscriptionSwitch.switchToggle.selectedIndex = 0;
    } else if (this.view.flxEditAlertInner.isVisible === true && popUpMainMessageText === deactivateAlertStatusi18Value) {
      this.view.customServiceStatusSwitch.switchToggle.selectedIndex = 1;
    } else if (this.view.flxEditAlertInner.isVisible === true && popUpMainMessageText === activateAlertStatusi18Value) {
      this.view.customServiceStatusSwitch.switchToggle.selectedIndex = 0;
    } else {
      this.resetUI();      
      var allData = this.view.segListing.data;
      var index = this.view.segListing.selectedRowIndex[1];
      var selectedRowData = allData[index];
      var alertJSON = {
        "alertID": selectedRowData.id
      };
      var callBackForToast = function(res) {
        if (res === self.sSuccess) {
          self.view.toastMessage.showToastMessage(kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Alert_deleted_successfully"), self);
        } else {
          self.view.toastMessage.showErrorToastMessage(kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.unable_to_delete_Alert_Message"), self);
        }
      };
      this.presenter.deleteAlert(alertJSON, callBackForToast);
    }
  },
  populateAlertDetail: function(data) {
    var self = this;
    this.view.flxAlertsMain.setVisibility(false);
    this.view.flxViewAlertData.setVisibility(true);
    var selectedRowData, allData;
    if (data.length <= 0) {
      allData = this.view.segListing.data;
      var index = this.view.segListing.selectedRowIndex[1];
      selectedRowData = allData[index];
    } else {
      var alertData = data.map(self.mappingAlertSegmentData.bind(self));
      self.setAlertSegmentdata(alertData);
      allData = this.view.segListing.data;
      selectedRowData = allData[0];
    }
    this.view.flxAlertsBreadCrumb.setVisibility(true);
    this.view.breadcrumbs.fontIconBreadcrumbsRight.setVisibility(true);
    this.view.breadcrumbs.btnPreviousPage.setVisibility(true);
    this.view.breadcrumbs.btnPreviousPage.text = this.trimmingBreadCrumbs(this.view.breadcrumbs.btnPreviousPage, this.recentTabData.data.Name.toUpperCase());
    this.view.breadcrumbs.fontIconBreadcrumbsRight2.setVisibility(true);
    this.view.breadcrumbs.lblCurrentScreen.setVisibility(true);
    this.view.breadcrumbs.lblCurrentScreen.text = this.trimmingBreadCrumbs(this.view.breadcrumbs.lblCurrentScreen, selectedRowData.lblHeaderName.toUpperCase());
    this.view.lblViewAlertTypeContent.text = this.recentTabData.data.Name;
    this.view.lblViewAlertStatusKey.text = "\ue921";
    this.view.lblViewAlertStatusKey.skin = selectedRowData.lblIconStatus.skin;
    this.view.lblViewAlertStatusValue.text = selectedRowData.lblStatus.text;
    this.view.lblViewAlertStatusValue.skin = selectedRowData.lblStatus.skin;
    this.view.lblViewAlertName.text = selectedRowData.lblHeaderName;
    this.view.lblViewAlertSubscriptionContent.text = selectedRowData.subscriptionContent;
    this.view.lblViewAlertChannelContent.text = selectedRowData.lblChannels;
    this.view.lblViewAlertDescriptionContent.text = selectedRowData.lblHeaderDescription;
    this.view.lblViewAlertContentContent.text = selectedRowData.alertContent;
  },
  resetErrorAlertType: function() {
    this.view.flxNoEditAlertMainNameError.setVisibility(false);
    this.view.tbxEditAlertMainName.skin = "skntbxLato35475f14px";
    this.view.flxNoEditAlertMainDescriptionError.setVisibility(false);
    this.view.txtareaEditAlertMainDescription.skin = "skntxtAreaLato35475f14Px";
  },
  showErrorAlertType: function() {
    var returnValue = true;
    if (this.view.tbxEditAlertMainName.text.trim() === "") {
      this.view.tbxEditAlertMainName.skin = "skinredbg";
      this.view.flxNoEditAlertMainNameError.setVisibility(true);
      returnValue = false;
    }
    if (this.view.txtareaEditAlertMainDescription.text.trim() === "") {
      this.view.txtareaEditAlertMainDescription.skin = "skinredbg";
      this.view.flxNoEditAlertMainDescriptionError.setVisibility(true);
      returnValue = false;
    }
    return returnValue;
  },
  populateAlertTypeEdit: function() {
    this.resetErrorAlertType();
    this.view.breadcrumbs.lblCurrentScreen.text = this.trimmingBreadCrumbs(this.view.breadcrumbs.lblCurrentScreen, this.recentTabData.data.Name.toUpperCase());
    this.view.tbxEditAlertMainName.text = this.recentTabData.data.Name;
    this.view.alertSubscriptionSwitch.switchToggle.selectedIndex = this.recentTabData.data.IsSubscriptionNeeded === "true" ? 0 : 1;
    this.view.txtareaEditAlertMainDescription.text = this.recentTabData.data.Description;
    this.view.lblAlertTypeNameCount.skin = "sknLblTransparent";
    this.view.lblAlertTypeDescriptionCount.skin = "sknLblTransparent";
    var getNames = function(data) {
      return {
        "name": data.Name
      };
    };
    var data = this.recentTabData.data.Alerts.map(getNames);
    if (data.length > 0) {
      var widgetData = {
        "lblAssociatedAlert": "name"
      };
      this.view.segAssociatedAlerts.widgetDataMap = widgetData;
      this.view.segAssociatedAlerts.setData(data);
      this.view.flxNoAssociatedAlerts.isVisible = false;
      this.view.segAssociatedAlerts.isVisible = true;
    } else {
      this.view.flxNoAssociatedAlerts.isVisible = true;
      this.view.segAssociatedAlerts.isVisible = false;
    }
  },
  saveEditedAlertTypeData: function() {
    kony.print("Inside saveEditedAlertTypeData() of frmAlertsManagementControler");
    var keys = [this.tab1, this.tab2, this.tab3, this.tab4];
    var alertTypeJSON = {};
    var self = this;
    for (var i = 0; i < keys.length; i++) {
      if (this.recentTabData.data.id === keys[i].id) {
        alertTypeJSON = {
          "alertTypeID": keys[i].id,
          "alertTypeName": this.view.tbxEditAlertMainName.text,
          "alertTypeDescription": this.view.txtareaEditAlertMainDescription.text,
          "isAlertSubscriptionRequired": this.view.alertSubscriptionSwitch.switchToggle.selectedIndex === 0 ? true : false
        };
        break;
      }
    }
    var callBackForToast = function(res) {
      if (res === self.sSuccess) {
        self.view.toastMessage.showToastMessage(kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Alert_type_updated_message"), self);
      } else {
        self.view.toastMessage.showErrorToastMessage(kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.unable_to_update_Alert_Message"), self);
      }
    };
    this.presenter.editAlertType(alertTypeJSON, callBackForToast);
  },
  resetErrorAlert: function() {
    this.view.flxNoEditAlertNameError.setVisibility(false);
    this.view.tbxEditAlertName.skin = "skntbxLato35475f14px";
    this.view.flxNoEditAlertDescriptionError.setVisibility(false);
    this.view.txtareaEditAlertDescription.skin = "skntxtAreaLato35475f14Px";
    this.view.flxNoEditAlertAlertContentError.setVisibility(false);
    this.view.flxNoCheckBoxSelected.setVisibility(false);
    this.view.flxEditAlertChannel.skin = "sknBorderffffff1px";
    this.view.txtareaEditAlertAlertContent.skin = "skntxtAreaLato35475f14Px";
  },
  showErrorAlert: function() {
    var returnValue = true;
    if (this.view.tbxEditAlertName.text.trim() === "") {
      this.view.flxNoEditAlertNameError.setVisibility(true);
      this.view.tbxEditAlertName.skin = "skinredbg";
      returnValue = false;
    }
    if (this.view.txtareaEditAlertDescription.text.trim() === "") {
      this.view.flxNoEditAlertDescriptionError.setVisibility(true);
      this.view.txtareaEditAlertDescription.skin = "skinredbg";
      returnValue = false;
    }
    if (this.view.txtareaEditAlertAlertContent.text.trim() === "") {
      this.view.lblNoEditAlertAlertContentError.text = 
        kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Alert_content_Message");
      this.view.flxNoEditAlertAlertContentError.setVisibility(true);
      this.view.txtareaEditAlertAlertContent.skin = "skinredbg";
      returnValue = false;
    }
    if (this.populateAlertPreview() === false) {
      returnValue = false;
    }
    if (this.view.imgSmsCheckBox.src === this.checkboxnormal
        && this.view.imgEmailCheckBox.src === this.checkboxnormal
        && this.view.imgPushCheckBox.src === this.checkboxnormal) {
      this.view.flxNoCheckBoxSelected.setVisibility(true);
      this.view.flxEditAlertChannel.skin = "sknBordereb30171px";
      returnValue = false;
    }
    return returnValue;
  },
  populateAlertData: function() {
    var self = this;
    this.resetErrorAlert();
    var allData = this.view.segListing.data;
    var index = this.view.segListing.selectedRowIndex[1];
    var selectedRowData = allData[index];
    this.view.tbxEditAlertName.text = selectedRowData.lblHeaderName;
    this.view.customServiceStatusSwitch.switchToggle.selectedIndex = selectedRowData.lblStatus.text === self.sActive ? 0 : 1;
    if (selectedRowData.SMS === "true" && selectedRowData.email === "true" && selectedRowData.push === "true") {
      this.view.imgAllCheckBox.src = self.checkboxselected;
    } else {
      this.view.imgAllCheckBox.src = self.checkboxnormal;
    }
    if (selectedRowData.SMS === "true") this.view.imgSmsCheckBox.src = self.checkboxselected;
    else this.view.imgSmsCheckBox.src = self.checkboxnormal;
    if (selectedRowData.email === "true") this.view.imgEmailCheckBox.src = self.checkboxselected;
    else this.view.imgEmailCheckBox.src = self.checkboxnormal;
    if (selectedRowData.push === "true") this.view.imgPushCheckBox.src = self.checkboxselected;
    else this.view.imgPushCheckBox.src = self.checkboxnormal;
    this.view.txtareaEditAlertDescription.text = selectedRowData.lblHeaderDescription;
    this.view.txtareaEditAlertAlertContent.text = selectedRowData.alertContent;
    this.view.lblEditAlertNameCount.skin = "sknLblTransparent";
    this.view.lblEditAlertDescriptionCount.skin = "sknLblTransparent";
    this.view.lblEditAlertContentCount.skin = "sknLblTransparent";
  },
  saveEditedAlertData: function() {

    kony.print("Inside saveEditedAlertData() of frmAlertsManagementControler");
    var alertJSON = {};
    var self = this;
    var recentdata = this.recentTabData.data.Alerts;
    var allData = this.view.segListing.data;
    var index = this.view.segListing.selectedRowIndex[1];
    var selectedRowData = allData[index];
    var selectedAlertData = recentdata.find(function(alert){
      return alert.id === selectedRowData.id;
    });
    alertJSON = {
      "alertID": selectedAlertData.id,
      "alertName": this.view.tbxEditAlertName.text,
      "alertTypeID": selectedAlertData.AlertType_id,
      "alertDescription": this.view.txtareaEditAlertDescription.text,
      "status_ID": this.view.customServiceStatusSwitch.switchToggle.selectedIndex === 0 ? "SID_ACTIVE" : "SID_INACTIVE",
      "alertContent": this.view.txtareaEditAlertAlertContent.text,
      "isAlertEnabledOn_SMS": (this.view.imgSmsCheckBox.src === self.checkboxselected).toString(),
      "isAlertEnabledOn_Email": (this.view.imgEmailCheckBox.src === self.checkboxselected).toString(),
      "isAlertEnabledOn_Push": (this.view.imgPushCheckBox.src === self.checkboxselected).toString(),
      "isAlertEnabledOn_All": (this.view.imgAllCheckBox.src === self.checkboxselected).toString()
    };
    var callBackForToast = function(res) {
      if (res === self.sSuccess) {
        self.view.toastMessage.showToastMessage(kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Alert_updated_successfully"), self);
      } else {
        self.view.toastMessage.showErrorToastMessage(kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.update_Alert_successful_Message"), self);
      }
    };
    this.presenter.editAlert(alertJSON, callBackForToast, false);
  },
  toggleContextualMenu: function(rowHeight) {
    kony.print("Inside toggleContextualMenu()");
    this.view.statusFilterMenu.isVisible = false;

    if (this.view.flxContextualMenu.isVisible === false) {
      var index = this.view.segListing.selectedIndex;
      var rowIndex = index[1];
      var templateArray = this.view.segListing.clonedTemplates;
      this.view.flxContextualMenu.setVisibility(true);
      this.view.forceLayout();
      //to caluclate top from preffered row heights
      var finalHeight = 60; //seg header height
      for(var i = 0; i < rowIndex; i++){
        finalHeight = finalHeight + templateArray[i].flxAlertGroupManagement.frame.height;
      }
      this.updateContextualMenu();
      var segmentWidget = this.view.segListing;
      var contextualWidget =this.view.flxContextualMenu;
      finalHeight = ((finalHeight + 45)- segmentWidget.contentOffsetMeasured.y);
      if(finalHeight+contextualWidget.frame.height > segmentWidget.frame.height){
        finalHeight = finalHeight - contextualWidget.frame.height - 45;
      }
      contextualWidget.top= finalHeight + "px";
      contextualWidget.setVisibility(true);
      this.view.flxContextualMenu.onHover = this.onHoverEventCallback;
      
    } else {
      this.view.flxContextualMenu.setVisibility(false);
    }
  },
  updateContextualMenu: function() {
    var self = this;
    kony.print("Inside updateContextualMenu()");
    var data = this.view.segListing.data;
    var index = this.view.segListing.selectedIndex;
    this.rowIndex = index[1];
    if (data[this.rowIndex].lblAlertStatus.text === self.sActive) {
      this.view.lblOption2.text = self.sDeactivateRecord;
      this.view.lblIconOption2.text = "\ue91c";
      this.view.lblIconOption2.skin = "sknIcon20px";
    } else {
      this.view.lblOption2.text = kony.i18n.getLocalizedString("i18n.SecurityQuestions.Activate");
      this.view.lblIconOption2.text = "\ue931";
      this.view.lblIconOption2.skin = "sknIcon20px";
    }
  },
  changingCheckBoxImg: function(index) {
    var self  = this;
    this.view.flxNoCheckBoxSelected.setVisibility(false);
    this.view.flxEditAlertChannel.skin = "sknBorderffffff1px";
    var imgArr = [this.view.imgSmsCheckBox, this.view.imgEmailCheckBox, this.view.imgPushCheckBox, this.view.imgAllCheckBox];
    var source;
    if (index !== 3) {
      if (imgArr[index].src === self.checkboxnormal) imgArr[index].src = self.checkboxselected;
      else imgArr[index].src = self.checkboxnormal;
    } else {
      if (imgArr[index].src === self.checkboxnormal) source = self.checkboxselected;
      else source = self.checkboxnormal;
      for (var i = 0; i < imgArr.length; i++) {
        imgArr[i].src = source;
      }
    }
    if (imgArr[0].src === self.checkboxselected && imgArr[1].src === self.checkboxselected && imgArr[2].src === self.checkboxselected) {
      imgArr[3].src = self.checkboxselected;
    } else {
      imgArr[3].src = self.checkboxnormal;
    }
  },
  showStatusChangePopup: function() {
    var self = this;
    this.view.flxDeleteAlert.isVisible = true;
    this.view.popUpDeactivate.btnPopUpCancel.text = self.sNoLeaveAsIs;
    this.view.popUpDeactivate.btnPopUpCancel.right = "160px";
    if (this.view.lblOption2.text === self.sDeactivateRecord) {
      this.view.popUpDeactivate.lblPopUpMainMessage.text = self.sDeactivateAlertStatus;
      this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.deactivate_alert_status_Question");
      this.view.popUpDeactivate.btnPopUpDelete.text = self.sYesDeactivate;
      this.view.popUpDeactivate.btnPopUpCancel.right = "180px";
    } else {
      this.view.popUpDeactivate.lblPopUpMainMessage.text = self.sActivateAlertStatus;
      this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.active_alert_status_Question");
      this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.YES_ACTIVATE");
    }
  },
  changeStatus: function(text) {
    this.view.flxDeleteAlert.isVisible = false;
    var self = this;
    var allData = this.view.segListing.data;
    var index = this.view.segListing.selectedRowIndex[1];
    var toastMsg = "";
    var alertJSON = {};
    var callBackForToast = function(res) {
      if (res === self.sSuccess) {
        self.view.toastMessage.showToastMessage(toastMsg, self);
      } else {
        self.view.toastMessage.showErrorToastMessage(kony.i18n.getLocalizedString("i18n.frmFAQController.unable_to_update_FAQ"), self);
      }
    };
    var selectedRowData = allData[index];
    if (text === self.sActivateAlertStatus) {
      alertJSON = {
        "alertID": selectedRowData.id,
        "status_ID": "SID_ACTIVE"
      };
      toastMsg = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Alert_activated_successfully");
    } else if (text === this.sDeactivateAlertStatus) {
      alertJSON = {
        "alertID": selectedRowData.id,
        "status_ID": "SID_INACTIVE"
      };
      toastMsg = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Deactivated_successfully");
    }
    this.presenter.editAlert(alertJSON, callBackForToast, true);
  },
  getVariableReference : function(){
    this.presenter.getVariableReferenceData();
  },
  setVariableReferenceSegmentData: function(vrData) {
    kony.print("Inside setVariableReferenceSegmentData() of frmAlertsManagementController");
    var dataMap = {
      "lbVariableReference": "lbVariableReference",
      "lbVariableReferenceValue": "lbVariableReferenceValue"
    };
    this.view.segVariableReference.widgetDataMap = dataMap;
    this.view.segVariableReference.data = [];
    var segVR = this.view.segVariableReference.data;
    for (var i = 0; i < vrData.length; ++i) {
      var toAdd = {
        "lbVariableReference": {
          "text": vrData[i].Code
        },
        "lbVariableReferenceValue": {
          "text": vrData[i].DefaultValue
        }
      };
      segVR.push(toAdd);
    }
    this.view.segVariableReference.setData(segVR);
    this.view.forceLayout();
  },
  populateAlertPreview: function(finalString) {
    var indexArr = [];
    var index = 0;
    var localIndex;
    var data = this.view.segVariableReference.data;
    for (var i = index; i < data.length; i++) {
      localIndex = finalString.indexOf("[#]" + data[i].lbVariableReference.text + "[/#]");
      if (localIndex >= 0) {
        index = localIndex;
        indexArr.push({
          "text": index,
          "varRefIndex": i
        });
      }
    }
    for (var j = 0; j < indexArr.length; j++) {
      finalString = finalString.replace("[#]" + data[indexArr[j].varRefIndex].lbVariableReference.text + "[/#]", data[indexArr[j].varRefIndex].lbVariableReferenceValue.text);
    }
    return finalString;
    //     var finalWrongString = finalString;
    //     if ((finalWrongString.indexOf("<") !== finalWrongString.lastIndexOf(">"))) {
    //       this.view.lblNoEditAlertAlertContentError.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.valid_variable_reference");
    //       this.view.flxNoEditAlertAlertContentError.setVisibility(true);
    //       this.view.txtareaEditAlertAlertContent.skin = "skinredbg";
    //       return false;
    //     } else {
    //       return true;
    //     }
  },
  setDatafilterMenu: function(headerName) {
    var data = [];
    var left = "";
    var self = this;
    if (headerName === "channel") {
      data = [{
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "checkbox.png",
        "lblDescription": "SMS"
      }, {
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "checkbox.png",
        "lblDescription": "Email"
      }, {
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "checkbox.png",
        "lblDescription": kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.Push")
      }];
      left = this.view.flxChannelContainer.frame.x - 4;
    } else if (headerName === "status") {
      data = [{
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "checkbox.png",
        "lblDescription": kony.i18n.getLocalizedString("i18n.secureimage.Active"),
        "id": "SID_ACTIVE"
      }, {
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "checkbox.png",
        "lblDescription": kony.i18n.getLocalizedString("i18n.secureimage.Inactive"),
        "id": "SID_INACTIVE"
      }];
      left = this.view.flxStatusContainer.frame.x - 25;
    }
    var widgetMap = {
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    this.view.statusFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
    this.view.statusFilterMenu.segStatusFilterDropdown.setData(data);
    this.view.statusFilterMenu.segStatusFilterDropdown.info = {
      "headerName": headerName
    };
    this.view.statusFilterMenu.left = left;
    var finalindiciesArray = headerName === "channel" ? [
      [0, self.filterIndiciesArray.channel]
    ] : [
      [0, self.filterIndiciesArray.status]
    ];
    this.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices = finalindiciesArray;
    this.view.statusFilterMenu.segStatusFilterDropdown.onRowClick = function() {
      if (headerName === "channel") self.filterIndiciesArray.channel = self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices !== null ? self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices[0][1] : [];
      else self.filterIndiciesArray.status = self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices !== null ? self.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices[0][1] : [];
      self.filterData();
    };
  },
  filterData: function() {
    var filteredData = [];
    var headerName = this.view.statusFilterMenu.segStatusFilterDropdown.info.headerName;
    var targetSegmentData;
    if (this.searchFlag === true) targetSegmentData = this.searchResentTabData;
    else targetSegmentData = this.recentTabData.data.Alerts;
    var filterSegmentData = this.view.statusFilterMenu.segStatusFilterDropdown.data;
    var filterSegmentIndexArray = this.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices !== null ? this.view.statusFilterMenu.segStatusFilterDropdown.selectedIndices[0][1] : [];
    if(filterSegmentIndexArray.length<=0){
      this.showNoResultsFound(kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.No_result_found"));
      this.view.flxSegHeader.setVisibility(true);
      return;
    }
    if (headerName === "status") {
      for (var i = 0; i < targetSegmentData.length; i++)
        for (var j = 0; j < filterSegmentIndexArray.length; j++)
          if (targetSegmentData[i].Status_id === filterSegmentData[filterSegmentIndexArray[j]].id) {
            filteredData.push(targetSegmentData[i]);
          }
    } else {
      for (var k = 0; k < targetSegmentData.length; k++)
        if ((filterSegmentIndexArray.indexOf(0) >= 0 && targetSegmentData[k].IsSmsActive === "" + (filterSegmentIndexArray.indexOf(0) >= 0)) ||
            (filterSegmentIndexArray.indexOf(1) >= 0 && targetSegmentData[k].IsEmailActive === "" + (filterSegmentIndexArray.indexOf(1) >= 0)) ||
            (filterSegmentIndexArray.indexOf(2) >= 0 && targetSegmentData[k].IsPushActive === "" + (filterSegmentIndexArray.indexOf(2) >= 0))) {
          filteredData.push(targetSegmentData[k]);
        }
    }
    if (filteredData.length > 0) {
      var alertData = filteredData.map(this.mappingAlertSegmentData.bind(this));
      this.setAlertSegmentdata(alertData);
      this.hideNoResultsFound();
    } else {
      this.showNoResultsFound(kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.No_result_found"));
    }
    this.view.flxSegHeader.setVisibility(true);
  },
  trimmingBreadCrumbs: function(widget, text) {
    var final_text = text;
    if (text.length > 30) final_text = text.substr(0, 30) + "...";
    widget.toolTip = text;
    return final_text;
  },
  getLangSegDataMapJson : function(){
    var self = this;
    var currLocale = (kony.i18n.getCurrentLocale()).replace("_","-");
    var getName = self.getLanguageNameForCode(currLocale);
    var segDataMap = {
      "lblAddAlertTypeSegLanguage":{"text":getName,
                                    "info":{"id":currLocale},
                                    "isVisible":true,
                                    "onClick":self.updateLanguagesBasedOnSelection},
      "lstBoxSelectLanguage":{"isVisible":false,
                              "onSelection":self.hideLangListBoxOnSelection,
                              "masterData":self.langToShow,
                              "selectedKey": currLocale,
                              "onHover":self.hideListBoxOnHover},
      "lblAddAlertTypeSegDisplayName":{"text":self.sAddName,
                                       "isVisible":true,
                                       "onClick":self.showLangTextBox},
      "tbxDisplayName":{"text":"",
                        "isVisible":false,
                        "onHover":self.onHoverHideTextBoxCallback,
                        "onKeyUp":self.addNewRowAtEnd},
      "lblAddAlertTypeSegDescription":{"text":self.sWriteDesc,
                                       "isVisible":true,
                                       "onClick":self.showLangTextBox},
      "lblIconDeleteLang":{"text":"\ue91b","skin":"sknIcon20px"},
      "flxDescription": {"isVisible" : false},
      "lblDescCount": {"isVisible":false},
      "tbxDescription":{"text":"",
                        "onHover":self.onHoverHideTextBoxCallback,
                        "onKeyUp":self.addNewRowAtEnd},
      "lblSeprator":"-",
      "flxDelete":{"isVisible":false, "onClick" : self.showAlertTypeDeleteLangPopup},
      "flxLanguageRowContainer":{"onHover": self.onHoverDisplayDelete},
      "template":"flxAlertsLanguageData"
    };
    return segDataMap;
  },
  /*
   * adding first default row to languages segment in add alert group
   */
  addDefaultRowDataForLangSeg : function(){
    var self = this;
    self.view.segaddalertTypeLanguages.setData([]);
    var widgetDataMap = {
      "lblAddAlertTypeSegLanguage":"lblAddAlertTypeSegLanguage",
      "lstBoxSelectLanguage":"lstBoxSelectLanguage",
      "lblAddAlertTypeSegDisplayName":"lblAddAlertTypeSegDisplayName",
      "tbxDisplayName":"tbxDisplayName",
      "lblAddAlertTypeSegDescription":"lblAddAlertTypeSegDescription",
      "tbxDescription":"tbxDescription",
      "lblSeprator":"lblSeprator",
      "flxDescription":"flxDescription",
      "lblDescCount":"lblDescCount",
      "flxDelete":"flxDelete",
      "lblIconDeleteLang":"lblIconDeleteLang",
      "flxLanguageRowContainer":"flxLanguageRowContainer",
      "flxAlertsLanguageData":"flxAlertsLanguageData"
    };
    self.view.segaddalertTypeLanguages.widgetDataMap = widgetDataMap;
    var segData = self.getLangSegDataMapJson();
    self.view.segaddalertTypeLanguages.setData([segData]);
    self.view.forceLayout();
  },
  /*
   * callback function to display languages listbox
   */
  showLangListBox :  function(event){
    var self = this;
    var ind = self.view.segaddalertTypeLanguages.selectedRowIndex[1];
    var rowData = self.view.segaddalertTypeLanguages.data[ind];
    rowData.lblAddAlertTypeSegLanguage.isVisible = false;
    rowData.lstBoxSelectLanguage.isVisible = true;
    self.view.segaddalertTypeLanguages.setDataAt(rowData, ind);
    self.view.forceLayout();
  },
  /*
   * hide the listbox on selection in languages segment
   */
  hideLangListBoxOnSelection : function(){
    var self = this;
    var ind = self.view.segaddalertTypeLanguages.selectedRowIndex[1];
    var rowData = self.view.segaddalertTypeLanguages.data[ind];
    var selectedValue = rowData.lstBoxSelectLanguage.selectedKeyValue;
    if(selectedValue[0] !== "select"){
      rowData.lblAddAlertTypeSegLanguage.text = selectedValue[1];
      rowData.lblAddAlertTypeSegLanguage.info.id = selectedValue[0];
      rowData.lblAddAlertTypeSegLanguage.isVisible = true;
      rowData.lstBoxSelectLanguage.isVisible = false;
      self.view.segaddalertTypeLanguages.setDataAt(rowData, ind);
    }
    self.view.forceLayout();
  },
  /*
   * callback function to display textbox on click of label
   */
  showLangTextBox : function(event){
    var self = this;
    var currWidgetId = event.id;
    var ind = self.view.segaddalertTypeLanguages.selectedRowIndex[1];
    var rowData = self.view.segaddalertTypeLanguages.data[ind];
    if(currWidgetId === "lblAddAlertTypeSegDisplayName"){
      rowData.tbxDisplayName.isVisible = true;
      rowData.lblAddAlertTypeSegDisplayName.isVisible = false;
    }else{
      rowData.flxDescription.isVisible = true;
      rowData.lblAddAlertTypeSegDescription.isVisible = false;
    }
    self.view.segaddalertTypeLanguages.setDataAt(rowData, ind);
    self.view.forceLayout();
  },
  /*
   * function to update languages listbox with the unselected languages list only
   */
  updateLanguagesBasedOnSelection: function() {
    var self = this;
    var assignedData = [],
        allLangId = [],diffList = [],commonList = [];
    var segData = self.view.segaddalertTypeLanguages.data;
    var currInd = self.view.segaddalertTypeLanguages.selectedRowIndex[1];
    var currRowData = self.view.segaddalertTypeLanguages.data[currInd];
    //get all assigned languges id's
    for (var i = 0; i < segData.length; i++) {
      if (segData[i].lblAddAlertTypeSegLanguage.info.id !== "" && i!== currInd ) assignedData.push(segData[i].lblAddAlertTypeSegLanguage.info.id);
    }
    //all exsisting language id's
    allLangId = self.langToShow.map(function(rec) {
      return rec[0];
    });
    //differentiate common and diff id's
    for (var j = 0; j < allLangId.length; j++) {
      if (assignedData.contains(allLangId[j])) {
        commonList.push(allLangId[j]);
      } else {
        diffList.push(allLangId[j]);
      }
    }
    var lstData = self.getListForListBox(diffList);
    currRowData.lstBoxSelectLanguage.masterData = lstData;
    if(currInd !== 0){
      currRowData.lblAddAlertTypeSegLanguage.isVisible = false;
      currRowData.lstBoxSelectLanguage.isVisible = true;
      currRowData.lstBoxSelectLanguage.selectedKey = lstData[0][0];
      self.view.segaddalertTypeLanguages.setDataAt(currRowData, currInd);
    }
  },
  /* 
   * function to return the required languages in listbox masterdata format
   * @param: unselected language id's list
   * @retrun: masterdata with given language id's
   */
  getListForListBox: function(data) {
    var self = this;
    var finalList = [];
    for (var i = 0; i < self.langToShow.length; i++) {
      if (data.contains(self.langToShow[i][0])) {
        finalList.push(self.langToShow[i]);
      }
    }
    return finalList;
  },
  /*
   * on hover callback for listbox - in case same language is selected from listbox again
   */
  hideListBoxOnHover : function(widget,context){
    var self = this;
    var ind = self.view.segaddalertTypeLanguages.selectedRowIndex[1];
    var rowData = self.view.segaddalertTypeLanguages.data[ind];
    var selectedLangId = rowData.lblAddAlertTypeSegLanguage.info.id;
    var listSelectedId = rowData.lstBoxSelectLanguage.selectedKey;
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      //do-nothing
    }else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      if(selectedLangId === listSelectedId){
        rowData.lblAddAlertTypeSegLanguage.isVisible = true;
        rowData.lstBoxSelectLanguage.isVisible = false;
        self.view.segaddalertTypeLanguages.setDataAt(rowData, ind);
      } else if(selectedLangId === ""){ // for first time selection
        rowData.lblAddAlertTypeSegLanguage.isVisible = true;
        rowData.lblAddAlertTypeSegLanguage.text = self.getLanguageNameForCode(listSelectedId);
        rowData.lblAddAlertTypeSegLanguage.info.id = listSelectedId;
        rowData.lstBoxSelectLanguage.isVisible = false;
        self.view.segaddalertTypeLanguages.setDataAt(rowData, ind);
      } else{
        rowData.lstBoxSelectLanguage.selectedKey = rowData.lblAddAlertTypeSegLanguage.info.id;
        rowData.lblAddAlertTypeSegLanguage.isVisible = true;
        rowData.lstBoxSelectLanguage.isVisible = false;
        self.view.segaddalertTypeLanguages.setDataAt(rowData, ind);
      }
    }
  },
  /*
   * function to add a row at end once started typing in name or description textboxes
   * @param- event,rowInfo
   */
  addNewRowAtEnd : function(event,rowInfo){
    var self = this;
    var segData = self.view.segaddalertTypeLanguages.data;
    var currInd = event ? rowInfo.rowIndex : rowInfo[1];
    kony.store.setItem("keyName", segData[currInd].tbxDisplayName.text);
    kony.store.setItem("keyDescription", segData[currInd].tbxDescription.text);
    if(segData.length === currInd+1 && segData.length !== self.langToShow.length){
      var newRow = self.getLangSegDataMapJson();
      newRow.lblAddAlertTypeSegLanguage.text = self.sLang;
      newRow.lblAddAlertTypeSegLanguage.info.id = "";
      newRow.lstBoxSelectLanguage.selectedKey = self.langToShow[0][0];
      self.view.segaddalertTypeLanguages.addDataAt(newRow, currInd+1);
      self.view.forceLayout();
    }
  },
  /*
   * callback function to hide textbox and show label
   */
  onHoverHideTextBoxCallback: function(widget, context) {
    var self = this;
    var ind = self.view.segaddalertTypeLanguages.selectedRowIndex[1];
    var rowData = self.view.segaddalertTypeLanguages.data[ind];
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      //do-nothing
    }else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      if (widget.id === "tbxDisplayName") {
        rowData.tbxDisplayName.isVisible = false;
        rowData.lblAddAlertTypeSegDisplayName.isVisible = true;
        if (rowData.tbxDisplayName.text === "" && rowData.lblAddAlertTypeSegDisplayName.text !== "Add Name") {
          rowData.tbxDisplayName.text = kony.store.getItem("keyName");
        }
        rowData.lblAddAlertTypeSegDisplayName.text = rowData.tbxDisplayName.text === "" ? "Add Name": rowData.tbxDisplayName.text ;
      } else {
        rowData.flxDescription.isVisible = false;
        rowData.lblAddAlertTypeSegDescription.isVisible = true;
        if (rowData.tbxDescription.text === "" && rowData.lblAddAlertTypeSegDescription.text !== self.sWriteDesc) {
          rowData.tbxDescription.text = kony.store.getItem("keyDescription");
        }
        rowData.lblAddAlertTypeSegDescription.text = rowData.tbxDescription.text === ""? self.sWriteDesc: rowData.tbxDescription.text;
      }
      self.view.segaddalertTypeLanguages.setDataAt(rowData, ind);
      self.view.forceLayout();
    }
  },
  /*
   * hover callback to display delete button on segment hover
   */
  onHoverDisplayDelete : function(widget, context){
    var self = this;
    var ind = context.rowIndex;
    var rowData = self.view.segaddalertTypeLanguages.data[ind];
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      if(ind !== 0){
        if(rowData.flxDelete.isVisible === false){
          rowData.flxDelete.isVisible = true;
          self.view.segaddalertTypeLanguages.setDataAt(rowData, ind);
        }
      }
    }else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      if(rowData.flxDelete.isVisible === true){
        rowData.flxDelete.isVisible = false;
        self.view.segaddalertTypeLanguages.setDataAt(rowData, ind);
      }
    }
    self.view.forceLayout();
  },
  /*
   * display delete confirmation popup in add/edit alert type scrren
   */
  showAlertTypeDeleteLangPopup : function(){
    var self = this;
    this.view.flxDeleteAlert.isVisible = true;
    this.view.popUpDeactivate.btnPopUpCancel.text = self.sNoLeaveAsIs;
    this.view.popUpDeactivate.btnPopUpCancel.right = "150px";
    this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.DeleteLanguage");
    this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.AreYouSureDeleteLanguage");
    this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesDelete");
    //overriding actions for alert type delete language
    this.view.popUpDeactivate.btnPopUpCancel.onClick = function(){
      self.view.flxDeleteAlert.setVisibility(false);
    };
    this.view.popUpDeactivate.flxPopUpClose.onClick = function(){
      self.view.flxDeleteAlert.setVisibility(false);
    };
    this.view.popUpDeactivate.btnPopUpDelete.onClick = function(){
      self.onClickOfDeleteLang();
      self.view.flxDeleteAlert.setVisibility(false);
    };
  },
  /*
   * delete current row from the languages segment
   */
  onClickOfDeleteLang : function(){
    var self = this;
    var currInd = self.view.segaddalertTypeLanguages.selectedRowIndex[1];
    self.view.segaddalertTypeLanguages.removeAt(currInd);
    self.view.forceLayout();
  },
  /*
   * map apps/users masterdata list to custom listboxes seg
   * @param: apps/users list
   * @return : mapped data
   */
  mapAppsUsersListData: function(data) {
    var self = this;
    var listBoxData = [];
    var widgetDataMap = {
      "id": "id",
      "lblDescription": "lblDescription",
      "imgCheckBox": "imgCheckBox",
      "flxCheckBox":"flxCheckBox",
      "flxSearchDropDown": "flxSearchDropDown"
    };
    self.view.customListboxApps.segList.widgetDataMap = widgetDataMap;
    self.view.customListboxUsertypes.segList.widgetDataMap = widgetDataMap;
    if (data) {
      listBoxData = data.map(function(rec) {
        return {
          "id": rec.id,
          "lblDescription": {"text":rec.Name},
          "imgCheckBox": {"src":self.checkboxnormal},
          "template": "flxSearchDropDown"
        };
      });
    }
    return listBoxData;
  },
  /*
   * set apps/users mapped data to listbox
   * @param : mapped data,category("apps" or "users")
   */
  setAppsUsersData : function(data,category){
    var self = this;
    var widgetPath ="";
    if(category === "apps"){
      widgetPath = self.view.customListboxApps;
    }else if(category === "users"){
      widgetPath = self.view.customListboxUsertypes;
    }
    var appData = self.mapAppsUsersListData(data);
    widgetPath.segList.setData(appData);
    widgetPath.segList.selectedIndices =[[0,[0]]];
    widgetPath.lblSelectedValue.text = self.sAll;
  },
  /*
   * on row click of custom listbox of apps/users-list
   * @param : category - apps or users
   */
  onCustomListBoxRowClick : function(category){
    var self = this;
    var widgetPath ="",segData ="",lblText = "";
    if(category === "apps"){
      widgetPath = self.view.customListboxApps;
      lblText =  kony.i18n.getLocalizedString("i18n.frmAlertsManagement.SelectApplicableApps");
    }else if(category === "users"){
      widgetPath = self.view.customListboxUsertypes;
      lblText = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.SelectApplicableUserTypes");
    }
    segData = widgetPath.segList.data;
    var arr = [];
    for(var i= 0; i< segData.length; i++){
      arr.push(i);
    }
    var selRows = widgetPath.segList.selectedRowItems;
    if(selRows){
      if(selRows.length === segData.length){
        widgetPath.imgCheckBox.src = self.checkboxselected;
        widgetPath.lblSelectedValue.text = self.sAll;
        widgetPath.segList.selectedIndices = [[0,arr]];
      }else{
        widgetPath.lblSelectedValue.text = selRows.length +" "+self.sSelected;
        widgetPath.imgCheckBox.src = self.checkboxnormal;
      }
    } else{
      widgetPath.lblSelectedValue.text = lblText;
    }
    self.view.forceLayout();

  },
  /*
   * select all/unselect all of custom listbox for apps and userlist
   * @param : category - apps or users
   */
  onClickOfSelectAll : function(category){
    var self = this;
    var widgetPath ="",segData ="",lblText = "";
    if(category === "apps"){
      widgetPath = self.view.customListboxApps;
      lblText =  kony.i18n.getLocalizedString("i18n.frmAlertsManagement.SelectApplicableApps");
    }else if(category === "users"){
      widgetPath = self.view.customListboxUsertypes;
      lblText = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.SelectApplicableUserTypes");
    }
    segData = widgetPath.segList.data;
    var arr = [];
    for(var i= 0; i< segData.length; i++){
      arr.push(i);
    }
    if(widgetPath.imgCheckBox.src === self.checkboxnormal){
      widgetPath.imgCheckBox.src = self.checkboxselected;
      widgetPath.lblSelectedValue.text = self.sAll;
      widgetPath.segList.selectedIndices = [[0,arr]];
    }else if(widgetPath.imgCheckBox.src === self.checkboxselected){
      widgetPath.imgCheckBox.src = self.checkboxnormal;
      widgetPath.segList.selectedIndices = null;
      widgetPath.lblSelectedValue.text = lblText;
    }
    self.view.forceLayout();
  },
  /*
   * set data to sub-alerts listing segment
   * @param : sub-alerts data
   */
  setSubAlertSegData : function(){
    var self=this;
    var data =[];
    var dataMap={
      "flxSubAlertHeaderDisplayName": "flxSubAlertHeaderDisplayName",
      "flxSubAlertHeaderCode": "flxSubAlertHeaderCode",
      "flxSubAlertHeaderDescription": "flxSubAlertHeaderDescription",
      "lblRoleHeaderSeperator": "lblRoleHeaderSeperator",
      "flxSubAlertHeaderStatus": "flxSubAlertHeaderStatus",
      "flxOptions": "flxOptions",
      "flxSubAlerts": "flxSubAlerts",
      "flxStatus": "flxStatus",
      "lblIconImgOptions": "lblIconImgOptions",
      "lblHeaderSeperator":"lblHeaderSeperator",
      "lblDescription": "lblDescription",
      "lblDisplayName": "lblDisplayName",
      "lblAlertStatus": "lblAlertStatus",
      "fontIconStatusImg": "fontIconStatusImg",
      "lblCode": "lblCode",
      "lblSeperator": "lblSeperator",
    };
    data = self.alertsJSON.map(function(subAlertsData) {
      return {
        "lblDisplayName": subAlertsData.name,
        "lblCode":{"text":self.AdminConsoleCommonUtils.getTruncatedString(subAlertsData.code, 28, 25),
                   "tooltip":subAlertsData.code,
                   "info":{"value" :subAlertsData.code }},
        "lblDescription": subAlertsData.description,
        "lblSeperator": "-",
        "lblIconImgOptions": {"text":"\ue91f"},
        "template":"flxSubAlerts",
        "lblAlertStatus": {
          "text": subAlertsData.status === "SID_ACTIVE" ? self.sActive : self.sInActive,
          "skin": subAlertsData.status === "SID_ACTIVE" ? "sknlblLato5bc06cBold14px" : "sknlblLatocacacaBold12px",
        },
        "fontIconStatusImg": {
          "text": "\ue921",
          "skin": subAlertsData.status === "SID_ACTIVE" ? "sknIcon13pxGreen" : "sknIcon13pxGray",
        },
        "flxOptions": {
          "onClick": function () {
            self.onClickOptions();
          }
        },
      };
    });
    this.view.segSubAlerts.widgetDataMap=dataMap;
    this.view.segSubAlerts.setData(data);
    this.view.flxNoResultFound.setVisibility(data.length <= 0);
    this.view.flxSegRoles.setVisibility(data.length > 0);
    this.view.flxSubAlertsHeader.setVisibility(data.length > 0);
    this.view.forceLayout();
  },
  onClickOptions:function(){
	var selItems = this.view.segSubAlerts.selectedItems[0];
    var heightVal= this.mouseYCoordinate-180;
    this.view.lblDeactivate.text=selItems.lblAlertStatus.text===this.sActive?this.sPermDeactivate :this.sPermActivate;
    this.view.lblDeactivateIcon.text=selItems.lblAlertStatus.text===this.sActive?"\ue91c":"\ue931";
    this.view.lblOption1.text = kony.i18n.getLocalizedString("i18n.roles.Edit");
    if (this.view.flxSelectOptions.isVisible===true){
      this.view.flxSelectOptions.isVisible = false;
    }
    else{
      this.view.flxSelectOptions.isVisible = true;
    }
    this.view.forceLayout();
    if(((this.view.flxSelectOptions.frame.height+heightVal)>(this.view.segSubAlerts.frame.height+50))&&this.view.flxSelectOptions.frame.height<this.view.segSubAlerts.frame.height+60){
      this.view.flxSelectOptions.top=((heightVal-this.view.flxSelectOptions.frame.height)-39)+"px";
    }
    else{
      this.view.flxSelectOptions.top=(heightVal)+"px";
    }
  },
  showViewContextualMenu : function(){
    this.view.lblDeactivateAlert.text=this.view.lblViewDetailsAlertStatus.text===this.sActive?this.sPermDeactivate :this.sPermActivate;
    this.view.lblDeactivateAlertIcon.text=this.view.lblViewDetailsAlertStatus.text===this.sActive?"\ue91c":"\ue931";
    var top = (this.view.flxOptionsSubAlerts.frame.y + 40) - this.view.flxViewSubAlert.contentOffsetMeasured.y;
    this.view.flxSubAlertContextualMenu.top = top +"dp";
    this.view.flxSubAlertContextualMenu.setVisibility(true);
  },
  saveScreenY:function(widget,context){
    this.mouseYCoordinate=context.screenY;
  },
  setSubAlertsView : function(subAlertData){
    var selectedIndex=this.view.segSubAlerts.selectedIndices[0][1];
    this.view.breadcrumbAlerts.btnPreviousPage.text=this.view.breadcrumbAlerts.lblCurrentScreen.text;
    this.view.breadcrumbAlerts.lblCurrentScreen.text=subAlertData.subAlertDefinition.name.toUpperCase();
    this.view.lblSubAlertName.text=subAlertData.subAlertDefinition.name;
    this.view.lblAlertChannelsValue.text=this.view.lblCategoryChannelValue.text;
    this.view.lblSubAlertCodeValue.text=subAlertData.subAlertDefinition.code;
    this.view.rtxAlertDescription.text=subAlertData.subAlertDefinition.description;
    this.view.lblViewDetailsStatusIcon.skin=subAlertData.subAlertDefinition.status === "SID_ACTIVE" ?
      "sknFontIconActivate" : "sknfontIconInactive";
    this.view.lblViewDetailsAlertStatus.text=subAlertData.subAlertDefinition.status === "SID_ACTIVE" ?
      kony.i18n.getLocalizedString("i18n.permission.Active") : kony.i18n.getLocalizedString("i18n.permission.Inactive");
    this.view.lblViewDetailsAlertStatus.skin=subAlertData.subAlertDefinition.status === "SID_ACTIVE" ?
      "sknlblLato5bc06cBold14px":"sknlblLatocacacaBold12px";
    this.view.lblTemplateHeader.text=this.sAlertContentTemplate;
    this.view.lblContentBy.text="View Content By";//kony.i18n.getLocalizedString("i18n.frmAlertsManagement.viewContentBy");
    this.showSupportedChannels();
    this.contentTemplates=subAlertData.communicationTemplates;
    this.setContentTemplateData();
    this.setDefaultSubAlertView();
    this.view.forceLayout();
  },
  setDefaultSubAlertView : function(){
    this.view.lstBoxSubAlertResponseState.setEnabled(true);
    this.view.lstBoxSubAlertLanguages.setEnabled(true);
    this.view.lstBoxSubAlertResponseState.skin="sknlbxBgffffffBorderc1c9ceRadius3Px";
    this.view.lstBoxSubAlertLanguages.skin="sknlbxBgffffffBorderc1c9ceRadius3Px";
    this.view.flxAddTemplateButton.setVisibility(true);
    this.view.flxOptionsSubAlerts.setVisibility(true);
    this.view.flxAddTemplate.setVisibility(false);
    this.view.flxSaveTemplateButtons.setVisibility(false);
    this.view.flxAlertTypes.setVisibility(false);
    this.view.flxSubAlerts.setVisibility(true);
    this.view.forceLayout();
  },
  setContentTemplateData : function(){
    var templateData=Object.keys(this.contentTemplates);
    if(templateData.length===0){
      this.view.flxViewTemplateListBox.setVisibility(false);
      this.view.flxViewTemplate.setVisibility(false);
      this.view.flxNoContentTemplate.setVisibility(true);
      this.showAddTemplateBtn();
      this.view.rtxNoTemplate.text=kony.i18n.getLocalizedString("i18n.frmAlertsManagement.NoTemplateCreated");
      this.view.forceLayout();
    }
    else{
      this.setTemplateListBoxData();
      this.setChannelsTemplateData();
      this.showAllBtns();
      this.view.flxViewTemplateListBox.setVisibility(true);
      this.view.flxViewTemplate.setVisibility(true);
      this.view.flxNoContentTemplate.setVisibility(false);
    }
  },
  showAddTemplateBtn : function(){
    this.view.flxAddTemplateButton.right="0px";
    this.view.flxOptionEdit.setVisibility(false);
    this.view.flxEyeicon.setVisibility(false);
    this.view.flxAddTemplateButton.setVisibility(true);
    this.view.forceLayout();
  },
  showAllBtns : function(){
    this.view.flxAddTemplateButton.right="200px";
    this.view.flxOptionEdit.setVisibility(true);
    this.view.flxEyeicon.setVisibility(true);
    this.view.flxAddTemplateButton.setVisibility(true);
    this.view.forceLayout();
  },
  setTemplateListBoxData : function(){
    var responseStateData=[];
    var templateLanguages=[];
    var templateLangData=[];
    var templateData=this.contentTemplates;
    if(Object.keys(this.contentTemplates).length!==0){
      if(templateData.SID_EVENT_SUCCESS){
        responseStateData.push(["SID_EVENT_SUCCESS","Success"]);
        for(var j in templateData.SID_EVENT_SUCCESS)
          templateLanguages.push(j);
      }
      if(templateData.SID_EVENT_FAILURE){
        responseStateData.push(["SID_EVENT_FAILURE","Failure"]);
        for(var k in templateData.SID_EVENT_FAILURE){
          if(templateLanguages.indexOf(k) == -1)
            templateLanguages.push(k);
        }
      }
      for(var x=0;x<templateLanguages.length;x++){
        for(var y=0;y<this.langToShow.length;y++){
          if(this.langToShow[y][0].toLowerCase()==templateLanguages[x].toLowerCase())
            templateLangData.push(this.langToShow[y]);
        }
      }
    }else{
      responseStateData.push(["SID_EVENT_SUCCESS","Success"],["SID_EVENT_FAILURE","Failure"]);
      templateLangData=this.langToShow;
    }
    this.view.lstBoxSubAlertResponseState.masterData=responseStateData;
    this.view.lstBoxSubAlertLanguages.masterData=templateLangData;
    this.view.forceLayout();
  },
  setChannelsTemplateData : function(){
    var templatesData=this.contentTemplates;
    var selectedLang=this.view.lstBoxSubAlertLanguages.selectedKey;
    var selectedState=this.view.lstBoxSubAlertResponseState.selectedKey;
    var channelsData=null;
    if(templatesData.hasOwnProperty(selectedState)){
      if(templatesData[selectedState].hasOwnProperty(selectedLang))
        channelsData=templatesData[selectedState][selectedLang];
    }
    this.view.flxViewSMSTemplate.setVisibility(false);
    this.view.flxViewNotificationCenter.setVisibility(false);
    this.view.flxViewPushNotification.setVisibility(false);
    this.view.flxViewEmailTemplate.setVisibility(false);
    if(channelsData===null){
      this.view.flxViewTemplate.setVisibility(false);
      this.view.rtxNoTemplate.text=kony.i18n.getLocalizedString("i18n.frmAlertsManagement.NoLanguageTemplate");
      this.view.flxNoContentTemplate.setVisibility(true);
    }else{
      this.view.flxNoContentTemplate.setVisibility(false);
      for(var channel in channelsData){
        if(channel==="CH_SMS"&&this.supportedChannels.contains("SMS/Text")){
          this.view.ViewTemplateSMS.lblChannelMsg.text=channelsData[channel].content;
          this.view.flxViewSMSTemplate.setVisibility(true);
        }else if(channel==="CH_NOTIFICATION_CENTER"&&this.supportedChannels.contains("Notification Center")){
          this.view.ViewTemplateCenter.lblTitleValue.text=channelsData[channel].templateSubject;
          this.view.ViewTemplateCenter.lblChannelMsg.text=channelsData[channel].content;
          this.view.flxViewNotificationCenter.setVisibility(true);
        }else if(channel==="CH_PUSH_NOTIFICATION"&&this.supportedChannels.contains("Push Notification")){
          this.view.ViewTemplatePush.lblTitleValue.text=channelsData[channel].templateSubject;
          this.view.ViewTemplatePush.lblChannelMsg.text=channelsData[channel].content;
          this.view.flxViewPushNotification.setVisibility(true);
        }else if(channel==="CH_EMAIL"&&this.supportedChannels.contains("Email")){
          this.view.lblEmailTitleValue.text=channelsData[channel].templateSubject;
          this.setRtxViewerData(channelsData[channel].content);
          this.view.flxViewEmailTemplate.setVisibility(true);
        }
      }
      this.view.flxEyeicon.setVisibility(true);
      this.view.flxViewTemplate.setVisibility(true);
    }
    this.view.forceLayout();
  },
  setListSelectedChannelsData : function(){
    var templatesData=this.contentTemplates;
    var selectedLang=this.view.lstBoxSubAlertLanguages.selectedKey;
    var selectedState=this.view.lstBoxSubAlertResponseState.selectedKey;
    var channelsData=null;
    if(templatesData.hasOwnProperty(selectedState)){
      if(templatesData[selectedState].hasOwnProperty(selectedLang))
        channelsData=templatesData[selectedState][selectedLang];
    }
    if(channelsData===null){
      this.view.txtSMSMsg.text="";
      this.view.txtPushNotification.text="";
      this.view.tbxPushNotificationTitle.text="";
      this.view.tbxNotiCenterTitle.text="";
      this.view.txtNotiCenterMsg.text="";
      this.view.tbxEmailSubject.text="";
      document.getElementById("iframe_rtxEmailTemplate").contentWindow.document.getElementById("editor").innerHTML="";
    }else{
      for(var channel in channelsData){
        if(channel==="CH_SMS"&&this.supportedChannels.contains("SMS/Text")){
          this.view.txtSMSMsg.text=channelsData[channel].content;
          this.view.flxAddSMSTemplate.setVisibility(true);
        }else if(channel==="CH_NOTIFICATION_CENTER"&&this.supportedChannels.contains("Notification Center")){
          this.view.tbxNotiCenterTitle.text=channelsData[channel].templateSubject;
          this.view.txtNotiCenterMsg.text=channelsData[channel].content;
          this.view.flxAddNotificationCenter.setVisibility(true);
        }else if(channel==="CH_PUSH_NOTIFICATION"&&this.supportedChannels.contains("Push Notification")){
          this.view.tbxPushNotificationTitle.text=channelsData[channel].templateSubject;
          this.view.txtPushNotification.text=channelsData[channel].content;
          this.view.flxAddPushNotification.setVisibility(true);
        }else if(channel==="CH_EMAIL"&&this.supportedChannels.contains("Email")){
          this.view.tbxEmailSubject.text=channelsData[channel].templateSubject;
          document.getElementById("iframe_rtxEmailTemplate").contentWindow.document.getElementById("editor").innerHTML=channelsData[channel].content;
          this.view.flxAddEmailTemplate.setVisibility(true);
        }
      }
    }
    this.view.forceLayout();
  },
  showEditSubAlert : function(){
    var selectedIndex=this.view.segSubAlerts.selectedIndices[0][1];
    this.view.txtbxSubAlertName.text=this.view.segSubAlerts.data[selectedIndex].lblDisplayName;
    this.view.lstbxSubAlertCode.masterData=[[this.view.segSubAlerts.data[selectedIndex].lblCode.text,this.view.segSubAlerts.data[selectedIndex].lblCode.text]];
    this.view.txtAlertDescription.text=this.view.segSubAlerts.data[selectedIndex].lblDescription;
    this.view.AlertStatusSwitch.switchToggle.selectedIndex=this.view.segSubAlerts.data[selectedIndex].lblAlertStatus.text===this.sActive?0:1;
    this.view.lblAddSubAlertHeader.text=kony.i18n.getLocalizedString("i18n.alertsManagment.editAlert");
    this.view.flxAlertNameAvailable.setVisibility(false);
    this.view.flxErrorAlertName.setVisibility(false);
    this.view.flxErrorAlertCode.setVisibility(false);
    this.view.flxNoDescriptionError.setVisibility(false);
    this.view.flxSubAlertCodeGrey.setVisibility(true);
    this.view.btnsave.text=this.sUpdateCaps;
    this.view.txtbxSubAlertName.skin="skntbxLato35475f14px";
    this.view.lstbxSubAlertCode.skin="sknlbxBgffffffBorderc1c9ceRadius3Px";
    this.view.txtAlertDescription.skin="skntbxLato35475f14px";
    this.view.flxAddSubAlertPopUp.setVisibility(true);
    this.view.forceLayout();
  },
  showAddSubAlert : function(alertCodes){
    var alertCodeList=[["Select","Select"]];
    this.alertGroupCurrAction=this.alertGroupActionConfig.CREATE;
    for(var i=0;i<alertCodes.length;i++){
      alertCodeList.push([alertCodes[i].id,alertCodes[i].id]);
    }
    this.view.lstbxSubAlertCode.masterData=alertCodeList;
    this.view.lstbxSubAlertCode.selectedKey="Select";
    this.view.txtbxSubAlertName.text="";
    this.view.txtAlertDescription.text="";
    this.view.lblAddSubAlertHeader.text=kony.i18n.getLocalizedString("i18n.frmAlertsManagement.AddAlert");
    this.view.flxAlertNameAvailable.setVisibility(false);
    this.view.flxErrorAlertName.setVisibility(false);
    this.view.flxErrorAlertCode.setVisibility(false);
    this.view.flxNoDescriptionError.setVisibility(false);
    this.view.btnsave.text=kony.i18n.getLocalizedString("i18n.frmAlertsManagement.SAVE");
    this.view.flxSubAlertCodeGrey.setVisibility(false);
    this.view.txtbxSubAlertName.skin="skntbxLato35475f14px";
    this.view.lstbxSubAlertCode.skin="sknlbxBgffffffBorderc1c9ceRadius3Px";
    this.view.txtAlertDescription.skin="skntbxLato35475f14px";
    this.view.AlertStatusSwitch.switchToggle.selectedIndex=0;
    this.view.flxAddSubAlertPopUp.setVisibility(true);
    this.view.forceLayout();
  },
  showDeactivateSubAlert : function(){
    var self = this;
    this.view.flxDeleteAlert.isVisible = true;
    this.view.popUpDeactivate.btnPopUpCancel.text = self.sNoLeaveAsIs;
    this.view.popUpDeactivate.btnPopUpCancel.right = "180px";
    this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.DeactivateSubAlertStatus");
    this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.DeactivateSubAlertMsg");
    this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.PopUp.YesDeactivate");
    //overriding actions for alert type delete language
    this.view.popUpDeactivate.btnPopUpCancel.onClick = function(){
      self.view.flxDeleteAlert.setVisibility(false);
    };
    this.view.popUpDeactivate.flxPopUpClose.onClick = function(){
      self.view.flxDeleteAlert.setVisibility(false);
    };
    this.view.popUpDeactivate.btnPopUpDelete.onClick = function(){
      self.activateDeactivateSubAlert("SID_INACTIVE");
      self.view.flxDeleteAlert.setVisibility(false);
    };
  },
  activateDeactivateSubAlert : function(statusId){
    var self = this;
    var alertData={
      "alertTypeCode":self.view.lblAlertTypeCodeValue.text,
      "code":self.view.segSubAlerts.selectedItems[0].lblCode.text,
      "statusId":statusId
    };
    self.presenter.editSubAlertStatus(alertData,self.view.flxSubAlerts.isVisible);
  },
  setRtxViewerData : function(data){
    this.emailRtxData=data;
    if(document.getElementById("iframe_rtxEmailViewer").contentWindow.document.getElementById("viewer")) {
      document.getElementById("iframe_rtxEmailViewer").contentWindow.document.getElementById("viewer").innerHTML = data;
    } else {
      if(!document.getElementById("iframe_rtxEmailViewer").newOnload) {
        document.getElementById("iframe_rtxEmailViewer").newOnload = document.getElementById("iframe_rtxEmailViewer").onload;
      }
      document.getElementById("iframe_rtxEmailViewer").onload = function() {
        document.getElementById("iframe_rtxEmailViewer").newOnload();
        document.getElementById("iframe_rtxEmailViewer").contentWindow.document.getElementById("viewer").innerHTML =data;
      };
    }
    this.view.forceLayout();
  },
  editContentTemplate : function(){
    this.alertGroupCurrAction=this.alertGroupActionConfig.EDIT;
    this.showSupportedChannels();
    this.view.lblTemplateHeader.text=kony.i18n.getLocalizedString("i18n.frmAlertsManagement.EditAlertContentTemplate");
    this.view.lblContentBy.text="Edit Content By";//kony.i18n.getLocalizedString("i18n.frmAlertsManagement.editContentBy");
    this.view.lstBoxSubAlertResponseState.setEnabled(false);
    this.view.lstBoxSubAlertLanguages.setEnabled(false);
    this.view.lstBoxSubAlertResponseState.skin="lstBxBre1e5edR3pxBgf5f6f8Disable";
    this.view.lstBoxSubAlertLanguages.skin="lstBxBre1e5edR3pxBgf5f6f8Disable";
    if(this.view.flxViewSMSTemplate.isVisible){
      this.view.txtSMSMsg.text=this.view.ViewTemplateSMS.lblChannelMsg.text;
    }else{
      this.view.txtSMSMsg.text="";
    }
    if(this.view.flxViewPushNotification.isVisible){
      this.view.tbxPushNotificationTitle.text=this.view.ViewTemplatePush.lblTitleValue.text;
      this.view.txtPushNotification.text=this.view.ViewTemplatePush.lblChannelMsg.text;
    }
    else{
      this.view.tbxPushNotificationTitle.text="";
      this.view.txtPushNotification.text="";
    }
    if(this.view.flxViewNotificationCenter.isVisible){
      this.view.tbxNotiCenterTitle.text=this.view.ViewTemplateCenter.lblTitleValue.text;
      this.view.txtNotiCenterMsg.text=this.view.ViewTemplateCenter.lblChannelMsg.text;
    }
    else{
      this.view.tbxNotiCenterTitle.text="";
      this.view.txtNotiCenterMsg.text="";
    }
    if(this.view.flxViewEmailTemplate.isVisible){
      this.view.tbxEmailSubject.text=this.view.lblEmailTitleValue.text;
      var emailEditorDocument = document.getElementById("iframe_rtxEmailTemplate").contentWindow.document;
      var emailViewerDocument = document.getElementById("iframe_rtxEmailViewer").contentWindow.document;
      this.emailRtxData=emailViewerDocument.getElementById("viewer").innerHTML;
      emailEditorDocument.getElementById("editor").innerHTML = this.emailRtxData;
    }
    else{
      this.view.tbxEmailSubject.text="";
      document.getElementById("iframe_rtxEmailTemplate").contentWindow.document.getElementById("editor").innerHTML="";
    }

    this.view.flxViewTemplate.setVisibility(false);
    this.view.flxOptionEdit.setVisibility(false);
    this.view.flxAddTemplateButton.setVisibility(false);
    this.view.flxOptionsSubAlerts.setVisibility(false);
    this.view.flxSaveTemplateButtons.setVisibility(true);
    this.view.btnTemplateSave.text=this.sUpdateCaps;
    this.view.flxAddTemplate.setVisibility(true);
  },
  addContentTemplate : function(){
    this.alertGroupCurrAction=this.alertGroupActionConfig.CREATE;
    this.view.txtSMSMsg.text="";
    this.view.txtPushNotification.text="";
    this.view.tbxPushNotificationTitle.text="";
    this.view.tbxNotiCenterTitle.text="";
    this.view.txtNotiCenterMsg.text="";
    this.view.tbxEmailSubject.text="";
    var langMasterData=this.listBoxDataLang.concat(this.langToShow);
    this.view.lstBoxSubAlertLanguages.masterData=langMasterData;
    this.view.lstBoxSubAlertResponseState.masterData=([["SID_EVENT_SUCCESS","Success"],["SID_EVENT_FAILURE","Failure"]]);
    this.view.lblTemplateHeader.text=kony.i18n.getLocalizedString("i18n.frmAlertsManagement.AddAlertContentTemplate");
    this.view.lblContentBy.text="Add Content By";//kony.i18n.getLocalizedString("i18n.frmAlertsManagement.addContentBy");
    this.showSupportedChannels();
    //     this.setTemplateListBoxData();
    this.view.flxViewTemplateListBox.setVisibility(true);
    this.view.flxViewTemplate.setVisibility(false);
    this.view.flxOptionEdit.setVisibility(false);
    this.view.flxAddTemplateButton.setVisibility(false);
    this.view.flxOptionsSubAlerts.setVisibility(false);
    this.view.btnTemplateSave.text=kony.i18n.getLocalizedString("i18n.frmAlertsManagement.SAVE");
    this.view.lstBoxSubAlertLanguages.selectedKey="select";
    if(this.supportedChannels.contains("Email"))
      document.getElementById("iframe_rtxEmailTemplate").contentWindow.document.getElementById("editor").innerHTML="";
    this.view.flxNoContentTemplate.setVisibility(false);
    //document.getElementById("iframe_rtxEmailTemplate").contentWindow.document.getElementsByClassName("table-palette")[0].style.display = "none";
    document.getElementById("iframe_rtxEmailTemplate").contentWindow.document.querySelector("#editor").dataset.text =kony.i18n.getLocalizedString("i18n.frmAlertsManagement.addContentHere");
    document.getElementById("iframe_rtxEmailTemplate").contentWindow.document.getElementsByClassName("icon-image")[0].style.display = "block";
    this.view.forceLayout();
  },
  saveTemplate : function(){
    var templateData;
    var addedChannels=[];
    var emailEditorDocument = document.getElementById("iframe_rtxEmailTemplate").contentWindow.document;
    if(this.view.flxAddSMSTemplate.isVisible&&this.view.txtSMSMsg.text!==""){
      var smsChannelData={
        "locale": this.view.lstBoxSubAlertLanguages.selectedKey,
        "statusId": this.view.lstBoxSubAlertResponseState.selectedKey,
        "channelId": "CH_SMS",
        "content": this.view.txtSMSMsg.text
      };
      addedChannels.push(smsChannelData);
    }
    if(this.view.flxAddPushNotification.isVisible&&this.view.txtPushNotification.text!==""&&this.view.tbxPushNotificationTitle.text!==""){
      var pushChannelData={
        "locale": this.view.lstBoxSubAlertLanguages.selectedKey,
        "statusId": this.view.lstBoxSubAlertResponseState.selectedKey,
        "channelId": "CH_PUSH_NOTIFICATION",
        "subject": this.view.tbxPushNotificationTitle.text,
        "content": this.view.txtPushNotification.text
      };
      addedChannels.push(pushChannelData);
    }
    if(this.view.flxAddNotificationCenter.isVisible&&this.view.tbxNotiCenterTitle.text!==""&&this.view.txtNotiCenterMsg.text!==""){
      var notiChannelData={
        "locale": this.view.lstBoxSubAlertLanguages.selectedKey,
        "statusId": this.view.lstBoxSubAlertResponseState.selectedKey,
        "channelId": "CH_NOTIFICATION_CENTER",
        "subject": this.view.tbxNotiCenterTitle.text,
        "content": this.view.txtNotiCenterMsg.text
      };
      addedChannels.push(notiChannelData);
    }
    if(this.view.flxAddEmailTemplate.isVisible&&this.view.tbxEmailSubject.text!==""&&emailEditorDocument.getElementById("editor").innerHTML.length>0){      
      var emailChannelData={
        "locale": this.view.lstBoxSubAlertLanguages.selectedKey,
        "statusId": this.view.lstBoxSubAlertResponseState.selectedKey,
        "channelId": "CH_EMAIL",
        "subject": this.view.tbxEmailSubject.text,
        "content": emailEditorDocument.getElementById("editor").innerHTML
      };
      addedChannels.push(emailChannelData);
    }
    if(addedChannels.length!==0){
      templateData={
        "alertTypeCode":this.view.lblAlertTypeCodeValue.text,
        "code":this.view.lblSubAlertCodeValue.text,
        "name":this.view.lblSubAlertName.text,
        "description":this.view.rtxAlertDescription.text,
        "addedTemplates":addedChannels,
        "removedTemplates":[]
      };
      this.callEditSubAlert(templateData);
    }else{
      this.setContentTemplateData();
    }
  },
  showSupportedChannels : function(){
    if(this.supportedChannels.contains("Email"))
      this.view.flxAddEmailTemplate.setVisibility(true);
    else
      this.view.flxAddEmailTemplate.setVisibility(false);
    if(this.supportedChannels.contains("SMS/Text"))
      this.view.flxAddSMSTemplate.setVisibility(true);
    else
      this.view.flxAddSMSTemplate.setVisibility(false);
    if(this.supportedChannels.contains("Push Notification"))
      this.view.flxAddPushNotification.setVisibility(true);
    else
      this.view.flxAddPushNotification.setVisibility(false);
    if(this.supportedChannels.contains("Notification Center"))
      this.view.flxAddNotificationCenter.setVisibility(true);
    else
      this.view.flxAddNotificationCenter.setVisibility(false);
    this.view.forceLayout();
  },
  validateEditAlertCategoryScreen : function(){
    var isValid = true;
    if(this.selectedChannelList.length ===0 )
    {
      this.view.flxCategoryChannelError.setVisibility(true);
      this.view.lblErrMsgRadioCategory.text = kony.i18n.getLocalizedString("i18n.errCode.20703");
      isValid = false;
    }
    if (this.view.customCategoryRadioButtonType.selectedValue === null) {
      this.view.flxCategoryRadioButtonError.setVisibility(true);
      this.view.lblErrMsgRadioCategory.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Please_select_value");
      isValid = false;
    }
    var isLangValid = this.validateCategorySupportedLanguages();
    if (!isLangValid) {
      this.view.flxEditAlertCategoryLanguagesError.setVisibility(true);
      isValid = false;
    }
    return isValid;
  },
  validateAddAlertGroupScreen : function(){
    //var self = this;
    var isValid = true;

    if(this.view.dataEntryAlertName.tbxData.text === ""){
      this.view.dataEntryAlertName.flxError.setVisibility(true);
      this.view.dataEntryAlertName.lblErrorMsg.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Alert_group_name_cannot_be_empty");
      this.view.dataEntryAlertName.tbxData.skin = "skinredbg";
      isValid =false;
    } else if(this.view.dataEntryAlertName.tbxData.text.length < 5){
      this.view.dataEntryAlertName.flxError.setVisibility(true);
      this.view.dataEntryAlertName.lblErrorMsg.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Name_cannot_exceed_characters");
      this.view.dataEntryAlertName.tbxData.skin = "skinredbg";
      isValid =false;
    }
    var uniqueName = this.validateAlertName(this.view.dataEntryAlertName.tbxData.text);
    if(uniqueName === false &&
       this.alertGroupCurrAction === this.alertGroupActionConfig.CREATE ){
      this.view.dataEntryAlertName.flxError.setVisibility(true);
      this.view.dataEntryAlertName.lblErrorMsg.text = kony.i18n.getLocalizedString("Name already exsist");
      this.view.dataEntryAlertName.tbxData.skin = "skinredbg";
      isValid = false;
    }
    if(this.view.lstBoxAddAlertTypeCode.selectedKey === "select"){
      this.view.flxAddAlertTypeCodeError.setVisibility(true);
      this.view.lblErrMsgAlertTypeCode.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Please_select_alertgroup_code");
      this.view.lstBoxAddAlertTypeCode.skin = "redListBxSkin";
      isValid = false;
    }
    if(this.view.customRadioButtonType.selectedValue === null){
      this.view.flxRadioGroupError.setVisibility(true);
      this.view.lblErrMsgRadioGroup.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Please_select_type");
      isValid =false;
    }
    if(this.view.customListboxApps.segList.selectedIndices === null){
      this.view.customListboxApps.flxSelectedText.skin = "sknFlxCalendarError";
      this.view.customListboxApps.flxListboxError.setVisibility(true);
      this.view.customListboxApps.lblErrorMsg.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Please_select_apps");
      isValid =false;
    }
    if(this.view.customListboxUsertypes.segList.selectedIndices === null){
      this.view.customListboxUsertypes.flxSelectedText.skin = "sknFlxCalendarError";
      this.view.customListboxUsertypes.flxListboxError.setVisibility(true);
      this.view.customListboxUsertypes.lblErrorMsg.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Please_select_usertype");
      isValid =false;
    }
    var isAttibuteValid = this.validateAddAttributes();
    if(!isAttibuteValid){
      isValid =false;
    }
    var isLangValid = this.validateSupportedLanguages();
    if(!isLangValid){
      this.view.flxAddAlertTypeLanguagesError.setVisibility(true);
      isValid = false;
    }
    return isValid;
  },
  /*
   * error validations for attributes in add/edit alert group screen
   */
  validateAddAttributes : function(){
    var self = this;
    var isValid = true;
    if(this.view.imgCheckboxAttribute.src === self.checkboxselected){
      if(this.view.lstBoxAddAlertTypeAttribute.selectedKey === "select"){
        this.view.flxAttributesError.setVisibility(true);
        this.view.lstBoxAddAlertTypeAttribute.skin = "redListBxSkin";
        isValid =false;
      }
      if(this.view.lstBoxAddAlertTypeCriteria.selectedKey === "select" &&
         this.view.flxAddAlertTypeAttributeCriteria.isVisible === true){
        this.view.flxAddAlertTypeErrorCriteria.setVisibility(true);
        this.view.lstBoxAddAlertTypeCriteria.skin = "redListBxSkin";
        isValid =false;
      }
      if(this.view.lstBoxAddAlertTypeFrequency.selectedKey === "select" && 
         this.view.flxAddAlertTypeFreqValue.isVisible === true){
        this.view.flxAddAlertTypeErrorFrequency.setVisibility(true);
        this.view.lstBoxAddAlertTypeFrequency.skin = "redListBxSkin";
        isValid =false;
      }
      if(this.view.tbxAddAlertTypeFromValue.text === ""){
        this.view.tbxAddAlertTypeFromValue.skin = "skinredbg";
        this.view.flxErrorFromValue.setVisibility(true);
        isValid =false;
      }
      if(this.view.tbxAddAlertTypeToValue.text === "" && this.view.flxAddAlertTypeToValue.isVisible === true){
        this.view.tbxAddAlertTypeToValue.skin = "skinredbg";
        this.view.flxAddAlertTypeErrorToValue.setVisibility(true);
        isValid =false;
      }
    }
    return isValid;
  },
  /*
   * check for unique alert group name
   * @param : alertName
   */
  validateAlertName : function(alertName){
    var self =this;
    var isValid = true;
    var segData =  self.view.segListing.data;
    for(var i=0;i<segData.length;i++){
      if(segData[i].lblAlertName === alertName){
        isValid = false;
        break;
      }
    }
    return isValid;
  },
  validateCategorySupportedLanguages: function() {
    var self = this;
    var isValid = true;
    var segData = self.view.segEditAlertCategoryLanguages.data;
    if (segData[segData.length - 1].lblCategoryLanguage.info.id === "" &&
        segData[segData.length - 1].tbxDisplayName.text === "" &&
        segData[segData.length - 1].tbxDescription.text === "") {
      segData = segData.slice(0, segData.length - 1);
    }
    for (var i = 0; i < segData.length; i++) {
      if (segData[i].lblCategoryLanguage.info.id === "" ||
          segData[i].tbxDisplayName.text === "" ||
          segData[i].tbxDescription.text === "") {
        isValid = false;
        break;
      }
    }
    return isValid;
  },
  /*
   * validate the fields in supported languages segment
   * @return: true or false
   */
  validateSupportedLanguages : function(){
    var self =this;
    var isValid = true;
    var segData =  self.view.segaddalertTypeLanguages.data;
    if(segData[segData.length-1].lblAddAlertTypeSegLanguage.info.id === "" &&
       segData[segData.length-1].tbxDisplayName.text === "" &&
       segData[segData.length-1].tbxDescription.text === ""){
      segData = segData.slice(0,segData.length -1);
    }
    for(var i=0;i<segData.length;i++){
      if(segData[i].lblAddAlertTypeSegLanguage.info.id === "" ||
         segData[i].tbxDisplayName.text === "" ||
         segData[i].tbxDescription.text === ""){
        isValid = false;
        break;
      }
    }
    return isValid;
  },
  /*
   * validate if alert code exsist
   * @return: true or false
   */
  checkForAlertCodeExsist : function(alertCode){
    var self =this;
    var isValid = true;
    var segData =  self.view.segListing.data;
    for(var i=0;i<segData.length;i++){
      if(segData[i].lblAlertCode.info.id === alertCode){
        isValid = false;
        break;
      }
    }
    return isValid;
  },
  clearValidationsForEditAlertCategory: function(){
    var self = this;
    self.view.flxCategoryChannelError.setVisibility(false);
    self.view.flxEditAlertCategoryLanguagesError.setVisibility(false);
  },
  /*
   * clears all the error validations for add/edit alert group screen
   */
  clearValidationsForAddAlertGroup : function(cat,skinWidPath,visWidPath){
    var self = this;
    if(cat === 1){
      skinWidPath.skin = "skntbxLato35475f14px";
      if(visWidPath) visWidPath.setVisibility(false);
    } else if(cat === 2){
      skinWidPath.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
      if(visWidPath) visWidPath.setVisibility(false);
    }else if(cat === 3){
      skinWidPath.skin = "sknflxffffffop100Bordercbcdd1Radius3px";
      if(visWidPath) visWidPath.setVisibility(false);
    } else{
      self.view.dataEntryAlertName.tbxData.skin = "skntbxLato35475f14px";
      self.view.lstBoxAddAlertTypeCode.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
      self.view.dataEntryCode.tbxData.skin = "skntbxLato35475f14px";
      self.view.customListboxApps.flxSelectedText.skin = "sknflxffffffop100Bordercbcdd1Radius3px";
      self.view.customListboxUsertypes.flxSelectedText.skin = "sknflxffffffop100Bordercbcdd1Radius3px";
      self.view.tbxAddAlertTypeFromValue.skin = "skntbxLato35475f14px";
      self.view.tbxAddAlertTypeToValue.skin = "skntbxLato35475f14px";

      self.view.lstBoxAddAlertTypeAttribute.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
      self.view.lstBoxAddAlertTypeCriteria.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
      self.view.lstBoxAddAlertTypeFrequency.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";

      self.view.dataEntryAlertName.flxError.setVisibility(false);
      self.view.flxAddAlertTypeCodeError.setVisibility(false);
      self.view.dataEntryCode.flxError.setVisibility(false);
      self.view.flxRadioGroupError.setVisibility(false);
      self.view.customListboxApps.flxListboxError.setVisibility(false);
      self.view.customListboxUsertypes.flxListboxError.setVisibility(false);
      //attributes
      self.view.flxAttributesError.setVisibility(false);
      self.view.flxAddAlertTypeErrorCriteria.setVisibility(false);
      self.view.flxAddAlertTypeErrorFrequency.setVisibility(false);
      self.view.flxErrorFromValue.setVisibility(false);
      self.view.flxAddAlertTypeErrorToValue.setVisibility(false);
      self.view.flxAddAlertTypeLanguagesError.setVisibility(false);
    }
    self.view.forceLayout();
  },
  /*
   * set details to alert type details screen
   * @param: alert type details
   */
  setDataToAlertTypeDetailsScreen: function(data){
    var self = this;
    var appTypes =[],userTypes = [], appsList ="",userTypesList ="";
    self.view.lblAlertTypeAlertNameValue.text = self.checkForNull(data.alertTypeDefinition.name,self.sNA);
    self.view.lblAlertTypeCodeValue.text =  self.checkForNull(data.alertTypeDefinition.alertCode,self.sNA);
    self.view.lblAlertTypeValue.text = self.checkForNull(data.alertTypeDefinition.alertType,self.sNA);
    self.view.lblAlertTypeDescValue.text = self.checkForNull(data.alertTypeDefinition.typeDescription,self.sNA);
    if(data.alertTypeDefinition.typeStatus === "SID_ACTIVE")
    {
      self.view.lblIconViewDetailsStatus.skin = "sknFontIconActivate";
      self.view.lblViewDetailsAlertTypeStatus.text = kony.i18n.getLocalizedString("i18n.permission.Active");
      self.view.lblViewDetailsAlertTypeStatus.skin = "sknlblLato5bc06cBold14px";
      if(self.tabData.categoryDefintion.categoryStatus === "SID_INACTIVE")
      {
        self.view.flxAlertTypeMessage.setVisibility(true);
        self.view.flxAlertMessage.setVisibility(true);
      }else{
        self.view.flxAlertTypeMessage.setVisibility(false);
        self.view.flxAlertMessage.setVisibility(false);
      }
    }else{
      self.view.lblIconViewDetailsStatus.skin = "sknfontIconInactive";
      self.view.lblViewDetailsAlertTypeStatus.text = kony.i18n.getLocalizedString("i18n.permission.Inactive");
      self.view.lblViewDetailsAlertTypeStatus.skin = "sknlblLatocacacaBold12px";
      self.view.flxAlertTypeMessage.setVisibility(true);
      self.view.flxAlertMessage.setVisibility(true);
      if(self.tabData.categoryDefintion.categoryStatus === "SID_ACTIVE")
      {
        self.view.alertTypeMessage.lblData.text = self.sAlertGroupInactive;
        self.view.alertMessage.lblData.text = self.sAlertGroupInactive;
      }
    }
    self.view.lblViewDetailsAlertDisplayName.text = data.alertTypeDefinition.name;
    appTypes = data.appTypes;
    userTypes = data.userTypes;
    appsList = appTypes.reduce(
      function (list, record) {
        var name = record.isSupported === "true" ? ", " + record.appName  : "";
        return list  + name ;
      }, "");
    userTypesList = userTypes.reduce(
      function (list, record) {
        var name = record.isSupported === "true" ? ", " + record.userTypeName  : "";
        return list  + name ;
      }, "");
    self.view.lblAlertTypeAppsValue.text = self.checkForNull(appsList.substr(2),self.sNA);
    self.view.lblAlertTypeUserTypeValue.text = self.checkForNull(userTypesList.substr(2),self.sNA);
    var attrName = self.checkForNull(data.alertTypeDefinition.alertAttribute,"");
    var attrCriteria = self.checkForNull(data.alertTypeDefinition.alertCondition,"");
    var attrValue1 = self.checkForNull(data.alertTypeDefinition.value1,"");
    var attrValue2 = self.checkForNull(data.alertTypeDefinition.value2,"");
    var attribute ="";
    if(attrName !== ""){
      attribute  = "" + attrName.alertattribute_name +" "+
        (attrCriteria !== "" ? attrCriteria.Name : "") +" "+
        (attrValue1) +" "+(attrValue2);
    }else{
      attribute = "";
    }
    self.view.lblAlertTypeReqAttributeValue.text = self.checkForNull(attribute,self.sNA);
    self.view.forceLayout();
  },
  /*
   * display popup for changing alert type status
   */
  displayDeactivatePopupForAlertType : function(statusId){
    var self = this;
    this.view.flxDeleteAlert.setVisibility(true);
    this.view.popUpDeactivate.btnPopUpCancel.text = self.sNoLeaveAsIs;
    this.view.popUpDeactivate.btnPopUpCancel.right = "160px";
    if (statusId === "SID_ACTIVE") {
      this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Alertgroup_deactivate_heading");
      this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Alertgroup_deactivate_message");
      this.view.popUpDeactivate.btnPopUpDelete.text = self.sYesDeactivate;
      this.view.popUpDeactivate.btnPopUpCancel.right = "180px";
    } else {
      this.view.popUpDeactivate.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Alertgroup_activate_heading");
      this.view.popUpDeactivate.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Alertgroup_activate_message");
      this.view.popUpDeactivate.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagementController.YES_ACTIVATE");
    }
    this.view.forceLayout();
    //overriding actions for deactivate alert group
    this.view.popUpDeactivate.flxPopUpClose.onClick = function(){
      self.view.flxDeleteAlert.setVisibility(false);
    };
    this.view.popUpDeactivate.btnPopUpCancel.onClick = function(){
      self.view.flxDeleteAlert.setVisibility(false);
    };
    this.view.popUpDeactivate.btnPopUpDelete.onClick = function(){
      self.presenter.editAlertGroup(self.activateDeactivate,self.alertGroupScreenContext);
      self.view.flxDeleteAlert.setVisibility(false);
    };
  },
  /*
   * highlight currently selected segment row
   */
  selectCurrentRow : function(){
    var self = this;
    var currInd = self.view.segSequenceList.selectedRowIndex[1];
    var segData = self.view.segSequenceList.data;
    var currRowData = self.view.segSequenceList.data[currInd];
    for(var i=0;i<segData.length;i++){
      if(i !== currInd && segData[i].flxAlertSequenceMap.skin === "sknFlxe1e5ed100O"){
        segData[i].flxAlertSequenceMap.skin = "sknbackGroundffffff100";
      } else{ //do nothing
      }
      self.view.segSequenceList.setDataAt(segData[i], i);
    }
    self.disableArrowsSequencePopup(currInd);
    currRowData.flxAlertSequenceMap.skin = "sknFlxe1e5ed100O";
    self.view.segSequenceList.setDataAt(currRowData, currInd);
    self.reorderAlertIndex = currInd;
    self.view.forceLayout();
  },
  /*
   * set visibility for breadcrumb buttons
   * @param : category - alertgroup or subalert
   */
  toggleBreadcrumbButtons :  function(category){
    var self = this;
    self.view.flxBreadcrumbAlerts.setVisibility(true);
    if(category === "alertgroup"){
      self.view.breadcrumbAlerts.fontIconBreadcrumbsRight2.setVisibility(false);
      self.view.breadcrumbAlerts.btnPreviousPage.setVisibility(false);
    }else if(category === "subalert"){
      self.view.breadcrumbAlerts.fontIconBreadcrumbsRight2.setVisibility(true);
      self.view.breadcrumbAlerts.btnPreviousPage.setVisibility(true);
    } else{
      self.view.flxBreadcrumbAlerts.setVisibility(false);
    }
  },
  /*
   * check for null,empty,undefined
   *@param : value, returnValue("","N.A")
   *@return : expected value
   */
  checkForNull : function(value,returnValue){
    if(value){
      return value;
    }else{
      return returnValue;
    }
  },
  validateFields : function(){
    if(this.view.txtbxSubAlertName.text===""||this.view.lstbxSubAlertCode.selectedKey==="Select"||this.view.txtAlertDescription.text===""){
      if(this.view.txtbxSubAlertName.text===""){
        this.view.txtbxSubAlertName.skin="skinredbg";
        this.view.flxErrorAlertName.setVisibility(true);
      }
      if(this.view.lstbxSubAlertCode.selectedKey==="Select"){
        this.view.lstbxSubAlertCode.skin="redListBxSkin";
        this.view.lblErrorAlertCode.text=kony.i18n.getLocalizedString("i18n.frmAlertsManagement.SelectAlertCode");
        this.view.flxErrorAlertCode.setVisibility(true);
      }
      if(this.view.txtAlertDescription.text===""){
        this.view.txtAlertDescription.skin="skinredbg";
        this.view.flxNoDescriptionError.setVisibility(true);
      }
      return false;
    }else{
      this.view.txtbxSubAlertName.skin="skntbxLato35475f14px";
      this.view.lstbxSubAlertCode.skin="sknlbxBgffffffBorderc1c9ceRadius3Px";
      this.view.txtAlertDescription.skin="skntbxLato35475f14px";
      this.view.flxNoDescriptionError.setVisibility(false);
      this.view.flxErrorAlertCode.setVisibility(false);
      this.view.flxErrorAlertName.setVisibility(false);
      return true;
    }
  },
  toCheckAlertNameAvailability: function () {
    var currentAlertName = this.view.txtbxSubAlertName.text.trim();
    var allAlerts = this.alertsJSON;
    var existingAlertNames = [];
    var doesExsist = false;
    for (var i = 0; i < allAlerts.length; i++) {
      existingAlertNames.push(allAlerts[i].name);
    }
    if(this.alertGroupCurrAction===this.alertGroupActionConfig.EDIT){
      var selectedIndex=this.view.segSubAlerts.selectedIndices[0][1];
      var editAlertName = this.view.segSubAlerts.data[selectedIndex].lblDisplayName;
      existingAlertNames = existingAlertNames.filter(function(user){return user !== editAlertName;});
    }
    for (var j = 0; j < existingAlertNames.length; j++) {
      if (currentAlertName === existingAlertNames[j]) {
        doesExsist = true;
        break;
      } else {
        doesExsist = false;
      }
    }
    if (doesExsist) {
      if(this.view.txtbxSubAlertName.skin !== "skntbxBordereb30173px"){
        this.view.flxAlertNameAvailable.setVisibility(false);
        this.view.flxErrorAlertName.setVisibility(true);
        this.view.lblErrorAlertName.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.AlertNameNotAvailable");
        this.view.lblErrorAlertName.skin = "sknLabelRed";
        this.view.txtbxSubAlertName.skin = "skntbxBordereb30173px";
      }
      return false;
    } else {
      this.view.flxErrorAlertName.setVisibility(false);
      this.view.flxAlertNameAvailable.setVisibility(true);
      this.view.lblAlertNameAvailable.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.AlertNameAvailable");
      this.view.lblAlertNameAvailable.skin = "sknlblLato5bc06cBold14px";
      this.view.txtbxSubAlertName.skin = "skntxtbxDetails0bbf1235271384a";
      return true;
    }
  },
  checkAlertCode : function(){
    var currentAlertCode = this.view.lstbxSubAlertCode.selectedKey;
    var allAlerts = this.alertsJSON;
    var existingAlerCodes = [];
    var doesExsist = false;
    if(this.alertGroupCurrAction===this.alertGroupActionConfig.EDIT)
      return true;
    for (var i = 0; i < allAlerts.length; i++) {
      existingAlerCodes.push(allAlerts[i].code);
    }
    for (var j = 0; j < existingAlerCodes.length; j++) {
      if (currentAlertCode === existingAlerCodes[j]) {
        doesExsist = true;
        break;
      } else {
        doesExsist = false;
      }
    }
    if (doesExsist) {
      if(this.view.txtbxSubAlertName.skin !== "redListBxSkin"){
        this.view.flxErrorAlertCode.setVisibility(true);
        this.view.lblErrorAlertCode.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Alert_code_already_exist");
        this.view.lblErrorAlertCode.skin = "sknLabelRed";
        this.view.lstbxSubAlertCode.skin = "redListBxSkin";
      }
      return false;
    } else {
      this.view.flxErrorAlertCode.setVisibility(false);
      this.view.lstbxSubAlertCode.skin = "sknlbxBgffffffBorderc1c9ceRadius3Px";
      return true;
    }
  },
  getSubAlertViewData : function(){
    var selectedIndex=this.view.segSubAlerts.selectedIndices[0][1];
    var subAlertId=this.view.segSubAlerts.data[selectedIndex].lblCode.info.value;
    var inputData={
      "SubAlertId":subAlertId
    };
    this.presenter.getSubAlertView(inputData);
  },
  createEditAlert : function(alertData){
    if(this.alertGroupCurrAction===this.alertGroupActionConfig.EDIT)
      this.callEditSubAlert(alertData);
    else if(this.alertGroupCurrAction===this.alertGroupActionConfig.CREATE)
      this.callCreateSubAlert(alertData);
    this.view.flxSubAlertCodeGrey.setVisibility(false);
    this.view.flxAddSubAlertPopUp.setVisibility(false);
  },
  callCreateSubAlert :function(createAlertData){
    this.presenter.createSubAlert(createAlertData);
  },
  callEditSubAlert :function(EditAlertData){
    this.presenter.editSubAlert(EditAlertData,this.view.flxSubAlerts.isVisible);
  },
  setPreviewData :function(){
    var emailData;
    if(this.view.flxViewTemplate.isVisible===true)
      this.showViewPreviewData();
    else if(this.view.flxAddTemplate.isVisible===true)
      this.showAddPreviewData();
  },
  showViewPreviewData : function(){
    if(this.view.flxViewNotificationCenter.isVisible===true){
      this.view.lblPreviewSubHeader1.text=this.populateAlertPreview(this.view.ViewTemplateCenter.lblTitleValue.text);
      this.view.btnNotifCenter.setVisibility(true);
      this.view.lblPreviewTemplateBody.text=this.view.ViewTemplateCenter.lblChannelMsg.text;
      this.view.lblPreviewTemplateBody.setVisibility(true);
      this.view.rtxViewer.setVisibility(false);
      this.view.flxTemplatePreviewHeader.setVisibility(true);
      this.view.flxPopUpButtons.top="3px";
      this.setSkinForChannelTabs(this.view.btnNotifCenter);
      this.view.flxPreviewPopup.setVisibility(true);
    }
    else
      this.view.btnNotifCenter.setVisibility(false);
    if(this.view.flxViewEmailTemplate.isVisible===true){
      //this.emailRtxData=document.getElementById("iframe_rtxEmailTemplate").contentWindow.document.getElementById("viewer").innerHTML;
      this.view.btnEmail.setVisibility(true);
      this.view.flxPreviewPopup.setVisibility(true);
      this.view.lblPreviewTemplateBody.setVisibility(false);
      this.view.rtxViewer.setVisibility(true);
      this.view.flxTemplatePreviewHeader.setVisibility(true);
      this.view.flxPopUpButtons.top="3px";
      this.view.lblPreviewSubHeader1.text=this.populateAlertPreview(this.view.lblEmailTitleValue.text);
      this.setSkinForChannelTabs(this.view.btnEmail);
      var emailData=this.populateAlertPreview(this.emailRtxData);
      if(document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer")) {
        document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML = emailData;
      } else {
        if(!document.getElementById("iframe_rtxViewer").newOnload) {
          document.getElementById("iframe_rtxViewer").newOnload = document.getElementById("iframe_rtxViewer").onload;
        }
        document.getElementById("iframe_rtxViewer").onload = function() {
          document.getElementById("iframe_rtxViewer").newOnload();
          document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML =emailData;
        };
      }
    }
    else
      this.view.btnEmail.setVisibility(false);
    if(this.view.flxViewPushNotification.isVisible===true){
      this.view.lblPreviewSubHeader1.text=this.populateAlertPreview(this.view.ViewTemplatePush.lblTitleValue.text);
      this.view.btnPushNoti.setVisibility(true);
      this.view.lblPreviewTemplateBody.setVisibility(true);
      this.view.rtxViewer.setVisibility(false);
      this.view.flxTemplatePreviewHeader.setVisibility(true);
      this.view.flxPopUpButtons.top="3px";
      this.view.lblPreviewTemplateBody.text=this.populateAlertPreview(this.view.ViewTemplatePush.lblChannelMsg.text);
      this.setSkinForChannelTabs(this.view.btnPushNoti);
      this.view.flxPreviewPopup.setVisibility(true);
    }
    else
      this.view.btnPushNoti.setVisibility(false);
    if(this.view.flxViewSMSTemplate.isVisible===true){
      this.view.btnSMS.setVisibility(true);
      this.view.lblPreviewTemplateBody.setVisibility(true);
      this.view.rtxViewer.setVisibility(false);
      this.setSkinForChannelTabs(this.view.btnSMS);
      this.view.lblPreviewTemplateBody.text=this.populateAlertPreview(this.view.ViewTemplateSMS.lblChannelMsg.text);
      this.view.flxPreviewPopup.setVisibility(true);
      this.view.flxTemplatePreviewHeader.setVisibility(false);
      this.view.flxPopUpButtons.top="70px";
    }
    else
      this.view.btnSMS.setVisibility(false);
    if(this.view.flxPreviewPopup.isVisible===false)
      this.view.toastMessage.showErrorToastMessage(kony.i18n.getLocalizedString("i18n.frmAlertsManagement.NoContentAdded"), this);
    this.view.forceLayout();
  },
  showAddPreviewData : function(){
    this.emailRtxData=document.getElementById("iframe_rtxEmailTemplate").contentWindow.document.getElementById("editor").innerHTML;
    if(this.view.flxNotiCenterTitle.isVisible===true&&this.view.txtNotiCenterMsg.text!==""&&this.view.tbxNotiCenterTitle.text!==""){
      this.view.lblPreviewSubHeader1.text=this.populateAlertPreview(this.view.tbxNotiCenterTitle.text);
      this.view.btnNotifCenter.setVisibility(true);
      this.setSkinForChannelTabs(this.view.btnNotifCenter);
      this.view.lblPreviewTemplateBody.text=this.populateAlertPreview(this.view.txtNotiCenterMsg.text);
      this.view.lblPreviewTemplateBody.setVisibility(true);
      this.view.flxTemplatePreviewHeader.setVisibility(true);
      this.view.flxPopUpButtons.top="3px";
      this.view.rtxViewer.setVisibility(false);
      this.view.flxPreviewPopup.setVisibility(true);
    }
    else
      this.view.btnNotifCenter.setVisibility(false);
    if(this.view.flxAddEmailTemplate.isVisible===true&&this.view.tbxEmailSubject.text!==""&&this.emailRtxData!==""){
      this.view.lblPreviewSubHeader1.text=this.populateAlertPreview(this.view.tbxEmailSubject.text);
      this.view.flxPreviewPopup.setVisibility(true);
      this.view.btnEmail.setVisibility(true);
      this.view.flxTemplatePreviewHeader.setVisibility(true);
      this.view.flxPopUpButtons.top="3px";
      this.view.lblPreviewTemplateBody.setVisibility(false);
      this.view.rtxViewer.setVisibility(true);
      this.setSkinForChannelTabs(this.view.btnEmail);
      var emailData=this.populateAlertPreview(this.emailRtxData);
      if(document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer")) {
        document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML = emailData;
      } else {
        if(!document.getElementById("iframe_rtxViewer").newOnload) {
          document.getElementById("iframe_rtxViewer").newOnload = document.getElementById("iframe_rtxViewer").onload;
        }
        document.getElementById("iframe_rtxViewer").onload = function() {
          document.getElementById("iframe_rtxViewer").newOnload();
          document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML =emailData;
        };
      }
    }
    else
      this.view.btnEmail.setVisibility(false);
    if(this.view.flxAddPushNotification.isVisible===true&&this.view.txtPushNotification.text!==""&&this.view.tbxPushNotificationTitle.text!==""){
      this.view.lblPreviewSubHeader1.text=this.populateAlertPreview(this.view.tbxPushNotificationTitle.text);  
      this.view.btnPushNoti.setVisibility(true);
      this.setSkinForChannelTabs(this.view.btnPushNoti);
      this.view.lblPreviewTemplateBody.text=this.populateAlertPreview(this.view.txtPushNotification.text);
      this.view.lblPreviewTemplateBody.setVisibility(true);
      this.view.flxTemplatePreviewHeader.setVisibility(true);
      this.view.flxPopUpButtons.top="3px";
      this.view.flxPreviewPopup.setVisibility(true);
      this.view.rtxViewer.setVisibility(false);
    }
    else
      this.view.btnPushNoti.setVisibility(false);
    if(this.view.flxAddSMSTemplate.isVisible===true&&this.view.txtSMSMsg.text!==""){
      this.view.btnSMS.setVisibility(true);
      this.setSkinForChannelTabs(this.view.btnSMS);
      this.view.flxTemplatePreviewHeader.setVisibility(false);
      this.view.flxPopUpButtons.top="70px";
      this.view.lblPreviewTemplateBody.text=this.populateAlertPreview(this.view.txtSMSMsg.text);
      this.view.lblPreviewTemplateBody.setVisibility(true);
      this.view.flxPreviewPopup.setVisibility(true);
      this.view.rtxViewer.setVisibility(false);
    }
    else
      this.view.btnSMS.setVisibility(false);
    if(this.view.flxPreviewPopup.isVisible===false)
      this.view.toastMessage.showErrorToastMessage(kony.i18n.getLocalizedString("i18n.frmAlertsManagement.NoContentAdded"), this);
    this.view.forceLayout();
  },
  /*
   * create request param for reordering alert groups
   */
  updateSequence : function(){
    var self = this;
    var i;
    var segData = self.view.segSequenceList.data;
    var typeOrder ={};
    var categoryOrder = {};
    var updateParam;
    if(self.reorderAlert === true){
      for(i=0;i< segData.length;i++){
        typeOrder[segData[i].id] = i+1;
      }
      updateParam ={"typeOrder" : typeOrder,
                    "alertCategoryCode" :self.view.lblCategoryCodeValue.text};
      self.presenter.updateAlertTypeSequence(updateParam);
    }else{
      for(i=0;i< segData.length;i++){
        categoryOrder[segData[i].id] = i+1;
      }
      updateParam ={"categoryOrder" : categoryOrder};
      self.presenter.updateAlertCategorySequence(updateParam);
    }
  },
  /*
  *function to fill edit screen for alert group
  *@param: alert group complete details
  */
  fillAlertGroupScreenForEdit : function(data){
    var self =this;

    self.view.lblAddAlertTypeName.text = data.alertTypeDefinition.name;
    self.view.dataEntryAlertName.tbxData.text = data.alertTypeDefinition.name;
    self.view.dataEntryCode.tbxData.text = data.alertTypeDefinition.alertCode;
    self.view.lstBoxAddAlertTypeCode.selectedKey = data.alertTypeDefinition.alertCode;
    self.view.addAlertTypeStatusSwitch.switchToggle.selectedIndex = data.alertTypeDefinition.typeStatus === "SID_ACTIVE" ?
      0:1;
    self.view.lstBoxAddAlertTypeCode.skin = "lstBxBre1e5edR3pxBgf5f6f8Disable";
    self.view.lstBoxAddAlertTypeCode.setEnabled(false);
    //assign alertype radiobuttons
    var alertType = data.alertTypeDefinition.alertType;
    var radioData = [{"selectedImg":self.radioSelected,"unselectedImg":self.radioNotSelected,
                      "src": (alertType.trim()).toLowerCase().indexOf("global")>=0 ? self.radioSelected : self.radioNotSelected,
                      "value":"Global Alert","id":"GA"},
                     {"selectedImg":self.radioSelected,"unselectedImg":self.radioNotSelected,
                      "src":(alertType.trim()).toLowerCase().indexOf("user") >=0 ? self.radioSelected : self.radioNotSelected,
                      "value":"User Alert","id":"UA"},];
    self.view.customRadioButtonType.setData(radioData);
    //assign attibute fields
    if(data.alertTypeDefinition.alertAttribute){
      var alertDefinition = data.alertTypeDefinition;
      self.view.imgCheckboxAttribute.src = self.checkboxnormal;
      self.view.lstBoxAddAlertTypeAttribute.selectedKey = alertDefinition.alertAttribute.alertattribute_id;
      self.changeUIForAttibuteType("attribute");
      self.view.lstBoxAddAlertTypeCriteria.selectedKey = alertDefinition.alertCondition.id;
      self.changeUIForAttibuteType("criteria");
      self.view.tbxAddAlertTypeFromValue.text = alertDefinition.value1;
      self.view.tbxAddAlertTypeToValue.text = alertDefinition.value2;
    }else{
      self.view.imgCheckboxAttribute.src = self.checkboxselected;
    }
    self.toggleAttributes();
    //assign customlistboxes
    this.selectAppsForEdit(data.appTypes);
    this.selectUsersForEdit(data.userTypes);
    //assign languages
    var langPref = data.displayPreferences;
    self.assignLanguagesSupportedAlertTypes(langPref);

  },
  /*
   * selects the apps exsisting for alert group
   * @param : app preferences list of alert group
   */
  selectAppsForEdit : function(appData){
    var selectInd = [];
    var segData = this.view.customListboxApps.segList.data;
    for(var i=0;i<segData.length;i++){
      for(var j=0;j<appData.length;j++){
        if(appData[j].isSupported === "true" && appData[j].appId === segData[i].id){
          selectInd.push(i);
        } 
      }
    }
    this.view.customListboxApps.segList.selectedRowIndices = [[0,selectInd]];
    if(segData.length === selectInd.length){
      this.view.customListboxApps.imgCheckBox.src = this.checkboxselected;
      this.view.customListboxApps.lblSelectedValue.text = this.sAll;
    }else{
      this.view.customListboxApps.imgCheckBox.src = this.checkboxnormal;
      this.view.customListboxApps.lblSelectedValue.text = selectInd.length+ " " +this.sSelected;
    }
  },
  /*
   * selects the apps exsisting for alert group
   * @param : user preferences list of alert group
   */
  selectUsersForEdit : function(userData){
    var selectInd = [];
    var segData = this.view.customListboxUsertypes.segList.data;
    for(var i=0;i<segData.length;i++){
      for(var j=0;j<userData.length;j++){
        if(userData[j].isSupported === "true" && userData[j].userTypeId === segData[i].id){
          selectInd.push(i);
        } 
      }
    }
    this.view.customListboxUsertypes.segList.selectedRowIndices = [[0,selectInd]];
    if(segData.length === selectInd.length){
      this.view.customListboxUsertypes.imgCheckBox.src = this.checkboxselected;
      this.view.customListboxUsertypes.lblSelectedValue.text = this.sAll;
    }else{
      this.view.customListboxUsertypes.imgCheckBox.src = this.checkboxnormal;
      this.view.customListboxUsertypes.lblSelectedValue.text = selectInd.length+ " "+this.sSelected;
    }
  },
  /*
   * assigns exsisting languages to supported lang segment
   * @param : language preferences of alert group
   */
  assignLanguagesSupportedAlertTypes : function(langData){
    var self =this;
    var mapData =[];
    var currLocale = (kony.i18n.getCurrentLocale()).replace("_","-");
    var getName = self.getLanguageNameForCode(currLocale);
    mapData = langData.map(function(rec){
      var rowData = self.getLangSegDataMapJson();
      rowData.lblAddAlertTypeSegLanguage.text = self.getLanguageNameForCode(rec.LanguageCode) || self.sLang;
      rowData.lblAddAlertTypeSegLanguage.info.id = rec.LanguageCode;
      rowData.lstBoxSelectLanguage.selectedKey = rec.LanguageCode;
      rowData.lblAddAlertTypeSegDisplayName.text = rec.DisplayName;
      rowData.tbxDisplayName.text = rec.DisplayName;
      rowData.lblAddAlertTypeSegDescription.text = rec.Description;
      rowData.tbxDescription.text = rec.Description;
      return rowData;
    });
    var defaultRow,currRow ;
    //set default locale language to first row
    for(var i=0;i< mapData.length;i++){
      if(mapData[i].lblAddAlertTypeSegLanguage.info.id === currLocale){
        currRow = mapData[i];
        mapData[i] = mapData[0];
        mapData[0] = currRow;
        break;
      }
    } 
    self.view.segaddalertTypeLanguages.setData(mapData);
    self.view.segaddalertTypeLanguages.info = {"segData" : langData};
    if(mapData.length <= 0){
      self.addDefaultRowDataForLangSeg();
    } else{
      self.addNewRowAtEnd(null,[0,mapData.length-1]);
    }
    self.view.forceLayout();
  },
  /*
   * creates request param for create and edit alert group
   * @return: request Param
   */
  createAlertGroupRequestParam :  function(){
    var self = this;
    var appList = self.view.customListboxApps.segList.selectedRowItems;
    var usersList =  self.view.customListboxUsertypes.segList.selectedRowItems;
    var appPrefer ={},userPrefer ={};
    var attribute,condition,attrId;
    for(var i=0;i<appList.length;i++){
      appPrefer[appList[i].id] = true;
    }
    for(var j=0;j<usersList.length;j++){
      userPrefer[usersList[j].id] = true;
    }
    var lang =  self.view.segaddalertTypeLanguages.data;
    var finalLangList = [];
    for(var k=0;k<lang.length;k++){
      if(lang[k].lblAddAlertTypeSegLanguage.info.id !== ""){
        finalLangList.push(lang[k]);
      }
    }
    var langPrefer = finalLangList.map(function(rec){
      return {
        "languageCode": rec.lblAddAlertTypeSegLanguage.info.id,
        "displayName": rec.lblAddAlertTypeSegDisplayName.text,
        "description": rec.lblAddAlertTypeSegDescription.text
      };
    });
    if(self.view.imgCheckboxAttribute.src === self.checkboxselected){
      attribute = self.view.lstBoxAddAlertTypeAttribute.selectedKeyValue;
      attrId = attribute[0];
      if((attribute[1].toLowerCase()).indexOf("frequency") >= 0){
        condition = self.view.lstBoxAddAlertTypeFrequency.selectedKey;
      }else{
        condition = self.view.lstBoxAddAlertTypeCriteria.selectedKey;
      }
      var attrCriteria = self.view.lstBoxAddAlertTypeCriteria.selectedKey;
    } else {
      attrId = "";
      condition = "";
      self.view.tbxAddAlertTypeFromValue.text = "";
      self.view.tbxAddAlertTypeToValue.text = "";
    }
    var reqParam = {
      "alertCategoryCode":self.view.lblCategoryCodeValue.text,
      "alertCode": self.view.lstBoxAddAlertTypeCode.selectedKey,
      "alertName": self.view.dataEntryAlertName.tbxData.text, 
      "statusId": "SID_ACTIVE",
      "isGlobalAlert": self.view.customRadioButtonType.selectedValue.id === "UA" ? "FALSE":"TRUE",
      "appPreferences": appPrefer,
      "userTypePreferences": userPrefer,
      "attributeId": attrId || "",
      "conditionId": condition || "",
      "value1": self.view.tbxAddAlertTypeFromValue.text || "",
      "value2": self.view.tbxAddAlertTypeToValue.text || "",
      "addedDisplayPreferences": langPrefer,
      "removedDisplayPreferences": []
    };
    return reqParam;
  },
  /*
   * form request param for edit alert group
   * @return: edit request param
   */
  editAlertGroupRequestParam : function(){
    var self =this;
    var appPrefer ={},userPrefer ={};
    var selAppList = self.view.customListboxApps.segList.selectedIndices[0][1];
    var selUsersList =  self.view.customListboxUsertypes.segList.selectedIndices[0][1];
    var appSegData = self.view.customListboxApps.segList.data;
    var userSegData = self.view.customListboxUsertypes.segList.data;
    var initialParam = self.createAlertGroupRequestParam();
    for(var i=0;i<appSegData.length;i++){
      if(selAppList.contains(i)){
        appPrefer[appSegData[i].id] = true;
      }else{
        appPrefer[appSegData[i].id] = false;
      }
    }
    for(var j=0;j<userSegData.length;j++){
      if(selUsersList.contains(j)){
        userPrefer[userSegData[j].id] = true;
      }else{
        userPrefer[userSegData[j].id] = false;
      }
    }
    var removedPref = self.getAddedRemovedLangList();
    initialParam.removedDisplayPreferences = removedPref.removed;
    var addedLang = removedPref.added.map(function(rec){
      return {
        "languageCode": rec.lblAddAlertTypeSegLanguage.info.id,
        "displayName": rec.lblAddAlertTypeSegDisplayName.text,
        "description": rec.lblAddAlertTypeSegDescription.text
      };
    });
    initialParam.statusId = self.view.addAlertTypeStatusSwitch.switchToggle.selectedIndex === 0 ?
      "SID_ACTIVE" : "SID_INACTIVE";
    initialParam.appPreferences = appPrefer;
    initialParam.userTypePreferences = userPrefer;
    initialParam.addedDisplayPreferences = addedLang;
    initialParam.alertCode = self.view.lstBoxAddAlertTypeCode.selectedKey;
    return initialParam;
  },
  /*
   * function to get removed lang list
   * @return: removed id's list
   */
  getAddedRemovedLangList : function(){
    var self = this;
    var originalList = self.view.segaddalertTypeLanguages.info.segData;
    var newList = self.view.segaddalertTypeLanguages.data;
    var orgId = originalList.map(function(rec){
      return rec.LanguageCode;
    });
    var newId = newList.map(function(rec){
      return rec.lblAddAlertTypeSegLanguage.info.id;
    });
    var removed = self.getDiffOfArray(orgId, newId);
    var added = self.getDiffOfArray(newId, orgId);
    var addList = newList.filter(function(rec){
      for(var i=0;i<newId.length;i++){
        if(newId[i] !== "" && newId[i] === rec.lblAddAlertTypeSegLanguage.info.id){
          return rec;
        }
      }

    });
    return {"added":addList,"removed":removed};
  },
  /*
   * call presentation cntrl for create alert group
   */
  createNewAlertGroup : function(){
    var self =this;
    var reqParam = self.createAlertGroupRequestParam();
    self.presenter.createAlertGroup(reqParam);
  },
  /*
   * call presentation cntrl for create alert group
   */
  editAlertGroup : function(){
    var self =this;
    var reqParam = self.editAlertGroupRequestParam();
    var context = self.alertGroupScreenContext;
    self.presenter.editAlertGroup(reqParam,context);
  },
  /*
   * function to change UI widgets based on attributes selection
   * @param : categroy - "attribute" or "criteris"
   */
  changeUIForAttibuteType : function(category){
    var self =this;
    self.view.flxAddAlertTypeAttributeCriteria.setVisibility(true);
    self.view.flxAddAlertTypeFreqValue.setVisibility(false);
    self.view.flxAddAlertTypeFromValue.setVisibility(true);
    self.view.flxAddAlertTypeToValue.setVisibility(false);
    self.view.lblAddAlertTypeFromValue.text =kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Value");
    if((self.view.lstBoxAddAlertTypeAttribute.selectedKey).indexOf("FREQUENCY") >= 0 && category === "attribute"){
      self.view.flxAddAlertTypeFreqValue.setVisibility(true);
      self.view.flxAddAlertTypeAttributeCriteria.setVisibility(false);
      self.view.flxAddAlertTypeFromValue.setVisibility(false);
      self.view.flxAddAlertTypeToValue.setVisibility(false);
    }
    if((self.view.lstBoxAddAlertTypeCriteria.selectedKey).indexOf("BETWEEN")>= 0 && category === "criteria"){
      self.view.lblAddAlertTypeFromValue.text = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Value_from");
      self.view.flxAddAlertTypeFromValue.setVisibility(true);
      self.view.flxAddAlertTypeToValue.setVisibility(true);
    }
  },
  /*
   * function to get language name value for language code
   *@param : language code
   *@return : language name value
   */
  getLanguageNameForCode : function(code){
    var self =this;
    for(var i=0;i<self.langToShow.length;i++){
      if(code === self.langToShow[i][0]){
        return self.langToShow[i][1];
      }
    }
  },
  /*
  * function to get difference of two sets.
  */
  getDiffOfArray: function (a1, a2) {
    return a1.filter(function(x) {
      if(a2.indexOf(x) >= 0) return false;
      else return true;
    });
  },
  /*
   * create payload for activate/deactivate alert group
   * @param: alertgroup details
   */
  activateDeactivateAlertGroup : function(data){
    var self = this;
    var alertType = data.alertTypeDefinition.alertType.toLowerCase();
    var reqParam = {
      "alertCategoryCode":self.view.breadcrumbAlerts.btnBackToMain.info.id,
      "alertCode": data.alertTypeDefinition.alertCode,
      "alertName": data.alertTypeDefinition.name, 
      "statusId": data.alertTypeDefinition.typeStatus === "SID_ACTIVE" ? "SID_INACTIVE" : "SID_ACTIVE",
      "isGlobalAlert": alertType.indexOf("global") >= 0 ? "TRUE" : "FALSE",
      "appPreferences": {},
      "userTypePreferences": {},
      "attributeId": data.alertTypeDefinition.alertAttribute ?
      data.alertTypeDefinition.alertAttribute.alertattribute_id : "",
      "conditionId":  data.alertTypeDefinition.alertCondition ?
      data.alertTypeDefinition.alertCondition.id : "",
      "value1": data.alertTypeDefinition.value1 || "",
      "value2": data.alertTypeDefinition.value2 || "",
      "addedDisplayPreferences": [],
      "removedDisplayPreferences": []
    };
    self.activateDeactivate = reqParam;
    self.displayDeactivatePopupForAlertType(data.alertTypeDefinition.typeStatus);
  }
});
