define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxArrow **/
    AS_FlexContainer_d97070127c414d6ea7f64f8ad6995452: function AS_FlexContainer_d97070127c414d6ea7f64f8ad6995452(eventobject, context) {
        var self = this;
        this.executeOnParent("showSelectedRowServices");
    },
    /** onClick defined for flxOptions **/
    AS_FlexContainer_e46c0024c34f42c5a0ea750a84ed1be9: function AS_FlexContainer_e46c0024c34f42c5a0ea750a84ed1be9(eventobject, context) {
        var self = this;
        this.executeOnParent("toggleVisibility");
    }
});