define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxArrow **/
    AS_FlexContainer_j3adcbdcf0844bb3ad620cef52c39678: function AS_FlexContainer_j3adcbdcf0844bb3ad620cef52c39678(eventobject, context) {
        var self = this;
        this.executeOnParent("showSelectedRowServices");
    },
    /** onClick defined for flxOptions **/
    AS_FlexContainer_c18ddefd471a4e24ba76ed00d9c4da80: function AS_FlexContainer_c18ddefd471a4e24ba76ed00d9c4da80(eventobject, context) {
        var self = this;
        this.executeOnParent("toggleVisibility");
    }
});