define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmPrivacyPolicy **/
    AS_Form_i89d0e54656b48bc988374a7ad2049dc: function AS_Form_i89d0e54656b48bc988374a7ad2049dc(eventobject) {
        var self = this;
        this.privacyPolicyPreShowActions();
    },
    /** onDeviceBack defined for frmPrivacyPolicy **/
    AS_Form_bb6d12bc249845dfa5542e98cccc482f: function AS_Form_bb6d12bc249845dfa5542e98cccc482f(eventobject) {
        var self = this;
        //registered
        this.onBrowserBack();
    }
});