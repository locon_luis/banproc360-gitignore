define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgPreviousArrow **/
    AS_Image_j56563ebfafd4b3d80502fec0c98cba7: function AS_Image_j56563ebfafd4b3d80502fec0c98cba7(eventobject, x, y) {
        var self = this;
        this.prevPage();
    },
    /** onTouchStart defined for imgNextArrow **/
    AS_Image_ed3c712efdb8401b8975d8b184830e46: function AS_Image_ed3c712efdb8401b8975d8b184830e46(eventobject, x, y) {
        var self = this;
        this.nextPage();
    },
    /** preShow defined for frmFAQ **/
    AS_Form_e54ab14a468c42918556ffa2e5676112: function AS_Form_e54ab14a468c42918556ffa2e5676112(eventobject) {
        var self = this;
        this.preShowActions();
    },
    /** onDeviceBack defined for frmFAQ **/
    AS_Form_c9a507440aa44451968a3593664409f3: function AS_Form_c9a507440aa44451968a3593664409f3(eventobject) {
        var self = this;
        //registered
        this.onBrowserBack();
    }
});