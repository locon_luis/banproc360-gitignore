define({ 

  //Type your controller code here 

  /**
    * Function preshow of actions
  **/
  TnCJSON: null,
  records: null,
  allRecords: null,
  editPage:0,
  termsAndConditions_id:null,
  inFilter:false,
  TnCList:[],
  allTnCList:[],
  supportedApps:[],
  checkboxselected : "checkboxselected.png",
  checkboxnormal : "checkboxnormal.png",
  checkbox :"checkbox.png",
  sAll : kony.i18n.getLocalizedString("i18n.frmAlertsManagement.All"),
  sSelected: kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Selected"),
  preShowActions :function(){
	//this.view.flxPhraseStatus.setVisibility(false);
  },
  hideHeaderButtons: function() {
    this.view.mainHeader.flxButtons.setVisibility(false);
    this.view.mainHeader.flxHeaderSeperator.setVisibility(false);
  },
  showHeaderButtons: function() {
    this.view.mainHeader.flxButtons.setVisibility(true);
    this.view.mainHeader.flxHeaderSeperator.setVisibility(true);
  },
  preShowTermsAndConditions: function(){
    this.view.flxBreadcrumb.setVisibility(false);
    this.view.flxDetailTermsAndConditions.setVisibility(false);
    this.view.mainHeader.lblUserName.text=kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
    this.view.mainHeader.lblHeading =  kony.i18n.getLocalizedString("i18n.TermsAndConditions.addTnC");
    this.view.subHeader.tbxSearchBox.text = "";
    this.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100";
    this.view.subHeader.flxClearSearchImage.setVisibility(false);
    this.view.flxDetailTermsAndConditions.setVisibility(false);
    this.hideHeaderButtons();
    this.hideNoResultsFound();
    this.view.staticData.skin = "skntxtAreaLato0i538905e260549Stat"; 
    this.view.staticData.flxStaticContantData.height = kony.os.deviceInfo().screenHeight - 330 + "px";
    this.view.staticData.height = kony.os.deviceInfo().screenHeight - 330 + "px";
    this.view.flxMain.height=kony.os.deviceInfo().screenHeight+"px";
    this.view.flxDetailTermsAndConditions.height = kony.os.deviceInfo().screenHeight - 150 +"px";
    this.view.flxTermsAndConditionsList.setVisibility(true);
    //this.view.flxPhraseStatus.setVisibility(false);
    this.flowActions();
    this.view.forceLayout();

  },
  hideAll: function(){
    this.view.flxTermsAndConditionsList.setVisibility(false);
    this.view.flxApplicableAppsFilter.setVisibility(false);
    this.view.flxDetailTermsAndConditions.setVisibility(false);
    this.view.customListboxApps.flxSegmentList.setVisibility(false);
    this.view.flxTandCPopUp.setVisibility(false);
    this.view.flxContentRichTxt.skin="sknflxffffffop100Bordercbcdd1Radius3px";
    this.view.tbxURLcontent.skin="skntxtbx484b5214px";
    this.view.tbxTitle.skin="skntxtbx484b5214px";
	this.view.flxTitleError.setVisibility(false);
	this.view.flxContentError.setVisibility(false);
    this.view.flxTitleError.setVisibility(false);
  },
  showListingScreen: function() {
    this.view.flxScrollMainContent.setVisibility(true);
    this.view.flxTandCList.setVisibility(true);
    this.view.mainHeader.lblHeading.text = "Terms And Conditions";
  },
  toTnCSegment: function(tnc) {
    var self = this;
    var str = "";
    var pref = tnc.appPreferences;
    for(var i=0;i<pref.length;i++){
      if(pref[i].isSupported === "true"){
        str += pref[i].appName + ", ";
      } 
    }
    if (str.substr(str.length-2,2) == ", ") {
      str = str.substring(0,str.length-2);
    }
    return {
      "data":tnc,
      "lblTitle": tnc.title || "N/A",
      "lblCode": tnc.code || "N/A",
      "lblApplicableApps": tnc.code === "C360_CustomerOnboarding" ? "Customer 360": (str?str:kony.i18n.getLocalizedString("i18n.frmAlertsManagement.NA")),
      "fonticonActive":{"isVisible":true,"text":"",
                        "onClick":self.termsAndConditionsEditView},
      lblSeparator: "-",
      template: "flxTermsAndConditions"
    };
  },
  setTnCSegmentData: function(resData,isFilter) {
    var self = this;
    var data = [];
    var dataMap = {
      "data":"data",
      "lblTitle": "lblTitle",
      "lblCode":"lblCode", 
      "lblApplicableApps": "lblApplicableApps",
      "fonticonActive": "fonticonActive",
      "lblSeparator": "lblSeperator",
    };
    if(isFilter && this.view.subHeader.tbxSearchBox.text!==""){
      resData=resData.filter(self.searchFilter);//.sort(this.sortBy.sortData);
    }
    if (resData) {
      data = resData.map(self.toTnCSegment.bind(self));
    }
    else if(resData==[])
      resData.map(this.toTnCSegment.bind(self));

    if(!isFilter){
      self.setDataForAppsFilter();
      self.view.flxFilterStatus.skin = "slFbox";
    }
    else{
      self.view.flxFilterStatus.skin = "sknflxCustomertagBlue";
    }
    var data = resData.map(this.toTnCSegment.bind(self));
    this.view.segTandC.widgetDataMap = dataMap;
    this.view.segTandC.setData(data);
    document.getElementById("frmTermsAndConditions_segTandC").onscroll = this.contextualMenuOff;
    this.view.forceLayout();
  },
  showNoRecordAddedYet: function() {
    this.hideAll();
    this.hideHeaderButtons();
  },
  searchFilter: function(tnc) {
    var searchText = this.view.subHeader.tbxSearchBox.text;
    if (typeof searchText === "string" && searchText.length > 0) {
      return (tnc.title.toLowerCase().indexOf(searchText.toLowerCase()) !== -1 || tnc.code.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
    } else {
      return true;
    }
  },
  setDataForAppsFilter : function(){
    var self = this;
    var appsList=[];
    var TnCData = self.TnCJSON;
    if(TnCData !== null){
      for(var j=0;j<TnCData[0].appPreferences.length;j++){
        if(!appsList.contains(TnCData[0].appPreferences[j].appId))
          appsList.push({"id":TnCData[0].appPreferences[j].appId,"desc":TnCData[0].appPreferences[j].appName});
      }
     this.setAppsData(appsList, "apps");
     appsList.push({"id":"NOT_APPLICABLE","desc": kony.i18n.getLocalizedString("i18n.frmAlertsManagement.NA")});
      
    }else{
      TnCData = [];
    }
    var widgetMap = {
      "id":"id",
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var appsData = appsList.map(function(rec){
      return {
        "id":rec.id,
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "checkbox.png",
        "lblDescription": {"text":rec.desc}
      };
    });
    self.view.applicableAppsFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
    self.view.applicableAppsFilterMenu.segStatusFilterDropdown.setData(appsData);
    var indices = [];
    for(var index = 0; index < appsData.length; index++){
      indices.push(index);
    }
    self.view.applicableAppsFilterMenu.segStatusFilterDropdown.selectedIndices = [[0,indices]];
    self.view.forceLayout();
  },
  performAppsFilter: function () {
    var self = this;
    var selApps = [];
    var selInd;
    var dataToShow = [];
    var allData = self.TnCJSON;
    var segAppData = self.view.applicableAppsFilterMenu.segStatusFilterDropdown.data;
    var indices = self.view.applicableAppsFilterMenu.segStatusFilterDropdown.selectedIndices;
    self.inFilter=true;
    if (indices !== null) { //show selected indices data
      selInd = indices[0][1];
      for(var i=0;i<selInd.length;i++){
        selApps.push(self.view.applicableAppsFilterMenu.segStatusFilterDropdown.data[selInd[i]].lblDescription);
      }
      if (selInd.length === segAppData.length) {
        if(self.view.subHeader.tbxSearchBox.text===""){//all are selected
          self.inFilter=false;
          self.setTnCSegmentData(self.TnCJSON,false);
        }else
          self.loadPageData();
      } else {
        dataToShow = allData.filter(function(rec){
          var pref = rec.appPreferences;
          var flag = false;
          for(var i=0;i<selApps.length;i++){
            for(var j=0;j<pref.length;j++){
            if(pref[j].isSupported==="true")
              flag = true;
            if(selApps[i].text === pref[j].appName&&pref[j].isSupported==="true"){
              return rec;
            }
             
            } 
            if((selApps[i].text === kony.i18n.getLocalizedString("i18n.frmAlertsManagement.NA")) && flag === false){
                return rec;
              }
          }

        });
        if (dataToShow.length > 0) {
          self.setTnCSegmentData(dataToShow,true);
        } else {
          self.view.segTandC.setData([]);
        }
      }
    } 
    else {
      self.view.rtxNoRecords.text=kony.i18n.getLocalizedString("i18n.frmUsersController.No_results_found");
      self.view.rtxNoRecords.setVisibility(true);
      self.view.flxNoRecordsFound.setVisibility(true);
      var data=self.view.segTandC.data;
      data=[];
      self.view.segTandC.setData(data);
    }
    self.view.forceLayout();
  },

  showTnCUI: function(tncs) {
    //this.resetEditScreen();
    if (tncs.length !== 0) {
      this.showListingScreen();
    } else {
      if (this.records === 0) {
        this.showNoResultsFound();
        this.view.forceLayout();
      } else {
        this.showNoRecordAddedYet();
      }
    }
    this.view.segTandC.setVisibility(true);
    this.setTnCSegmentData(tncs,false);
    this.view.forceLayout();
  },
  willUpdateUI: function (context) {
    var self = this;
    this.updateLeftMenu(context);
    if(!context){
      return;
    }
    if(context.toastModel){
      if(context.toastModel.status===kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SUCCESS"))
        this.view.toastMessage.showToastMessage(context.toastModel.message,this);
      else
        this.view.toastMessage.showErrorToastMessage(context.toastModel.message,this);
    }else if(context.tncs){
      self.preShowTermsAndConditions();
      self.TnCJSON = context.tncs;
      this.setDataForAppsFilter();
      this.loadPageData = function() {
        var searchResult = context.tncs.filter(self.searchFilter);
        self.records = context.tncs.filter(self.searchFilter).length;
        self.allRecords = context.tncs;
        self.showTnCUI(searchResult);
      };
      this.loadPageData();
    }else if (context.code) {
      this.tabData = context.records;
      this.showTermsAndConditionDetailScreen();
    }else if(context.alertDetails && context.action === "edit"){
      this.showAddTermsAndConditionScreen();
      //this.view.flxAlertTypesContextualMenu.setVisibility(false);
      this.fillTermsAndConditionScreenForEdit(context.records);
    }
    else if(context.termsAndConditionsViewModel){
      this.hideEditScreen();
      if(context.termsAndConditionsViewModel.termsAndConditions !== null){
        this.setTermsAndConditionsData(context.termsAndConditionsViewModel.termsAndConditions);
      }else{
        this.showAddTermsAndConditionsScreen();
      }
      //kony.adminConsole.utils.hideProgressBar(this.view);
    }else if(context.action === "showLoadingScreen"){
      kony.adminConsole.utils.showProgressBar(this.view);
    }else if(context.action === "hideLoadingScreen"){
      kony.adminConsole.utils.hideProgressBar(this.view);
    }
    this.view.forceLayout();
  },
  showAddTermsAndConditionsScreen : function(){
    this.view.flxDetailTermsAndConditions.setVisibility(false);
    this.view.flxNoTermsAndConditions.setVisibility(true);
  },
  hideEditScreen: function(){
    this.view.flxAddTermsAndCondition.setVisibility(false);
    this.view.flxBreadcrumb.setVisibility(false);
  },
   onHoverEventCallback:function(widget, context) {
    var scopeObj = this;
    var widGetId = widget.id;
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER||context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      scopeObj.view[widGetId].setVisibility(true);
    } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      scopeObj.view[widGetId].setVisibility(false);
    }
  },
  mapAppsListData: function(data) {
    var self = this;
    var listBoxData = [];
    var widgetDataMap = {
      "id": "id",
      "lblDescription": "lblDescription",
      "imgCheckBox": "imgCheckBox",
      "flxCheckBox":"flxCheckBox",
      "flxSearchDropDown": "flxSearchDropDown"
    };
    self.view.customListboxApps.segList.widgetDataMap = widgetDataMap;
    if (data) {
      listBoxData = data.map(function(rec) {
        return {
          "id": rec.id,
          "lblDescription": {"text":rec.desc},
          "imgCheckBox": {"src":self.checkboxnormal},
          "template": "flxSearchDropDown"
        };
      });
    }
    return listBoxData;
  },
  setAppsData : function(data,category){
    var self = this;
    var widgetPath ="";
    if(category === "apps"){
      widgetPath = self.view.customListboxApps;
    }
    var appData = self.mapAppsListData(data);
    widgetPath.segList.setData(appData);
    widgetPath.segList.selectedIndices =[[0,[0]]];
    widgetPath.lblSelectedValue.text = self.sAll;
  },
  onClickOfSelectAll : function(category){
    var self = this;
    var widgetPath ="",segData ="";
    if(category === "apps"){
      widgetPath = self.view.customListboxApps;
    }
    segData = widgetPath.segList.data;
    var arr = [];
    for(var i= 0; i< segData.length; i++){
      arr.push(i);
    }
    if(widgetPath.imgCheckBox.src === self.checkboxnormal){
      widgetPath.imgCheckBox.src = self.checkboxselected;
      widgetPath.lblSelectedValue.text = self.sAll;
      widgetPath.segList.selectedIndices = [[0,arr]];
    }else if(widgetPath.imgCheckBox.src === self.checkboxselected){
      widgetPath.imgCheckBox.src = self.checkboxnormal;
      widgetPath.segList.selectedIndices = null;
    }
    self.view.forceLayout();
  },
  onCustomListBoxRowClick : function(category){
    var self = this;
    var widgetPath ="",segData ="";
    if(category === "apps"){
      widgetPath = self.view.customListboxApps;
    }
    segData = widgetPath.segList.data;
    var arr = [];
    for(var i= 0; i< segData.length; i++){
      arr.push(i);
    }
    var selRows = widgetPath.segList.selectedRowItems;
    if(selRows){
      if(selRows.length === segData.length){
        widgetPath.imgCheckBox.src = self.checkboxselected;
        widgetPath.lblSelectedValue.text = self.sAll;
        widgetPath.segList.selectedIndices = [[0,arr]];
      }else{
        widgetPath.lblSelectedValue.text = selRows.length +" "+self.sSelected;
        widgetPath.imgCheckBox.src = self.checkboxnormal;
      }
    } else{
      widgetPath.lblSelectedValue.text = "0 " +self.sSelected;
    }
    self.view.forceLayout();
  },
  /*
   * selects the apps exsisting for alert group
   * @param : app preferences list of alert group
   */
  selectAppsForEdit : function(appData){
    var selectInd = [];
    var segData = this.view.customListboxApps.segList.data;
    for(var i=0;i<segData.length;i++){
      for(var j=0;j<appData.length;j++){
        if(appData[j].isSupported === "true" && appData[j].appId === segData[i].id){
          selectInd.push(i);
        } 
      }
    }
    this.view.customListboxApps.segList.selectedRowIndices = [[0,selectInd]];
    if(segData.length === selectInd.length){
      this.view.customListboxApps.imgCheckBox.src = this.checkboxselected;
      this.view.customListboxApps.lblSelectedValue.text = this.sAll;
    }else{
      this.view.customListboxApps.imgCheckBox.src = this.checkboxnormal;
      this.view.customListboxApps.lblSelectedValue.text = selectInd.length+ " " +this.sSelected;
    }
  },
  createTnCRequestParam :  function(){
    var self = this;
    var reqParam = {};
    var tncText;
    var appList = self.view.customListboxApps.segList.selectedRowItems;
    var appPrefer ={};
    var attribute,condition,attrId;
    if(appList !== null){
      for (var i = 0; i < appList.length; i++) {
        appPrefer[appList[i].id] = true;
      }
    }
    if(self.view.imgYes.src === "radio_selected.png")
    {
      tncText = document.getElementById("iframe_rtxTnCEdit").contentWindow.document.getElementById("editor").innerHTML;
      if(tncText==="" || tncText === "<br>"){
        self.view.flxContentError.isVisible = true;
        return reqParam;
      }
      else{
        self.view.flxContentError.isVisible = false;
     }
    }else{
      tncText = this.view.tbxURLcontent.text;
    }
    reqParam = {
      "termsAndConditionsCode": self.view.segTandC.selecteditems[0].lblCode,
      "contentType": self.view.imgYes.src === "radio_selected.png"? "TEXT":"URL",
      "languageCode": "en-US",
      "termsAndConditionsTitle": self.view.tbxTitle.text,
      "termsAndConditionsDescription": self.view.txtAreaDescription.text,
      "termsAndConditionsContent": tncText,
      "appPreferences":appPrefer,
    };
    return reqParam;
  },
  editTnCRequestParam : function(){
    var self =this;
    var appPrefer ={};
    var selAppList=[];
	if(self.view.customListboxApps.segList.selectedRowIndices !== null)
         selAppList = self.view.customListboxApps.segList.selectedRowIndices[0][1];
    var appSegData = self.view.customListboxApps.segList.data;
    var initialParam = self.createTnCRequestParam();
    for(var i=0;i<appSegData.length;i++){
      if(selAppList.contains(i)){
        appPrefer[appSegData[i].id] = true;
      }else{
        appPrefer[appSegData[i].id] = false;
      }
    }
    initialParam.appPreferences = appPrefer;
    return initialParam;
  },
  editTnC : function(){
    var self =this;
    var reqParam = self.editTnCRequestParam();
    self.presenter.editTermsAndConditions(reqParam);
  },
  setEditViewData: function(){
    var selectedItem = this.view.segTandC.selecteditems[0];
    this.view.tbxTitle.text=selectedItem.lblTitle;
    var data = selectedItem.data;
    this.view.txtAreaDescription.text=data.description;
    this.selectAppsForEdit(data.appPreferences);
    this.view.flxBreadcrumb.setVisibility(true);
    this.view.flxContentError.setVisibility(false);
    this.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmTermsAndConditionsController.Edit")+ kony.i18n.getLocalizedString("i18n.TermsAndConditions.addTnC");		
    this.view.breadcrumbs.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.frmTermsAndConditionsController.EDIT")+ " " +kony.i18n.getLocalizedString("i18n.TermsAndConditions.addTnC").toUpperCase();
    if(data.termsAndConditionsContent["en-US"].contentTypeId === "TEXT")
    {
      this.view.imgYes.src = "radio_selected.png";
      this.view.imgNo.src = "radio_notselected.png";
      var tncEditorDocument = document.getElementById("iframe_rtxTnCEdit").contentWindow.document;
      tncEditorDocument.getElementById("editor").innerHTML = data.termsAndConditionsContent["en-US"].content;
      this.view.flxURLTextBox.setVisibility(false);
      this.view.flxContentRichTxt.setVisibility(true);
    }else{
      this.view.imgYes.src = "radio_notselected.png";
      this.view.imgNo.src = "radio_selected.png";
      this.view.tbxURLcontent.text = data.termsAndConditionsContent["en-US"].content;
      this.view.flxContentRichTxt.setVisibility(false);
      this.view.flxURLTextBox.setVisibility(true);
   }
    this.view.forceLayout();
  },
  flowActions :function(){
    var scopeObj=this;
    scopeObj.view.flxApplicableAppsFilter.onHover = scopeObj.onHoverEventCallback;
    // Add Static content
    this.view.flxYes.onTouchStart = function(){
     scopeObj.view.textToUrlPopup.rtxPopUpDisclaimer.text=kony.i18n.getLocalizedString("i18n.TermsAndConditions.UrlToTextMessage");
     scopeObj.view.flxTextToUrlPopup.isVisible = true;
     scopeObj.view.forceLayout();
    };
    this.view.flxNo.onTouchStart = function(){
      scopeObj.view.textToUrlPopup.rtxPopUpDisclaimer.text=kony.i18n.getLocalizedString("i18n.TermsAndConditions.TextToUrlMessage");
      scopeObj.view.flxTextToUrlPopup.isVisible = true;
      scopeObj.view.forceLayout();
    };
    this.view.textToUrlPopup.btnPopUpCancel.onClick = function(){
      scopeObj.view.flxTextToUrlPopup.isVisible = false;
    };
    this.view.textToUrlPopup.btnPopUpDelete.onClick = function(){
      scopeObj.toggleRadio();
      scopeObj.view.flxTextToUrlPopup.isVisible = false;
      scopeObj.view.forceLayout();
    };
    this.view.textToUrlPopup.flxPopUpClose.onClick = function(){
      scopeObj.view.flxTextToUrlPopup.isVisible = false;
    };
    this.view.customListboxApps.flxSelectedText.onClick = function(){
      scopeObj.view.customListboxApps.flxSegmentList.setVisibility(scopeObj.view.customListboxApps.flxSegmentList.isVisible === false);
    };
    this.view.customListboxApps.flxCheckBox.onClick = function(){
      scopeObj.onClickOfSelectAll("apps");
    };
    this.view.customListboxApps.segList.onRowClick = function(){
      scopeObj.onCustomListBoxRowClick("apps");
    };
    this.view.customListboxApps.flxDropdown.onClick = function(){
      scopeObj.view.customListboxApps.flxSegmentList.setVisibility(scopeObj.view.customListboxApps.flxSegmentList.isVisible === false);
    };
    this.view.customListboxApps.btnOk.onClick = function(){
      scopeObj.view.customListboxApps.flxSegmentList.setVisibility(scopeObj.view.customListboxApps.flxSegmentList.isVisible === false);
    };
    this.view.flxViewEditButton.onClick = function(){
      scopeObj.termsAndConditionsEditView();
    };
    this.view.segTandC.onRowClick=function(){
      scopeObj.termsAndCondtionsDetailView();
    };
    this.view.applicableAppsFilterMenu.segStatusFilterDropdown.onRowClick=function(){
      scopeObj.currentPage=1;
      scopeObj.performAppsFilter();
    };
    this.view.noStaticData.btnAddStaticContent.onClick = function(){
      scopeObj.view.flxAddTermsAndCondition.isVisible =true;
      scopeObj.view.flxNoTermsAndConditions.isVisible = false;
      scopeObj.view.flxNoTnCError.isVisible = false;
      scopeObj.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmTermsAndConditionsController.Add")+ kony.i18n.getLocalizedString("i18n.TermsAndConditions.addTnC");
      scopeObj.view.flxBreadcrumb.setVisibility(false);
      var tncEditorDocument = document.getElementById("iframe_rtxTnC").contentWindow.document;
      tncEditorDocument.getElementById("editor").innerHTML = "";
      tncEditorDocument.getElementsByClassName("table-palette")[0].style.marginLeft = "0px";
    };
    this.view.subHeader.tbxSearchBox.onKeyUp = function() {
      scopeObj.loadPageData();
      if(scopeObj.view.subHeader.tbxSearchBox.text === ""){
        scopeObj.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100";
        scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
      	scopeObj.hideNoResultsFound();
      	scopeObj.view.forceLayout();
      }else{
        scopeObj.view.subHeader.flxSearchContainer.skin ="slFbox0ebc847fa67a243Search";
        scopeObj.view.subHeader.flxClearSearchImage.setVisibility(true);
      }
      if (scopeObj.records === 0) {
        scopeObj.showNoResultsFound();
        scopeObj.view.forceLayout();
      } else {
        scopeObj.hideNoResultsFound();
        scopeObj.view.forceLayout();
      }
      scopeObj.view.forceLayout();
    };
    this.view.subHeader.flxClearSearchImage.onClick = function() {
      scopeObj.view.subHeader.tbxSearchBox.text = "";
      scopeObj.view.subHeader.flxSearchContainer.skin = "sknflxd5d9ddop100";
      scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
      scopeObj.loadPageData();
      scopeObj.hideNoResultsFound();
      scopeObj.view.forceLayout();
    };
    // On Add T&C Cancel btn 
//     this.view.fontIconFilterStatus.onTouchStart = function(){
//       var flxLeft = scopeObj.view.flxApplicableApps.frame.x;
//       scopeObj.view.flxApplicableAppsFilter.left = (flxLeft-20)+"px";
       
//       scopeObj.view.flxApplicableAppsFilter.setVisibility(!scopeObj.view.flxApplicableAppsFilter.isVisible);
//       scopeObj.view.forceLayout();
//     };
    this.view.commonButtons.btnCancel.onClick  = function(){
      scopeObj.view.flxTandCPopUp.setVisibility(true);
    };

    // On Add T&C Save btn 
    this.view.commonButtons.btnSave.onClick = function(){
      if(scopeObj.validateTnC())
      	scopeObj.editTnC();
    };
    this.view.tandCPopUp.flxPopUpClose.onTouchStart = function(){
      scopeObj.view.flxTandCPopUp.setVisibility(false);
    };
    // For Edit click
    this.view.staticData.flxEditOption.onTouchStart = function(){
      scopeObj.editPage=1;
      scopeObj.view.flxAddTermsAndCondition.isVisible =true;
      scopeObj.view.flxNoTermsAndConditions.isVisible = false;
      scopeObj.view.flxDetailTermsAndConditions.isVisible = false;
      scopeObj.view.flxNoTnCError.isVisible = false;
      scopeObj.view.mainHeader.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmTermsAndConditionsController.Edit")+ kony.i18n.getLocalizedString("i18n.TermsAndConditions.addTnC");
      scopeObj.view.flxBreadcrumb.setVisibility(true);
      scopeObj.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.TermsAndConditions.addTnC").toUpperCase();
      scopeObj.view.breadcrumbs.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.frmTermsAndConditionsController.EDIT")+ " " +kony.i18n.getLocalizedString("i18n.TermsAndConditions.addTnC").toUpperCase();
      var tncEditorDocument = document.getElementById("iframe_rtxTnC").contentWindow.document;
      var tncViewerDocument = document.getElementById("iframe_rtxViewer").contentWindow.document;
      tncEditorDocument.getElementById("editor").innerHTML = tncViewerDocument.getElementById("viewer").innerHTML;
      tncEditorDocument.getElementsByClassName("table-palette")[0].style.marginLeft = "0px";
      if(scopeObj.view.flxDetailTermsAndConditions.staticData.flxStaticContantHeader.flxStatus.lblstaticContentStatus.text === kony.i18n.getLocalizedString("i18n.secureimage.Inactive")){
        scopeObj.view.SwitchToggleStatus.selectedindex = 1;
      }else{
        scopeObj.view.SwitchToggleStatus.selectedindex = 0;
      }
      scopeObj.view.forceLayout();
    };
    this.view.breadcrumbs.btnBackToMain.onClick = function(){
      scopeObj.hideAll();
      scopeObj.view.flxMainSubHeader.setVisibility(true);
      scopeObj.view.flxTermsAndConditionsList.setVisibility(true);
      scopeObj.view.flxBreadcrumb.setVisibility(false);
    };
	this.view.tandCPopUp.btnPopUpCancel.onClick = function(){
      scopeObj.view.flxTandCPopUp.isVisible =false;
    };
    this.view.tandCPopUp.btnPopUpDelete.onClick = function(){
      scopeObj.view.flxTandCPopUp.setVisibility(false);
      scopeObj.view.flxNoTermsAndConditions.setVisibility(false);
      scopeObj.view.flxDetailTermsAndConditions.setVisibility(false);
      scopeObj.view.flxTermsAndConditionsList.setVisibility(true);
      scopeObj.view.flxMainSubHeader.setVisibility(true);
      scopeObj.view.flxBreadcrumb.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    this.view.staticData.flxSelectOptions.onHover = this.onDropDownsHoverCallback;
    this.view.tbxTitle.onBeginEditing = function(){
      scopeObj.view.lblHeadingCount.setVisibility(true);
      scopeObj.view.lblHeadingCount.text = scopeObj.view.tbxTitle.text.length + "/50";
      scopeObj.view.forceLayout();
    };
    this.view.tbxTitle.onKeyUp = function(){
      if(scopeObj.view.tbxTitle.text.trim().length!==0){
        scopeObj.view.tbxTitle.skin="skntxtbx484b5214px";
		scopeObj.view.flxTitleError.setVisibility(false);
      }
      scopeObj.view.lblHeadingCount.text = scopeObj.view.tbxTitle.text.length + "/50";
      scopeObj.view.forceLayout();
    };
    this.view.tbxTitle.onBeginEditing = function(){
      scopeObj.view.lblHeadingCount.setVisibility(true);
      scopeObj.view.lblHeadingCount.text = scopeObj.view.tbxTitle.text.length + "/50";
      scopeObj.view.forceLayout();
    };
    this.view.tbxTitle.onEndEditing = function(){
      scopeObj.view.lblHeadingCount.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    this.view.txtAreaDescription.onBeginEditing = function(){
      scopeObj.view.lblDescriptionCount.setVisibility(true);
      scopeObj.view.lblDescriptionCount.text = scopeObj.view.txtAreaDescription.text.length + "/250";
      scopeObj.view.forceLayout();
    };
    this.view.txtAreaDescription.onKeyUp = function(){
      scopeObj.view.lblDescriptionCount.text = scopeObj.view.txtAreaDescription.text.length + "/250";
      scopeObj.view.forceLayout();
    };
    this.view.txtAreaDescription.onEndEditing = function(){
      scopeObj.view.lblDescriptionCount.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    this.view.rtxTnCEdit.onKeyUp = function(){
      var tncEditorDocument = document.getElementById("iframe_rtxTnCEdit").contentWindow.document;
      if (tncEditorDocument.getElementById("editor").innerHTML.trim().length !== 0) {
        scopeObj.view.flxContentRichTxt.skin = "sknflxffffffop100Bordercbcdd1Radius3px";
        scopeObj.view.flxContentError.setVisibility(false);
      }
    };
    this.view.tbxURLcontent.onKeyUp = function(){
      if(scopeObj.view.tbxURLcontent.text.trim().length!==0){
        scopeObj.view.tbxURLcontent.skin="skntxtbx484b5214px";
        scopeObj.view.flxContentError.setVisibility(false);
      }
    };
  },
   hideNoResultsFound: function() {
    this.view.flxTandCList.setVisibility(true);
    this.view.flxNoRecordsFound.setVisibility(false);
  },
  termsAndConditionsEditView: function(){
    this.hideAll();
    this.view.flxMainSubHeader.setVisibility(false);
    this.view.flxDetailBody.setVisibility(false);
    this.view.flxEditBody.setVisibility(true);
    this.setEditViewData();
    this.view.flxViewEditButton.setVisibility(false);
    this.view.flxDetailTermsAndConditions.setVisibility(true);
    this.view.flxEditBodyWrapper.setContentOffset({
            y: 0,
            x: 0
        });
   var selectedItem = this.view.segTandC.selecteditems[0];
   this.view.lblTandCHeading.text=selectedItem.lblTitle;
    
  },
  validateTnC: function() {
    var flag = true;
    var tncEditorDocument = document.getElementById("iframe_rtxTnCEdit").contentWindow.document;
    if (this.view.tbxTitle.text.trim().length === 0) {
      flag = false;
      this.view.tbxTitle.skin = "skinredbg";
      this.view.flxTitleError.setVisibility(true);
      this.view.scrollToWidget(this.view.tbxTitle);
    }
    if ((this.view.flxURLTextBox.isVisible && this.view.tbxURLcontent.text.trim().length === 0)) {
      flag = false;
      this.view.tbxURLcontent.skin = "skinredbg";
      this.view.lblContextErrorMessage.text = kony.i18n.getLocalizedString("i18n.frmTermsAndConditions.contentBlankMessage");
      this.view.flxContentError.setVisibility(true);
      this.view.scrollToWidget(this.view.tbxURLcontent);
    } else if (this.view.flxContentRichTxt.isVisible && tncEditorDocument.getElementById("editor").innerHTML.trim().length === 0) {
      flag = false;
      this.view.flxContentRichTxt.skin = "sknRedBorder";
      this.view.lblContextErrorMessage.text = kony.i18n.getLocalizedString("i18n.frmTermsAndConditions.contentBlankMessage");
      this.view.flxContentError.setVisibility(true);
      this.view.scrollToWidget(this.view.flxContentRichTxt);
    }
    return flag;
  },
  termsAndCondtionsDetailView: function(){
    var selectedItem = this.view.segTandC.selecteditems[0];
    this.hideAll();
    this.view.flxMainSubHeader.setVisibility(false);
    this.view.flxDetailTermsAndConditions.setVisibility(true);
    this.view.flxDetailBody.setVisibility(true);
    this.view.flxEditBody.setVisibility(false);
    this.view.flxViewEditButton.setVisibility(true);
    this.view.breadcrumbs.lblCurrentScreen.text=selectedItem.lblTitle.toUpperCase();
    this.view.flxBreadcrumb.setVisibility(true);
    this.view.lblTandCHeading.text=selectedItem.lblTitle;
    this.view.lblTitleValue.text=selectedItem.lblTitle;
    this.view.lblApplicableAppsValue.text=selectedItem.lblApplicableApps;
    this.view.lblCodeValue.text=selectedItem.lblCode;
    var data = selectedItem.data;
    this.view.lblDescriptionValue.text = data.description;
    if(data.termsAndConditionsContent["en-US"].contentTypeId === "URL"){
      this.view.lblUrlContent.setVisibility(true);
      this.view.staticData.setVisibility(false);
      this.view.lblUrlContent.text = data.termsAndConditionsContent["en-US"].content; 
    }
    else{
      this.view.staticData.setVisibility(true);
      this.view.lblUrlContent.setVisibility(false);
    if(document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer")) {
      document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML = data.termsAndConditionsContent["en-US"].content;
    } else {
      if(!document.getElementById("iframe_rtxViewer").newOnload) {
        document.getElementById("iframe_rtxViewer").newOnload = document.getElementById("iframe_rtxViewer").onload;
      }
      document.getElementById("iframe_rtxViewer").onload = function() {
        document.getElementById("iframe_rtxViewer").newOnload();
        document.getElementById("iframe_rtxViewer").contentWindow.document.getElementById("viewer").innerHTML ="";
      };
    }
    }
    this.view.flxDetailTermsAndConditions.setContentOffset({
                y:0,
                x:0
            });
    this.view.flxDetailBody.setContentOffset({
                y:0,
                x:0
            });
    this.view.forceLayout();
  },
  showNoResultsFound: function() {
   this.view.flxTandCList.setVisibility(false);
    this.view.rtxNoRecords.text = kony.i18n.getLocalizedString("i18n.frmPoliciesController.No_results_found") + '"' + this.view.subHeader.tbxSearchBox.text + '"' + kony.i18n.getLocalizedString("i18n.frmPoliciesController.Try_with_another_keyword");
    this.view.flxNoRecordsFound.setVisibility(true);
  },
  onDropDownsHoverCallback:function(widget,context){
    var self=this;
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      self.view.staticData.flxSelectOptions.setVisibility(true);
    } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      self.view.staticData.flxSelectOptions.setVisibility(false);
    }
    self.view.forceLayout();
  },
  toggleRadio : function(){
   var scopeObj = this;
    if(scopeObj.view.flxURLTextBox.isVisible){
      scopeObj.view.tbxURLcontent.text="";
      scopeObj.view.imgYes.src = "radio_selected.png";
      scopeObj.view.imgNo.src = "radio_notselected.png";
      scopeObj.view.flxURLTextBox.setVisibility(false);
      scopeObj.view.flxContentRichTxt.setVisibility(true);
    }
  else {
      var tncEditorDocument = document.getElementById("iframe_rtxTnCEdit").contentWindow.document;
      tncEditorDocument.getElementById("editor").innerHTML = "";
      scopeObj.view.imgNo.src = "radio_selected.png";
      scopeObj.view.imgYes.src = "radio_notselected.png";
      scopeObj.view.flxContentRichTxt.setVisibility(false);
      scopeObj.view.flxURLTextBox.setVisibility(true);
     }
    this.view.tbxURLcontent.skin="skntxtbx484b5214px";
    this.view.flxContentRichTxt.skin="sknflxffffffop100Bordercbcdd1Radius3px";
    this.view.flxContentError.setVisibility(false);
  }
});
